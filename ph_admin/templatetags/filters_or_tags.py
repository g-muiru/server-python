import ph_model.models as ph_model
from django import template
from ph_util import mixins
import enum
from django.conf import settings


class EVENT_URL_CONFIG(mixins.PairEnumMixin, enum.Enum):
    """EVENT URL config."""
    GRID = 'http://{}/#/details/content/grid/{}'
    QUEEN = 'http://{}/#/details/content/queen/{}'
    CIRCUIT = 'http://{}/#/details/content/circuit/{}'
    POWERSTATION = '{}/#/details/content/powerstation/{}'
    INVERTER = '{}/#/details/content/inverter/{}'

register = template.Library()

@register.simple_tag
def construct_event_url(eventType, itemId):
    urlTemplate = EVENT_URL_CONFIG.value_of_case_insensitive(eventType) or "http://{}".format(settings.PH_SERVER)
    if urlTemplate:
        return urlTemplate.format(settings.PH_UI, itemId)

