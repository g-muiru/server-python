from django.conf.urls import include, url
from urlmiddleware.conf import middleware, mpatterns
from ph_common_api import middleware_manager as ph_middleware_manager


from ph_admin import views

urlpatterns = [
       url('^register/user', views.UserCreationView.as_view(), name='user-creation' ),
       url('^register/customer', views.CustomerCreationView.as_view(),  name='customer-creation'),
       url('^register/honeycomb', views.HoneycombCustomerCreationView.as_view(),  name='honeycomb-customer-creation'),
       url('^sms-tool', views.SmsToolView.as_view(),  name='sms-tool'),
       url('^logout', views.LogoutView.as_view(),  name='logout'),
       url(r'hooks/queenfirmwares', views.QueenFirmwareUpdateView.as_view(), name='queen_firmware_update'),
       url(r'hooks/smsinbound', views.SMSInboundView.as_view(), name='sms_inboud_view'),
       url(r'hooks/payment/mvola', views.SMSInboundView.as_view(), name='payment-mvola')
]


middlewarepatterns = mpatterns('',
    middleware(r'^hooks/', ph_middleware_manager.DisableCSRFIfNotRequired),
)
