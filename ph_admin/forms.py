from django import forms
import ph_model.models as ph_model
from ph_model.models import municipality

class CustomerCreationForm(forms.ModelForm):
    firstName = forms.CharField(label='First Name', max_length=100)
    lastName = forms.CharField(label='Last Name', max_length=100)
    phoneNumber = forms.CharField(label='Phone Number', max_length=100)
    email = forms.EmailField(label='Email')
    municipality = forms.ModelChoiceField(queryset=municipality.Municipality.objects.all())

    class Meta:
        model = ph_model.customer.Customer
        fields = ('firstName', 'lastName', 'email', 'phoneNumber', 'municipality')

class SmsToolForm(forms.Form):
    targetCsv = forms.FileField(label='Recipients CSV')
    message = forms.CharField(label='Message', max_length=160, widget=forms.TextInput(attrs={'size':160}))

    class Meta:
        fields = ('targetCsv', 'message')
