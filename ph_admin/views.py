""" set default encode to utf-8 to ameliorate unicode problems til we can upgrade to 3 """
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from django.contrib.auth import forms as django_forms
from django.contrib.auth.views import logout
import forms as ph_forms
from django.views.generic.edit import FormView
from ph_operation import customer_manager
from django.views.generic.base import View
from django import  http
from ph_operation import queen_firmware_manager
from ph_operation import sms_manager
from ph_util import email_util
import logging
import json
import csv

LOGGER = logging.getLogger(__name__)

class UserCreationView(FormView):
    """Django user admin view."""
    template_name = 'user_register.html'
    form_class = django_forms.UserCreationForm
    success_url = '/admin/auth/user/'

    def form_valid(self, form):
        form.save()
        return super(UserCreationView, self).form_valid(form)


class CustomerCreationView(FormView):
    """powerhive customer creation view."""
    template_name = 'customer_register.html'
    form_class = ph_forms.CustomerCreationForm
    success_url = '/admin/ph_model/customer/'

    def form_valid(self, form):
        # don't save to db here, let the customer class do it
        #form.save()
        # !!! TODO: fix the customer class account creation :/
        customer = form.instance
        customer_manager.CustomerManager.initialize_customer(customer)
        return super(CustomerCreationView, self).form_valid(form)


class HoneycombCustomerCreationView(View):
    """user creation handler for posts from honeycomb/angular"""
    def get(self, request):
        raise Exception('not supported.')

    def post(self, request):
        data = json.loads(request.body)
        form = ph_forms.CustomerCreationForm(data)
        if form.is_valid():
            customer = form.instance
            customer_manager.CustomerManager.initialize_customer(customer)
            return http.HttpResponse('success')
        else:
            return http.HttpResponse(form.errors.as_json())

class SmsToolView(FormView):
    """mass sms tool"""
    template_name = 'sms_tool.html'
    form_class = ph_forms.SmsToolForm
    success_url = '/ph_admin/sms-tool/success'

    def post(self, request, *args, **kwargs):
        form = ph_forms.SmsToolForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['targetCsv']
            message = request.POST['message']
            results = sms_manager.send_mass_sms(file, message)
            return http.HttpResponse(json.dumps(results), content_type='application/json')
        else:
            return http.HttpResponse(form.errors.as_json())

class QueenFirmwareUpdateView(View):
    """Sync queen firmwares."""
    def get(self, request):
        """Sync released queen firmwares to list of available firmwares,"""
        queen_firmware_manager.QueenFirmwareManager.sync_firmwares()
        return http.HttpResponse('Firmwares sync done!!!')

class LogoutView(View):
    """Logout"""
    def get(self, request):
        logout(request)
        return http.HttpResponse('Logout')



class SMSInboundView(View):
    """Process incoming sms."""
    def get(self, request):
        """Process get sms, if the notifier happended to send via get request"""
        raise Exception('Incoming message access via get not supported.')

    def post(self, request):
        """Process inbound message."""
        #TODO(estifanos): Process the actual sms message remove email notification not sure if is important.
        data = {"path": request.get_full_path(),
                "message": request.body,
                "requester": request.get_host()}
        #email_util.send_template_email("Incmoing SMS Message", 'hook_notification_template.html', data)
        sms_manager.CustomerSMSManager.fetch_incoming_sms_message()
        return http.HttpResponse('SMS Message processed')






