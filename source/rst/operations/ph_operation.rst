:orphan:

Operations
==========
.. automodule:: ph_operation.airtel_payment_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.bonus_power_cron
    :members:
    :undoc-members:
.. automodule:: ph_operation.circuit_analyzer
    :members:
    :undoc-members:
.. automodule:: ph_operation.circuit_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.cron
    :members:
    :undoc-members:
.. automodule:: ph_operation.customer_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.event_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.fulcrum_customer_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.fulcrum_import_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.loan_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.monthly_commercial_tariff_fixed_fee
    :members:
    :undoc-members:
.. automodule:: ph_operation.payment_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.queen_firmware_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.scratchcard_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.sms_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.statement_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tariff_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.transaction_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.base_test
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_airtel_payment_manger
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_circuit_analyzer
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_circuit_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_customer_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_event_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_fulcrum_customer_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_fulcrum_import_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_loan_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_payment_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_queen_firmware_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_scratchcard_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_sms_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_tariff_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.tests_transaction_manager
    :members:
    :undoc-members:
.. automodule:: ph_operation.tests.unit.tests_tariff_charge
    :members:
    :undoc-members:
