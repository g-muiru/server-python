Register User
=============

TODO: example of all possible rst stuff


TODO: put this in an understandable format


to create a new user (o&m / technical, not customer)

[12:31]
first visit http://data.powerhive.com/ph_admin/register/user while signed in as superuser

[12:31]
and create the username and password

[12:31]
then go to http://data.powerhive.com/admin/auth/user/ and fill in the rest of the details

[12:31]
THEN,

Elliot Kulakow [12:32]
Nice, yeah I actually don't know how that works lol

[12:32]
We need to do the permissions thing for any new grid too

Ben Rosenberg [12:32]
go to http://data.powerhive.com/admin/ph_model/permission/ and add a permission for the grid you want the user to have access to

[12:32]
for any grid at all

[12:32]
if you dont add a grid permission the account wont be able to do much of anything

[12:33]
you can add view-only perms

[12:33]
and so, having said all that

[12:33]
the user you have now created, will not be able to access django suit

[12:33]
so if you logout and try to login to django suit with that account, you’ll get auth errors

[12:33]
but if you go to honeycomb.powerhive.com and login, that will work

[12:34]
so for instance, i made a new account for ken@reliantlabs to do testing

[12:34]
and he access to the unallocated grid only

[12:34]
that grid is really full of stuff and takes a lot of time to load, so we might want to create a testing grid and move the queen he’s working on into it

[12:35]
in fact i may do that yet today

[12:35]
or a reliantlabs grid or whatever

[12:35]
and then we can have separate grids for UL and BEMA or whatever the case, as needed

Steve Hermes [12:37]
@ben good synopsis @elliot had we put our chat on a public channel it would of had a lot of noise included  --- so @ben thanks for summarizing and stuff like this belongs in the documentation

Elliot Kulakow [12:40]
Nice


think it might be documented somewhere, but

[12:41]
i dont recall where offhand

[12:41]
the usual suspects

[12:41]
oh probably not the grid permissions stuff tho

[12:43]
oh yeah, it is all there

[12:43]
https://github.com/Powerhive1/Server-Python/wiki/create-new-user