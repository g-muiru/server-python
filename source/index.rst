

Powerhive's Django Server documentation
=======================================

.. toctree::
    :glob:

    rst/overview

How to
======

**TODO: Will be moving all the pertinent wiki documentation here so it becomes part of the code base**


:ref:`Register User`

.. toctree::
    :maxdepth: 4
    :caption: Contents:
        admin/admin
        common_api/common_api
        ext_lib/ext_lib
        finance/finance
        firmware_grid/firmware_grid
        mobile_payment_service/mobile_payment_service
        models/models
        models/migrations/migrations
        om_chart_api/om_chart_api
        om_grid_api/om_grid_api
        operations/ph_operation
        powerhive/powerhive
        script/script
        smart_selects/smart_selects
        url_middleware/url_middleware
        util/util

How to:
=======

.. toctree::
    :glob:

    rst/register_user

Read me
=======

.. toctree::
    :glob:

    md/README
