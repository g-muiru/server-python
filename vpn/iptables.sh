#!/bin/sh
NAME=iptables
DESC="iptables config"

case "$1" in
  start)
        iptables -t nat -I POSTROUTING -s 172.31.0.0/20 -o eth0 -j MASQUERADE
        ;;
  stop)
        iptables -t nat -F
        ;;
     *)
        N=$NAME
        echo "Usage: $N {start|stop}" >&2
        exit 1
        ;;
esac

exit 0

~                                                                                                                                                    
~                        
