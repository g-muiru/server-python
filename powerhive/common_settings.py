# coding=utf-8
"""
Django settings common to all configs for powerhive project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
import os
import warnings
from decimal import Decimal

# VAT in multiplier
VAT_MULTIPLIER = Decimal(.16)
# noinspection PyCompatibility
warnings.filterwarnings(
    u'ignore',
    message=u'DateTimeField AggregateGridTracking.lastRun received a naive datetime',
    category=RuntimeWarning, )

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

SETTING_PATH = os.path.dirname(os.path.dirname(__file__))

# Path to setting
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Project directory
PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#21@zasncy@$-=*j##f@*+)2k9sy8q-@r-eo-_r9km47s0fcp)'

FULCRUM_API_TOKEN = 'f499e4fc04efa0644080afdf0bf46f29306b6141f411fee7c03de459f33b1c5520e3faacc9571445'

# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'noreply@powerhive.com'
SERVER_EMAIL = 'noreply@powerhive.com'
EMAIL_HOST_PASSWORD = 'Drone1234!'
EMAIL_USE_TLS = True
POWERHIVE_NO_REPY = 'noreply@powerhive.com'
POWERHIVE_DEV_TEAM = ['dev@powerhive.com','j.machira@powerhive.com']
ADMINS = [('Devs', 'dev@powerhive.com')]
MANAGERS = ADMINS

FIRMWARE_RELATIVE_URL = 'api/queenfirmwares'
DEFAULT_FIRMWARE_FILE_NAME = 'queen_firmware'
FIRMWARE_PATH = '/powerhive/resource/firmware'
FIRMWARE_FILE_EXTENSION = '.bin'

PH_SERVER = 'datadev.powerhive.com'
PH_UI = 'honeycombdev.powerhive.com'

DISABLE_SMS = False

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            "{}/ph_admin/templates".format(PROJECT_PATH),
            "{}/ph_admin/email_templates".format(PROJECT_PATH)
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                'django.template.context_processors.request'
            ]
        }
    }
]

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_csv_exports',
    'activity_log',
    'django_extensions',
    'rest_framework',
    'django_nvd3',
    'djangobower',
    'mptt',
    'ph_model',
    'ph_admin',
    'ph_common_api',
    'ph_ext_lib',
    'ph_firmware_grid_api',
    'ph_mobile_payment_service',
    'ph_operation',
    'ph_om_chart_api',
    'ph_om_grid_api',
    'ph_util',
    'django_cron',
    'ph_script',
    'corsheaders',
    'requests',
    'import_export',
    'memoize'
)

MIDDLEWARE_CLASSES = [
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'activity_log.middleware.ActivityLogMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'urlmiddleware.URLMiddleware',
    'ph_common_api.middleware_manager.DisableCSRFIfNotRequired',
    'ph_common_api.middleware_manager.ExceptionMiddleware',
]

CRON_CLASSES = [
    "ph_operation.cron.ProcessUsageTariff",
    "ph_operation.cron.ProcessBalances",
    "ph_operation.cron.ProcessExpiredEvents",
    "ph_operation.cron.NotifyCronJobStatus",
    "ph_operation.cron.SyncInboundSMS",
    "ph_operation.cron.DeductLoan",
    "ph_operation.cron.MonthlyStatement",
    "ph_operation.cron.CustomerDailyUpdate",
    "ph_operation.cron.UpdateMasterTransaction",
    "ph_operation.cron.ImportFromFulcrum",
    "ph_operation.cron.CloverfieldCustomerImport",
    "ph_operation.cron.ProcessPaidDeposits",
    "ph_operation.cron.ClearCircuitOverrides",
    "ph_operation.cron.QueenConnectionMonitor",
    "ph_operation.cron.GridConnectionMonitor",
    # "ph_operation.cron.SendReminderSMS",
    # "ph_finance.cron.ProcessDailyGridActivity",
    "ph_script.cron.FirstSolarByWeeklyReportCron",
    "ph_script.cron.NetSuiteDailyCsvCron",
    "ph_script.cron.DailyMaintenanceCron",
    "ph_script.cron.FirstUsageCheckCron"
]

NUM_CRON_CLASSES = len(CRON_CLASSES)

DJANGO_CRON_LOCK_BACKEND = 'django_cron.backends.lock.file.FileLock'
DJANGO_CRON_LOCKFILE_PATH = '/tmp'

ROOT_URLCONF = 'powerhive.urls'

WSGI_APPLICATION = 'powerhive.wsgi.application'

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = "/"

AUTH_PROFILE_MODULE = 'ph_model.user.User'
# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Los_Angeles'

USE_I18N = True

USE_L10N = True

USE_TZ = True

APPEND_SLASH = True

# ActivityLog settings
ACTIVITYLOG_AUTOCREATE_DB = False

# Log anonimus actions?
ACTIVITYLOG_ANONIMOUS = False

# Update last activity datetime in user profile. Needs updates for user model.
ACTIVITYLOG_LAST_ACTIVITY = False

# Only this methods will be logged
ACTIVITYLOG_METHODS = ('DELETE', 'POST', 'OPTIONS', 'PUT')

# List of response statuses, which logged. By default - all logged.
# Don't use with ACTIVITYLOG_EXCLUDE_STATUSES
# ACTIVITYLOG_STATUSES = (200, )

# List of response statuses, which ignores. Don't use with ACTIVITYLOG_STATUSES
# ACTIVITYLOG_EXCLUDE_STATUSES = (302, )

# URL substrings, which ignores
ACTIVITYLOG_EXCLUDE_URLS = ('/admin/activity_log/activitylog', '/api/',)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

# Extension url used to fetch static files e.g. data.powerhive.com/static/..
STATIC_URL = '/static/'

# Rest api setup
REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        # 'rest_framework.permissions.IsAuthenticated',
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'TEST_REQUEST_DEFAULT_FORMAT': 'json'
}

# Settings for Cross Origin Resource Sharing
# Only domains listed in the whitelist will be allowed.
# TODO: Prod setup of OM app. Add domain to whitelist.
CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = (
    '127.0.0.1:8000',
    'honeycomb.powerhive.com',
    'honeycombdev.powerhive.com'
)

# Relative location to ./manage.py where static files copied to.
STATIC_ROOT = 'static'

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

SUIT_CONFIG = {
    # Set to empty string if you want to hide search from menu
    'SEARCH_URL': ''
}

# used for various accounting tasks
DEFAULT_ACCOUNTING_EMAILS = {'s.hermes@powerhive.com', 'invoice@powerhive.com'}
TEST_EMAILS = {'s.hermes@powerhive.com'}
# TODO set this to empty string "" when you want to email everyone
ACCOUNTING_TEST = "test"
