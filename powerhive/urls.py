from django.conf.urls import  include, url
from django.contrib import admin
from django.contrib.auth import middleware as django_auth_middleware#.AuthenticationMiddleware
from ph_admin import urls as ph_admin_urls
from ph_common_api import auth
from ph_common_api import middleware_manager
from urlmiddleware.conf import middleware, mpatterns
from ph_firmware_grid_api import firmware_middleware
from ph_mobile_payment_service import  urls as mpservice_urls
import django

from ph_model.admin import admin as ph_model_admin

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	url(r'^api/', include('ph_firmware_grid_api.urls')),
    url(r'^om/chart/', include('ph_om_chart_api.urls')),
    url(r'^om/', include('ph_om_grid_api.urls')),
    url(r'^ph_admin/', include(ph_admin_urls, namespace='ph_admin') ),
	url(r'^chaining/', include('smart_selects.urls')),
    url(r'^login/$', django.contrib.auth.views.login, kwargs={"template_name":"admin/login.html"},name='login'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^mmp/', include(mpservice_urls, namespace='ph_mobile_payment_service')),
    url(r'^$', django.views.generic.RedirectView.as_view(url='/admin/'))
]



middlewarepatterns = mpatterns('',
    middleware(r'^om/', auth.OMLoginRequiredMiddleware),
    middleware(r'^api/', django_auth_middleware.AuthenticationMiddleware),
    middleware(r'^om/', middleware_manager.RequestLoggingMiddleware),
    middleware(r'^api/', middleware_manager.RequestLoggingMiddleware),
    middleware(r'^api/', firmware_middleware.QueenContactServerMiddleware),
    middleware(r'^mmp/',middleware_manager.RequestLoggingMiddleware)
)
