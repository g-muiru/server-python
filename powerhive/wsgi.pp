"""
WSGI config for powerhive project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""
import newrelic.agent
import os
from django.conf import settings
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "powerhive.local_settings")
from django.db import connection

"""
# for testing with in memory DB
if connection.vendor != 'postgresql':
    newrelic.agent.initialize(os.path.abspath(os.path.dirname(__name__)) + '/powerhive/newrelic_local.ini')
else:
    server_env = getattr(settings, "SERVER_ENV", None)
    if server_env is not None:
        if server_env == 'prod':
            newrelic.agent.initialize( os.path.abspath(os.path.dirname(__name__)) + '/powerhive/newrelic.ini')
        elif server_env == 'local':
            newrelic.agent.initialize( os.path.abspath(os.path.dirname(__name__)) + '/powerhive/newrelic_local.ini')
        elif server_env == 'cron':
            newrelic.agent.initialize( os.path.abspath(os.path.dirname(__name__)) + '/powerhive/newrelic_cron.ini')
"""

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

