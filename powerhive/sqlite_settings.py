# coding=utf-8
""" run unit tests: manage.py test --settings=powerhive.sqlite_settings """

import django
from common_settings import *

DEBUG = True

TEMPLATES[0]['OPTIONS']['debug'] = True
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
FIRMWARE_BASE_URL = 'http://0.0.0.0:8000'

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--nocapture',
    '--nologcapture',
    # '--with-coverage',  # activate coverage report -- model coverage will not be accurate -- see manage.py
    '--with-doctest',  # activate doctest: find and run docstests
    '--verbosity=2',  # verbose output
    '--with-xunit',  # enable XUnit plugin
    '--xunit-file=xunittest.xml',  # the XUnit report file
    '--cover-xml',  # produle XML coverage info
    '--cover-xml-file=coverage.xml',  # the coverage info file
    '--exclude-dir=ph_mobile_payment_service/tests/integration',
    '--exclude-test=ph_operation.tests.tests_fulcrum_import_manager.RecordImportTestCase',
    '--exclude-test=ph_operation.tests.tests_fulcrum_import_manager.FormImportTestCase',
    '--exclude-test=ph_operation.tests.tests_fulcrum_customer_manager',
    '--exclude-test=ph_finance.tests.tests_aggregate_manager.AggregationTestCase',
    '--exclude-test=ph_firmware_grid_api.tests.tests_views.QueenMonitorTestCase',
    '--exclude-test=ph_operation.tests.tests_sms_manager.GridSMSManagerTestCase',
    '--exclude-test=ph_operation.tests.tests_sms_manager.ScratchcardPaymentIncomingSMSTestCase',
    '--exclude-test=ph_util.tests.tests_loan_utils',
    '--exclude-test=ph_script.tests.tests_first_use',
    '--exclude-test=ph_operation.tests.tests_payments',
    # You may also specify the packages to be covered here '--cover-package=blog,examples'
    # currently done in .coveragerc file in project root
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    }
}

DISABLE_SMS = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'local': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'null': {
            "class": 'logging.NullHandler',
        }
    },
    'loggers': {
        '': {
            'handlers': ['local'],
            'level': "DEBUG",
        },
    }
}

django.setup()
