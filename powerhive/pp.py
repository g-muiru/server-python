from common_settings import *
import random

DEBUG=False
PH_SERVER = 'data.powerhive.com'
PH_UI = 'honeycomb.powerhive.com'

#SERVER_ENV = 'prod'

FIRMWARE_BASE_URL = 'http://data.powerhive.com'

DISABLE_SMS = False

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.postgresql_psycopg2',
        'NAME': 'ph',
        'USER': 'poweruser',
        'PASSWORD': 'asdflighttheworld',
        'HOST': 'ph.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
        'PORT': '5432',
     },
     'replica': {
         'ENGINE':'django.db.backends.postgresql_psycopg2',
         'NAME': 'ph',
         'USER': 'poweruser',
         'PASSWORD': 'asdflighttheworld',
         'HOST': 'ph-read-replica.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
         'PORT': '5432',
    }
}

# class ReadRouter(object):
#
#     def db_for_read(self, model, **hints):
#         if model._meta.app_label == 'ph_model':
#             return 'slave_db1'
#         return 'default'
#
# DATABASE_ROUTERS = ['powerhive.prod_settings.ReadRouter']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                     '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True
        },
        'production':{
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : '/var/log/nginx/data.powerhive.com.django.log',
            'maxBytes': 1024*1024*20, # 20 MB
            'backupCount' : 100,
            'formatter': 'main_formatter'
        },
        'file':{
            'class' : 'logging.FileHandler',
            'filename' : '/var/log/nginx/data.powerhive.com.django.log',
            'formatter': 'main_formatter',
        },
        'null': {
            "class": 'logging.NullHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'WARNING',
            'propagate': True
        },
        '': {
            'handlers': ['mail_admins','production'],
            'level': 'INFO',
            'propagate': True
        },
    }
}
