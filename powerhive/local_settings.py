# coding=utf-8
""" The default settings file for testing
    run tests: manage.py test
    or run tests: manage.py test --settings=powerhive.local_settings """

import django
from common_settings import *

DEBUG = True

SERVER_ENV = 'local'

TEMPLATES[0]['OPTIONS']['debug'] = True
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
FIRMWARE_BASE_URL = 'http://0.0.0.0:8000'

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--nocapture',
    '--nologcapture',
    # '--with-coverage',  # activate coverage report -- model coverage will not be accurate -- see manage.py
    '--with-doctest',  # activate doctest: find and run docstests
    '--verbosity=2',  # verbose output
    '--with-xunit',  # enable XUnit plugin
    #'--xunit-file=xunittest.xml',  # the XUnit report file
    '--cover-xml',  # produce XML coverage info
    #'--cover-xml-file=coverage.xml',  # the coverage info file
    '--exclude-dir=ph_mobile_payment_service/tests/integration',
    # You may also specify the packages to be covered here '--cover-package=blog,examples'
    # currently done in .coveragerc file in project root
]
#"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'testdb',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
    'replica': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'testdb',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

"""
DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.postgresql_psycopg2',
        'NAME': 'ph',
        'USER': 'poweruser',
        'PASSWORD': 'asdflighttheworld',
        'HOST': 'ph.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
        'PORT': '5432',
     },
     'replica': {
         'ENGINE':'django.db.backends.postgresql_psycopg2',
         'NAME': 'ph',
         'USER': 'poweruser',
         'PASSWORD': 'asdflighttheworld',
         'HOST': 'ph-read-replica.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
         'PORT': '5432',
    }
}
"""

DISABLE_SMS = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'local': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'null': {
            "class": 'logging.NullHandler',
        }
    },
    'loggers': {
        '': {
            'handlers': ['local'],
            'level': "DEBUG",
        },
        'zeep.transports': {
            'level': 'DEBUG',
            'propagate': True,
            'handlers': ['local'],
        }
    }
}

django.setup()
