# coding=utf-8
"""
True unit tests run with no DB
run unit tests: manage.py test --nodb
"""
from common_settings import *
import django
from mock import patch

try:
    # noinspection PyPep8Naming
    from django_nose import NoseTestSuiteRunner as base_runner
except ImportError:
    # Django < 1.6 fallback
    import django.test as base_runner


class NoDatabaseMixin(object):
    """
    Test runner mixin which skips the DB setup/teardown
    when there are no subclasses of TransactionTestCase to improve the speed
    of running the tests.
    """

    def build_suite(self, *args, **kwargs):
        """ Check if any of the tests to run subclasses TransactionTestCase. """
        suite = super(NoDatabaseMixin, self).build_suite(*args, **kwargs)
        return suite

    # noinspection PyAttributeOutsideInit
    def setup_databases(self):
        """ Trap any attempted accesses to a DB """
        self._db_patch = patch('django.db.backends.utils.CursorWrapper')
        self._db_mock = self._db_patch.start()
        self._db_mock.side_effect = RuntimeError('No testing the database!')
        return None

    # noinspection PyUnusedLocal
    def teardown_databases(self, *args):
        """ Remove cursor patch. """
        self._db_patch.stop()
        return None


class FastTestRunner(NoDatabaseMixin, base_runner):
    """Actual test runner sub-class to make use of the mixin."""


DEBUG = True

TEMPLATES[0]['OPTIONS']['debug'] = True
MAIL_BACKEND = 'bandit.backends.smtp.HijackSMTPBackend'
BANDIT_EMAIL = 's.hermes@powerhive.com.com'
FIRMWARE_BASE_URL = 'http://0.0.0.0:8000'

TEST_RUNNER = 'powerhive.nodb_settings.FastTestRunner'
NOSE_ARGS = [
    '--nocapture',
    '--nologcapture',
    # '--with-coverage',  # activate coverage report
    '--with-doctest',  # activate doctest: find and run docstests
    '--verbosity=2',  # verbose output
    '--with-xunit',  # enable XUnit plugin
    '--xunit-file=xunittest.xml',  # the XUnit report file
    '--cover-xml',  # produle XML coverage info
    '--cover-xml-file=coverage.xml',  # the coverage info file

    # TODO expand this test 'ph_model.tests.unit.tests_account',
    'ph_util.tests.tests_generic_helpers_util',
    'ph_model.tests.unit.tests_tariff',
    'ph_operation.tests.unit.tests_tariff_charge',
    'ph_script.tests.unit.tests_net_suite',
    'ph_util.tests.unit.tests_accounting_util',
    'ph_util.tests.unit.tests_cron',

    '--exclude-dir=ph_mobile_payment_service',
    '--exclude-dir=ph_admin',
    '--exclude-dir=ph_common_api',
    '--exclude-dir=ph_finance',
    '--exclude-dir=ph_firmware_grid_api',
    '--exclude-dir=ph_mobile_payment_service',
    '--exclude-dir=ph_om_chart_api',
    '--exclude-dir=ph_om_grid_api',
    '--exclude-dir=ph_operation',
    '--exclude-dir=ph_script',
    '--exclude-dir=ph_util',
    # You may also specify the packages to be covered here '--cover-package=blog,examples'
    # currently done in .coveragerc file in project root
]
"""

::

    We do not need a DB, but we could use this
    Note many tests fail using Sqlite

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'tmp/db.sqlite',
            'TEST_NAME': 'tmp/test.sqlite'
        }
    }

"""

DISABLE_SMS = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'local': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'null': {
            "class": 'logging.NullHandler',
        }
    },
    'loggers': {
        '': {
            'handlers': ['local'],
            'level': "DEBUG",
        },
    }
}

django.setup()
