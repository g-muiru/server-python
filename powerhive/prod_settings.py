from common_settings import *
import random

DEBUG=False
PH_SERVER = 'data.powerhive.com'
PH_UI = 'honeycomb.powerhive.com'

SERVER_ENV = 'prod'

FIRMWARE_BASE_URL = 'http://data.powerhive.com'

DISABLE_SMS = False

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.postgresql_psycopg2',
        'NAME': 'ph',
        'USER': 'poweruser',
        'PASSWORD': 'asdflighttheworld',
        'HOST': 'ph.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
        'PORT': '5432',
     },
     'replica': {
         'ENGINE':'django.db.backends.postgresql_psycopg2',
         'NAME': 'ph',
         'USER': 'poweruser',
         'PASSWORD': 'asdflighttheworld',
         'HOST': 'ph-read-replica.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
         'PORT': '5432',
    }
}

# don't expose DRF api form POST fields in prod
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        )
}


# class ReadRouter(object):
#
#     def db_for_read(self, model, **hints):
#         if model._meta.app_label == 'ph_model':
#             return 'slave_db1'
#         return 'default'
#
# DATABASE_ROUTERS = ['powerhive.prod_settings.ReadRouter']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                     '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
        'django': {
            'format': 'django: %(message)s',
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True
        },
        'production':{
            'class' : 'logging.handlers.WatchedFileHandler',
            'filename' : '/var/log/nginx/data.powerhive.com.django.log',
            'formatter': 'main_formatter'
        },
        'file':{
            'class' : 'logging.FileHandler',
            'filename' : '/var/log/nginx/data.powerhive.com.django.log',
            'formatter': 'main_formatter',
        },
        'null': {
            "class": 'logging.NullHandler',
        },
        'logging.handlers.SysLogHandler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'local7',
            'formatter': 'django',
            'address': '/dev/log',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'WARNING',
            'propagate': True
        },
        '': {
            'handlers': ['mail_admins','production'],
            'level': 'INFO',
            'propagate': True
        },
        'loggly_logs': {
            'handlers': ['logging.handlers.SysLogHandler'],
            'propagate': True,
            'format': 'django: %(message)s',
            'level': 'DEBUG',
        }
    }
}
