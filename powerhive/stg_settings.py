# coding=utf-8
# Import all common settings
"""Staging settings"""
from common_settings import *

PH_SERVER = 'datadev.powerhive.com'
PH_UI = 'honeycombdev.powerhive.com'

FIRMWARE_BASE_URL = 'http://datadev.powerhive.com'

DISABLE_SMS = True

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'phdev',
        'USER': 'poweruser',
        'PASSWORD': 'asdflighttheworld',
        'HOST': 'ph-staging.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
        'PORT': '5432',
    },
    'replica': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'phdev',
        'USER': 'poweruser',
        'PASSWORD': 'asdflighttheworld',
        'HOST': 'ph-staging-read-replica.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
        'PORT': '5432',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'production': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/nginx/data.powerhive.com.django.log',
            'formatter': 'main_formatter'
        },
        'null': {
            "class": 'logging.NullHandler',
        }
    },
    'loggers': {
        '': {
            'handlers': ['production'],
            'level': "DEBUG",
            'propagate': True
        },
    }
}
