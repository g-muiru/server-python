# coding=utf-8
""" Cron CLI settings """

DEBUG = True
TEMPLATE_DEBUG = True
# MIDDLEWARE_CLASSES.remove('ph_common_api.middleware_manager.ExceptionMiddleware')
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
FIRMWARE_BASE_URL = 'http://0.0.0.0:8000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'testdb',
        'USER': 'postgres',
        'PASSWORD': 'power123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
    'replica': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ph',
        'USER': 'poweruser',
        'PASSWORD': 'asdflighttheworld',
        'HOST': 'ph-read-replica.cpaipntc9bpw.us-west-2.rds.amazonaws.com',
        'PORT': '5432',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'local': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
    },
    'loggers': {
        '': {
            'handlers': ['local'],
            'level': "DEBUG",
        },
        'django.db.backends': {
            'handlers': ['local'],
            'propagate': False,
            'level': 'DEBUG',
        },
    }
}
