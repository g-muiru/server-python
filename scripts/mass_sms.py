# coding=utf-8
""" usage: ./manage.py shell --local < scripts/mass_sms.py """

from ph_operation import sms_manager
import phonenumbers

import csv

DEFAULT_COUNTRY_CODE = '+254'
DEFAULT_COUNTRY = 'KE'
DEFAULT_SHORT_CODE_POWERHIVE = "22870"
SOURCEFILE = 'scripts/test_data.csv'

with open(SOURCEFILE, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in spamreader:
        try:
            phoneNumber = phonenumbers.parse(row[0], DEFAULT_COUNTRY)
        except AttributeError:
            print("bad phone number: " + row[0])
        except phonenumbers.NumberParseException:
            print("bad phone number: " + row[0])

        if phoneNumber and phonenumbers.is_possible_number(phoneNumber):
            phAccount, gateway, smsHost = sms_manager.get_sms_config(DEFAULT_SHORT_CODE_POWERHIVE)
            phoneNumber = phonenumbers.format_number(phoneNumber, phonenumbers.PhoneNumberFormat.E164)
            message = 'This is a test'
            print(phoneNumber)
            recipients = gateway.sendMessage(phoneNumber, message, from_=DEFAULT_SHORT_CODE_POWERHIVE)
            print(recipients)
