import sys
import json

fins = sys.argv[1:]

for fin in fins:
    try:
        print fin
        data = json.load(open(fin))

        outdata = []

        for item in data:
            model = item['model']
            model = model.split('.')
            model = model[0] + '.' + model[-1]
            item['model'] = model
            if model != 'auth.User':
                fields = item['fields']
                if 'created' not in fields:
                    fields['created'] = '2016-01-01T00:00:00.000Z'
                if 'updated' not in fields:
                    fields['updated'] = fields['created']
                item['fields'] = fields
            outdata.append(item)

        fout = open(fin,'w')
        fout.write(json.dumps(outdata,indent=4))
    except IOError:
        print fin
