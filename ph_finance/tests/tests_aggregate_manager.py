# coding=utf-8
"""Tests for aggregate_manager.py"""
from datetime import datetime
from datetime import timedelta
from ph_finance import aggregate_manager
from ph_finance.tests import base_test
import ph_model.models as ph_model
import delorean

from ph_model.models import Customer
from ph_model.models.transaction import update_mth_balances, MasterTransactionHistory, UsageCharge
from ph_util.accounting_util import str_obj


class BaseAggregationTestCase(base_test.BaseTestFinance):
    dga = aggregate_manager.DailyGridActivity()
    startDate = delorean.Delorean(datetime(2014, 9, 1), timezone='Africa/Nairobi').date
    endDate = startDate + timedelta(days=1)


class AggregationTestCase(BaseAggregationTestCase):
    def test_customer_with_usage(self):
        """

        """
        self.assertEqual(self.dga._get_customer_using_electricity(1, self.startDate, self.endDate), 2)

    def test_no_customer_with_usage(self):
        """

        """
        self.assertEqual(self.dga._get_customer_using_electricity(2, self.startDate, self.endDate), 0)

    def test_energy_used(self):
        """

        """
        self.assertEqual(self.dga._get_energy_used(1, self.startDate, self.endDate), 1236.00)

    def test_no_energy_used(self):
        """

        """
        self.assertEqual(self.dga._get_energy_used(2, self.startDate, self.endDate), 0)

    def test_debits(self):
        """

        """
        self.assertEqual(self.dga._get_debits(1, self.startDate, self.endDate), 11.00)

    def test_no_debits(self):
        """

        """
        self.assertEqual(self.dga._get_debits(2, self.startDate, self.endDate), 0)

    def test_average_tariff(self):
        """

        """
        self.assertEqual(self.dga._get_average_tariff(1, self.startDate, self.endDate), float(11) / 1236)

    def test_no_average_tariff(self):
        """

        """
        self.assertEqual(self.dga._get_average_tariff(2, self.startDate, self.endDate), 0)

    def test_deposits(self):
        """

        """
        self.assertEqual(self.dga._get_deposits(1, self.startDate, self.endDate), 50.00)

    def test_no_deposits(self):
        """

        """
        self.assertEqual(self.dga._get_deposits(2, self.startDate, self.endDate), 0)

    def test_wh_per_user(self):
        """

        """
        self.assertEqual(self.dga._get_wh_per_user(1, self.startDate, self.endDate), float(1236) / 2)

    def test_no_wh_per_user(self):
        """

        """
        self.assertEqual(self.dga._get_wh_per_user(2, self.startDate, self.endDate), 0)

    def test_arpu(self):
        """

        """
        self.assertEqual(self.dga._get_arpu(1, self.startDate, self.endDate), float(11) / 2)

    def test_no_arpu(self):
        """

        """
        self.assertEqual(self.dga._get_arpu(2, self.startDate, self.endDate), 0)

    def test_balance(self):
        """

        """
        update_mth_balances()

        self.assertEqual(self.dga._get_balance(1, self.endDate), 300.00)
        self.assertEqual(self.dga._get_balance(1, self.endDate + timedelta(days=5000)), 350.00)
        self.assertEqual(self.dga._get_balance(1, self.endDate - timedelta(days=5000)), 0.0)

    def test_uncollected(self):
        """

        """
        update_mth_balances()

        self.assertEqual(self.dga._get_uncollected(1, self.endDate), -112.76)
        self.assertEqual(self.dga._get_uncollected(1, self.endDate - timedelta(days=5000)), 0.0)
        self.assertEqual(self.dga._get_uncollected(1, self.endDate + timedelta(days=5000)), -112.76)

    def test_no_balance(self):
        """

        """
        self.assertEqual(self.dga._get_balance(2, self.endDate), 0)

    def test_save_aggregate(self):
        """

        """
        grid = ph_model.grid.Grid.objects.get(pk=1)
        self.dga.process_grid(grid)

        total_aggregate = ph_model.aggregate.AggregateGrid.objects.count()

        self.assertEqual(total_aggregate, 1)

    def test_due_for_running(self):
        """

        """
        grid = ph_model.grid.Grid.objects.get(pk=1)
        self.assertTrue(self.dga._due_for_running(grid))
        aggregate_grid_trackings = ph_model.aggregate.AggregateGridTracking.objects.all()
        self.assertEqual(aggregate_grid_trackings.count(), 1)

    def test_process_grids(self):
        """

        """
        self.dga.process_grids()
        grid = ph_model.grid.Grid.objects.get(pk=1)
        self.assertFalse(self.dga._due_for_running(grid))

    def test_grid_count(self):
        """

        """
        grids = ph_model.grid.Grid.objects.all()
        self.assertEqual(len(grids), 1)

    def test_get_aggregate_grid_tracking_existing(self):
        """

        """
        grid = ph_model.grid.Grid.objects.get(pk=1)
        new_agt = ph_model.aggregate.AggregateGridTracking()
        new_agt.grid = grid
        new_agt.lastRun = delorean.Delorean(datetime(2000, 1, 1), timezone="UTC").datetime
        new_agt.save()

        agt = ph_model.aggregate.AggregateGridTracking.objects.filter(grid=grid)
        self.assertEqual(agt[:1][0].lastRun, delorean.Delorean(datetime(2000, 1, 1), timezone="UTC").datetime)

    def test_adjustment(self):
        """

        """
        self.assertEqual(self.dga._get_adjustments(1, self.startDate, self.endDate), 200.00)
