from django import test
from django.core.management import call_command


class BaseTestFinance(test.TestCase):
    """Base model for view test classes"""

    @classmethod
    def setUp(self):
        call_command('loaddata', 'ph_finance/tests/fixtures/grid_config.json', verbosity=0)

