import django_cron
from ph_finance import aggregate_manager

class ProcessDailyGridActivity(django_cron.CronJobBase):
    """Run daily report."""

    RUN_EVERY_MINS = 60
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-finance-process-daily-grid-activity'

    def do(self):
        dga = aggregate_manager.DailyGridActivity()
        dga.process_grids()
