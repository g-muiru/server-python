# coding=utf-8
"""Finance related aggregation"""
import datetime
import ph_model.models as ph_model
import logging
from django.db.models import Sum
from delorean import Delorean

from ph_util.test_utils import test_mode

LOGGER = logging.getLogger(__name__)

DB = 'replica'

if test_mode():
    DB = 'default'


class DailyGridActivity(object):
    """ Aggregate daily grid activity """

    def process_grids(self):
        """Get grids"""
        grids = ph_model.grid.Grid.objects.using(DB).all()

        for grid in grids:
            try:
                while self._due_for_running(grid):
                    self.process_grid(grid)
            except Exception as e:
                LOGGER.error(e)

    @staticmethod
    def _set_last_run(aggregate_grid_tracking, last_run):
        """

        :param aggregate_grid_tracking:
        :param last_run:
        """
        aggregate_grid_tracking.lastRun = last_run
        aggregate_grid_tracking.save()

    # TODO(Frank) For empty query this will throw error, it is fine since this is private function but
    # calling from public api like process_grid it is not captured
    @staticmethod
    def _get_aggregate_grid_tracking(grid):
        """

        :param grid:
        :return:
        """
        timezone = grid.municipality.timeZone

        aggregate_grid_tracking = ph_model.aggregate.AggregateGridTracking.objects.filter(grid=grid)

        if not aggregate_grid_tracking.count():
            new_aggregate_grid_tracking = ph_model.aggregate.AggregateGridTracking()
            new_aggregate_grid_tracking.grid = grid
            new_aggregate_grid_tracking.lastRun = Delorean().last_day().shift(timezone).midnight
            new_aggregate_grid_tracking.save()
            return new_aggregate_grid_tracking
        else:
            # latest(https://docs.djangoproject.com/en/1.7/ref/models/querysets/#django.db.models.query.QuerySet.latest)
            agt = list(aggregate_grid_tracking[:1])
            return agt and agt[0]

    @staticmethod
    def _due_for_running(grid):
        """
        :param grid:
        :return:
        """
        timezone = grid.municipality.timeZone

        current_date_time = Delorean()
        current_date_time.shift(timezone)

        # Get last run
        aggregate_grid_tracking = ph_model.aggregate.AggregateGridTracking.objects.using(DB).filter(grid=grid)
        if not aggregate_grid_tracking.count():
            # Get first datetime of usage to seed lastRun
            usage = ph_model.usage.Usage.objects.using(DB).filter(customer__circuit__queen__grid=grid).order_by(
                'collectTime').first()

            # The grid does not have any usage and should not run daily report
            # until at least one usage is collected.
            if not usage:
                return False

            new_aggregate_grid_tracking = ph_model.aggregate.AggregateGridTracking()
            new_aggregate_grid_tracking.grid = grid
            new_aggregate_grid_tracking.lastRun = Delorean(usage.collectTime).shift(timezone).midnight
            new_aggregate_grid_tracking.save()
            return True

        agt = list(aggregate_grid_tracking[:1])
        return current_date_time.date - Delorean(agt[0].lastRun).shift(timezone).date > datetime.timedelta(days=1)

    def process_grid(self, grid):
        """

        :param grid:
        """
        aggr_grid_tracking = self._get_aggregate_grid_tracking(grid)
        days = datetime.timedelta(days=1)
        start_date = Delorean(aggr_grid_tracking.lastRun).shift(grid.municipality.timeZone).date + days
        end_date = start_date + days

        aggregate_grid = ph_model.aggregate.AggregateGrid()

        aggregate_grid.aggregateDate = start_date
        aggregate_grid.grid = grid
        aggregate_grid.customersUsing = self._get_customer_using_electricity(grid, start_date, end_date)
        aggregate_grid.energyUsed = self._get_energy_used(grid, start_date, end_date)
        aggregate_grid.debits = self._get_debits(grid, start_date, end_date)
        aggregate_grid.averageTariff = self._get_average_tariff(grid, start_date, end_date)
        aggregate_grid.deposits = self._get_deposits(grid, start_date, end_date)
        aggregate_grid.adjustments = self._get_adjustments(grid, start_date, end_date)
        aggregate_grid.whPerUser = self._get_wh_per_user(grid, start_date, end_date)
        aggregate_grid.arpu = self._get_arpu(grid, start_date, end_date)
        aggregate_grid.totalBalance = self._get_balance(grid, end_date)

        aggregate_grid.save()
        self._set_last_run(aggr_grid_tracking, start_date)

    @staticmethod
    def _get_customer_using_electricity(grid, start_date, end_date):

        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        customers_using_electricity = ph_model.usage.Usage.objects.using(DB).filter(customer_id__isnull=False,
                                                                                    customer__circuit__queen__grid=grid,
                                                                                    collectTime__gte=start_date,
                                                                                    collectTime__lt=end_date).values_list(
            'customer_id', flat=True)
        customers = ph_model.customer.Customer.objects.using(DB).filter(id__in=list(set(customers_using_electricity)))

        return customers.count()

    @staticmethod
    def _get_energy_used(grid, start_date, end_date):
        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        energy_used = ph_model.usage.Usage.objects.using(DB).filter(
            customer__circuit__queen__grid=grid,
            collectTime__gte=start_date,
            collectTime__lt=end_date).aggregate(Sum('intervalWh'))

        return energy_used['intervalWh__sum'] and round(energy_used['intervalWh__sum'], 5) or 0

    @staticmethod
    def _get_debits(grid, start_date, end_date):
        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        debits = ph_model.transaction.UsageCharge.objects.using(DB).filter(
            usage__customer_id__isnull=False,
            usage__customer__circuit__queen__grid=grid,
            usage__collectTime__gte=start_date,
            usage__collectTime__lt=end_date).aggregate(
            Sum('amount'))

        return debits['amount__sum'] and round(debits['amount__sum'], 2) or 0

    def _get_average_tariff(self, grid, start_date, end_date):
        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        debits = self._get_debits(grid, start_date, end_date)
        energy_used = self._get_energy_used(grid, start_date, end_date)

        return energy_used and debits / energy_used or 0

    @staticmethod
    def _get_deposits(grid, start_date, end_date):
        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        deposits = ph_model.transaction.MobilePayment.objects.using(DB).filter(
            account__accountOwner__circuit__queen__grid=grid,
            processedTime__gte=start_date,
            processedTime__lt=end_date).aggregate(
            Sum('amount'))

        return deposits['amount__sum'] and round(deposits['amount__sum'], 2) or 0

    @staticmethod
    def _get_adjustments(grid, start_date, end_date):
        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        credits_local = ph_model.transaction.CreditAdjustmentHistory.objects.using(DB).filter(
            account__accountOwner__circuit__queen__grid=grid,
            transactionType='Credit',
            processedTime__gte=start_date,
            processedTime__lt=end_date).aggregate(
            Sum('amount')
        )
        debits = ph_model.transaction.CreditAdjustmentHistory.objects.using(DB).filter(
            account__accountOwner__circuit__queen__grid=grid,
            transactionType='Debit',
            processedTime__gte=start_date,
            processedTime__lt=end_date).aggregate(
            Sum('amount')
        )

        credit = credits_local['amount__sum'] and round(credits_local['amount__sum'], 2) or 0
        debit = debits['amount__sum'] and round(debits['amount__sum'], 2) or 0
        total = credit - debit

        return total

    def _get_wh_per_user(self, grid, start_date, end_date):
        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        energy_used = self._get_energy_used(grid, start_date, end_date)
        users = self._get_customer_using_electricity(grid, start_date, end_date)

        return users and energy_used / users or 0

    def _get_arpu(self, grid, start_date, end_date):
        """

        :param grid:
        :param start_date:
        :param end_date:
        :return:
        """
        debits = self._get_debits(grid, start_date, end_date)
        users = self._get_customer_using_electricity(grid, start_date, end_date)

        return users and debits / users or 0

    @staticmethod
    def _get_balance(grid, end_date):
        """

        :param grid:
        :param end_date:
        :return:
        """
        balance = 0

        # Get customers of the grid
        customers = ph_model.customer.Customer.objects.using(DB).filter(circuit__queen__grid=grid)

        # Get latest usageCharges of each customer for the date and add to balance
        for customer in customers:
            # noinspection PyBroadException
            try:
                mth = ph_model.transaction.MasterTransactionHistory.objects.filter(sourceProcessedTime__lte=end_date) \
                    .filter(account_id=customer.account_id) \
                    .latest('sourceProcessedTime')

                customer_balance = mth.accountBalance
            except Exception:
                customer_balance = 0

            balance += customer_balance
        return round(balance, 2)

    @staticmethod
    def _get_uncollected(grid, end_date):
        """

        :param grid:
        :param end_date:
        :return:
        """
        uncol_bal = 0

        # Get customers of the grid
        customers = ph_model.customer.Customer.objects.using(DB).filter(circuit__queen__grid=grid)
        # Get latest usageCharges of each customer for the date and add to balance
        for customer in customers:
            # noinspection PyBroadException
            try:
                mth = ph_model.transaction.MasterTransactionHistory.objects.filter(sourceProcessedTime__lte=end_date) \
                    .filter(account_id=customer.account_id) \
                    .latest('sourceProcessedTime')
                uncol = mth.uncollected_bal
            except Exception:
                uncol = 0

            uncol_bal += uncol
        return round(uncol_bal, 2)
