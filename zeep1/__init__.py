from zeep1.client import CachingClient, Client  # noqa
from zeep1.transports import Transport  # noqa
from zeep1.plugins import Plugin  # noqa
from zeep1.xsd.valueobjects import AnyObject  # noqa

__version__ = '2.5.0'
