"""
    zeep1.xsd
    --------

"""
from zeep1.xsd.const import Nil, SkipValue  # noqa
from zeep1.xsd.elements import *  # noqa
from zeep1.xsd.schema import Schema  # noqa
from zeep1.xsd.types import *  # noqa
from zeep1.xsd.types.builtins import *  # noqa
from zeep1.xsd.valueobjects import *  # noqa
