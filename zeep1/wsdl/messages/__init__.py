"""
    zeep1.wsdl.messages
    ~~~~~~~~~~~~~~~~~~

    The messages are responsible for serializing and deserializing

    .. inheritance-diagram::
            zeep1.wsdl.messages.soap.DocumentMessage
            zeep1.wsdl.messages.soap.RpcMessage
            zeep1.wsdl.messages.http.UrlEncoded
            zeep1.wsdl.messages.http.UrlReplacement
            zeep1.wsdl.messages.mime.MimeContent
            zeep1.wsdl.messages.mime.MimeXML
            zeep1.wsdl.messages.mime.MimeMultipart
       :parts: 1

"""
from .http import *  # noqa
from .mime import *  # noqa
from .soap import *  # noqa
