#TODO: #

HEXING meters
014303609789
014303609813
014303609821
014303609805
014303609839
014303609797
014303609870
014303609847
014303609862
014303609854


    add 1/2 of daily fee to uncollected balance at the start of the month for pilot sights
    send sms 25th of the month with amount due (estimate)
    add uncollected to sms inquiry
    add * ACC_NUM payment logic to pay down uncollected


























2,424.07 + 50     41707  BAD DEBT  due Apr 1 2019   Move debt to collections account due Apr 1st 2019
                                                    
1,424.29 + 10     41708            due Apr 1 2019

1,278.65 + 10     41718

1,191.03 + 10     41720


4 types of balances:    

**account**       credit for electricity and loans

**clearing_bal**   zero account balance and move amount here TBD on how / when we collect this

overdraft     fixed -100 Kes when granted adds 100 Kes to the account balance and subtracts -100 Kes to this balance
              You can not get an overdraft until the overdraft balance is zero.
              All payments go to overdraft if overdraft balance is not zero.
              
**unpaid_bal**   Any missed fixed fees or loan payments (due to insufficient funds in account) go here ---- due at the end of the month

TARIFF        only goes a max 1 daily fee negative --- then uncollected amounts billed go to the uncollected account

pay down collections         SMS *45555    

ask for 100 KES overdraft    SMS #45555    --- only honor if overdraft balance is zero

when paying, if overdraft has a balance reduce it, otherwise apply to account balance






3 types of balances:    

**account**       credit for electricity and loans  

**clearing_bal**   zero account balance and move amount here TBD on how / when we collect this

**unpaid_bal**   Any missed fixed fees or loan payments (due to insufficient funds in account) go here ---- due at the end of the month

TARIFF        account only goes a max 1 fixed daily fee negative --- uncollected tariff amounts billed go to the uncollected account

LOANS         only deducted if account has sufficient funds --- uncollected loan amounts billed go to the uncollected account

pay down collections         SMS *45555    --- only honor if collections balance is zero, else uncollected balance is zero, otherwise add to account

****pay down uncollected         SMS #45555    --- only honor if uncollected balance is zero, otherwise add to account



[![Build Status](http://circleci-badges-max.herokuapp.com/img/Powerhive1/Server-Python?token=c4a0b853ce4b1754dd7b5243a184ff92bf1ba748)](https://circleci.com/gh/Powerhive1/Server-Python)
[![codecov](https://codecov.io/gh/Powerhive1/Server-Python/branch/master/graph/badge.svg?token=CkyAWWMKhs)](https://codecov.io/gh/Powerhive1/Server-Python)

<ul>
<h3>Powerhive project  written in Python on Django.</h3>
<li>
<ul><h4>packages:</h4>

   <li><strong>ph_model:</strong> persistence layer, containing database models, gui based model admins,  fixtures, migrations, tests</li>
   <li><strong>ph_grid_api:</strong> containing django-rest-framework api interfacing hardwares in the field, containing handshake json schema  definition, views, serializer, test data schema, tests.</li>
   <li><strong>ph_om_api:</strong> containing django-rest-framework api interfacing om ui, containing views, serializer, test data schema, tests.</li>
   <li><strong>webui:</strong> web application to handle Operation and maintenance webui, containing static pages, UI , rest api.</li>
   <li><strong>ph_operation:</strong> business logic operation to run payments, tariffs, background tasks.</li>
   <li><strong>ph_util:</strong>utility for all powerhive apps, independent of any apps in Server-Python.</li>
   <li>TODO comment on other packages</li>
</ul>
</li>
<li>

  <ul><b>Code style used unless builtins:</b>
     <li> ClassName method_name propertyName ExceptionName CONSTANT_VARAIBLE test_method_name</li>
     <li> New line 4 Spaces
  </ul>
</li>
<li>
  <ul><b>Remote Server debugging </b>
    <li> Make sure rpdb is installed (pip install rpdb)</li>
    <li> Set a debugger, import rpdb;rpdb.set_trace()</li>
    <li> Invoke code, terminal will show up in port 4444 of server by default ( nc 127.0.0.1 4444 )</li>
  </ul>
</li>

</ul>

#Local server

Requires a python virtual envirnoment and a postgres **testdb** DB

### Python setup

NOTE: 

    You can setup virtual environments outside the project root, in fact it is reccomended.
    If you do, you must change to the project root dir after activating the virtual envirnoment.

From the project root execute the following commands:

    virtualenv env
    source env/bin/activate
    python -m pip install --upgrade pip
    pip install --trusted-host pypi.python.org -r requirements.txt

### DB setup

Install postgres 9.6 or newer user: **postgres** password: **power123**<br/>
(Production is on 9.5.4. Previous client versions will not connect to prod.) <br/>
(Tests and migrations ran successfully on 9.4.5) <br/>
(Migrations fail on 9.3) <br/>

Create DB **testdb**

Restore DB dump from production or staging

<a name="running"></a>
### Running the server

From the project root run:

    ./manage.py migrate

Run from the project root:

    ./manage.py runserver 8001

Then goto:   http://127.0.0.1:8001/

# Testing locally

**local_settings.py** declares **django_nose.NoseTestSuiteRunner** to be the test runner.

It is configured to provide coverage and specifies the modules to test. 

Currently the sole integration test is excluded in normal testing.

**.coveragerc** & **exclude_dirs.txt** set what is included in the coverage report.

### Testing Setup

From the project root execute the following commands:

    virtualenv testenv
    source testenv/bin/activate
    pip install -r testreq.txt
    
### Running all tests using no DB
NOTE: Currently there are a limited number of tests, see **nodb_settings.py**

Speed and true unit testing is the reason for testing with no DB.

    ./manage.py test --nodb
    
### Running all tests using in memory sqlite DB
NOTE: Some tests do not run correctly and they are excluded, see **sqlite_settings.py**

Speed is the primary reason for testing with a in memory DB.

    ./manage.py test --sqlite
    
### Running all tests using local PG DB

    ./manage.py test -p
    
### Coverage
Prior to running any tests, the coverage data and reports are ereased. 

An html report is automatically generated in the project root **htmlcov** dir:

If you want to generate your own report, run either command

    coverage report
    coverage html


# Local Load testing

Make sure your server is running ( [Running the server](#running))

from the project root **locust** dir   run:

    locust --host=http://127.0.0.1:8001

then goto: [http://127.0.0.1:8089/](http://127.0.0.1:8089/)

# Documentation

This is a work in progress, you can see a version of the documentation at: [Powerhive Server Docs](http://powerhive.webfactional.com/server/docs/)

For this discussion we'll assume the project root is **Server-Python**

### Generating the documentation locally

1. You must **pip** install the **source/requirements.txt** file.

2. You must modify the env\Lib\site-packages\django\db\models\base.py file.

Django 1.10.5 requires an **app_label**, so we add one if necessary by adding one line of code at line 109 and commenting out lines 110 - 115

         108   if app_config is None:
         109       app_label = str(module).split('.')[0] #smh allows sphinx to build
         110       #if not abstract:
         111           #raise RuntimeError(
         112           #    "Model class %s.%s doesn't declare an explicit "
         113           #    "app_label and isn't in an application in "
         114           #    "INSTALLED_APPS." % (module, name)
         115           #)
         116
         117   else:
         118       app_label = app_config.label
        
3. Make the html, run from the project root:
  
        make html
         
4. A **sphinx** dir will be created at the same level as the project root.

5. Load the **index.html** file in the **../sphinx/html** dir into your browser, and have a look.         
        

<ul>
<h3>Powerhive project  written in Python on Django</h3>

<ul><h4>packages:</h4>

   <li><strong>ph_model:</strong> persistence layer, containing database models, gui based model admins,  fixtures, migrations, tests</li>
   <li><strong>ph_grid_api:</strong> containing django-rest-framework api interfacing hardwares in the field, containing handshake json schema  definition, views, serializer, test data schema, tests.</li>
   <li><strong>ph_om_api:</strong> containing django-rest-framework api interfacing om ui, containing views, serializer, test data schema, tests.</li>
   <li><strong>webui:</strong> web application to handle Operation and maintenance webui, containing static pages, UI , rest api.</li>
   <li><strong>ph_operation:</strong> business logic operation to run payments, tariffs, background tasks.</li>
   <li><strong>ph_util:</strong>utility for all powerhive apps, independent of any apps in Server-Python.</li>
   <li>TODO comment on other packages</li>
</ul>
</li>
<li>

  <ul><b>Code style used unless builtins:</b>
     <li> ClassName method_name propertyName ExceptionName CONSTANT_VARAIBLE test_method_name</li>
     <li> New line 4 Spaces
  </ul>
</li>
<li>
  <ul><b>Remote Server debugging </b>
    <li> Make sure rpdb is installed (pip install rpdb)</li>
    <li> Set a debugger, import rpdb;rpdb.set_trace()</li>
    <li> Invoke code, terminal will show up in port 4444 of server by default ( nc 127.0.0.1 4444 )</li>
  </ul>
</li>

</ul>
