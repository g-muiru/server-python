#TODO: lot of warnings / errors in here --- surprised it works

from spyne.model.primitive import Unicode, Integer, AnyDict, Mandatory
from spyne.model.complex import Iterable, ComplexModel
from spyne.service import ServiceBase
from django.db.models import Q
from spyne.protocol.soap import Soap11
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import Unicode, Integer, AnyDict
from spyne.model.complex import Iterable, ComplexModel, Array
from spyne.model import Unicode
from spyne.service import ServiceBase
from spyne.decorator import rpc
import ph_model.models as ph_model
from spyne import error
from django.conf import settings
import random
import hashlib

from ph_operation import payment_manager

default_ns = 'mmp.powerhive'
mpesa_ns = 'http://cps.huawei.com/cpsinterface/c2bpayment'

def get_api_key():
    return hashlib.sha224( str(random.getrandbits(256)) ).hexdigest()

def _get_kyc_info(kycInfo):
    """get know your customer information. from list of dict value of @param keyInfo.
    Arg values:
       fName = '[Personal Details][First Name]'
       lName = '[Personal Details][Last Name]'
       mName = '[Personal Details][Middle Name]'

    Returns:
       list of (fName, mName, lName)
    """
    try:
        result = {}
        for k in kycInfo:
           if 'First' in k.KYCName:
                result['firstName'] = k.KYCValue
           elif 'Middle' in k.KYCName:
               result['middleName'] = k.KYCValue
           elif 'Last' in k.KYCName:
               result['lastName'] = k.KYCValue
        return result
    except Exception:
        return {}



class MandatoryString(Unicode):
    class Attributes(Unicode.Attributes):
        nullable=False
        min_occurs=1

class KYCInfo(ComplexModel):
    KYCName = Unicode(max_length=255)
    KYCValue = Unicode(max_length=128)
    __namespace__ = mpesa_ns

    class Attributes(ComplexModel.Attributes):
        nullable=True
        min_occurs=0
        max_occurs='unbounded'


class C2BPaymentValidationResult(ComplexModel):
    __namespace__ = mpesa_ns
    ResultCode = Unicode(max_length=20, sub_ns = default_ns)
    ResultDesc = Unicode(max_length=1024, sub_ns = default_ns)
    ThirdPartyTransID = Unicode(sub_ns = default_ns)

class C2BPaymentConfirmationRequestResult(Unicode):
    __namespace__ = mpesa_ns
   # __type_name__ = 'C2BPaymentConfirmationRequestResult'

class C2BPaymentConfirmationRequestDetailResult(ComplexModel):
    ResultCode = Unicode(max_length=20)
    PowerHiveTransactionTime = Unicode(max_length=1024)
    PowerHiveTransactionID = Unicode
    CustomerMsisdn = Unicode
    MmuMsisdn = Unicode
    TransactionAmount = Unicode


class C2BRegisterURLResult(ComplexModel):
    ValidationUrl = Unicode(max_length=1024)
    ConfirmationUrl = Unicode(max_length=1024)


class C2BPaymentValidationRequestBody(ComplexModel):

    TransType = MandatoryString
    TransID = MandatoryString
    TransTime = MandatoryString
    TransAmount = MandatoryString
    BusinessShortCode = MandatoryString
    BillRefNumber = Unicode
    InvoiceNumber = Unicode
    MSISDN = MandatoryString
    KYCInfo = KYCInfo
    __namespace__ = mpesa_ns 
    __type_name__ = 'C2BPaymentValidationRequest'

class C2BPaymentConfirmationRequestBody(C2BPaymentValidationRequestBody):
    OrgAccountBalance = MandatoryString
    ThirdPartyTransID = Unicode
    __type_name__ = 'C2BPaymentConfirmationRequest'


class MPESAMMPService(ServiceBase):
    """Base MMP push type service provided to mobile money payment processors."""

    @rpc(C2BPaymentValidationRequestBody,_returns=C2BPaymentValidationResult,_body_style='bare',_soap_body_style='rpc',_out_message_name='C2BPaymentValidationResult')
    def C2BPaymentValidationRequest(ctx,request):
        KYCInfo = request.KYCInfo
        TransID = request.TransID
        TransType = request.TransType
        TransTime = request.TransTime
        TransAmount = request.TransAmount
        BusinessShortCode = request.BusinessShortCode
        BillRefNumber = request.BillRefNumber
        InvoiceNumber = request.InvoiceNumber
        MSISDN = request.MSISDN
        kycInfo=_get_kyc_info(KYCInfo)
        mmpOrginization = ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MPESA.value
        c2bpvr = payment_manager.MpesaPaymentManager.validate_payment_request(transId=TransID, transType=TransType,
           transTime=TransTime, transAmount=TransAmount, businessShortCode=BusinessShortCode, billRefNumber=BillRefNumber,
           invoiceNumber=InvoiceNumber, msisdn=MSISDN, serviceProvider=mmpOrginization, kycInfo=kycInfo)
        if not isinstance(c2bpvr,ph_model.mobile_payment_provider.C2BPaymentValidationRequest):
            c2bpvr = map(str,c2bpvr)
            return C2BPaymentValidationResult(ResultCode=c2bpvr[0],ResultDesc=c2bpvr[1],ThirdPartyTransID='-1')
        return C2BPaymentValidationResult(ResultCode=str(c2bpvr.responseCode), ResultDesc=str(c2bpvr.responseDesc), ThirdPartyTransID=str(c2bpvr.id))


    @rpc(C2BPaymentConfirmationRequestBody,_returns=Unicode,_body_style='bare',_soap_body_style='rpc',_out_message_name = 'C2BPaymentConfirmationRequestResult')
    def C2BPaymentConfirmationRequest(ctx,request):
        TransID = request.TransID
        ThirdPartyTransID = request.ThirdPartyTransID
        mmpOrganization = ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MPESA.value
        response = payment_manager.MpesaPaymentManager.confirm_payment_request(transId=TransID, c2bPaymentValidationId=ThirdPartyTransID, serviceProvider=mmpOrganization)
        return str(response) #C2BPaymentConfirmationRequestResult(response)

############### none of this is tested below here

def authenticate(mmpOrganization, apiKey):
     try:
         serviceProvider = ph_model.mobile_payment_provider.NetworkServiceProvider.objects.get_or_none(serviceProviderId=mmpOrganization, apiKey=apiKey)
         if not serviceProvider or not (mmpOrganization or apiKey) or apiKey != serviceProvider.apiKey  or mmpOrganization and \
            mmpOrganization.lower() != serviceProvider.serviceProviderId.lower():
               raise error.RequestNotAllowed('Access denied!')
         return serviceProvider
     except Exception as e:
         raise error.RequestNotAllowed('Access denied!')

class GenericMMPService(ServiceBase):
    """Base MMP push type service provided to mobile money payment processors."""


    @rpc(MandatoryString, MandatoryString, MandatoryString, MandatoryString, MandatoryString, MandatoryString, MandatoryString,
         MandatoryString, MandatoryString, Unicode, Iterable(KYCInfo),
         _returns=C2BPaymentValidationResult)
    def C2BValidatePaymentRequest(ctx, ApiKey, MmpOrganization, MmpCountry , TransactionType,
            TransactionID, TransactionTime, TransactionAmount, PowerhiveAccount, MmuMsisdn, CustomerMsisdn,
            KYCInfos):
        authenticate(mmpOrganization=MmpOrganization, apiKey=ApiKey)
        kycInfo = _get_kyc_info(KYCInfos)
        if MmuMsisdn and not MmuMsisdn.startswith('+'):
            MmuMsisdn = '+' + MmuMsisdn
        country = ph_model.country.Country.objects.get(Q(countryIsoCode=MmpCountry)|Q(countryCode=MmpCountry))
        mmpOrginization = ph_model.mobile_payment_provider.MobilePaymentServiceProvider.value_of_case_insensitive(MmpOrganization)
        c2bpvr = payment_manager.MpesaPaymentManager.validate_payment_request(transType=TransactionType,
           transId=TransactionID, transTime=TransactionTime, transAmount=TransactionAmount,
           businessShortCode=PowerhiveAccount, msisdn=MmuMsisdn, kycInfo=kycInfo, country=country,
           serviceProvider=mmpOrginization, customerPhone=CustomerMsisdn)
        return C2BPaymentValidationResult(ResultCode=c2bpvr.responseCode, ResultDesc=c2bpvr.responseDesc, ThirdPartyTransID=c2bpvr.id)


    @rpc(MandatoryString, MandatoryString, MandatoryString, MandatoryString, MandatoryString, MandatoryString, MandatoryString,
         _returns=C2BPaymentConfirmationRequestDetailResult)
    def C2BConfirmPaymentRequest(ctx, ApiKey, MmpOrganization, TransactionID, ThirdPartyTransID, TransactionTime, TransactionAmount, PowerhiveAccount):
        authenticate(mmpOrganization=MmpOrganization, apiKey=ApiKey)
        c2bpvr = payment_manager.MpesaPaymentManager.confirm_payment_request(transId=TransactionID, c2bPaymentValidationId=ThirdPartyTransID)
        if not c2bpvr:
           raise error.ValidationError('Haven\' recieved previous validation request')
        PowerHiveTransactionTime = c2bpvr.created.strftime('%Y%m%d%H%M%S')
        return C2BPaymentConfirmationRequestDetailResult(ResultCode=c2bpvr.responseCode, PowerHiveTransactionTime=PowerHiveTransactionTime,
           PowerHiveTransactionID=c2bpvr.id, CustomerMsisdn=str(c2bpvr.payerPhoneNumber), MmuMsisdn=str(c2bpvr.payerPhoneNumber),
           TransactionAmount=c2bpvr.transAmount)

    @rpc(MandatoryString, MandatoryString,  _returns=C2BRegisterURLResult)
    def C2BInvocationURL(ctx, ApiKey, MmpOrganization):
        serviceProvider = authenticate(mmpOrganization=MmpOrganization, apiKey=ApiKey)
        return C2BRegisterURLResult(ValidationUrl='https://' + settings.PH_SERVER + serviceProvider.validationUrl,
                                    ConfirmationUrl='https://' + settings.PH_SERVER + serviceProvider.verificationUrl)


