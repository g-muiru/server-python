from django.conf.urls import  url
from spyne.server.django import DjangoView
from spyne.protocol.soap import Soap11
from spyne.application import Application

from views import MPESAMMPService, GenericMMPService

import logging

def fix_nsmap_mpesa(application):
    conversion_dict = {
        'tns': 'c2b',
        'soap11env': 'soapenv',
    }
    nsmap = application.interface.nsmap
    prefmap = application.interface.prefmap
    #logging.debug('\n\n'+str(prefmap[application.interface.app.tns])+'\n\n')  
    #logging.debug(str(nsmap))
    #logging.debug('\n\n'+str(prefmap))
    for k, v in conversion_dict.iteritems():
        prefmap[nsmap[k]] = v
        nsmap[v] = nsmap[k]
        del nsmap[k]
    nsmap[None] = 'mmp.powerhive'
    nsmap['None'] = 'mmp.powerhive'
    prefmap['mmp.powerhive']=None
    #logging.debug(str(nsmap))
    #logging.debug('\n\n'+str(prefmap))
    #logging.debug('\n\n'+str(prefmap[application.interface.app.tns])+'\n\n')  
    application.interface.nsmap = nsmap
    application.interface.prefmap = prefmap

mpesa_app = Application(
    services=[MPESAMMPService], tns='http://cps.huawei.com/cpsinterface/c2bpayment',
    in_protocol=Soap11(validator='soft'), out_protocol=Soap11()
)

fix_nsmap_mpesa(mpesa_app)

urlpatterns = [ 
    
    url(r'^mpesa/$', DjangoView.as_view(application=mpesa_app, cache_wsdl = False)),
    #url(r'^mpesa/$', DjangoView.as_view(
    #    services=[MPESAMMPService], tns='mmp.powerhive',
    #    in_protocol=Soap11(validator='soft'), out_protocol=Soap11(),
    #    cache_wsdl=False)),
    #url(r'^payamaya/$', DjangoView.as_view(
    #    services=[GenericMMPService], tns='mmp.powerhive',
    #    in_protocol=Soap11(validator='lxml'), out_protocol=Soap11(),
    #    cache_wsdl=False))
]
