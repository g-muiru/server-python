from ph_mobile_payment_service.tests import base_test
from ph_mobile_payment_service import views
from django.core.management import call_command
from spyne import error



class MPESAServiceTestCase(base_test.BaseTestMPS):

    def test_C2BPaymentValidationRequest(self):
        #TODO(estifanos) Mock ws push and test. Not sure what to test here
        pass

    def test_get_kyc_mpesa_info(self):
        data = [views.KYCInfo(KYCValue=u'Estifanos', KYCName=u'[First Name]'),
                views.KYCInfo(KYCValue=u'Gebru', KYCName=u'[Middle Name]'),
                views.KYCInfo(KYCValue=u'Gebrehiwot', KYCName=u'[Last Name]')]
        data = views._get_kyc_info(data)
        expected = {'firstName': 'Estifanos', 'middleName': 'Gebru',  'lastName': 'Gebrehiwot'}
        self.assertEquals(expected, data)

    def test_get_kyc_mvola_info(self):
        data = iter([views.KYCInfo(KYCValue=u'Estifanos', KYCName=u'[First Name]'),
                views.KYCInfo(KYCValue=u'Gebru', KYCName=u'[Middle Name]'),
                views.KYCInfo(KYCValue=u'Gebrehiwot', KYCName=u'[Last Name]')])
        data = views._get_kyc_info(data)
        expected = {'firstName': 'Estifanos', 'middleName': 'Gebru',  'lastName': 'Gebrehiwot'}
        self.assertEquals(expected, data)



class APIKeyAuthetnicateTestCase(base_test.BaseTestMPS):

    @classmethod
    def setUp(self):
        call_command('loaddata', 'ph_mobile_payment_service/tests/fixtures/mobile_payment_config.json', verbosity=0)

    def test_authenticate_success(self):
        self.assertTrue(views.authenticate('mvola', 'test_api_key'))

    def test_authenticate_failure_invalid_api_key(self):
        self.assertRaises(error.RequestNotAllowed, views.authenticate, 'mvola', 'test_api_key_invalid')

    def test_authenticate_failure_invalid_service_provider_id(self):
        self.assertRaises(error.RequestNotAllowed, views.authenticate, 'mvolaa', 'test_api_key')

    def test_authenticate_failure_invalid_api_key_and_service_provider_id(self):
        self.assertRaises(error.RequestNotAllowed, views.authenticate, 'mvolaa', 'test_api_key_invalid')



