from suds.client import Client
from suds.plugin import MessagePlugin
import sys
import traceback

i='1'
if len(sys.argv)>1:
    i = sys.argv[1]

# TODO this test needs work !!!!!
# Right now it is expected to fail

#url = 'http://data.powerhive.com/mmp/mpesa?wsdl'
url = 'http://localhost:8000/mmp/mpesa?wsdl'
#imp = doctor.Import('http://www.w3.org/2001/XMLSchema',location='http://www.w3.org/2001/XMLSchema')
#imp.filter.add('http://cps.huawei.com/cpsinterface/c2bpayment')
#imp.filter.add('mmp.powerhive')
#c = Client(url,doctor=doctor.ImportDoctor(imp))

class LoggingWebServicePlugin(MessagePlugin):

    def __init__(self):
        self.last_sent_message = None
        self.last_received_reply = None

    def sending(self, context):
        if context.envelope:
            self.last_sent_message = context.envelope

    def parsed(self, context):
        if context.reply:
            self.last_received_reply = context.reply

    def last_sent(self):
        return self.last_sent_message

    def last_received(self):
        return self.last_received_reply

logging_plugin = LoggingWebServicePlugin()
c = Client(url, cache = None, plugins = [logging_plugin])
kyc = c.factory.create('KYCInfo')

try:
    ##message = '''<?xml version='1.0' encoding='UTF-8'?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><c2b:C2BPaymentValidationRequest xmlns:c2b="http://cps.huawei.com/cpsinterface/c2bpayment"><TransType>Pay Bill</TransType><TransID>KCV110I8DL</TransID><TransTime>20160331110110</TransTime><TransAmount>70.00</TransAmount><BusinessShortCode>802840</BusinessShortCode><BillRefNumber>0732011556</BillRefNumber><MSISDN>254728171730</MSISDN><KYCInfo><KYCName>[Personal Details][First Name]</KYCName><KYCValue>Tun</KYCValue></KYCInfo><KYCInfo><KYCName>[Personal Details][Last Name]</KYCName><KYCValue>Opiata</KYCValue></KYCInfo></c2b:C2BPaymentValidationRequest></soapenv:Body></soapenv:Envelope>'''
#    message = '''<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><c2b:C2BPaymentValidationRequest xmlns:c2b="http://cps.huawei.com/cpsinterface/c2bpayment"><TransType>Pay Bill</TransType><TransID>KCV110I8DL</TransID><TransTime>20160331110110</TransTime><TransAmount>70.00</TransAmount><BusinessShortCode>802840</BusinessShortCode><BillRefNumber>0732011556</BillRefNumber><MSISDN>254728171730</MSISDN><KYCInfo><KYCName>[Personal Details][First Name]</KYCName><KYCValue>Tun</KYCValue></KYCInfo><KYCInfo><KYCName>[Personal Details][Last Name]</KYCName><KYCValue>Opiata</KYCValue></KYCInfo></c2b:C2BPaymentValidationRequest></soapenv:Body></soapenv:Envelope>'''
#    message = '''<?xml version="1.0" encoding="utf-8" ?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><ns1:C2BPaymentValidationRequest xmlns:ns1="http://cps.huawei.com/cpsinterface/c2bpayment"><TransType>PayBill</TransType><TransID>1234560000007031</TransID><TransTime>20140227082020</TransTime><TransAmount>123.00</TransAmount><BusinessShortCode>802840</BusinessShortCode><BillRefNumber>0732011556</BillRefNumber><InvoiceNumber>X334343</InvoiceNumber><MSISDN>40722703614</MSISDN><KYCInfo><KYCName>[Personal Details][First Name]</KYCName><KYCValue>Hoiyor</KYCValue></KYCInfo><KYCInfo><KYCName>[Personal Details][Middle Name]</KYCName><KYCValue>G</KYCValue></KYCInfo><KYCInfo><KYCName>[Personal Details][Last Name]</KYCName><KYCValue>Chen</KYCValue></KYCInfo></ns1:C2BPaymentValidationRequest></soapenv:Body></soapenv:Envelope>'''
    message = '''<?xml version="1.0" encoding="utf-8" ?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><ns1:C2BPaymentValidationRequest xmlns:ns1="http://cps.huawei.com/cpsinterface/c2bpayment"><TransType>Pay Bill</TransType><TransID>KK415UPU8X</TransID><TransTime>20161104132420</TransTime><TransAmount>50.00</TransAmount><BusinessShortCode>829250</BusinessShortCode><BillRefNumber>0714009968</BillRefNumber><MSISDN>254790991050</MSISDN><KYCInfo><KYCName>[Personal Details][First Name]</KYCName><KYCValue>MAUREEN</KYCValue></KYCInfo><KYCInfo><KYCName>[Personal Details][Last Name]</KYCName><KYCValue>ONYANGO</KYCValue></KYCInfo></ns1:C2BPaymentValidationRequest></soapenv:Body></soapenv:Envelope>'''
    print c.service.C2BPaymentValidationRequest(__inject={'msg':message})
    print logging_plugin.last_received()
    print logging_plugin.last_sent()
    #c2bpvr = c.service.C2BPaymentValidationRequest(TransType = 'PayBill', TransID = 'KCV110I8DL', TransTime = '20160331110110', TransAmount = '70.0', BusinessShortCode = '802840', BillRefNumber = '0732011556', MSISDN = '254728171730', KYCInfo = kyc)
    #print c2bpvr.ResultCode, c2bpvr.ResultDesc
except:
    traceback.print_exc()
    print logging_plugin.last_received()
    print logging_plugin.last_sent()
#sys.exit()

try:
    c2bpvr = c.service.C2BPaymentValidationRequest(TransType = 'PayBill', TransID = i, TransTime = '20140227082020', TransAmount = '123.0', BusinessShortCode = '1234', BillRefNumber = '', InvoiceNumber = '', MSISDN = '+254732011556', KYCInfo = kyc)
    print c2bpvr.ResultCode, c2bpvr.ResultDesc
except Exception:
    traceback.print_exc()
    print 'exception'
    print logging_plugin.last_sent()


c2bpvr = c.service.C2BPaymentValidationRequest(TransType = 'PayBill', TransID = i+'1', TransTime = '20140227082020', TransAmount = '123.0', BusinessShortCode = '1234', BillRefNumber = '', InvoiceNumber = '', MSISDN = '3290', KYCInfo = kyc)

print c2bpvr.ResultCode, c2bpvr.ResultDesc

c2bpvr = c.service.C2BPaymentValidationRequest(TransType = 'PayBill', TransID = i+'2', TransTime = '20140227082020', TransAmount = '123.0', BusinessShortCode = '1234', BillRefNumber = '+254732011556', InvoiceNumber = '', MSISDN = '254734883291', KYCInfo = kyc)

print c2bpvr.ResultCode, c2bpvr.ResultDesc

#try:
#    message = '''<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:ns0="http://cps.huawei.com/cpsinterface/c2bpayment" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><ns1:Body><ns0:C2BPaymentConfirmationRequest><ns0:TransType>PayBill</ns0:TransType><ns0:TransID>12</ns0:TransID><ns0:TransTime>20140227082020</ns0:TransTime><ns0:TransAmount>123.0</ns0:TransAmount><ns0:BusinessShortCode>1234</ns0:BusinessShortCode><ns0:BillRefNumber></ns0:BillRefNumber><ns0:InvoiceNumber></ns0:InvoiceNumber><ns0:OrgAccountBalance>123.0</ns0:OrgAccountBalance><ns0:ThirdPartyTransID></ns0:ThirdPartyTransID><ns0:MSISDN>254734883290</ns0:MSISDN></ns0:C2BPaymentConfirmationRequest></ns1:Body></SOAP-ENV:Envelope>'''
#    print c.service.C2BPaymentConfirmationRequest(__inject={'msg':message})
#except:
#    traceback.print_exc()
#    print logging_plugin.last_received()
#    print logging_plugin.last_sent()
c2bpcr = c.service.C2BPaymentConfirmationRequest(TransType = 'PayBill', TransID = i, TransTime = '20140227082020', TransAmount = '123.0', BusinessShortCode = '1234', BillRefNumber = '', InvoiceNumber = '', OrgAccountBalance='123.0', ThirdPartyTransID = '', MSISDN = '+254732011556', KYCInfo = kyc)

print c2bpcr

c2bpcr = c.service.C2BPaymentConfirmationRequest(TransType = 'PayBill', TransID = i+'2', TransTime = '20140227082020', TransAmount = '123.0', BusinessShortCode = '1234', BillRefNumber = '', InvoiceNumber = '', OrgAccountBalance='123.0', ThirdPartyTransID = '', MSISDN = '254734883290', KYCInfo = kyc)

print c2bpcr

c2bpcr = c.service.C2BPaymentConfirmationRequest(TransType = 'PayBill', TransID = i+'100', TransTime = '20140227082020', TransAmount = '123.0', BusinessShortCode = '1234', BillRefNumber = '', InvoiceNumber = '', OrgAccountBalance='123.0', ThirdPartyTransID = '', MSISDN = '254734883290', KYCInfo = kyc)

print c2bpcr


