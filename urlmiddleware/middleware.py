from django.core.exceptions import ImproperlyConfigured
from memoize import memoize
from urlmiddleware.base import MiddlewareResolver404
from urlmiddleware.urlresolvers import resolve


@memoize()
def matched_middleware(path):
    return resolve(path)


class URLMiddleware(object):
    """
    To install urlmiddleware, one global middleware class needs to be
    added so it can then act as an entry point and match other middleware
    classes.
    """

    @staticmethod
    def get_matched_middleware(path, middleware_method=None):

        middleware_instances = []

        try:
            middleware_matches = matched_middleware(path)
        except MiddlewareResolver404:
            return []

        for middleware_class in middleware_matches:

            if not callable(middleware_class):
                raise ImproperlyConfigured(
                    "%s is expected to be a callable that accepts no arguments." % middleware_class)

            mw_instance = middleware_class()
            if middleware_method and not hasattr(mw_instance, middleware_method):
                continue
            middleware_instances.append(mw_instance)

        return middleware_instances

    def process_request(self, request):
        matched_middleware_pr = self.get_matched_middleware(request.path, 'process_request')
        for middleware in matched_middleware_pr:
            response = middleware.process_request(request)
            if response:
                return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        matched_middleware_pv = self.get_matched_middleware(request.path, 'process_view')
        for middleware in matched_middleware_pv:
            response = middleware.process_view(request, view_func, view_args,
                                               view_kwargs)
            if response:
                return response

    def process_template_response(self, request, response):
        matched_middleware_ptr = self.get_matched_middleware(request.path, 'process_template_response')
        for middleware in matched_middleware_ptr:
            response = middleware.process_template_response(request, response)
        return response

    def process_response(self, request, response):
        matched_middleware_pr = self.get_matched_middleware(request.path, 'process_response')
        for middleware in matched_middleware_pr:
            response = middleware.process_response(request, response)
        return response

    def process_exception(self, request, exception):
        matched_middleware_pe = self.get_matched_middleware(request.path, 'process_exception')
        for middleware in matched_middleware_pe:
            response = middleware.process_exception(request, exception)
            if response:
                return response
