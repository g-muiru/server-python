# coding=utf-8
""" DB utilities """

import logging
import string
import traceback
from datetime import timedelta, datetime
from decimal import Decimal

import pytz
from dateutil.relativedelta import relativedelta
from delorean import utcnow, Delorean
from django.db import connection, DatabaseError

from django.db.models import Sum
from ph_model.models.customer import Customer
from ph_model.models.transaction import UsageCharge
from ph_model.models.transaction import save_usage_charge_to_master_transaction_accounting
from ph_model.models.usage import Usage
from ph_operation import tariff_manager
from ph_util.date_util import str_to_dt
from ph_util.test_utils import test_mode

logger = logging.getLogger(__name__)


def refresh_materialized_views(sql_array, max_date=None, with_data=False, test=False):
    """
    Executes the sql strings handed in
    :param test:        Some debugging
    :param sql_array:   array of strings -- mv name
    :param max_date:    array of strings -- name of date field and checks if we ned to refresh the data
    :param with_data:   True means add WITH DATA clause
    :return: 
    """
    c = connection.cursor()
    cnt = 0
    # noinspection PyBroadException
    try:
        msg = 'start:' + str(utcnow()) + '\n'
        for idx, sql in enumerate(sql_array):
            secs = utcnow().epoch
            try:
                update = True
                mv = string.replace(sql, 'REFRESH MATERIALIZED VIEW ', '')
                if with_data:
                    if max_date is not None:
                        select = 'select max(' + max_date[0][idx][0] + ') from public.'
                        c.execute(select + mv)
                        dt = c.fetchone()

                        if dt is not None:
                            if isinstance(dt[0], datetime):
                                d = (utcnow().date - dt[0].date()).days
                            else:
                                d = (utcnow().date - dt[0]).days
                            update = max_date[0][idx][1] < d
                            msg += '%s:   update: %s days: %s %s\n' % (mv, str(update), str(max_date[0][idx][1]), str(d))
                    sql += ' WITH DATA'
                if update:
                    c.execute(sql)
                    cnt += 1
                msg += 'sql: %s  \nupdate: %s  \nelapsed: %s\n' % (sql, str(update), str(utcnow().epoch - secs))
                if test:
                    print(msg)
                logger.info(msg)
            except Exception as e:
                traceback.print_exc()
                logger.error(str(e))
                continue
    finally:
        c.close()
        return cnt, len(sql_array)


def process_materialized_views(view_list=None, max_date=None, data=True):
    """
    Refresh materialized viws with or without data
    NOTE: You can only refresh with data if the materialized view already has data
    NOTE: active_accounts view is updated every 6 hours in the first use cron.

    'REFRESH MATERIALIZED VIEW mth_chk_jan_2017'                            takes about 30 secs
    'REFRESH MATERIALIZED VIEW active_accounts'                             takes about 20 secs
    'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days'    takes about 60 secs
    'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days'                   takes about 7.5 mins
    'REFRESH MATERIALIZED VIEW circuit_monitors_queen',                     takes about 4 minutes

    :param view_list:   List of materialized view names to refresh
    :param max_date:    If not None we will only run the sql if the data is stale
    :param data:        True refresh with data
    :return: 
    """

    if view_list:
        logger.info('view_list: %s, max_date: %s, data: %s' % (str(view_list), str(max_date), str(data)))
        return refresh_materialized_views(view_list, with_data=data)

    md = [['dt', 0], ['dt', -1], ['dt', 0], ['date', 0], ['dt', 0]],
    logger.info('view_list: %s, max_date: %s, data: %s' % (str(view_list), str(md), str(data)))
    return refresh_materialized_views(
        ['REFRESH MATERIALIZED VIEW mth_chk_jan_2017',
         'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days',
         'REFRESH MATERIALIZED VIEW circuit_monitors_mv',
         'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days',
         'REFRESH MATERIALIZED VIEW circuit_monitors_queen'
         ],
        md,
        data)


def load_fixed_charges_to_master_transaction(usages=None, test=False):
    """
    Processes a list of usage IDs, ensuring only the daily fixed fee is charged
    Creates records in the masterTransactionHistory table
    NOTE: only called from all the assess_daily_fee functions so no transaction annotation needed

    :param usages:  List of usage IDs
    :param test:    True for debug
    :return:
    """
    usage_charges = UsageCharge.objects.filter(usage_id__in=usages).all()
    if not usage_charges:
        return
    usage_charges_ids = []
    for usageCharge in usage_charges:
        save_usage_charge_to_master_transaction_accounting(usageCharge, test)
        usage_charges_ids.append(usageCharge.id)

    UsageCharge.objects.filter(id__in=usage_charges_ids).update(syncToMasterTransaction=True)


# TODO current grid blackout based daily fee
def assess_daily_fee_grid(divisor, test=None, dt=utcnow().date):
    """
    Check if customers paid the daily fee on operational grids.
    If they did not pay, charge them if the grid was not completely down the previous day.
    This fixes the problem of no daily fee charged if grid is down at midnight or user does not have credit.
    This drives account balances negative.

    Typically this would be called with todays date.
    :param divisor      divide fixed amount by this
    :param test:        If > 0 then deduct fixed fees, if > 1 do not deduct fixed fees
    :param dt:          Date to check
    :return:
    """

    # TODO: we can get rid of these checks once we decide charging hourly is the way to go
    if isinstance(dt, datetime):
        dt = dt.date()
    dt2 = dt - timedelta(days=1)
    # TODO remove Used to be the collect time   date_time = datetime(year=dt.year, month=dt.month, day=dt.day, hour=9, tzinfo=pytz.UTC)

    d1 = "'" + str(dt.year) + '-' + str(dt.month) + '-' + str(dt.day) + "'"
    d2 = "'" + str(dt2.year) + '-' + str(dt2.month) + '-' + str(dt2.day) + "'"

    q = """ SELECT * FROM ph_model_customer WHERE account_id IN (
SELECT acc_id FROM 
(SELECT date("collectTime") dt, 
g.name grid, round(count(*) / 1920::NUMERIC,2) AS qcnt, sum("intervalWh") ttl_wh
FROM ph_model_usage u
LEFT JOIN ph_model_circuit c ON c.id = u.circuit_id
LEFT JOIN ph_model_queen q ON q.id = c.queen_id
LEFT JOIN (SELECT * FROM ph_model_grid g WHERE g.operational_date <= current_date ) g ON g.id = q.grid_id
GROUP BY grid,date("collectTime")) AS a,
(SELECT acc_id,grid FROM customer_no_fixed_fee_v1(""" + d1 + """) WHERE cnt_fixed_billed < 1) AS b
WHERE a.dt = """ + d2 + """ AND a.grid IS NOT NULL AND a.grid = b.grid AND a.ttl_wh > 1); """

    db = 'default'

    if test is not None:
        q = "SELECT * FROM ph_model_customer WHERE account_id IN (SELECT acc_id FROM customer_no_fixed_fee_v1(" + \
            d1 + ") WHERE grid NOT IN ('EngrValidation') and tariff_id is not null);"
        db = 'default'

    accs = []
    zero = Decimal(0.0)
    tstart = utcnow()
    start = True
    xx = 0
    cnt = 0
    logger.info("assess_daily_fee query %s %s" % (q, db))
    try:
        logger.info('fixed: divisor: %s' % str(divisor))
        for c in Customer.objects.exclude(circuit_id__isnull=True).exclude(tariff_id__isnull=True):  # .using(db).raw(q):
            if c.circuit.queen.grid.operational_date > dt:
                logger.debug("assess_daily_fee not operational acount: %s   grid: %s" % (str(c.account_id), c.circuit.queen.grid.name))
                continue

            if start:
                print(utcnow().datetime - tstart.datetime)
                logger.info("assess_daily_fee query complete: %s" % (utcnow().datetime - tstart.datetime))
                start = False

            if test is not None:
                print(utcnow() - tstart)
                c.no_daily_fee = False
                xx += 1
                c.cnt_fixed_billed = test
                # these dates allow billing
                c.op_date = str_to_dt('2010-08-23').date()
                c.queen_op_date = c.op_date

            now_utc = utcnow().datetime
            usage = Usage(
                updated=now_utc,
                created=now_utc,
                intervalVmin=zero,
                collectTime=now_utc,
                intervalVAmax=zero,
                intervalVAmaxH=zero,
                intervalWhH=zero,
                circuit_id=c.circuit_id,
                intervalWh=zero,
                customer_id=c.id,
                processed='False')
            usage.save()
            accs.append(c.account_id)

            processed_usages, usage_charges, duplicate_usages = tariff_manager.TariffManager.process_usages([usage], divisor)

            if processed_usages and len(processed_usages) != 0:
                try:
                    uc = usage_charges[0]
                    if test:
                        print(uc.account_id, uc.amount)
                    uc.save()
                except Exception as e:
                    logger.error("ERROR: %s" % e.message)
                    if test is not None:
                        print("Error Saving uc: ", e.message)
                    xx += 1
                    pass
                load_fixed_charges_to_master_transaction(processed_usages, test)

                cnt += 1
                if test is not None:
                    print(cnt, xx, c.account_id, "no daily", c.no_daily_fee, "fixed billed", c.cnt_fixed_billed,
                          "grid, queen dt", c.op_date, c.queen_op_date, dt)
        if test is not None:
            print(utcnow().datetime - tstart.datetime)
            print(accs, q)
            print (len(accs))
        logger.info('assess_daily_fee  cnt: %s' % str(cnt) + '   ' + str(len(accs)))
    except Exception as e:
        logger.error("ERROR: %s" % e.message)
        traceback.print_exc()
        if test is not None:
            print("Error Saving uc!!!!!!!!!: ", e.message)

    logger.info('accounts to apply daily fee: %s  ----- total accounts applied: %s' % (str(len(accs)), str(cnt)))
    return accs


# TODO remove once we decide not to use it
# pragma: no cover
def assess_daily_fee_always(test=None, dt=utcnow().date):
    """
    Check if customers paid the daily fee on operational grids.
    If they did not pay, charge them.
    This fixes the problem of no daily fee charged if grid is down at midnight or user does not have credit.
    This drives account balances negative.

    Typically this would be called with yesterdays date.
    :param test:        If > 0 then deduct fixed fees, if > 1 do not deduct fixed fees
    :param dt:          Date to check
    :return:
    """

    if isinstance(dt, datetime):
        dt = dt.date()
    # TODO remove Used to be the collect time   date_time = datetime(year=dt.year, month=dt.month, day=dt.day, hour=9, tzinfo=pytz.UTC)

    d1 = "'" + str(dt.year) + '-' + str(dt.month) + '-' + str(dt.day) + "'"

    q = "SELECT * FROM ph_model_customer WHERE account_id IN (SELECT acc_id FROM customer_no_fixed_fee_v1(" + d1 + ") WHERE cnt_fixed_billed < 1);"

    accs = []
    zero = Decimal(0.0)
    tstart = utcnow()
    start = True
    xx = 0
    cnt = 0
    logger.info("assess_daily_fee query %s" % q)
    try:
        for c in Customer.objects.raw(q):
            if start:
                print(utcnow().datetime - tstart.datetime)
                logger.info("assess_daily_fee query complete: %s" % (utcnow().datetime - tstart.datetime))
                start = False

            if test is not None:
                print(utcnow() - tstart)
                c.no_daily_fee = False
                print(tstart.datetime, c.no_daily_fee, c.circuit.queen.grid.name, c.account_id,
                      c.tariff.entryTariffCalendar.entryTariffSegment.perSegmentCharge, c.circuit.deviceId)
                xx += 1
                c.cnt_fixed_billed = test
                # these dates allow billing
                c.op_date = str_to_dt('2010-08-23').date()
                c.queen_op_date = c.op_date

            now_utc = utcnow().datetime
            usage = Usage(
                updated=now_utc,
                created=now_utc,
                intervalVmin=zero,
                collectTime=now_utc,
                intervalVAmax=zero,
                intervalVAmaxH=zero,
                intervalWhH=zero,
                circuit_id=c.circuit_id,
                intervalWh=zero,
                customer_id=c.id,
                processed='False')
            usage.save()
            accs.append(c.account_id)

            processed_usages, usage_charges, duplicate_usages = tariff_manager.TariffManager.process_usages([usage])

            if processed_usages and len(processed_usages) != 0:
                try:
                    uc = usage_charges[0]
                    if test:
                        print(uc.account_id, uc.amount)
                    uc.save()
                except Exception as e:
                    logger.error("ERROR: %s" % e.message)
                    if test is not None:
                        print("Error Saving uc: ", e.message)
                    xx += 1
                    pass
                load_fixed_charges_to_master_transaction(processed_usages, test)

                cnt += 1
                if test is not None:
                    print(cnt, xx, c.account_id, "no daily", c.no_daily_fee, "fxed billed", c.cnt_fixed_billed,
                          "grid, queen dt", c.op_date, c.queen_op_date, dt)
        if test is not None:
            print(utcnow().datetime - tstart.datetime)
            print(accs, q)
            print (len(accs))
    except Exception as e:
        logger.error("ERROR: %s" % e.message)
        traceback.print_exc()
        if test is not None:
            print("Error Saving uc!!!!!!!!!: ", e.message)

    logger.info('accounts to apply daily fee: %s  ----- total accounts applied: %s' % (str(len(accs)), str(cnt)))
    return accs


# TODO this takes communicaton and circuit voltage into account.
# pragma: no cover
def assess_daily_fee_uptime_circuit(test=None, dt=utcnow().date - timedelta(days=1)):
    """
    Check if customers paid the daily fee on operational grids.
    If they did not pay, charge them.
    This fixes the problem of no daily fee charged if grid is down at midnight or user does not have credit.
    This drives account balances negative.

    Typically this would be called with yesterdays date.

    Note that the biz rule to apply the daily fee or not is implemented

    HC Fixed Tariff Rules  2017-08-20
    The fixed portion of the tariff is only charged if certain uptime requirements are met.

    (1) Out of spec voltage for 12 or more hours between 7am and 1am EAT the following morning.
                 67% or 12 hrs without proper voltage in 18hr period    4:00 -  22:00 UTC

    (2) Out of spec voltage at queen for 3 or more hours between 6pm 12am EAT.
                 50% or  3hrs without proper voltage in     5hr period  15:00 - 21:00  UTC

This is calculated once per day, for the previous day, on a circuit by circuit basis.


    TODO block DB processing has issues, figure out why

    :param test:        If > 0 then deduct fixed fees, if > 1 do not deduct fixed fees
    :param dt:          Date to check
    :return:
    """

    db = 'default'

    if isinstance(dt, datetime):
        dt = dt.date()
    date_time = datetime(year=dt.year, month=dt.month, day=dt.day, hour=9, tzinfo=pytz.UTC)
    d1 = "'" + str(dt.year) + '-' + str(dt.month) + '-' + str(dt.day) + "'"

    # checking if fixed fee has been charged in 2 different ways to make sure we assess the fixed fee correctly
    q = "SELECT * FROM (SELECT id, c.* FROM ph_model_customer AS cc,(SELECT * FROM customer_no_fixed_fee_v1(" + \
        d1 + ")) AS c WHERE cc.account_id = c.acc_id) AS x LEFT JOIN (SELECT * FROM no_daily_fee(" + d1 + \
        ")) AS n ON x.grid = n.grid AND x.qn = n.qn AND x.cn = n.cn WHERE x.grid != 'EngrValidation';"

    accs = []
    xx = 0
    cnt = 0
    start = True
    zero = Decimal(0.0)
    # TODO make up some test data --- circuit monitors that include / exclude daily fixed fees
    if test is not None:
        q = "SELECT * FROM ph_model_customer WHERE account_id IN (SELECT acc_id FROM customer_no_fixed_fee_v1(" + \
            d1 + ") WHERE grid NOT IN ('EngrValidation'));"
        db = 'default'

    tstart = utcnow()
    logger.info("assess_daily_fee query %s" % q)
    try:
        for c in Customer.objects.raw(q, using=db):
            if start:
                print(utcnow().datetime - tstart.datetime)
                logger.info("assess_daily_fee query complete: %s" % (utcnow().datetime - tstart.datetime))
                start = False
            if test is not None:
                print(utcnow() - tstart)
                c.no_daily_fee = False
                print(tstart.datetime, c.no_daily_fee, c.circuit.queen.grid.name, c.account_id,
                      c.tariff.entryTariffCalendar.entryTariffSegment.perSegmentCharge, c.circuit.deviceId)
                xx += 1
                c.cnt_fixed_billed = test
                # these dates allow billing
                c.op_date = str_to_dt('2010-08-23').date()
                c.queen_op_date = c.op_date

            # we are suppose to charge the fixed fee and we have not billed it already and site / queen is operational
            # Note we can not check test mode, because we are looking in the past
            if (not c.no_daily_fee) and c.cnt_fixed_billed < 1 and (c.op_date <= dt or c.queen_op_date <= dt):
                usage = Usage(
                    updated=date_time,
                    created=date_time,
                    intervalVmin=zero,
                    collectTime=date_time,
                    intervalVAmax=zero,
                    intervalVAmaxH=zero,
                    intervalWhH=zero,
                    circuit_id=c.circuit_id,
                    intervalWh=zero,
                    customer_id=c.id,
                    processed='False')
                usage.save()
                accs.append(c.account_id)

                processed_usages, usage_charges, duplicate_usages = tariff_manager.TariffManager.process_usages([usage])

                if processed_usages and len(processed_usages) != 0:
                    try:
                        if test is not None:
                            print(usage_charges[0].account_id)

                        usage_charges[0].save()
                    except Exception as e:
                        logger.error("ERROR: %s" % e.message)
                        if test is not None:
                            print("Error Saving uc: ", e.message)
                        pass
                    load_fixed_charges_to_master_transaction(processed_usages, test)
            else:
                xx += 1
            cnt += 1
            if test is not None:
                print(cnt, xx, c.account_id, "no daily", c.no_daily_fee, "fxed billed", c.cnt_fixed_billed,
                      "grid, queen dt", c.op_date, c.queen_op_date, dt)

        if test is not None:
            print(utcnow().datetime - tstart.datetime)
            print(accs, q)
            print (len(accs), xx)

    except Exception as e:
        logger.error("ERROR: %s" % e.message)
        if test is not None:
            print("Error Saving uc!!!!!!!!!: ", e.message)

    logger.info('accounts to apply daily fee: %s  ----- total accounts: %s' % (str(len(accs)), str(cnt)))
    return accs


def run_db_update(sql_str, conn=connection, log=logger, debug=False):
    """
    Executes a DB update SQL statement
    :param sql_str:     SQL to execute --- not validated
    :param conn:        Connection to ue
    :param log:         logger to use
    :param debug:       if true print to console
    :return:            msg
    """
    err = None
    st = utcnow().epoch
    msg = 'Start time UTC: %s\n' % utcnow().format_datetime()
    msg += 'SQL: "%s"' % sql_str
    with conn.cursor() as cursor:
        try:
            cursor.execute(sql_str)
            s = utcnow().epoch - st
        except DatabaseError as e:
            s = utcnow().epoch - st
            err = e
            msg += 'DatabaseError %s \nafter %s seconds' % (str(e), s)
            log.error(msg, exc_info=1)

        if err is not None:
            msg = "Updated  %s  rows in %s seconds\n" % (cursor.rowcount, str(s))
            log.info(msg)
        if debug:
            print(msg)
        return msg


def calc_monthly_usage_stima_points(customer, today=utcnow()):
    """
        Caluclates the monthly usage and STIMA points
    :param today:       Date to consider or None
    :param customer:    The customer to get usage on
    :return:
    """
    if isinstance(today, Delorean):
        today = today.datetime
    first_of_this_month = datetime(today.year, today.month, 1)
    first_of_next_month = datetime(today.year, today.month, 1) + relativedelta(months=1)
    if test_mode():
        all_use_date = datetime(2001, 1, 1)
    else:
        all_use_date = datetime(2017, 3, 1)
    usage = Usage.customer_usages.all().filter(
        customer_id=customer.id, collectTime__gte=all_use_date, collectTime__lt=first_of_next_month).aggregate(Sum('intervalWh'))
    stima_points = usage.get('intervalWh__sum', 0)
    usage = Usage.customer_usages.all().filter(
        customer_id=customer.id, collectTime__gte=first_of_this_month, collectTime__lt=first_of_next_month).aggregate(Sum('intervalWh'))
    usage_month = usage.get('intervalWh__sum', 0)
    return (0, usage_month)[usage_month is not None], int((0, stima_points)[stima_points is not None] / 10), \
           (0, stima_points)[stima_points is not None] / 1000
