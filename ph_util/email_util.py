# coding=utf-8
"""Email utils"""

from django.conf import settings
from django.core import mail
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from ph_util.test_utils import test_mode
from powerhive.common_settings import DEFAULT_ACCOUNTING_EMAILS, TEST_EMAILS


def send_template_email(subject, template_name, data, sender=None, receivers=None, attachments=None):
    """Send email rendering @param templateName using context data from @param data."""
    data = data or {}
    data['PH_SERVER'] = settings.PH_SERVER
    receivers = receivers or settings.POWERHIVE_DEV_TEAM
    sender = sender or settings.POWERHIVE_NO_REPY
    rendered_data = render_to_string(template_name, data)
    msg = mail.EmailMessage(subject, rendered_data, sender, receivers)
    msg.content_subtype = "html"
    if attachments:
        for attachment in attachments:
            # noinspection PyBroadException
            try:
                msg.attach_file(attachment)
            except:
                pass
    return msg.send()


def send_loan_init_email(msg, emails=DEFAULT_ACCOUNTING_EMAILS):
    """
    Send loan info, called when a loan is initiated

    :param msg:     body of email
    :param emails:  list if recipients
    """
    if test_mode():  # pragma: no cover
        emails = TEST_EMAILS

    email = EmailMessage(
        'Loan Initiated',
        msg,
        settings.SERVER_EMAIL,
        emails,
        reply_to=[settings.SERVER_EMAIL],
        headers={'Message-ID': 'loan initiated'}
    )
    return email.send()
