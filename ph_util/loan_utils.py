# coding=utf-8
""" loan utilities """

from decimal import Decimal
import math
from delorean import utcnow, now
import logging
from ph_model.models import transaction, NetworkServiceProvider
from ph_model.models.account import Account
from ph_model.models.loan import Loan
from ph_model.models.customer import CustomerType
from ph_model.models.event import EventTypeConst
from ph_model.models.transaction import TransactionType
from ph_model.models.user import User
from ph_operation import loan_manager, customer_manager, event_manager
from ph_util.accounting_util import LoanType, RESIDENTIAL_CONN_LOAN_AMT, CONN_LOAN_DEPOSIT_AMT, \
    COMMERCIAL_CONN_LOAN_AMT, add_vat, CONN_LOAN_DAYS, CONN_LOAN_FEE_AMT, calc_vat, MUNIS_NO_VAT, NO_VAT_REASON, \
    BONUS_PWR_ADJ_TYPE, COMMERCIAL_I_BONUS_POWER_AMT, COMMERCIAL_II_BONUS_POWER_AMT, RESIDENTIAL_BONUS_POWER_AMT, DEAD_LOAN_ACCOUNT
from ph_model.models import CreditAdjustmentHistory
from ph_util.date_util import get_utc_now

logger = logging.getLogger(__name__)


def create_connection_fee_loan(customer, no_deposit=False, force=False, now_utc=utcnow()):
    """
    Create the payment plan for the connection fee if necessary
    Deducts the deposit required to get the loan
    Checks if VAT credit has to be applied

    :param customer:
    :param no_deposit: True only make loan
    :param force    True then make the loan no matter what
    :param now_utc:
    :return:        The loan
    """

    loan = None
    if customer is None:
        return loan
    if customer.connectionFeeLoan:
        l = Loan
        if force:
            conn_loan_cnt = 0
        else:
            conn_loan_cnt = l.objects.filter(
                account_id=customer.account.id,
                loanType=LoanType.CONNECTION_FEE_LOAN.name,
                loanAmount__gte=CONN_LOAN_DEPOSIT_AMT).count()

        if conn_loan_cnt == 0:
            conn_loan_amt = COMMERCIAL_CONN_LOAN_AMT
            if customer.customerType == CustomerType.RESIDENTIAL.name:
                conn_loan_amt = RESIDENTIAL_CONN_LOAN_AMT
            loan_amt = conn_loan_amt - CONN_LOAN_DEPOSIT_AMT + CONN_LOAN_FEE_AMT

            daily_fee = add_vat(loan_amt) / CONN_LOAN_DAYS
            create_loan(
                loan_cls=l,
                loan_type=LoanType.CONNECTION_FEE_LOAN.name,
                acc=customer.account,
                loan_amt=loan_amt,
                start_date=now_utc.datetime,
                payment=daily_fee)

            # make 1 payment connection payment plan deposit loan, and pay it off now
            if no_deposit is False:
                loan = create_loan(
                    loan_cls=Loan,
                    loan_type=LoanType.CONNECTION_FEE_LOAN.name,
                    acc=customer.account,
                    loan_amt=Decimal(CONN_LOAN_DEPOSIT_AMT),
                    start_date=now_utc.datetime,
                    payment=Decimal(add_vat(CONN_LOAN_DEPOSIT_AMT)),
                    notes='The amount customers are suppose to deposit to get a connection payment plan')
                if loan.account.id != DEAD_LOAN_ACCOUNT:
                    loan_handler = loan_manager.Loan(loan)
                    loan_handler.repay_loan(customer)

                    if customer.municipality.name in MUNIS_NO_VAT:
                        t_type = TransactionType.CREDIT.value
                        from ph_operation import transaction_manager
                        transaction_type = transaction_manager.Credit

                        trans = transaction_type(customer.account, calc_vat(conn_loan_amt))
                        trans.process()
                        adjustment_history = CreditAdjustmentHistory(
                            account=customer.account,
                            user=User.objects.all()[0],
                            reason=NO_VAT_REASON,
                            amount=calc_vat(conn_loan_amt),
                            processedTime=get_utc_now(),
                            transactionType=t_type,
                            adj_type='Marketing'
                        )
                        adjustment_history.save()
    return loan


def apply_bonus_power(cust, account):
    """
    Bonus power generates a credit adjustment

    :param  cust:   The customer
    :param account: The customer account
    :return:        The adjustment
    """
    # TODO what user do we use for this ?
    user = User.objects.all()[0]
    ah_ret = None
    amt = RESIDENTIAL_BONUS_POWER_AMT
    if cust.customerType == CustomerType.COMMERCIAL.name:
        amt = COMMERCIAL_I_BONUS_POWER_AMT
    elif cust.customerType == CustomerType.COMMERCIAL_II.name:
        amt = COMMERCIAL_II_BONUS_POWER_AMT
    if user is not None and cust is not None:
        reason = 'Free 1KWH bonus power'
        ah_ret = customer_manager.CustomerManager.adjust_credit(
            cust,
            user,
            reason,
            amt,
            BONUS_PWR_ADJ_TYPE,
        )
    else:
        reason = ''
        if user is None:
            reason += 'User does not exist  '
        if cust is None:
            reason += 'Customer does not exist'
        event_manager.raise_event(
            cust.id,
            EventTypeConst.BONUS_POWER_CREDIT_FAILED.value, [
                {'available balance': account.accountBalance},
                {'Credit amount': amt}])
    return ah_ret

def create_connection_fee_loan_one_payment(customer, force=False, now_utc=utcnow()):
    """
    Create the payment plan for the connection fee if necessary
    Deducts the total amount from the customer's account
    Checks if VAT credit has to be applied

    :param customer: 
    :param force    True then make the loan no matter what
    :param now_utc:
    :return:        The paid off loan
    """

    loan = None
    if customer is None or customer.connectionFeeLoan:
        return loan
    if force:
        conn_loan_cnt = 0
    else:
        conn_loan_cnt = Loan.objects.filter(
            account_id=customer.account.id,
            loanType=LoanType.CONNECTION_FEE_LOAN.name).count()

    if conn_loan_cnt == 0:
        conn_loan_amt = COMMERCIAL_CONN_LOAN_AMT
        if customer.customerType == CustomerType.RESIDENTIAL.name:
            conn_loan_amt = RESIDENTIAL_CONN_LOAN_AMT

        # make 1 payment connection payment plan deposit loan, and pay it off now
        loan = create_loan(
            loan_cls=Loan,
            loan_type=LoanType.CONNECTION_FEE_LOAN.name,
            acc=customer.account,
            loan_amt=Decimal(conn_loan_amt),
            start_date=now_utc.datetime,
            payment=Decimal(add_vat(conn_loan_amt)),
            notes='Did not request loan, paying full connection fee')
        if loan.account.id != DEAD_LOAN_ACCOUNT:
            loan_handler = loan_manager.Loan(loan)
            ll, acc_bal = loan_handler.repay_loan(customer)
            if customer.municipality.name in MUNIS_NO_VAT:
                t_type = TransactionType.CREDIT.value
                from ph_operation import transaction_manager
                transaction_type = transaction_manager.Credit

                trans = transaction_type(customer.account, calc_vat(conn_loan_amt))
                trans.process()
                adjustment_history = CreditAdjustmentHistory(
                    account=customer.account,
                    user=User.objects.all()[0],
                    reason=NO_VAT_REASON,
                    amount=calc_vat(conn_loan_amt),
                    processedTime=get_utc_now(),
                    transactionType=t_type,
                    adj_type='Marketing'
                )
                adjustment_history.save()

            account = Account.objects.get(pk=customer.account_id)
            # they had enough $$$ to pay the connection fee in one go
            if not ll.passedDue:
                apply_bonus_power(customer, account)
    return loan


def create_loan(loan_cls, loan_type, acc, loan_amt, start_date, payment, freq=1, notes=''):
    """
    Want a loan, call this function
    :param loan_cls:    Loan class
    :param loan_type:   loan type string from LoanType.XXXX.name
    :param acc:         Account
    :param loan_amt:    amount of the loan
    :param start_date:  start date for payments
    :param payment:     the payment amount, used to determine the number of payments
    :param freq:        how often to make payments in days
    :param notes:       additional notes
    :return: the loan that was created
    """
    ll = loan_cls(
        account=acc,
        outStandingPrinciple=add_vat(loan_amt),
        loanAmount=loan_amt,
        startDate=start_date,
        paymentFrequency=freq,
        totalNumberOfPayments=math.ceil(add_vat(loan_amt) / Decimal(payment)),
        loanType=loan_type,
        notes=notes +
              '\nCreate ' + str(loan_type) + ' , Amount: ' + format(loan_amt, '0.4f') + ' for Account: ' + str(acc) +
              '\nVAT:                ' + format((calc_vat(loan_amt)), '0.4f') +
              '\nNumber of payments: ' + format((int(add_vat(loan_amt) / Decimal(payment))), '0.4f') +
              '\nStart date:         ' + str(start_date))
    ll.save()
    return ll


# noinspection PyTypeChecker
def make_notes(notes, loan_type, loan_amt, acc, ttl_days, start_date, freq):
    """
    Build the notes for a loan
    :param notes:       initial notes
    :param loan_type:   loan type string from LoanType.XXXX.name
    :param loan_amt:    amount of the loan
    :param acc:         Account
    :param ttl_days:    Term of the loan
    :param start_date:  start date for payments
    :param freq:        how often to make payments in days
    :return: the notes
    """
    payment_amt = float(loan_amt) / float(ttl_days)
    notes = '\nDate UTC: ' + now().utcnow().format_datetime() + '\n' + notes + '\nCreate ' + str(loan_type) + ' loan, Amount: ' + \
            format(loan_amt, '0.4f') + ' for Account: ' + str(acc) + '\n NO VAT'
    if freq == 1:
        notes = notes + '   Daily payments of  ' + format(payment_amt, '0.4f')
    else:
        notes = notes + '   Payments every  ' + str(freq) + '  days of  ' + format(payment_amt, '0.4f')
    notes + '\nNumber of payments: ' + format((int(Decimal(loan_amt) / Decimal(payment_amt))), '0.4f') + '\nStart date:  ' + str(start_date)
    return notes


def create_payment_plan_no_vat(loan_cls, loan_type, acc, loan_amt, start_date, ttl_days, freq=1, notes=''):
    """
    Want a loan without VAT added, call this function
    :param loan_cls:    Loan class
    :param loan_type:   loan type string from LoanType.XXXX.name
    :param acc:         Account
    :param loan_amt:    amount of the loan
    :param start_date:  start date for payments
    :param ttl_days:    Term of the loan
    :param freq:        how often to make payments in days
    :param notes:       additional notes
    :return: the loan that was created
    """
    ll = loan_cls(
        account=acc,
        outStandingPrinciple=loan_amt,
        loanAmount=loan_amt,
        startDate=start_date,
        paymentFrequency=freq,
        totalNumberOfPayments=ttl_days,
        loanType=loan_type,
        notes=make_notes(notes, loan_type, loan_amt, acc, ttl_days, start_date, freq))
    ll.save()
    return ll

def mth_mobile_balance(amount, acc_id):
    """
    Create mobile payment
    :param amount:      the amount of the payment
    :param acc_id           account ID
    :return:
    """

    transaction.MobilePayment(
        account=Account.objects.get(pk=acc_id), transactionId="MP12345",
        networkServiceProvider=NetworkServiceProvider.objects.get_or_none(pk=1),
        amount=amount, processedTime=utcnow().datetime).save()
