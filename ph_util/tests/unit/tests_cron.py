# coding=utf-8
""" Verify cron jobs can be imported """

from django.conf import settings
from django.test import SimpleTestCase
from django.utils.module_loading import import_string

class CronClassTests(SimpleTestCase):
    """Verify the classes referenced in the cron config exist"""

    def test_cron_classes(self):
        """
        Verify we can import the cron classes
               
        :return: 
        """

        with self.assertRaises(Exception) as context:
            import_string('ph_operation.cron.UpdateBonusPower')
        self.assertTrue('ph_operation.cron" does not define a "UpdateBonusPower" in context.exception')

        classes = settings.CRON_CLASSES
        for cls in classes:
            import_string(cls)

        self.assertEqual(settings.NUM_CRON_CLASSES, len(classes))
