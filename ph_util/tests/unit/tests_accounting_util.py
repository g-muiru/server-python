# coding=utf-8
"""Tests for date util classes."""
from decimal import Decimal, ROUND_FLOOR

from django.conf import settings
from django.test import SimpleTestCase

from ph_util.email_util import send_loan_init_email
from ph_util.accounting_util import init_loan_type_to_accounting_code, AccountingEnums, LoanType, add_vat, calc_vat
from ph_util.generic_helpers import have_internet
from ph_util.test_utils import assert_eq
from ddt import ddt, unpack, idata


class LoanInitEmailTests(SimpleTestCase):
    """Send the loan init email -- requires internet connection"""
    if not have_internet():
        print("\nNo internet, LoanInitEmailTests will not run")
    else:
        msg = '123456\n\nABCDEF\n\n'
        val = send_loan_init_email(msg)
        assert_eq(1, val)


@ddt
class LoanUtilTests(SimpleTestCase):
    """Tests loan utils"""
    data = (
        (LoanType.CONNECTION_FEE_LOAN, AccountingEnums.LOAN_CONN_FEE_INITc235, False),
        (LoanType.APPLIANCE_LOAN, AccountingEnums.LOAN_APPLIANCE_INITc278, False),
        (LoanType.FINANCIAL_ASSISTANCE_LOAN, AccountingEnums.LOAN_FINANCIAL_INITc279, False),
        (LoanType.OTHER_LOAN, AccountingEnums.LOAN_OTHER_INITc280, False),
        (LoanType.KUKU_POA_NO_VAT_LOAN, AccountingEnums.LOAN_KUKU_POA_INITc369, False),
        (LoanType.INTERNET_PAYMENT_PLAN, AccountingEnums.LOAN_INTERNET_INITc370, False),
        ('CONNECTION_FEE_LOAN', AccountingEnums.LOAN_CONN_FEE_INITc235, False),
        ('APPLIANCE_LOAN', AccountingEnums.LOAN_APPLIANCE_INITc278, False),
        ('FINANCIAL_ASSISTANCE_LOAN', AccountingEnums.LOAN_FINANCIAL_INITc279, False),
        ('OTHER_LOAN', AccountingEnums.LOAN_OTHER_INITc280, False),
        ('KUKU_POA_NO_VAT_LOAN', AccountingEnums.LOAN_KUKU_POA_INITc369, False),
        ('INTERNET_PAYMENT_PLAN', AccountingEnums.LOAN_INTERNET_INITc370, False),
        (1, AccountingEnums.LOAN_OTHER_INITc280, False),
        ('absndf', AccountingEnums.LOAN_OTHER_INITc280, False),
        (None, AccountingEnums.LOAN_OTHER_INITc280, False),
        (AccountingEnums.COM2_FIXED_FEEc232, AccountingEnums.COM2_FIXED_FEEc232, False),
        (AccountingEnums.COM1_FIXED_FEEc231, AccountingEnums.COM1_FIXED_FEEc231, False),

        (LoanType.CONNECTION_FEE_LOAN, AccountingEnums.VAT_LOAN_INITc301, True),
        (LoanType.APPLIANCE_LOAN, AccountingEnums.VAT_LOAN_INITc301, True),
        (LoanType.FINANCIAL_ASSISTANCE_LOAN, AccountingEnums.VAT_LOAN_INITc301, True),
        (LoanType.OTHER_LOAN, AccountingEnums.VAT_LOAN_INITc301, True),
        (LoanType.KUKU_POA_NO_VAT_LOAN, AccountingEnums.VAT_LOAN_INITc301, True),
        (LoanType.INTERNET_PAYMENT_PLAN, AccountingEnums.VAT_LOAN_INITc301, True),
        ('CONNECTION_FEE_LOAN', AccountingEnums.VAT_LOAN_INITc301, True),
        ('APPLIANCE_LOAN', AccountingEnums.VAT_LOAN_INITc301, True),
        ('FINANCIAL_ASSISTANCE_LOAN', AccountingEnums.VAT_LOAN_INITc301, True),
        ('OTHER_LOAN', AccountingEnums.VAT_LOAN_INITc301, True),
        ('KUKU_POA_PAYMENT_PLAN', AccountingEnums.VAT_LOAN_INITc301, True),
        ('INTERNET_PAYMENT_PLAN', AccountingEnums.VAT_LOAN_INITc301, True),
        (1, AccountingEnums.VAT_LOAN_INITc301, True),
        ('absndf', AccountingEnums.VAT_LOAN_INITc301, True),
        (None, AccountingEnums.VAT_LOAN_INITc301, True),
        (AccountingEnums.COM2_FIXED_FEEc232, AccountingEnums.VAT_TARIFF_FIXEDc346, True),
        (AccountingEnums.COM1_FIXED_FEEc231, AccountingEnums.VAT_TARIFF_FIXEDc346, True)
    )

    @idata(data)
    @unpack
    def test_loan_type_to_netsuite_code(self, loan_type, expect, vat):
        """Tests init_loan_type_to_accounting_code"""
        assert_eq(expect, init_loan_type_to_accounting_code(loan_type, vat))

    @staticmethod
    def test_calc_vat():
        """Tests calc_vat function, assumes 16% """
        assert_eq(16, round(calc_vat(100), 4))
        assert_eq(1.6, round(calc_vat(10), 4))
        assert_eq(0.16, round(calc_vat(1), 4))
        assert_eq(-16, round(calc_vat(-100), 4))
        assert_eq(-1.6, round(calc_vat(-10), 4))
        assert_eq(-0.16, round(calc_vat(-1), 4))
        assert_eq('100', calc_vat('100'))
        assert_eq('-100', calc_vat('-100'))
        assert_eq('Mary had a little lamb', calc_vat('Mary had a little lamb'))

    data = (Decimal(0.00), Decimal(5.00), Decimal(-99.76), float('99.99'), int('66'),
            Decimal('1.1'), 348304986.66345435, -120391099234.457987435, '123456', 'asn,mndsf',
            (1, 2, 3), [1, 'ddd', None], None)

    @idata(data)
    def test_add_vat(self, val):
        """VAT added correctly, Note that is you hand in garbage, you get back the same garbage"""
        if type(val) not in (Decimal, float, int):
            assert_eq(val, add_vat(val))
        else:
            assert_eq(Decimal(Decimal(val) * Decimal(1 + settings.VAT_MULTIPLIER)).quantize(Decimal('0.00'), rounding=ROUND_FLOOR), add_vat(val))
