# coding=utf-8
"""Tests for never neg tariff classes."""
from django.test import SimpleTestCase

from ph_util.accounting_util import RESIDENTIAL_TARIFF_DAILY_FIXED_AMT, KENYA_VAT_RATE
from ph_util.never_neg_tariff_utils import FIXED_UNCOLLECTED, GUINEA_PIG_FIXED_UNCOLLECTED, CLEARING_DUE_DATE, SMS_DATE, PILOT_GUINEA_PIGS
from ph_util.test_utils import assert_eq


class NeverNegUnitTests(SimpleTestCase):
    assert_eq(round(FIXED_UNCOLLECTED / 2, 3), round(GUINEA_PIG_FIXED_UNCOLLECTED, 3))
    assert_eq(round(RESIDENTIAL_TARIFF_DAILY_FIXED_AMT * (1 + KENYA_VAT_RATE), 3), round(FIXED_UNCOLLECTED, 3))
    assert_eq('2019-04-01', CLEARING_DUE_DATE)
    assert_eq(25, SMS_DATE)
    assert_eq(['Baranne-CF', 'Nyamondo-CF', 'Matangamano-CF'], PILOT_GUINEA_PIGS)
