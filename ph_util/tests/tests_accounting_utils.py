# coding=utf-8
"""Test accounting utils"""

from ddt import ddt, unpack, idata
from django.core.management import call_command
import ph_model.models as ph_model
from ph_operation.tariff_manager import get_operational_date

from ph_operation.tests import base_test
from ph_util.date_util import str_to_dt


@ddt
class LoanDbTest(base_test.BaseTestOperation):
    def setUp(self):
        """Create especial customer and circuit or related usage objects for the dynamic usage test from file."""
        super(LoanDbTest, self).setUp()
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_operation/tests/fixtures/usage_config.json', verbosity=0)

    before_date_str = '2001-01-01'
    after_date_str = '2222-02-22'
    data = (
        (before_date_str, before_date_str),
        (before_date_str, after_date_str),
        (after_date_str, before_date_str)
    )

    @idata(data)
    @unpack
    def test_get_operational_date(self, str_dt_grid, str_dt_queen):
        """
        Tests operational date.

        :param str_dt_grid:     grid date
        :param str_dt_queen:    queen date
        :return:
        """
        circuit = ph_model.circuit.Circuit.objects.get(pk=100)
        circuit.queen.grid.operational_date = str_to_dt(str_dt_grid)
        circuit.queen.operational_date = str_to_dt(str_dt_queen)
        circuit.queen.grid.save()
        circuit.queen.save()
        self.assertEqual(str_to_dt(self.before_date_str), get_operational_date(circuit))
