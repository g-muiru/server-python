"""Tests for phone number util classes."""
from django import test

from ph_util import phone_number_util

class E164FormatPhoneNumberUtilTest(test.TestCase):
    """Tests phone_number_util"""

    def test_get_e164_format(self):
        """Tests ph_util.phone_number_util.get_e164_format"""
        phone = "+15103162285"
        self.assertEquals(phone,
            phone_number_util.get_e164_format(phone,'+1'))

    def test_get_e164_format(self):
        """Tests ph_util.phone_number_util.get_e164_format"""
        phone = "5103162285"
        self.assertEquals('+15103162285',
            phone_number_util.get_e164_format(phone, countryCode='+1'))

    def test_get_e164_format(self):
        """Tests ph_util.phone_number_util.get_e164_format"""
        phone = "05103162285"
        self.assertEquals('+15103162285',
            phone_number_util.get_e164_format(phone, countryCode='+1'))

    def test_get_e164_format(self):
        """Tests ph_util.phone_number_util.get_e164_format"""
        phone = "05103162285"
        self.assertIsNone(phone_number_util.get_e164_format(phone))

    def test_get_e164_format(self):
        """Tests ph_util.phone_number_util.get_e164_format"""
        phone = "5103162285"
        self.assertIsNone(phone_number_util.get_e164_format(phone))
