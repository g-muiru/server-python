# coding=utf-8
"""Tests for never neg tariff classes."""

from decimal import Decimal
from django.core.management import call_command
from ph_model.models import Account, Customer
from ph_model.models.municipality import Municipality
from ph_model.models.transaction import MasterTransactionHistory, update_mth_balances
from ph_operation import transaction_manager
from ph_operation.tests import base_test
from ph_util.accounting_util import half_fixed_site
from ph_util.date_util import str_to_dt
from ph_util.never_neg_tariff_utils import FIXED_UNCOLLECTED, GUINEA_PIG_FIXED_UNCOLLECTED, SMS_DATE, PILOT_GUINEA_PIGS, \
    calc_due, first_of_month_processing, sms_date_processing, SMS_TEMPLATE, process_uncollected
from ph_util.test_utils import assert_eq
from ddt import ddt, unpack, idata


class NeverNegTestCase(base_test.BaseTestOperation):
    def setUp(self):
        """ Setup"""
        super(NeverNegTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/loan_config.json', verbosity=0)
        MasterTransactionHistory.objects.all().delete()
        Customer.objects.get(id=2).delete()
        Customer.objects.get(id=603).delete()
        m = Municipality.objects.get(id=1)
        m.name = PILOT_GUINEA_PIGS[0]
        m.save()
        for c in Customer.objects.all():
            if c.id == 601:
                c.municipality_id = 1
            else:
                c.municipality_id = 2
            c.save()


# TODO: can not handle positive uncollected balance --- make sure over payment into uncollected  goes to clearing or account balance
# TODO:  (100, -10, 222, -111, 100, -10, 222, -111),
# noinspection PyArgumentEqualDefault,PyArgumentEqualDefault
@ddt
class NeverNegTests(NeverNegTestCase):
    def calc_due_test(self):
        """ PENDING TODO: NOT USED for now, clearing balance is not dealt with yet"""
        for c in Customer.objects.all():
            assert_eq(calc_due(c.account, 13), (None, None))
            assert_eq(calc_due(c.account, -1), (None, None))
            assert_eq(calc_due(c.account, 0), (0, 0))
            assert_eq(calc_due(c.account, 1), (0, 0))
            assert_eq(calc_due(c.account, 11), (0, 0))
            assert_eq(calc_due(c.account, 12), (0, 0))
            c.account.clearing_bal = -100.00
            assert_eq(calc_due(c.account, 1), (100, 100))
            assert_eq(calc_due(c.account, 2), (50, 50))
            assert_eq(calc_due(c.account, 4), (25, 25))
            assert_eq(calc_due(c.account, 10), (10, 10))

    def process_uncollected_test(self):
        """ Validate correct code executes on correct day"""
        res = process_uncollected(str_to_dt('2019-03-01'), True)
        idx = 0
        for x in res:
            if idx == 0:
                assert_eq(0, x[0])
                assert_eq(round(-307.4579999999999770921021991, 2), round(x[1], 2))
                assert_eq(0, x[2])
            else:
                assert_eq((0, 0, 0), x)
            idx += 1

        res = process_uncollected(str_to_dt('2019-03-01',delorean=True), True)
        idx = 0
        for x in res:
            if idx == 0:
                assert_eq(round(-307.4579999999999770921021991, 2), round(x[0],2))
                assert_eq(round(-307.4579999999999770921021991, 2), round(x[1], 2))
                assert_eq(round(-307.4579999999999770921021991, 2), round(x[2],2))
            else:
                assert_eq((0, 0, 0), x)
            idx += 1

        res = process_uncollected(str_to_dt('2019-03-25'), True)
        for x in res:
            if x[0] is not None:
                self.assertTrue('Friendly reminder:' in x[0].message)

        res = process_uncollected(str_to_dt('2019-03-25',delorean=True), True)
        for x in res:
            if x[0] is not None:
                self.assertTrue('Friendly reminder:' in x[0].message)

        assert_eq([], process_uncollected(str_to_dt('2019-03-24'), True))
        assert_eq([], process_uncollected(str_to_dt('2019-03-24',delorean=True), True))

    data = [
        (100, -10, 0, 0, 100, -10, 0, 0),
        (100, -10, -20, 0, 100, -10, 0, -20),
        (100, -10, -222, -111, 100, -10, 0, -333),
        # TODO (100, -10, 222, -111, 100, -10, 222, -111),

        (0, -10, 0, 0, 0, -10, 0, 0),
        (0, -10, -20, 0, 0, -10, 0, -20),
        (0, -10, -222, -111, 0, -10, 0, -333),
        # TODO (0, -10, 222, -111, 0, -10, 222, -111)
    ]

    @idata(data)
    @unpack
    def first_of_month_processing_test(self, starting_bal, clearing, uncollected, reconnect, expected, expected_clear, expected_uncol,
                                       expected_recon):
        """
        Calc account, uncollected, reconnect balances, meant to be called the 1st of the month

        :param starting_bal:
        :param clearing:
        :param uncollected:
        :param reconnect:
        :param expected:
        :param expected_clear:
        :param expected_uncol:
        :param expected_recon:
        :return:
        """

        c = Customer.objects.get(id=601)
        c.account.accountBalance = Decimal(0.0)
        c.account.clearing_bal = clearing
        c.account.uncollected_bal = uncollected
        c.account.reconnect_bal = reconnect
        c.account.save()
        transaction_manager.TransactionManager. \
            process_mobile_payment(c.account, ('xx' + str(c.id)), starting_bal, str_to_dt('2016-03-02T01:01:00Z'))

        update_mth_balances()
        first_of_month_processing(c)

        uc = Decimal(0.0)
        if half_fixed_site(c):
            uc = Decimal(-297.54)
        acc = Account.objects.get(id=c.account_id)
        assert_eq(round(expected, 2), round(acc.accountBalance, 2))
        assert_eq(round(expected_clear, 2), round(acc.clearing_bal, 2))
        assert_eq(round(Decimal(expected_uncol) + uc, 2), round(acc.uncollected_bal, 2))
        assert_eq(round(expected_recon, 2), round(acc.reconnect_bal, 2))
        update_mth_balances()
        acc = Account.objects.get(id=c.account_id)
        assert_eq(round(expected, 2), round(acc.accountBalance, 2))
        assert_eq(round(expected_clear, 2), round(acc.clearing_bal, 2))
        assert_eq(round(Decimal(expected_uncol) + uc, 2), round(acc.uncollected_bal, 2))
        assert_eq(round(expected_recon, 2), round(acc.reconnect_bal, 2))

        acc.uncollected_bal = -100
        acc.save()
        c = Customer.objects.get(id=c.id)
        first_of_month_processing(c)
        acc = Account.objects.get(id=c.account_id)
        assert_eq(round(expected, 2), round(acc.accountBalance, 2))
        assert_eq(round(expected_clear, 2), round(acc.clearing_bal, 2))
        assert_eq(round(uc, 2), round(acc.uncollected_bal, 2))
        assert_eq(round(expected_recon - 100, 2), round(acc.reconnect_bal, 2))
        update_mth_balances()
        acc = Account.objects.get(id=c.account_id)
        assert_eq(round(expected, 2), round(acc.accountBalance, 2))
        assert_eq(round(expected_clear, 2), round(acc.clearing_bal, 2))
        assert_eq(round(uc, 2), round(acc.uncollected_bal, 2))
        assert_eq(round(expected_recon - 100, 2), round(acc.reconnect_bal, 2))


@ddt
class SmsDateProcessingTestCase(base_test.BaseTestOperation):
    def setUp(self):
        """ Setup"""
        super(SmsDateProcessingTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/loan_config.json', verbosity=0)
        MasterTransactionHistory.objects.all().delete()
        Customer.objects.get(id=2).delete()
        Customer.objects.get(id=603).delete()
        Customer.objects.get(id=602).delete()
        m = Municipality.objects.get(id=1)
        m.name = PILOT_GUINEA_PIGS[0]
        m.save()
        for c in Customer.objects.all():
            if c.id == 601:
                c.municipality_id = 1
            else:
                c.municipality_id = 2
            c.save()

    data1 = [
        (100, -10, 0, 0, 100, -10, 0, 0, 31),
        (100, -10, -20, 0, 100, -10, 0, -20, 31),
        (100, -10, -222, -111, 100, -10, 0, -333, 31),
        (0, -10, 0, 0, 0, -10, 0, 0, 31),
        (0, -10, -20, 0, 0, -10, 0, -20, 31),
        (0, -10, -222, -111, 0, -10, 0, -333, 31),

        (100, -10, 0, 0, 100, -10, 0, 0, 30),
        (100, -10, -20, 0, 100, -10, 0, -20, 30),
        (100, -10, -222, -111, 100, -10, 0, -333, 30),
        (0, -10, 0, 0, 0, -10, 0, 0, 30),
        (0, -10, -20, 0, 0, -10, 0, -20, 30),
        (0, -10, -222, -111, 0, -10, 0, -333, 30),

        (100, -10, 0, 0, 100, -10, 0, 0, 28),
        (100, -10, -20, 0, 100, -10, 0, -20, 28),
        (100, -10, -222, -111, 100, -10, 0, -333, 28),
        (0, -10, 0, 0, 0, -10, 0, 0, 28),
        (0, -10, -20, 0, 0, -10, 0, -20, 28),
        (0, -10, -222, -111, 0, -10, 0, -333, 28),
    ]

    @idata(data1)
    @unpack
    def sms_date_processing_test(self, starting_bal, clearing, uncollected, reconnect,
                                 expected, expected_clear, expected_uncol, expected_recon, days_in_mnth):
        """
        Send out SMS stating what will be owed at the end of the month
        Could be called anytime of the month, but meant to be called on SMS_DATE
        :param self:
        :param starting_bal:
        :param clearing:
        :param uncollected:
        :param reconnect:
        :param expected:
        :param expected_clear:
        :param expected_uncol:
        :param expected_recon:
        :param days_in_mnth:
        :return:
        """

        fixed = FIXED_UNCOLLECTED
        c = Customer.objects.get(id=601)
        c.account.accountBalance = Decimal(0.0)
        c.account.clearing_bal = clearing
        c.account.uncollected_bal = uncollected
        c.account.reconnect_bal = reconnect
        c.account.save()
        transaction_manager.TransactionManager. \
            process_mobile_payment(c.account, ('xx' + str(c.id)), starting_bal, str_to_dt('2016-03-02T01:01:00Z'))

        uc = Decimal(0.0)
        if half_fixed_site(c):
            fixed = GUINEA_PIG_FIXED_UNCOLLECTED
            uc = Decimal(-297.54)
        update_mth_balances()

        first_of_month_processing(c)
        res = sms_date_processing(c, fixed, days_in_mnth, True)
        amt_due = abs(c.account.uncollected_bal) + Decimal(fixed * (days_in_mnth - SMS_DATE))
        if res[0] is not None:
            assert_eq(SMS_TEMPLATE % round(amt_due, 2), res[0].message)
        acc = Account.objects.get(id=c.account_id)
        assert_eq(round(expected, 2), round(acc.accountBalance, 2))
        assert_eq(round(expected_clear, 2), round(acc.clearing_bal, 2))

        assert_eq(round(Decimal(expected_uncol) + uc, 2), round(acc.uncollected_bal, 2))
        assert_eq(round(expected_recon, 2), round(acc.reconnect_bal, 2))
        update_mth_balances()
        res = sms_date_processing(c, fixed, days_in_mnth, True)
        amt_due = abs(c.account.uncollected_bal) + Decimal(fixed * (days_in_mnth - SMS_DATE))
        if res[0] is not None:
            assert_eq(SMS_TEMPLATE % round(amt_due, 2), res[0].message)
        acc = Account.objects.get(id=c.account_id)
        assert_eq(round(expected, 2), round(acc.accountBalance, 2))
        assert_eq(round(expected_clear, 2), round(acc.clearing_bal, 2))
        assert_eq(round(Decimal(expected_uncol) + uc, 2), round(acc.uncollected_bal, 2))
        assert_eq(round(expected_recon, 2), round(acc.reconnect_bal, 2))
