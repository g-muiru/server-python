# coding=utf-8
"""Tests for date util classes."""
from django import test
import datetime
from ph_util import date_util
import ph_model.models as ph_model
import mock
import pytz


def mock_utc_midnight():
    """
    mock midnight 2015-01-01 UTC
    :return:
    """
    return datetime.datetime(year=2015, month=1, day=1, tzinfo=pytz.utc)


class DateUtilTest(test.TestCase):
    """Tests date_time_util"""

    def test_to_epoch(self):
        """Tests ph_util.date_util.to_epoch"""
        some_day = datetime.datetime(
            month=3, day=17, year=2015, hour=1, minute=1, second=1)
        expected_epoch = 1426554061
        self.assertEquals(expected_epoch, date_util.to_epoch(some_day))


class DateUtilTestMidNightPassed(test.TestCase):
    """Tests date_time_util"""

    @staticmethod
    def _mid_night_passed(date_time, time_zone, range_in_minutes):
        date_time = ph_model.fields.DateTimeUTC().to_python(date_time)
        return date_util.mid_night_just_passed_in_range(time_zone, range_in_minutes=range_in_minutes, date_time=date_time)[1]

    @mock.patch("ph_util.date_util.get_utc_now", mock_utc_midnight)
    def test_unspecified_datetime(self):
        self.assertTrue(self._mid_night_passed(date_time=None, time_zone='UTC', range_in_minutes=0))

    def test_mid_night_just_passed_in_range(self):
        self.assertTrue(self._mid_night_passed(date_time="2014-07-21T00:00:00Z", time_zone='UTC', range_in_minutes=0))
        self.assertTrue(self._mid_night_passed(date_time="2014-07-21T00:01:00Z", time_zone='UTC', range_in_minutes=1))
        self.assertFalse(self._mid_night_passed(date_time="2014-07-21T00:02:00Z", time_zone='UTC', range_in_minutes=1))

        # Kenya timezone with +3 hours
        self.assertTrue(self._mid_night_passed(date_time="2014-07-20T21:00:00Z", time_zone='Africa/Nairobi', range_in_minutes=0))
        self.assertTrue(self._mid_night_passed(date_time="2014-07-20T21:01:00Z", time_zone='Africa/Nairobi', range_in_minutes=1))
        self.assertFalse(self._mid_night_passed(date_time="2014-07-20T21:02:00Z", time_zone='Africa/Nairobi', range_in_minutes=1))
