# coding=utf-8
"""Test the loan and Db utils"""
from decimal import Decimal

from ddt import ddt, unpack, idata
from django.core.management import call_command

# noinspection PyProtectedMember
from ph_model.models import Account, Usage, Customer, Circuit
from ph_model.models.circuit import SwitchEnabledStatus
from ph_model.models.transaction import save_usage_charge_to_master_transaction_accounting, update_mth_balances, UsageCharge, MasterTransactionHistory
from ph_operation import tariff_manager
from ph_operation.tests import base_test
from ph_util.accounting_util import LoanType, add_vat
from ph_util.date_util import str_to_dt
from ph_util.db_utils import process_materialized_views, refresh_materialized_views, assess_daily_fee_grid
from ph_util.loan_utils import make_notes


# noinspection PyArgumentEqualDefault
@ddt
class LoanTest(base_test.BaseTestOperation):
    def setUp(self):
        """Create especial customer and circuit or related usage objects for the dynamic usage test from file."""
        super(LoanTest, self).setUp()
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_operation/tests/fixtures/usage_config.json', verbosity=0)

    def test_process_materialized_views(self):
        cnt, length = process_materialized_views(data=False)
        # TODO either bring the "circuit_monitors_queen" materialized view back into the migrations (it was removed by better view)
        # TODO or delete it from tests and cron job --- there are a few Looks that use it.
        self.assertEquals(cnt + 1, length)

    def test_make_notes(self):
        notes = make_notes("Start of notes", LoanType.KUKU_POA_NO_VAT_LOAN.name, 111.11, 44444, 3.33, str_to_dt('2017-10-10'), 5)
        self.assertTrue('NO VAT' in notes)
        self.assertTrue('Payments every  5  days' in notes)
        notes = make_notes("Start of notes", LoanType.KUKU_POA_NO_VAT_LOAN.name, 111.11, 44444, 6.66, str_to_dt('2017-10-10'), 1)
        self.assertTrue('NO VAT' in notes)
        self.assertTrue('Daily payments of' in notes)

    data = (
        (['REFRESH MATERIALIZED VIEW active_accounts',
          'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days',
          'REFRESH MATERIALIZED VIEW circuit_monitors_mv',
          'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days',
          'REFRESH MATERIALIZED VIEW does_not_exist'], True, 4),
        (['REFRESH MATERIALIZED VIEW active_accounts',
          'REFRESH MATERIALIZED VIEW circuit_monitors_mv',
          'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days',
          'REFRESH MATERIALIZED VIEW does_not_exist',
          'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days'], True, 3),
        (['REFRESH MATERIALIZED VIEW does_not_exist',
          'REFRESH MATERIALIZED VIEW active_accounts',
          'REFRESH MATERIALIZED VIEW circuit_monitors_mv',
          'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days',
          'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days'], True, 0),
        (['REFRESH MATERIALIZED VIEW active_accounts',
          'REFRESH MATERIALIZED VIEW circuit_monitors_mv',
          'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days',
          'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days',
          'REFRESH MATERIALIZED VIEW does_not_exist'], False, 4),
        (['REFRESH MATERIALIZED VIEW active_accounts',
          'REFRESH MATERIALIZED VIEW circuit_monitors_mv',
          'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days',
          'REFRESH MATERIALIZED VIEW does_not_exist',
          'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days'], False, 3),
        (['REFRESH MATERIALIZED VIEW does_not_exist',
          'REFRESH MATERIALIZED VIEW active_accounts',
          'REFRESH MATERIALIZED VIEW circuit_monitors_mv',
          'REFRESH MATERIALIZED VIEW usage_raw_March_2017_days',
          'REFRESH MATERIALIZED VIEW cloverfield_circuit_monitor_gaps_30_days'], False, 0)
    )

    @idata(data)
    @unpack
    def test_process_all_materialized_views(self, sql_array, wd, expected_cnt):
        cnt, length = refresh_materialized_views(sql_array, with_data=wd)
        self.assertEquals(cnt, expected_cnt)
        self.assertEquals(len(sql_array), length)

    # noinspection PyUnreachableCode,PyUnusedLocal,PyUnusedLocal
    def test_process_usages_by_circuits(self):
        """Clear all existing usages and test usage processed by list of circuits."""
        UsageCharge.objects.all().delete()
        MasterTransactionHistory.objects.all().delete()
        Usage.objects.all().delete()

        for a in Account.objects.all():
            a.accountBalance = 100.0
            a.save()

        self.assertEqual(0, UsageCharge.objects.count())
        cust = Customer.objects.get(pk=100)
        circuit = Circuit.objects.get(pk=100)

        circuit.queen.grid.operational_date = str_to_dt('2001-01-01').date()
        circuit.queen.grid.save()
        circuit.queen.operational_date = str_to_dt('2001-01-01').date()
        circuit.queen.save()
        circuit.switchEnabled = SwitchEnabledStatus.ENABLED.value
        circuit.save()
        cust.tariff_id = 12
        cust.save()
        Usage(customer_id=100, circuit_id=100, collectTime=str_to_dt('2016-01-02T01:01:00Z'), intervalWh=10).save()
        pu, uc, duc = tariff_manager.TariffManager.process_usages_by_circuits([100])
        self.assertTrue(len(pu) == 1)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 0)
        uc = UsageCharge.objects.all()[0]
        self.assertTrue(MasterTransactionHistory.objects.count() == 0)
        self.assertTrue(uc.perSegmentChargeComponent != 0)
        save_usage_charge_to_master_transaction_accounting(uc)

        Usage(customer_id=100, circuit_id=100, collectTime=str_to_dt('2016-01-02T21:01:00Z'), intervalWh=10).save()
        Usage(customer_id=100, circuit_id=100, collectTime=str_to_dt('2016-01-02T21:01:00Z'), intervalWh=10).save()
        Usage(customer_id=100, circuit_id=100, collectTime=str_to_dt('2016-01-02T21:01:00Z'), intervalWh=10).save()
        Usage(customer_id=100, circuit_id=100, collectTime=str_to_dt('2016-01-02T21:01:00Z'), intervalWh=10).save()
        MasterTransactionHistory.objects.all().delete()
        pu, uc, duc = tariff_manager.TariffManager.process_usages_by_circuits([100])
        self.assertTrue(len(pu) == 4)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 3)
        uc = UsageCharge.objects.all()[0]
        self.assertTrue(MasterTransactionHistory.objects.count() == 0)
        self.assertTrue(uc.perSegmentChargeComponent != 0)
        save_usage_charge_to_master_transaction_accounting(uc)
        self.assertEqual(2, MasterTransactionHistory.objects.count())
        MasterTransactionHistory.objects.all().delete()
        # TODO make up some test data --- circuit monitors that include / exclude daily fees
        # this deducts the fixed fees
        assess_daily_fee_grid(divisor=1, test=0, dt=str_to_dt('2017-10-10').date())
        update_mth_balances([1], trans_diff=0.0, project='Cloverfield', row_limit=1000, bal_diff=0.00)
        update_mth_balances([2], trans_diff=0.0, project='Cloverfield', row_limit=1000, bal_diff=0.00)
        update_mth_balances([100], trans_diff=0.0, project='Cloverfield', row_limit=1000, bal_diff=0.00)

        # TODO why does the order of daily fees change ?????
        self.assertEquals(6, MasterTransactionHistory.objects.count())
        acc = acc_bal = uncol = mth_bal = 0
        for mth in MasterTransactionHistory.objects.all().order_by('id'):
            acc = Account.objects.get(id=mth.account_id)
            acc_bal = round(Decimal(acc.accountBalance), 4)
            uncol = round(Decimal(acc.uncollected_bal), 4)
            mth_bal = round(Decimal(mth.accountBalance), 4)

        self.assertTrue(abs(mth_bal - acc_bal) < 0.05 or abs(acc_bal - round(add_vat(mth_bal), 4)) < 0.05)
        self.assertEqual(uncol, -19.836)

        # nothing should change
        # this does not deduct the fixed fees
        assess_daily_fee_grid(divisor=1, test=1, dt=str_to_dt('2017-10-10'))
        update_mth_balances([1], trans_diff=0.0, project='Cloverfield', row_limit=1000, bal_diff=0.00)
        update_mth_balances([2], trans_diff=0.0, project='Cloverfield', row_limit=1000, bal_diff=0.00)
        update_mth_balances([100], trans_diff=0.0, project='Cloverfield', row_limit=1000, bal_diff=0.00)
        self.assertEquals(12, MasterTransactionHistory.objects.count())

        for a in Account.objects.all():
            bal = round(a.accountBalance, 2)
            uncol = round(a.uncollected_bal, 2)
