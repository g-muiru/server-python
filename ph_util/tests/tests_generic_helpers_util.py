# coding=utf-8
"""Tests for helper functions."""

from django.test import SimpleTestCase

from ph_util.generic_helpers import OSinfo, have_internet


class SystemRequirementsTest(SimpleTestCase):
    """Tests loading requirements.txt -- requires internet connection if requirements are not already loaded"""

    def test_sys_req(self):
        if not have_internet():
            print("\nNo internet, test_sys_req will not run")
        else:
            self.assertEquals(0, OSinfo('pip install -r requirements.txt'))


class TestRequirementsTest(SimpleTestCase):
    """Tests loading testreq.txt  -- requires internet connection if requirements are not already loaded"""

    def test_test_req(self):
        if not have_internet():
            print("\nNo internet, test_test_req will not run")
        else:
            self.assertEquals(0, OSinfo('pip install -r testreq.txt'))
