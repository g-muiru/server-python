"""Tests for mixin classes."""
from django import test
import enum

from ph_util import mixins

class PairEnumMixinsTest(test.TestCase):
    """Tests mixins.PairEnumMixin"""

    class Color( mixins.PairEnumMixin, enum.Enum):
        BLACK= 1
        RED = 2
        ORANGE = 3

    def setUp(self):
        self.color = PairEnumMixinsTest.Color

    def test_get_values(self):
        """Tests ph_util.mixins.PairEnumMixins.get_values()."""
        values = [1,2,3]
        self.assertEquals(self.color.get_values(), values)

    def test_get_keys(self):
        """Tests ph_util.mixins.PairEnumMixins.get_keys()."""
        keys = ['BLACK', 'RED', 'ORANGE']
        self.assertEquals(self.color.get_keys(), keys)

    def test_is_valid_key(self):
        """Tests ph_util.mixins.PairEnumMixins.is_valid_key()."""
        validKey = 'BLACK'
        invalidKey =  'CAR'
        self.assertTrue(self.color.is_valid_key(validKey))
        self.assertFalse(self.color.is_valid_key(invalidKey))

    def test_value_of(self):
        """Tests ph_util.mixins.PairEnumMixins.value_of()."""
        key, value = 'RED', 2
        self.assertEquals(self.color.value_of(key), value)

    def test_value_of_key_insensitive(self):
        """Tests ph_util.mixins.PairEnumMixins.value_of()."""
        key, value = 'red', 2
        self.assertEquals(self.color.value_of_case_insensitive(key), value)

    def test_get_keys_map(self):
        """Tests ph_util.mixins.PairEnumMixins.get_keys_map()."""
        expected = [('BLACK', 1), ('RED', 2), ('ORANGE', 3)]
        self.assertEquals(self.color.get_keys_map(), expected)


    def test_get_values_map(self):
        """Tests ph_util.mixins.PairEnumMixins.get_values_map()."""
        expected = [(1, 'BLACK'), (2, 'RED'), (3, 'ORANGE')]
        self.assertEquals(self.color.get_values_map(), expected)


