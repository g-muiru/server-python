import phonenumbers


def is_valid_e164_format(phone):
    try:
      phone = phonenumbers.parse(phone)
      return phonenumbers.is_valid_number(phone)
    except Exception:
      return False

def get_e164_format(phone, countryCode=None):
    if len(phone) > 10 and phone[0] != '+':
        phone = '+' + phone
    if is_valid_e164_format(phone):
        return phonenumbers.format_number(phonenumbers.parse(phone), phonenumbers.PhoneNumberFormat.E164)
    if countryCode and not countryCode.startswith('+'):
        countryCode = '+' + countryCode
    if phone.startswith('0'):
        phone = phone[1:]
    if countryCode:
        phone = countryCode + phone
    try:
      phone = phonenumbers.parse(phone)
    except Exception:
        return
    return phonenumbers.format_number(phone, phonenumbers.PhoneNumberFormat.E164)

