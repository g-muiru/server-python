# coding=utf-8
""" Date time related utils"""
import datetime
import calendar

import delorean
from delorean import parse

DEFAULT_TIME_ZONE = "Africa/Nairobi"
DELTA_365_DAYS = datetime.timedelta(days=365)
DELTA_30_DAYS = datetime.timedelta(days=30)

def to_utc_datetime(epoch_time):
    """

    :param epoch_time:
    :return:
    """
    dt = datetime.datetime.utcfromtimestamp(epoch_time)
    return dt


def to_epoch(actual_date_time):
    """

    :param actual_date_time:
    :return:
    """
    return calendar.timegm(actual_date_time.timetuple())


def get_utc_now():
    """

    :return:
    """
    return delorean.Delorean(datetime=datetime.datetime.utcnow(), timezone='UTC').datetime


def mid_night_just_passed_in_range(time_zone, range_in_minutes, date_time=None):
    """
    Returns true if midnight of @datetime in @timezone within a range.
    :param time_zone:            timezone string
    :param range_in_minutes:    number of minutes
    :param date_time:           date time object
    :return: tuple of date and boolean
    """
    date_time = date_time or get_utc_now()
    if not date_time.tzinfo:
        d = delorean.Delorean(datetime=date_time, timezone='UTC')
    else:
        d = delorean.Delorean(datetime=date_time)
    time_zone_date_time = d.shift(time_zone).datetime
    return time_zone_date_time, time_zone_date_time.hour == 0 and 0 <= time_zone_date_time.minute <= range_in_minutes


def str_to_dt(dt_str, day_first=False, year_first=True, delorean=False):
    """
    You know what this does, expects YYYY MM DD format
    :param dt_str:      The string to convert
    :param delorean:    Boolean for return type
    :return:            datetime unless delorean is True
    """
    dt = parse(dt_str, dayfirst=day_first, yearfirst=year_first)
    if delorean is True:
        return dt
    return dt.datetime
