# coding=utf-8
"""Mixin utilities."""


class PairEnumMixin(object):
    """Enumeration mixin, backported from python 3.3."""

    @classmethod
    def get_values(cls):
        """Get all enumeration values."""
        return [p.value for p in cls.__members__.values()]

    @classmethod
    def get_keys(cls):
        """Get all enumeration keys."""
        return cls.__members__.keys()

    @classmethod
    def is_valid_key(cls, key):
        """check if the key is registered in the enumeration."""
        return key in cls.get_keys()

    @classmethod
    def value_of(cls, key):
        """

        :param key:
        :return:
        """
        paired_value = cls.__members__.get(key, None)
        if paired_value:
            return paired_value.value

    @classmethod
    def value_of_case_insensitive(cls, key):
        """

        :param key:
        :return:
        """
        if not key:
            return
        match = None
        for k in cls.get_keys():

            if key.upper() == k.upper():
                match = k
                break
        paired_value = cls.__members__.get(match, None)
        if paired_value:
            return paired_value.value

    @classmethod
    def get_keys_map(cls):
        """Returns list of tuples with key value pair."""
        return [(enumObject.name, enumObject.value) for enumObject in cls.__members__.values()]

    @classmethod
    def get_values_map(cls):
        """Returns list of tuples with value key pair, reverse of @get_keys_map."""
        return [(enumObject.value, enumObject.name) for enumObject in cls.__members__.values()]
