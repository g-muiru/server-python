# coding=utf-8
""" Utilities related to testing"""
from decimal import Decimal, ROUND_UP

from delorean import utcnow
from django.db import connection

from powerhive.common_settings import BASE_DIR


def test_mode(db_name = connection.settings_dict['NAME']):
    """
    Determines if we are in test mode

    :param db_name: hand in a db name if you want
    :return: true if not in test mode
    """

    return (('sqlite' in connection.vendor) or ('test' in db_name))


def assert_eq(expect, actual):
    """
    Return meaningful info on failure

    :param expect:  The expected value
    :param actual:  The actual value
    :return:        true if they are equal
    """
    assert expect == actual, "\n*Expected:\n%s\n*Actual:\n%s" % (expect, actual)


def get_sql_from_files(path):
    """
    Concatenates all the SQL for creating functions in the DB
    NOTE: this is Postgres specific code
    :return: string of SQL
    """
    return concat_lines(BASE_DIR + path + '/*.*')


def concat_lines(path):
    """
    Concat lines in all files in a dir
    :param path:    Full path to directory
    :return:        string
    """
    s = ''

    import glob
    files = sorted(glob.glob(path))
    for file in files:
        f = open(file, 'r')
        for l in f:
            s += l
        f.close()
    return s


DECI_PLACES = Decimal('0.000')


def quant(val, deci_place=DECI_PLACES, rounding=ROUND_UP):
    """
    Quantization helper
    :param val:
    :param deci_place:
    :param rounding:
    :return:
    """
    res = val.quantize(deci_place, rounding)
    return res.quantize(Decimal('0.00'), rounding=None)