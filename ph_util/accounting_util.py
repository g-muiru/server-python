# coding=utf-8
"""Some useful utilities"""

from decimal import Decimal, ROUND_FLOOR
from django.conf import settings
from enum import Enum

from ph_util import mixins
from ph_util.date_util import str_to_dt

# TODO many of these constants should be in the DB
# Currently many of these values come from https://docs.google.com/document/d/13smT9pB4UJD5JFG2x8mvPoEs93HpQnqLyFUhOBTUSKc/edit?ts=5877c731


class PaymentEnums(mixins.PairEnumMixin, Enum):
    """Enums to identify payments"""
    ACCOUNT = 'ACCOUNT'
    UNCOLLECTED = 'UNCOLLECTED',
    CLEARING = 'CLEARING',
    RECONNECT = 'RECONNECT'

# This is where bad loans go, and no more payments are made on them
DEAD_LOAN_ACCOUNT = 42048

LOW_BALANCE_WARNING_AMOUNT = 150
DEFAULT_SMS_LOW_BALANCE_DATE_STR = '1900-01-01'

# these are special cases where we do not charge VAT
MUNIS_NO_VAT = ('Kirwa site C', 'Kirwa site E', 'Rigena site I', '97', '260 A', '265', '191', '185', '179')
NO_VAT_REASON = 'Welcome gift'

KUKU_POA_SUBSCRIPTION_5_AMT = 792
KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT = 660
KUKU_POA_SUBSCRIPTION_5_DAYS = 30
RESIDENTIAL_CONN_LOAN_AMT = 3000
COMMERCIAL_CONN_LOAN_AMT = 5000
CONN_LOAN_DEPOSIT_AMT = 1000
CONN_LOAN_FEE_AMT = 500
CONN_LOAN_DAYS = 730

COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT = Decimal(774)
COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT = Decimal(1027)
COMMERCIAL_FIXED_TARIFF_START_DAY = 3

BONUS_PWR_ADJ_TYPE = 'BONUS_PWR'
CREDIT_REFUND_ADJ_TYPE = 'REFUND'

DAY_TARIFF_KWH_RATE = Decimal(44.1)
NIGHT_TARIFF_KWH_RATE = Decimal(63.3)
RESIDENTIAL_TARIFF_DAILY_FIXED_AMT = Decimal(17.1)
KENYA_VAT_RATE = Decimal(.16)
partial_mnth_divisor = Decimal(30)

COMMERCIAL_I_BONUS_POWER_AMT = Decimal((COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT / partial_mnth_divisor) + NIGHT_TARIFF_KWH_RATE)
COMMERCIAL_II_BONUS_POWER_AMT = Decimal((COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT / partial_mnth_divisor) + NIGHT_TARIFF_KWH_RATE)
RESIDENTIAL_BONUS_POWER_AMT = Decimal(RESIDENTIAL_TARIFF_DAILY_FIXED_AMT + NIGHT_TARIFF_KWH_RATE)

FAR_FUTURE_DATE = str_to_dt('2222-02-02').date()


class AccountingEnums(mixins.PairEnumMixin, Enum):
    """Enums to identify loan items"""
    COM1_FIXED_FEEc231 = 231,
    COM2_FIXED_FEEc232 = 232,
    RES_FIXED_FEEc236 = 236,
    COM1_VAR_FEEc233 = 233,
    COM2_VAR_FEEc234 = 234,
    RES_VAR_FEEc238 = 238,
    CRED_ADJ_FEEc248 = 248,
    LOAN_CONN_FEE_INITc235 = 235,
    LOAN_APPLIANCE_INITc278 = 278,
    LOAN_FINANCIAL_INITc279 = 279,
    LOAN_OTHER_INITc280 = 280,
    LOAN_PAYMENTc600 = 600,
    VAT_LOAN_PAYMENTc601 = 601,
    INTEREST_LOAN_PAYMENTc602 = 602,
    VAT_INTEREST_LOAN_PAYMENTc603 = 603,
    VAT_TARIFF_FIXEDc346 = 346,
    VAT_TARIFF_VARIABLEc345 = 345,
    VAT_LOAN_INITc301 = 301,
    LOAN_KUKU_POA_INITc369 = 369,
    LOAN_INTERNET_INITc370 = 370


class LoanType(mixins.PairEnumMixin, Enum):
    """ Types of loans"""
    CONNECTION_FEE_LOAN = 'CONNECTION_FEE_LOAN'
    APPLIANCE_LOAN = 'APPLIANCE_LOAN'
    FINANCIAL_ASSISTANCE_LOAN = 'FINANCIAL_ASSISTANCE_LOAN'
    OTHER_LOAN = 'OTHER_LOAN',
    COMMERCIAL_FIXED_TARIFF_LOAN = 'COMMERCIAL_FIXED_TARIFF_LOAN'
    KUKU_POA_NO_VAT_LOAN = 'KUKU_POA_PAYMENT_PLAN'
    INTERNET_PAYMENT_PLAN = 'INTERNET_PAYMENT_PLAN'


class LoanRepayment(mixins.PairEnumMixin, Enum):
    """ fixed loan repayment schedules """
    DAILY = 'DAILY'
    WEEKLY = 'WEEKLY'
    FIRST_AND_FIFTEENTH = 'FIRST_AND_FIFTEENTH'
    FIFTEENTH_AND_LAST = 'FIFTEENTH_AND_LAST'
    FIRST_OF_THE_MONTH = 'FIRST_OF_THE_MONTH'
    LAST_OF_THE_MONTH = 'LAST_OF_THE_MONTH'


def init_loan_type_to_accounting_code(src, vat=False):
    """
    Maps loan type enum to accounting code enum
    :param src: Loan type enum
    :param vat: if true return VAT accounting code enum no matter what src is
    :return: Acounting code enum
    """

    # Special handling for monthly commercial fixed fee
    if type(src) is AccountingEnums:
        if vat:
            return AccountingEnums.VAT_TARIFF_FIXEDc346
        else:
            return src

    if vat:
        return AccountingEnums.VAT_LOAN_INITc301

    if type(src) is LoanType:
        src = src.name

    if src == LoanType.CONNECTION_FEE_LOAN.name:
        return AccountingEnums.LOAN_CONN_FEE_INITc235
    if src == LoanType.APPLIANCE_LOAN.name:
        return AccountingEnums.LOAN_APPLIANCE_INITc278
    if src == LoanType.FINANCIAL_ASSISTANCE_LOAN.name:
        return AccountingEnums.LOAN_FINANCIAL_INITc279
    if src == LoanType.KUKU_POA_NO_VAT_LOAN.name:
        return AccountingEnums.LOAN_KUKU_POA_INITc369
    if src == LoanType.INTERNET_PAYMENT_PLAN.name:
        return AccountingEnums.LOAN_INTERNET_INITc370

    return AccountingEnums.LOAN_OTHER_INITc280


def add_vat(val):
    """

    :param val: Decimal Float or integer
    :return: val if val is not numeric
    """

    if type(val) in (Decimal, float, int):
        return Decimal(Decimal(val) * Decimal(1 + settings.VAT_MULTIPLIER)).quantize(Decimal('0.00'), rounding=ROUND_FLOOR)
    return val


def calc_vat(val):
    """

    :param val: Decimal Float or integer
    :return: val if val is not numeric
    """

    if type(val) in (Decimal, float, int):
        return Decimal(val) * settings.VAT_MULTIPLIER
    return val


def str_obj(obj):  # pragma: no cover
    """
    display all the attributes of an object
    :param obj:
    :return:
    """
    if not hasattr(obj, '__dict__'):
        return str(obj)
    sb = []
    for key in obj.__dict__:
        sb.append("{key}='{value}'".format(key=key, value=obj.__dict__[key]))

    return '\nStart obj\nname=' + obj.__class__.__name__ + '\n' + '\n'.join(sb) + '\nEnd obj\n'


# TODO exception handling
def deci_or_float(value, do_float=False):  # pragma: no cover
    """
    Numeric converter, assumes you pass in a number
    :param value:       value to convert
    :param do_float:     if true return float
    :return:            decimal or float
    """
    value = str(value)
    if do_float:
        return float(value)
    return Decimal(value)


def half_fixed_site(cust):
    """
    Check for sites where we only collect 1/2 the fixed daily fee
    :param cust:    The customer of the site
    :return:        True if site collects 1/2 the fixed daily fee
    """
    if cust is None:
        return False
    return '-CF' in cust.municipality.name
