# coding=utf-8
""" code that will probably get thrown away """
import logging
import calendar
from datetime import datetime
from decimal import Decimal
from delorean import utcnow
from ph_model.models import Customer
from ph_model.models.transaction import MasterTransactionHistory
from ph_operation import sms_manager
from ph_util.accounting_util import half_fixed_site
from ph_util.date_util import str_to_dt

# TODO: bad magic numbers
CLEARING_DUE_DATE = '2019-04-01'
# TODO: if tariff fixed amount changes these have to change
FIXED_UNCOLLECTED = Decimal(17.1 * 1.16)
GUINEA_PIG_FIXED_UNCOLLECTED = Decimal(FIXED_UNCOLLECTED / 2)

PILOT_GUINEA_PIGS = ['Baranne-CF', 'Nyamondo-CF', 'Matangamano-CF']

SMS_DATE = 25
ZERO = Decimal(0.0)

SMS_TEMPLATE = \
    'Friendly reminder: To keep using stima, you must pay KShs %s via M-Pesa (829250) by the 1st of the month. Questions? Call 0722 999922'

LOGGER = logging.getLogger(__name__)


# TODO: NOT USED for now, clearing balance is not dealt with yet
def calc_due(acc, mnths):
    """

    :param acc:     Customer account
    :param mnths:   # of months till CLEARING_DUE_DATE
    :return:
    """
    amt_due = ZERO
    clearing = ZERO
    if mnths < 0 or mnths > 12:
        return None, None

    if mnths > 0:
        if acc.clearing_bal < 0:
            clearing = Decimal(abs(acc.clearing_bal / mnths))
            amt_due = clearing
    amt_due += Decimal(abs(acc.uncollected_bal))
    return amt_due, clearing


def first_of_month_processing(c, days_in_month=30):
    """

    :param c:
    :param days_in_month:
    :return:
    """

    changed = False
    amt_due = Decimal(c.account.uncollected_bal)
    # print(c.account_id, round(c.account.clearing_bal, 2), round(c.account.uncollected_bal, 2), round(c.account.reconnect_bal, 2), round(amt_due, 2))
    # they owe $$$ so move past due balances to reconnect and zero uncollected
    if amt_due < 0:
        c.account.reconnect_bal += amt_due
        c.account.uncollected_bal = ZERO
        changed = True
    if half_fixed_site(c):
        c.account.uncollected_bal -= GUINEA_PIG_FIXED_UNCOLLECTED * days_in_month
        changed = True
    if changed:
        # TODO: there maybe a race condition here depending on the processed flag
        mth = MasterTransactionHistory.objects.filter(account_id=c.account_id).last()
        if mth is not None:
            mth.reconnect_bal = c.account.reconnect_bal
            mth.uncollected_bal = ZERO
            mth.save()
        c.account.save()
    return c.account.reconnect_bal, c.account.uncollected_bal, amt_due


def sms_date_processing(c, fixed, days_in_mnth, no_send=False):
    """

    :param c:
    :param fixed:
    :param days_in_mnth:
    :param no_send:
    :return:
    """

    # uncollected + remaining daily fees
    amt_due = abs(c.account.uncollected_bal) + Decimal(fixed * (days_in_mnth - SMS_DATE))
    # print(c.account_id, round(c.account.clearing_bal, 2), round(c.account.uncollected_bal, 2), round(c.account.reconnect_bal, 2), round(amt_due, 2))
    message = SMS_TEMPLATE % round(amt_due, 2)
    LOGGER.info('sending message %s to %s   %s' % (message, c.getName(), str(c.account_id)))
    return sms_manager.CustomerSMSManager(c.id, message).send(no_send)


def process_uncollected(dt_param=utcnow(), no_send=False):
    """
    On the SMS_DATE of the month send sms to customers with their uncollected balance due
    This is an approximate amount due the end of the month as paid fixed fees and Loan payments are not considered

    On the 1st of the month move uncollected to reconnect, and zero out uncollected

    :param dt_param:
    :param no_send:
    :return:
    """

    dt = dt_param
    if type(dt_param) != datetime:
        dt = dt_param.date

    res = []
    if dt.day == 1 or dt.day == SMS_DATE:
        days_in_mnth = calendar.monthrange(dt.year, dt.month)[1]
        due_dt = str_to_dt(CLEARING_DUE_DATE)
        # TODO: will be used if we calc clearing payment amount ( calc_due(acc, mnths) )
        # noinspection PyUnusedLocal
        mnths = due_dt.month - dt.month + (due_dt.year - dt.year) * 12
        for c in Customer.objects.filter(municipality__name__in=PILOT_GUINEA_PIGS) \
                .order_by('municipality__name', 'account_id'):
            if dt.day == 1:
                res.append(first_of_month_processing(c, days_in_mnth))
            else:
                res.append(sms_date_processing(c, GUINEA_PIG_FIXED_UNCOLLECTED, days_in_mnth, no_send))
        for c in Customer.objects.exclude(municipality__name__in=PILOT_GUINEA_PIGS) \
                .order_by('municipality__name', 'account_id'):
            if dt.day == 1:
                res.append(first_of_month_processing(c, days_in_mnth))
            else:
                res.append(sms_date_processing(c, FIXED_UNCOLLECTED, days_in_mnth, no_send))
    return res
