#!/usr/bin/env python
# coding=utf-8
"""manage django startup"""
import os
import sys

DEFAULT_SETTING = 'powerhive.local_settings'

SETTING_ARGS = {
    "--stg": "powerhive.stg_settings",
    "--prod": "powerhive.prod_settings",
    "--local": "powerhive.local_settings",
    "--cron": "powerhive.cron_settings",
    "--cli": "powerhive.cron_cli_settings",
    "--nodb": "powerhive.nodb_settings",
    "--sqlite": "powerhive.sqlite_settings",
    "runserver": "powerhive.local_settings"
}

CUSTOM_ARGS = ('--stg', '--prod', '--cron', '--cli', '--local', '--nodb', '--sqlite')


def get_setting():
    """Gets first occurrence of setting.

    e.g. ./manage.py runfcgi host=127.0.0.1 port=8080 stg will run it in staging env."""
    args = sys.argv
    for arg in args:
        if arg in SETTING_ARGS.keys():
            return SETTING_ARGS[arg]
    return DEFAULT_SETTING


def remove_custom_args():
    """Remove django unknown args from input."""
    for arg in sys.argv:
        if arg in CUSTOM_ARGS:
            sys.argv.remove(arg)


if __name__ == "__main__":
    """Need to pass running envirnoment when running ./manage.py otherwise will use default one.
    E.g ./manage.py syncdb --stg
    """
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", get_setting())
    remove_custom_args()
    from django.core.management import execute_from_command_line

    """
        We have to run coverage from here when using django_nose,
        otherwise the coverage report on our models would not be accurate.
    """
    if 'test' in sys.argv:
        import coverage
        import shutil

        try:
            shutil.rmtree('htmlcov')
        except OSError:
            pass

        cov = coverage.coverage(data_suffix=True)
        cov.start()

        execute_from_command_line(sys.argv)

        cov.stop()
        cov.save()
        # specify -p on commandline to combine coverage data from multiple runs
        if '-p' in sys.argv:
            cov.combine()
        cov.report()
        cov.html_report()
    else:
        execute_from_command_line(sys.argv)
