# coding=utf-8
"""Exception middleware, to return custom JSON error response."""
import logging

from django.conf import settings
from django.contrib import auth
from django.http import HttpResponseRedirect

from ph_common_api import json_response
from ph_om_grid_api.views import make_loan

LOGGER = logging.getLogger(__name__)


def login(request):
    """Redirect to url from query parameters or host."""
    if request.user.is_authenticated():
        if request.GET.get('host'):
            fullUrl = '{}'.format(request.GET.get('host'))
        else:
            fullUrl = '%s://%s' % ('https' if request.is_secure() else 'http',
                                   request.get_host())
        return HttpResponseRedirect(fullUrl)


class OMLoginRequiredMiddleware(object):
    """Login handler middleware for O&M."""

    @staticmethod
    def process_request(request):
        """

        :param request:
        :return:
        """
        usr = request.POST.get('username')
        pw = request.POST.get('password')
        # TODO config or environ vars
        if usr == 'internet' and pw == 'pbkdf2_sha256$30000$vSOIKNuPSjP4$7ARnhy9wnajecQ21JSDWtoY6cd/Yz5tQAuVpWUE4bow=':
            acc_id, cust_id, amount, bal = make_loan(request, None, require_payment=True)
            return json_response.PHJsonHTTPResponse([{'acc_id': acc_id, 'cust_id': cust_id, 'amount': amount, 'bal_after': bal}])
        else:
            request.user = auth.get_user(request)  # don't use SimpleLazyObject since we're assuming real objects in the rest of the codebase :(
            login_path = request.build_absolute_uri(settings.LOGIN_URL)
            if request.path != login_path and not request.user.is_authenticated():
                url = '{}?next=/om/login/'.format(login_path)
                return json_response.PHJsonHTTPResponse({'auth': False, 'url': url})
