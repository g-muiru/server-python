"""Exception middleware, to return custom JSON error response."""

import datetime
import decimal
import logging
import json
from django import  http
from django.conf import settings
import acl


LOGGER = logging.getLogger(__name__)

class _ComplexEncoder(json.JSONEncoder):
    """Json encoder for unsupprted objects."""
    def default(self, o):
        if type(o) is datetime.date or type(o) is datetime.datetime:
           return o.isoformat()
        if type(o) is decimal.Decimal:
           return float(o)
        return json.JSONEncoder.default(self, o)

class PHJsonHTTPResponse(http.HttpResponse):
    """JSON Http Response."""

    def __init__(self, content, content_type='application/json', status=None, isEncoded=False):
        if not isEncoded:
            content=json.dumps(content, cls=_ComplexEncoder),
        super(PHJsonHTTPResponse, self).__init__(
            content=content,
            status=status,
            content_type=content_type,
        )

class JsonErrorResponse(object):
    """Wrapper for Json error response."""

    @classmethod
    def get_response(cls, request, e):
        status = 500
        content = {
               'user':  request.user.username,
               'error': {
                 'message': str(e),
                 'code': 500
            }
        }
        if type(e) == acl.AccessDenied:
           content['error']['code'] = 403
           status = 403
        return PHJsonHTTPResponse(content=content, status=status)


class JsonSuccessPostResponse(object):
    """Wrapper for Json success response."""
    @classmethod
    def get_response(cls, data, detail=False):
        status = 201
        try:
            affectedEntries = 'Success'
            if detail or settings.DEBUG:
               affectedEntries =  data
            content = {
                'code': 201,
                 'message': affectedEntries
            }
            return PHJsonHTTPResponse(content=content, status=status)
        except Exception:
            return PHJsonHTTPResponse(content=data, status=status)


class JsonQueuedResponse(object):
    """Wrapper for Json success/error response queued. Message could possibly contains errors and success."""
    @classmethod
    def get_response(cls, httpResponse, status=207):
        data = [response.content for response in httpResponse]
        content = {
               'messages': data
            }
        return PHJsonHTTPResponse(content=content, status=status)
