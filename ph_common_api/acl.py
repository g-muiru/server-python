"""OM Access control list. Associates permission, user, and hardwares(grid/queen/probe/circuit), or events
   In-memory filtering user's permitted artifacts."""

import enum
import logging
from django.db.models import Q
from django.core import exceptions

import ph_model.models as ph_model
from ph_util import mixins
from ph_operation import event_manager

LOGGER = logging.getLogger(__name__)

EVENTS_DEFAULT_OFFSET = 0
EVENTS_DEFAULT_LIMIT = 20

class AclModels(mixins.PairEnumMixin, enum.Enum):
    """Models with access permission."""
    PROJECT = ph_model.project.Project
    GRID = ph_model.grid.Grid
    QUEEN = ph_model.queen.Queen
    PROBE = ph_model.probe.Probe
    CIRCUIT = ph_model.circuit.Circuit
    POWERSTATION = ph_model.powerstation.PowerStation
    INVERTER = ph_model.inverter.Inverter
    BATTERYBANK = ph_model.battery.BatteryBank
    EVENT = ph_model.event.Event
    EVENT_TYPE = ph_model.event.EventType
    ADJUSTMENT = ph_model.transaction.CreditAdjustmentHistory
    AGGREGATEGRID = ph_model.aggregate.AggregateGrid


class AccessDenied(Exception):
    """User not allowed to access this object."""
    pass


class UnsupportedAclException(Exception):
    """acl not supported in this model."""
    pass

class CustomerWithOutAccountException(Exception):
    """Customer without account exception."""
    pass


def check_exception(func):
    """Wrapps exception."""

    def re_raise(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except exceptions.ObjectDoesNotExist:
            raise AccessDenied('Access denied')

    return re_raise


def check_permission(user, model, deviceId, minRequiredRole):
    """Helper to check permission of @param user against @param minuRequiredRole.

    Raises:
        AccessDenied if minimum required role is not satisfied.
    """
    if not AclModels.is_valid_key(model.upper()):
        raise UnsupportedAclException('acl not supported in this model'.format(str(model)))
    model = AclModels.value_of(model.upper())
    UserAcl(user, model, minRequiredRole).check_permission(deviceId)

class UserAcl(object):

    def __init__(self, user, model, minRequiredRole=ph_model.permission.Role.VIEW.value):
        """"Initializes user acl, returns all use own hardware ids."""
        if not isinstance(user,ph_model.user.User):
            user = ph_model.user.User.objects.get(userInfo = user)
        self.user = user
        self.model = model
        if type(model) == AclModels:
            self.model = model.value
        self.minRequiredRole = minRequiredRole
        self.permissions = ph_model.permission.Permission.objects.filter(user=self.user, role__gte=minRequiredRole)
        self.userGridIds = [p.itemId for p in self.permissions if p.itemType.lower() == 'grid']
        self.userProjectIds = [p.itemId for p in self.permissions if p.itemType.lower() == 'project']
        if self.userProjectIds:
            projectGridIds = [p.id for p in ph_model.grid.Grid.objects.all().filter(project__in=self.userProjectIds)]
            self.userGridIds += projectGridIds

    @property
    def userQueenIds(self):
        userQueens = AclModels.QUEEN.value.objects.all().filter(grid__pk__in=self.userGridIds)
        return [q.pk for q in userQueens]

    @property
    def userCircuitIds(self):
        userCircuits = AclModels.CIRCUIT.value.objects.all().filter(queen__grid__pk__in=self.userGridIds)
        return [q.pk for q in userCircuits]

    @property
    def userPowerstationIds(self):
        userPowerstation = AclModels.POWERSTATION.value.objects.all().filter(grid__pk__in=self.userGridIds)
        return [q.pk for q in userPowerstation]

    @property
    def userInverterIds(self):
        userInverters = AclModels.INVERTER.value.objects.all().filter(powerstation__grid__pk__in=self.userGridIds)
        return [q.pk for q in userInverters]

    @property
    def userBatteryBankIds(self):
        userBatteryBank = AclModels.BATTERYBANK.value.objects.all().filter(powerstation__grid__pk__in=self.userGridIds)
        return [q.pk for q in userBatteryBank]

    @property
    def userCustomerIds(self):
        userCustomers = ph_model.customer.Customer.objects.all().filter(circuit__queen__grid__pk__in=self.userGridIds)
        return [q.pk for q in userCustomers]

    @check_exception
    def get_highest_permission_role(self, id):
        """Returns highest permission role associated to hardware."""
        object = self.model.objects.get(pk=id)
        if self.model == AclModels.GRID.value:
            gridId = object.id
        elif self.model == AclModels.QUEEN.value:
            gridId = object.grid.id
        elif self.model == AclModels.PROBE.value:
            gridId = object.queen.grid.id
        elif self.model == AclModels.CIRCUIT.value:
            gridId = object.queen.grid.id
        elif self.model == AclModels.POWERSTATION.value:
            gridId = object.grid.id
        elif self.model == AclModels.INVERTER.value:
            gridId = object.powerstation.grid.id
        elif self.model == AclModels.BATTERYBANK.value:
            gridId = object.powerstation.grid.id

        grid = ph_model.grid.Grid.objects.get(id=gridId)
        project = grid.project.id

        # support for project-wide roles from single permission
        try:
            project_role = ph_model.permission.Permission.objects.get(user=self.user, itemType='PROJECT', itemId=project).role
            return project_role
        except:
            pass

        return ph_model.permission.Permission.objects.get(user=self.user, itemType='GRID', itemId=gridId).role

    @check_exception
    def check_permission(self, id):
        if not self.userGridIds:
            raise AccessDenied('You do not have minimum access role to view data for id: {}'.format(id))
        highestPermissionRole = self.get_highest_permission_role(id)
        has_permission = highestPermissionRole >= self.minRequiredRole
        if not has_permission:
            raise AccessDenied('You do not have minimum access role to view data for id: {}'.format(id))

    def get_device_by_id(self, id):
        self.check_permission(id)
        return self.model.objects.get(pk=id)


    def get_devices(self):
        """Returns user @param models."""
        if not self.userGridIds:
            raise AccessDenied('Access denied')
        if self.model == AclModels.GRID.value:
            devices = AclModels.GRID.value.objects.all().filter(pk__in=self.userGridIds)
        elif self.model == AclModels.QUEEN.value:
            devices = AclModels.QUEEN.value.objects.all().filter(grid__pk__in=self.userGridIds)
        elif self.model == AclModels.PROBE.value:
            devices = AclModels.PROBE.value.objects.all().filter(queen__grid__pk__in=self.userGridIds)
        elif self.model == AclModels.CIRCUIT.value:
            devices = AclModels.CIRCUIT.value.objects.all().filter(queen__grid__pk__in=self.userGridIds)
        elif self.model == AclModels.POWERSTATION.value:
            devices = AclModels.POWERSTATION.value.objects.all().filter(grid__pk__in=self.userGridIds)
        elif self.model == AclModels.INVERTER.value:
            devices = AclModels.INVERTER.value.objects.all().filter(powerstation__grid__pk__in=self.userGridIds)
        elif self.model == AclModels.BATTERYBANK.value:
            devices = AclModels.BATTERYBANK.value.objects.all().filter(powerstation__grid__pk__in=self.userGridIds)
        return devices

    def get_parent_and_children_events_query(self, parentDeviceId, severity, **kwargs):
        self.check_permission(parentDeviceId)
        if self.model == AclModels.GRID.value:
            eventManager = event_manager.GridEvent
        if self.model == AclModels.QUEEN.value:
            eventManager = event_manager.QueenEvent
        if self.model == AclModels.CIRCUIT.value:
            eventManager = event_manager.CircuitEvent
        if self.model == AclModels.POWERSTATION.value:
            eventManager = event_manager.PowerstationEvent
        if self.model == AclModels.INVERTER.value:
            eventManager = event_manager.InverterEvent
        if self.model == AclModels.BATTERYBANK.value:
            eventManager = event_manager.BatteryBankEvent
        return eventManager.get_all_events_by_parent_model_query(parentDeviceId, severity, **kwargs)


    @classmethod
    def get_all_events_query(cls, user, severity, minRequiredRole=ph_model.permission.Role.VIEW.value):
        """Returns query data of events(grid, queen, circuit) of user @user.user. if model is provided it will fetch only
        model type of events, e.g if model=Queen this will return events of queens owned by a user, excluding circuits
        or gird events."""
        userAcl = UserAcl(user, AclModels.GRID, minRequiredRole)
        if not userAcl.userGridIds:
            raise AccessDenied('You do not have minimum access role to view data for id: {}'.format(id))
        eventsQuery = AclModels.EVENT.value.objects.filter(
            Q(eventType__itemType='GRID', itemId__in=userAcl.userGridIds) |
            Q(eventType__itemType='QUEEN', itemId__in=userAcl.userQueenIds) |
            Q(eventType__itemType='CIRCUIT', itemId__in=userAcl.userCircuitIds) |
            Q(eventType__itemType='POWERSTATION', itemId__in=userAcl.userPowerstationIds) |
            Q(eventType__itemType='INVERTER', itemId__in=userAcl.userInverterIds) |
            Q(eventType__itemType='BATTERYBANK', itemId__in=userAcl.userBatteryBankIds),
            Q(eventType__severity__gte=severity))

        return eventsQuery


# TODO(estifanos): Should we raise event?? how to figure out users permission if customer not associated with circuit.
class CustomerAcl(object):
    """Acl associated with single artifact."""

    @check_exception
    def __init__(self, user, id, minRequiredRole=ph_model.permission.Role.VIEW.value):
        """"Initializes acl object, uses gird permission for all hard-wares.

        Args:
            user: Authenticated django user or ph_model.user.
            id: for detail view id(primary key) of artifact.
            minRequiredRole: Minimum required user role to see customer data."""
        self.customer = ph_model.customer.Customer.objects.get(pk=id)
        self.userAcl = UserAcl(user, AclModels.CIRCUIT,  minRequiredRole)
        try:
            self.userAcl.get_device_by_id(self.customer.circuit.id)
        except AttributeError:
            LOGGER.warning('Customer without circuit {}'.format(str(self.customer)))

    def get_customer(self):
        """Get customer"""
        customer = ph_model.customer.Customer.objects.filter(pk=self.customer.id)
        return customer

    def get_payment_log(self, sortBy='-processedTime'):
        """Gets payment made to this customer."""
        account = self.customer.account
        if not account:
            # TODO(estifanos): raise event?
            raise CustomerWithOutAccountException('Account not configured for customer {}'.format(str(self.customer)))
        paymentLogs = ph_model.transaction.MobilePayment.objects.filter(account=self.customer.account).order_by(sortBy)
        return paymentLogs

    def get_adjustment_log(self, sortBy='-processedTime'):
        """Gets adjustments made to this customer"""
        account = self.customer.account
        if not account:
            raise CustomerWithOutAccountException('Account not configured for customer {}'.format(str(self.customer)))

        adjustmentLogs = ph_model.transaction.CreditAdjustmentHistory.objects.filter(account=self.customer.account).order_by(sortBy)
        return adjustmentLogs

    def get_tariff_history(self, sortBy='-updated'):
        """Gets tariff history of a customer."""
        return ph_model.customer.CustomerTariffHistory.objects.filter(customer=self.customer).order_by('-updated', sortBy)

    def get_loans(self):
        """Gets loans of a customer."""
        return ph_model.loan.Loan.objects.filter(account=self.customer.account)
