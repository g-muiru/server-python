"""Tests acl module for userId owned grid and their events"""
import datetime
from django import test
from django.core.management import call_command
from ph_common_api import acl
from  ph_operation import event_manager

import ph_model.models as ph_model

def generate_event(pk, itemId, eventNumber, collectTime=None):
    if not collectTime:
        collectTime = datetime.datetime.utcnow()
    eventType=ph_model.event.EventType.objects.get(eventNumber=eventNumber)
    ph_model.event.Event(id=pk, eventType=eventType, itemId=itemId, collectTime=collectTime).save()

def generate_events():
    """Generate event to test event permission visibility."""
    generate_event(pk=1, itemId=301, eventNumber=ph_model.event.EventTypeConst.GRID_FAILURE.value)
    generate_event(pk=2, itemId=301, eventNumber=ph_model.event.EventTypeConst.QUEEN_FAILURE.value)
    generate_event(pk=3, itemId=301, eventNumber=ph_model.event.EventTypeConst.PROBE_FAILURE.value)
    generate_event(pk=4, itemId=301, eventNumber=ph_model.event.EventTypeConst.CIRCUIT_FAILURE.value)
    generate_event(pk=5, itemId=301, eventNumber=ph_model.event.EventTypeConst.POWERSTATION_FAILURE.value)
    generate_event(pk=6, itemId=301, eventNumber=ph_model.event.EventTypeConst.INVERTER_FAILURE.value)
    generate_event(pk=7, itemId=301, eventNumber=ph_model.event.EventTypeConst.BATTERYBANK_FAILURE.value)
    generate_event(pk=8, itemId=302, eventNumber=ph_model.event.EventTypeConst.GRID_FAILURE.value)
    generate_event(pk=9, itemId=302, eventNumber=ph_model.event.EventTypeConst.QUEEN_FAILURE.value)
    generate_event(pk=10, itemId=302, eventNumber=ph_model.event.EventTypeConst.PROBE_FAILURE.value)
    generate_event(pk=11, itemId=302, eventNumber=ph_model.event.EventTypeConst.CIRCUIT_FAILURE.value)
    generate_event(pk=12, itemId=302, eventNumber=ph_model.event.EventTypeConst.POWERSTATION_FAILURE.value)
    generate_event(pk=13, itemId=302, eventNumber=ph_model.event.EventTypeConst.INVERTER_FAILURE.value)
    generate_event(pk=14, itemId=302, eventNumber=ph_model.event.EventTypeConst.BATTERYBANK_FAILURE.value)



class BaseTestAcl(test.TestCase):
    """Base model for view test classes"""

    def setUp(self):
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_common_api/tests/fixtures/permission_based_hardware_config.json', verbosity=0)

        # Test configuration from ph_common_api/tests/data/permission_based_hardware
        self.EXPECTED_HARDWARE_AND_EVENTS_PERMISSION_DATA = {
            'user301': {'grids': 301, 'queens': 301, 'circuits': 301, 'powerstaion': 301, 'batterBank': 301,
                        'inverter': 301, 'events': {'grid': 1, 'queen': 2, 'probe': 3, 'circuit': 4, 'powerstaion': 5,
                        'inverter': 6, 'battery': 7}, 'role': 1},
            'user302': {'grids': 302, 'queens': 302, 'circuits': 302, 'powerstaion': 302, 'batterBank': 302,
                        'inverter': 302, 'events': {'grid': 8, 'queen': 9, 'probe': 10,  'circuit': 11, 'powerstaion': 12,
                        'inverter': 13, 'battery': 14}, 'role': 1},
            'user303': {'grids': [301,302], 'queens': [301,302], 'circuits': [301,302],
                        'powerstation': [301,302], 'batteryBank': [301,302], 'inverters': [301,302],
                        'events': {'grid': [1,5], 'queen': [2, 6],  'circuit': [4, 8]}, 'role':2},
            'user304': {'grids': [], 'queens': [], 'circuits': [], 'events': [],
                        'powerstation': [], 'batteryBank': [], 'inverters': []}
        }
        generate_events()



class DeviceAclTestCase(BaseTestAcl):

    def _assertUserOwnHardware(self, userId, model, id, own):
        """
        userId: int userId primary key
        model: acl.AclModels(GRID/CIRCUIT/GRID
        id: model id
        own: Boolean Check to test for own or not owned, not owned by the user should raise access denied exception.
        """
        user = ph_model.user.User.objects.get(pk=userId)
        userDeviceAcl = acl.UserAcl(user, model)
        if own:
            self.assertEquals(userDeviceAcl.get_device_by_id(id), model.value.objects.get(pk=id))
        else:
            self.assertRaises(acl.AccessDenied, userDeviceAcl.get_device_by_id, id)

    def _assertPermissionRole(self, userId, model, id, role, own=True):
        """
        userId: int userId primary key
        model: acl.AclModels(GRID/CIRCUIT/GRID
        id: model id
        role: int expected permission role
        own: Boolean Check to test for own or not owned, not owned by the user should raise access denied exception.
        """
        user = ph_model.user.User.objects.get(pk=userId)
        userDeviceAcl = acl.UserAcl(user, model)
        if own:
            self.assertEquals(userDeviceAcl.get_highest_permission_role(id), role)
        else:
            self.assertRaises(acl.AccessDenied, userDeviceAcl.get_device_by_id, id)


    def test_get_grid_user_owning_grid(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.GRID, id=301, own=True)
        self._assertPermissionRole(userId=301, model=acl.AclModels.GRID, id=301, role=1, own=True)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.GRID, id=302, own=True)
        self._assertPermissionRole(userId=302, model=acl.AclModels.GRID, id=302, role=1, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.GRID, id=301, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.GRID, id=302, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.GRID, id=301, role=2, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.GRID, id=302, role=2, own=True)

    def test_get_grid_user_not_owning_grid(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.GRID, id=302, own=False)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.GRID, id=301, own=False)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.GRID, id=303, own=False)

    def test_get_queen_user_owning_queen(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.QUEEN, id=301, own=True)
        self._assertPermissionRole(userId=301, model=acl.AclModels.QUEEN, id=301, role=1, own=True)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.QUEEN, id=302, own=True)
        self._assertPermissionRole(userId=302, model=acl.AclModels.QUEEN, id=302, role=1, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.QUEEN, id=301, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.QUEEN, id=302, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.QUEEN, id=301, role=2, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.QUEEN, id=302, role=2, own=True)

    def test_get_queen_user_not_owning_queen(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.QUEEN, id=302, own=False)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.QUEEN, id=301, own=False)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.QUEEN, id=303, own=False)

    def test_get_powerstation_user_owning_powerstation(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.POWERSTATION, id=301, own=True)
        self._assertPermissionRole(userId=301, model=acl.AclModels.POWERSTATION, id=301, role=1, own=True)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.POWERSTATION, id=302, own=True)
        self._assertPermissionRole(userId=302, model=acl.AclModels.POWERSTATION, id=302, role=1, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.POWERSTATION, id=301, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.POWERSTATION, id=302, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.POWERSTATION, id=301, role=2, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.POWERSTATION, id=302, role=2, own=True)

    def test_get_powerstation_user_not_owning_powerstation(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.POWERSTATION, id=302, own=False)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.POWERSTATION, id=301, own=False)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.POWERSTATION, id=303, own=False)

    def test_get_battery_bank_user_owning_battery_bank(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.BATTERYBANK, id=301, own=True)
        self._assertPermissionRole(userId=301, model=acl.AclModels.BATTERYBANK, id=301, role=1, own=True)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.BATTERYBANK, id=302, own=True)
        self._assertPermissionRole(userId=302, model=acl.AclModels.BATTERYBANK, id=302, role=1, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.BATTERYBANK, id=301, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.BATTERYBANK, id=302, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.BATTERYBANK, id=301, role=2, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.BATTERYBANK, id=302, role=2, own=True)

    def test_get_battery_bank_user_not_owning_battery_bank(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.BATTERYBANK, id=302, own=False)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.BATTERYBANK, id=301, own=False)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.BATTERYBANK, id=303, own=False)

    def test_get_inverter_bank_user_owning_inverter(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.INVERTER, id=301, own=True)
        self._assertPermissionRole(userId=301, model=acl.AclModels.INVERTER, id=301, role=1, own=True)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.INVERTER, id=302, own=True)
        self._assertPermissionRole(userId=302, model=acl.AclModels.INVERTER, id=302, role=1, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.INVERTER, id=301, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.INVERTER, id=302, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.INVERTER, id=301, role=2, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.INVERTER, id=302, role=2, own=True)

    def test_get_battery_bank_user_not_owning_inverter(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.INVERTER, id=302, own=False)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.INVERTER, id=301, own=False)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.INVERTER, id=303, own=False)

    def test_get_probes_user_owning_probe(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.PROBE, id=301, own=True)
        self._assertPermissionRole(userId=301, model=acl.AclModels.PROBE, id=301, role=1, own=True)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.PROBE, id=302, own=True)
        self._assertPermissionRole(userId=302, model=acl.AclModels.PROBE, id=302, role=1, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.PROBE, id=301, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.PROBE, id=302, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.PROBE, id=301, role=2, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.PROBE, id=302, role=2, own=True)

    def test_get_probes_user_not_owning_probe(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.PROBE, id=302, own=False)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.PROBE, id=301, own=False)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.PROBE, id=303, own=False)

    def test_get_circuit_user_owning_circuit(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.CIRCUIT, id=301, own=True)
        self._assertPermissionRole(userId=301, model=acl.AclModels.CIRCUIT, id=301, role=1, own=True)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.CIRCUIT, id=302, own=True)
        self._assertPermissionRole(userId=302, model=acl.AclModels.CIRCUIT, id=302, role=1, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.CIRCUIT, id=301, own=True)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.CIRCUIT, id=302, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.CIRCUIT, id=301, role=2, own=True)
        self._assertPermissionRole(userId=303, model=acl.AclModels.CIRCUIT, id=302, role=2, own=True)

    def test_get_circuit_user_not_owning_circuit(self):
        self._assertUserOwnHardware(userId=301, model=acl.AclModels.CIRCUIT, id=302, own=False)
        self._assertUserOwnHardware(userId=302, model=acl.AclModels.CIRCUIT, id=301, own=False)
        self._assertUserOwnHardware(userId=303, model=acl.AclModels.CIRCUIT, id=303, own=False)


class DevicesAclTestCase(BaseTestAcl):

    def _assertUserOwns(self, userId, model, ids, haveAccess=True):
        """
        Utility to fetch models of type @param model of ids @param ids

        model: acl.AclModels(GRID/CIRCUIT/GRID
        ids: list of model ids
        """
        try:
            user = ph_model.user.User.objects.get(pk=userId)
            userOwnedHardwares = [hardware.pk for hardware in acl.UserAcl(user, model).get_devices()]
            self.assertItemsEqual(userOwnedHardwares, ids)
        except acl.AccessDenied:
            if haveAccess:
                self.fail('User not owner ')

    def test_get_grids_user_owning_grid(self):
        self._assertUserOwns(userId=301, model=acl.AclModels.GRID, ids=[301])
        self._assertUserOwns(userId=302, model=acl.AclModels.GRID, ids=[302])
        self._assertUserOwns(userId=303, model=acl.AclModels.GRID, ids=[302, 301])

    def test_get_grids_user_owning_queens(self):
        self._assertUserOwns(userId=301, model=acl.AclModels.QUEEN, ids=[301])
        self._assertUserOwns(userId=302, model=acl.AclModels.QUEEN, ids=[302])
        self._assertUserOwns(userId=303, model=acl.AclModels.QUEEN, ids=[302, 301])

    def test_get_grids_user_owning_probes(self):
        self._assertUserOwns(userId=301, model=acl.AclModels.PROBE, ids=[301])
        self._assertUserOwns(userId=302, model=acl.AclModels.PROBE, ids=[302])
        self._assertUserOwns(userId=303, model=acl.AclModels.PROBE, ids=[302, 301])

    def test_get_grids_user_owning_circuits(self):
        self._assertUserOwns(userId=301, model=acl.AclModels.CIRCUIT, ids=[301])
        self._assertUserOwns(userId=302, model=acl.AclModels.CIRCUIT, ids=[302])
        self._assertUserOwns(userId=303, model=acl.AclModels.CIRCUIT, ids=[302, 301])

    def test_get_grids_user_owning_no_hardware(self):
        self._assertUserOwns(userId=304, model=acl.AclModels.GRID, ids=[], haveAccess=False)
        self._assertUserOwns(userId=304, model=acl.AclModels.QUEEN, ids=[], haveAccess=False)
        self._assertUserOwns(userId=304, model=acl.AclModels.PROBE, ids=[], haveAccess=False)
        self._assertUserOwns(userId=304, model=acl.AclModels.CIRCUIT, ids=[],haveAccess=False)


class EventAcl(BaseTestAcl):

    def _assertUserCanSeeEvents(self, userId, model, modelId, severity, eventIds):
        """
        Utility to fetch models of type @param model of ids @param ids

        model: acl.AclModels(GRID/CIRCUIT/GRID
        ids: list of event ids
        """
        user = ph_model.user.User.objects.get(pk=userId)
        userVisibleEvents = [event.pk for event in acl.UserAcl(user, model).get_parent_and_children_events_query(modelId, severity)]
        self.assertItemsEqual(userVisibleEvents, eventIds)

    def test_get_grid_events(self):
        """Tests acl.UserAcl.get_parent_and_children_events which should return bulk of events on grid, i.e all events on grid, queens, circuits
        powerstation, batterybank, inverters."""
        self._assertUserCanSeeEvents(userId=301, model=acl.AclModels.GRID.value, modelId= 301, eventIds=[1, 2, 4, 5, 6, 7], severity=3)
        self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.GRID.value, modelId= 302, eventIds=[8, 9, 11, 12, 13, 14], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.GRID.value, modelId= 301,
                                     eventIds=[1, 2, 4, 5, 6, 7], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.GRID.value, modelId= 302,
                                     eventIds=[8, 9, 11,12, 13 ,14], severity=3)


    def test_get_queen_events(self):
        """Tests acl.UserAcl.get_parent_and_children_events which should return events on the queen, i.e all events on queen and it's circuits."""
        self._assertUserCanSeeEvents(userId=301, model=acl.AclModels.QUEEN.value, modelId= 301, eventIds=[2, 4], severity=3)
        self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.QUEEN.value, modelId= 302, eventIds=[9, 11], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.QUEEN.value, modelId= 301,
                                     eventIds=[2, 4], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.QUEEN.value, modelId= 302,
                                     eventIds=[9, 11], severity=3)


    def test_get_circuit_events(self):
        """Tests acl.UserAcl.get_parent_and_children_events which should return events on the circuit"""
        self._assertUserCanSeeEvents(userId=301, model=acl.AclModels.CIRCUIT.value, modelId= 301, eventIds=[4], severity=3)
        self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.CIRCUIT.value, modelId= 302, eventIds=[11], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.CIRCUIT.value, modelId= 301, eventIds=[4], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.CIRCUIT.value, modelId= 302, eventIds=[11], severity=3)

    def test_get_powerstation_events(self):
        """Tests acl.UserAcl.get_parent_and_children_events which should return events on the powerstation and its' battery bank with inverter"""
        self._assertUserCanSeeEvents(userId=301, model=acl.AclModels.POWERSTATION.value, modelId= 301, eventIds=[5, 6, 7], severity=3)
        self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.POWERSTATION.value, modelId= 302, eventIds=[12, 13, 14], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.POWERSTATION.value, modelId= 301, eventIds=[5, 6, 7], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.POWERSTATION.value, modelId= 302, eventIds=[12, 13, 14], severity=3)


    def test_get_inverter_events(self):
        """Tests acl.UserAcl.get_parent_and_children_events which should return events on the on the invertres"""
        self._assertUserCanSeeEvents(userId=301, model=acl.AclModels.INVERTER.value, modelId= 301, eventIds=[6], severity=3)
        self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.INVERTER.value, modelId= 302, eventIds=[13], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.INVERTER.value, modelId= 301, eventIds=[6], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.INVERTER.value, modelId= 302, eventIds=[13], severity=3)

    def test_get_battery_bank_events(self):
        """Tests acl.UserAcl.get_parent_and_children_events which should return events on the on the battery bank"""
        self._assertUserCanSeeEvents(userId=301, model=acl.AclModels.BATTERYBANK.value, modelId= 301, eventIds=[7], severity=3)
        self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.BATTERYBANK.value, modelId= 302, eventIds=[14], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.BATTERYBANK.value, modelId= 301, eventIds=[7], severity=3)
        self._assertUserCanSeeEvents(userId=303, model=acl.AclModels.BATTERYBANK.value, modelId= 302, eventIds=[14], severity=3)

    def test_get_events_hardware_not_owned_by_user(self):
        try:
          self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.CIRCUIT.value, modelId= 301, eventIds=[4], severity=3)
          self.fail()
        except acl.AccessDenied:
            pass

    def test_get_events_for_unavailable_hardware_by_user(self):
        try:
          self._assertUserCanSeeEvents(userId=302, model=acl.AclModels.CIRCUIT.value, modelId= 500, eventIds=[4], severity=3)
          self.fail()
        except acl.AccessDenied:
            pass


class EventsAcl(BaseTestAcl):

    def _assertUserCanSeeEvents(self, userId, eventIds, severity, haveAccess=True):
        """
        Utility to fetch models of type @param model of ids @param ids

        ids: list of event ids
        """
        try:
            user = ph_model.user.User.objects.get(pk=userId)
            userVisibleEvents = [event.pk for event in acl.UserAcl.get_all_events_query(user, severity)]
            self.assertItemsEqual(userVisibleEvents, eventIds)
        except acl.AccessDenied:
            if haveAccess:
                self.fail('User not owner ')


    def test_get_all_events(self):
        """Tests acl.UserAcl.get_parent_and_children_events_all_events which should return all events on owned hardwares."""
        self._assertUserCanSeeEvents(userId=301, eventIds=[1, 2,  4, 5, 6, 7], severity=3)
        self._assertUserCanSeeEvents(userId=302, eventIds=[8, 9, 11, 12, 13, 14], severity=3)
        self._assertUserCanSeeEvents(userId=303, eventIds=[1, 2, 4, 5, 6, 7,8, 9, 11, 12, 13, 14], severity=3)
        self._assertUserCanSeeEvents(userId=304, eventIds=[], severity=3, haveAccess=False)



class BaseCustomerTestAcl(test.TestCase):
    """Base model for view test classes"""

    def setUp(self):
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_common_api/tests/fixtures/customer_account_config.json', verbosity=0)

        # Test configuration from ph_common_api/tests/data/customer_mobile_payment_data.json
        self.EXPECTED_HARDWARE_AND_MOBILE_PAYMENT_PERMISSION_DATA = {
            'user101': {'grids': 401, 'queens': 401, 'circuits': 401, 'account': 401, 'mobilepayment': 401, 'role': 1},
            'user102': {},
        }

    def _assertUserCanSeePaymentData(self, userId, customerId, paymentIds, role, haveAccess=True):
        """
        Utility to fetch models of type @param model of ids @param ids

        userId: O&M user id.
        customerId: Customer owning customer data(or hardware associated with customer)
        paymentIds: list of payment ids to compare result with.
        role: Minimum expected role to see the data.
        """
        try:
           user = ph_model.user.User.objects.get(pk=userId)
           userOwnCustomerPayments = [payment.pk for payment in acl.CustomerAcl(user, customerId, role).get_payment_log()]
           self.assertItemsEqual(userOwnCustomerPayments, paymentIds)
        except acl.AccessDenied:
            if haveAccess:
                self.fail('User not owner ')

    def _assertUserCanSeeTariffHistoryData(self, userId, customerId, tariffHistoryIds, role, haveAccess=True):
        """
        Utility to fetch models of type @param model of ids @param ids

        userId: O&M user id.
        customerId: Customer owning customer data(or hardware associated with customer)
        tariffHistoryIds: list of tariff history ids to compare result with.
        role: Minimum expected role to see the data.
        """
        try:
           user = ph_model.user.User.objects.get(pk=userId)
           userOwnCustomerTariffHistories = [payment.pk for payment in acl.CustomerAcl(user, customerId, role).get_tariff_history()]
           self.assertItemsEqual(userOwnCustomerTariffHistories, tariffHistoryIds)
        except acl.AccessDenied:
            if haveAccess:
                self.fail('User not owner ')

    def test_get_payment_log(self):
        """Tests acl.CustomerAcl.get_payment_log."""
        self._assertUserCanSeePaymentData(401, 401, [401], 1, haveAccess=True)
        #From fixture user 401 have readonly(1) access
        self._assertUserCanSeePaymentData(401, 401, [401], 2, haveAccess=False)
        self._assertUserCanSeePaymentData(402, 401, [401], 1, haveAccess=False)

        # Trying to get un existing customer should also raise acl.AccessDenied exception.
        self._assertUserCanSeePaymentData(402, 402, [401], 1, haveAccess=False)

    def test_get_tariff_history(self):
        """Tests acl.CustomerAcl.get_payment_log."""
        self._assertUserCanSeeTariffHistoryData(401, 401, [401], 1, haveAccess=True)
        #From fixture user 401 have readonly(1) access
        self._assertUserCanSeeTariffHistoryData(401, 401, [401], 2, haveAccess=False)
        self._assertUserCanSeeTariffHistoryData(402, 401, [401], 1, haveAccess=False)

        # Trying to get un existing customer should also raise acl.AccessDenied exception.
        self._assertUserCanSeeTariffHistoryData(402, 402, [401], 1, haveAccess=False)
