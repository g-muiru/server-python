"""Exception middleware, to return custom JSON error response."""

import logging
import json_response
from django.contrib import auth
import ph_model.models as ph_model

LOGGER = logging.getLogger(__name__)

class ExceptionMiddleware(object):
    """Wrapper for exception middleware."""
    def  process_exception(self, request, e):
        LOGGER.exception(str(e))
        return json_response.JsonErrorResponse.get_response(request, e)

class DisableCSRFIfNotRequired(object):
    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)

class RequestLoggingMiddleware(object):

    def process_request(self, request):
        log_list = []
        log_list.append(request.method)
        log_list.append(request.path)
        log_list.append(request.body)
        LOGGER.info(' - '.join(log_list))

