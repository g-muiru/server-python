# coding=utf-8
""" SMS provider"""
import urllib
# noinspection PyCompatibility
import urllib2
import json


class AfricasTalkingGatewayException(Exception):
    """
    Should report something ??????
    """
    pass


class AfricasTalkingGateway:
    """
    Interface functions for SMS provider
    """

    # noinspection PyPep8Naming
    def __init__(self, username_, apiKey_):
        self.username = username_
        self.apiKey = apiKey_

        self.SMSURLString = "https://api.africastalking.com/version1/messaging"
        self.VoiceURLString = "https://voice.africastalking.com/call"

    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
    def sendMessage(self, to_, message_, from_=None, bulkSMSMode=1, enqueue=0, keyword=None, linkId=None):
        """
        Send SMS

        :param to_:             to name
        :param message_:        message body
        :param from_:           optional parameter should be populated with the value of a shortcode or alphanumeric that is registered with us
        :param bulkSMSMode_:    optional parameter will be used by the Mobile Service Provider to determine who gets billed for a message sent
            using a Mobile-Terminated ShortCode. The default value is 1 (which means that you, the sender, gets charged).
            This parameter will be ignored for messages sent using alphanumerics or Mobile-Originated shortcodes.
        :param enqueue:        optional parameter is useful when sending a lot of messages at once where speed is of the essence
        :param keyword:        optional parameter is used to specify which subscription product to use to send messages for premium rated short codes
        :param linkId:         optional parameter is specified when responding to an on-demand content request on a premium rated short code
        :return:
        """

        if len(to_) == 0 or len(message_) == 0:
            raise AfricasTalkingGatewayException("Please provide both to_ and message_ parameters")
        values = {'username': self.username,
                  'to': to_,
                  'message': message_}

        if from_ is not None:
            values["from"] = from_
            values["bulkSMSMode"] = bulkSMSMode

        if enqueue > 0:
            values["enqueue"] = enqueue
        if keyword is not None:
            values["keyword"] = keyword

        if linkId is not None:
            values["linkId"] = linkId

        headers = {'Accept': 'application/json',
                   'apikey': self.apiKey}
        try:
            data = urllib.urlencode(values)
            request = urllib2.Request(self.SMSURLString, data, headers=headers)
            response = urllib2.urlopen(request)
            the_page = response.read()

        except urllib2.HTTPError as e:
            the_page = e.read()
            decoded = json.loads(the_page)
            raise AfricasTalkingGatewayException(decoded['SMSMessageData']['Message'])

        else:
            decoded = json.loads(the_page)
            recipients = decoded['SMSMessageData']['Recipients']
            return recipients

    # noinspection PyPep8Naming,PyPep8Naming
    def fetchMessages(self, lastReceivedId_):
        """
        Get messages

        :param lastReceivedId_: last SMS message pulled from SMS provider
        :return:
        """

        url = "%s?username=%s&lastReceivedId=%s" % (self.SMSURLString, self.username, lastReceivedId_)
        headers = {'Accept': 'application/json',
                   'apikey': self.apiKey}

        try:
            request = urllib2.Request(url, headers=headers)
            response = urllib2.urlopen(request)
            the_page = response.read()

        except urllib2.HTTPError as e:

            the_page = e.read()
            decoded = json.loads(the_page)
            raise AfricasTalkingGatewayException(decoded['SMSMessageData']['Message'])

        else:

            decoded = json.loads(the_page)
            messages = decoded['SMSMessageData']['Messages']

            return messages

    def call(self, from_, to_):
        """
        Request SMS from provider

        :param from_:   SMS from
        :param to_:     SMS to
        :return:
        """
        values = {'username': self.username,
                  'from': from_,
                  'to': to_}

        headers = {'Accept': 'application/json',
                   'apikey': self.apiKey}

        try:
            data = urllib.urlencode(values)
            request = urllib2.Request(self.VoiceURLString, data, headers=headers)
            # noinspection PyUnusedLocal
            response = urllib2.urlopen(request)

        except urllib2.HTTPError as e:

            the_page = e.read()
            decoded = json.loads(the_page)
            raise AfricasTalkingGatewayException(decoded['ErrorMessage'])
