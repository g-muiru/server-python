# coding=utf-8
""" UniFi controller API provider"""
import urllib
# noinspection PyCompatibility
import urllib2
import json
import ssl
import logging
from cookielib import CookieJar

LOGGER = logging.getLogger(__name__)


#class UnifiException(Exception):
#    """
#    Should report something ??????
#    """
#    LOGGER.warning(Exception)
#    pass


class Unifi:
    """
    Interface functions for Unifi Controller API
    """

    # noinspection PyPep8Naming
    def __init__(self):

        # !!! NBO office controller, site 185 controller tk
        self.username = "powerhiveEA"
        self.password = "P0g0P0w3rh1"
        self.UnifiControllerURL = "https://41.215.74.214:54002"
        self.cj = CookieJar() 

    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
    def login(self):
        """
        Login to UniFi Controller

        :return:
        """
        values = {'username': self.username,
                  'password': self.password}

        headers = {'Accept': 'application/json'}

        login_url = self.UnifiControllerURL + "/api/login"

        # self-signed cert hack
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx), urllib2.HTTPCookieProcessor(self.cj))
        
        try:
            data = json.dumps(values)
            opened = opener.open(login_url, data)
            response = opened.read()

        except urllib2.HTTPError as e:
            the_page = e.read()
            decoded = json.loads(the_page)

    def get_status(self):
        
        status_url = self.UnifiControllerURL + "/api/s/default/stat/sta"

        # self-signed cert hack
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx), urllib2.HTTPCookieProcessor(self.cj))

        try:
            opened = opener.open(status_url)
            response = opened.read()
            return response

        except urllib2.HTTPError as e:
            the_page = e.read()
            decoded = json.loads(the_page)

    def stat_vouchers(self, create_time = 0):

        status_url = self.UnifiControllerURL + "/api/s/default/stat/voucher"
        values = { 'create_time' : create_time, }

        # self-signed cert hack
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx), urllib2.HTTPCookieProcessor(self.cj))

        try:
            data = json.dumps(values)
            opened = opener.open(status_url, data)
            response = opened.read()
            return response

        except urllib2.HTTPError as e:
            the_page = e.read()
            decoded = json.loads(the_page)


    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
    def create_voucher(self, account = 0):
        """
        create hotspot use voucher
        """
        #!!!TODO: take args
        values = {'cmd': 'create-voucher',
                  'expire': 90,
                  'n': 1,
                  'quota' : 0,
                  'note' : "for account " + str(account),
                  }
        #!!!TODO: implement up, down, bytes

        headers = {'Accept': 'application/json'}

        create_voucher_url = self.UnifiControllerURL + "/api/s/default/cmd/hotspot"

        # self-signed cert hack
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx), urllib2.HTTPCookieProcessor(self.cj))

        try:
            data = json.dumps(values)
            opened = opener.open(create_voucher_url, data)
            response = opened.read()
            LOGGER.debug(response)
            return response

        except urllib2.HTTPError as e:
            the_page = e.read()
            decoded = json.loads(the_page)

