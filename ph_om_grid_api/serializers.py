"""OM Nested serializer class, for OM specific APIs.

   Usage:
     summary: ModelSerializerSummary only serializes fields defined in meta.
     detail: ModelSerializer serializes all the fiellds.
     dynamic serializer: modelSerializer(fields=(x,y,z))

"""
import enum
from rest_framework import serializers
import ph_model.models as ph_model
import logging
from django.contrib.auth import models as django_user
from datetime import datetime

LOGGER = logging.getLogger(__name__)

from ph_util import mixins

class MODELS(mixins.PairEnumMixin, enum.Enum):
    """Helper model constants."""
    GRID = ph_model.grid.Grid
    QUEEN = ph_model.queen.Queen
    PROBE = ph_model.probe.Probe
    CIRCUIT = ph_model.circuit.Circuit
    POWERSTATION = ph_model.powerstation.PowerStation
    INVERTER = ph_model.inverter.Inverter
    BATTERYBANK = ph_model.battery.BatteryBank


def _get_circuit_om_name(circuit):
    """Helper to return circuit custom om name."""
    try:
        return 'C{} ({} {})'.format(circuit.number, circuit.circuitOwner.firstName, circuit.circuitOwner.lastName)
    except:
        return 'C{}'.format(circuit.number)

class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)



class _HardwareSerializer(DynamicFieldsModelSerializer):
    """Common hardware serializer class."""
    # highestUnclearedEvent = serializers.SlugRelatedField(slug_field='severity')
    highestUnclearedEvent = serializers.SerializerMethodField('get_highest_severity')

    def get_highest_severity(self, obj):
        event = obj.highestUnclearedEvent
        if event:
            return event.eventType.severity


class MunicipalitySerializer(serializers.ModelSerializer):

    class Meta:
        model = ph_model.municipality.Municipality

class AccountSerializer(DynamicFieldsModelSerializer):
    """ph_model.account.Account serializer."""

    class Meta:
        model = ph_model.account.Account

class LoanSerializer(DynamicFieldsModelSerializer):
    """ph_model.loan.Loan serializer."""

    class Meta:
        model = ph_model.loan.Loan


class TariffSerializer(DynamicFieldsModelSerializer):
    """ph_model.tariff.Tariff serializer."""

    class Meta:
        model = ph_model.tariff.Tariff


class TariffSerializerSummary(DynamicFieldsModelSerializer):
    """ph_model.tariff.Tariff serializer."""

    class Meta:
        model = ph_model.tariff.Tariff
        fields= ('tariffId', 'name')


class TariffSerializerCompact(DynamicFieldsModelSerializer):
    """ph_model.tariff.Tariff serializer."""

    class Meta:
        model = ph_model.tariff.Tariff
        fields= ('tariffId', )

class DjangoUserSerializer(DynamicFieldsModelSerializer):
    """Django OM user serializer."""

    class Meta:
        model = django_user.User
        fields= ('username', )

class DjangoUserSerializerSummary(DjangoUserSerializer):
    """Django OM user serializer."""

    class Meta:
        model = django_user.User

class DjangoUserSerializerCompact(DjangoUserSerializer):
    """Django OM user serializer."""

    class Meta:
        model = django_user.User

class UserSerializer(DynamicFieldsModelSerializer):
    """OM user serializer."""
    userInfo = DjangoUserSerializer()

    class Meta:
        model = ph_model.user.User

class UserSerializerSummary(UserSerializer):
    """OM user serializer."""

    class Meta:
        model = ph_model.user.User

class UserSerializerCompact(UserSerializer):
    """OM user serializer."""

    class Meta:
        model = ph_model.user.User

class CustomerSerializer(DynamicFieldsModelSerializer):
    """ph_model.customer.Customer serializer."""
    account = AccountSerializer()
    tariff = TariffSerializer()
    municipalityName = serializers.SerializerMethodField('get_municipality_name')

    def get_municipality_name(self, obj):
        return obj.municipality.name

    class Meta:
        model = ph_model.customer.Customer


class CustomerSerializerSummary(DynamicFieldsModelSerializer):
    """ph_model.customer.Customer serializer."""
    tariff = TariffSerializerSummary()

    class Meta:
        model = ph_model.customer.Customer
        fields= ('id', 'account', 'tariff', 'lastName')


class CustomerSerializerCompact(DynamicFieldsModelSerializer):
    """ph_model.customer.Customer serializer."""

    class Meta:
        model = ph_model.customer.Customer
        fields= ('id', )


class CustomerMobilePayment(DynamicFieldsModelSerializer):
    """ph_model.transaction.MobilePayment serializer."""

    class Meta:
        model = ph_model.transaction.MobilePayment

class AdjustmentSerializer(DynamicFieldsModelSerializer):
    """ph_model.transaction.CreditAdjustmentHistory serializer."""

    class Meta:
        model = ph_model.transaction.CreditAdjustmentHistory
        depth = 2

class CircuitSerializer(_HardwareSerializer):
    """ph_model.circuit.Circuit serializer."""
    customer = CustomerSerializer(source='circuitOwner')
    omName = serializers.SerializerMethodField('get_name')
    queenOMName = serializers.SerializerMethodField('get_queen_om_name')
    probeNumber = serializers.SerializerMethodField('get_probe_number')

    def get_probe_number(self, obj):
        return obj.probe and obj.probe.number

    def get_name(self, obj):
       return _get_circuit_om_name(obj)

    def get_queen_om_name(self, obj):
        return obj.queen.get_name

    class Meta:
        model = ph_model.circuit.Circuit


class CircuitSerializerSummary(CircuitSerializer):
    """ph_model.circuit.Circuit serializer."""
    customer = CustomerSerializerSummary(source='circuitOwner')

    class Meta:
        model = ph_model.circuit.Circuit
        fields= ('id', 'number', 'switchEnabled', 'highestUnclearedEvent', 'customer', 'omName', 'queenOMName')


class CircuitSerializerCompact(CircuitSerializer):
    """ph_model.circuit.Circuit serializer."""
    customer = CustomerSerializerCompact(source='circuitOwner')

    class Meta:
        model = ph_model.circuit.Circuit
        fields= ('id', 'omName', 'switchEnabled', 'number')


class CircuitSwitchEnableSerializer(CircuitSerializer):
    """ph_model.circuit.Circuit serializer."""

    class Meta:
        model = ph_model.circuit.Circuit
        fields= ('id', 'overrideSwitchEnabled')



class ProbeSerializer(_HardwareSerializer):
    """ph_model.probe.Probe serializer."""

    circuits = CircuitSerializer(many=True)
    omName = serializers.CharField(source='get_name')

    class Meta:
        model = ph_model.probe.Probe


class ProbeSerializerSummary(ProbeSerializer):
    """ph_model.probe.Probe serializer."""

    class Meta:
        model = ph_model.probe.Probe
        fields= ('id', 'highestUnclearedEvent', 'circuits', 'omName')



class ProbeSerializerCompact(DynamicFieldsModelSerializer):
    """ph_model.probe.Probe serializer."""

    class Meta:
        model = ph_model.probe.Probe
        fields= ('id', 'circuits', 'number')


class QueenSerializer(_HardwareSerializer):
    """ph_model.queen.Queen serializer."""

    omName = serializers.CharField(source='get_name')
    gridOMName = serializers.SerializerMethodField('get_grid_om_name')
    circuits = CircuitSerializer(source='queen_circuits', many=True)
    currentFirmwareVersion = serializers.CharField(source='currentFirmware.version')
    expectedFirmwareVersion = serializers.CharField(source='expectedFirmware.version')
    powerstationOMName = serializers.SerializerMethodField('get_ps_om_name')
    powerstation  = serializers.SerializerMethodField('get_ps_id')

    def get_ps_id(self, obj):
        return hasattr(obj.grid, 'powerstation') and self.get_is_ps(obj) and obj.grid.powerstation.id or None

    def get_ps_om_name(self, obj):
        return hasattr(obj.grid, 'powerstation') and self.get_is_ps(obj) and obj.grid.powerstation.get_name or None

    def get_is_ps(self, data):
        if (hasattr(data.grid, 'powerstation') and data.grid.powerstation.queen == data):
            isPs = 1
        else:
            isPs = 0
        return isPs

    def get_grid_om_name(self, obj):
        return obj.grid.get_name

    class Meta:
        model = ph_model.queen.Queen


class QueenSerializerSummary(QueenSerializer):
    """ph_model.queen.Queen serializer."""

    circuits = CircuitSerializerSummary(source='queen_circuits', many=True)

    class Meta:
        model = ph_model.queen.Queen
        fields = ('id', 'highestUnclearedEvent', 'circuits', 'omName', 'gridOMName', 'lastContact', 'firstContactDate',
                 'osStarted', 'fwStarted', 'number', 'latitude', 'longitude')


class QueenSerializerCompact(QueenSerializer):
    """ph_model.queen.Queen serializer."""
    circuits = CircuitSerializerCompact(source='queen_circuits', many=True)
    probes = ProbeSerializerCompact(many=True)

    class Meta:
        model = ph_model.queen.Queen
        fields = ('id', 'circuits', 'probes', 'omName', 'latitude', 'longitude', 'number')

class QueenSerializerMinimal(QueenSerializer):
    """ph_model.queen.Queen serializer, no circuits or probes"""

    class Meta:
        model = ph_model.queen.Queen
        fields = ('id', 'omName', 'gridOMName', 'latitude', 'longitude')


class BatteryBankSerializer(_HardwareSerializer):
    """ph_model.battery.BatteryBank serializer."""

    omName = serializers.CharField(source='get_name')

    class Meta:
        model = ph_model.battery.BatteryBank


class BatteryBankSerializerSummary(BatteryBankSerializer):
    """ph_model.battery.BatteryBank serializer."""

    class Meta:
        fields= ('id', 'deviceId', 'capacity', 'stateOfCharge', 'omName')
        model = ph_model.battery.BatteryBank


class BatteryBankSerializerCompact(BatteryBankSerializer):
    """ph_model.battery.BatteryBank serializer."""

    class Meta:
        fields= ('id',)
        model = ph_model.battery.BatteryBank


class InverterSerializer(_HardwareSerializer):
    """ph_model.inverter.Inverter serializer."""
    omName = serializers.CharField(source='get_name')
    powerstationOMName = serializers.CharField(source='powerstation.get_name')

    class Meta:
        model = ph_model.inverter.Inverter


class InverterSerializerSummary(InverterSerializer):
    """ph_model.inverter.Inverter serializer."""

    class Meta:
        model = ph_model.inverter.Inverter
        fields= ('id', 'deviceId', 'type', 'omName')


class InverterSerializerCompact(InverterSerializer):
    """ph_model.inverter.Inverter serializer."""

    class Meta:
        model = ph_model.inverter.Inverter
        fields= ('id','omName')


class PowerstationSerializer(_HardwareSerializer):
    """ph_model.powerstation.Powerstation serializer."""

    omName = serializers.CharField(source='get_name')
    inverters = InverterSerializer(many=True)
    batteryBanks = BatteryBankSerializer(source='battery_banks',many=True)
    queenOMName = serializers.CharField(source='queen.get_name')

    class Meta:
        model = ph_model.powerstation.PowerStation


class PowerstationSerializerSummary(PowerstationSerializer):
    """ph_model.powerstation.Powerstation serializer."""

    inverters = InverterSerializerSummary(many=True)
    batteryBanks = BatteryBankSerializerSummary(source='battery_banks',many=True)

    class Meta:
        model = ph_model.powerstation.PowerStation
        fields= ('id', 'grid', 'queen', 'inverters', 'batteryBanks')


class PowerstationSerializerCompact(PowerstationSerializer):
    """ph_model.powerstation.Powerstation serializer."""

    omName = serializers.CharField(source='get_name')
    inverters = InverterSerializerCompact(many=True)
    batteryBanks = BatteryBankSerializerCompact(source='battery_banks',many=True)

    class Meta:
        model = ph_model.powerstation.PowerStation
        fields= ('id', 'inverters', 'batteryBanks','omName')


class GridSerializer(_HardwareSerializer):
    """ph_model.grid.Grid serializer."""

    powerstation = PowerstationSerializerSummary()
    queens = QueenSerializerMinimal(many=True)
    omName = serializers.CharField(source='get_name')
    projectName = serializers.CharField(source='project.name')
    municipalityName = serializers.CharField(source='municipality.name')
    regionName = serializers.CharField(source='municipality.region.name')
    countryName = serializers.CharField(source='municipality.region.country.name')

    class Meta:
        model = ph_model.grid.Grid


class FulcrumRecordSerializer(serializers.ModelSerializer):
    """ph_model.fulcrum.FulcrumRecord serializer."""

    class Meta:
        model = ph_model.fulcrum.FulcrumRecord


class GridListSerializer(_HardwareSerializer):
    """just the grid itself, no queens"""

    omName = serializers.CharField(source='get_name')
    projectName = serializers.CharField(source='project.name')
    municipalityName = serializers.CharField(source='municipality.name')
    regionName = serializers.CharField(source='municipality.region.name')
    countryName = serializers.CharField(source='municipality.region.country.name')

    class Meta:
        model = ph_model.grid.Grid



class GridSerializerSummary(GridSerializer):
    """ph_model.grid.Grid serializer."""
    powerstation = PowerstationSerializerSummary()
    queens = QueenSerializerSummary(many=True)

    class Meta:
        model = ph_model.grid.Grid
        fields= ('id', 'highestUnclearedEvent', 'powerstation', 'queens', 'omName')


class GridSerializerCompact(GridSerializer):
    """ph_model.grid.Grid serializer."""

    powerstation = PowerstationSerializerCompact()
    queens = QueenSerializerCompact(many=True)

    class Meta:
        model = ph_model.grid.Grid
        fields= ('id', 'powerstation', 'queens')


class EventypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ph_model.event.EventType


class EventDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = ph_model.event.EventDetail


class CustomerTariffHistorySerializer(serializers.ModelSerializer):
    tariff = TariffSerializerSummary()
    class Meta:
        model = ph_model.customer.CustomerTariffHistory


class EventSerializer(serializers.ModelSerializer):
    details = EventDetailSerializer(required=False,many=True) ### TODO this shouldn't be "many", but the framework chokes without it.  why?
    eventType = serializers.SlugRelatedField(slug_field='eventNumber',queryset = ph_model.event.Event.objects.all())
    eventDescription = serializers.SerializerMethodField('get_event_description')
    omDeviceName = serializers.SerializerMethodField('get_device_name_of_event')

    def get_event_description(self, obj):
        try:
            return obj.eventType.eventDescription
        except Exception:
            return ''

    def get_device_name_of_event(self, obj):
        """Returns name of device associated to the event."""
        try:
            eventType = obj.eventType.itemType
            modelClass = MODELS.value_of(eventType)
            device = modelClass.objects.get(pk=obj.itemId)
            if modelClass == MODELS.value_of('CIRCUIT'):
                return _get_circuit_om_name(device)
            return device.get_name
        except Exception:
            return eventType and str(eventType)

    class Meta:
        model = ph_model.event.Event

class AggregateGridSerializer(serializers.ModelSerializer):
    energyUsed = serializers.SerializerMethodField("get_energy_used")
    debits = serializers.SerializerMethodField()
    averageTariff = serializers.SerializerMethodField("get_average_tariff")
    deposits = serializers.SerializerMethodField()
    adjustments = serializers.SerializerMethodField()
    whPerUser = serializers.SerializerMethodField("get_wh_per_user")
    arpu = serializers.SerializerMethodField("get_30_days_arpu")
    totalBalance = serializers.SerializerMethodField("get_total_balance")

    def get_energy_used(self, obj):
        return float(obj.energyUsed)

    def get_debits(self, obj):
        return float(obj.debits)

    def get_average_tariff(self, obj):
        return float(obj.averageTariff)

    def get_deposits(self, obj):
        return float(obj.deposits)

    def get_adjustments(self, obj):
        return float(obj.adjustments)

    def get_wh_per_user(self, obj):
        return float(obj.whPerUser)

    def get_30_days_arpu(self, obj):
        return float(obj.arpu) * 30

    def get_total_balance(self, obj):
        return float(obj.totalBalance)

    class Meta:
        model = ph_model.aggregate.AggregateGrid
        depth = 1

