from django.conf import urls
from rest_framework import routers

import views
from ph_common_api import auth as om_auth

router = routers.DefaultRouter()
router.register(r'eventtypes', views.EventTypeViewSet,'EventTypes')
router.register(r'municipalitys', views.MunicipalityViewSet,'Municipalities')
router.register(r'aggregategrids', views.AggregateGridViewSet,'AggregateGrids')
router.register(r'tariffs', views.TariffViewSet,'Tariffs')


# TODO(estifanos): Look if possible to combine both url for detail and summary.
urlpatterns = [
    urls.url(r'^', urls.include(router.urls)),
    urls.url(r'^users/current/$', views.UserView.as_view(), name='om-current_user'),
    urls.url(r'^login/$', om_auth.login, name='om-login'),
    urls.url(r'^gridlist/$', views.GridListView.as_view(), name='om-gridlist'),
    urls.url(r'^grids/([\w-]+)/$', views.GridView.as_view(), name='om-grid'),
    urls.url(r'^grids/$', views.GridView.as_view(), name='om-grids'),
    urls.url(r'^queens/([\w-]+)/$', views.QueenView.as_view(), name='om-queen'),
    urls.url(r'^queens/$', views.QueenView.as_view(), name='om-queens'),
    urls.url(r'^queens/([\w-]+)/switch_enable/$', views.QueenEnableSwitchView.as_view(), name='om-switch-enable-queen'),
    urls.url(r'^probes/([\w-]+)/$', views.ProbeView.as_view(), name='om-probe'),
    urls.url(r'^probes/$', views.ProbeView.as_view(), name='om-probes'),
	urls.url(r'^circuits/([\w-]+)/$', views.CircuitView.as_view(), name='om-circuit'),
    urls.url(r'^circuits/([\w-]+)/switch_enable/$', views.CircuitEnableSwitchView.as_view(), name='om-switch-enable-circuit'),
    urls.url(r'^customers/search/$', views.CustomerSearchView.as_view(), name='om-customer-search'),
    urls.url(r'^customers/([\w-]+)/$', views.CustomerView.as_view(), name='om-customer'),
    urls.url(r'^customers/([\w-]+)/mobilepayment/$', views.CustomerMobilePaymentView.as_view(), name='om-mobile-payment'),
    urls.url(r'^customers/([\w-]+)/tariffhistory/$', views.CustomerTariffHistoryView.as_view(), name='om-customer-tariff-history'),
    urls.url(r'^customers/([\w-]+)/tickets/$', views.CustomerSupportTicketsView.as_view(), name='om-customer-support-tickets'),
    urls.url(r'^customers/([\w-]+)/loans/$', views.CustomerLoansView.as_view(), name='om-customer-loans'),
    urls.url(r'^customers/([\w-]+)/makeloan/$', views.post_make_loan_view, name='om-customer-make-loan'),
    urls.url(r'^customers/([\w-]+)/adjustments/$', views.AdjustmentView.as_view(), name='om-customer-adjustment-history'),
    urls.url(r'^customers/([\w-]+)/adjustbalance/$', views.post_balance_adjustment_view, name='om-customer-adjust-balance'),
    urls.url(r'^circuits/$', views.CircuitView.as_view(), name='om-circuits'),
    urls.url(r'^powerstations/([\w-]+)/$', views.PowerStationView.as_view(), name='om-powerstation'),
    urls.url(r'^powerstations/$', views.PowerStationView.as_view(), name='om-powerstations'),
    urls.url(r'^batterybanks/([\w-]+)/$', views.BatteryBankView.as_view(), name='om-batterybank'),
    urls.url(r'^batterybanks/$', views.BatteryBankView.as_view(), name='om-batterybanks'),
    urls.url(r'^inverters/([\w-]+)/$', views.InverterView.as_view(), name='om-inverter'),
    urls.url(r'^inverters/$', views.InverterView.as_view(), name='om-inverters'),
    urls.url(r'^events/$', views.EventView.as_view(), name='om-events'),
    urls.url(r'^sms/([\w-]+)/([\w-]+)/([\w-]+)/([\w-]+)/$', views.get_sms_view, name='om-sms-get'),
    urls.url(r'^sms/([\w-]+)/([\w-]+)/$', views.post_sms_view, name='om-sms-post'),
    urls.url(r'^fulcrumrecords/([\w-]+)/$', views.FulcrumRecordView.as_view(), name='om-fulcrumrecords'),
    urls.url(r'^fulcrumrecords/$', views.FulcrumRecordView.as_view(), name='om-fulcrumrecords'),
]
