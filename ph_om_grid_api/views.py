# coding=utf-8
"""Operation and Maintenance view module."""

import logging
import operator
from decimal import Decimal

import delorean
import enum
import requests
from django.core import paginator as django_paginator
from django.core import serializers as django_serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from rest_framework import filters
from rest_framework import generics
from rest_framework import permissions
from rest_framework import response
from rest_framework import views
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes

import ph_model.models as ph_model
import serializers
from ph_common_api import acl
from ph_common_api import json_response
from ph_operation import circuit_manager
from ph_operation import customer_manager
from ph_operation import event_manager
from ph_operation import sms_manager
from ph_operation import tariff_manager
from ph_operation.loan_manager import Loan
from ph_util import mixins
from ph_util.accounting_util import LoanType, add_vat, DEAD_LOAN_ACCOUNT
from ph_util.date_util import str_to_dt, get_utc_now
from ph_util.loan_utils import create_payment_plan_no_vat

LOGGER = logging.getLogger(__name__)

MINIMUM_DATA_WRITE_ROLE = ph_model.permission.Role.WRITE.value


class BaseReadOnlyOMViewSet(viewsets.ReadOnlyModelViewSet):
    """Base O&M Read only View set."""
    pass


class TariffViewSet(BaseReadOnlyOMViewSet):
    """ query def"""
    queryset = ph_model.tariff.Tariff.objects.all()
    serializer_class = serializers.TariffSerializer


class MunicipalityViewSet(BaseReadOnlyOMViewSet):
    """ query def"""
    queryset = ph_model.municipality.Municipality.objects.all()
    serializer_class = serializers.MunicipalitySerializer


class GridListViewSet(BaseReadOnlyOMViewSet):
    """ query def"""
    queryset = ph_model.grid.Grid.objects.all()
    serializer_class = serializers.GridListSerializer


class EventTypeViewSet(BaseReadOnlyOMViewSet):
    """ query def"""
    queryset = ph_model.event.EventType.objects.all()
    serializer_class = serializers.EventypeSerializer


class AggregateGridViewSet(BaseReadOnlyOMViewSet):
    """ query def"""
    queryset = ph_model.aggregate.AggregateGrid.objects.all()
    serializer_class = serializers.AggregateGridSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('aggregateDate',)


class BaseOMAPIView(views.APIView):
    """Custom APIView access low level to have flexibility when integrating with permission and middelwares
    It also provides way to return compact  vs detailed response data.

    Properties:
        model: ph_model, django model associated with the view.
        serializer_class: ph_om_grid_api.serializer.Object to serializer detailed object.
        serializer_summary_class: ph_om_grid_api.serializer.Object to serializer summary object.
    """

    def _get_display_type(self):
        """Utility to decide whether to use detailed or summary responses. default returns summary response."""
        display = self.request.query_params.get('display')
        return display and display.lower()

    def _get_json_data(self, data, many=True):
        """Returns serializer json string from registered serializers."""
        if self._get_display_type() == 'summary' and self.serializer_summary_class:
            serializer = self.serializer_summary_class(data, many=many)
        elif self._get_display_type() == 'compact' and self.serializer_compact_class:
            serializer = self.serializer_compact_class(data, many=many)
        else:
            serializer = self.serializer_class(data, many=many)
        return serializer.data


class BaseDeviceOMAPIView(BaseOMAPIView):
    """Base device O&M api.

    Properties:
        model: ph_model, django model associated with the view.
        serializer_class: ph_om_grid_api.serializer.Object to serializer detailed object.
        serializer_summary_class: ph_om_grid_api.serializer.Object to serializer summary object.
    """

    # noinspection PyUnusedLocal
    def get(self, request, format=None):
        """@override request get method."""
        many = True
        pk = len(self.args) > 0 and self.args[0]

        if pk:
            many = False
            data = acl.UserAcl(request.user, model=self.model).get_device_by_id(pk)
        else:
            data = acl.UserAcl(request.user, model=self.model).get_devices()
        json_data = self._get_json_data(data, many)
        return response.Response(json_data)


class GridListView(BaseDeviceOMAPIView):
    """ query def"""
    queryset = ph_model.grid.Grid.objects.all()
    model = ph_model.grid.Grid
    serializer_class = serializers.GridListSerializer
    serializer_summary_class = serializers.GridListSerializer


class GridView(BaseDeviceOMAPIView):
    """ph_model.grid.Grid view"""

    queryset = ph_model.grid.Grid.objects.all()
    model = ph_model.grid.Grid
    serializer_class = serializers.GridSerializer
    serializer_summary_class = serializers.GridSerializerSummary
    serializer_compact_class = serializers.GridSerializerCompact


# noinspection PyUnusedLocal
class QueenView(BaseDeviceOMAPIView):
    """ph_model.queen.Queen view"""

    queryset = ph_model.queen.Queen.objects.all()
    model = ph_model.queen.Queen
    serializer_class = serializers.QueenSerializer
    serializer_summary_class = serializers.QueenSerializerSummary
    serializer_compact_class = serializers.QueenSerializerCompact

    def post(self, request, format=None):
        """@override request post method."""
        pk = self.args[0]
        data = request.data
        acl.check_permission(request.user, 'QUEEN', pk, MINIMUM_DATA_WRITE_ROLE)

        queen = ph_model.queen.Queen.objects.get(id=pk)
        if data['grid']:
            newGrid = ph_model.grid.Grid.objects.get(id=data['grid']['id'])
            oldGrid = queen.grid
            queen.grid = newGrid
            queen.save()
            eventType = ph_model.event.EventTypeConst.QUEEN_GRID_CHANGE.value
            event_manager.raise_event(pk, eventType, [{'fromGrid': oldGrid.name, 'toGrid': newGrid.name}])

        json_data = self._get_json_data(queen, many=False)

        return response.Response(json_data)


# noinspection PyUnusedLocal
class QueenEnableSwitchView(BaseDeviceOMAPIView):
    """ph_model.circuit.Circuit view"""

    queryset = ph_model.circuit.Circuit.objects.all()
    model = ph_model.circuit.Circuit
    serializer_class = serializers.CircuitSwitchEnableSerializer
    serializer_summary_class = serializers.CircuitSwitchEnableSerializer
    serializer_compact_class = serializers.CircuitSwitchEnableSerializer

    def post(self, request, format=None):
        """@override request post method."""
        pk = self.args[0]
        data = request.data
        acl.check_permission(request.user, 'QUEEN', pk, MINIMUM_DATA_WRITE_ROLE)

        queen = ph_model.queen.Queen.objects.get(id=pk)
        circuits = ph_model.circuit.Circuit.objects.filter(queen=queen)
        json_data = None
        for circuit in circuits:
            updatedCircuit = circuit_manager.CircuitManager.override_switch_enabled(
                self.model.objects.get(id=circuit.id),
                data['overrideSwitchEnabled'], whLimit=data.get('whLimit'), vaLimit=data.get('vaLimit'))
            json_data = self._get_json_data(updatedCircuit, many=False)

        if data['overrideSwitchEnabled']:
            eventType = ph_model.event.EventTypeConst.QUEEN_CIRCUIT_TEST_ENABLED.value
        else:
            eventType = ph_model.event.EventTypeConst.QUEEN_CIRCUIT_TEST_DISABLED.value
        event_manager.raise_event(pk, eventType)

        return response.Response(json_data)


class ProbeView(BaseDeviceOMAPIView):
    """ph_model.probe.Probe view"""

    queryset = ph_model.probe.Probe.objects.all()
    model = ph_model.probe.Probe
    serializer_class = serializers.ProbeSerializer
    serializer_summary_class = serializers.ProbeSerializerSummary
    serializer_compact_class = serializers.ProbeSerializerCompact


# noinspection PyArgumentEqualDefault,PyUnusedLocal
class CustomerView(BaseOMAPIView):
    """ph_model.customer.Customer view"""
    queryset = ph_model.customer.Customer.objects.all()
    model = ph_model.customer.Customer
    serializer_class = serializers.CustomerSerializer
    serializer_summary_class = serializers.CustomerSerializerSummary
    serializer_compact_class = serializers.CustomerSerializerCompact

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        pk = len(self.args) > 0 and self.args[0]
        customers = acl.CustomerAcl(request.user, pk).get_customer()
        json_data = self._get_json_data(customers, many=True)
        return response.Response(json_data)

    def put(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        pk = len(self.args) > 0 and self.args[0]
        # noinspection PyBroadException,PyBroadException
        try:
            customers = acl.CustomerAcl(request.user, pk).get_customer()
        except Exception as e:
            LOGGER.error('access denied assigning customer %s' % str(e))
            return

        if customers and customers[0]:
            customer = customers[0]
            # noinspection PyBroadException
            try:
                circuit = request.data['circuit'] and ph_model.circuit.Circuit.objects.get(id=request.data['circuit']) or None
            except Exception as e:
                LOGGER.error('circuit error: %s' % str(e))
                circuit = None

            if circuit and customer.status != 'ACTIVE':
                customer.status = 'ACTIVE'

            accountBalance = customer.account.accountBalance
            if circuit:
                if accountBalance > 0:
                    circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
                else:
                    circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.DISABLED.value
                circuit.save()

            old_circuit = customer.circuit

            customer.circuit = circuit
            customer.save()

            # noinspection PyBroadException
            try:
                if circuit and (not old_circuit or old_circuit.id != circuit.id):
                    eventType = ph_model.event.EventTypeConst.CUSTOMER_CONNECTED_TO_CIRCUIT.value
                    event_manager.raise_event(circuit.id, eventType, [{'newCustomer': str(customer)}])

                if old_circuit and (not circuit or old_circuit.id != circuit.id):
                    eventType = ph_model.event.EventTypeConst.CUSTOMER_DISCONNECTED_FROM_CIRCUIT.value
                    event_manager.raise_event(old_circuit.id, eventType, [{'oldCustomer': str(customer)}])

                if request.data.get('tariff'):
                    tariff_manager.TariffManager.init_tariff(customer, request.data['tariff'])
            except Exception as e:
                LOGGER.error('json err: %s' % str(e))

        json_data = self._get_json_data(customers, many=True)
        return response.Response(json_data)


class CustomerSearchView(CustomerView):
    """ph_model.customer.Customer search view"""

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        cust_filters = []
        if request.GET.get('firstName'):
            cust_filters.append(Q(firstName__icontains=request.GET['firstName']))

        if request.GET.get('lastName'):
            cust_filters.append(Q(lastName__icontains=request.GET['lastName']))

        if request.GET.get('phoneNumber'):
            cust_filters.append(Q(phoneNumber__icontains=request.GET['phoneNumber']))

        if request.GET.get('accountId'):
            cust_filters.append(Q(account__id=request.GET['accountId']))

        if len(cust_filters) == 0:
            json_data = self._get_json_data([])
            return response.Response(json_data)

        # noinspection PyCompatibility
        customers = ph_model.customer.Customer.objects.filter(reduce(operator.and_, cust_filters))

        # noinspection PyArgumentEqualDefault
        json_data = self._get_json_data(customers, many=True)
        return response.Response(json_data)


class CircuitView(BaseDeviceOMAPIView):
    """ph_model.circuit.Circuit view"""

    queryset = ph_model.circuit.Circuit.objects.all()
    model = ph_model.circuit.Circuit
    serializer_class = serializers.CircuitSerializer
    serializer_summary_class = serializers.CircuitSerializerSummary
    serializer_compact_class = serializers.CircuitSerializerCompact


class CircuitEnableSwitchView(BaseDeviceOMAPIView):
    """ph_model.circuit.Circuit view"""

    queryset = ph_model.circuit.Circuit.objects.all()
    model = ph_model.circuit.Circuit
    serializer_class = serializers.CircuitSwitchEnableSerializer
    serializer_summary_class = serializers.CircuitSwitchEnableSerializer
    serializer_compact_class = serializers.CircuitSwitchEnableSerializer

    # noinspection PyUnusedLocal
    def post(self, request, format=None):
        """@override request post method."""
        pk = self.args[0]
        data = request.data
        acl.check_permission(request.user, 'CIRCUIT', pk, MINIMUM_DATA_WRITE_ROLE)

        updatedCircuit = circuit_manager.CircuitManager.override_switch_enabled(
            self.model.objects.get(id=pk),
            data['overrideSwitchEnabled'], whLimit=data.get('whLimit'), vaLimit=data.get('vaLimit'))

        if data['overrideSwitchEnabled']:
            eventType = ph_model.event.EventTypeConst.CIRCUIT_TEST_ENABLED.value
        else:
            eventType = ph_model.event.EventTypeConst.CIRCUIT_TEST_DISABLED.value
        event_manager.raise_event(pk, eventType)

        json_data = self._get_json_data(updatedCircuit, many=False)
        return response.Response(json_data)


class PowerStationView(BaseDeviceOMAPIView):
    """ph_model.powerstation.Powerstation view"""

    queryset = ph_model.powerstation.PowerStation.objects.all()
    model = ph_model.powerstation.PowerStation
    serializer_class = serializers.PowerstationSerializer
    serializer_summary_class = serializers.PowerstationSerializerSummary
    serializer_compact_class = serializers.PowerstationSerializerCompact


class InverterView(BaseDeviceOMAPIView):
    """ph_model.inverter.Inverter view"""

    queryset = ph_model.inverter.Inverter.objects.all()
    model = ph_model.inverter.Inverter
    serializer_class = serializers.InverterSerializer
    serializer_summary_class = serializers.InverterSerializerSummary
    serializer_compact_class = serializers.InverterSerializerCompact


class BatteryBankView(BaseDeviceOMAPIView):
    """ph_model.battery.BatteryBank view"""

    queryset = ph_model.battery.BatteryBank.objects.all()
    model = ph_model.battery.BatteryBank
    serializer_class = serializers.BatteryBankSerializer
    serializer_summary_class = serializers.BatteryBankSerializerSummary
    serializer_compact_class = serializers.BatteryBankSerializerCompact


# noinspection PyArgumentEqualDefault
class CustomerTariffHistoryView(BaseOMAPIView):
    """ph_model.customer.CustomerTariffHistory view"""

    queryset = ph_model.customer.CustomerTariffHistory.objects.all()
    model = ph_model.customer.CustomerTariffHistory
    serializer_class = serializers.CustomerTariffHistorySerializer
    serializer_summary_class = serializers.CustomerTariffHistorySerializer
    serializer_compact_class = serializers.CustomerTariffHistorySerializer

    # noinspection PyUnusedLocal
    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        pk = len(self.args) > 0 and self.args[0]
        tariffHistories = acl.CustomerAcl(request.user, pk).get_tariff_history()
        json_data = self._get_json_data(tariffHistories, many=True)
        return response.Response(json_data)


# noinspection PyUnusedLocal
class CustomerLoansView(BaseOMAPIView):
    """ph_model.loan.Loan view"""

    queryset = ph_model.loan.Loan.objects.all()
    model = ph_model.loan.Loan
    serializer_class = serializers.LoanSerializer
    serializer_summary_class = serializers.LoanSerializer
    serializer_compact_class = serializers.LoanSerializer

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        pk = len(self.args) > 0 and self.args[0]
        loans = acl.CustomerAcl(request.user, pk).get_loans()
        # noinspection PyArgumentEqualDefault
        json_data = self._get_json_data(loans, many=True)
        return response.Response(json_data)


# noinspection PyUnusedLocal
class CustomerSupportTicketsView(generics.GenericAPIView):
    """ fetch customer support tickets from happyfox """
    queryset = ph_model.customer.Customer.objects.all()
    model = ph_model.customer.Customer

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        pk = len(self.args) > 0 and self.args[0]
        customers = acl.CustomerAcl(request.user, pk).get_customer()
        if customers and customers[0]:
            customer = customers[0]
            user = "200ef612adba45ae9441268a17288bdd"
            password = "d381407209df4120890a2cfcabb16527"
            resp = requests.get('https://powerhiveea.happyfox.com/api/1.1/json/tickets/?q=honeycomb_customer_id:' + str(customer.account.id),
                                auth=(user, password))
            return response.Response(resp.json())


# noinspection PyUnusedLocal
class UserView(BaseOMAPIView):
    """ph_model.user.User view"""

    queryset = ph_model.user.User.objects.all()
    model = ph_model.user.User
    serializer_class = serializers.UserSerializer
    serializer_summary_class = serializers.UserSerializerSummary
    serializer_compact_class = serializers.UserSerializerCompact

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        json_data = self._get_json_data(request.user.user, many=False)
        return response.Response(json_data)


# noinspection PyUnusedLocal
class CustomerMobilePaymentView(BaseOMAPIView):
    """ph_model.transaction.MobilePayment view"""

    queryset = ph_model.transaction.MobilePayment.objects.all()
    model = ph_model.transaction.MobilePayment
    serializer_class = serializers.CustomerMobilePayment
    serializer_summary_class = serializers.CustomerMobilePayment
    serializer_compact_class = serializers.CustomerMobilePayment

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        pk = len(self.args) > 0 and self.args[0]
        mobilePayments = acl.CustomerAcl(request.user, pk).get_payment_log()
        # noinspection PyArgumentEqualDefault
        json_data = self._get_json_data(mobilePayments, many=True)
        return response.Response(json_data)


# noinspection PyUnusedLocal
class AdjustmentView(BaseOMAPIView):
    """ph_model.transaction.CreditAdjustmentHistory view"""
    queryset = ph_model.transaction.CreditAdjustmentHistory.objects.all()
    model = ph_model.transaction.CreditAdjustmentHistory
    serializer_class = serializers.AdjustmentSerializer
    serializer_summary_class = serializers.AdjustmentSerializer
    serializer_compact_class = serializers.AdjustmentSerializer

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        pk = len(self.args) > 0 and self.args[0]
        creditAdjustments = acl.CustomerAcl(request.user, pk).get_adjustment_log()
        # noinspection PyArgumentEqualDefault,PyArgumentEqualDefault
        json_data = self._get_json_data(creditAdjustments, many=True)
        return response.Response(json_data)


# noinspection PyUnusedLocal
class EventView(BaseOMAPIView):
    """ph_model.circuit.Circuit view"""

    queryset = ph_model.event.Event.objects.all()
    model = ph_model.event.Event
    serializer_class = serializers.EventSerializer
    serializer_summary_class = serializers.EventSerializer
    serializer_compact_class = serializers.EventSerializer

    @staticmethod
    def _get_int_or_none(d):
        # noinspection PyBroadException
        try:
            return int(d)
        except Exception:
            LOGGER.warning('Invalid Id {}'.format(d))

    @staticmethod
    def _get_model(type):
        """Returns acl.Models  string @param type to acl.MODELS."""
        if type and acl.AclModels.is_valid_key(type.upper()):
            return acl.AclModels.value_of(type.upper())

    @staticmethod
    def _get_serverity(severity):
        """Gets ph_model.alarm.Severity from @param severity"""
        # noinspection PyBroadException
        try:
            return int(severity)
        except Exception:
            return acl.ph_model.event.Severity.FINE.value

    @staticmethod
    def get_municipality_timezone(model, deviceId):
        """

        :param model:
        :param deviceId:
        :return:
        """
        if model == 'queen':
            device = ph_model.queen.Queen.objects.get(pk=deviceId)
            municipality = ph_model.municipality.Municipality.objects.get(grid__queens=device)
            return municipality and municipality.timeZone
        elif model == 'circuit':
            device = ph_model.circuit.Circuit.objects.get(pk=deviceId)
            municipality = ph_model.municipality.Municipality.objects.get(grid__queens__queen_circuits=device)
            return municipality and municipality.timeZone
        elif model == 'grid':
            device = ph_model.grid.Grid.objects.get(pk=deviceId)
            municipality = ph_model.municipality.Municipality.objects.get(grid=device)
            return municipality and municipality.timeZone
        else:
            return 'UTC'

    def get(self, request, format=None):
        """@override request get method."""
        model = self._get_model(request.query_params.get('type'))
        modelType = request.query_params.get('type')
        severity = self._get_serverity(request.query_params.get('severity'))
        deviceId = self._get_int_or_none(request.query_params.get('id'))
        per_page = self._get_int_or_none(request.query_params.get('per_page')) or 20
        page = self._get_int_or_none(request.query_params.get('page')) or 1

        kwargs = {}
        if request.query_params.get('minCollectTime'):
            kwargs["collectTime__gte"] = request.query_params.get('minCollectTime')
        if request.query_params.get('maxCollectTime'):
            kwargs["collectTime__lte"] = request.query_params.get('maxCollectTime')

        sortBy = request.query_params.get('sort') or '-eventType__severity'
        if deviceId:
            eventsQuery = acl.UserAcl(request.user, model or
                                      acl.AclModels.GRID.value).get_parent_and_children_events_query(deviceId, severity, **kwargs)
        else:
            eventsQuery = acl.UserAcl.get_all_events_query(request.user, severity)

        paginator = django_paginator.Paginator(eventsQuery.order_by(sortBy), per_page)
        try:
            page = paginator.page(page)
        except PageNotAnInteger:
            page = []
        except EmptyPage:
            page = []

        timeZone = self.get_municipality_timezone(modelType, deviceId)
        for d in page:
            d.collectTime = delorean.Delorean(d.collectTime).shift(timeZone).datetime

        # noinspection PyArgumentEqualDefault
        json_data = self._get_json_data(page, many=True)
        return response.Response({'count': paginator.count, 'num_pages': paginator.num_pages,
                                  'results': json_data})


# noinspection PyUnusedLocal
class FulcrumRecordView(BaseOMAPIView):
    """ph_model.fulcrum.FulcrumRecord view"""

    queryset = ph_model.fulcrum.FulcrumRecord.objects.all()
    model = ph_model.fulcrum.FulcrumRecord
    serializer_class = serializers.FulcrumRecordSerializer
    serializer_summary_class = serializers.FulcrumRecordSerializer
    serializer_compact_class = serializers.FulcrumRecordSerializer

    def get(self, request, format=None):
        """

        :param request:
        :param format:
        :return:
        """
        # pk = len(self.args) > 0 and self.args[0]
        limit = None
        many = True

        kwargs = {}
        if request.query_params.get('form'):
            kwargs["form"] = request.query_params.get('form')
        else:
            limit = 10

        if request.query_params.get('powerhive_site_id'):
            kwargs["form_data__powerhive_site_id__choice_values__contains"] = \
                request.query_params.get('powerhive_site_id')
        else:
            limit = 10

        if request.query_params.get('customer_name'):
            kwargs["form_data__customer_name__contains"] = \
                request.query_params.get('customer_name')

        # TODO: This param does not work yet as the Fulcrum data name has a trailing underscore.
        if request.query_params.get('customer_honeycomb_account_id'):
            kwargs["form_data__customer_honeycomb_account_id__contains"] = \
                request.query_params.get('customer_honeycomb_account_id')

        if limit:
            # Prevent massive output, but allow easy test drive of endpoint
            data = self.model.objects.filter(**kwargs)[:limit]
        else:
            data = self.model.objects.filter(**kwargs)
        json_data = self._get_json_data(data, many)

        return response.Response(json_data)


@api_view(['GET'])
@permission_classes([permissions.IsAuthenticated])
def get_sms_view(request, model, deviceId, page, numPerPage):
    """

    :param request:
    :param model:
    :param deviceId:
    :param page:
    :param numPerPage:
    :return:
    """
    # Reject all devices other than circuits
    if model != 'customers':
        responseMessages = [json_response.JsonErrorResponse.get_response(request, Exception("Not supported: {}".format(model)))]
        return json_response.JsonQueuedResponse.get_response(responseMessages)

    sms_queryset = ph_model.SMS.objects.filter(customer_id=deviceId).order_by('-collectTime')
    paginator = Paginator(sms_queryset, numPerPage)
    try:
        sms = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        sms = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver empty set
        sms = []

    # sms = reversed(sms)
    return json_response.PHJsonHTTPResponse(
        django_serializers.serialize("json", sms, fields=('message', 'collectTime', 'smsFlow')), isEncoded=True)


# noinspection PyPep8Naming
class SMS_MANAGER(mixins.PairEnumMixin, enum.Enum):
    """Helper model constants."""
    GRID = sms_manager.GridSMSManager
    QUEEN = sms_manager.QueenSMSManager
    CIRCUIT = sms_manager.CircuitSMSManager
    CUSTOMER = sms_manager.CustomerSMSManager


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def post_sms_view(request, model, sms_id):
    """

    :param request:
    :param model:
    :param sms_id:
    :return:
    """
    message = request.data['message']
    smsManager = SMS_MANAGER.value_of(model.upper())(sms_id, message)
    smsManager.send()

    # TODO: Determine appropriate response
    return response.Response([{'message': message}])


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def post_balance_adjustment_view(request, customer_id):
    """

    :param request:
    :param customer_id:
    :return:
    """
    user_id = request.user.id
    # have to lookup by userInfo_id (fk to django auth_user table pk) rather than id (ph usermodel pk)
    user = ph_model.user.User.objects.get(userInfo_id=user_id)
    amount = request.data['amount']
    reason = request.data['reason']
    adjType = None
    if 'adjType' in request.data:
        adjType = request.data['adjType']
    customer = ph_model.customer.Customer.objects.get(id=customer_id)
    customer_manager.CustomerManager.adjust_credit(customer, user, reason, amount, adjType)

    # TODO: Determine appropriate response
    # return response.Response([{'user': user.id}])
    return response.Response([{'amount': amount, 'reason': reason}])


def make_loan(request, customer_id, require_payment=False):
    """
    TODO split this into 2 functions
    :param data:
    :param customer_id:
    :param require_payment:
    :return:
    """
    if request.POST.get('amount') is None:
        data = request.data
    else:
        data = request.POST
    acc_id = data.get('account_id')
    if acc_id is not None:
        customer = ph_model.customer.Customer.objects.get(account_id=acc_id)
    else:
        customer = ph_model.customer.Customer.objects.get(id=customer_id)
    amount = Decimal(data.get('amount'))
    if require_payment:
        if amount > customer.account.accountBalance:
            return customer.account_id, None, round(amount,2), round(customer.account.accountBalance,2)
    repaymentSchedule = data.get('repaymentSchedule')
    totalPayments = Decimal(data.get('totalPayments'))
    sd = data.get('startDate')
    if sd is None:
        startDate = get_utc_now()
    else:
        startDate = str_to_dt(sd)
    loanType = data.get('loanType')
    notes = data.get('notes')
    if "INTERNET" in loanType:
        loanType = LoanType.INTERNET_PAYMENT_PLAN.name

    loan = ph_model.loan.Loan

    lt = None
    if "KUKU_POA" in loanType:
        lt = LoanType.KUKU_POA_NO_VAT_LOAN.name
    if lt is not None:
        new_loan = create_payment_plan_no_vat(
            loan_cls=loan,
            loan_type=lt,
            acc=customer.account,
            loan_amt=amount,
            start_date=startDate,
            ttl_days=totalPayments)
    else:
        new_loan = create_loan_om(
            loan_cls=loan,
            loan_type=loanType,
            acc=customer.account,
            loan_amt=amount,
            start_date=startDate,
            total_payments=totalPayments,
            repayment_schedule=repaymentSchedule,
            notes=notes)
    acc_bal = customer.account.accountBalance
    if new_loan is not None and require_payment and new_loan.account.id != DEAD_LOAN_ACCOUNT:
        loan_handler = Loan(new_loan)
        new_loan, acc_bal = loan_handler.repay_loan(customer, get_utc_now())

    return customer.account_id, customer.id, round(amount, 2), round(acc_bal, 2)


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def post_make_loan_view(request, customer_id):
    """

    :param request:
    :param customer_id:
    :return:
    """

    acc_id, cust_id, amount, acc_bal = make_loan(request, customer_id)
    return response.Response([{'acc_id': acc_id, 'cust_id': cust_id, 'amount': amount, 'bal_after': acc_bal}])


def create_loan_om(loan_cls, loan_type, acc, loan_amt, start_date, total_payments, repayment_schedule, notes, freq=1):
    """
    admin-created loan via honeycomb, leaving create_loan() for backwards compat
    :param loan_cls:            Loan class
    :param loan_type:           loan type string from LoanType.XXXX.name
    :param acc:                 Account
    :param loan_amt:            amount of the loan
    :param start_date:          start date for payments
    :param total_payments:      number of payments
    :param repayment_schedule:  fixed repayment schedule
    :param notes:               tell us something about this loan
    :param freq:                how often to make payments in days (required by model, but overridden by repaymentSchedule)
    :return: the loan that was created
    """
    loan = loan_cls(
        account=acc,
        outStandingPrinciple=add_vat(loan_amt),
        loanAmount=loan_amt,
        startDate=start_date,
        fixedRepayment='True',
        repaymentSchedule=repayment_schedule,
        paymentFrequency=freq,
        totalNumberOfPayments=total_payments,
        loanType=loan_type,
        notes=notes)

    loan.save()
    return loan
