# coding=utf-8
"""
Test grid API
"""
from django.core.urlresolvers import reverse
from django.core.management import call_command
from rest_framework import test as rest_framework_test
from rest_framework import status

from ph_model.models import Loan
from ph_util.accounting_util import LoanType


class BaseTestView(rest_framework_test.APITestCase):
    """Base model for view test classes"""

    def setUp(self):
        """Initialize common tests utilities."""
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_om_grid_api/tests/fixtures/permission_based_hardware_config.json', verbosity=0)
        call_command('loaddata', 'ph_model/tests/fixtures/initial_data.json', verbosity=0)


class APIDeviceGetEndPointTest(BaseTestView):
    """Tests apis endpoint urls."""

    # permission based devices loaded from fixtures/permission_based_hardware_config.json
    DEVICE_GET_END_POINTS = [
        {'urlId': 'om-grid', 'args': (301,)},
        {'urlId': 'om-grids'},
        {'urlId': 'om-queen', 'args': (301,)},
        {'urlId': 'om-queens'},
        {'urlId': 'om-probe', 'args': (301,)},
        {'urlId': 'om-probes'},
        {'urlId': 'om-circuit', 'args': (301,)},
        {'urlId': 'om-circuits'},
        {'urlId': 'om-powerstation', 'args': (301,)},
        {'urlId': 'om-powerstations'},
        {'urlId': 'om-batterybank', 'args': (301,)},
        {'urlId': 'om-batterybanks'},
        {'urlId': 'om-inverter', 'args': (301,)},
        {'urlId': 'om-inverters'}
    ]

    DEVICE_POST_END_POINTS = [
        {'urlId': 'om-switch-enable-circuit', 'args': (301,), 'data': {'overrideSwitchEnabled': True}},
        {'urlId': 'om-customer-make-loan', 'args': (2,),
         'data': {'amount': 100,'totalPayments': 10,'startDate': '2017-11-22','loanType': LoanType.INTERNET_PAYMENT_PLAN.name,'notes': 'Some notes'}},
    ]

    def test_device_end_point_get_success(self):
        """
        Check for OK status GET
        :return:
        """
        self.client.login(username='user301', password='user301')
        for endPoint in self.DEVICE_GET_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)

    def test_device_end_point_get_access_denied(self):
        """
        Check for FORBIDDEN status GET
        :return:
        """
        self.client.login(username='user304', password='user304')
        for endPoint in self.DEVICE_GET_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, url)

    def test_device_end_point_post_success(self):
        """
        Check for OK status POST
        :return:
        """
        self.client.login(username='user301', password='user301')
        for idx, endPoint in enumerate(self.DEVICE_POST_END_POINTS):
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            response = self.client.post(url, endPoint.get('data'))
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)
        self.assertEquals(Loan.objects.count(), 2)


class APIEventEndPointTest(BaseTestView):
    """Tests event api endpoint urls."""

    # permission based devices  with events loaded from fixtures/permission_based_hardware_config.json
    EVENT_END_POINTS = [
        {'urlId': 'om-events', 'args': None, 'params': 'id=301&type=grid&severity=2&sort=-collectTime'},
    ]

    def test_event_end_point_get_success(self):
        """
        Check for OK status GET EVENT
        """
        self.client.login(username='user301', password='user301')
        for endPoint in self.EVENT_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            if endPoint.get('params'):
                url = '{}?{}'.format(url, endPoint.get('params'))
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)

    def test_event_end_point_get_access_denied(self):
        """
        Check for FORBIDDEN status GET EVENT
        """
        self.client.login(username='user304', password='user304')
        for endPoint in self.EVENT_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            if endPoint.get('params'):
                url = '{}?{}'.format(url, endPoint.get('params'))
            # self.assertRaises(acl.AccessDenied, self.client.get, url)
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, url)


class APICustomerEndPointTest(BaseTestView):
    """Tests apis endpoint urls."""

    def setUp(self):
        """
        Load test data
        """
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_om_grid_api/tests/fixtures/customer_account_config.json', verbosity=0)

    # permission based customer data loaded from fixtures/customer_account_config.json
    CUSTOMER_END_POINTS = [
        {'urlId': 'om-mobile-payment', 'args': (401,)},
        {'urlId': 'om-customer-tariff-history', 'args': (401,)}
    ]

    def test_customer_end_point_get_success(self):
        """
        Check for OK status GET CUSTOMER
        """
        self.client.login(username='user401', password='user401')
        for endPoint in self.CUSTOMER_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)

    def test_customer_end_point_get_access_denied(self):
        """
        Check for FORBIDDEN status GET CUSTOMER
        """
        self.client.login(username='user402', password='user402')
        for endPoint in self.CUSTOMER_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            # self.assertRaises(acl.AccessDenied, self.client.get, url)
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, url)


class APIUserEndPointTest(BaseTestView):
    """Tests apis endpoint urls."""

    def setUp(self):
        """
        Load test data
        """
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_om_grid_api/tests/fixtures/customer_account_config.json', verbosity=0)

    def test_fetch_current_user_end_point(self):
        """
        Check for OK status GET USER
        """
        self.client.login(username='user401', password='user401')
        url = reverse('om-current_user')
        response = self.client.get(url)
        self.assertEquals(response.status_code, status.HTTP_200_OK, url)


