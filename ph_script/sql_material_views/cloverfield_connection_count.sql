-- View: public.cloverfield_circuit_monitor_gaps_30_days
drop
	materialized view if exists public.cloverfield_circuit_monitor_gaps_30_days;

create
	materialized view public.cloverfield_circuit_monitor_gaps_30_days TABLESPACE pg_default as select
		date(
			cm."collectTime"
		) as date,
		cc.number as circuit,
		q.number as queen,
		g.name as grid,
		p.name as project,
		concat( c."firstName", ' ', c."lastName" ) as name,
		cc."switchEnabled" as switch_enabled,
		round( cm."vacAvg"::numeric, 2 ) as vac_avg,
		round( cm."vacMin"::numeric, 2 ) as vac_min,
		round( cm."vacMax"::numeric, 2 ) as vac_max,
		cm."collectTime" as collect_time,
		date_part(
			'epoch'::text,
			cm."collectTime" - lag(
				cm."collectTime"
			) over(
				partition by cm.circuit_id
			order by
				cm."collectTime"
			)
		) as delta
	from
		ph_model_circuitmonitor cm left join ph_model_circuit cc on
		cc.id = cm.circuit_id left join ph_model_queen q on
		q.id = cc.queen_id left join ph_model_grid g on
		g.id = q.grid_id left join ph_model_project p on
		p.id = g.project_id left join ph_model_customer c on
		cc.id = c.circuit_id
	where
		date_part(
			'day'::text,
			now()- cm."collectTime"
		)< 30::double precision
		and c.circuit_id is not null
		and p.name::text = 'Cloverfield'::text
		and cc."switchEnabled"::text <> 'UNKNOWN'::text WITH NO DATA;

comment on
materialized view public.cloverfield_circuit_monitor_gaps_30_days is 'Time in seconds between circuit monitors for the past 30 days';