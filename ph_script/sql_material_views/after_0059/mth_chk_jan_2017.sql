-- to refresh data  -- REFRESH MATERIALIZED VIEW mth_chk_jan_2017 with data;
-- only includes data back to January 2017 --- On 2017-11-22 takes 1.5 minutes to refresh with data
-- View: public.mth_chk_jan_2017

DROP MATERIALIZED VIEW IF EXISTS public.mth_chk_jan_2017;

CREATE MATERIALIZED VIEW public.mth_chk_jan_2017
TABLESPACE pg_default
as select round(case when x.billed is null then null
            when x.mth_bal >= 0 then x.amt
            when x.billed < x.mth_bal then x.amt + x.mth_bal
            else 0 end,6) as collected,
        x.* from
   (SELECT
    a.billed,
    a.adjustments,
    a.wh,
    a.muni,
    a.mth_id,
    a.acc_id,
    a.diff_mins,
    a.dt,
    a.created,
    a.trans,
    a.s_id,
    a.src,
    a.amt,
    a.mth_bal,
    a.bal,
    a.acc_bal,
    a.mth_bal - a.bal AS diff,
    a.uncollected_bal,
    a.clearing_bal,
    a.reconnect_bal
   FROM ( SELECT m.name AS muni,
            mth.id AS mth_id,
            mth.account_id AS acc_id,
            (date_part('epoch'::text, mth.created - mth."sourceProcessedTime") / 60::double precision)::integer AS diff_mins,
            mth."sourceProcessedTime" AS dt,
            mth.created,
            mth."transactionType" AS trans,
            mth."sourceId" AS s_id,
            mth.source AS src,
            round(mth.amount, 6) AS amt,
            case when mth.source::text ~~ 'CreditAdjustmentHistory%'::text THEN
                 case when mth."transactionType" = 'Credit'then mth.amount else -mth.amount end else 0 end as adjustments,
            case when mth.source::text ~~ '%VAR_FEE%'::text THEN
                          (select "intervalWh" from ph_model_usage u, ph_model_usagecharge uc
                            where u.id = uc.usage_id and uc.id = mth."sourceId") else null end as wh,
            round(
                CASE
                    WHEN mth.source::text ~~ '%INITc%'::text THEN null
                    WHEN mth."transactionType"::text <> 'Credit'::text THEN - mth.amount
                    ELSE NULL::numeric
                end,6) as billed,
            round(mth."accountBalance", 6) AS mth_bal,
            round(sum(
                CASE
                    WHEN mth.source::text ~~ '%INITc%'::text THEN 0::numeric
                    WHEN mth."transactionType"::text = 'Credit'::text THEN mth.amount
                    WHEN mth."transactionType"::text <> 'Credit'::text THEN - mth.amount
                    ELSE NULL::numeric
                END) OVER (PARTITION BY mth.account_id ORDER BY mth.account_id, mth."sourceProcessedTime", mth.source, mth."transactionType"), 6) AS bal,
            round(a_1."accountBalance", 6) AS acc_bal,
            round(mth.uncollected_bal, 6) AS uncollected_bal,
            round(mth.clearing_bal, 6) AS clearing_bal,
            round(mth.reconnect_bal, 6) AS reconnect_bal
           FROM ph_model_mastertransactionhistory mth
             LEFT JOIN ph_model_customer c ON c.account_id = mth.account_id
             LEFT JOIN ph_model_account a_1 ON c.account_id = a_1.id
             LEFT JOIN ph_model_municipality m ON m.id = c.municipality_id
          ORDER BY mth.account_id, mth."sourceProcessedTime", mth.source, mth."transactionType") a
  WHERE a.dt > '2016-12-31 00:00:00+00'::timestamp with time zone) as x WITH NO DATA;
