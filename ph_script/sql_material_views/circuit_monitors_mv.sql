-- to refresh data  -- REFRESH MATERIALIZED VIEW [ CONCURRENTLY ] circuit_monitors_mv with data;
-- only includes data back to March 2017 --- 2017-08-22 Takes 4 minutes to refresh with data
-- View: public.circuit_monitors_mv

DROP MATERIALIZED VIEW IF EXISTS public.circuit_monitors_mv;

CREATE MATERIALIZED VIEW public.circuit_monitors_mv as
 SELECT queen."monitorInterval" AS monitor_interval,
    86400 / queen."monitorInterval" AS actual_monitors,
    queen.number AS qn,
    grid.name AS grid,
    circuitmonitor.circuit_id AS circ_id,
    date(circuitmonitor."collectTime") AS dt,
    100.00 * 0.9808 AS kpi_uptime_percent,
    count(DISTINCT queen.id) AS reporting_queens,
    count(DISTINCT circuitmonitor.circuit_id) AS reporting_circuits,
    count(circuitmonitor."collectTime") AS monitor_cnt,
    sum(COALESCE(
        CASE
            WHEN circuitmonitor."vacMin" >= 200::double precision THEN 1
            ELSE 0
        END, 0)) AS vac_min_gte_200_cnt
   FROM ph_model_grid grid
     LEFT JOIN ph_model_queen queen ON queen.grid_id = grid.id
     LEFT JOIN ph_model_circuit circuit ON queen.id = circuit.queen_id
     LEFT JOIN (select * from ph_model_circuitmonitor,
                (select
                  case when LAG("collectTime") over (partition BY circuit_id ORDER BY "collectTime") != "collectTime" then id end as id
                 from ph_model_circuitmonitor) as a
                where ph_model_circuitmonitor.id = a.id) as circuitmonitor ON circuit.id = circuitmonitor.circuit_id
     LEFT JOIN ph_model_project project ON grid.project_id = project.id
  WHERE circuitmonitor."collectTime" > '2017-02-28 18:00:00-06'::timestamp with time zone
  GROUP BY queen."monitorInterval", (86400 / queen."monitorInterval"), queen.number, grid.name, circuitmonitor.circuit_id, (date(circuitmonitor."collectTime"))
  ORDER BY grid.name DESC
WITH NO DATA;
