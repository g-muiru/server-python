-- to refresh data  -- REFRESH MATERIALIZED VIEW usage_raw_March_2017_days with data;
-- TODO this actually has over 90 days of data ---- RENAME eventually
-- View: public.usage_raw_31_days -- legacy name to access renamed materialized view
-- Materialized View: public.usage_raw_March_2017_days

DROP VIEW IF EXISTS public.usage_raw_31_days;

DROP MATERIALIZED VIEW IF EXISTS public.usage_raw_95_days;

DROP materialized VIEW IF EXISTS public.usage_raw_March_2017_days;
CREATE MATERIALIZED VIEW public.usage_raw_March_2017_days AS
 SELECT a.dt,
    a.id,
    a.created,
    a.updated,
    a."intervalWh",
    a."intervalVAmax",
    a."intervalVmin",
    a."collectTime",
    a.processed,
    a.circuit_id,
    a.customer_id,
    a."intervalWhH",
    a."intervalVAmaxH",
    a.grid,
    a.qn,
    a.cn
   FROM ( SELECT dt.dt,
            x.id,
            x.created,
            x.updated,
            x."intervalWh",
            x."intervalVAmax",
            x."intervalVmin",
            x."collectTime",
            x.processed,
            x.circuit_id,
            x.customer_id,
            x."intervalWhH",
            x."intervalVAmaxH",
            x.grid,
            x.qn,
            x.cn
           FROM generate_series('2017-03-01', (now() + '1 day'), '01:00:00'::interval) dt(dt)
             LEFT JOIN ( SELECT u.id,
                    u.created,
                    u.updated,
                    u."intervalWh",
                    u."intervalVAmax",
                    u."intervalVmin",
                    u."collectTime",
                    u.processed,
                    u.circuit_id,
                    u.customer_id,
                    u."intervalWhH",
                    u."intervalVAmaxH",
                    g.name AS grid,
                    q.number AS qn,
                    c.number AS cn
                   FROM ph_model_usage u
                     LEFT JOIN ph_model_circuit c ON c.id = u.circuit_id
                     LEFT JOIN ph_model_queen q ON q.id = c.queen_id
                     LEFT JOIN ph_model_grid g ON g.id = q.grid_id
                  WHERE u."collectTime" > '2017-03-01') x ON date_part('hour'::text, x."collectTime") = date_part('hour'::text, dt.dt) AND
                        date_part('day'::text, x."collectTime") = date_part('day'::text, dt.dt) AND
                        date_part('month'::text, x."collectTime") = date_part('month'::text, dt.dt) AND
                        date_part('year'::text, x."collectTime") = date_part('year'::text, dt.dt)) a
WITH NO DATA;
-- legacy name to access renamed materialized view remove this when Looker no longer references it
CREATE OR REPLACE VIEW public.usage_raw_31_days AS select * from public.usage_raw_March_2017_days;