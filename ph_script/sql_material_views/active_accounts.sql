-- to refresh data  -- REFRESH MATERIALIZED VIEW [ CONCURRENTLY ] active_accounts with data;

-- View: public.active_accounts

DROP MATERIALIZED VIEW if exists public.active_accounts;

CREATE MATERIALIZED VIEW public.active_accounts TABLESPACE pg_default AS
 SELECT date(u."collectTime") AS date,
    m.name AS muni,
    g.name AS grid,
    q.number AS queen,
    cc.number AS circuit,
    acc.id AS account_id,
    max(( SELECT count(*) AS count
           FROM ph_model_project p,
            ph_model_grid g_1,
            ph_model_municipality m_1,
            ph_model_customer c_1
          WHERE p.id = g_1.project_id AND g_1.name::text = m_1.name::text AND c_1.municipality_id = m_1.id
                AND c_1.circuit_id IS NOT NULL AND c_1.days_no_balance < 90 AND p.name::text = 'Cloverfield'::text)) AS cloverfield_connected_cnt,
    round(sum(COALESCE(uc.amount, 0::numeric)), 4) AS sum_amt,
    round(sum(
        CASE
            WHEN acc.id <> ALL (ARRAY[42033, 41975]) THEN COALESCE(uc.amount, 0::numeric)
            ELSE 0.0
        END), 4) AS sum_amt_exclude_acc_42033_41975,
    sum(
        CASE
            WHEN COALESCE(uc.amount, 0::numeric) > 0.0 THEN
            CASE
                WHEN uc."perSegmentChargeComponent" > 0::numeric THEN
                CASE
                    WHEN uc.amount >= uc."perSegmentChargeComponent" THEN round(COALESCE(uc.amount - uc."perSegmentChargeComponent", 0::numeric), 4)
                    ELSE round(COALESCE(uc.amount, 0::numeric), 4)
                END
                ELSE round(COALESCE(uc.amount, 0::numeric), 4)
            END
            ELSE 0::numeric
        END) AS var_amt,
    sum(
        CASE
            WHEN COALESCE(uc.amount, 0::numeric) > 0.0 AND (acc.id <> ALL (ARRAY[42033, 41975])) THEN
            CASE
                WHEN uc."perSegmentChargeComponent" > 0::numeric THEN
                CASE
                    WHEN uc.amount >= uc."perSegmentChargeComponent" THEN round(COALESCE(uc.amount - uc."perSegmentChargeComponent", 0::numeric), 4)
                    ELSE round(COALESCE(uc.amount, 0::numeric), 4)
                END
                ELSE round(COALESCE(uc.amount, 0::numeric), 4)
            END
            ELSE 0::numeric
        END) AS var_amt_exclude_acc_42033_41975,
    round(sum(COALESCE(u."intervalWh", 0::numeric)), 4) AS wh,
    round(sum(
        CASE
            WHEN acc.id <> ALL (ARRAY[42033, 41975]) THEN COALESCE(u."intervalWh", 0::numeric)
            ELSE 0.0
        END), 4) AS wh_exclude_acc_42033_41975,
    round(max(COALESCE(uc."perSegmentChargeComponent", 0::numeric)), 4) AS max_fixed,
    sum(
        CASE
            WHEN COALESCE(u."intervalWh", 0::numeric) = 0.0 THEN 1
            ELSE 0
        END) AS active_fixed,
    sum(
        CASE
            WHEN COALESCE(u."intervalWh", 0::numeric) > 0.0 THEN 1
            ELSE 0
        END) AS active_variable
   FROM ph_model_account acc
     LEFT JOIN ph_model_customer c ON acc.id = c.account_id
     LEFT JOIN ph_model_circuit cc ON cc.id = c.circuit_id
     LEFT JOIN ph_model_queen q ON cc.queen_id = q.id
     LEFT JOIN ph_model_grid g ON g.id = q.grid_id
     LEFT JOIN ph_model_municipality m ON m.id = c.municipality_id
     LEFT JOIN ph_model_usage u ON u.circuit_id = c.circuit_id
     LEFT JOIN ph_model_usagecharge uc ON u.id = uc.usage_id
  WHERE date(u."collectTime") > '2017-03-08'::date
  GROUP BY (date(u."collectTime")), m.name, g.name, q.number, cc.number, acc.id
WITH NO DATA;