# coding=utf-8
"""Netsuite CSV file generation"""
import sys
from datetime import datetime, timedelta
from django.conf import settings
from ph_util.test_utils import test_mode
# noinspection PyCompatibility
from powerhive.common_settings import DEFAULT_ACCOUNTING_EMAILS, TEST_EMAILS, ACCOUNTING_TEST

# noinspection PyCompatibility
reload(sys)
sys.setdefaultencoding('utf-8')

DB_ALIAS = 'replica'  # Use the read replica DB
DEFAULT_DATE = '2222-02-22'
EOL = '\n'
SEPERATOR = ','
DATE_FORMAT = '%Y-%m-%d'
credit_fn = 'credit_XXX.csv'
debit_fn = 'debit_XXX.csv'
cust_fn = 'customer_XXX.csv'


def monthy_accounting_csv(
        start_dt_str=DEFAULT_DATE,
        end_dt_str=DEFAULT_DATE,
        emails=DEFAULT_ACCOUNTING_EMAILS,
        test_dt=datetime.utcnow()):
    """
    Generates monthly CSV files for debit, credit, and customers, then emails it to someone.
    The report aggregates debt invoices over muni_name, acc_id, and trans_code.
    Credit and customer reports do not aggregate anything.
    If there is an error the email may not of been sent, and this should be rerun.
    Note that if this function is called without dates, on the 1st of any month, it will generate a report for the previous month,
    Otherwise it uses the dates given. If no dates are given and it is not the 1st of the month, yesterday's data is fetched.

    :param start_dt_str:    string representing start date (it can include time, but time will be dropped) Format: 'YYYY-MM-DD' or 'YYYY-MM-DD HH:MM'
    :param end_dt_str:      string representing end date (it can include time)
    :param emails:          array of email addresses to send to
    :param test_dt:         if provided it will be used to determine report data range
    :return:                tuple of error, credit_data, debit_data, cust_data, and email subject

    """

    from django import db
    conn = None
    error = None
    credit_data = None
    debit_data = None
    cust_data = None
    chk_dt = test_dt

    # set the data range
    start_dt_str, end_dt_str = set_dates(chk_dt, start_dt_str, end_dt_str)

    subject = "CSV's generated at " + str(datetime.utcnow()) + " UTC  start date: " + start_dt_str + "   end date: " + end_dt_str
    print(subject)
    try:
        conn = db.connections[DB_ALIAS]
        cur = conn.cursor()

        credit_data = credit_header()
        cur.callproc('credit', (start_dt_str, end_dt_str))
        credit_data = process_rows(cur, credit_data)
        #print('credit')
        debit_data = aggr_debit_header()
        cur.callproc('aggr_debit', (start_dt_str, end_dt_str))
        debit_data = process_rows(cur, debit_data)
        #print('debit')
        cust_data = cust_header()
        cur.callproc('customers', (start_dt_str, end_dt_str))
        cust_data = process_rows(cur, cust_data)
        #print('cust')
        # close the DB cursor
        cur.close()
        send_email(subject, emails, credit_data, cust_data, debit_data, end_dt_str)
        #print('done')

    except Exception as err:  # pragma: no cover
        error = err
        print("!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!\n%s\n!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!\n" % str(err))
    finally:
        if conn is not None:  # pragma: no cover
            conn.close()
        return error, credit_data, debit_data, cust_data, subject


def cust_header():
    """
    Set CSV header
    :return: Customer CSV header
    """
    return \
        'version' + SEPERATOR + \
        'acc_id' + SEPERATOR + \
        'created_dt' + SEPERATOR + \
        'updated_dt' + SEPERATOR + \
        'customer_type' + SEPERATOR + \
        'first_name' + SEPERATOR + \
        'last_name' + SEPERATOR + \
        'national_id_number' + SEPERATOR + \
        'phone_number' + SEPERATOR + \
        'status' + SEPERATOR + \
        'acc_balance' + SEPERATOR + \
        'tariff_name' + SEPERATOR + \
        'country,reqion' + SEPERATOR + \
        'project' + SEPERATOR + \
        'muni_name' + SEPERATOR + \
        'queen_number' + EOL


def aggr_debit_header():
    """
    Set CSV header
    :return: Debit CSV header
    """
    return \
        'version' + SEPERATOR + \
        'acc_id' + SEPERATOR + \
        'muni_name' + SEPERATOR + \
        'process_dt' + SEPERATOR + \
        'trans_id' + SEPERATOR + \
        'trans_code' + SEPERATOR + \
        'amt_shillings' + SEPERATOR + \
        'cnt_invoices' + SEPERATOR + \
        'memo' + EOL


def credit_header():
    """
    Set CSV header
    :return: Credit CSV header
    """
    return \
        'version' + SEPERATOR + \
        'trans_id' + SEPERATOR + \
        'process_dt' + SEPERATOR + \
        'acc_id' + SEPERATOR + \
        'memo' + SEPERATOR + \
        'trans_code' + SEPERATOR + \
        'amt_shillings' + SEPERATOR + \
        'provider' + SEPERATOR + \
        'reason' + SEPERATOR + \
        'muni_name' + EOL


def debit_header():
    """
    Set CSV header
    :return: Debit CSV header
    """
    return \
        'version' + SEPERATOR + \
        'trans_id' + SEPERATOR + \
        'process_dt' + SEPERATOR + \
        'acc_id' + SEPERATOR + \
        'cust_type' + SEPERATOR + \
        'trans_str' + SEPERATOR + \
        'trans_code' + SEPERATOR + \
        'amt_shillings' + SEPERATOR + \
        'memo' + SEPERATOR + \
        'loan_id' + SEPERATOR + \
        'loan_type' + SEPERATOR + \
        'loan_payment' + SEPERATOR + \
        'loan_balance' + SEPERATOR + \
        'payment_cnt' + SEPERATOR + \
        'payments_required' + SEPERATOR + \
        'loan_amt' + SEPERATOR + \
        'past_due' + SEPERATOR + \
        'reason' + SEPERATOR + \
        'muni_name' + EOL


def send_email(subject, emails, credit_data, cust_data, debit_data, end_dt_str):
    """
    Send emails to recipients with CSV files attached if not in test mode

    :param credit_data:     credit CSV data
    :param cust_data:       customer  CSV data
    :param debit_data:      debit  CSV data
    :param emails:          set of email addresses to send to
    :param end_dt_str:      end date of data
    :param subject:         subject of email
    :return:
    """
    if test_mode(ACCOUNTING_TEST):  # pragma: no cover
        print('test emails')
        emails = TEST_EMAILS

    from django.core.mail import EmailMessage
    email = EmailMessage(
        subject,
        subject + '\nFind the CSV files attached',
        settings.SERVER_EMAIL,
        emails,
        reply_to=[settings.SERVER_EMAIL],
        headers={'Message-ID': 'credit debit customer csv'}
    )
    print(emails)
    email.attach(credit_fn.replace('XXX', end_dt_str), credit_data)
    email.attach(debit_fn.replace('XXX', end_dt_str), debit_data)
    email.attach(cust_fn.replace('XXX', end_dt_str), cust_data)
    return email.send()


def set_dates(default_dt, start_dt_str, end_dt_str):
    """
    if dates are specified then use them otherwise
    if today is the 1st of the month then set the dates to the previous month's data,
    otherwise set dates to yesterday's data

    :param default_dt:      default flags no dates specified
    :param end_dt_str:      end of the data range
    :param start_dt_str:    start of the data range
    :return:                tuple of start_dt_str, end_dt_str
    """
    if start_dt_str == DEFAULT_DATE == end_dt_str:
        if default_dt.day == 1:
            end_dt_str = default_dt.strftime(DATE_FORMAT)
            try:
                default_dt = default_dt.replace(month=default_dt.month - 1)
            except ValueError:
                if default_dt.month == 1:
                    default_dt = default_dt.replace(month=12)
                    default_dt = default_dt.replace(year=default_dt.year - 1)
                else:
                    raise Exception('WTF this should never happen')  # pragma: no cover
            start_dt_str = default_dt.strftime(DATE_FORMAT)
        else:
            end_dt_str = default_dt.strftime(DATE_FORMAT)
            start_dt_str = (default_dt - timedelta(1)).strftime(DATE_FORMAT)
    return start_dt_str, end_dt_str


def chk_date(val):
    """
    changes a YYYY-MM-DD format string to DD/MM/YYYY string
    :param val:     date string
    :return:        date string or original if not correct format
    """
    if len(val) == 10 and val[:2] == '20':
        dt_parts = val.split('-')
        if len(dt_parts) == 3:
            return dt_parts[2] + '/' + dt_parts[1] + '/' + dt_parts[0]
    return val


def process_rows(cur, data):
    """
    Go through all the rows in the cursor and update the data
    Exlude Berkeley customers
    Truncate certain strings to 20 chars --- need Brody to verify which ones
    Remove -CF from pilot customers

    :param cur:     cursor
    :param data:    comma seperated values
    :return:        data appended with row data
    """
    row = cur.fetchone()
    while row is not None:
        i = 0
        len_data = len(row)
        if "Berkeley" not in str(row):
            while i < len_data:
                # replace these characters as they will mess up the CSV
                val = str(row[i]).replace(EOL, '').replace(SEPERATOR, ' ')
                val = val.replace("-CF", "")
                if val != "None":
                    val = chk_date(val)
                    data += val
                if i == len_data - 1:
                    data += EOL
                else:
                    data += SEPERATOR
                i += 1
        row = cur.fetchone()
    return data
