# coding=utf-8
""""
List of cron job for this applications.
Run by: python manage.py runcrons, Needs to be run by os cron runner.
E.g in linux
crontab -e
# Add line
1 2 3 4 5 /path/to/manage.py runcrons
"""

import django_cron

from ph_operation.sms_manager import db_maintenance
from ph_script.operational_first_usage_cron import OperationalFirstUsage
from ph_script import netsuite_csv
from ph_util.db_utils import process_materialized_views

WEEK_IN_MINUTES = 24 * 60  # 20160
DAILY_IN_MINUTES = 1440
# uses server time which is currently PST 2017-10-20
RUN_AT_TIMES = ['12:00']
RUN_AT_TIMES_100 = ['1:00']
RETRY_AFTER_FAILURE_MINS_100 = 60
RETRY_AFTER_FAILURE_MINS = 15


# noinspection PyMethodMayBeStatic
class FirstSolarByWeeklyReportCron(django_cron.CronJobBase):
    """Run tariff of unprocessed usages.
       python manage.py runcrons "ph_script.cron.FirstSolarByWeeklyReportCron"
    """

    RUN_EVERY_MINS = WEEK_IN_MINUTES
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'First-solar-by-weekly-report'

    def do(self):
        """

        :return:
        """
        pass
        # TODO disabled per Rik
        # report.generate_report()


# noinspection PyMethodMayBeStatic
class NetSuiteDailyCsvCron(django_cron.CronJobBase):
    """Run the csv generator.
        python manage.py runcrons "ph_script.cron.NetSuiteDailyCsvCron"
    """
    schedule = django_cron.Schedule(run_at_times=RUN_AT_TIMES, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    code = 'Net-suite-daily-csv'

    def do(self):
        """generate csv files for netsuite"""
        netsuite_csv.monthy_accounting_csv()


class DailyMaintenanceCron(django_cron.CronJobBase):
    """Creates loans to pay the monthly tariff fixed fee for commercial customers
        python manage.py runcrons "ph_script.cron.DailyMaintenanceCron"
    """

    schedule = django_cron.Schedule(run_at_times=RUN_AT_TIMES_100, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS_100)
    code = 'ph-script-daily-maintenance'

    @staticmethod
    def do():
        """ this should only run once per day """
        db_maintenance()


class FirstUsageCheckCron(django_cron.CronJobBase):
    """Perform accounting actions when a customer first uses power on an operational grid
        python manage.py runcrons "ph_script.cron.FirstUsageCheckCron"
    """
    RUN_EVERY_MINS = 6 * 60
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-first-use-view-update'

    @staticmethod
    def do():
        """update master transaction table"""
        OperationalFirstUsage.check_first_usage(False, False)
        process_materialized_views(['REFRESH MATERIALIZED VIEW active_accounts'])
