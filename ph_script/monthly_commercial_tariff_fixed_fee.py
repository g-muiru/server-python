# coding=utf-8
"""Monthly Account Statement Manager"""
import logging

from ph_model.models import Customer
from ph_model.models import Loan
from ph_model.models.customer import CustomerType
from ph_util.accounting_util import add_vat, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, \
    COMMERCIAL_FIXED_TARIFF_START_DAY, AccountingEnums

from ph_util.date_util import get_utc_now
from dateutil.relativedelta import relativedelta

from ph_util.loan_utils import create_loan

LOGGER = logging.getLogger(__name__)


class MonthlyCommercialTariffFixedFee(object):
    """Creates loans to pay the monthly tariff fixed fee for commercial customers"""

    @classmethod
    def monthly_check(cls, today=get_utc_now()):
        """
        Check for commercial loans for fixed fee, and create them if necessary.
        Note that the grid or queen operational_date must be today or in the past.
        :param today:   date to check
        :return:        [] if not time to process or no loans created, otherwise a list of loan id's created
        """
        id_list = []
        if today.day < 4:
            LOGGER.debug("check for commercial tariff loans starting next month, if none make one for all commercial custs")
            start_date = (today + relativedelta(months=1)).replace(day=COMMERCIAL_FIXED_TARIFF_START_DAY)

            for cust in Customer.objects.filter(customerType__in=[CustomerType.COMMERCIAL.name, CustomerType.COMMERCIAL_II.name]):
                cnt = Loan.objects.filter(loanType__in=[str(AccountingEnums.COM2_FIXED_FEEc232), str(AccountingEnums.COM1_FIXED_FEEc231)]). \
                    filter(account_id=cust.account_id). \
                    filter(startDate__gte=start_date).count()
                if cnt == 0 \
                        and cust.circuit_id is not None \
                        and (cust.circuit.queen.grid.operational_date <= today.date() or cust.circuit.queen.operational_date <= today.date()):
                    if cust.customerType == CustomerType.COMMERCIAL_II.name:
                        amt = COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT
                        loan_type = AccountingEnums.COM2_FIXED_FEEc232
                    else:
                        amt = COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT
                        loan_type = AccountingEnums.COM1_FIXED_FEEc231
                    loan = create_loan(
                        loan_cls=Loan,
                        loan_type=loan_type,
                        acc=cust.account,
                        loan_amt=amt,
                        start_date=start_date,
                        payment=add_vat(amt))
                    id_list.append(loan.pk)
        else:
            LOGGER.debug("it's not time to run this yet")

        return id_list
