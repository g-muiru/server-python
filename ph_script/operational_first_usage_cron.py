# coding=utf8
"""Create connection fee loan, add bonus power, and set account balance"""
import logging
import traceback
from django import db
from ph_model.models import Customer
from ph_util.loan_utils import create_connection_fee_loan, create_connection_fee_loan_one_payment

LOGGER = logging.getLogger(__name__)

DB_ALIAS = 'default'  # Use the read replica DB


class OperationalFirstUsage(object):
    """Creates loans to give free power to customers."""

    @classmethod
    def check_first_usage(cls, no_deposit=False, force=False, db_alias=DB_ALIAS):
        """
        Check for bonus power loans, and set start date if needed

        :param no_deposit:  True means only make loans not deposit charges
        :param force:       True means make the loan no matter what
        :param db_alias:    Needed for testing
        :return:            [] if no loans repaid, otherwise a list of loan id's created
        """

        conn = None
        sql = "select distinct acc_id from operational_first_use where connection_loan = true and conn_loan_exists = false  and ttl_wh > 1;"
        LOGGER.debug("check for customers that requested connection loans but do not have them yet")
        acc_id_list = []

        def extract_ids(cursor):
            """
            Expects cursor to return account_ids as first column 
            :param cursor:  Executed cursor
            :return:        list of ids
            """
            id_list = []
            r = cursor.fetchone()
            while r is not None:
                id_list.append(r[0])
                r = cursor.fetchone()
            return id_list

        try:
            conn = db.connections[db_alias]
            cur = conn.cursor()
            cur.execute(sql)
            acc_id_list = extract_ids(cur)

            for acc_id in acc_id_list:
                cust = Customer.objects.get_or_none(account_id=acc_id)
                create_connection_fee_loan(cust, no_deposit, force)

            sql = "select distinct acc_id from operational_first_use where connection_loan = false and conn_loan_exists = false and ttl_wh > 1;"
            LOGGER.debug("check for customers that did not request connection loans")
            cur.execute(sql)
            acc_id_list = extract_ids(cur)

            for acc_id in acc_id_list:
                cust = Customer.objects.get_or_none(account_id=acc_id)
                create_connection_fee_loan_one_payment(cust)
        except Exception as err:  # pragma: no cover
            traceback.print_exc()
            print("!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!\n%s\n!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!\n" % str(err))
        finally:
            if conn is not None:  # pragma: no cover
                if db_alias != 'default':
                    conn.close()
            return acc_id_list
