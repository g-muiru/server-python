# coding=utf-8
"""Unit Test the monthly CSV functions --- nodb"""
import datetime
import re

import pytz
from django.test import SimpleTestCase
from ddt import ddt, unpack, idata
# noinspection PyCompatibility
import StringIO
from ph_script.netsuite_csv import set_dates, DEFAULT_DATE, DATE_FORMAT, SEPERATOR, EOL, credit_header, aggr_debit_header, cust_header, debit_header
from ph_util.test_utils import assert_eq, get_sql_from_files


@ddt
class NetSuiteTestCase(SimpleTestCase):
    """ Class to test the Tariff models """

    # data to run tests multiple times, self.cust.lastUsageProcessedTime & self.cust.municipality.timeZone
    last_dt_tz = (
        # no month or year rollover
        (datetime.datetime(2014, 1, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         None,
         None,
         datetime.datetime(2013, 12, 1),
         datetime.datetime(2014, 1, 1)),
        (datetime.datetime(2016, 1, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         None,
         None,
         datetime.datetime(2016, 12, 1),
         datetime.datetime(2016, 1, 1)),
    )
    # data to run tests multiple times
    no_start_end_dates = (
        (datetime.datetime(2014, 1, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2013, 12, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 1, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2016, 1, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2015, 12, 1).strftime(DATE_FORMAT),
         datetime.datetime(2016, 1, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 1, 31, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 1, 30).strftime(DATE_FORMAT),
         datetime.datetime(2014, 1, 31).strftime(DATE_FORMAT)),
        (datetime.datetime(2016, 1, 31, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2016, 1, 30).strftime(DATE_FORMAT),
         datetime.datetime(2016, 1, 31).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 2, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 1, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 2, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 3, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 2, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 3, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 4, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 3, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 4, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 5, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 4, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 5, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 6, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 5, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 6, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 7, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 6, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 7, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 8, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 7, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 8, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 9, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 8, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 9, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 10, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 9, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 10, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 11, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 10, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 11, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 12, 1, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 11, 1).strftime(DATE_FORMAT),
         datetime.datetime(2014, 12, 1).strftime(DATE_FORMAT)),
        (datetime.datetime(2014, 12, 11, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2014, 12, 10).strftime(DATE_FORMAT),
         datetime.datetime(2014, 12, 11).strftime(DATE_FORMAT)),
        (datetime.datetime(2016, 2, 29, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         DEFAULT_DATE,
         DEFAULT_DATE,
         datetime.datetime(2016, 2, 28).strftime(DATE_FORMAT),
         datetime.datetime(2016, 2, 29).strftime(DATE_FORMAT)),
    )
    start_end_dates = (
        (datetime.datetime(2017, 10, 5, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         datetime.datetime(2017, 9, 1).strftime(DATE_FORMAT),
         datetime.datetime(2017, 9, 4).strftime(DATE_FORMAT),
         datetime.datetime(2017, 9, 1).strftime(DATE_FORMAT),
         datetime.datetime(2017, 9, 4).strftime(DATE_FORMAT)),
        (datetime.datetime(2016, 1, 31, hour=11, minute=59, second=59, tzinfo=pytz.utc),
         datetime.datetime(2016, 2, 29).strftime(DATE_FORMAT),
         datetime.datetime(2016, 3, 4).strftime(DATE_FORMAT),
         datetime.datetime(2016, 2, 29).strftime(DATE_FORMAT),
         datetime.datetime(2016, 3, 4).strftime(DATE_FORMAT)),
    )

    # noinspection PyArgumentList,PyArgumentList
    @idata(no_start_end_dates)
    @unpack
    def test_set_dates_no_start_end(self, default_dt, start_date_str, end_date_str, expect_start_str, expect_end_str):
        """
        Ensure start_dt_str and end_dt_str get set correctly based on default_dt

        :param default_dt:      Datetime that determines how start_dt_str and end_dt_str get set

        :return:                true on success
        """
        start, end = set_dates(default_dt, start_date_str, end_date_str)
        assert_eq(expect_start_str, start)
        assert_eq(expect_end_str, end)

    # noinspection PyArgumentList,PyArgumentList
    @idata(start_end_dates)
    @unpack
    def test_set_dates_with_start_end(self, default_dt, start_date_str, end_date_str, expect_start_str, expect_end_str):
        """
        Ensure start_dt_str and end_dt_str get set correctly

        :param default_dt:      Datetime that determines how start_dt_str and end_dt_str get set

        :return:                true on success
        """
        start, end = set_dates(default_dt, start_date_str, end_date_str)
        assert_eq(expect_start_str, start)
        assert_eq(expect_end_str, end)

    def test_all_headers(self):
        """
        Make sure All CSV headers are formated correctly
        :return:
        """
        sql = get_sql_from_files("/ph_script/sql")

        sr = ["CREATE OR REPLACE FUNCTION public.debit", "RETURNS TABLE", "LANGUAGE"]
        q = self.get_columns(sql, sr)
        assert_eq(debit_header(), q)

        sr = ["CREATE OR REPLACE FUNCTION public.aggr_debit", "RETURNS TABLE", "LANGUAGE"]
        q = self.get_columns(sql, sr)
        assert_eq(aggr_debit_header(), q)

        sr = ["CREATE OR REPLACE FUNCTION public.customers", "RETURNS TABLE", "LANGUAGE"]
        q = self.get_columns(sql, sr)
        assert_eq(cust_header(), q)

        sr = ["CREATE OR REPLACE FUNCTION public.credit", "RETURNS TABLE", "LANGUAGE"]
        q = self.get_columns(sql, sr)
        assert_eq(q, credit_header())

    @staticmethod
    def get_columns(sql, srch):
        """
        Get the column names from SQL
        Expects SQL to be formated with each column and each string in srch to be on it's own line,
        :param sql: Full SQL
        :param srch:  Array of strings to identify where column names start and end
        :return:    Comma seperated string of column names
        """
        idx = 0
        q = ""
        s = StringIO.StringIO(sql)
        for line in s:
            if idx == 3:
                break
            if idx == 2:
                if srch[idx] in line:
                    q = q[:-1] + EOL
                else:
                    q += re.findall(r'\S+', line)[0] + SEPERATOR
            if srch[idx] in line:
                idx += 1
        return q
