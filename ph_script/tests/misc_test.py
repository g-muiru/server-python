# coding=utf-8
"""Netsuite CSV file generation"""
import sys
from datetime import datetime, timedelta

from decimal import Decimal
from django.conf import settings

from ph_model.models import Loan, Account
from ph_util.accounting_util import KUKU_POA_SUBSCRIPTION_5_DAYS, KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT, str_obj
from ph_util.loan_utils import create_payment_plan_no_vat
from ph_util.test_utils import test_mode
# noinspection PyCompatibility
from powerhive.common_settings import DEFAULT_ACCOUNTING_EMAILS, TEST_EMAILS

# noinspection PyCompatibility
reload(sys)
sys.setdefaultencoding('utf-8')



from ph_util.date_util import get_utc_now, str_to_dt


def thous_check(today=get_utc_now()): #pragma no cover
    # acc id
    ids = (
        43335,
        43338,
    )

    return -1
    exit(1)

    CRED_ONLY = False

    id_list = []
    none_list = []
    start_date = today
    for id in ids:
        from ph_model.models import Customer
        cust = Customer.objects.get_or_none(account_id=id)
        from ph_model.models.municipality import Municipality
        muni = Municipality.objects.get(id=cust.municipality_id)
        from ph_model.models.user import User
        user = User.objects.all()[0]
        from ph_model.models import Loan
        ###### l = Loan.objects.get_or_none(account_id=id, loanAmount=1000)
        l = Loan.objects.get(account_id=id)

        if cust is not None:

            # print(id, l.loanAmount, cust.connectionFeeLoan)
            dt = muni.connectionFeeDueDate
            if l is not None and l.loanAmount == 2000 and CRED_ONLY != True:
                id_list.append(id)
                print("2000 ", id, l.startDate.date(), cust.connectionFeeLoan, l.loanAmount)
                # l.startDate = dt
                # l.save()
                if cust.connectionFeeDue != dt:
                    cust.connectionFeeDue = dt
                    ####print(cust.connectionFeeDue, id)
                    cust.save()
                # make 1000 Kes loan 1 payment
                from ph_util.accounting_util import create_loan
                from ph_util.accounting_util import LoanType
                from ph_util.accounting_util import add_vat
                loan = create_loan(
                    loan_cls=Loan,
                    loan_type=LoanType.CONNECTION_FEE_LOAN.name,
                    acc=cust.account,
                    loan_amt=1000,
                    start_date=start_date,
                    payment=add_vat(1000),
                    notes='The 1160 Kes connection fee customers are suppose to deposit to get a connection payment plan')
                from ph_operation import loan_manager

                loan_handler = loan_manager.Loan(loan)
                print(str_obj(loan_handler))
                loan_handler.repay_loan()
                # make cred adjust 480 Kes
            else:
                if l is None:
                    none_list.append(id)
                    continue
                else:
                    id_list.append(id)
                    print ("JUST CRED ", id, l.startDate.date(), cust.connectionFeeLoan, l.loanAmount)
                    # make cred adjust 480 Kes

            from ph_model.models.transaction import TransactionType
            t_type = TransactionType.CREDIT.value
            from ph_operation import transaction_manager
            transaction_type = transaction_manager.Credit

            transaction = transaction_type(cust.account, 480)
            transaction.process()
            from ph_model.models import CreditAdjustmentHistory
            adjustment_history = CreditAdjustmentHistory(
                account=cust.account,
                user=user,
                reason='Welcome gift',
                amount=480,
                processedTime=get_utc_now(),
                transactionType=t_type,
                adj_type='Marketing'
            )
            adjustment_history.save()
            """
            amount = 3000
            if l is not None and l.loanAmount == 1000:
                print("\n22222222222222222222222222222222222\n")
                amount = 2000
            if muni.connectionFeeDueDate is None:
                muni.connectionFeeDueDate = start_date + timedelta(days=30)
                print("\nMMMMMMMMMMMM\n%s\nMMMMMMMMMMMMMMMMM\n" % muni.connectionFeeDueDate)
                muni.save()
            if cust.connectionFeeDue != muni.connectionFeeDueDate:
                cust.connectionFeeDue = muni.connectionFeeDueDate
                print(cust.connectionFeeDue, id)
                cust.save()
                cust = Customer.objects.get_or_none(account_id=id)
            else:
                print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            print(cust.connectionFeeLoan,id, cust.firstName,  amount)
            print(cust.connectionFeeDue, muni.connectionFeeDueDate, cust.connectionFeeDue == muni.connectionFeeDueDate )


            from ph_util.accounting_util import create_loan
            from ph_util.accounting_util import LoanType
            from ph_util.accounting_util import add_vat
            from ph_util.accounting_util import RESIDENTIAL_CONN_LOAN_AMT
            conn_loan = RESIDENTIAL_CONN_LOAN_AMT
            from ph_model.models.customer import CustomerType
            if cust.customerType == CustomerType.COMMERCIAL.name:
                from ph_util.accounting_util import COMMERCIAL_CONN_LOAN_AMT
                conn_loan = COMMERCIAL_CONN_LOAN_AMT

            days = (muni.connectionFeeDueDate.date() - today.date()).days
            if days < 1:
                days = 1
            daily_fee = add_vat(conn_loan) / days
            print('\nDDDDDDDDDDDDDDDDDDDD\n',start_date.date(),muni.connectionFeeDueDate.date(),days,daily_fee)

            loan = create_loan(loan_cls=Loan,
                               loan_type=LoanType.CONNECTION_FEE_LOAN.name,
                               acc=cust.account,
                               loan_amt=conn_loan,
                               start_date=start_date,
                               payment=daily_fee)
            id_list.append(loan.pk)
            """

        else:
            print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', id)
            """
            from ph_operation import loan_manager
            loan_handler = loan_manager.Loan(l)
            print(str_obj(loan_handler))
            loan_handler.repay_loan()
            """

    print('\n@@@@@@@@@@@@@@@@@\n%s\n%s\n@@@@@@@@@@@@@@@\n' % (id_list, none_list))
    return id_list


def refund_kirwa(): #pragma no cover
    # acc id
    ids = (
        41954,
        41955,
        41957,
        41958,
        41960,
        41961,
        41962,
        41963,
        41969,
        41970,
        41971,
        41975,
        41976,
        41977,
        41978,
        41980,
        41982,
        41983,
        41986,
        41989,
        41994,
        42005,
        42007,
        42008,
        42009,
        42013,
        42014,
        42017,
        42019,
        42020,
        42032,
        42033,
        42034,
        42035,
        42039,
        42052,
        42055,
        42056,
        42062,
        42063,
        42065,
        42070,
        42073,
        42076,
        42077,
        42078,
        42081,
        42085,
        42087,
        42091,
        42097,
        42407,
        42423,
        42429,
        42434,
        42457,
        42458,
        42463,
        42464,
        42465,
        42466,
        42470,
        42474,
        42475,
        42476,
        42478,
        42479,
        42487,
        42492,
        42504,
        42505,
        42506,
        42507,
        42508,
        42509,
        42512,
        42514,
        42530,
        42531,
        42716,
        42718,
        42805,
        42854,
        42869,
        42892,
        42893,
        42896,
        42897,
        42898,
        42899,
        42901,
        42902,
        42904,
        44038,
        44040,
        44041,
        44043,
        44048,
        44264,
        44265,
        44298,
        45121,
        41966,
        41967,
        41972,
        41974,
        41979,
        41984,
        41985,
        41988,
        41990,
        41991,
        41992,
        41995,
        41996,
        41998,
        41999,
        42000,
        42001,
        42021,
        42024,
        42027,
        42029,
        42031,
        42038,
        42048,
        42049,
        42051,
        42054,
        42059,
        42060,
        42067,
        42068,
        42072,
        42074,
        42075,
        42079,
        42080,
        42082,
        42084,
        42089,
        42093,
        42095,
        42098,
        42099,
        42425,
        42426,
        42435,
        42455,
        42460,
        42461,
        42462,
        42468,
        42469,
        42471,
        42477,
        42480,
        42481,
        42482,
        42483,
        42484,
        42486,
        42488,
        42489,
        42496,
        42499,
        42510,
        42516,
        42681,
        42686,
        42688,
        42717,
        42867,
        42868,
        42870,
        42871,
        42882,
        42883,
        42894,
        42900,
        42905,
        42906,
        42907,
        44044,
        44049,
        44257,
        44258,
        44259,
        44260,
        44261,
        44299,
        45172,
        45173
    )

    amt = (
        2059.1,
        6949.1,
        359.1,
        2379.1,
        2424.1,
        3100,
        2600,
        2420,
        3000,
        5045,
        2359.1,
        3019.1,
        1940,
        4061.1,
        2829.1,
        2769,
        3530,
        1200,
        2359.1,
        3059.1,
        5148,
        2359.1,
        3130,
        4093.1,
        2659.1,
        2779,
        2279,
        2659.1,
        2910,
        4644.1,
        4358,
        4000,
        3500,
        3800,
        3050,
        3280,
        2359.1,
        359.1,
        3000,
        2829.1,
        2170,
        3628,
        3000,
        4400,
        2700,
        1000,
        2359.1,
        3000,
        1000,
        3420,
        5159.1,
        2029.1,
        3600,
        359.1,
        3080,
        3000,
        359.1,
        2359.1,
        2944,
        3500,
        2357.1,
        1230,
        834.1,
        2728,
        3000,
        409.1,
        3200,
        1525,
        2359.1,
        3400,
        3734.1,
        1000,
        2709.1,
        3500,
        359.1,
        4168,
        3464.1,
        2359.1,
        1670,
        4100,
        1129.1,
        2359.1,
        2339.1,
        3659.1,
        859.1,
        1970,
        3100,
        3449,
        2219,
        2930,
        879.1,
        2459.1,
        2759.1,
        3300,
        1500,
        3000,
        1527,
        3029.1,
        3090,
        1000,
        1749.1,
        4530,
        2200,
        2970,
        3300,
        3300,
        2970,
        3000,
        3000,
        3800,
        3020,
        4470,
        3180,
        1500,
        3000,
        3303,
        3000,
        1100,
        1000,
        3620,
        3339,
        4621,
        1200,
        3000,
        1543,
        3100,
        3875,
        3800,
        3707,
        2678,
        3100,
        1000,
        1470,
        1570,
        1470,
        2995,
        1000,
        3000,
        1900,
        3000,
        3020,
        2700,
        3250,
        1400,
        3600,
        4150,
        4040,
        3440,
        3000,
        4420,
        3000,
        2970,
        3000,
        3100,
        2680,
        1200,
        1170,
        2300,
        2160,
        1050,
        1470,
        1000,
        1000,
        2000,
        2940,
        3040,
        2175,
        3000,
        4500,
        1500,
        2300,
        3000,
        3000,
        3850,
        1000,
        3100,
        3000,
        3000,
        1020,
        2270,
        3100,
        3000,
        1020,
        3000,
        3000,
        1000,
        3000,
        2000,
        3100,
        3100,
        3000,
        3000,
        2070
    )

    return -1
    exit(1)

    from ph_model.models.user import User
    user = User.objects.all()[0]
    idx = -1
    for id in ids:
        idx += 1
        from ph_model.models import Customer
        c = Customer.objects.get_or_none(account_id=ids[idx])


        """
        from ph_model.models import Customer
        c = Customer.objects.get_or_none(account_id=ids[idx])

        print(id, ids[idx], amt[idx], idx, c.account.accountBalance)
        print("\n!!!!!!!!!!!!!!!!!1\n")

        from ph_model.models.transaction import TransactionType
        t_type = TransactionType.CREDIT.value
        from ph_operation import transaction_manager
        transaction_type = transaction_manager.Credit

        aaamt = Decimal(Decimal(amt[idx]) - Decimal(c.account.accountBalance))
        abs_amt = abs(aaamt)
        if amt < 0:
            print('DEBIT   ', abs_amt)
            t_type = TransactionType.DEBIT.value
            transaction_manager.Debit(c.account, abs_amt)
        else:
            print('CREDIT  ',abs_amt)
            t_type = TransactionType.CREDIT.value
            transaction = transaction_manager.Credit(c.account, abs_amt)

        # this SMS stuff needs to change depending on what we are doing
        message = '%s Acc Balance: %s a Refund for %s has been credited' % (
        c.firstName, str(round(c.account.accountBalance, 2)), str(round(amt[idx], 2)))
        from ph_operation import sms_manager
        #send SMS
        #sms_manager.CustomerSMSManager(cust.id, message).send()
        print(cust.id, message)
        #raw_input("Press Enter to continue...")
        
        transaction.process()
        from ph_model.models import CreditAdjustmentHistory
        adjustment_history = CreditAdjustmentHistory(
            account=c.account,
            user=user,
            reason='Adjust Kirwa C customer balance to total of deposits',
            amount=abs_amt,
            processedTime=get_utc_now(),
            transactionType=t_type,
            adj_type='Marketing'
        )
        adjustment_history.save()
        print(str_obj(transaction))
        print(str_obj(adjustment_history))
        """

    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

    """
        message = '%s Acc Balance: %s a Refund for %s has been credited' % (
        cust.firstName, str(round(cust.account.accountBalance, 2)), str(round(amt[idx], 2)))
        from ph_operation import sms_manager
          send SMS
        sms_manager.CustomerSMSManager(cust.id, message).send()
        print(cust.id, message)
        raw_input("Press Enter to continue...")
        exit(555)
        

         credit adjust
        from ph_model.models.transaction import TransactionType
        t_type = TransactionType.CREDIT.value
        from ph_operation import transaction_manager
        transaction_type = transaction_manager.Credit

        transaction = transaction_type(cust.account, amt[idx])

        transaction.process()
        from ph_model.models import CreditAdjustmentHistory
        adjustment_history = CreditAdjustmentHistory(
            account=cust.account,
            user=user,
            reason='Refund all usage charges by dictate of Chris Horner',
            amount=amt[idx],
            processedTime=get_utc_now(),
            transactionType=t_type,
            adj_type='REFUND'
        )
        adjustment_history.save()
        print(str_obj(transaction))
        print(str_obj(adjustment_history))
    """


def pilot_1000(): #pragma no cover
    """
    set all pilot customers to a 1000 ksh balance
    :return:
    """


    ai = (
        45698,
        45697,
        45695,
        45694,
        45685,
        45684,
        45679,
        45669,
        45668,
        45664,
        45661,
        45660,
        45557,
        45554,
        45553,
        45550,
        45549,
        45548,
        45547,
        45545,
        45544,
        45543,
        45541,
        45539,
        45537,
        45536,
        45535,
        45534,
        45490,
        45481,
        45441,
        45438,
        45376,
        45375,
        45373,
        45300,
        45267,
        44090,
        44051,
        44050,
        44044,
        42908,
        42907,
        42900,
        42717,
        42711,
        42710,
        42709,
        42705,
        42704,
        42702,
        42698,
        42696,
        42695,
        42694,
        42690,
        42689,
        42688,
        42686,
        42674,
        42663,
        42662,
        42658,
        42652,
        42651,
        42649,
        42647,
        42643,
        42642,
        42640,
        42639,
        42635,
        42633,
        42632,
        42631,
        42623,
        42622,
        42615,
        42613,
        42610,
        42609,
        42607,
        42604,
        42603,
        42599,
        42589,
        42587,
        42581,
        42578,
        42576,
        42572,
        42571,
        42566,
        42565,
        42563,
        42561,
        42560,
        42558,
        42551,
        42546,
        42536,
        42526,
        42524,
        42523,
        42522,
        42521,
        42520,
        42518,
        42496,
        42486,
        42468,
        42455,
        42426,
        42425,
        42099,
        42076,
        42072,
        42067,
        42054,
        42051,
        42049,
        42048,
        42019,
        41999,
        41996,
        41995,
        41974,
        41967,
        41937,
        41720,
        41716,
        41714,
        41713,
        41710,
        41708,
        41707,
        41706,
        41705,
        41704,
        41703,
        41619,
        41617,
        41615,
        41614,
        41613,
        41609,
        41606,
        41603,
        41601,
    )

    from ph_model.models.user import User
    user = User.objects.all()[0]
    idx = -1

    from ph_model.models import Customer
    # custs = Customer.objects.filter(municipality_id=(2270942, 2270944, 2270943).filter(circuit_id=None)

    ###########for c in Customer.objects.all().filter(municipality_id__in=(2270942, 2270944, 2270943)).exclude(circuit_id=None):
    for id in ai:

        c = Customer.objects.get_or_none(account_id=id)
        idx += 1
        print(c.account_id, c.account.accountBalance, c.circuit_id)
        print (idx)


        from ph_model.models.transaction import TransactionType
        t_type = TransactionType.CREDIT.value
        from ph_operation import transaction_manager
        transaction_type = transaction_manager.Credit

        amt = 595
        abs_amt = abs(amt)
        if amt < 0:
            transaction_manager.Debit(c.account, abs_amt)
        else:
            transaction = transaction_manager.Credit(c.account, abs_amt)

        transaction.process()
        from ph_model.models import CreditAdjustmentHistory
        adjustment_history = CreditAdjustmentHistory(
            account=c.account,
            user=user,
            reason='Credit back Kuku Poa fees 595 Ksh',
            amount=abs_amt,
            processedTime=get_utc_now(),
            transactionType=t_type,
            adj_type='REFUND'
        )
        adjustment_history.save()
        #print(str_obj(transaction))
        #print(str_obj(adjustment_history))
        print(transaction.accountBalance,c.account_id)
        print("\n!!!!!!!!!!!!!!!!!\n")
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")


def kuku_loans(today=get_utc_now()): #pragma no cover
    """
    [10:44:56] Daniel Boucher:
    41994 Peterson  Ondara
    42013 Sarah Bichoka
    42014 Sarah Onderi
    42898 Wilkista Nyanchera
    42464 Yuniah Kemunto
    41980 Yuniah Mongeri
    42897 Zachariah Siocha
    [10:45:40] Steve Hermes: 1st loan on Aug 20th ?

    loan start 2017-08-20
    """

# 2017-08-29 ll
    ai = (
        42073,
        42505,
        42055,
        42896,
        42035,
        42465,
        42081,
        42434,
        42052,
        44038,
        42009,
        42716,
        42077,
        42476,
        42718,
        42007,
        41977,
        41961,
        42062,
        42509,
        42407,
        42466
    )

    la = (
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
        30,
    )



    return -1
    exit(-1)

    from ph_model.models import Customer
    from ph_model.models import Loan
    from ph_util.accounting_util import LoanType

    id_list = []
    none_list = []
    start_date = today
    from ph_model.models.user import User
    user = User.objects.all()[0]
    cnt = 0
    idx = 0
    print('TTL:', Loan.objects.filter(loanType=LoanType.KUKU_POA_NO_VAT_LOAN.name).count())
    for id in ai:
        acc = ai[cnt]
        amt = la[cnt]
        cust = Customer.objects.get_or_none(account_id=acc) ###########id)

        if cust: ##################### and Loan.objects.filter(account_id=id, loanType=LoanType.KUKU_POA_NO_VAT_LOAN.name, startDate__gte=str_to_dt('2017-07-01')).count() == 0:
            loan = create_payment_plan_no_vat(
                loan_cls=Loan,
                loan_type=LoanType.KUKU_POA_NO_VAT_LOAN.name,
                acc=cust.account,
                loan_amt= Decimal(amt * 22),  ###########KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT,
                start_date=str_to_dt('2017-09-01'),
                ttl_days=amt) #############KUKU_POA_SUBSCRIPTION_5_DAYS)
            cnt += 1
            print(cnt, cust)
        else:
            print(cnt, cust, '   no cust ')
        idx += 1

    print('TTL:', Loan.objects.filter(loanType=LoanType.KUKU_POA_NO_VAT_LOAN.name).count())

    """
    cnt = 0
    for id in aug_20_ids:
        cust = Customer.objects.get_or_none(account_id=id)

        if cust and \
                        Loan.objects.filter(account_id=id, loanType=LoanType.KUKU_POA_NO_VAT_LOAN.name,
                                            startDate__gte=str_to_dt('2017-07-01')).count() == 0:
            loan = create_payment_plan_no_vat(
                loan_cls=Loan,
                loan_type=LoanType.KUKU_POA_NO_VAT_LOAN.name,
                acc=cust.account,
                loan_amt=KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT,
                start_date=str_to_dt('2017-08-20'),
                ttl_days=KUKU_POA_SUBSCRIPTION_5_DAYS)
            cnt += 1
            print(cnt, cust, loan)
        else:
            print(cnt, cust, '   no cust if not none loan exists')
    """
    print(Loan.objects.filter(loanType=LoanType.KUKU_POA_NO_VAT_LOAN.name).count())

def kuku_loansxxxx(today=get_utc_now()): #pragma no cover
    # acc id
    ids = (
        10106,
    )

    id_list = []
    none_list = []
    start_date = today
    for id in ids:
        from ph_model.models import Customer
        cust = Customer.objects.get_or_none(account_id=id)
        from ph_model.models.municipality import Municipality
        muni = Municipality.objects.get(id=cust.municipality_id)
        from ph_model.models.user import User
        user = User.objects.all()[0]
        from ph_model.models import Loan
        from ph_util.accounting_util import LoanType

        """
        loan = create_payment_plan_no_vat(
            loan_cls=Loan,
            loan_type=LoanType.KUKU_POA_NO_VAT_LOAN.name,
            acc=cust.account,
            loan_amt=KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT,
            start_date=str_to_dt('2017-08-1'),
            ttl_days=KUKU_POA_SUBSCRIPTION_5_DAYS)

        print(loan)
        """

        print("\n###################################################\n")
        ###### l = Loan.objects.get_or_none(account_id=id, loanAmount=1000)
        for l in Loan.objects.filter(account_id=id, loanType=LoanType.KUKU_POA_NO_VAT_LOAN.name, startDate__gte=str_to_dt('2017-07-01')) \
                .order_by('-startDate').all():
            print(str_obj(l))

        exit(9)

        if cust is not None:

            # print(id, l.loanAmount, cust.connectionFeeLoan)
            dt = muni.connectionFeeDueDate
            if l is not None and l.loanAmount == 2000 != True:
                id_list.append(id)
                print("2000 ", id, l.startDate.date(), cust.connectionFeeLoan, l.loanAmount)
                # l.startDate = dt
                # l.save()
                if cust.connectionFeeDue != dt:
                    cust.connectionFeeDue = dt
                    ####print(cust.connectionFeeDue, id)
                    cust.save()
                # make 1000 Kes loan 1 payment
                from ph_util.accounting_util import create_loan
                from ph_util.accounting_util import LoanType
                from ph_util.accounting_util import add_vat
                loan = create_loan(
                    loan_cls=Loan,
                    loan_type=LoanType.CONNECTION_FEE_LOAN.name,
                    acc=cust.account,
                    loan_amt=1000,
                    start_date=start_date,
                    payment=add_vat(1000),
                    notes='The 1160 Kes connection fee customers are suppose to deposit to get a connection payment plan')
                from ph_operation import loan_manager

                loan_handler = loan_manager.Loan(loan)
                print(str_obj(loan_handler))
                loan_handler.repay_loan()
                # make cred adjust 480 Kes
            else:
                if l is None:
                    none_list.append(id)
                    continue
                else:
                    id_list.append(id)
                    print ("JUST CRED ", id, l.startDate.date(), cust.connectionFeeLoan, l.loanAmount)
                    # make cred adjust 480 Kes

            from ph_model.models.transaction import TransactionType
            t_type = TransactionType.CREDIT.value
            from ph_operation import transaction_manager
            transaction_type = transaction_manager.Credit

            transaction = transaction_type(cust.account, 480)
            transaction.process()
            from ph_model.models import CreditAdjustmentHistory
            adjustment_history = CreditAdjustmentHistory(
                account=cust.account,
                user=user,
                reason='Welcome gift',
                amount=480,
                processedTime=get_utc_now(),
                transactionType=t_type,
                adj_type='Marketing'
            )
            adjustment_history.save()
            """
            amount = 3000
            if l is not None and l.loanAmount == 1000:
                print("\n22222222222222222222222222222222222\n")
                amount = 2000
            if muni.connectionFeeDueDate is None:
                muni.connectionFeeDueDate = start_date + timedelta(days=30)
                print("\nMMMMMMMMMMMM\n%s\nMMMMMMMMMMMMMMMMM\n" % muni.connectionFeeDueDate)
                muni.save()
            if cust.connectionFeeDue != muni.connectionFeeDueDate:
                cust.connectionFeeDue = muni.connectionFeeDueDate
                print(cust.connectionFeeDue, id)
                cust.save()
                cust = Customer.objects.get_or_none(account_id=id)
            else:
                print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            print(cust.connectionFeeLoan,id, cust.firstName,  amount)
            print(cust.connectionFeeDue, muni.connectionFeeDueDate, cust.connectionFeeDue == muni.connectionFeeDueDate )


            from ph_util.accounting_util import create_loan
            from ph_util.accounting_util import LoanType
            from ph_util.accounting_util import add_vat
            from ph_util.accounting_util import RESIDENTIAL_CONN_LOAN_AMT
            conn_loan = RESIDENTIAL_CONN_LOAN_AMT
            from ph_model.models.customer import CustomerType
            if cust.customerType == CustomerType.COMMERCIAL.name:
                from ph_util.accounting_util import COMMERCIAL_CONN_LOAN_AMT
                conn_loan = COMMERCIAL_CONN_LOAN_AMT

            days = (muni.connectionFeeDueDate.date() - today.date()).days
            if days < 1:
                days = 1
            daily_fee = add_vat(conn_loan) / days
            print('\nDDDDDDDDDDDDDDDDDDDD\n',start_date.date(),muni.connectionFeeDueDate.date(),days,daily_fee)

            loan = create_loan(loan_cls=Loan,
                               loan_type=LoanType.CONNECTION_FEE_LOAN.name,
                               acc=cust.account,
                               loan_amt=conn_loan,
                               start_date=start_date,
                               payment=daily_fee)
            id_list.append(loan.pk)
            """

        else:
            print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', id)
            """
            from ph_operation import loan_manager
            loan_handler = loan_manager.Loan(l)
            print(str_obj(loan_handler))
            loan_handler.repay_loan()
            """

    print('\n@@@@@@@@@@@@@@@@@\n%s\n%s\n@@@@@@@@@@@@@@@\n' % (id_list, none_list))
    return id_list



acc_credit = (
42693,
44093,
42691,
42651,
42712,
45301,
44051,
42523,
44053,
42599,
42656,
45490,
42657,
42641,
42642,
42644,
42628,
42626,
42627,
42640,
42630,
45329,
42597,
42591,
42618,
42623,
42624,
42646,
42658,
45314,
42518,
42622,
45327,
42521,
42633,
42567,
44054,
45264,
42648,
42653,
42654,
42649,
42650,
42663,
42664,
42683,
42666,
42708,
42647,
42652,
42655,
42662,
42589,
42596,
45300,
42520,
42522,
42609,
42610,
42617,
42629,
42632,
42643,
42588,
42583,
42585,
45267,
45306,
45307,
42584,
42592,

)

aaaa = (
22,
22,
22,
22,
110,
22,
22,
22,
22,
66,
22,
22,
22,
22,
22,
22,
)

vvvv = (

)

def do_con_loan_credit(): #pragma no cover

#    for l in Loan.objects.all().filter(id__in=pilot_con_credit):
#        print(l.created,l.loanAmount,l.account_id)
    #print(get_utc_now() + timedelta(hours=3))
    """
    for idx, a in enumerate(acc_credit, start=0):  # Python indexes start at zero
        print(idx, a, acc_credit[idx], aaaa[idx])
    exit(111)
    return
    #return -1
    #exit(1)
    """

    exit(111)


    from ph_model.models.user import User

    user = User.objects.all()[0]




    from ph_model.models import Customer

    # custs = Customer.objects.filter(municipality_id=(2270942, 2270944, 2270943).filter(circuit_id=None)


    from ph_model.models.transaction import TransactionType
    from ph_operation import transaction_manager
    from ph_model.models import CreditAdjustmentHistory


    for idx, a in enumerate(acc_credit, start=0):  # Python indexes start at zero
        #print(idx, a, aaaa[idx])

        c = Customer.objects.get_or_none(account_id=a)


        if c is not None:
            amt =  160 ########aaaa[idx]
            # assumes a credit
            t_type = TransactionType.CREDIT.value
            transaction = transaction_manager.Credit(c.account, amt)


            transaction.process()
            adjustment_history = CreditAdjustmentHistory(
                account=c.account,
                user=user,
                reason='Refund multiple connection loan payments on Nov 27 2017 ',
                amount=amt,
                processedTime=get_utc_now() + timedelta(hours=3),
                transactionType=t_type,
                adj_type='Accounting'
            )
            adjustment_history.save()


            #print(str_obj(transaction))
            #print(str_obj(adjustment_history))
            print(idx, transaction.accountBalance, c.account_id,c.firstName, c.lastName, c.phoneNumber)
            """
            amt = vvvv[idx]
            t_type = TransactionType.CREDIT.value
            transaction = transaction_manager.Credit(account, amt)
            transaction.process()

            from ph_model.models import CreditAdjustmentHistory

            adjustment_history = CreditAdjustmentHistory(
                account=account,
                user=user,
                reason='Refund delayed variable charges 9-22 - 10-6 VAT',
                amount=amt,
                processedTime=get_utc_now(),
                transactionType=t_type,
                adj_type='VAT'
            )
            adjustment_history.save()
            print(str_obj(transaction))
            print(str_obj(adjustment_history))

            
            amt = -480
            t_type = TransactionType.DEBIT.value
            transaction = transaction_manager.Credit(account, amt)
            transaction.process()

            from ph_model.models import CreditAdjustmentHistory

            adjustment_history = CreditAdjustmentHistory(
                account=account,
                user=user,
                reason='Refund connection fee welcome gift',
                amount=amt,
                processedTime=get_utc_now(),
                transactionType=t_type,
                adj_type='Marketing'
            )
            adjustment_history.save()
            print(str_obj(transaction))
            print(str_obj(adjustment_history))
            """

        else:
            print('\n!!!!!!!!!!!!!!!!!\n\n!!!!!!!!!!!!!!!!!\n\n!!!!!!!!!!!!!!!!!\n\n!!!!!!!!!!!!!!!!!\n\n!!!!!!!!!!!!!!!!!\n\n!!!!!!!!!!!!!!!!!\n')
            print(a)

    """
    for l in Loan.objects.all().filter(id__in=pilot_con_credit):
        idx += 1
        print( l.loanAmount, l.account_id)
        print (idx)
        print("\n!!!!!!!!!!!!!!!!!\n")

        from ph_model.models.transaction import TransactionType

        from ph_operation import transaction_manager



        amt = 3.43
        t_type = TransactionType.CREDIT.value
        transaction = transaction_manager.Credit(l.account, amt)
        transaction.process()

        from ph_model.models import CreditAdjustmentHistory

        adjustment_history = CreditAdjustmentHistory(
            account=l.account,
            user=user,
            reason='Refund pilot site connection fee amount loan ID: ' + str(l.id),
            amount=amt,
            processedTime=get_utc_now(),
            transactionType=t_type,
            adj_type='Accounting'
        )
        adjustment_history.save()
        print(str_obj(transaction))
        print(str_obj(adjustment_history))

        amt = 0.548
        t_type = TransactionType.CREDIT.value
        transaction = transaction_manager.Credit(l.account, amt)
        transaction.process()

        adjustment_history = CreditAdjustmentHistory(
            account=l.account,
            user=user,
            reason='Refund pilot site connection fee VAT loan ID: ' + str(l.id),
            amount=amt,
            processedTime=get_utc_now(),
            transactionType=t_type,
            adj_type='VAT'
        )
        adjustment_history.save()

        print(str_obj(transaction))
        print(str_obj(adjustment_history))
    """
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")




# TODO remove after March 19th 2018
#TempCreditKukuPoa.apply_kuku_credit()


class TempCreditKukuPoa:
    """
    TODO remove this after March 19th

    https://app.asana.com/0/148294678343819/565167443426345/f
    KES 50 per account per day.
    Credit once daily.
    Start date today.
    End date March 19.

    """

    def __init__(self):
        pass

    @staticmethod
    def apply_kuku_credit():
        """
        Temporary credit
        :return:
        """
        acc_credit = (42078,
                      42081,
                      42869,
                      42508,
                      41961,
                      41990,
                      42032,
                      42013,
                      44265,
                      41989,
                      42097,
                      42070,
                      42718,
                      42049,
                      42455,
                      42019,
                      41955,
                      42486,
                      42465,
                      41975,
                      42506,
                      42514,
                      42470,
                      42464,
                      41976,
                      41983,
                      41957,
                      41977,
                      42492,
                      41986,
                      42055,
                      42484,
                      42466,
                      44038)

        from ph_model.models.user import User

        user = User.objects.all()[0]

        from ph_model.models import Customer

        from ph_model.models.transaction import TransactionType
        from ph_operation import transaction_manager
        from ph_model.models import CreditAdjustmentHistory
        cnt = 0

        for idx, a in enumerate(acc_credit):  # Python indexes start at zero
            print(idx, a)

            c = Customer.objects.get_or_none(account_id=a)

            if c is not None and get_utc_now() < str_to_dt('2018-03-20'):
                cnt += 1
                amt = 50
                t_type = TransactionType.CREDIT.value
                transaction = transaction_manager.Credit(c.account, amt)

                transaction.process()
                adjustment_history = CreditAdjustmentHistory(
                    account=c.account,
                    user=user,
                    reason='KP HH Brooding Pilot ',
                    amount=amt,
                    processedTime=get_utc_now(), # no need for 3 hour delta + datetime.timedelta(hours=3),
                    transactionType=t_type,
                    adj_type='Marketing'
                )
                adjustment_history.save()

        #LOGGER.info(str(len(acc_credit)) + '  Records, ' + str(cnt) + '  Updated 50 Kes  ')
