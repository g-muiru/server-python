# coding=utf-8
"""Tests first usage functions."""
from decimal import Decimal
from django.core.management import call_command
from django.db import connection
from ph_model.models import Account, LoanPaymentHistory, Loan, Usage
from ph_model.models.customer import CustomerType, Customer
from ph_model.models.transaction import MasterTransactionHistory, update_mth_balances
from ph_operation import transaction_manager
from ph_operation.tests import base_test
from ph_script.operational_first_usage_cron import OperationalFirstUsage
from ph_util.accounting_util import AccountingEnums, CONN_LOAN_FEE_AMT, COMMERCIAL_CONN_LOAN_AMT, \
    CONN_LOAN_DEPOSIT_AMT, calc_vat, RESIDENTIAL_CONN_LOAN_AMT, add_vat, MUNIS_NO_VAT, \
    RESIDENTIAL_BONUS_POWER_AMT, COMMERCIAL_I_BONUS_POWER_AMT, COMMERCIAL_II_BONUS_POWER_AMT
from ph_util.date_util import str_to_dt
from ph_util.test_utils import assert_eq
from ddt import ddt, unpack, idata


class LoanBaseTestCase(base_test.BaseTestOperation):
    def setUp(self):
        """ Setup"""
        super(LoanBaseTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/loan_config.json', verbosity=0)
        self.test_account = Account.objects.get(id=603)
        MasterTransactionHistory.objects.all().delete()
        Usage.objects.all().delete()
        Loan.objects.all().delete()
        LoanPaymentHistory.objects.all().delete()


# noinspection PyArgumentEqualDefault,PyArgumentEqualDefault
@ddt
class FirstUseWithLoanTestCase(LoanBaseTestCase):
    """Test functions in operational_first_usage_cron.py"""

    op_date_str = '2002-01-10'
    before_op_date_str = '2002-01-05'
    after_op_date_str = '2002-01-12'

    data = [
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 100, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 159, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 160, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 999, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 1000, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 1159, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 1160, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 3999, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 4000, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 4449, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 4500, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 10000, 0, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 100, -1160, 100),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 159, -1160, 159),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 160, -1000, 160 - 160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 999, -1000, 999 - 160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1000, 1000 - 1160, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1159, -160, 159),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1160, 1160 - 1160, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 3999, 0, 3999 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4000, 0, 4000 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4499, 0, 4499 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4500, 0, 4500 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 10000, 0, 10000 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 100, -1160, 100),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 159, -1160, 159),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 160, -1000, 160 - 160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 999, -1000, 999 - 160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1000, 1000 - 1160, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1159, -160, 159),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1160, 1160 - 1160, 0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 3999, 0, 3999 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4000, 0, 4000 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4499, 0, 4499 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4500, 0, 4500 - 1160),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 10000, 0, 10000 - 1160),

        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 100, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 159, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 160, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 999, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 1000, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 1159, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 1160, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 3999, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 4000, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 4449, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 4500, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 4500, 'VatVille', 10000, 0, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 100, -1160, 100),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 159, -1160, 159),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 160, -1000, 160 - 160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 999, -1000, 999 - 160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1000, 1000 - 1160, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1159, -160, 159),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1160, 1160 - 1160, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 3999, 0, 3999 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4000, 0, 4000 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4499, 0, 4499 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4500, 0, 4500 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 10000, 0, 10000 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 100, -1160, 100),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 159, -1160, 159),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 160, -1000, 160 - 160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 999, -1000, 999 - 160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1000, 1000 - 1160, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1159, -160, 159),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 1160, 1160 - 1160, 0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 3999, 0, 3999 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4000, 0, 4000 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4499, 0, 4499 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 4500, 0, 4500 - 1160),
        (CustomerType.COMMERCIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 7, 2, 4500, 'VatVille', 10000, 0, 10000 - 1160),

        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 100, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 159, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 160, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 999, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 1000, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 1159, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 1160, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 1999, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 2000, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 2499, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 2500, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], before_op_date_str, op_date_str, 1, 0, 2500, '97', 5000, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 100, -1160, 100),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 159, -1160, 159),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 160, -1000, 160 - 160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 999, -1000, 999 - 160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 1000, 1000 - 1160, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 1159, 1000 - 1160, 1159 - 1000),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 1160, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 1999, 0, 1999 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 2000, 0, 2000 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 2499, 0, 2499 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 2500, 0, 2500 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], after_op_date_str, op_date_str, 8, 2, 2500, '97', 5000, 0, 5000 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 100, -1160, 100),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 159, -1160, 159),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 160, -1000, 160 - 160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 999, -1000, 999 - 160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 1000, 1000 - 1160, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 1159, 1000 - 1160, 1159 - 1000),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 1160, 0, 0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 1999, 0, 1999 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 2000, 0, 2000 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 2499, 0, 2499 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 2500, 0, 2500 - 1160),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10], op_date_str, op_date_str, 8, 2, 2500, '97', 5000, 0, 5000 - 1160),
    ]

    for mnv in MUNIS_NO_VAT:
        data.append((CustomerType.RESIDENTIAL.name, [601, 1, 55], after_op_date_str, op_date_str, 8, 2,
                     RESIDENTIAL_CONN_LOAN_AMT + CONN_LOAN_FEE_AMT - CONN_LOAN_DEPOSIT_AMT, mnv, 9543.33, 0, 8383.33))
        data.append((CustomerType.COMMERCIAL.name, [601, 1, 55], after_op_date_str, op_date_str, 8, 2,
                     COMMERCIAL_CONN_LOAN_AMT + CONN_LOAN_FEE_AMT - CONN_LOAN_DEPOSIT_AMT, mnv, 9543.33, 0, 8383.33))
        data.append((CustomerType.COMMERCIAL_II.name, [601, 1, 55], after_op_date_str, op_date_str, 8, 2,
                     COMMERCIAL_CONN_LOAN_AMT + CONN_LOAN_FEE_AMT - CONN_LOAN_DEPOSIT_AMT, mnv, 9543.33, 0, 8383.33))

    # noinspection PyTypeChecker
    @idata(data)
    @unpack
    def test_first_use(self, cust_type, usage, usage_date_str, op_date_str,
                       expected_mth, expected_loans, expected_amt, muni_name, starting_bal, expected_uncol, expected_bal):
        """
        Process first use to create connection fee loans

        :param cust_type:
        :param usage:
        :param usage_date_str:
        :param op_date_str:
        :param expected_mth:
        :param expected_loans:
        :param expected_amt:
        :param muni_name:
        :param starting_bal:
        :param expected_uncol:
        :param expected_bal:
        :return:
        """

        if connection.vendor != 'postgresql':
            print("FirstUseWithLoanTestCase.test_first_use can only be run with a postgres DB")
            pass
        else:
            # save the account starting balances
            start_balance = []
            for c in Customer.objects.all():
                if c.id == usage[0]:
                    c.customerType = cust_type
                    c.connectionFeeLoan = True
                    c.save()
                    c.municipality.name = muni_name
                    c.municipality.save()
                    MasterTransactionHistory.objects.all().delete()
                    c.account.accountBalance = Decimal(0.0)
                    c.account.save()
                    transaction_manager.TransactionManager. \
                        process_mobile_payment(c.account, 'xx0', starting_bal, str_to_dt('2016-03-02T01:01:00Z'))
                    Usage(customer_id=usage[0], circuit_id=usage[1], collectTime=usage_date_str, intervalWh=usage[2]).save()
                if c.circuit_id == usage[1]:
                    c.circuit.queen.grid.operational_date = str_to_dt(op_date_str).date()
                    c.circuit.queen.grid.save()
                    c.circuit.queen.operational_date = str_to_dt(op_date_str).date()
                    c.circuit.queen.save()
                update_mth_balances()
                start_balance.append([c.id, Decimal(round(c.account.accountBalance, 2)), Decimal(round(c.account.uncollected_bal, 2))])

            Loan.objects.all().delete()
            # create a usage record
            Usage(customer_id=usage[0], circuit_id=usage[1], collectTime=usage_date_str, intervalWh=usage[2]).save()
            # run the first use check using the default DB
            OperationalFirstUsage.check_first_usage(False, False, 'default')

            assert_eq(expected_mth, MasterTransactionHistory.objects.count())
            assert_eq(expected_loans, Loan.objects.count())

            cnt_init = 0
            cnt_vat_init = 0
            cnt_payment = 0
            cnt_vat_payment = 0
            idx = 0

            update_mth_balances()

            for mth in MasterTransactionHistory.objects.all().order_by('id'):
                if mth.account_id == usage[0] and mth.source == AccountingEnums.LOAN_CONN_FEE_INITc235.name:
                    cnt_init += 1
                    if cnt_init == 1:
                        self.assertEqual(round(mth.amount, 2), expected_amt)
                    else:
                        self.assertEqual(round(mth.amount, 2), CONN_LOAN_DEPOSIT_AMT)
                    self.assertEqual(round(mth.uncollected_bal, 2), expected_uncol)
                    self.assertEqual(round(mth.accountBalance, 2), expected_bal)
                if mth.account_id == usage[0] and mth.source == AccountingEnums.VAT_LOAN_INITc301.name:
                    cnt_vat_init += 1
                    assert_eq(True, round(mth.amount, 2) == round(calc_vat(CONN_LOAN_DEPOSIT_AMT), 2) or
                              round(mth.amount, 2) == round(calc_vat(expected_amt), 2))
                if mth.account_id == usage[0] and mth.source == AccountingEnums.LOAN_PAYMENTc600.name:
                    cnt_payment += 1
                    assert_eq(CONN_LOAN_DEPOSIT_AMT, round(mth.amount, 2))
                if mth.account_id == usage[0] and mth.source == AccountingEnums.VAT_LOAN_PAYMENTc601.name:
                    cnt_vat_payment += 1
                    assert_eq(round(calc_vat(CONN_LOAN_DEPOSIT_AMT), 2), round(mth.amount, 2))
                idx += 1
            ll_cnt = 2
            pp_cnt = 1
            if str_to_dt(usage_date_str) < str_to_dt(op_date_str):
                ll_cnt = 0
                pp_cnt = 0
            if expected_amt != 0:
                assert_eq(ll_cnt, cnt_init)
                assert_eq(ll_cnt, cnt_vat_init)
                assert_eq(pp_cnt, cnt_payment)
                assert_eq(pp_cnt, cnt_vat_payment)

                for sb in start_balance:
                    cc = Customer.objects.get_or_none(pk=sb[0])
                    acc_bal = round(Decimal(cc.account.accountBalance), 2)
                    uncol_bal = round(Decimal(cc.account.uncollected_bal), 2)
                    if sb[0] == usage[0]:
                        if expected_amt != 0:
                            if muni_name in MUNIS_NO_VAT:
                                cla = COMMERCIAL_CONN_LOAN_AMT
                                if cust_type == CustomerType.RESIDENTIAL.name:
                                    cla = RESIDENTIAL_CONN_LOAN_AMT
                                if sb[1] < round(calc_vat(CONN_LOAN_DEPOSIT_AMT), 2):
                                    if str_to_dt(usage_date_str) < str_to_dt(op_date_str):
                                        assert_eq(starting_bal, acc_bal)
                                        assert_eq(expected_uncol, sb[2])
                                    else:
                                        assert_eq(round(sb[1] + calc_vat(cla), 2), acc_bal)
                                        assert_eq(expected_uncol, uncol_bal)
                                elif sb[1] < CONN_LOAN_DEPOSIT_AMT:
                                    if str_to_dt(usage_date_str) < str_to_dt(op_date_str):
                                        assert_eq(starting_bal, acc_bal)
                                        assert_eq(expected_uncol, sb[2])
                                    else:
                                        assert_eq(round((starting_bal + calc_vat(cla)) - calc_vat(CONN_LOAN_DEPOSIT_AMT), 2), acc_bal)
                                        assert_eq(expected_uncol, uncol_bal)
                                else:
                                    if str_to_dt(usage_date_str) < str_to_dt(op_date_str):
                                        assert_eq(expected_uncol, sb[2])
                                        assert_eq(acc_bal, sb[1])
                                    else:
                                        if starting_bal < add_vat(CONN_LOAN_DEPOSIT_AMT):
                                            assert_eq(round(sb[1] + calc_vat(cla) - CONN_LOAN_DEPOSIT_AMT, 2), acc_bal)
                                            assert_eq(expected_uncol, round(-calc_vat(CONN_LOAN_DEPOSIT_AMT), 2))
                                        else:
                                            assert_eq(round(sb[1] + calc_vat(cla) - calc_vat(CONN_LOAN_DEPOSIT_AMT) - CONN_LOAN_DEPOSIT_AMT, 2),
                                                      acc_bal)
                                            assert_eq(expected_uncol, sb[2])
                            else:
                                cla = COMMERCIAL_CONN_LOAN_AMT
                                if cust_type == CustomerType.RESIDENTIAL.name:
                                    cla = RESIDENTIAL_CONN_LOAN_AMT
                                if sb[1] < round(calc_vat(CONN_LOAN_DEPOSIT_AMT), 0):
                                    assert_eq(sb[1], acc_bal)
                                    if str_to_dt(usage_date_str) < str_to_dt(op_date_str):
                                        assert_eq(expected_uncol, sb[2])
                                    else:
                                        assert_eq(expected_uncol, sb[2] - add_vat(CONN_LOAN_DEPOSIT_AMT))
                                elif sb[1] < CONN_LOAN_DEPOSIT_AMT:
                                    if str_to_dt(usage_date_str) < str_to_dt(op_date_str):
                                        assert_eq(expected_uncol, sb[2])
                                        assert_eq(acc_bal, sb[1])
                                    else:
                                        assert_eq(round(sb[1] - calc_vat(CONN_LOAN_DEPOSIT_AMT), 2), acc_bal)
                                        assert_eq(sb[2] - CONN_LOAN_DEPOSIT_AMT, expected_uncol)
                                elif sb[1] < calc_vat(cla):
                                    assert_eq(round(Decimal(sb[1])
                                                    - add_vat(CONN_LOAN_DEPOSIT_AMT) + calc_vat(cla), 2), acc_bal)
                                    assert_eq(expected_uncol, sb[2])
                        else:
                            assert_eq(round(Decimal(Customer.objects.get_or_none(pk=sb[0]).account.accountBalance), 2),
                                      round(Decimal(sb[1]), 2))
                    else:
                        assert_eq(round(Decimal(Customer.objects.get_or_none(pk=sb[0]).account.accountBalance), 2),
                                  round(Decimal(sb[1]), 2))

            cnt_loans = Loan.objects.count()
            cnt_mth = MasterTransactionHistory.objects.count()
            # run the first use check using the default DB, and verify nothing happens
            OperationalFirstUsage.check_first_usage('default')
            assert_eq(cnt_mth, MasterTransactionHistory.objects.count())
            assert_eq(cnt_loans, Loan.objects.count())


# noinspection PyArgumentEqualDefault,PyArgumentEqualDefault,PyArgumentEqualDefault,PyArgumentEqualDefault
@ddt
class FirstUseTestCase(LoanBaseTestCase):
    """
    Test functions in operational_first_usage_cron.py no loans
    """

    op_date_str = '2002-01-10'
    before_op_date_str = '2002-01-05'
    after_op_date_str = '2002-01-12'
    data1 = [
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 5801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 5801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 10000,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 3479,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 3480,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 3481,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 9543.33,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 100,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 479,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 480,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 5, 1, 481,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 5000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 5001,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 5000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 5001,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 10000,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 3479,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 3480,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 3481,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 9543.33,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 100,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 479,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 480,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 481,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 6, 1, 2999,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 3000,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 3001,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -480.0),

        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 5801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 5801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'VatVille', -5000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 10000,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 3479,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 3480,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 3481,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 9543.33,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 100,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 479,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 480,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 5, 1, 481,
         RESIDENTIAL_CONN_LOAN_AMT, 'VatVille', -3000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 5000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 5001,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5800.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 10000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 4999,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 5000,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 5001,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 5800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 9543.33,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 700,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5800.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 800,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 801,
         COMMERCIAL_CONN_LOAN_AMT, 'Kirwa site C', -5000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 10000,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 3479,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 3480,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 3481,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 9543.33,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 100,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 479,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 480,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 481,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 6, 1, 2999,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -3000.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 3000,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -480.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], after_op_date_str, op_date_str, 7, 1, 3001,
         RESIDENTIAL_CONN_LOAN_AMT, 'Kirwa site C', -480.0),

        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 10000,
         10000, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5800,
         5800, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5801,
         5801, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5000,
         5000, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5001,
         5001, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 700,
         700, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 800,
         800, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 801,
         801, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 10000,
         10000, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5800,
         5800, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5801,
         5801, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5000,
         5000, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5001,
         5001, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 700,
         700, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 800,
         800, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 801,
         801, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 10000,
         10000, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3479,
         3479, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3480,
         3480, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3481,
         3481, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 2999,
         2999, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3000,
         3000, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3001,
         3001, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 479,
         479, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 480,
         480, 'VatVille', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 481,
         481, 'VatVille', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 10000,
         10000, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5800,
         5800, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5801,
         5801, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5000,
         5000, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5001,
         5001, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 700,
         700, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 800,
         800, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 801,
         801, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 10000,
         10000, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5800,
         5800, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5801,
         5801, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 4999,
         4999, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5000,
         5000, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 5001,
         5001, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 700,
         700, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 800,
         800, 'Kirwa site C', 0.0),
        (CustomerType.COMMERCIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 801,
         801, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 10000,
         10000, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3479,
         3479, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3480,
         3480, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3481,
         3481, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 2999,
         2999, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3000,
         3000, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 3001,
         3001, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 9543.33,
         9543.33, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 479,
         479, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 480,
         480, 'Kirwa site C', 0.0),
        (CustomerType.RESIDENTIAL.name, [601, 1, 10.1], before_op_date_str, op_date_str, 1, 0, 481,
         481, 'Kirwa site C', 0.0),
    ]

    for mnv in MUNIS_NO_VAT:
        data1.append((CustomerType.RESIDENTIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 9543.33,
                      RESIDENTIAL_CONN_LOAN_AMT, mnv, 0.0))
        data1.append((CustomerType.COMMERCIAL_II.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 9543.33,
                      COMMERCIAL_CONN_LOAN_AMT, mnv, 0.0))
        data1.append((CustomerType.COMMERCIAL.name, [601, 1, 10.1], op_date_str, op_date_str, 7, 1, 9543.33,
                      COMMERCIAL_CONN_LOAN_AMT, mnv, 0.0))

    # noinspection PyTypeChecker
    @idata(data1)
    @unpack
    def test_first_use(self, cust_type, usage, usage_date_str, op_date_str, expected_mth, expected_loans, starting_bal,
                       expected_amt, muni_name, expected_uncol):
        """
        Process first use for customers without loans

        :param cust_type:
        :param usage:
        :param op_date_str:
        :param expected_mth:
        :param expected_loans:
        :param starting_bal:
        :param expected_amt:
        :param expected_uncol:
        :return:
        """
        if connection.vendor != 'postgresql':
            print("FirstUseTestCase.test_first_use can only be run with a postgres DB")
            pass
        else:
            start_balance = []
            MasterTransactionHistory.objects.all().delete()
            for c in Customer.objects.all():
                if c.account_id == usage[0]:
                    c.customerType = cust_type
                    c.save()
                    c.municipality.name = muni_name
                    c.municipality.save()
                    MasterTransactionHistory.objects.all().delete()
                    c.account.accountBalance = Decimal(0.0)
                    c.account.save()
                    transaction_manager.TransactionManager. \
                        process_mobile_payment(c.account, 'xx0', starting_bal, str_to_dt('2016-03-02T01:01:00Z'))
                    Usage(customer_id=usage[0], circuit_id=usage[1], collectTime=usage_date_str, intervalWh=usage[2]).save()
                if c.circuit_id == usage[1]:
                    c.circuit.queen.grid.operational_date = str_to_dt(op_date_str).date()
                    c.circuit.queen.grid.save()
                    c.circuit.queen.operational_date = str_to_dt(op_date_str).date()
                    c.circuit.queen.save()
                update_mth_balances()
                start_balance.append([c.id, round(c.account.accountBalance, 2), round(c.account.uncollected_bal, 2)])

            # run the first use check using the default DB
            OperationalFirstUsage.check_first_usage(False, False, 'default')
            assert_eq(expected_mth, MasterTransactionHistory.objects.count())
            assert_eq(expected_loans, Loan.objects.count())

            cnt_init = 0
            cnt_vat_init = 0
            cnt_payment = 0
            cnt_vat_payment = 0

            for mth in MasterTransactionHistory.objects.all().order_by('id'):
                if mth.account_id == usage[0] and mth.source == AccountingEnums.LOAN_CONN_FEE_INITc235.name:
                    cnt_init += 1
                    assert_eq(round(mth.amount, 2), expected_amt)
                if mth.account_id == usage[0] and mth.source == AccountingEnums.VAT_LOAN_INITc301.name:
                    cnt_vat_init += 1
                    assert_eq(round(mth.amount, 2), round(calc_vat(expected_amt), 2))
                if mth.account_id == usage[0] and mth.source == AccountingEnums.LOAN_PAYMENTc600.name:
                    cnt_payment += 1
                    assert_eq(round(expected_amt, 2), round(mth.amount, 2))
                if mth.account_id == usage[0] and mth.source == AccountingEnums.VAT_LOAN_PAYMENTc601.name:
                    cnt_vat_payment += 1
                    assert_eq(round(calc_vat(expected_amt), 2), round(mth.amount, 2))

            update_mth_balances()

            if usage_date_str == self.before_op_date_str:
                assert_eq(0, cnt_init)
                assert_eq(0, cnt_vat_init)
                assert_eq(0, cnt_payment)
                assert_eq(0, cnt_vat_payment)
            elif expected_mth != 0:
                assert_eq(1, cnt_init)
                assert_eq(1, cnt_vat_init)
                assert_eq(1, cnt_payment)
                assert_eq(1, cnt_vat_payment)

                for sb in start_balance:
                    cc = Customer.objects.get_or_none(pk=sb[0])
                    acc_bal = round(Decimal(cc.account.accountBalance), 2)
                    uncol_bal = round(Decimal(cc.account.uncollected_bal), 2)
                    if sb[0] == usage[0]:
                        vat = 0
                        la = 0
                        if expected_amt != 0:
                            amt = 0
                            if acc_bal >= 0.01:
                                amt = RESIDENTIAL_BONUS_POWER_AMT
                                vat = calc_vat(RESIDENTIAL_CONN_LOAN_AMT)
                                la = add_vat(RESIDENTIAL_CONN_LOAN_AMT)
                                if cust_type == CustomerType.COMMERCIAL.name:
                                    amt = COMMERCIAL_I_BONUS_POWER_AMT
                                    vat = calc_vat(COMMERCIAL_CONN_LOAN_AMT)
                                    la = add_vat(COMMERCIAL_CONN_LOAN_AMT)
                                elif cust_type == CustomerType.COMMERCIAL_II.name:
                                    amt = COMMERCIAL_II_BONUS_POWER_AMT
                                    vat = calc_vat(COMMERCIAL_CONN_LOAN_AMT)
                                    la = add_vat(COMMERCIAL_CONN_LOAN_AMT)
                            if muni_name in MUNIS_NO_VAT:
                                if (amt == RESIDENTIAL_BONUS_POWER_AMT and sb[1] >= RESIDENTIAL_CONN_LOAN_AMT) \
                                        or sb[1] >= COMMERCIAL_CONN_LOAN_AMT:
                                    expect = Decimal(sb[1]) - Decimal(expected_amt - amt)
                                    if op_date_str != self.before_op_date_str:
                                        if starting_bal < round(la, 2):
                                            expect += Decimal(vat)
                                    assert_eq(round(expect, 2), acc_bal)
                            else:
                                if (amt == RESIDENTIAL_BONUS_POWER_AMT and sb[1] >= add_vat(RESIDENTIAL_CONN_LOAN_AMT)) \
                                        or sb[1] >= add_vat(COMMERCIAL_CONN_LOAN_AMT):
                                    assert_eq(round(Decimal(sb[1]) - Decimal(add_vat(expected_amt)) + Decimal(amt), 2), acc_bal)
                        else:
                            assert_eq(round(Decimal(sb[1]), 2), acc_bal)
                        if op_date_str != self.before_op_date_str:
                            assert_eq(round(Decimal(expected_uncol), 2), uncol_bal)
                        else:
                            assert_eq(round(Decimal(expected_uncol) - vat, 2), uncol_bal)
                    else:
                        assert_eq(round(Decimal(sb[1]), 2), acc_bal)

            cnt_loans = Loan.objects.count()
            cnt_mth = MasterTransactionHistory.objects.count()
            # run the first use check using the default DB, and verify nothing happens
            OperationalFirstUsage.check_first_usage(False, False, 'default')
            assert_eq(cnt_mth, MasterTransactionHistory.objects.count())
            assert_eq(cnt_loans, Loan.objects.count())
