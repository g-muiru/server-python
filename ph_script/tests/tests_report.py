# coding=utf-8
"""Tests report."""
import datetime
import decimal

import mock
from django import db
from django import test
from django.core.management import call_command
from django.db import connection

import ph_model.models as ph_model
from ph_script import report
from ph_script.netsuite_csv import DB_ALIAS
from ph_util.accounting_util import DEFAULT_SMS_LOW_BALANCE_DATE_STR
from ph_util.test_utils import assert_eq, test_mode


# noinspection PyUnusedLocal
def mock_convert_currency(amount, current_currency='KSH', target_currency='USD'):
    """

    :param amount:
    :param current_currency:
    :param target_currency:
    :return:
    """
    return amount.quantize(decimal.Decimal('.01'))


class TestUtilsTestCase(test.TestCase):
    def testmode_test(self):
        if 'sqlite' in connection.vendor:
            self.assertTrue(test_mode())
            self.assertTrue(test_mode('this_test_db_test'))
            self.assertTrue(test_mode('this_db'))
        else:
            self.assertTrue(test_mode())
            self.assertTrue(test_mode('this_test_db_test'))
            self.assertFalse(test_mode('this_db'))


class UsageReportTestCase(test.TestCase):
    """Tests get usage per day."""

    def setUp(self):
        """
        Setup
        """
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_script/tests/fixtures/circuit_usage_data.json', verbosity=0)
        self.customerCircuit = ph_model.circuit.Circuit.objects.get(pk=101)
        self.nonCustomerCircuit = ph_model.circuit.Circuit.objects.get(pk=102)

    @mock.patch('ph_script.report.convert_currency', mock_convert_currency)
    def test_get_usage_per_day(self):
        """Tests ph_util.date_util.to_epoch"""
        date = datetime.datetime.strptime('Jan 2 2014', '%b %d %Y').date()
        look_back_days = 2
        usages_report = report.get_usage_charge_per_day(date=date, look_back_days=look_back_days)
        # Calculated from the fixtures.
        expected = [{'usage': '200.00', 'charge': '200.00', 'date': '2014-01-01', 'asp': '1.00',
                     'circuitDeviceId': 'Q101P101C101', 'circuitId': 101, 'circuitName': 'Q1C1'},

                    {'usage': '200.00', 'charge': '200.00', 'date': '2014-01-02', 'asp': '1.00',
                     'circuitDeviceId': 'Q101P101C101', 'circuitId': 101, 'circuitName': 'Q1C1'},

                    {'usage': '200.00', 'charge': '200.00', 'date': '2014-01-01', 'asp': '1.00',
                     'circuitDeviceId': 'Q102P102C102', 'circuitId': 102, 'circuitName': 'Q1C2'},

                    {'usage': '200.00', 'charge': '200.00', 'date': '2014-01-02', 'asp': '1.00',
                     'circuitDeviceId': 'Q102P102C102', 'circuitId': 102, 'circuitName': 'Q1C2'}]
        self.assertEquals(expected, usages_report)


class DepositReportTestCase(test.TestCase):
    """Tests get usage per day."""

    def setUp(self):
        """
        Setup
        """
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_script/tests/fixtures/credit_debit_data.json', verbosity=0)

    @mock.patch('ph_script.report.convert_currency', mock_convert_currency)
    def test_get_usage_per_day(self):
        """Tests ph_util.date_util.to_epoch"""
        date = datetime.datetime.strptime('Jan 2 2014', '%b %d %Y').date()
        look_back_days = 2
        deposit_report = report.get_deposit_per_day(date=date, look_back_days=look_back_days)
        # Calculated from the fixtures.
        expected = [{'deposit': '200.00', 'date': '2014-01-01', 'circuitDeviceId': 'Q101P101C101',
                     'circuitId': 101, 'circuitName': 'Q1C1'},
                    {'deposit': '200.00', 'date': '2014-01-02', 'circuitDeviceId': 'Q101P101C101',
                     'circuitId': 101, 'circuitName': 'Q1C1'}]
        self.assertEquals(expected, deposit_report)


class FixedChargeReportTestCase(test.TestCase):
    """Tests get usage per day."""

    def setUp(self):
        """
        Setup
        """
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_script/tests/fixtures/fixed_charge_data.json', verbosity=0)
        self.customerCircuit = ph_model.circuit.Circuit.objects.get(pk=101)
        self.nonCustomerCircuit = ph_model.circuit.Circuit.objects.get(pk=102)

    @mock.patch('ph_script.report.convert_currency', mock_convert_currency)
    def test_get_usage_per_day(self):
        """Tests ph_util.date_util.to_epoch"""
        date = datetime.datetime.strptime('Jan 2 2014', '%b %d %Y').date()
        look_back_days = 2
        usages_report = report.get_daily_tariff_fixed_charge_per_day(date=date, look_back_days=look_back_days)
        # Calculated from the fixtures.
        expected = [{'fixedCharge': '100.00', 'date': '2014-01-01', 'circuitDeviceId': 'Q101P101C101',
                     'circuitId': 101, 'circuitName': 'Q1C1'},
                    {'fixedCharge': '100.00', 'date': '2014-01-02', 'circuitDeviceId': 'Q101P101C101',
                     'circuitId': 101, 'circuitName': 'Q1C1'},
                    {'fixedCharge': '100.00', 'date': '2014-01-01', 'circuitDeviceId': 'Q102P102C102',
                     'circuitId': 102, 'circuitName': 'Q1C2'},
                    {'fixedCharge': '100.00', 'date': '2014-01-02', 'circuitDeviceId': 'Q102P102C102',
                     'circuitId': 102, 'circuitName': 'Q1C2'}]
        self.assertEquals(expected, usages_report)


class GenerateFinalReportTestCase(test.TestCase):
    """Tests get usage per day."""

    def setUp(self):
        """
        Setup
        """
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_script/tests/fixtures/fixed_charge_data.json', verbosity=0)
        call_command('loaddata', 'ph_script/tests/fixtures/circuit_usage_data.json', verbosity=0)
        call_command('loaddata', 'ph_script/tests/fixtures/credit_debit_data.json', verbosity=0)

    @mock.patch('ph_script.report.convert_currency', mock_convert_currency)
    def test_generate_report(self):
        """Tests ph_util.date_util.to_epoch"""
        # noinspection PyUnusedLocal
        date = datetime.datetime.strptime('Jan 2 2014', '%b %d %Y').date()
        # noinspection PyUnusedLocal
        look_back_days = 2
        # TODO(estifanos) #Refactor and uncomment,  this will create report file, only uncomment when required
        # report.generate_report(date=date, lookBackDays=look_back_days)


class MonthlyCsvFilesTestCase(test.TestCase):
    """
    Generate the daily csv's and validate the results
    This test depends on Postgres DB functions
    """

    def test_daily_csv(self):
        if connection.vendor != 'postgresql':
            print("test_daily_csv can only be run with a postgres DB")
            pass
        else:
            sql = \
                """
                INSERT INTO public.ph_model_country(id, created, updated, name, "countryCode", "countryIsoCode", "countryITUCode")
                VALUES(44, '2017-01-14', '2017-01-15', 'USA', 'US', 'USA', 'USA');
                INSERT INTO public.ph_model_region(id, created, updated, name, country_id)
                VALUES(44, '2017-01-14', '2017-01-15', 'USA', 44);
                INSERT INTO public.ph_model_municipality(id, created, updated, name, "timeZone", region_id, "connectionFeeDueDate")
                VALUES(44, '2017-01-14', '2017-01-15', 'Puni Muni', 'Some TZ', 44, '2017-01-01');
                INSERT INTO public.ph_model_account(id, reconnect_bal, clearing_bal, uncollected_bal, stima, created, updated, "accountBalance", "sms_low_bal_sent", "sms_neg_bal_sent")
                VALUES(44, 0.0, 0.0, 0.0, 1, '2017-01-14', '2017-01-15', 123.456, 
                """

            sql += "'" + DEFAULT_SMS_LOW_BALANCE_DATE_STR + "',"
            sql += "'" + DEFAULT_SMS_LOW_BALANCE_DATE_STR + "');"

            sql += """INSERT INTO public.ph_model_customer(created, updated, id, "customerId", "firstName", "lastName", email, "phoneNumber",
                "cumulativeDailyUsageWh", "cumulativeKwh", account_id, latitude, longitude,status, "dailySMSBalRequested", municipality_id,
                "connectionFeeLoan", "customerType", days_no_balance) VALUES('2017-01-14', '2017-01-15', 44, 44, 'Steve', 'Tester', 's@s,com',
                '+23456789', 1.1, 2.2, 44, 0, 0, 'alive', 3, 44, false, 'normal', 1);
                """

            email_subject = """start date: 2010-01-10   end date: 2120-01-10"""
            no_cust_data = "version,acc_id,created_dt,updated_dt,customer_type,first_name,last_name,national_id_number,phone_number,status," \
                           + "acc_balance,tariff_name,country,reqion,project,muni_name,queen_number\n"
            cust_data = no_cust_data + "1.0,44,14/01/2017,15/01/2017,normal,Steve,Tester,,+23456789,alive,123.45600000000000000000,," + \
                        "USA,USA,,Puni Muni,\n"

            conn = db.connections[DB_ALIAS]
            cur = conn.cursor()
            cur.execute(sql)
            cur.close()
            conn.close()

            from ph_script.netsuite_csv import monthy_accounting_csv
            csv_tuple = monthy_accounting_csv(test_dt=datetime.datetime(2020, 2, 1))
            self.assertEquals(csv_tuple[0], None, 'Error element should be None')
            assert_eq(no_cust_data, csv_tuple[3])
            csv_tuple = monthy_accounting_csv(start_dt_str='2010-01-10', end_dt_str='2120-01-10', test_dt='2017-02-02')
            assert_eq(cust_data, csv_tuple[3])
            assert_eq(email_subject in csv_tuple[4], True)
            self.assertEquals(csv_tuple[0], None, 'Error element should be None')
