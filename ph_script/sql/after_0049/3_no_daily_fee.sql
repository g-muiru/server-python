DROP FUNCTION IF EXISTS public.no_daily_fee(text);
CREATE OR REPLACE FUNCTION public.no_daily_fee(dt text)
 RETURNS TABLE(_dt character varying, no_daily_fee boolean, project character varying, grid character varying, qn character varying, cn integer, circ_id integer, vac_bad_min_cnt_15_21 bigint, vac_bad_max_cnt_15_21 bigint, recieved_cnt_15_21 bigint, percent_recieved_15_21 numeric, percent_bad_recieved_15_21 numeric, percent_bad_15_21 numeric, vac_bad_min_cnt_4_22 bigint, vac_bad_max_cnt_4_22 bigint, recieved_cnt_4_22 bigint, percent_recieved_4_22 numeric, percent_bad_recieved_4_22 numeric, percent_bad_4_22 numeric)
 LANGUAGE sql
AS $function$
select
  _dt,
  case
  	  when percent_recieved_4_22 = -1 and percent_recieved_15_21 = -1 then true
	  when a.percent_bad_4_22 > 0.67 then true
	  when b.percent_bad_15_21 > 0.5 then true
	  else false
	end as no_daily_fee,
  a.project,
 	a.grid,
 	a.qn,
 	a.cn ,
 	a.circ_id,
 	vac_bad_min_cnt_15_21,
 	vac_bad_max_cnt_15_21,
 	recieved_cnt_15_21,
 	percent_recieved_15_21,
 	percent_bad_recieved_15_21,
 	percent_bad_15_21,
 	vac_bad_min_cnt_4_22,
 	vac_bad_max_cnt_4_22,
 	recieved_cnt_4_22,
 	percent_recieved_4_22,
 	percent_bad_recieved_4_22,
 	percent_bad_4_22
from
(
-- 12 hrs 67% bad
select $1 as _dt,project, grid, qn, cn, circ_id, coalesce(vac_bad_min_cnt,-1) as vac_bad_min_cnt_4_22 ,
	coalesce(vac_bad_max_cnt,-1) as vac_bad_max_cnt_4_22,
	coalesce(expected_cnt,-1) as expected_cnt_4_22, coalesce(recieved_cnt,-1) as recieved_cnt_4_22,
	coalesce(round((recieved_cnt / expected_cnt::float)::numeric,2),-1) as percent_recieved_4_22,
	coalesce(round(((vac_bad_min_cnt + vac_bad_max_cnt) / recieved_cnt::float)::numeric,2),-1) as percent_bad_recieved_4_22,
	coalesce(case when recieved_cnt < expected_cnt
		then round(((expected_cnt - recieved_cnt + vac_bad_min_cnt + vac_bad_max_cnt) / expected_cnt::float)::numeric,2)
		else round(((vac_bad_min_cnt + vac_bad_max_cnt) / recieved_cnt::float)::numeric,2) end,-1) as percent_bad_4_22
from
	(select p.name as project, g.name as grid, q.number as qn, c.number as cn , c.id as circ_id,
	(86400 / q."monitorInterval") / 24 * 18 as expected_cnt
	from  ph_model_project as p, ph_model_grid as g, ph_model_queen as q, ph_model_circuit as c, ph_model_customer as cc
	where q.grid_id = g.id and q.id = c.queen_id and cc.circuit_id = c.id and p.id = g.project_id
	order by grid,qn,cn) as z
	left join
		(select circuit_id,
			count(id) as recieved_cnt,
			sum(case when cm."vacMin" < 200 then 1 else 0 end ) as vac_bad_min_cnt,
			sum(case when cm."vacMax" > 260 then 1 else 0 end ) as vac_bad_max_cnt
		from ph_model_circuitmonitor as cm
		where cm."collectTime" between ($1 || 'T04:00')::timestamp without time zone and
                                    ($1 || 'T22:00')::timestamp without time zone
		group by circuit_id) as x on x.circuit_id = z.circ_id
)as a
left join(
-- 6hours 50% is bad
select project, grid, qn, cn, circ_id, coalesce(vac_bad_min_cnt,-1) as vac_bad_min_cnt_15_21 ,
	coalesce(vac_bad_max_cnt,-1) as vac_bad_max_cnt_15_21,
	coalesce(expected_cnt,-1) as expected_cnt_15_21, coalesce(recieved_cnt,-1) as recieved_cnt_15_21,
	coalesce(round((recieved_cnt / expected_cnt::float)::numeric,2),-1) as percent_recieved_15_21,
	coalesce(round(((vac_bad_min_cnt + vac_bad_max_cnt) / recieved_cnt::float)::numeric,2),-1) as percent_bad_recieved_15_21,
	coalesce(case when recieved_cnt < expected_cnt
		then round(((expected_cnt - recieved_cnt + vac_bad_min_cnt + vac_bad_max_cnt) / expected_cnt::float)::numeric,2)
		else round(((vac_bad_min_cnt + vac_bad_max_cnt) / recieved_cnt::float)::numeric,2) end,-1) as percent_bad_15_21
from
	(select p.name as project, g.name as grid, q.number as qn, c.number as cn , c.id as circ_id,
	(86400 / q."monitorInterval") /24 * 6 as expected_cnt
	from  ph_model_project as p, ph_model_grid as g, ph_model_queen as q, ph_model_circuit as c, ph_model_customer as cc
	where q.grid_id = g.id and q.id = c.queen_id and cc.circuit_id = c.id and p.id = g.project_id
	order by grid,qn,cn) as z
	left join
		(select circuit_id,
			count(id) as recieved_cnt,
			sum(case when cm."vacMin" < 200 then 1 else 0 end ) as vac_bad_min_cnt,
			sum(case when cm."vacMax" > 260 then 1 else 0 end ) as vac_bad_max_cnt
		from ph_model_circuitmonitor as cm
		where cm."collectTime" between ($1 || 'T15:00')::timestamp without time zone and
                                    ($1 || 'T21:00')::timestamp without time zone
		group by circuit_id) as x on x.circuit_id = z.circ_id
)as b on a.project = b.project and a.grid = b.grid and  a.qn = b.qn and a.cn = b.cn and a.circ_id = b.circ_id;
$function$;
COMMENT ON FUNCTION public.no_daily_fee(text)
    IS 'Used to evaluate circuits without good voltage and decide whether to charge the daily fixed fee.
    No matter the date, the monitor interval is the current queen setting.

Examples:
Select * from no_daily_fee(''2016-12-01'');

Params
dt        the date to check
';
