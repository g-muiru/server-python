DROP VIEW IF EXISTS  public.trans_diff;
CREATE OR REPLACE VIEW public.trans_diff AS
 SELECT a.project,
    a.muni,
    a.grid,
    a.op_date,
    a.acc_id,
    a.circ_id,
    min(a.dt) AS dt,
    max(abs(a.mth_bal - (a.bal - uncol_bal))) AS abs_diff,
    min(a.acc_bal) AS acc_bal
   FROM ( SELECT p.name AS project,
            m.name AS muni,
            g.name AS grid,
            g.operational_date AS op_date,
            mth.id AS mth_id,
            mth.account_id AS acc_id,
            mth.created AS dt,
            round(mth."accountBalance", 6) AS mth_bal,
            c.circuit_id AS circ_id,
            round(sum(
                CASE
                    WHEN mth.source::text ~~ '%INITc%'::text THEN 0::numeric
                    WHEN mth."transactionType"::text = 'Credit'::text THEN mth.amount
                    WHEN mth."transactionType"::text <> 'Credit'::text THEN - mth.amount
                    ELSE NULL::numeric
                END) OVER (PARTITION BY mth.account_id ORDER BY mth.account_id, mth."sourceProcessedTime", mth.id, mth.source, mth."transactionType"), 6) AS bal,
            round(a_1."accountBalance", 6) AS acc_bal,
            round(coalesce(a_1.uncollected_bal,0), 6) as uncol_bal
           FROM ph_model_mastertransactionhistory mth
             LEFT JOIN ph_model_customer c ON c.account_id = mth.account_id
             LEFT JOIN ph_model_account a_1 ON c.account_id = a_1.id
             LEFT JOIN ph_model_municipality m ON m.id = c.municipality_id
             LEFT JOIN ph_model_circuit circuit ON c.circuit_id = circuit.id
             LEFT JOIN ph_model_queen q ON circuit.queen_id = q.id
             LEFT JOIN ph_model_grid g ON q.grid_id = g.id
             LEFT JOIN ph_model_project p ON g.project_id = p.id
          ORDER BY mth.account_id, mth."sourceProcessedTime", mth.id, mth.source, mth."transactionType") a
  GROUP BY a.project, a.muni, a.grid, a.op_date, a.acc_id, a.circ_id
  ORDER BY (max(abs(a.mth_bal - a.bal))) DESC, a.muni, (min(a.dt)), a.acc_id;

COMMENT ON VIEW public.trans_diff IS
'Used to see errors in account balance in the matertransactionhistory table.

Example:
SELECT * FROM trans_diff WHERE abs_diff between 0.01 and 100;
';