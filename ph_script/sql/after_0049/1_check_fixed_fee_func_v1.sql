-- FUNCTION: public.customer_no_fixed_fee_v1(text)
DROP FUNCTION IF EXISTS public.customer_no_fixed_fee_v1(text);

CREATE OR REPLACE FUNCTION public.customer_no_fixed_fee_v1(d1 text)
    RETURNS TABLE(
    acc_id integer,
    cnt_fixed_billed bigint,
    amount_fixed_billed numeric,
    cnt_var_billed bigint,
    amount_var_billed numeric,
    dt date,
    op_date date,
    queen_op_date date,
    grid character varying,
    qn character varying,
    cn integer)
    LANGUAGE 'sql'
    COST 100
    VOLATILE
    ROWS 1000
AS $function$
select cc.account_id,
      coalesce(mx,-1) as cnt_fixed_billed, round(coalesce(xx,-1),2) as amount_fixed_billed,
      coalesce(my,-1) as cnt_var_billed, round(coalesce(xy,-1),2) as amount_var_billed,
      dt, grid.operational_date as op_date, queen.operational_date as queen_op_date,
      grid.name as grid, queen.number as qn, circuit."number" as cn
FROM ph_model_customer  AS cc
LEFT JOIN ph_model_tariff  AS tariff ON cc.tariff_id = "tariffId"
LEFT JOIN ph_model_account  AS account ON cc.account_id = account.id
LEFT JOIN ph_model_circuit  AS circuit ON cc.circuit_id = circuit.id
LEFT JOIN ph_model_queen  AS queen ON circuit.queen_id = queen.id
LEFT JOIN ph_model_grid  AS grid ON queen.grid_id = grid.id
left join
  (select account_id, date("sourceProcessedTime") as dt,
      sum(case when  "source" like any (array['RES_FIXED%','COM1_FIXED%','COM2_FIXED%']) then 1 else 0 end) as mx,
      sum(case when  "source" like any (array['RES_FIXED%','COM1_FIXED%','COM2_FIXED%']) then amount else 0 end) as xx,
      sum(case when  "source" like any (array['RES_VAR%','COM1_VAR%','COM2_VAR%']) then 1 else 0 end) as my,
      sum(case when  "source" like any (array['RES_VAR%','COM1_VAR%','COM2_VAR%']) then amount else 0 end) as xy
   from ph_model_mastertransactionhistory
   where date("sourceProcessedTime") = DATE($1)
   group by account_id, dt) as mth
on cc.account_id = mth.account_id
WHERE
   cc.circuit_id is not null
   and (grid.operational_date <= DATE($1) OR queen.operational_date <= DATE($1))
   and grid.name != 'EngrValidation'
   order by cnt_fixed_billed;
$function$;

COMMENT ON FUNCTION public.customer_no_fixed_fee_v1(text)
    IS 'Returns all customers from operational sites with circuits.

cnt_fixed_billed = -1 means no fixed charges recorded, could be meter comm problem
cnt_fixed_billed =  0 means no fixed charges assessed because of voltage problems

cnt_var_billed = -1 means no variable charges recorded, could be meter comm problem
cnt_var_billed =  0 means no variable charges assessed because of voltage problems

Examples:
Select * from customer_no_fixed_fee_v1(''2016-12-01'',''2016-12-02'');

Params
d1      date to check';


