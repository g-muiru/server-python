DROP VIEW IF EXISTS public.operational_first_use;

CREATE OR REPLACE VIEW public.operational_first_use AS
 SELECT ofu.project,
    ofu.grid,
    ofu.acc_id,
    ofu.op_date,
    ofu.queen_op_date,
    ofu.collect_date,
    ofu.ttl_wh,
    ofu.connection_loan,
        CASE
            WHEN l.cnt > 0 THEN true
            ELSE false
        END AS conn_loan_exists,
    ll."loanType" AS loan_type
   FROM ( SELECT project.name AS project,
            grid.name AS grid,
            c.account_id AS acc_id,
            c."connectionFeeLoan" AS connection_loan,
            max(grid.operational_date) AS op_date,
            max(queen.operational_date) as queen_op_date,
            round(sum(usage."intervalWh"), 2) AS ttl_wh,
            min(date(usage."collectTime")) AS collect_date
           FROM ph_model_usage usage
             LEFT JOIN ph_model_circuit circuit ON usage.circuit_id = circuit.id
             LEFT JOIN ph_model_customer c ON c.circuit_id = circuit.id
             LEFT JOIN ph_model_queen queen ON circuit.queen_id = queen.id
             LEFT JOIN ph_model_grid grid ON queen.grid_id = grid.id
             LEFT JOIN ph_model_project project ON project.id = grid.project_id
          WHERE (grid.operational_date <= now() OR queen.operational_date  <= now())
          AND c.account_id IS NOT NULL AND date(usage."collectTime") >= grid.operational_date
          GROUP BY project.name, grid.name, c.account_id, c."connectionFeeLoan") ofu
     LEFT JOIN ( SELECT ph_model_loan.account_id,
            count(*) AS cnt
           FROM ph_model_loan
          WHERE ph_model_loan."loanType"::text like '%CONNECTION_FEE_LOAN%'::text
          GROUP BY ph_model_loan.account_id) l ON l.account_id = ofu.acc_id
     LEFT JOIN ph_model_loan ll ON ll.account_id = ofu.acc_id
  WHERE ofu.ttl_wh > 10::numeric
  and ofu.grid not in ('BaraNne','Mokomoni','Nyamondo','Ogembo', 'Matangamano');

COMMENT ON VIEW public.operational_first_use
    IS 'Get date of first use on a circuit on or after the operational date.
    The operational date must be in the past and usage for the day must be greater than 10 wh.
    NOTE: The operational date can be set into the future to disable HC from charging customers for usage.';