DROP VIEW IF EXISTS public.mth_trans_before_today;

CREATE OR REPLACE VIEW public.mth_trans_before_today AS
 SELECT m.id,
    m.created,
    m.updated,
    m.amount,
    m.account_id,
    m."transactionType",
    m.source,
    m."sourceId",
    m."sourceProcessedTime",
    m."accountBalance"
   FROM ph_model_mastertransactionhistory m,
    ( SELECT m_1.account_id,
            x.dt,
            min(m_1.id) AS mid
           FROM ph_model_mastertransactionhistory m_1,
            ( SELECT ph_model_mastertransactionhistory.account_id,
                    max(date(ph_model_mastertransactionhistory."sourceProcessedTime")) AS dt
                   FROM ph_model_mastertransactionhistory
                  WHERE date(ph_model_mastertransactionhistory.created) <= '2017-10-25'::date
                  GROUP BY ph_model_mastertransactionhistory.account_id) x
          WHERE date(m_1.created) = x.dt AND m_1.account_id = x.account_id
          GROUP BY m_1.account_id, x.dt
          ORDER BY x.dt DESC, m_1.account_id) y
  WHERE m.id = y.mid
  ORDER BY m.created DESC;

COMMENT ON VIEW public.mth_trans_before_today
    IS 'Returns the most recent transactions by account_id, excluding today''s transactions.

Example:
SELECT * FROM mth_trans_before_today WHERE account_id = 42033;
';
