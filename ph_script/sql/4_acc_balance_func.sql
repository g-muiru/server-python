-- FUNCTION: public.acc_balance(text, text)

DROP FUNCTION IF EXISTS public.acc_balance(text, text);

CREATE OR REPLACE FUNCTION public.acc_balance(d1 text,d2 text)
    RETURNS TABLE(
        version character varying,
        row_num bigint,
        acc_id integer,
        max_dt timestamp without time zone,
        bal numeric,
        principle numeric,
        mth_adjust numeric,
        mth_after_adjust numeric,
        current_balance numeric,
        query_date_balance character varying,
        error numeric)
    LANGUAGE 'sql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
    ROWS 1000.0
AS $function$

SELECT
	'1.0'::varchar as version,
	row_number() OVER(ORDER BY acc.id) as row_num,
	acc.id as acc_id,
	max_dt::timestamp without time zone,
	(select round("accountBalance",2) from ph_model_mastertransactionhistory
	where ph_model_mastertransactionhistory.account_id = acc.id
	and id = max_id) as bal,
    COALESCE(ll.principle,0),
    COALESCE(adjust, 0) as mth_adjust,
    COALESCE(after_adjust, 0) as mth_after_adjust,
    acc."accountBalance" as current_balance,
    to_char(acc."accountBalance" - COALESCE(after_adjust, 0), '999999999999999999999999999D9999999999999999999999999999999') as query_date_balance,
    (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)) as error
FROM ph_model_account as acc
LEFT JOIN (SELECT account_id, sum(adjust) as adjust, max(max_dt) as max_dt, max(max_id) as max_id
           FROM (SELECT account_id, "transactionType",
                max(id) as max_id,
                max("sourceProcessedTime") as max_dt,
                CASE
                    WHEN "transactionType" = 'Credit' THEN sum(amount)
                    ELSE -sum(amount)
                END as adjust
                FROM public.ph_model_mastertransactionhistory
                WHERE "sourceProcessedTime" <= $2::timestamp without time zone AND
                      "sourceProcessedTime" >= $1::timestamp without time zone
            GROUP BY  account_id, "transactionType") as xxx
           GROUP BY  account_id) as mth
ON mth.account_id = acc.id
LEFT JOIN (SELECT account_id, sum(adjust) as after_adjust FROM (SELECT account_id, "transactionType",
                CASE
                    WHEN "transactionType" = 'Credit' THEN sum(amount)
                    ELSE -sum(amount)
                END as adjust
                FROM public.ph_model_mastertransactionhistory
                WHERE "sourceProcessedTime" >= $2::timestamp without time zone
            GROUP BY  account_id, "transactionType") as yyy
           GROUP BY  account_id) as mth_after
ON mth_after.account_id = acc.id
LEFT JOIN (SELECT ll.account_id, loan_amt,
  sum(ll.amount) as paid,
  round(loan_amt - COALESCE(sum(ll.amount),0),4)::numeric as principle,
  cnt_payments,count(ll.amount)/2 as cnt,
  payment,
  sum(ll.amount) / (count(ll.amount)/2) calc_payment from
  (SELECT account_id, loan_id,round("loanAmount"*1.16,0) as loan_amt,
  "outStandingPrinciple" as principle,
  "totalNumberOfPayments" as cnt_payments,
  round("loanAmount"*1.16 / "totalNumberOfPayments",6) as payment,
  "processedTime",
   amount
	FROM ph_model_loan as l
  LEFT JOIN ph_model_loanpaymenthistory as lph
  ON l.id = lph.loan_id and amount > 0 and "processedTime" < $2::timestamp
  where "loanAmount" > 1500
   ) as ll
   group by ll.account_id, payment,loan_amt,cnt_payments) as ll
   ON acc.id = ll.account_id
ORDER BY acc.id

$function$;


COMMENT ON FUNCTION public.acc_balance(text, text)
    IS 'Used to get account balance on a particular date for all accounts

Examples:
Select * from acc_balance(''2016-12-01 00:00:00'', ''2016-12-11 00:00:00'');

Select * from acc_balance(''2017-01-01'', ''2017-01-07'');

Params
d1        ignore transactions befor this date
d2        Date to get balance on, if in the future, then current balance';
