-- FUNCTION: public.customers(text, text)

DROP FUNCTION IF EXISTS public.customers(text, text);

CREATE OR REPLACE FUNCTION public.customers(
	d1 text,
	d2 text)
    RETURNS TABLE(
        version varchar,
        acc_id integer,
        created_dt varchar,
        updated_dt varchar,
        customer_type varchar,
        first_name varchar,
        last_name varchar,
        national_id_number varchar,
        phone_number varchar,
        status varchar,
        acc_balance numeric,
        tariff_name varchar,
        country varchar,
        reqion varchar,
        project varchar,
        muni_name varchar,
        queen_number varchar)
    LANGUAGE 'sql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
    ROWS 1000.0
AS $function$

SELECT
	'1.0'::varchar AS version,
	account.id  AS acc_id,
	TO_CHAR(account.created , 'YYYY-MM-DD') AS created_dt,
    TO_CHAR(account.updated , 'YYYY-MM-DD') AS updated_dt,
	customer."customerType"  AS customer_type,
	customer."firstName" AS first_name,
    customer."lastName" AS last_name,
	customer."nationalIdNumber"  AS national_id_number,
	customer."phoneNumber"  AS phone_number,
	customer.status  AS status,
	account."accountBalance"  AS acc_balance,
	tariff.name  AS tariff_name,
	country.name  AS country,
	region.name  AS region,
	project.name  AS project,
	municipality.name  AS muni_name,
	queen.number  AS queen_number
FROM ph_model_customer  AS customer
LEFT JOIN ph_model_account  AS account ON (customer.account_id) = (account.id)
LEFT JOIN ph_model_circuit  AS circuit ON (customer.circuit_id) = (circuit.id)
LEFT JOIN ph_model_tariff  AS tariff ON (customer.tariff_id) = (tariff."tariffId")
LEFT JOIN ph_model_municipality  AS municipality ON (customer.municipality_id) = (municipality.id)
LEFT JOIN ph_model_queen  AS queen ON (circuit.queen_id) = (queen.id)
LEFT JOIN ph_model_grid  AS grid ON (queen.grid_id) = (grid.id)
LEFT JOIN ph_model_project  AS project ON (grid.project_id) = (project.id)
LEFT JOIN ph_model_region  AS region ON (municipality.region_id) = (region.id)
LEFT JOIN ph_model_country  AS country ON (region.country_id) = (country.id)

WHERE
    account.created >= $1::timestamp without time zone AND
    account.created <= $2::timestamp without time zone
ORDER BY account.id

$function$;

COMMENT ON FUNCTION public.customers(text, text)
    IS 'Used to get customer info. Uses created date to filter records

Examples:
Select * from customers(''2016-12-01 00:00:00'',''2016-12-02 00:00:00'');

Select * from customers(''2016-12-31'',''2017-01-01'');

Params
d1        Start date
d2        End date';
