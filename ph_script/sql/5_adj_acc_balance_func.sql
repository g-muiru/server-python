-- FUNCTION: public.adj_acc_balance(text, text)

DROP FUNCTION IF EXISTS public.adj_acc_balance(text, text);

CREATE OR REPLACE FUNCTION public.adj_acc_balance(d1 text, d2 text)
    RETURNS TABLE(
        version character varying,
        row_num bigint,
        acc_id integer,
        max_dt timestamp without time zone,
        bal numeric,
        principle numeric,
        mth_adjust numeric,
        mth_after_adjust numeric,
        current_balance numeric,
        query_date_balance character varying,
        error numeric)
    LANGUAGE 'sql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
    ROWS 1000.0
AS $function$

SELECT
	'1.0'::varchar as version,
	row_number() OVER(ORDER BY acc.id) as row_num,
	acc.id as acc_id,
	max_dt::timestamp without time zone,
	(select round("accountBalance",2) from ph_model_mastertransactionhistory
	where ph_model_mastertransactionhistory.account_id = acc.id
	and id = max_id) as bal,
    COALESCE(ll.principle,0),
    COALESCE(adjust, 0) as mth_adjust,
    COALESCE(after_adjust, 0) as mth_after_adjust,
    acc."accountBalance" as current_balance,
    to_char(acc."accountBalance" - COALESCE(after_adjust, 0), '999999999999999999999999999D9999999999999999999999999999999') as query_date_balance,
    CASE
    	WHEN d1 > '2014-01-01' THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-33.881)
    	ELSE CASE
        WHEN acc.id = 10101 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-33.881)
        WHEN acc.id = 10102 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-36.31)
        WHEN acc.id = 10103 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-48.457)
        WHEN acc.id = 10104 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-76.424)
        WHEN acc.id = 10105 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-86.435)
        WHEN acc.id = 10106 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-35.269)
        WHEN acc.id = 10107 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+0.28)
        WHEN acc.id = 10108 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-240.201)
        WHEN acc.id = 10109 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-219.664)
        WHEN acc.id = 10301 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-5.015)
        WHEN acc.id = 10302 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1.597)
        WHEN acc.id = 10303 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-45.872)
        WHEN acc.id = 10304 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-154.949)
        WHEN acc.id = 10305 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-153.028)
        WHEN acc.id = 10306 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-95.609)
        WHEN acc.id = 10307 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-607.553)
        WHEN acc.id = 10308 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-145.289)
        WHEN acc.id = 10309 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-187.875)
        WHEN acc.id = 10310 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-2.159)
        WHEN acc.id = 10313 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-92.743)
        WHEN acc.id = 10315 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-51.246)
        WHEN acc.id = 20501 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-31.461)
        WHEN acc.id = 20502 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1.696)
        WHEN acc.id = 20503 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-65.179)
        WHEN acc.id = 20504 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1040.472)
        WHEN acc.id = 20505 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-241.04)
        WHEN acc.id = 20506 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-26.664)
        WHEN acc.id = 20507 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-35.342)
        WHEN acc.id = 20508 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-22.463)
        WHEN acc.id = 20509 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-65.823)
        WHEN acc.id = 20510 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-69.969)
        WHEN acc.id = 20513 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-9.91)
        WHEN acc.id = 20514 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-55.461)
        WHEN acc.id = 20515 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-43.04)
        WHEN acc.id = 20516 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-708.101)
        WHEN acc.id = 20517 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-326.737)
        WHEN acc.id = 20518 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-191.53)
        WHEN acc.id = 20519 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-46.373)
        WHEN acc.id = 20520 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-69.832)
        WHEN acc.id = 20601 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+0.686)
        WHEN acc.id = 20602 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-95.447)
        WHEN acc.id = 20603 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-93.152)
        WHEN acc.id = 20604 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1.055)
        WHEN acc.id = 20605 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-4.843)
        WHEN acc.id = 20606 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-35.224)
        WHEN acc.id = 20607 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-88.45)
        WHEN acc.id = 20608 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-514.11)
        WHEN acc.id = 20609 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-51.024)
        WHEN acc.id = 20610 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-31.091)
        WHEN acc.id = 20613 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-295.18)
        WHEN acc.id = 20614 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-336.201)
        WHEN acc.id = 20615 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+6.169)
        WHEN acc.id = 20616 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-240.554)
        WHEN acc.id = 20617 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-335.668)
        WHEN acc.id = 20618 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-138.752)
        WHEN acc.id = 20619 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-327.346)
        WHEN acc.id = 20620 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-237.922)
        WHEN acc.id = 20701 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+0.204)
        WHEN acc.id = 20702 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-0.172)
        WHEN acc.id = 20703 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-261.253)
        WHEN acc.id = 20704 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+8.23)
        WHEN acc.id = 20705 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-776.53)
        WHEN acc.id = 20706 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-149.587)
        WHEN acc.id = 20707 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+0.011)
        WHEN acc.id = 20708 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-177.179)
        WHEN acc.id = 20709 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-144.149)
        WHEN acc.id = 20710 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+22.368)
        WHEN acc.id = 20713 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-97.058)
        WHEN acc.id = 20714 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-29.914)
        WHEN acc.id = 20715 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-229.298)
        WHEN acc.id = 20716 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-50.243)
        WHEN acc.id = 20717 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+0.103)
        WHEN acc.id = 20718 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-30.249)
        WHEN acc.id = 20719 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+108.978)
        WHEN acc.id = 20720 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-151.442)
        WHEN acc.id = 20801 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-22.955)
        WHEN acc.id = 20802 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-32.501)
        WHEN acc.id = 20803 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-581.497)
        WHEN acc.id = 20804 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-104.333)
        WHEN acc.id = 20805 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-329.878)
        WHEN acc.id = 20806 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1.398)
        WHEN acc.id = 20807 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-407.192)
        WHEN acc.id = 20808 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1862.106)
        WHEN acc.id = 20809 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-88.786)
        WHEN acc.id = 20810 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-46.829)
        WHEN acc.id = 20813 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-21.332)
        WHEN acc.id = 20814 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-50.229)
        WHEN acc.id = 20815 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-823.167)
        WHEN acc.id = 20816 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-195.307)
        WHEN acc.id = 20817 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-56.597)
        WHEN acc.id = 20818 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-49.698)
        WHEN acc.id = 20819 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-955.726)
        WHEN acc.id = 20820 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-305.259)
        WHEN acc.id = 20901 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-203.799)
        WHEN acc.id = 20902 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-532.209)
        WHEN acc.id = 20903 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-47.923)
        WHEN acc.id = 20904 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-2.111)
        WHEN acc.id = 20905 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-40.782)
        WHEN acc.id = 20906 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-402.354)
        WHEN acc.id = 20907 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-300.91)
        WHEN acc.id = 20908 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-242)
        WHEN acc.id = 20909 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-240.283)
        WHEN acc.id = 20910 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-295.745)
        WHEN acc.id = 20913 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-6.711)
        WHEN acc.id = 20914 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1217.378)
        WHEN acc.id = 20915 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-109.196)
        WHEN acc.id = 20916 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-251.626)
        WHEN acc.id = 20917 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-12.968)
        WHEN acc.id = 20918 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-165.469)
        WHEN acc.id = 20919 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-69.145)
        WHEN acc.id = 20920 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-116.349)
        WHEN acc.id = 21001 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-264.314)
        WHEN acc.id = 21002 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-109.389)
        WHEN acc.id = 21003 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-10.18)
        WHEN acc.id = 21004 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-171.228)
        WHEN acc.id = 21005 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-364.238)
        WHEN acc.id = 21006 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-69.865)
        WHEN acc.id = 21007 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-206.836)
        WHEN acc.id = 21008 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-400.066)
        WHEN acc.id = 21009 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-52.11)
        WHEN acc.id = 21010 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-234.155)
        WHEN acc.id = 21011 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-593.263)
        WHEN acc.id = 21012 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-157.7)
        WHEN acc.id = 21013 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-58.179)
        WHEN acc.id = 21014 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-0.902)
        WHEN acc.id = 21015 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-36.637)
        WHEN acc.id = 21016 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-11.722)
        WHEN acc.id = 21017 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-28.364)
        WHEN acc.id = 21019 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-78.682)
        WHEN acc.id = 21020 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-42.841)
        WHEN acc.id = 21101 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-59.967)
        WHEN acc.id = 21102 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-123.070675479563)
        WHEN acc.id = 21103 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-256.569)
        WHEN acc.id = 21104 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-11.083349170971)
        WHEN acc.id = 21105 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-322.22)
        WHEN acc.id = 21106 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-176.052100508746)
        WHEN acc.id = 21107 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-39.372)
        WHEN acc.id = 21108 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-228.58457177257)
        WHEN acc.id = 21109 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-90)
        WHEN acc.id = 21110 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-277.2498069442)
        WHEN acc.id = 21113 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-0.127)
        WHEN acc.id = 21114 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+0.201986527804)
        WHEN acc.id = 21115 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+0.134)
        WHEN acc.id = 21116 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-49.229685928612)
        WHEN acc.id = 21117 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-7.792)
        WHEN acc.id = 21118 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-156.079273233036)
        WHEN acc.id = 21119 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-90.877)
        WHEN acc.id = 21120 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-20.219828119991)
        WHEN acc.id = 31201 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-84.383)
        WHEN acc.id = 31203 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-96.6819122511)
        WHEN acc.id = 31204 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-474.515)
        WHEN acc.id = 31205 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+854.56183448285)
        WHEN acc.id = 31206 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-84.112)
        WHEN acc.id = 31207 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-164.079)
        WHEN acc.id = 31208 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-114.712)
        WHEN acc.id = 31209 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-217.746)
        WHEN acc.id = 31210 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-379.929)
        WHEN acc.id = 31211 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+45.964)
        WHEN acc.id = 31212 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+18.144)
        WHEN acc.id = 31213 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-390.072)
        WHEN acc.id = 31214 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-83.583)
        WHEN acc.id = 31215 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-258.154)
        WHEN acc.id = 31216 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-792.949)
        WHEN acc.id = 31217 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-301.5461895072)
        WHEN acc.id = 31218 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-84.676)
        WHEN acc.id = 31219 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-282.017)
        WHEN acc.id = 31220 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-501.421)
        WHEN acc.id = 31301 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-43.407)
        WHEN acc.id = 31302 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-196.436)
        WHEN acc.id = 31303 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-247.824)
        WHEN acc.id = 31304 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-83.932)
        WHEN acc.id = 31305 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-83.726)
        WHEN acc.id = 31306 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-197.367)
        WHEN acc.id = 31307 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-568.017)
        WHEN acc.id = 31308 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-287.716)
        WHEN acc.id = 31309 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-0.195)
        WHEN acc.id = 31310 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-153.392)
        WHEN acc.id = 31313 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-85.067)
        WHEN acc.id = 31314 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-181.879)
        WHEN acc.id = 31315 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-84.15)
        WHEN acc.id = 31316 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-112.583)
        WHEN acc.id = 31317 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-347.855)
        WHEN acc.id = 31318 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-348.981)
        WHEN acc.id = 31319 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-84.035)
        WHEN acc.id = 31320 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-131.224)
        WHEN acc.id = 31401 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-777.67)
        WHEN acc.id = 31402 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-33.889)
        WHEN acc.id = 31403 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-347.663)
        WHEN acc.id = 31404 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-207.322)
        WHEN acc.id = 31405 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-264.033)
        WHEN acc.id = 31406 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-200.547)
        WHEN acc.id = 31407 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-226.96)
        WHEN acc.id = 31408 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-499.797)
        WHEN acc.id = 31409 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-286.198)
        WHEN acc.id = 31410 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-494.518)
        WHEN acc.id = 31413 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-200.459)
        WHEN acc.id = 31414 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-226.323)
        WHEN acc.id = 31415 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-273.044)
        WHEN acc.id = 31416 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-453.471)
        WHEN acc.id = 31417 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-500.82)
        WHEN acc.id = 31418 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-448.554)
        WHEN acc.id = 31419 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-572.72)
        WHEN acc.id = 31420 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-343.89)
        WHEN acc.id = 31501 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-286.897)
        WHEN acc.id = 31502 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-2.456)
        WHEN acc.id = 31503 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-329.784)
        WHEN acc.id = 31504 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-226.301)
        WHEN acc.id = 31505 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-203.217)
        WHEN acc.id = 31507 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-257.837)
        WHEN acc.id = 31508 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-454.173)
        WHEN acc.id = 31509 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-251.908)
        WHEN acc.id = 31510 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-363.976)
        WHEN acc.id = 31513 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-202.622)
        WHEN acc.id = 31514 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-264.08)
        WHEN acc.id = 31515 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-262.049)
        WHEN acc.id = 31516 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-203.903)
        WHEN acc.id = 31517 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-222.392)
        WHEN acc.id = 31518 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-285.201)
        WHEN acc.id = 31519 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-267.126)
        WHEN acc.id = 31520 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-283.383)
        WHEN acc.id = 41601 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+2039.07)
        WHEN acc.id = 41602 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-911.26)
        WHEN acc.id = 41603 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+203.25)
        WHEN acc.id = 41604 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1495.46)
        WHEN acc.id = 41605 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+49.94)
        WHEN acc.id = 41606 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+939.98)
        WHEN acc.id = 41607 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+1573.32)
        WHEN acc.id = 41608 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+707.43)
        WHEN acc.id = 41609 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+6573.63676069109)
        WHEN acc.id = 41610 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-204.61)
        WHEN acc.id = 41612 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-363.28)
        WHEN acc.id = 41613 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+1185.79)
        WHEN acc.id = 41614 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1923.76)
        WHEN acc.id = 41615 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+3469.8)
        WHEN acc.id = 41616 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-2218.09)
        WHEN acc.id = 41617 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1549.43)
        WHEN acc.id = 41618 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-840.91)
        WHEN acc.id = 41619 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+381.999040938314)
        WHEN acc.id = 41701 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+582.98)
        WHEN acc.id = 41702 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+339.89)
        WHEN acc.id = 41703 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+223.59)
        WHEN acc.id = 41704 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+735.04)
        WHEN acc.id = 41705 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+786.43)
        WHEN acc.id = 41706 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+1333.98)
        WHEN acc.id = 41707 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+146.94)
        WHEN acc.id = 41708 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-2549.18)
        WHEN acc.id = 41709 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-2582.02)
        WHEN acc.id = 41710 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+298.71)
        WHEN acc.id = 41713 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+926.1)
        WHEN acc.id = 41714 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1.34)
        WHEN acc.id = 41715 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+334.23)
        WHEN acc.id = 41716 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+6195.9)
        WHEN acc.id = 41717 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+284.06)
        WHEN acc.id = 41718 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+352.25)
        WHEN acc.id = 41719 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+5233.48762098842)
        WHEN acc.id = 41720 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+917.03)
        WHEN acc.id = 41801 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-78.77)
        WHEN acc.id = 41802 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-7.227)
        WHEN acc.id = 41803 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-26.199)
        WHEN acc.id = 41804 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-27.232)
        WHEN acc.id = 41805 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-186.119)
        WHEN acc.id = 41806 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-32.497)
        WHEN acc.id = 41807 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-148.68)
        WHEN acc.id = 41808 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-10.788)
        WHEN acc.id = 41809 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-729.648)
        WHEN acc.id = 41810 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-118.238)
        WHEN acc.id = 41813 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-53.365)
        WHEN acc.id = 41814 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-437.945)
        WHEN acc.id = 41815 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-204.719)
        WHEN acc.id = 41816 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-169.507)
        WHEN acc.id = 41817 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-6.314)
        WHEN acc.id = 41818 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-100.37)
        WHEN acc.id = 41819 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-78.49)
        WHEN acc.id = 41820 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-173.161)
        WHEN acc.id = 41901 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-41.187)
        WHEN acc.id = 41902 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-263.869)
        WHEN acc.id = 41903 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-7.43)
        WHEN acc.id = 41904 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-47.865)
        WHEN acc.id = 41905 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-33.181)
        WHEN acc.id = 41906 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-2.771)
        WHEN acc.id = 41907 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-67.455)
        WHEN acc.id = 41908 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-4.613)
        WHEN acc.id = 41909 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-54.311)
        WHEN acc.id = 41910 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-24.919)
        WHEN acc.id = 41913 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-62.461)
        WHEN acc.id = 41914 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-3.124)
        WHEN acc.id = 41915 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-41.61)
        WHEN acc.id = 41916 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-97.157)
        WHEN acc.id = 41917 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-30.111)
        WHEN acc.id = 41918 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-90.781)
        WHEN acc.id = 41919 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-91.47)
        WHEN acc.id = 41920 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-9.248)
        WHEN acc.id = 41921 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+395.821)
        WHEN acc.id = 41922 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-233.93289474007)
        WHEN acc.id = 41923 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-552.06573516985)
        WHEN acc.id = 41924 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+265.517)
        WHEN acc.id = 41925 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+174.863)
        WHEN acc.id = 41926 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-44.177)
        WHEN acc.id = 41927 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+267.852)
        WHEN acc.id = 41928 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-0)
        WHEN acc.id = 41929 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+42.74858903235)
        WHEN acc.id = 41930 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-0)
        WHEN acc.id = 41932 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-1100)
        WHEN acc.id = 41933 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-100000)
        WHEN acc.id = 41935 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+500)
        WHEN acc.id = 41937 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+450)
        WHEN acc.id = 41939 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-123)
        WHEN acc.id = 41940 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-99)
        WHEN acc.id = 41941 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)-50000)
        WHEN acc.id = 42576 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+3000)
        WHEN acc.id = 42546 THEN (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0)+19250)
        ELSE (acc."accountBalance" - COALESCE(after_adjust, 0) - COALESCE(adjust, 0))
        END
	END as error
FROM ph_model_account as acc
LEFT JOIN (SELECT account_id, sum(adjust) as adjust, max(max_dt) as max_dt, max(max_id) as max_id
           FROM (SELECT account_id, "transactionType",
                max(id) as max_id,
                max("sourceProcessedTime") as max_dt,
                CASE
                    WHEN "transactionType" = 'Credit' THEN sum(amount)
                    ELSE -sum(amount)
                END as adjust
                FROM public.ph_model_mastertransactionhistory
                WHERE "sourceProcessedTime" <= $2::timestamp without time zone AND
                      "sourceProcessedTime" >= $1::timestamp without time zone
            GROUP BY  account_id, "transactionType") as xxx
           GROUP BY  account_id) as mth
ON mth.account_id = acc.id
LEFT JOIN (SELECT account_id, sum(adjust) as after_adjust FROM (SELECT account_id, "transactionType",
                CASE
                    WHEN "transactionType" = 'Credit' THEN sum(amount)
                    ELSE -sum(amount)
                END as adjust
                FROM public.ph_model_mastertransactionhistory
                WHERE "sourceProcessedTime" >= $2::timestamp without time zone
            GROUP BY  account_id, "transactionType") as yyy
           GROUP BY  account_id) as mth_after
ON mth_after.account_id = acc.id
LEFT JOIN (SELECT ll.account_id, loan_amt,
  sum(ll.amount) as paid,
  round(loan_amt - COALESCE(sum(ll.amount),0),4)::numeric as principle,
  cnt_payments,count(ll.amount)/2 as cnt,
  payment,
  sum(ll.amount) / (count(ll.amount)/2) calc_payment from
  (SELECT account_id, loan_id,round("loanAmount"*1.16,0) as loan_amt,
  "outStandingPrinciple" as principle,
  "totalNumberOfPayments" as cnt_payments,
  round("loanAmount"*1.16 / "totalNumberOfPayments",6) as payment,
  "processedTime",
   amount
	FROM ph_model_loan as l
  LEFT JOIN ph_model_loanpaymenthistory as lph
  ON l.id = lph.loan_id and amount > 0 and "processedTime" < $2::timestamp
  where "loanAmount" > 1500
   ) as ll
   group by ll.account_id, payment,loan_amt,cnt_payments) as ll
   ON acc.id = ll.account_id
ORDER BY acc.id
$function$;

COMMENT ON FUNCTION public.adj_acc_balance(text, text)
    IS 'Used to get account balance on a particular date for all accounts with adjusted pilot customers balances when the start date is before 2014-01-01

Examples:
Select * from adj_acc_balance(''2016-12-01 00:00:00'', ''2016-12-11 00:00:00'');

Select * from adj_acc_balance(''2017-01-01'', ''2017-01-07'');

Params
d1        ignore transactions before this date
d2        Date to get balance on, if in the future, then current balance';
