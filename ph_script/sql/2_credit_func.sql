-- FUNCTION: public.mobile_payment_type(text)

DROP FUNCTION IF EXISTS public.mobile_payment_type(text);

CREATE OR REPLACE FUNCTION public.mobile_payment_type(
	provider text)
    RETURNS varchar
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
AS $function$

BEGIN

RETURN (CASE
        WHEN provider = 'AIRTEL' THEN '1045 Airtel Mobile Holding Acct'
        WHEN provider = 'MPESA' THEN '1046 MPESA Mobile Money'
        ELSE 'UNKNOWN'
    END);

END

$function$;

COMMENT ON FUNCTION public.mobile_payment_type(text)
    IS 'Will convert current ''provider'' of mobile payments to appropriate NetSuite bank account string.
        Should only be called from the public.credit function with the provider. ';

-- FUNCTION: public.credit(text, text)

DROP FUNCTION IF EXISTS public.credit(text, text);

CREATE OR REPLACE FUNCTION public.credit(
	d1 text,
	d2 text)
    RETURNS TABLE(
    version varchar,
    trans_id integer,
    process_dt varchar,
    acc_id integer,
    memo varchar,
    trans_code integer,
    amt_shillings numeric,
    provider varchar,
    reason varchar,
    muni_name varchar)
    LANGUAGE 'sql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
    ROWS 1000.0
AS $function$

SELECT
	'1.0'::varchar AS version,
	credit_history."id"  AS trans_id,
	TO_CHAR(credit_history."sourceProcessedTime" , 'YYYY-MM-DD') AS process_dt,
	account.id  AS acc_id,
	credit_history.source  AS memo,
    CASE
  		WHEN credit_history.source = 'MobilePayment' THEN 2
        WHEN credit_history.source = 'LoanPaymentHistory' THEN 3
        WHEN credit_history.source = 'CreditAdjustmentHistory' THEN 284
        WHEN credit_history.source like 'CreditAdjustmentHistory%' THEN (SELECT * FROM adjust_type(credit_history.source))
        WHEN credit_history.source = 'Com1DailyFee' THEN 231
        WHEN credit_history.source = 'Com2DailyFee' THEN 232
        WHEN credit_history.source = 'ResDailyFee' THEN 236
        WHEN credit_history.source = 'Com1MonthlyFee' THEN 231
        WHEN credit_history.source = 'Com2MonthlyFee' THEN 232
        WHEN credit_history.source = 'ResMonthlyFee' THEN 236
        WHEN credit_history.source = 'ConnFee' THEN 235
        WHEN credit_history.source = 'LoanEquipFee' THEN 278
        WHEN credit_history.source = 'LoanConnFee' THEN 279
        WHEN credit_history.source = 'LoanMiscFee' THEN 280
        ELSE 0
    END AS trans_code,
	credit_history.amount  AS amt_shillings,
	(Select * from mobile_payment_type(networkserviceprovider."serviceProvider")) AS provider,
	creditadjustmenthistory.reason  AS reason,
	municipality.name as muni_name
FROM ph_model_mastertransactionhistory  AS credit_history
LEFT JOIN ph_model_account  AS account ON (credit_history."account_id") = (account."id")
LEFT JOIN ph_model_creditadjustmenthistory  AS creditadjustmenthistory ON
    credit_history."sourceId" = (creditadjustmenthistory."id") AND (credit_history."source") like 'CreditAdjustmentHistory%'
LEFT JOIN ph_model_mobilepayment  AS mobilepayment ON (credit_history."sourceId") = (mobilepayment."id") AND
    credit_history.source = 'MobilePayment'
LEFT JOIN ph_model_networkserviceprovider  AS networkserviceprovider ON
    mobilepayment."networkServiceProvider_id" = (networkserviceprovider."id")
LEFT JOIN ph_model_customer  AS customer ON account.id = customer.account_id
LEFT JOIN ph_model_municipality  AS municipality ON customer.municipality_id = municipality.id

WHERE credit_history."sourceProcessedTime"  >= $1::timestamp without time zone AND
		credit_history."sourceProcessedTime" <= $2::timestamp without time zone AND
        credit_history."transactionType" = 'Credit'
ORDER BY 3 DESC;

$function$;

COMMENT ON FUNCTION public.credit(text, text)
    IS 'Used to get credit transactions for all queens

Examples:
Select * from credit(''2016-12-01 00:00:00'',''2016-12-02 00:00:00'');

Select * from credit(''2016-12-31'',''2017-01-01'');

Params
d1        Start date
d2        End date';
