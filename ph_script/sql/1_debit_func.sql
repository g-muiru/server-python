  -- FUNCTION: public.adjust_type(text)

DROP FUNCTION IF EXISTS public.adjust_type(text);

CREATE OR REPLACE FUNCTION public.adjust_type(adj_type text)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
AS $function$

BEGIN
-- these values are also in the O&M software adjustBalance.content.html file -- they must match exactly
RETURN (CASE
        WHEN adj_type = 'CreditAdjustmentHistory-REFUND' THEN 338
        WHEN adj_type = 'CreditAdjustmentHistory-Bad Debt' THEN 319
        WHEN adj_type = 'CreditAdjustmentHistory-Clearing' THEN 320
        WHEN adj_type = 'CreditAdjustmentHistory-Accounting' THEN 321
        WHEN adj_type = 'CreditAdjustmentHistory-Marketing' THEN 284
        WHEN adj_type = 'CreditAdjustmentHistory-BONUS_PWR' THEN 284
        WHEN adj_type = 'CreditAdjustmentHistory-VAT' THEN 331
        ELSE 284
    END);

END
$function$;
COMMENT ON FUNCTION public.adjust_type(text)
    IS '2017-07-31';

-- FUNCTION: public.usage_type(text)

DROP FUNCTION IF EXISTS public.usage_type(text);

CREATE OR REPLACE FUNCTION public.usage_type(cust_type text)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
AS $function$

BEGIN

RETURN (CASE
        WHEN cust_type = 'COMMERCIAL' THEN 234
        WHEN cust_type = 'COMMERCIAL_II' THEN 233
        WHEN cust_type = 'RESIDENTIAL' THEN 238
        ELSE 0
    END);

END
$function$;
COMMENT ON FUNCTION public.usage_type(text)
    IS '2017-07-31';

-- FUNCTION: public.trans_code(text)

DROP FUNCTION IF EXISTS public.trans_code(text);

CREATE OR REPLACE FUNCTION public.trans_code(trans_str text)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
AS $function$

BEGIN

RETURN (CASE
        WHEN trans_str = 'COMMERCIAL' THEN 234
        WHEN trans_str = 'COMMERCIAL_II' THEN 233
        WHEN trans_str = 'RESIDENTIAL' THEN 238
        WHEN trans_str = 'COM1_FIXED_FEEc231' THEN 231
        WHEN trans_str = 'COM2_FIXED_FEEc232' THEN 232
        WHEN trans_str = 'RES_FIXED_FEEc236' THEN 236
        WHEN trans_str = 'COM1_VAR_FEEc233' THEN 233
        WHEN trans_str = 'COM2_VAR_FEEc234' THEN 234
        WHEN trans_str = 'RES_VAR_FEEc238' THEN 238
        WHEN trans_str = 'VAT_TARIFF_FIXEDc299' THEN 346
        WHEN trans_str = 'VAT_TARIFF_VARIABLEc300' THEN 345
        WHEN trans_str = 'VAT_TARIFF_FIXEDc346' THEN 346
        WHEN trans_str = 'VAT_TARIFF_VARIABLEc345' THEN 345
        WHEN trans_str = 'VAT_LOAN_INITc301' THEN 301
        WHEN trans_str = 'CRED_ADJ_FEEc248' THEN 248
        WHEN trans_str = 'LOAN_CONN_FEE_INITc235' THEN 235
        WHEN trans_str = 'LOAN_APPLIANCE_INITc278' THEN 278
        WHEN trans_str = 'LOAN_FINANCIAL_INITc279' THEN 279
        WHEN trans_str = 'LOAN_KUKU_POA_INITc369' THEN 369
        WHEN trans_str = 'LOAN_INTERNET_INITc370' THEN 370
        WHEN trans_str = 'LOAN_OTHER_INITc280' THEN 280
        ELSE 0
    END);
END
$function$;
COMMENT ON FUNCTION public.trans_code(text)
    IS '2017-07-31';

-- FUNCTION: public.debit(text, text)

DROP FUNCTION IF EXISTS public.debit(text, text);

CREATE OR REPLACE FUNCTION public.debit(
	d1 text,
	d2 text)
    RETURNS TABLE(
        version character varying,
        trans_id integer,
        process_dt character varying,
        acc_id integer,
        cust_type character varying,
        trans_str character varying,
        trans_code integer,
        amt_shillings numeric,
        memo character varying,
        loan_id integer,
        loan_type character varying,
        loan_payment numeric,
        loan_balance numeric,
        payment_cnt integer,
        payments_required integer,
        loan_amt numeric,
        past_due boolean,
        reason character varying,
        muni_name character varying)
    LANGUAGE 'sql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
    ROWS 1000.0
AS $function$

SELECT
	'1.0'::varchar AS version,
	debit_history.id  AS trans_id,
	TO_CHAR(debit_history."sourceProcessedTime" , 'YYYY-MM-DD') AS process_dt,
	debit_history.account_id  AS acc_id,
	customer."customerType" AS cust_type,
	CASE WHEN debit_history.source = 'VAT_TARIFF_FIXEDc299' then 'VAT_TARIFF_FIXEDc346'
	     WHEN debit_history.source = 'VAT_TARIFF_VARIABLEc300' then 'VAT_TARIFF_VARIABLEc345'
	     ELSE debit_history.source END AS trans_str,
    CASE
        WHEN debit_history.source = 'UsageCharge' THEN (SELECT * FROM usage_type(customer."customerType"))
        WHEN debit_history.source like 'CreditAdjustmentHistory%' THEN (SELECT * FROM adjust_type(debit_history.source))
        ELSE (SELECT * FROM trans_code(debit_history.source))
    END AS trans_code,
	debit_history.amount  AS amt_shillings,
	cast(usage."intervalWh" as text)  AS memo,
	-- CASE WHEN (usage."intervalWh") > 0 AND (usagecharge.amount) = 0  THEN 'Yes' ELSE 'No' END AS unbilled_usage,
	loanpaymenthistory.loan_id  AS loan_id,
	loan."loanType"  AS loan_type,
	loanpaymenthistory."principlePayment"  AS loan_payment,
    loan."outStandingPrinciple"  AS loan_balance,
	loan."lastPaymentNumber"  AS payment_cnt,
    loan."totalNumberOfPayments"  AS payments_required,
    loan."loanAmount" as loan_amt,
    loan."passedDue" as past_due,
	creditadjustmenthistory.reason  AS reason,
    municipality.name  AS muni_name

FROM ph_model_mastertransactionhistory  AS debit_history
LEFT JOIN ph_model_usagecharge  AS usagecharge ON debit_history."sourceId" = usagecharge.id AND
debit_history.source in ('UsageCharge','COM1_FIXED_FEEc231','COM2_FIXED_FEEc232','RES_FIXED_FEEc236','COM1_VAR_FEEc233','COM2_VAR_FEEc234','RES_VAR_FEEc238')
LEFT JOIN ph_model_usage  AS usage ON usagecharge.usage_id = usage.id
LEFT JOIN ph_model_loanpaymenthistory  AS loanpaymenthistory ON debit_history."sourceId" = loanpaymenthistory.id AND
debit_history.source in ('LOAN_CONN_FEE_INITc235','LOAN_APPLIANCE_INITc278','LOAN_FINANCIAL_INITc279','LOAN_OTHER_INITc280',
                         'VAT_TARIFF_FIXEDc346','VAT_TARIFF_VARIABLEc345','LOAN_KUKU_POA_INITc369','LOAN_INTERNET_INITc370')
LEFT JOIN ph_model_loan  AS loan ON (loanpaymenthistory."loan_id") = (loan."id")
LEFT JOIN ph_model_creditadjustmenthistory  AS creditadjustmenthistory ON debit_history."sourceId" = creditadjustmenthistory.id AND
debit_history.source in ('CreditAdjustmentHistory','CRED_ADJ_FEEc248')
LEFT JOIN ph_model_customer  AS customer ON debit_history.account_id = customer.account_id
LEFT JOIN ph_model_municipality  AS municipality ON customer.municipality_id = municipality.id

WHERE debit_history."sourceProcessedTime"  >= $1::timestamp without time zone AND
	debit_history."sourceProcessedTime" <= $2::timestamp without time zone AND
    (NOT COALESCE((usagecharge.amount) = 0 and (usage."intervalWh") = 0 , FALSE)) AND
    debit_history."transactionType" = 'Debit' AND
    debit_history.source not in ('LoanPaymentHistory','LOAN_PAYMENTc600','VAT_LOAN_PAYMENTc601','INTEREST_LOAN_PAYMENTc602','VAT_INTEREST_LOAN_PAYMENTc603') AND
    debit_history.amount != 0 AND
    municipality.name not in ('Berkeley', 'Nyamondo', 'Mokomoni', 'BaraNne','Matangamano','Ogembo','Nairobi')
ORDER BY 3 DESC;
$function$;

COMMENT ON FUNCTION public.debit(text, text)
    IS '2017-07-31 Used to get debit transactions for all customers -- NOTE: loan payments are excluded

Examples:
Select * from debit(''2016-12-01 00:00:00'',''2016-12-02 00:00:00'');

Select * from debit(''2016-12-31'',''2017-01-01'');

Params
d1        Start date
d2        End date';

-- FUNCTION: public.debit(text, text)

DROP FUNCTION IF EXISTS public.aggr_debit(text, text);

CREATE OR REPLACE FUNCTION public.aggr_debit(
	d1 text,
	d2 text)
    RETURNS TABLE(
        version character varying,
        acc_id integer,
        muni_name character varying,
        process_dt character varying,
        trans_id integer,
        trans_code integer,
        amt_shillings numeric,
        cnt_invoices bigint,
        memo character varying)
    LANGUAGE 'sql'
    COST 100.0
    VOLATILE NOT LEAKPROOF
    ROWS 1000.0
AS $function$

SELECT
	'1.0'::varchar AS version,
	acc_id,
	muni_name,
	min(process_dt) as process_dt,
	min(trans_id) as trans_id,
	trans_code,
	sum(amt_shillings) as amt_shillings,
	count(amt_shillings) as cnt_invoices,
	trans_str as memo
FROM debit($1, $2)
GROUP BY muni_name, acc_id, trans_code, trans_str
ORDER BY acc_id, trans_code, muni_name;
$function$;

COMMENT ON FUNCTION public.aggr_debit(text, text)
    IS '2017 07-31 Used to get debit transactions for all customers and aggrigate by


Examples:
Select * from aggr_debit(''2016-12-01 00:00:00'',''2016-12-02 00:00:00'');

Select * from aggr_debit(''2016-12-31'',''2017-01-01'');

Params
d1        Start date
d2        End date';
