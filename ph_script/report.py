# coding=utf-8
"""
First Solar Report
"""
import datetime
import ph_model.models as ph_model
import itertools
import decimal
from ph_util import email_util

ACTIVE_GRIDS = ['BaraNne']


def circuit_sort(d):
    """

    :param d:
    :return:
    """
    return d['circuitId']


def date_sorted(d):
    """

    :param d:
    :return:
    """
    return d['date']


# noinspection PyUnusedLocal,PyUnusedLocal
def convert_currency(amount, current_currency='KSH', target_currency='USD'):
    """Converts currency from @param currentCurrency to @param targetCurrency"""
    # TODO(estifanos) confirmed by Steve to use hardcoded value until prioritized ."""
    v = decimal.Decimal(0.0098) * amount
    return v.quantize(decimal.Decimal('.01'))


# noinspection PyShadowingNames,PyShadowingNames
def get_usage_charge_per_day(date, look_back_days):
    """
    Gets usage report.

    Attributes:
        date : datetime.date
        look_back_days: num of days to look back.
    """
    usage_fields = ('created', 'intervalWh', 'customer_id', 'circuit_id', 'circuit__deviceId', 'charge__amount',
                    'circuit__number', 'circuit__queen__number')
    usage_groups = []
    if not date:
        # noinspection PyArgumentList,PyArgumentList,PyArgumentList
        date = datetime.date().today()
    last_n_days = date - datetime.timedelta(look_back_days)
    usages = ph_model.usage.Usage.objects.values(*usage_fields).filter(
        created__gte=last_n_days,
        created__lt=date,
        circuit__queen__grid__name__in=ACTIVE_GRIDS).order_by('circuit', 'created')

    grouped_usages_by_customers = itertools.groupby(usages, lambda usage: usage['circuit_id'])
    for k, groupedUsageByCustomer in grouped_usages_by_customers:
        grouped_usages_customer_daily = itertools.groupby(
            list(groupedUsageByCustomer), lambda usage: usage['created'].strftime("%Y-%m-%d"))
        # noinspection PyAssignmentToLoopOrWithParameter
        for k, groupedUsageByCustomerDaily in grouped_usages_customer_daily:
            usage_groups.append(list(groupedUsageByCustomerDaily))

    usage_and_asp = []
    for usageGroup in usage_groups:
        usage = decimal.Decimal(sum([d['intervalWh'] for d in usageGroup if d['intervalWh'] is not None])).quantize(
            decimal.Decimal('.01'))
        charge = decimal.Decimal(sum([d['charge__amount'] for d in usageGroup if d['charge__amount'] is not None]))
        asp = usage and decimal.Decimal(charge / usage)
        data = {'circuitDeviceId': usageGroup[0]['circuit__deviceId'],
                'usage': str(usage),
                'charge': str(convert_currency(charge)), 'circuitId': usageGroup[0]['circuit_id'],
                'circuitName': 'Q{}C{}'.format(usageGroup[0]['circuit__queen__number'],
                                               usageGroup[0]['circuit__number']),
                'asp': str(convert_currency(asp)), 'date': str(usageGroup[0]['created'].date())}
        usage_and_asp.append(data)
    return sorted(usage_and_asp, key=circuit_sort)


# noinspection PyShadowingNames
def get_deposit_per_day(date, look_back_days):
    """
    Gets deposit report.

    Attributes:
        date : datetime.date
        look_back_days: num of days to look back.
    """
    credit_fields = (
        'created', 'account__accountOwner__circuit_id', 'account__accountOwner', 'account__accountOwner__circuit',
        'transactionType',
        'account__accountOwner__circuit__deviceId', 'amount', 'account__accountOwner__circuit__number',
        'account__accountOwner__circuit__queen__number')
    credit_groups = []
    last_n_days = date - datetime.timedelta(look_back_days)
    transactions = ph_model.transaction.MasterTransactionHistory.objects.values(*credit_fields).filter(
        created__gte=last_n_days, created__lt=date,
        account__accountOwner__circuit__queen__grid__name__in=ACTIVE_GRIDS,
        transactionType__iexact=ph_model.transaction.TransactionType.CREDIT.value).order_by(
        'account__accountOwner__circuit', 'created')

    grouped_credit_by_customers = itertools.groupby(
        transactions, lambda credit: credit['account__accountOwner__circuit'])
    for k, groupedCreditByCustomer in grouped_credit_by_customers:
        grouped_usages_customer_daily = itertools.groupby(
            list(groupedCreditByCustomer), lambda transaction: transaction['created'].strftime("%Y-%m-%d"))
        # noinspection PyAssignmentToLoopOrWithParameter
        for k, groupedCreditByCustomerDaily in grouped_usages_customer_daily:
            credit_groups.append(list(groupedCreditByCustomerDaily))

    credits_local = []
    for creditGroup in credit_groups:
        credit = decimal.Decimal(sum([d['amount'] for d in creditGroup if d['amount'] is not None]))

        data = {'circuitDeviceId': creditGroup[0]['account__accountOwner__circuit__deviceId'],
                'circuitId': creditGroup[0]['account__accountOwner__circuit_id'],
                'circuitName': 'Q{}C{}'.format(creditGroup[0]['account__accountOwner__circuit__queen__number'],
                                               creditGroup[0]['account__accountOwner__circuit__number']),
                'deposit': str(convert_currency(credit)), 'date': str(creditGroup[0]['created'].date())}
        credits_local.append(data)
    return sorted(credits_local, key=circuit_sort)


# noinspection PyArgumentList,PyAssignmentToLoopOrWithParameter
def get_daily_tariff_fixed_charge_per_day(date, look_back_days):
    """
    Gets Daily fixed charge.

    Attributes:
        date : datetime.date
        look_back_days: num of days to look back.
    """
    tariffDailyFixedCharge = ('created', 'usage__circuit_id', 'usage__circuit_id', 'perSegmentChargeComponent',
                              'usage__circuit__deviceId', 'usage__circuit__number', 'usage__circuit__queen__number',
                              'amount')
    fixedCreditGroups = []
    if not date:
        # noinspection PyArgumentList,PyArgumentList
        date = datetime.date().today()
    last_n_days = date - datetime.timedelta(look_back_days)
    transactions = ph_model.transaction.UsageCharge.objects.values(*tariffDailyFixedCharge).filter(
        created__gte=last_n_days, created__lt=date,
        usage__circuit__queen__grid__name__in=ACTIVE_GRIDS).order_by('usage__circuit', 'created')

    groupedCreditByCustomers = itertools.groupby(transactions, lambda credit: credit['usage__circuit_id'])
    for k, groupedCreditByCustomer in groupedCreditByCustomers:
        groupedUsagesCustomerDaily = itertools.groupby(
            list(groupedCreditByCustomer),
            lambda transaction: transaction['created'].strftime("%Y-%m-%d"))
        for k, groupedCreditByCustomerDaily in groupedUsagesCustomerDaily:
            fixedCreditGroups.append(list(groupedCreditByCustomerDaily))

    daily_fixed_charges = []
    for dailyFixedCharge in fixedCreditGroups:
        fixedCharge = decimal.Decimal(
            sum([d['perSegmentChargeComponent'] for d in dailyFixedCharge if
                 d['perSegmentChargeComponent'] is not None]))

        data = {'circuitDeviceId': dailyFixedCharge[0]['usage__circuit__deviceId'],
                'circuitId': dailyFixedCharge[0]['usage__circuit_id'],
                'circuitName': 'Q{}C{}'.format(dailyFixedCharge[0]['usage__circuit__queen__number'],
                                               dailyFixedCharge[0]['usage__circuit__number']),
                'fixedCharge': str(convert_currency(fixedCharge)),
                'date': str(dailyFixedCharge[0]['created'].date())}
        daily_fixed_charges.append(data)
    return sorted(daily_fixed_charges, key=circuit_sort)


FIRST_SOLAR_REPORT_BASE_DIR = '/powerhive/first_solar_report'


# noinspection PyShadowingNames
def get_circuit_report_csv_format(sorted_data_by_circuit_id, key):
    #TODO: fix this test
    pass
    """

    :param sorted_data_by_circuit_id:
    :param key:
    :return:
    """
    idNameDict = {}
    for d in sorted_data_by_circuit_id:
        idNameDict[d['circuitId']] = d['circuitName']
    value = 'Date,' + ','.join([str(k) for k in idNameDict.values()]) + '\n'
    sortedData = sorted(sorted_data_by_circuit_id, key=date_sorted)
    dateGrouped = itertools.groupby(sortedData, lambda d: d['date'])
    for dateGroupedValueKey, dateGroupedValue in dateGrouped:
        circuitData = sorted(list(dateGroupedValue), key=circuit_sort)
        value += circuitData[0]['date']
        for circuitDatum in circuitData:
            value += ',' + circuitDatum[key]
        value += '\n'
    return value


def generate_report(date=None, look_back_days=14, emails=None):
    """Exports generated scratchcard pins. if @param emails send email as attachment.

    Arguments:
        query: list of dict query data.
        emails: list email to get notification with pin attachments.
    """
    date = date or datetime.date.today()
    usageAndCharge = get_usage_charge_per_day(date, look_back_days)
    data = [{'header': 'Usage per day (Wh)', 'data': get_circuit_report_csv_format(usageAndCharge, 'usage')},
            {'header': 'ASP ($kWh)', 'data': get_circuit_report_csv_format(usageAndCharge, 'asp')},
            {'header': 'Deposits',
             'data': get_circuit_report_csv_format(get_deposit_per_day(date, look_back_days), 'deposit')},
            {'header': 'Daily Fixed Charge ($)',
             'data': get_circuit_report_csv_format(get_daily_tariff_fixed_charge_per_day(date, look_back_days),
                                                   'fixedCharge')}]

    file_name = '{}/{}_exported_pin.csv'.format(FIRST_SOLAR_REPORT_BASE_DIR,
                                                datetime.datetime.now().strftime("%Y_%m_%d_%H_%m_%S"))
    with open(file_name, 'wb') as f:
        for d in data:
            f.write(d['header'])
            f.write('\n')
            f.write(d['data'])
            f.write('\n\n\n\n\n')
    email_util.send_template_email('First solar Report!', 'first_solar_report.html', {},
                                   receivers=emails, attachments=[file_name])
