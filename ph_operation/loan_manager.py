# coding=utf-8
"""Loan manager module."""
import datetime
import logging
from decimal import Decimal

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db.models import F
from django.db.models.query import Q

import event_manager
import ph_model.models as ph_model
from ph_model.models import Customer
from ph_util import date_util
from ph_util.accounting_util import deci_or_float, add_vat, AccountingEnums, LoanRepayment, calc_vat, LoanType, DEAD_LOAN_ACCOUNT, MUNIS_NO_VAT
from ph_util.date_util import get_utc_now

LOGGER = logging.getLogger(__name__)

DEFAULT_PAYMENT_FREQUENCY_IN_DAYS = 1


# noinspection PyUnusedLocal
def calculate_payment(rate, per, nper, principal, balance, vat=True):
    # TODO validate this with interest on the loan
    """

    :param rate:        daily int rate      loan.annualInterestRate * loan.paymentFrequency / 365
    :param per:         not used            loan.lastPaymentNumber
    :param nper:        number of payments  loan.totalNumberOfPayments
    :param principal:   loan amount         loan.loanAmount
    :param balance:     loan priniciple     loan.outStandingPrinciple
    :param vat          True means add vat
    :return:            map principle payment, interest payment, and total payment
    """

    if rate != 0:
        pmt = rate / (1 - (1 + rate) ** (-nper)) * principal
    else:
        pmt = principal / nper
    normal_pmt = pmt
    ipmt = rate * balance
    if vat:
        ppmt = add_vat(pmt - ipmt)
        if balance < ppmt:
            ppmt = balance
            pmt = ppmt / (1 + settings.VAT_MULTIPLIER)
    else:
        ppmt = pmt - ipmt
        if balance < ppmt:
            ppmt = balance
            pmt = ppmt
    if per == nper:
        left_over = balance - ppmt
        if 0.0 < abs(left_over) <= normal_pmt:
            ppmt += left_over
            pmt += left_over
    return map(deci_or_float, (ppmt, ipmt, pmt))


PRINCIPLE_PAYMENT_INDEX = 0
INTEREST_PAYMENT_INDEX = 1
PAYMENT_INDEX = 2


class Loan(object):
    """Class to manage loan, to replay update account information. ."""

    def __init__(self, loan):
        self.loan = loan
        self.account = loan.account
        rate = Decimal(self.loan.annualInterestRate * self.loan.paymentFrequency / 365)
        per = Decimal(self.loan.lastPaymentNumber + 1)
        nper = Decimal(self.loan.totalNumberOfPayments)
        pv = Decimal(self.loan.loanAmount)
        balance = Decimal(self.loan.outStandingPrinciple)
        payment = calculate_payment(rate, per, nper, pv, balance,
                                    loan.loanType != LoanType.KUKU_POA_NO_VAT_LOAN.name)
        self.principalPayment = abs(payment[PRINCIPLE_PAYMENT_INDEX])
        self.interestPayment = abs(payment[INTEREST_PAYMENT_INDEX])
        self.payment = abs(payment[PAYMENT_INDEX])

    def repay_loan(self, cust, date_time=get_utc_now()):
        """
        Update account with calculated loan payment.

        :param cust:        the customer
        :param date_time:   date time for processing
        :return:            the updated loan for testing purposes
        """

        # Loan payments will NOT send the customer's account balance negative, and power will NOT be shut off
        if self.loan.loanType == LoanType.KUKU_POA_NO_VAT_LOAN.name:
            vat = Decimal(0.0)
        else:
            vat = calc_vat(self.payment)
        self.loan.outStandingPrinciple -= self.principalPayment
        self.loan.lastPaymentNumber += 1
        self.loan.lastPaymentReceivedDate = date_time

        amt = self.payment
        if cust.municipality.name not in MUNIS_NO_VAT:
            amt += vat
        amt = Decimal(round(amt,2))
        if self.account.accountBalance < amt:
            event_manager.raise_event(
                self.account.accountOwner.id,
                ph_model.event.EventTypeConst.CUSTOMER_ACCOUNT_LOAN_PAYMENT_INSUFFICIENT_BALANCE.value, [
                    {'available balance': self.account.accountBalance},
                    {'Loan payment amount': add_vat(self.payment)}])
            self.loan.passedDue = True
        self.loan.save()
        notcollected = self.account.accountBalance < self.payment
        if self.account.accountBalance >= self.payment:
            self.account.accountBalance -= self.payment
        else:
            self.account.uncollected_bal -= self.payment

        self.account.save()
        self.update_payment_history(cust, vat, date_time, notcollected)
        return self.loan, self.account.accountBalance

    def update_payment_history(self, cust, vat, date_time, notcollected):
        """Update payment history."""
        payment_history = ph_model.loan.LoanPaymentHistory(
            loan=self.loan,
            principlePayment=self.principalPayment,
            interestPayment=self.interestPayment,
            balanceAfterPayment=self.account.accountBalance,
            processedTime=date_time,
            amount=self.payment,
            notcollected=notcollected)
        payment_history.save_accounting(AccountingEnums.LOAN_PAYMENTc600)

        if vat > 0.0:
            vat = Decimal(round(deci_or_float(vat),2))
            notcollected = self.account.accountBalance < vat
            if self.account.accountBalance >= vat:
                self.account.accountBalance -= vat
            else:
                self.account.uncollected_bal -= vat
            self.account.save()
            payment_history = ph_model.loan.LoanPaymentHistory(
                loan=self.loan,
                principlePayment=0.0,
                interestPayment=0.0,
                balanceAfterPayment=self.account.accountBalance,
                processedTime=date_time,
                amount=vat,
                notcollected=notcollected)
            payment_history.save_accounting(AccountingEnums.VAT_LOAN_PAYMENTc601)


class LoanManager(object):
    """Loan manager to handle loans."""

    # TODO(estifanos) Use db query to grab loans, currently F() with delta with own filed seems not to be supported look into DateTimeNode Helper.
    @classmethod
    def repay_due_loans(cls, date_time=get_utc_now(), test=False):
        """
        :param date_time:
        :param test:
        :return:

        Only make payments on loans when:
        the total number of payments has not been made and
        the outstanding principle is greater than 1 and
        the loan start date is less than or equal to today and
        lastPaymentReceivedDate is less than the date_time date and
        it's past a payment date

        :return:
        """
        num = 0
        municipalities = ph_model.municipality.Municipality.objects.all()
        for municipality in municipalities:
            dt, municipality_time_is_mid_night = date_util.mid_night_just_passed_in_range(
                time_zone=municipality.timeZone,
                range_in_minutes=5,
                date_time=date_time)
            # do all munis
            due_loans = ph_model.loan.Loan.objects.filter(Q(account__accountOwner__municipality=municipality))
            due_loans = due_loans.exclude(lastPaymentNumber=F('totalNumberOfPayments'))
            due_loans = due_loans.exclude(outStandingPrinciple__lte=1.0)
            due_loans = due_loans.exclude(startDate__gte=dt)
            due_loans = due_loans.exclude(account_id=DEAD_LOAN_ACCOUNT)
            due_loans = due_loans.exclude(lastPaymentReceivedDate__gte=date_time.replace(hour=0, minute=0, second=0, microsecond=0))
            today = date_time
            last_date_of_month = datetime.datetime(today.year, today.month, 1) + relativedelta(months=1, days=-1)
            first_of_this_month = datetime.datetime(today.year, today.month, 1)
            for due_loan in due_loans:
                due_dt = None
                cnt = due_loan.lastPaymentNumber
                # TODO: move this into a helper function
                if due_loan.fixedRepayment is True:
                    if due_loan.repaymentSchedule == LoanRepayment.DAILY.name:
                        due_dt = date_time
                    if due_dt is None and due_loan.repaymentSchedule == LoanRepayment.WEEKLY.name:
                        if today.weekday() == due_loan.startDate.weekday():
                            due_dt = date_time
                    if due_dt is None and (due_loan.repaymentSchedule == LoanRepayment.FIRST_OF_THE_MONTH.name or
                                           due_loan.repaymentSchedule == LoanRepayment.FIRST_AND_FIFTEENTH.name):
                        if today.day == first_of_this_month.day:
                            due_dt = date_time
                    if due_dt is None and (due_loan.repaymentSchedule == LoanRepayment.FIFTEENTH_AND_LAST.name or
                                           due_loan.repaymentSchedule == LoanRepayment.FIRST_AND_FIFTEENTH.name):
                        if today.day == 15:
                            due_dt = date_time
                    if due_dt is None and (due_loan.repaymentSchedule == LoanRepayment.LAST_OF_THE_MONTH.name or
                                           due_loan.repaymentSchedule == LoanRepayment.FIFTEENTH_AND_LAST.name):
                        if today.day == last_date_of_month.day:
                            due_dt = date_time
                else:
                    due_dt = due_loan.startDate + (cnt * datetime.timedelta(days=due_loan.paymentFrequency))
                if due_dt and date_time >= due_dt:
                    loan_handler = Loan(due_loan)
                    if test:
                        num += 1
                        print(due_loan.account_id, due_loan.lastPaymentReceivedDate, num, date_time, due_dt,)
                    else:
                        if due_loan.account.id != DEAD_LOAN_ACCOUNT:
                            loan_handler.repay_loan(Customer.objects.get_or_none(account_id=due_loan.account_id), date_time)
