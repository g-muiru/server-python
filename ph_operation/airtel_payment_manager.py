# coding=utf-8
"""Airtel Payment manager"""
import abc
import collections
import datetime
import enum
import logging
import pytz
import delorean

from suds.client import Client

import ph_model.models as ph_model
from ph_util import mixins
import transaction_manager
import event_manager
from ph_util.accounting_util import PaymentEnums

LOGGER = logging.getLogger(__name__)


class PaymentServiceFailureException(Exception):
    """TODO: log something"""
    pass


class UnRegisteredPhoneAccountException(Exception):
    """Unregistered phone account making payment, can't link any account to it."""
    pass


AIRTEL = ph_model.mobile_payment_provider.MobilePaymentServiceProvider.AIRTEL.value

AIRTEL_DATE_FORMAT = '%Y%m%d%H%M%S'  # YYYYMMDDHHMMSS
SUCCESS_RESPONSE = 'OK'
PAYMENT_SERVICE_STATUS = {0: SUCCESS_RESPONSE, 1: 'NO_PAYMENTS', 2: 'FAILURE', 5: 'INVALID_DATE_RANGE'}

# For first time sync backtrack this amount of hours, about one week
FIRST_TIME_LOOK_UP_INTERVAL_HOURS = 7 * 24
LOOK_UP_TIME_WINDOW_HOURS = 24

TRANSACTIONS_SEPARATOR = '],['
TRANSACTION_DETAIL_SEPRATOR = '#'  # u'\u00A3'


# TODO(estifanos): Refactor code to read user/pass from db. ADD Retries, add memoize, confirm time interval in in UTC.
class AirtelPaymentWebservice(object):
    """Abstract airtel payment base class"""

    # noinspection PyPep8Naming,PyPep8Naming
    def __init__(self, user, password, timeFrom, timeTo, port='MerchantQueryWebServiceSoapPort'):
        self.port = port
        self.timeFrom = timeFrom
        self.timeTo = timeTo
        self.user = user
        self.password = password
        self.detailTransactionResponse = []

    def get_client(self, url):
        """Returns webservice client for @param wsdl url"""
        client = Client(url)
        client.set_options(port=self.port)
        return client

    @abc.abstractmethod
    def fetch_payments(self):
        """Fetch payment transaction from Airtel webservice.
        Returns:
        @WSDL.RequestTransactionsResponse from self.url.{
        Status = 0
        TotalTransactions = 26
        TotalAmount = 2535.0
        Transactions =  '[254021621244925473376958990732851956],...'
        Message = "Successfully fetched 26 Transactions Valued at 2535 "
        }
        Raises:
        Custom exception {
        Status = 1 or 2 ..
        TotalTransactions = 0
        TotalAmount = 0.0
        Message = "Error in the request"
        }
        """
        return


class AirtelPaymentWebserviceV1(AirtelPaymentWebservice):
    """Version 1 airtel payment webservice."""
    url = 'https://41.223.56.58:44433/MerchantQueryService.asmx?WSDL'

    def fetch_payments(self):
        """

        :return:
        """
        # noinspection PyAttributeOutsideInit
        self.client = self.get_client(self.url)
        response = self.client.service.RequestTransactionByTimeIntervalDetailed(
            self.user, self.password,
            self.timeFrom, self.timeTo)
        return response


class AirtelPaymentWebserviceV2(AirtelPaymentWebservice):
    """Version 2 airtel payment webservice."""
    # url = 'https://41.223.56.58:8924/MerchantQueryService.asmx?WSDL'
    # url = 'https://41.223.58.41:8446/MerchantQueryService.asmx?WSDL'
    # url = 'https://airtelmoneymq.ke.airtel.com:8446/MerchantQueryService.asmx?WSDL'
    url = 'https://airtelmoneymqtest.ke.airtel.com:8443/MerchantQueryService.asmx?WSDL'

    def fetch_payments(self):
        """

        :return:
        """
        # noinspection PyAttributeOutsideInit
        self.client = self.get_client(self.url)
        response = self.client.service.RequestTransactionByTimeIntervalDetailed(
            self.user, self.password,
            self.timeFrom, self.timeTo)
        return response


class PaymentServiceVersion(mixins.PairEnumMixin, enum.Enum):
    """Payment service versions"""
    V1 = AirtelPaymentWebserviceV1
    V2 = AirtelPaymentWebserviceV2


class AirtelPaymentManager(object):
    """Airtel payment service manager."""

    class AirtelDetailResponseObject(object):
        """Parse Airtel responses"""
        # noinspection PyCompatibility
        TRANSACTION_DETAIL = collections.namedtuple(
            'DT',
            'TRANSACTION_ID PHONE_NUMBER AMOUNT FEE MSISDN COLLECT_TIME FNAME LNAME')._make(xrange(8))

        def __init__(self, transaction):
            """E.G data 11501017139505  254786313920 50 2.50 0738529019 20150113211036 GLADYS MBECHE"""
            self.payerPhoneNumber = "+{}".format(transaction[self.TRANSACTION_DETAIL.PHONE_NUMBER])
            msisdn = transaction[self.TRANSACTION_DETAIL.MSISDN]

            if msisdn and len(msisdn) in (9, 10):
                if msisdn[0] == '0':
                    msisdn = msisdn[1:]
                # noinspection PyBroadException
                try:
                    self.msisdn = "+254{}".format(str(int(msisdn)))
                except Exception:
                    self.msisdn = None
            elif len(msisdn) in (12, 13):
                if len(msisdn) == 12:
                    msisdn = '+' + msisdn
                self.msisdn = msisdn
            else:
                self.msisdn = msisdn
            self.transactionId = transaction[self.TRANSACTION_DETAIL.TRANSACTION_ID]
            self.paidAmount = transaction[self.TRANSACTION_DETAIL.AMOUNT]
            # noinspection PyPep8Naming
            collectTimeAsString = transaction[self.TRANSACTION_DETAIL.COLLECT_TIME]  # Format has changed
            self.collectTime = datetime.datetime.strptime(collectTimeAsString, '%Y-%m-%d %H:%M:%S.%f')
            self.collectTime = delorean.Delorean(self.collectTime, timezone='Africa/Nairobi').shift('UTC').datetime

    # noinspection PyPep8Naming
    @classmethod
    def _get_transaction_detail_objects(cls, transactionResponseRawData):
        """
        Utility to massage airtels concatenated transaction detail.
        Somehow we have to parse "[a#b],[c#d],[e#f]" into [[a.b],[c,d],[e,f]] feel free to modify this even though
        it takes me a while to come up with this stupid algorithm,
        Returns: List of cls.AirtelDetailResponseObject.
        """
        transactions = []
        if not transactionResponseRawData:
            return []
        # noinspection PyPep8Naming
        transactionData = transactionResponseRawData[1:-1].split(TRANSACTIONS_SEPARATOR)
        for transactionDatum in transactionData:
            transaction = AirtelPaymentManager.AirtelDetailResponseObject(transactionDatum.split(TRANSACTION_DETAIL_SEPRATOR))
            transactions.append(transaction)
        return transactions

    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
    @classmethod
    def process_new_payments(
            cls, serviceProviderName=AIRTEL, serviceVersion=PaymentServiceVersion.V2, endDate=None,
            lookBackSeconds=None):
        """Process new payments, look up service provider configuration from db."""
        # noinspection PyPep8Naming
        serviceProviders = ph_model.mobile_payment_provider.NetworkServiceProvider.objects.filter(serviceProvider=serviceProviderName)
        for serviceProvider in serviceProviders:

            # noinspection PyPep8Naming
            endDate = endDate or datetime.datetime.utcnow() + datetime.timedelta(seconds=1)  # Add a second for inclusive
            # noinspection PyPep8Naming
            endDate = endDate.replace(tzinfo=pytz.UTC)
            if lookBackSeconds:
                # noinspection PyPep8Naming
                startDate = endDate - datetime.timedelta(seconds=lookBackSeconds)
            else:
                try:
                    # noinspection PyPep8Naming
                    lastCollectTime = ph_model.mobile_payment_provider.PaymentSyncHistory.successfulPayment.latest().collectTimeTo
                    # noinspection PyPep8Naming
                    startDate = lastCollectTime - + datetime.timedelta(seconds=1)  # Add a second for inclusive
                except ph_model.mobile_payment_provider.PaymentSyncHistory.DoesNotExist:
                    # noinspection PyPep8Naming
                    startDate = endDate - datetime.timedelta(hours=FIRST_TIME_LOOK_UP_INTERVAL_HOURS)

            # noinspection PyPep8Naming
            airTelPayment = serviceVersion.value(serviceProvider.userName, serviceProvider.password, startDate.strftime(AIRTEL_DATE_FORMAT),
                                                 endDate.strftime(AIRTEL_DATE_FORMAT))
            response = airTelPayment.fetch_payments()
            status = PAYMENT_SERVICE_STATUS.get(response.Status) or PAYMENT_SERVICE_STATUS.get(1)
            if status != SUCCESS_RESPONSE:
                payment = ph_model.mobile_payment_provider.PaymentSyncHistory(
                    paymentServiceProvider=serviceProvider,
                    collectTimeFrom=startDate, collectTimeTo=endDate,
                    totalPayments=response.TotalTransactions or 0,
                    totalPayedAmount=response.TotalAmount or 0, status=status)
                payment.save()
                return

            transactions = cls._get_transaction_detail_objects(response.Transactions)
            for transaction in transactions:
                account = None
                if len(transaction.msisdn) == 13:
                    # noinspection PyPep8Naming
                    phoneAccount = ph_model.account.PhoneAccount.objects.get_or_none(
                        mobileMoneyNumber=transaction.msisdn) or \
                                   ph_model.account.PhoneAccount.objects.get_or_none(
                                       mobileMoneyNumber=transaction.payerPhoneNumber)
                    if phoneAccount:
                        account = phoneAccount.account
                else:
                    account = ph_model.account.Account.objects.get_or_none(id=transaction.msisdn)

                if not account:
                    # noinspection PyPep8Naming
                    detailValue = 'payer {} - payee: {} - time: {}'.format(transaction.payerPhoneNumber, transaction.msisdn,
                                                                           str(transaction.collectTime))
                    # noinspection PyPep8Naming
                    previousEntry = ph_model.event.EventDetail.objects.get_or_none(itemValue=detailValue)
                    if not previousEntry:
                        event_manager.raise_event(
                            serviceProvider.id,
                            ph_model.event.EventTypeConst.NETWORK_SERVICE_UNKNOWN_CUSTOMER_PAYMENT.value,
                            [{'detail': detailValue}])
                    continue
                try:
                    transaction_manager.TransactionManager.process_mobile_payment(
                        account, transaction.transactionId,
                        transaction.paidAmount, transaction.collectTime,
                        payment_type=PaymentEnums.ACCOUNT.name,
                        network_service_provider=serviceProvider)

                except transaction_manager.DuplicateTransactions:
                    pass
                    # airtel returns transactions in a 24h rollup, so any transaction will appear as a duplicate on subsequent calls
                    # til the rollup rolls over, generating a lot of warning
                    # LOGGER.warning('Duplicate transaction occurred! %s', transaction.transactionId)

            payment = ph_model.mobile_payment_provider.PaymentSyncHistory(
                paymentServiceProvider=serviceProvider,
                collectTimeFrom=startDate, collectTimeTo=endDate,
                totalPayments=response.TotalTransactions,
                totalPayedAmount=response.TotalAmount, status=status)
            payment.save()
            serviceProvider.collectTime = endDate
            serviceProvider.status = status
            serviceProvider.save()


class TransactionUtil(object):
    """Handle Airtel transactions"""

    # noinspection PyPep8Naming,PyPep8Naming
    def __init__(self, startDate, endDate):
        """
        fetch payments by phone number,
        Arguments:
        startDate: formatted date YYYYMMDDHHMMSS datetime.strftime(AIRTEL_DATE_FORMAT)
        endDate: formatted date YYYYMMDDHHMMSS
        """
        self.startDate = startDate
        self.endDate = endDate
        self.payments = []
        self.fetch_payments()

    def fetch_payments(self):
        """
        parse Airtel payments
        :return:
        """
        # noinspection PyPep8Naming
        serviceProviders = ph_model.mobile_payment_provider.NetworkServiceProvider.objects.filter(serviceProvider=AIRTEL)
        for serviceProvider in serviceProviders:
            # noinspection PyPep8Naming
            airTelPayment = AirtelPaymentWebserviceV2(serviceProvider.userName, serviceProvider.password, self.startDate,
                                                      self.endDate)
            response = airTelPayment.fetch_payments()
            # noinspection PyProtectedMember
            self.payments.extend(AirtelPaymentManager._get_transaction_detail_objects(response.Transactions))

    # noinspection PyPep8Naming
    def filter_payments_by_phone_number(self, phoneNumber):
        """
        Put payments into array
        :param phoneNumber: account phone number for payments
        :return: array of payments
        """
        results = []
        for payment in self.payments:
            if phoneNumber in payment.payerPhoneNumber or payment.msisdn and phoneNumber in payment.msisdn:
                results.append(payment.__dict__)
        return results
