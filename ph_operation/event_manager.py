# coding=utf-8
"""
Module to handle events. Responsible in updating hardware or devices to highest severity reported event,
as well as expiring events.

Classes:
  CircuitEvent
  ProbeEvent
  QueenEvent
  GridEvent
  EventManager
"""

import abc
import datetime
import enum
import logging
from django.db.models import Q

from ph_model.models import Event, Circuit, Probe, Queen, Grid, PowerStation, Inverter, BatteryBank, C2BPaymentValidationRequest, \
    SMSServiceProvider, Customer, PotentialCustomerImportHistory, NetworkServiceProvider
from ph_model.models.event import DEFAULT_EVENT_EXPIRATION_HOUR, EventType, EventDetail
from ph_util import email_util
from ph_util import mixins

LOGGER = logging.getLogger(__name__)

# Minimum highest severity event need attention by users.
MIN_HIGHEST_SEVERE_EVENT = 3


class UnknownEventType(Exception):
    """Unknown event type exception"""
    pass


class EventItemDoesNotExist(Exception):
    """Item associated with event does not exist."""
    pass


class _BaseEvent(object):
    """Event wrapper class, to interact with ph_model.event.Event."""

    # Need to override model associated with event.
    model = None

    def __init__(self, ev):
        """
        Attributes:
          ev: ph_model.Event
        """
        self.event = ev
        self.eventType = self.event.eventType
        self.severity = self.eventType.severity

    @classmethod
    def assert_event_item_exists(cls, event):
        """Asserts that Item id associated with event exists."""
        item = cls.model.objects.get_or_none(pk=event.itemId)
        if not item:
            raise EventItemDoesNotExist("Event Item not exist")

    def remove_expired_events(self):
        """
        ?????
        """
        pass

    def update_hardware(self):
        """
        ?????
        """
        pass

    def get_hardwares(self):
        """
        ?????
        """
        pass

    def get_events_by_item_ids(self):
        """
        ?????
        """
        pass


# noinspection PyPep8Naming
class _DeviceEvent(_BaseEvent):
    """Event wrapper class, to interact with ph_model.event.Event."""
    eventModel = Event

    @classmethod
    def get_events_query_by_item_ids(cls, item_ids, severity=0):
        """Fetches all events by item ids of the hardware."""
        return cls.eventModel.objects.filter(eventType__itemType=cls.ITEM_TYPE,
                                             eventType__severity__gte=severity,
                                             itemId__in=item_ids)

    def update_hardware(self):
        """Updated hardware and it's parent if self.event is most severe than hardware..highestUnclearedEvent."""
        hardwares = self.get_hardwares()
        if hardwares is not None:
            # noinspection PyTypeChecker
            for hardware in hardwares:
                if hardware is not None:
                    if not hardware.highestUnclearedEvent:
                        hardware.highestUnclearedEvent = self.event
                        hardware.save()
                    elif hardware.highestUnclearedEvent.eventType.severity < self.severity >= MIN_HIGHEST_SEVERE_EVENT:
                        hardware.highestUnclearedEvent = self.event
                        hardware.save()

    def remove_expired_events(self):
        """
        remove expired events
        """
        hardwares = self.get_hardwares()
        if hardwares is not None:
            # noinspection PyTypeChecker
            for hardware in hardwares:
                if hardware:
                    hardware.highestUnclearedEvent = None
                    hardware.save()

    @abc.abstractmethod
    def get_hardwares(self):
        """Returns hardware and parent hardware."""
        pass

    # noinspection PyShadowingBuiltins
    @abc.abstractmethod
    def get_all_events_by_parent_model_query(self, id, severity, sortBy):
        """Returns all events query associated to parent @parm id as well children ."""


# noinspection PyPep8Naming
class CircuitEvent(_DeviceEvent):
    """Circuit events"""
    ITEM_TYPE = 'CIRCUIT'
    model = Circuit

    # @override
    def get_hardwares(self):
        """

        :return:
        """
        circuit = self.model.objects.get(pk=self.event.itemId)
        probe = circuit.probe
        queen = circuit.queen
        grid = queen.grid
        return [circuit, probe, queen, grid]

    # @override
    @classmethod
    def get_all_events_by_parent_model_query(cls, circuitId, severity, **kwargs):
        """

        :param circuitId:
        :param severity:
        :param kwargs:
        :return:
        """
        circuitEventsQuery = cls.eventModel.objects.filter(eventType__itemType=cls.ITEM_TYPE,
                                                           eventType__severity__gte=severity,
                                                           itemId=circuitId, **kwargs)
        return circuitEventsQuery


class ProbeEvent(_DeviceEvent):
    """Proe events"""
    ITEM_TYPE = 'PROBE'
    model = Probe

    # @override
    def get_hardwares(self):
        """

        :return:
        """
        probe = self.model.objects.get(pk=self.event.itemId)
        queen = probe.queen
        grid = queen.grid
        return [probe, queen, grid]

    # noinspection PyPep8Naming
    @classmethod
    def get_all_events_by_parent_model_query(cls, probeId, severity, **kwargs):
        """

        :param probeId:
        :param severity:
        :param kwargs:
        :return:
        """
        probe = cls.model.objects.get(pk=probeId)
        probeCircuitIds = probe.circuits.all().values_list('pk', flat=True)
        probeEventsQuery = cls.eventModel.objects.filter(
            Q(eventType__itemType='CIRCUIT', itemId__in=probeCircuitIds) |
            Q(eventType__itemType='PROBE', itemId=probeId),
            Q(eventType__severity__gte=severity))
        return probeEventsQuery


# noinspection PyBroadException
class QueenEvent(_DeviceEvent):
    """Queen events"""
    ITEM_TYPE = 'QUEEN'
    model = Queen

    # @override
    def get_hardwares(self):
        """

        :return:
        """
        try:
            queen = self.model.objects.get(pk=self.event.itemId)
        except:
            queen = None
        if queen is not None:
            grid = queen.grid
            return [queen, grid]

            # @override

    @classmethod
    def get_all_events_by_parent_model_query(cls, queenId, severity, **kwargs):
        """

        :param queenId:
        :param severity:
        :param kwargs:
        :return:
        """
        queen = cls.model.objects.get(pk=queenId)
        queenCircuitIds = queen.queen_circuits.all().values_list('pk', flat=True)
        queenEventsQuery = cls.eventModel.objects.filter(
            Q(eventType__itemType='CIRCUIT', itemId__in=queenCircuitIds) |
            Q(eventType__itemType='QUEEN', itemId=queenId),
            Q(eventType__severity__gte=severity), **kwargs)
        return queenEventsQuery


class GridEvent(_DeviceEvent):
    """Grid events"""
    ITEM_TYPE = 'GRID'
    model = Grid

    # @override
    def get_hardwares(self):
        """

        :return:
        """

        grid = self.model.objects.get_or_none(pk=self.event.itemId)
        return [grid]

    # @override
    @classmethod
    def get_all_events_by_parent_model_query(cls, gridId, severity, **kwargs):
        # TODO this isn't working "cannot resolve keyword 'grid_id' into field"
        """

        :param gridId:
        :param severity:
        :param kwargs:
        :return:
        """
        gridsQueenIds = \
            cls.model.objects.all().filter(id=gridId).values_list('pk', flat=True)
        gridsCircuitIds = Circuit.objects.all().filter(
            queen_id__in=gridsQueenIds).values_list('pk', flat=True)
        gridPowerStationIds = \
            PowerStation.objects.all().filter(grid_id=gridId).values_list('pk', flat=True)
        gridInverterIds = Inverter.objects.all().filter(
            powerstation_id__in=gridPowerStationIds).values_list('pk', flat=True)
        gridBatteryBankIds = BatteryBank.objects.all().filter(
            powerstation__in=gridPowerStationIds).values_list('pk', flat=True)

        gridEventsQuery = cls.eventModel.objects.all().filter(
            Q(eventType__itemType='GRID', itemId=gridId) |
            Q(eventType__itemType='QUEEN', itemId__in=gridsQueenIds) |
            Q(eventType__itemType='CIRCUIT', itemId__in=gridsCircuitIds) |
            Q(eventType__itemType='POWERSTATION', itemId__in=gridPowerStationIds) |
            Q(eventType__itemType='INVERTER', itemId__in=gridInverterIds) |
            Q(eventType__itemType='BATTERYBANK', itemId__in=gridBatteryBankIds),
            Q(eventType__severity__gte=severity),
            **kwargs)
        return gridEventsQuery


class PowerstationEvent(_DeviceEvent):
    """Powerstation events"""
    ITEM_TYPE = 'POWERSTATION'
    model = PowerStation

    # @override
    def get_hardwares(self):
        """

        :return:
        """
        powerstation = self.model.objects.get(pk=self.event.itemId)
        grid = powerstation.grid
        return [powerstation, grid]

    # @override
    @classmethod
    def get_all_events_by_parent_model_query(cls, powerstationId, severity, **kwargs):
        """

        :param powerstationId:
        :param severity:
        :param kwargs:
        :return:
        """
        powerstation = cls.model.objects.get(pk=powerstationId)
        powerstationInverterIds = powerstation.inverters.all().values_list('pk', flat=True)
        powerstationBatteryBankIds = powerstation.battery_banks.all().values_list('pk', flat=True)
        powerstationEventsQuery = cls.eventModel.objects.all().filter(
            Q(eventType__itemType='INVERTER', itemId__in=powerstationInverterIds) |
            Q(eventType__itemType='BATTERYBANK', itemId=powerstationBatteryBankIds) |
            Q(eventType__itemType='POWERSTATION', itemId=powerstationId),
            Q(eventType__severity__gte=severity),
            **kwargs)
        return powerstationEventsQuery


class InverterEvent(_DeviceEvent):
    """Inverter events"""
    ITEM_TYPE = 'INVERTER'
    model = Inverter

    # @override
    def get_hardwares(self):
        """

        :return:
        """
        inverter = self.model.objects.get(pk=self.event.itemId)
        powerstation = inverter.powerstation
        grid = powerstation.grid
        return [inverter, powerstation, grid]

        # @override

    @classmethod
    def get_all_events_by_parent_model_query(cls, inverterId, severity, **kwargs):
        """

        :param inverterId:
        :param severity:
        :param kwargs:
        :return:
        """
        inverterEventsQuery = cls.eventModel.objects.all().filter(
            Q(eventType__itemType='INVERTER', itemId=inverterId),
            Q(eventType__severity__gte=severity),
            **kwargs)
        return inverterEventsQuery


class BatteryBankEvent(_DeviceEvent):
    """Battery events"""
    ITEM_TYPE = 'BATTERYBANK'
    model = BatteryBank

    # @override
    def get_hardwares(self):
        """

        :return:
        """
        batteryBank = self.model.objects.get(pk=self.event.itemId)
        powerstation = batteryBank.powerstation
        grid = powerstation.grid
        return [batteryBank, powerstation, grid]

    # @override
    @classmethod
    def get_all_events_by_parent_model_query(cls, batteryBankId, severity, **kwargs):
        """

        :param batteryBankId:
        :param severity:
        :param kwargs:
        :return:
        """
        return cls.eventModel.objects.filter(
            eventType__itemType='BATTERYBANK',
            eventType__severity__gte=severity,
            itemId=batteryBankId)


class NetworkServiceProviderEvent(_BaseEvent):
    """Mobile money events"""
    ITEM_TYPE = 'NETWORK_SERVICE_PROVIDER'
    model = NetworkServiceProvider


class C2BPaymentValidationRequestEvent(_BaseEvent):
    """MPESA events"""
    ITEM_TYPE = 'C2B_PAYMENT_VALIDATION_REQUEST'
    model = C2BPaymentValidationRequest


class SMSServiceProviderEvent(_BaseEvent):
    """SMS events"""
    ITEM_TYPE = 'SMS_SERVICE_PROVIDER'
    model = SMSServiceProvider


class CustomerEvent(_BaseEvent):
    """Customer events"""
    ITEM_TYPE = 'CUSTOMER'
    model = Customer


class PotentialCustomerEvent(_BaseEvent):
    """Potential customer events"""
    ITEM_TYPE = 'POTENTIAL_CUSTOMER_IMPORT_HISTORY'
    model = PotentialCustomerImportHistory


class EventWrapper(mixins.PairEnumMixin, enum.Enum):
    """Event Wrappers"""
    GRID = GridEvent
    QUEEN = QueenEvent
    PROBE = ProbeEvent
    CIRCUIT = CircuitEvent
    POWERSTATION = PowerstationEvent
    INVERTER = InverterEvent
    BATTERYBANK = BatteryBankEvent
    NETWORK_SERVICE_PROVIDER = NetworkServiceProviderEvent
    MOBILE_PAYMENT_VALIDATION = C2BPaymentValidationRequestEvent
    SMS_SERVICE_PROVIDER = SMSServiceProviderEvent
    CUSTOMER = CustomerEvent
    POTENTIAL_CUSTOMER_IMPORT_HISTORY = PotentialCustomerEvent


class EventManager(object):
    """Class to manage hardware events."""

    @classmethod
    def _get_event_wrapper_by_event_type(cls, eventType):
        """Wrapper for ph_model.event.Event."""
        modelType = eventType.itemType.upper()
        if EventWrapper.is_valid_key(modelType):
            return EventWrapper.value_of(modelType)
        raise UnknownEventType('Unknown item type - {}'.format(modelType))

    @classmethod
    def update_hardware_if_most_severe(cls, event):
        """Update hardware if @param event is highest severe.

        Attributes:
           ev : ph_model.event.Event.
        """
        eventType = event.eventType
        cls._get_event_wrapper_by_event_type(eventType)(event).update_hardware()

    @classmethod
    def _update_hardware_with_most_severe_unexpired_event(cls):
        """Re-connect hardware with most severe events, this needs to be called after expiring most severe events."""

        expirationLookBack = datetime.datetime.utcnow() - datetime.timedelta(hours=DEFAULT_EVENT_EXPIRATION_HOUR)
        mostSevereEvents = Event.objects.all().filter(
            isExpired=False,
            eventType__severity__gte=MIN_HIGHEST_SEVERE_EVENT,
            updated__gte=expirationLookBack
        ).order_by('-eventType__severity')
        LOGGER.info('updating from %d unexpired events' % len(mostSevereEvents))
        for mostSevereEvent in mostSevereEvents:
            eventWrapper = cls._get_event_wrapper_by_event_type(mostSevereEvent.eventType)(mostSevereEvent)
            eventWrapper.update_hardware()

    @classmethod
    def process_expired_events(cls):  # TODO test this shit
        """Detach most severe expired events, and call cls._update_hardware_with_most_severe_unexpired_event to
        reattach hardware to the next un expired most severe events."""

        expirationLookBack = datetime.datetime.utcnow() - datetime.timedelta(hours=DEFAULT_EVENT_EXPIRATION_HOUR)
        expiredEvents = Event.objects.all().filter(
            isExpired=False,
            updated__lte=expirationLookBack
        )
        print('processing %d expired events' % len(expiredEvents))
        cnt = 1
        for expiredEvent in expiredEvents:
            cnt += 1
            expiredEvent.isExpired = True
            eventWrapper = cls._get_event_wrapper_by_event_type(expiredEvent.eventType)(expiredEvent)
            eventWrapper.remove_expired_events()
            expiredEvent.save()
        cls._update_hardware_with_most_severe_unexpired_event()


def raise_event(itemId, eventNumber, eventDetailData=None):
    """
    eventNumber: Number event type.
    eventDetails: list of dict of event detail.
    """
    eventDetailData = eventDetailData or []
    eventType = EventType.objects.get_or_raise_exception(UnknownEventType, eventNumber=eventNumber)
    # noinspection PyProtectedMember
    eventWrapper = EventManager._get_event_wrapper_by_event_type(eventType)
    event = Event(eventType=eventType, itemId=itemId, collectTime=datetime.datetime.utcnow())
    eventWrapper.assert_event_item_exists(event)
    event.save()
    eventDetails = []
    for eventDetailDatum in eventDetailData:
        # noinspection PyCompatibility
        for itemName, itemValue in eventDetailDatum.iteritems():
            eventDetails.append(EventDetail(event=event, itemName=itemName, itemValue=itemValue))
    if eventDetails:
        EventDetail.objects.bulk_create(eventDetails)
    # noinspection PyTypeChecker
    notify_event(event)


def notify_event(event):
    """Notify event if event type is configured to notify immediate."""
    eventType = event.eventType
    if not eventType.notifyImmediate:
        return
    eventDetails = event.details.all()
    data = {'eventType': eventType, 'event': event, 'eventDetails': eventDetails}
    email_util.send_template_email("Event Notification {}".format(eventType.eventDescription), 'event_raised_template.html', data)
