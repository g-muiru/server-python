# coding=utf-8
"""
Module to handle customer logic.

Classes:
Customer: Wrapper on top of ph_model.customer.Customer..
CustomerManager: Manager class to run logic on customer.
CustomerManager.initialize_customer: Initialize new customer, associating ,
"""
import datetime
import logging
import circuit_manager
import event_manager
import ph_model.models as ph_model
import tariff_manager
import transaction_manager
import sms_manager
from django.db.models.query import Q
from ph_model.models.customer import CustomerType
from ph_util import date_util
from ph_util.accounting_util import COMMERCIAL_CONN_LOAN_AMT, RESIDENTIAL_CONN_LOAN_AMT, add_vat, CONN_LOAN_DEPOSIT_AMT
from ph_util.date_util import get_utc_now

LOGGER = logging.getLogger(__name__)


class Customer(object):
    """Customer wrapper class, to enable instantiate customer."""

    def __init__(self, customer=None):
        self.initialized = False
        self._customer = customer
        if customer is None:
            return
        try:
            self._customer = ph_model.customer.Customer.objects.get(id=customer.id)
        except ph_model.customer.Customer.DoesNotExist:
            self._set_new_account()
            event_manager.raise_event(
                self._customer.id,
                ph_model.event.EventTypeConst.NEW_CUSTOMER_CREATED.value,
                [{'customerId': self._customer.customerId}])
        self.account = self._customer.account

    def _set_new_account(self):
        """Set new account."""
        account = ph_model.account.Account()
        account.save()
        self._customer.account = account
        self._customer.save()

    def add_new_phone_accounts(self, phone_accounts):
        """

        :param phone_accounts:
        """
        if phone_accounts:
            for phone_account in phone_accounts:
                # noinspection PyProtectedMember
                phone_account.account = self._customer._account
                phone_account.save()
        else:
            phone_account = ph_model.account.PhoneAccount(
                account=self._customer.account,
                mobileMoneyNumber=self._customer.phoneNumber,
                firstName=self._customer.firstName, lastName=self._customer.lastName)
            phone_account.save()

    def get_customer(self):
        """

        :return:
        """
        return self._customer


class CustomerManager(object):
    """Class to manage circuit logic."""

    @classmethod
    def initialize_customer(cls, customer, tariff_id=None, phone_accounts=None):
        """Initialize customer with default values."""
        customer = Customer(customer)
        tariff_manager.TariffManager.init_tariff(customer.get_customer(), tariff_id)
        customer.add_new_phone_accounts(phone_accounts)

    @classmethod
    def adjust_credit(cls, customer, user, reason, amount, adj_type=None):
        """
        Adjusts credit for a customer.

        :param customer:    ph_model.Customer, customer to add credit to.
        :param user:        O&M user responsible adding credit to the @param customer.
        :param reason:      Reason for adjusting the amount.
        :param amount:      amount adjusted to the customer.
        :param adj_type:    used by NetSuite
        :return:            Adjustment history obj
        """
        customer = Customer(customer)
        if amount > 0:
            t_type = ph_model.transaction.TransactionType.CREDIT.value
            transaction_type = transaction_manager.Credit
        else:
            t_type = ph_model.transaction.TransactionType.DEBIT.value
            transaction_type = transaction_manager.Debit
        amount = abs(amount)
        transaction = transaction_type(customer.account, amount)
        transaction.process(adj_type)
        adjustment_history = ph_model.transaction.CreditAdjustmentHistory(
            account=customer.account,
            user=user,
            reason=reason,
            amount=amount,
            accountBalanceBefore=transaction.accountBalanceBefore,
            accountBalanceAfter=transaction.accountBalance,
            processedTime=get_utc_now(),
            transactionType=t_type,
            adj_type=adj_type
        )
        adjustment_history.save()
        circuit_manager.CircuitManager.configure_circuit_after_account_adjustment(customer.account)
        return adjustment_history

    # TODO(Estifanos)Add test
    @classmethod
    def update_customer_daily_config_data(cls):
        """Place holder to update miscellaneous customer data which needs to be updated daily."""
        municipalities = ph_model.municipality.Municipality.objects.all()
        for municipality in municipalities:
            dt, municipality_time_is_mid_night = date_util.mid_night_just_passed_in_range(
                time_zone=municipality.timeZone,
                range_in_minutes=10)
            if not municipality_time_is_mid_night:
                continue
            customers = ph_model.customer.Customer.objects.filter(Q(circuit__queen__grid__municipality=municipality))
            for customer in customers:
                customer.dailySMSBalRequested = 0
                customer.save()


class PotentialCustomerManager(object):
    """handles various tasks related to new customers"""
    CONNECTION_FEE_PAID_TEMPLATE = \
        'Your Powerhive connection fee is paid! We need 200 customers to bring stima to your village, ' \
        'so please tell your neighbors. Thanks! Questions? Call 0722 999922.'
    CONNECTION_FEE_REMINDER_TEMPLATE = \
        'Friendly reminder: To get stima, you must now pay your connection fee of KShs %s via M-Pesa (829250) or Airtel Money (22870). ' \
        'Questions? Call 0722 999922'

    @classmethod
    def get_amount_owed_by_customer_type(cls, customer_type, connection_fee_loan=False):
        """

        :param customer_type:
        :param connection_fee_loan:
        :return:
        """
        if customer_type == CustomerType.RESIDENTIAL:
            if connection_fee_loan:
                return add_vat(RESIDENTIAL_CONN_LOAN_AMT)
            else:
                return add_vat(RESIDENTIAL_CONN_LOAN_AMT)
        elif customer_type == CustomerType.COMMERCIAL:
            return add_vat(COMMERCIAL_CONN_LOAN_AMT)
        elif customer_type == CustomerType.COMMERCIAL_II:
            return add_vat(COMMERCIAL_CONN_LOAN_AMT)

    @classmethod
    def process_paid_connection_fees(cls):
        """
        Send sms when customer pays their connection fee
        # smh TODO generate invoice
        """
        potential_customers = ph_model.customer.Customer.objects.filter(status=ph_model.customer.CustomerStatus.POTENTIAL.value)
        type_tuples = [(CustomerType.RESIDENTIAL, False), (CustomerType.RESIDENTIAL, True),
                       (CustomerType.COMMERCIAL, False), (CustomerType.COMMERCIAL_II, False)]
        for customerType, connectionFeeLoan in type_tuples:
            amount_owed = cls.get_amount_owed_by_customer_type(customerType, connectionFeeLoan)

            customers = potential_customers.filter(
                account__accountBalance__gte=amount_owed, customerType=customerType.value, connectionFeeLoan=connectionFeeLoan)
            for c in customers:
                LOGGER.info('%s paid their deposit!' % c.getName())
                c.status = ph_model.customer.CustomerStatus.PAID_DEPOSIT.value
                c.save()
                event_manager.raise_event(
                    c.id,
                    ph_model.event.EventTypeConst.CUSTOMER_PAID_DEPOSIT.value,
                    [{'customer': str(c)}]
                )
                sms_manager.CustomerSMSManager(c.id, cls.CONNECTION_FEE_PAID_TEMPLATE).send()

    @classmethod
    def send_connection_fee_reminders(cls, days_between_messages=3):
        """

        :param days_between_messages:
        """
        contact_only_before = get_utc_now() - datetime.timedelta(days_between_messages)
        potential_customers = ph_model.customer.Customer.objects.filter(status=ph_model.customer.CustomerStatus.POTENTIAL.value)
        potential_customer_sms = ph_model.sms.SMS.objects.filter(customer__in=potential_customers, collectTime__gte=contact_only_before)
        potential_customers = potential_customers.exclude(id__in=potential_customer_sms.values_list('customer_id', flat=True)).distinct()
        sent_customers = set()
        for c in potential_customers:
            if c.id not in sent_customers:
                sent_customers.add(c.id)
            else:
                continue
            connection_fee = add_vat(CONN_LOAN_DEPOSIT_AMT)
            if not c.connectionFeeLoan:
                if c.customerType == CustomerType.RESIDENTIAL:
                    connection_fee = add_vat(RESIDENTIAL_CONN_LOAN_AMT)
                else:
                    connection_fee = add_vat(COMMERCIAL_CONN_LOAN_AMT)
            amount_owed = connection_fee - c.account.accountBalance
            if amount_owed <= 0:
                LOGGER.info('connection fee already paid by %s, not sending reminder' % c.getName())
                continue
            amount_owed = "{:,}".format(int(amount_owed))
            if c.connectionFeeDue is None:
                continue
            if c.connectionFeeDue < get_utc_now():
                LOGGER.info('connection fee past due for %s, not sending reminder' % c.getName())
                continue
            date_owed = c.connectionFeeDue.strftime('%d %b')
            message = cls.CONNECTION_FEE_REMINDER_TEMPLATE % amount_owed
            LOGGER.info('sending message %s to %s date_owed: %s' % (message, c.getName(), date_owed))
            sms_manager.CustomerSMSManager(c.id, message).send()
