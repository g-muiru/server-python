# coding=utf-8
"""
Module to handle events. Responsible in updating hardware or devices to highest severity reported event,
as well as expiring events.

Classes:
GridSMSManager
QueenSMSManager
CircuitSMSManager
CustomerSMSManager
"""

import abc
import collections
import csv
import logging
import re
from datetime import timedelta

import phonenumbers
from delorean import utcnow
from django.conf import settings

import event_manager
import ph_model.models as ph_model
import scratchcard_manager
from ph_ext_lib import africas_talking_gateway
from ph_model.models import Customer
from ph_util import phone_number_util
from ph_util.accounting_util import DEFAULT_SMS_LOW_BALANCE_DATE_STR, LOW_BALANCE_WARNING_AMOUNT
from ph_util.date_util import str_to_dt
from ph_util.db_utils import calc_monthly_usage_stima_points, process_materialized_views
from ph_util.test_utils import test_mode

LOGGER = logging.getLogger(__name__)

DEFAULT_SHORT_CODE_POWERHIVE = "22870"
DEFAULT_SENDER = "Powerhive"
DEFAULT_COUNTRY_CODE = "+254"
DEFAULT_COUNTRY = "KE"


class SMSManagerException(Exception):
    """TODO: log exception"""
    pass


# noinspection PyPep8Naming
def get_customer_for_target_number(targetNumber):
    """

    :param targetNumber:
    :return:
    """
    # noinspection PyBroadException,PyUnusedLocal
    try:
        if len(targetNumber) > 10:
            return ph_model.customer.Customer.objects.filter(phoneNumber=targetNumber)[0]
        else:
            return ph_model.customer.Customer.objects.get(account__id=targetNumber)
    except Exception as e:
        return None


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class _IncomingSMS(object):
    """Abstract base class to different incoming sms type."""
    CODE = None
    TYPE = None
    IS_AUTO_RESPONSE = False

    def __init__(self, message, senderPhone):
        """
        Attribute:
        CODE: SMS text code to do inquery, e.g. Balance, #Bal ...
        TYPE: enum sms type, e.g. BalanceInquiry
        IS_AUTO_RESPONSE: Boolean to indicate if is auto response
        message: text body of incoming sms message
        senderPhone: Sender's phone number.
        """
        self.message = message
        self.cleanedMessage = message.lower()
        self.senderPhone = senderPhone
        self.type = self.TYPE
        self.isAutoResponse = self.IS_AUTO_RESPONSE
        self.intendedCustomerNumbers = self.identify_potential_target_numbers()
        self.customer = self.identify_target_customer()
        self.customerPhone = self.customer and str(self.customer.phoneNumber)
        if self.customerPhone != self.senderPhone:
            self.intendedCustomerNumbers = [self.customerPhone, self.senderPhone]
        else:
            self.intendedCustomerNumbers = [self.senderPhone]
        self.isCustomerActive = self.customer and self.customer.status in ph_model.customer.CustomerStatus.ACTIVE_STATUSES.value

    def identify_target_customer(self):
        """
        Due to legacy feature, sms target could be sender's phone number or one of number listed in text body.

        :return customer from list of potential customer phones.
        """
        customer = None
        for targetNumber in self.intendedCustomerNumbers:
            customer = get_customer_for_target_number(targetNumber)
            if customer:
                break
        return customer

    # noinspection PyMethodMayBeStatic
    def convert_phone_to_international_format(self, phoneNumbers):
        """

        :param phoneNumbers:
        :return:
        """
        return phoneNumbers

    @abc.abstractmethod
    def identify_potential_target_numbers(self):
        """Returns list of potential target phone numbers."""
        pass

    @abc.abstractmethod
    def get_auto_response_message(self):
        """Returns template of response message."""
        pass

    @classmethod
    def can_handle_sms(cls, textBody):
        """Returns boolean if textBody can be precessed by this class."""
        pass

    @classmethod
    def auto_response(cls, inComingSMSHandler, phAccount, gateway):
        """Auto response incoming message."""
        pass


# noinspection PyAbstractClass,PyPep8Naming
class DefaultIncomingSMS(_IncomingSMS):
    """Abstract SMS"""
    CODE = None
    IS_AUTO_RESPONSE = False
    TYPE = ph_model.sms.SMSType.INFORMATION.value

    def identify_potential_target_numbers(self):
        """wrapper"""
        return [self.senderPhone]

    def can_handle_sms(self, cleanedMessage):
        """useless"""
        return True


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class ScratchcardPaymentIncomingSMS(_IncomingSMS):
    """Screatch card SMS"""
    CODE = ['#spay#']
    TYPE = ph_model.sms.SMSType.SCRATCH_CARD_PAYMENT.value
    IS_AUTO_RESPONSE = True
    BALANCE_AFTER_PAYMENT_RESPONSE_TEMPLATE = "Hi {}, your new account balance is {}"

    def get_pin(self):
        """Expected data format #spay#KEN12343223232#optionalPhone"""
        try:
            return [d.strip() for d in self.message.split('#') if d][1]
        except IndexError:
            pass

    def identify_potential_target_numbers(self):
        """Expected data format #spay#KEN12343223232#optionalPhone"""
        data = [d.strip() for d in self.cleanedMessage.split('#') if d]
        phoneNumbers = []
        if data and len(data) == 3:
            # Target phone number is included in the message
            phoneNumbers.append(str(int(data[2])))
        phoneNumbers.append(self.senderPhone)
        intendedCustomerPhones = []
        for phoneNumber in phoneNumbers:
            if not (phoneNumber.startswith('+') or phoneNumber.startswith('00')):
                phoneNumber = "{}{}".format(DEFAULT_COUNTRY_CODE, phoneNumber)
            intendedCustomerPhones.append(phoneNumber)
        return intendedCustomerPhones

    @classmethod
    def can_handle_sms(cls, cleanedMessage):
        """

        :param cleanedMessage:
        :return:
        """
        for code in cls.CODE:
            if code in cleanedMessage:
                return True
        return False

    def get_auto_response_message(self):
        """

        :return:
        """
        name = self.customer.firstName or self.customer.lastName
        return self.BALANCE_AFTER_PAYMENT_RESPONSE_TEMPLATE.format(name, self.customer.account.accountBalance)

    @classmethod
    def auto_response(cls, inComingSMSHandler, phAccount, gateway):
        """Auto response incoming message.
        Attribute:
        inComingSMSHandler: self object
        phAccount: powerhive account associated with this sms handler:
        gateway: e.g Africas talk gateway
        """
        if not isinstance(inComingSMSHandler, cls) or not inComingSMSHandler.isCustomerActive:
            return

        scratchcardManager = scratchcard_manager.ScratchcardManager(inComingSMSHandler.customer, inComingSMSHandler.get_pin())
        if scratchcardManager.isPinValid:
            scratchcardManager.process()
            autoResponseMessage = inComingSMSHandler.get_auto_response_message()
            send_sms(inComingSMSHandler.intendedCustomerNumbers, autoResponseMessage, phAccount, gateway, phAccount.shortCode)


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class BalanceInquiryIncomingSMS(_IncomingSMS):
    """Balance inquiriers"""
    CODE = ['balance', 'bal']
    TYPE = ph_model.sms.SMSType.BALANCE_INQUIRY.value
    IS_AUTO_RESPONSE = True
    BALANCE_INQUIRY_RESPONSE_TEMPLATE = "Hi %s, your account balance is Kshs %2.2f. Total STIMA PTS: %s."

    def identify_potential_target_numbers(self):
        """Identify phone numbers"""
        targetNumbers = [str(int(x)) for x in re.findall(r'\d+', self.cleanedMessage)]
        targetNumbers.append(self.senderPhone)
        intendedCustomerNumbers = []
        for targetNumber in targetNumbers:
            if len(targetNumber) >= 9 and not targetNumber.startswith(DEFAULT_COUNTRY_CODE):
                targetNumber = phone_number_util.get_e164_format(targetNumber, DEFAULT_COUNTRY_CODE)
            intendedCustomerNumbers.append(targetNumber)
        return intendedCustomerNumbers

    @classmethod
    def can_handle_sms(cls, cleanedMessage):
        """

        :param cleanedMessage:
        :return:
        """
        for code in cls.CODE:
            if code == cleanedMessage or code in cleanedMessage:
                return True
        return False

    def get_auto_response_message(self):
        """

        :return:
        """
        # noinspection PyBroadException
        try:
            dailyAllowedSMSBalReq = self.customer.account.accountRule.dailyAllowedSMSBalReq
        except Exception:
            dailyAllowedSMSBalReq = ph_model.account.DEFAULT_NUM_OF_ALLOWED_BAL_REQ
        if self.customer.dailySMSBalRequested >= dailyAllowedSMSBalReq:
            return
        usageRollup, stima_points, kwh = calc_monthly_usage_stima_points(self.customer)
        resp = self.BALANCE_INQUIRY_RESPONSE_TEMPLATE % (self.customer.firstName, float(self.customer.account.accountBalance), stima_points)
        LOGGER.info(resp)
        return resp

    @classmethod
    def auto_response(cls, inComingSMSHandler, phAccount, gateway):
        """Auto response incoming message.
        Attribute:
        inComingSMSHandler: self object
        phAccount: powerhive account associated with this sms handler:
        gateway: e.g Africas talk gateway
        """

        if not isinstance(inComingSMSHandler, cls) or not inComingSMSHandler.isCustomerActive:
            return
        autoResponseMessage = inComingSMSHandler.get_auto_response_message()
        respond_to = inComingSMSHandler.intendedCustomerNumbers
        send_sms(respond_to, autoResponseMessage, phAccount, gateway, phAccount.shortCode)
        inComingSMSHandler.customer.dailySMSBalRequested += 1
        inComingSMSHandler.customer.save()


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class InternetIncomingSMS(_IncomingSMS):
    """Internet Voucher Purchase or Subscription"""
    CODE = ['internet']
    TYPE = "INTERNET"
    IS_AUTO_RESPONSE = True
    INTERNET_RESPONSE_TEMPLATE = "Hi %s, your internet access code is %s"

    def identify_potential_target_numbers(self):
        """Identify phone numbers"""
        targetNumbers = [str(int(x)) for x in re.findall(r'\d+', self.cleanedMessage)]
        targetNumbers.append(self.senderPhone)
        intendedCustomerNumbers = []
        for targetNumber in targetNumbers:
            if len(targetNumber) >= 9 and not targetNumber.startswith(DEFAULT_COUNTRY_CODE):
                targetNumber = phone_number_util.get_e164_format(targetNumber, DEFAULT_COUNTRY_CODE)
            intendedCustomerNumbers.append(targetNumber)
        return intendedCustomerNumbers

    @classmethod
    def can_handle_sms(cls, cleanedMessage):
        """

        :param cleanedMessage:
        :return:
        """
        for code in cls.CODE:
            if code == cleanedMessage or code in cleanedMessage:
                return True
        return False

    def get_auto_response_message(self):
        """
        TODO get a voucher and return the code
        :return:
        """
        # noinspection PyBroadException
        resp = self.INTERNET_RESPONSE_TEMPLATE % (self.customer.firstName, "0460307021")
        LOGGER.info(resp)
        return resp

    @classmethod
    def auto_response(cls, inComingSMSHandler, phAccount, gateway):
        """Auto response incoming message.
        Attribute:
        inComingSMSHandler: self object
        phAccount: powerhive account associated with this sms handler:
        gateway: e.g Africas talk gateway
        """

        if not isinstance(inComingSMSHandler, cls) or not inComingSMSHandler.isCustomerActive:
            return
        autoResponseMessage = inComingSMSHandler.get_auto_response_message()
        respond_to = inComingSMSHandler.intendedCustomerNumbers
        send_sms(respond_to, autoResponseMessage, phAccount, gateway, phAccount.shortCode)
        inComingSMSHandler.customer.dailySMSBalRequested += 1
        inComingSMSHandler.customer.save()


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class IncomingSMSFactory(object):
    """SMS factory"""
    INCOMING_SMS_TYPE = [BalanceInquiryIncomingSMS, ScratchcardPaymentIncomingSMS, InternetIncomingSMS]
    DEFAULT_SMS_TYPE = DefaultIncomingSMS

    def __init__(self, message, senderPhone):
        """

        :param message:
        :param senderPhone:
        """
        incomingSMSTypeCount = 0
        inComingSMSHandler = None
        cleanedMessage = message.lower()
        for incomingSmsType in self.INCOMING_SMS_TYPE:
            if incomingSmsType.can_handle_sms(cleanedMessage):
                inComingSMSHandler = incomingSmsType
                incomingSMSTypeCount = +1
        if incomingSMSTypeCount > 1:
            # TODO(estifanos) Raise event sms type have multiple incoming smstype
            pass

        inComingSMSHandler = inComingSMSHandler or self.DEFAULT_SMS_TYPE
        self.incomingSMS = inComingSMSHandler(message, senderPhone)


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
def get_sms_config(shortCode):
    """Returns tuple of phAccount, gateway"""
    phAccount = ph_model.sms.PowerhiveSMSAccount.objects.get(shortCode=shortCode)
    smsHost = phAccount.smsHost
    gateway = africas_talking_gateway.AfricasTalkingGateway(smsHost.userName, smsHost.apiKey)
    return phAccount, gateway, smsHost


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
def send_mass_sms(targetCsv, message):
    """ sends bulk sms to users in csv """
    toNumbers = ""
    spamreader = csv.DictReader(targetCsv)
    for row in spamreader:
        phoneNumber = None
        try:
            phoneNumber = phonenumbers.parse(row['phone_number'], DEFAULT_COUNTRY)
        except AttributeError:
            LOGGER.warning("bad phone number: " + row['phone_number'])
        except phonenumbers.NumberParseException:
            LOGGER.warning("bad phone number: " + row['phone_number'])

        if phoneNumber and phonenumbers.is_possible_number(phoneNumber):
            if toNumbers:
                toNumbers = toNumbers + "," + phonenumbers.format_number(phoneNumber, phonenumbers.PhoneNumberFormat.E164)
            else:
                toNumbers = phonenumbers.format_number(phoneNumber, phonenumbers.PhoneNumberFormat.E164)

    if toNumbers:
        phAccount, gateway, smsHost = get_sms_config(DEFAULT_SHORT_CODE_POWERHIVE)
        recipients = gateway.sendMessage(toNumbers, message, from_=DEFAULT_SENDER)
        event_manager.raise_event(
            smsHost.id, ph_model.event.EventTypeConst.SMS_BROADCAST_TOOL_SUCCESS.value, [{'message': message}])
        return recipients


# noinspection PyPep8Naming,PyPep8Naming,PyUnusedLocal
def send_sms(to, message, phAccount, gateway, shortCode, no_send=False):
    """
    Sends sms message to specified users.

    :param to:
    :param message:
    :param phAccount:
    :param gateway:
    :param shortCode:
    :param no_send:
    :return:
    """

    if settings.DISABLE_SMS or not (to and message):
        if not test_mode():
            return message
    smss = []
    if type(to) not in (str, unicode) and isinstance(to, collections.Iterable):
        to = ",".join(to)
    try:
        if test_mode():
            recipients = [{'number': str(to), 'status': 'OK', 'messageId': 12345, 'cost': 111}]
        else:
            recipients = gateway.sendMessage(to, message, from_=DEFAULT_SENDER)

        for recipient in recipients:
            # Note that only the Status "Success" means the message was sent
            customer = get_customer_for_target_number(targetNumber=recipient['number'])
            if not customer:
                event_manager.raise_event(phAccount.smsHost.id,
                                          ph_model.event.EventTypeConst.SMS_FROM_UNREGISTERED_USER.value, [{'number': recipient['number']}])
                smss.append(None)
                continue
            sms = ph_model.sms.SMS(message=message, status=recipient['status'],
                                   outBoundSMSId=recipient['messageId'], phAccount=phAccount, customer=customer,
                                   smsFlow=ph_model.sms.SMSFlow.OUTBOUND.value, cost=recipient['cost'])
            smss.append(sms)

        if no_send is False:
            smss = [i for i in smss if i is not None]
            if len(smss) > 0:
                ph_model.sms.SMS.objects.bulk_create(smss)
    except africas_talking_gateway.AfricasTalkingGatewayException as e:
        raise SMSManagerException('{}'.format(str(e)))
    return smss


# noinspection PyPep8Naming
def fetch_incoming_sms_message(phAccount, gateway):
    """Sync incoming sms message."""
    # noinspection PyPep8Naming
    latestInboundSMSId = phAccount.lastInBoundSMSId
    # noinspection PyPep8Naming
    firstSMSData = {}
    # noinspection PyPep8Naming
    smsServiceProvider = phAccount.smsHost
    while True:
        messages = gateway.fetchMessages(latestInboundSMSId)
        if not messages:
            break
        if not firstSMSData:
            # noinspection PyPep8Naming
            firstSMSData = messages[0]
        # noinspection PyPep8Naming
        latestSMSData = messages[-1]
        # noinspection PyPep8Naming
        latestInboundSMSId = latestSMSData['id']
        for message in messages:
            # noinspection PyPep8Naming
            collectTime = message['date']
            # noinspection PyPep8Naming
            incomingSMSHandler = IncomingSMSFactory(message['text'], message['from'])
            # noinspection PyPep8Naming
            incomingSMS = incomingSMSHandler.incomingSMS
            customer = incomingSMS.customer

            if not customer:
                event_manager.raise_event(smsServiceProvider.id,
                                          ph_model.event.EventTypeConst.SMS_FROM_UNREGISTERED_USER.value, [{'number': message['from']}])
                continue
            if customer.status == ph_model.customer.CustomerStatus.INACTIVE.value:
                continue
            # noinspection PyPep8Naming
            existingSMS = ph_model.sms.SMS.objects.get_or_none(inBoundSMSId=message['id'], phAccount=phAccount)
            if existingSMS:
                continue
            sms = ph_model.sms.SMS(collectTime=collectTime, message=incomingSMS.message, inBoundSMSId=message['id'],
                                   phAccount=phAccount, customer=customer, smsFlow=ph_model.sms.SMSFlow.INBOUND.value,
                                   type=incomingSMS.type, senderPhone=message['from'])
            sms.save()
            incomingSMS.auto_response(incomingSMS, phAccount, gateway)

    phAccount.lastInBoundSMSId = latestInboundSMSId
    phAccount.save()


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
class _SMSManager(object):
    """SMS wrapper class, to interact with ph_model.sms"""

    # Abstract property need to be override.
    model = None

    def __init__(self, sms_id, message, shortCode=DEFAULT_SHORT_CODE_POWERHIVE):
        """
        Attributes:
        sms_id: model id
        message: Message to be sent
        shortCode: short code to identify sender.
        """
        self.shortCode = shortCode
        self.message = message
        self.phAccount, self.gateway, self.smsHost = get_sms_config(shortCode)
        self.object = self.model.objects.get(id=sms_id)
        # noinspection PyTypeChecker
        self.to = ",".join(self.get_recipients_phone())

    @abc.abstractmethod
    def get_recipients_phone(self):
        """Gets recipients phone numbers associated to the model."""
        pass

    def send(self, no_send=False):
        """Sends sms message to specified users."""
        return send_sms(self.to, self.message, self.phAccount, self.gateway, self.shortCode, no_send)

    @classmethod
    def fetch_incoming_sms_message(cls, shortCode=DEFAULT_SHORT_CODE_POWERHIVE):
        """

        :param shortCode:
        :return:
        """
        phAccount, gateway, smsHost = get_sms_config(shortCode)
        return fetch_incoming_sms_message(phAccount, gateway)


class GridSMSManager(_SMSManager):
    """wrapper"""
    model = ph_model.grid.Grid

    def get_recipients_phone(self):
        """wrapper"""
        return ph_model.customer.Customer.objects.values_list('phoneNumber', flat=True).filter(circuit__queen__grid=self.object)


class QueenSMSManager(_SMSManager):
    """wrapper"""
    model = ph_model.queen.Queen

    def get_recipients_phone(self):
        """get phone numbers"""
        return ph_model.customer.Customer.objects.values_list('phoneNumber', flat=True).filter(circuit__queen=self.object)


class CircuitSMSManager(_SMSManager):
    """wrapper"""
    model = ph_model.circuit.Circuit

    def get_recipients_phone(self):
        """get phone numbers"""
        return ph_model.customer.Customer.objects.values_list('phoneNumber', flat=True).filter(circuit=self.object)


class CustomerSMSManager(_SMSManager):
    """"wrapper"""
    model = ph_model.customer.Customer

    def get_recipients_phone(self):
        """get phone numbers"""
        return ph_model.customer.Customer.objects.values_list('phoneNumber', flat=True).filter(id=self.object.id)


LOW_BAL_ENGLISH = 'Dear %s, Your Powerhive remaining  balance is less than KShs %s. Purchase electricity credits now to avoid a supply disruption.'
LOW_BAL_SWAHILI = 'Jambo %s. Salio lako la stima limebakia chini ya shilingi %s. Tafadhali ongeza pesa ili stima isikatike.'
NEG_BAL_ENGLISH = 'Dear %s, Your Powerhive remaining balance has been depleted. Purchase electricity credits now in order to have your power back.'
NEG_BAL_SWAHILI = 'Jambo %s, akaunti yako ya stima imeisha pesa. Tafadhali weka pesa ili stima irudi leo. Asante!'


def notify_balance(cust, cur_date):
    """
    This code blocks on sending SMS
    sms notification for low balance when account balance is below the low balance threshold and > 0
    only at operational sites (grids) and customers with circuits.

    Also send

    :param cust: The customer to check
    :param cur_date: Set to run for date other than today
    :return: True if SMS sent

    """
    if cust.circuit_id is None or cust.circuit.queen.grid.operational_date > utcnow().date:
        return False

    if cust.account.accountBalance <= LOW_BALANCE_WARNING_AMOUNT:
        if cust.account.accountBalance > 0:
            dt = cust.account.sms_low_bal_sent
            if cur_date - dt < timedelta(days=7):
                return False
            if cust.languagePreference == 'English':
                message = LOW_BAL_ENGLISH % (cust.firstName, LOW_BALANCE_WARNING_AMOUNT)
            else:
                message = LOW_BAL_SWAHILI % (cust.firstName, LOW_BALANCE_WARNING_AMOUNT)
            CustomerSMSManager(cust.id, message).send()
            cust.account.sms_low_bal_sent = cur_date
            cust.account.sms_neg_bal_sent = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()
            cust.account.save()
            return True
        else:
            dt = cust.account.sms_neg_bal_sent
            if cur_date - dt < timedelta(days=7):
                return False
            if cust.languagePreference == 'English':
                message = NEG_BAL_SWAHILI % cust.firstName
            else:
                message = NEG_BAL_ENGLISH % cust.firstName
            CustomerSMSManager(cust.id, message).send()
            cust.account.sms_neg_bal_sent = cur_date
            cust.account.sms_low_bal_sent = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()
            cust.account.save()
            return True
    else:
        # reset week timer if account balance goes positive
        dt = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()
        do_save = False
        if cust.account.sms_low_bal_sent != dt:
            do_save = True
            cust.account.sms_low_bal_sent = dt

        if cust.account.sms_neg_bal_sent != dt:
            do_save = True
            cust.account.sms_neg_bal_sent = dt

        if do_save:
            cust.account.save()
    return False


def db_maintenance():  # pragma: no cover
    """
    By default refresh data in materialized views
    Update the days_no_balance column
    Verify daily fee for residential customers was deducted
    Designed to be called once per day

    :return:
    """
    update_days_no_balance(Customer.objects.all().order_by('id'))
    process_materialized_views()


def update_stima_kwh(c):
    """
    Save stima calc to account
    :param c:
    :return:
    """
    res = calc_monthly_usage_stima_points(c)
    if res[0] == 0 and res[1] == 0 and res == res[2]:
        print('SKIP')
        return
    if c.account.stima != res[1]:
        c.account.stima = res[1]
        c.account.save()
    if c.cumulativeKwh != res[2]:
        c.cumulativeKwh = res[2]
        c.save()


def update_days_no_balance(customers, cur_date=utcnow().date):
    """ Increments the Account days_no_balance field if required.
        Customers without a circuit are decremented to indicate how many days without a circuit
        This code is designed to run once per day

        :param customers:    customers to check
        :param cur_date:     date to check
        :return:             results if testing
       """
    vals = []
    for c in customers:
        update_stima_kwh(c)
        notify_balance(c, cur_date)
        org_cnt = c.days_no_balance
        if c.circuit_id is None:
            if c.days_no_balance > 0:
                c.days_no_balance = -1
            else:
                c.days_no_balance -= 1
        else:
            if c.account.accountBalance <= 0:
                if c.days_no_balance < 0:
                    c.days_no_balance = 1
                else:
                    c.days_no_balance += 1
            else:
                c.days_no_balance = 0
        if org_cnt != c.days_no_balance:
            c.save()
            if test_mode():
                vals.append((c.account_id, c.circuit_id, c.days_no_balance))
    return vals
