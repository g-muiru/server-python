# coding=utf-8
"""Tests for customer manager"""
import datetime

from ddt import ddt, unpack, idata
from delorean import utcnow
from django.core.management import call_command

import ph_model.models as ph_model
from ph_model.models import Customer, Loan, Circuit
from ph_model.models.customer import CustomerType
from ph_operation import customer_manager
from ph_operation.sms_manager import update_days_no_balance
from ph_operation.tests import base_test
from ph_util.accounting_util import LOW_BALANCE_WARNING_AMOUNT, DEFAULT_SMS_LOW_BALANCE_DATE_STR
from ph_util.date_util import str_to_dt
from ph_util.loan_utils import mth_mobile_balance


class BaseCustomerTestCase(base_test.BaseTestOperation):
    """Base customer manger test case."""

    def setUp(self):
        """ setup """
        super(BaseCustomerTestCase, self).setUp()


class CustomerTestCase(BaseCustomerTestCase):
    """Tests customer_manager.Customer."""

    def test_init(self):
        customer = ph_model.customer.Customer(customerId=123, phoneNumber='712139722', municipality_id=1)
        customer_wrapper = customer_manager.Customer(customer)
        self.assertIsNotNone(customer_wrapper.get_customer().id)


class CustomerManagerTestCase(BaseCustomerTestCase):
    """Tests customer_manager.CustomerManager."""

    def test_initialize_customer(self):
        ph_model.event.Event.objects.all().delete()
        circuit = ph_model.circuit.Circuit.objects.get(deviceId='NO_CUSTOMER_CIRCUIT')
        customer = ph_model.customer.Customer(customerId=123, phoneNumber='712139722',
                                              municipality_id=1, circuit=circuit)
        self.assertIsNone(customer.id)
        self.assertIsNone(customer.tariff)
        self.assertRaises(ph_model.account.PhoneAccount.DoesNotExist,
                          ph_model.account.PhoneAccount.objects.get, mobileMoneyNumber='712139722')
        customer_manager.CustomerManager.initialize_customer(customer)
        self.assertIsNotNone(customer.customerId)
        self.assertIsNotNone(customer.account)
        self.assertIsNotNone(customer.tariff)
        self.assertIsNotNone(ph_model.account.PhoneAccount.objects.get(mobileMoneyNumber='712139722'))
        new_customer_created_event_count = ph_model.event.Event.objects.filter(
            eventType__eventNumber=ph_model.event.EventTypeConst.NEW_CUSTOMER_CREATED.value).count()
        self.assertEqual(new_customer_created_event_count, 1)


class PotentialCustomerManagerTestCase(BaseCustomerTestCase):
    def test_process_paid_fees(self):
        customer = ph_model.customer.Customer.objects.first()
        customer.status = 'POTENTIAL'
        customer.account.accountBalance = 9999
        customer.account.save()
        customer.save()
        customer_manager.PotentialCustomerManager.process_paid_connection_fees()
        customer = ph_model.customer.Customer.objects.get(id=customer.id)
        self.assertEquals(customer.status, 'PAID_DEPOSIT')

    def test_connection_fee_reminder(self):
        customer = ph_model.customer.Customer.objects.first()
        customer.status = 'POTENTIAL'
        customer.account.accountBalance = 0
        customer.connectionFeeDue = customer_manager.get_utc_now() + datetime.timedelta(days=5)
        customer.account.save()
        customer.save()
        ph_model.sms.SMS.objects.all().delete()
        customer_manager.PotentialCustomerManager.send_connection_fee_reminders()
        self.assertEqual(ph_model.sms.SMS.objects.count(), 1, 'test mode sms not really sent')


class CustomerCreditAdjustmentTestCase(BaseCustomerTestCase):
    def setUp(self):
        """ setup """
        super(CustomerCreditAdjustmentTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/customer_credit_adjustment_config.json', verbosity=0)
        self.account = ph_model.account.Account.objects.get(id=701)
        self.customer = ph_model.customer.Customer.objects.get(id=701)
        # Configured in test fixture.
        self.ACCOUNT_BALANCE = 100.0
        mth_mobile_balance(self.ACCOUNT_BALANCE, self.account.id)
        self.reason = 'Testing purpose.'

    def test_adjust_credit_credit(self):
        """Tests customer_manager.CustomerManager.adjust_credit."""
        amount_to_be_adjusted = 110.0
        balance_after_adjustment = 210.0
        user = ph_model.user.User.objects.all()[0]
        if not user:
            return
        ph_model.transaction.CreditAdjustmentHistory.objects.all().delete()
        self.assertEqual(self.ACCOUNT_BALANCE, self.account.accountBalance)
        customer_manager.CustomerManager.adjust_credit(self.customer, user, self.reason, amount_to_be_adjusted)
        self.assertEqual(balance_after_adjustment, ph_model.account.Account.objects.get(id=self.account.id).accountBalance)

    def test_adjust_credit_debit(self):
        """Tests customer_manager.CustomerManager.adjust_credit."""
        amount_to_be_adjusted = -100.0
        balance_after_adjustment = 0.0
        user = ph_model.user.User.objects.all()[0]
        if not user:
            return
        ph_model.transaction.CreditAdjustmentHistory.objects.all().delete()
        self.assertEqual(self.ACCOUNT_BALANCE, self.account.accountBalance)
        customer_manager.CustomerManager.adjust_credit(self.customer, user, self.reason, amount_to_be_adjusted)
        self.assertEqual(balance_after_adjustment, ph_model.account.Account.objects.get(id=self.account.id).accountBalance)


class BalanceCheckTestCase(BaseCustomerTestCase):
    def setUp(self):
        """ setup """
        super(BalanceCheckTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/customer_credit_adjustment_config.json', verbosity=0)
        for c in Customer.objects.all():
            if c.account_id % 2 == 1:
                c.account.accountBalance = 0
            else:
                c.account.accountBalance = 999
            if c.account_id > 500:
                c.circuit_id = None
                c.save()
            c.account.save()

    def test_update_days_no_balance_create_connection_loan(self):
        """
        Tests update_days_no_balance function
        Increments days_no_balance if balance is zero and customer has a circuit
        Decriments days_no_balance if customer does not have a circuit
        days_no_balance = -1 if  days_no_balance is > zero, and customer does not have a circuit
        :return: 
        """
        test_func = update_days_no_balance
        Loan.objects.all().delete()
        customers = Customer.objects.all().order_by('id')
        res = test_func(customers)
        self.assertEquals((1, 3, 2), res[0])
        self.assertEquals((2, 4, 0), res[1])
        self.assertEquals((601, None, -1), res[2])
        res = test_func(customers)
        self.assertEquals((1, 3, 3), res[0])
        self.assertEquals((601, None, -2), res[1])
        res = test_func(customers)
        self.assertEquals((1, 3, 4), res[0])
        self.assertEquals((601, None, -3), res[1])
        self.assertEquals(Loan.objects.count(), 0)
        idx = 0
        cids = [1,2,5,701]
        for c in Customer.objects.all():
            if c.account_id % 2 == 0:
                c.account.accountBalance = 0
            else:
                c.account.accountBalance = 999
            if c.account_id > 500:
                c.customerType = CustomerType.COMMERCIAL.name
                c.circuit_id = cids[idx]
                idx += 1
                c.save()
            c.account.save()
        customers = Customer.objects.all().order_by('id')
        res = test_func(customers)
        self.assertEquals((1, 3, 0), res[0])
        self.assertEquals((2, 4, 1), res[1])
        self.assertEquals((601, 1, 0), res[2])
        res = test_func(customers)
        self.assertEquals((2, 4, 2), res[0])
        res = test_func(customers)
        self.assertEquals((2, 4, 3), res[0])

        self.assertEquals(Loan.objects.count(), 0)

        for c in Customer.objects.all():
            c.circuit.queen.grid.operational_date = str_to_dt('2001-01-01').date()
            c.connectionFeeLoan = True
            c.circuit.queen.grid.save()
            c.account.accountBalance = 0
            c.save()
            c.account.save()

        customers = Customer.objects.all().order_by('id')
        res = test_func(customers)
        self.assertEquals((1, 3, 1), res[0])
        self.assertEquals((2, 4, 4), res[1])
        self.assertEquals((601, 1, 1), res[2])
        res = test_func(customers)
        self.assertEquals((1, 3, 2), res[0])
        self.assertEquals((2, 4, 5), res[1])
        self.assertEquals((601, 1, 2), res[2])
        res = test_func(customers)
        self.assertEquals((1, 3, 3), res[0])
        self.assertEquals((2, 4, 6), res[1])
        self.assertEquals((601, 1, 3), res[2])

        self.assertEquals(Loan.objects.count(), 0)

        for c in Customer.objects.all():
            c.account.accountBalance = 999
            c.circuit_id = None
            c.save()
            c.account.save()

        customers = Customer.objects.all().order_by('id')
        res = test_func(customers)
        self.assertEquals((1, None, -1), res[0])
        self.assertEquals((2, None, -1), res[1])
        self.assertEquals((601, None, -1), res[2])
        res = test_func(customers)
        self.assertEquals((1, None, -2), res[0])
        self.assertEquals((2, None, -2), res[1])
        self.assertEquals((601, None, -2), res[2])
        res = test_func(customers)
        self.assertEquals((1, None, -3), res[0])
        self.assertEquals((2, None, -3), res[1])
        self.assertEquals((601, None, -3), res[2])

        self.assertEquals(Loan.objects.count(), 0)

        for c in Customer.objects.all():
            if c.account_id > 500:
                c.account.accountBalance = 0
                c.account.save()

        customers = Customer.objects.all().order_by('id')
        res = test_func(customers)
        self.assertEquals((1, None, -4), res[0])
        self.assertEquals((2, None, -4), res[1])
        self.assertEquals((601, None, -4), res[2])
        res = test_func(customers)
        self.assertEquals([(1, None, -5), (2, None, -5), (601, None, -5), (602, None, -5), (603, None, -5), (701, None, -5)], res)
        res = test_func(customers)
        self.assertEquals([(1, None, -6), (2, None, -6), (601, None, -6), (602, None, -6), (603, None, -6), (701, None, -6)], res)

        self.assertEquals(Loan.objects.count(), 0)


@ddt
class LowBalanceTestCase(BaseCustomerTestCase):
    CUR_DATE = utcnow().date
    OP = CUR_DATE - datetime.timedelta(days=10000)
    NOOP = CUR_DATE + datetime.timedelta(days=10000)
    ZERO = 0.0

    data_neg = (
        (LOW_BALANCE_WARNING_AMOUNT, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - LOW_BALANCE_WARNING_AMOUNT - 100, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, CUR_DATE, NOOP),
        (ZERO, 0, CUR_DATE, NOOP),
        (ZERO - 100, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - LOW_BALANCE_WARNING_AMOUNT - 100, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (ZERO, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (ZERO - 100, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        # cross month
        (LOW_BALANCE_WARNING_AMOUNT, 0, str_to_dt('2017-1-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - LOW_BALANCE_WARNING_AMOUNT - 100, 0, str_to_dt('2017-1-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, str_to_dt('2017-1-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, str_to_dt('2017-1-27').date(), NOOP),
        (ZERO, 0, str_to_dt('2017-1-27').date(), NOOP),
        (ZERO - 100, 0, str_to_dt('2017-1-27').date(), NOOP),
        # cross year
        (LOW_BALANCE_WARNING_AMOUNT, 0, str_to_dt('2017-12-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - LOW_BALANCE_WARNING_AMOUNT - 100, 0, str_to_dt('2017-12-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, str_to_dt('2017-12-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, str_to_dt('2017-12-27').date(), NOOP),
        (ZERO, 0, str_to_dt('2017-12-27').date(), NOOP),
        (ZERO - 100, 0, str_to_dt('2017-12-27').date(), NOOP),
        (ZERO, 1, CUR_DATE, OP),
        (ZERO - 100, 1, CUR_DATE, OP),
        (ZERO, 1, CUR_DATE + datetime.timedelta(days=6), OP),
        (ZERO - 100, 1, CUR_DATE + datetime.timedelta(days=6), OP),
        # cross month
        (ZERO, 1, str_to_dt('2017-1-27').date(), OP),
        (ZERO - 100, 1, str_to_dt('2017-1-27').date(), OP),
        # cross year
        (ZERO, 1, str_to_dt('2017-12-27').date(), OP),
        (ZERO - 100, 1, str_to_dt('2017-12-27').date(), OP),
    )

    data_low = (
        (LOW_BALANCE_WARNING_AMOUNT - 10, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, CUR_DATE, NOOP),
        (ZERO, 0, CUR_DATE, NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - 10, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        (ZERO, 0, CUR_DATE + datetime.timedelta(days=6), NOOP),
        # cross month
        (LOW_BALANCE_WARNING_AMOUNT - 10, 0, str_to_dt('2017-1-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 0, str_to_dt('2017-1-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, str_to_dt('2017-1-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, str_to_dt('2017-1-27').date(), NOOP),
        (ZERO, 0, str_to_dt('2017-1-27').date(), NOOP),
        # cross year
        (LOW_BALANCE_WARNING_AMOUNT - 10, 0, str_to_dt('2017-12-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 0, str_to_dt('2017-12-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, str_to_dt('2017-12-27').date(), NOOP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, str_to_dt('2017-12-27').date(), NOOP),
        (ZERO, 0, str_to_dt('2017-12-27').date(), NOOP),

        (LOW_BALANCE_WARNING_AMOUNT - 10, 1, CUR_DATE, OP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 1, CUR_DATE, OP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, CUR_DATE, OP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, CUR_DATE, OP),
        (LOW_BALANCE_WARNING_AMOUNT - 10, 1, CUR_DATE + datetime.timedelta(days=6), OP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 1, CUR_DATE + datetime.timedelta(days=6), OP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, CUR_DATE + datetime.timedelta(days=6), OP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, CUR_DATE + datetime.timedelta(days=6), OP),
        (ZERO, 1, CUR_DATE + datetime.timedelta(days=6), OP),
        # cross month
        (LOW_BALANCE_WARNING_AMOUNT - 10, 1, str_to_dt('2017-1-27').date(), OP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 1, str_to_dt('2017-1-27').date(), OP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, str_to_dt('2017-1-27').date(), OP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, str_to_dt('2017-1-27').date(), OP),
        (ZERO, 1, str_to_dt('2017-1-27').date(), OP),
        # cross year
        (LOW_BALANCE_WARNING_AMOUNT - 10, 1, str_to_dt('2017-12-27').date(), OP),
        (LOW_BALANCE_WARNING_AMOUNT - 5, 1, str_to_dt('2017-12-27').date(), OP),
        (LOW_BALANCE_WARNING_AMOUNT + 0.01, 0, str_to_dt('2017-12-27').date(), OP),
        (LOW_BALANCE_WARNING_AMOUNT + 5, 0, str_to_dt('2017-12-27').date(), OP),
        (ZERO, 1, str_to_dt('2017-12-27').date(), OP),
    )

    @idata(data_low)
    @unpack
    def test_low_balance_reminder(self, bal, res, test_date, op_date):
        """
        Test that low balance SMSs only get sent for balances under LOW_BALANCE_WARNING_AMOUNT
        and only get sent once a week

        :param bal:         Account balance
        :param res:         the result of the update_days_no_balance call
        :param test_date:   date update_days_no_balance call uses
        :return:
        """

        default_dt = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()
        customer = ph_model.customer.Customer.objects.get(pk=1)
        customer.account.accountBalance = bal
        customer.account.save()
        customer.save()
        if customer.circuit_id is not None:
            customer.circuit.queen.grid.operational_date = op_date
            customer.circuit.queen.grid.save()
        incr = 1
        dt = test_date

        if bal >= LOW_BALANCE_WARNING_AMOUNT or op_date > dt:
            incr = 0
            dt = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()

        if bal > 0.0 or op_date > dt:
            neg_dt = default_dt
        else:
            neg_dt = test_date
            dt = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()

        ph_model.sms.SMS.objects.all().delete()
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, neg_dt)
        self.assertEqual(ph_model.sms.SMS.objects.count(), res, 'test mode sms not really sent')
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, dt)
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=3))
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, neg_dt)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, dt)
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=6))
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, neg_dt)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, dt)
        self.assertEqual(ph_model.sms.SMS.objects.count(), res, 'test mode sms not really sent')
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=7))
        if bal <= 0.0 and op_date <= test_date + datetime.timedelta(days=7):
            neg_dt = test_date + datetime.timedelta(days=7)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, neg_dt)
        self.assertEqual(ph_model.sms.SMS.objects.count(), res + incr, 'test mode sms not really sent')
        customer = ph_model.customer.Customer.objects.get(pk=1)
        customer.account.accountBalance = LOW_BALANCE_WARNING_AMOUNT + 100
        customer.account.save()
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=7))
        self.assertEqual(ph_model.sms.SMS.objects.count(), res + incr, 'test mode sms not really sent')
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, default_dt)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, default_dt)

    @idata(data_neg)
    @unpack
    def test_neg_balance_reminder(self, bal, res, test_date, op_date):
        """
        Test that neg balance SMSs only get sent for balances under NEG_BALANCE_WARNING_AMOUNT
        and only get sent once a week

        :param bal:         Account balance
        :param res:         the result of the update_days_no_balance call
        :param test_date:   date update_days_no_balance call uses
        :return:
        """

        default_dt = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()
        customer = ph_model.customer.Customer.objects.get(pk=1)
        customer.account.accountBalance = bal
        customer.account.save()
        customer.save()
        if customer.circuit_id is not None:
            customer.circuit.queen.grid.operational_date = op_date
            customer.circuit.queen.grid.save()
        incr = 1
        dt = test_date

        if bal > 0 or op_date > dt:
            incr = 0
            dt = str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date()

        ph_model.sms.SMS.objects.all().delete()
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, default_dt)
        self.assertEqual(ph_model.sms.SMS.objects.count(), res, 'test mode sms not really sent')
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, dt)
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=3))
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, default_dt)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, dt)
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=6))
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, default_dt)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, dt)
        self.assertEqual(ph_model.sms.SMS.objects.count(), res, 'test mode sms not really sent')
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=7))
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, default_dt)
        self.assertEqual(ph_model.sms.SMS.objects.count(), res + incr, 'test mode sms not really sent')
        customer = ph_model.customer.Customer.objects.get(pk=1)
        customer.account.accountBalance = LOW_BALANCE_WARNING_AMOUNT + 100
        customer.account.save()
        update_days_no_balance([ph_model.customer.Customer.objects.get(pk=1)], test_date + datetime.timedelta(days=7))
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, default_dt)
        self.assertEqual(ph_model.sms.SMS.objects.count(), res + incr, 'test mode sms not really sent')
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_low_bal_sent, default_dt)
        self.assertEqual(ph_model.customer.Customer.objects.get(pk=1).account.sms_neg_bal_sent, default_dt)
