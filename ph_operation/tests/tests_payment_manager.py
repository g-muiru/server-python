# coding=utf-8
""" Payment manager tests """

import decimal
from django.core.management import call_command
import ph_model.models as ph_model
from ph_operation.tests import base_test
from ph_operation import payment_manager
from ph_util.loan_utils import mth_mobile_balance


class MMPBaseTest(base_test.BaseTestOperation):
    def setUp(self):
        """Create especial customer and circuit or related usage objects for the dynamic usage test from file."""
        super(MMPBaseTest, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/payment_config.json', verbosity=0)


class PaymentManagerTestCase(MMPBaseTest):
    @staticmethod
    def _send_payment_validation_request(payerPhoneNumber, billRefNumber=None,
                                         serviceProvider=ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MPESA.value,
                                         businessShortCode='1234'):
        serviceProvider = serviceProvider or ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MPESA.value
        kycInfo = {'firstName': 'Estifanos', 'middleName': 'Gebru', 'lastName': 'Gebrehiwot'}
        # noinspection PyArgumentEqualDefault
        vr = payment_manager.MpesaPaymentManager.validate_payment_request(transType='Pay bill', transId='MPESA-1',
                                                                          transTime='20150101010101', transAmount='100',
                                                                          businessShortCode=businessShortCode, msisdn=payerPhoneNumber,
                                                                          kycInfo=kycInfo, serviceProvider=serviceProvider,
                                                                          billRefNumber=billRefNumber, invoiceNumber=None,
                                                                          )

        return vr

    def test_validate_payment_request_valid_payer_mpesa(self):
        payerPhoneNumber = '+254733769589'
        billRefNumber = None
        customerAccount = ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber)
        mth_mobile_balance(100.00, customerAccount.id)
        customerPreviousBalance = customerAccount.accountBalance
        vr = self._send_payment_validation_request(payerPhoneNumber, billRefNumber=billRefNumber)
        self.assertEquals(vr.responseCode, ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.SUCCESS.value)
        self.assertEquals(ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber).accountBalance,
                          customerPreviousBalance)
        payment_manager.MpesaPaymentManager.confirm_payment_request(
            vr.transId,
            'This param not used',
            ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MPESA.value)
        self.assertEquals(ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber).accountBalance,
                          customerPreviousBalance + decimal.Decimal(100.00))

    def test_validate_payment_request_different_payer_mpesa(self):
        customerPhoneNumber = '+254733769589'
        payerPhoneNumber = '+254733769590'
        billRefNumber = '733769589'
        customerAccount = ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=customerPhoneNumber)
        mth_mobile_balance(100.00, customerAccount.id)
        customerPreviousBalance = customerAccount.accountBalance
        vr = self._send_payment_validation_request(payerPhoneNumber, billRefNumber=billRefNumber)
        self.assertEquals(vr.responseCode, ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.SUCCESS.value)
        self.assertEquals(ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=customerPhoneNumber).accountBalance,
                          customerPreviousBalance)
        payment_manager.MpesaPaymentManager.confirm_payment_request(
            vr.transId,
            'This param not used',
            ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MPESA.value)
        self.assertEquals(ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=customerPhoneNumber).accountBalance,
                          customerPreviousBalance + decimal.Decimal(100.00))

    def test_validate_payment_request_account_number_mpesa(self):
        payerPhoneNumber = '+254733769589'
        customerAccount = ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber)
        billRefNumber = str(customerAccount.id)
        mth_mobile_balance(100.00, customerAccount.id)
        customerPreviousBalance = customerAccount.accountBalance
        vr = self._send_payment_validation_request(payerPhoneNumber, billRefNumber=billRefNumber)
        self.assertEquals(vr.responseCode, ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.SUCCESS.value)
        self.assertEquals(ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber).accountBalance,
                          customerPreviousBalance)
        payment_manager.MpesaPaymentManager.confirm_payment_request(
            vr.transId,
            'This param not used',
            ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MPESA.value)
        self.assertEquals(ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber).accountBalance,
                          customerPreviousBalance + decimal.Decimal(100.00))

    def __test_validate_payment_request_valid_payer_mvola(self):  # don't want to test paying MVOLA from a Kenyan number...
        payerPhoneNumber = '+254733769589'
        serviceProvider = ph_model.mobile_payment_provider.MobilePaymentServiceProvider.MVOLA.value

        customerAccount = ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber)
        mth_mobile_balance(100.00, customerAccount.id)
        customerPreviousBalance = customerAccount.accountBalance
        vr = self._send_payment_validation_request(payerPhoneNumber, serviceProvider=serviceProvider, businessShortCode='1235')
        self.assertEquals(vr.responseCode, ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.SUCCESS.value)
        self.assertEquals(ph_model.account.Account.objects.get(phoneaccount__mobileMoneyNumber=payerPhoneNumber).accountBalance,
                          customerPreviousBalance + decimal.Decimal(100.00))

    def test_verification_payment_request_valid_payer(self):
        payerPhoneNumber = '+254733769589'
        vr = self._send_payment_validation_request(payerPhoneNumber)
        self.assertEquals(vr.status, ph_model.mobile_payment_provider.C2BPaymentValidationRequestStatus.PENDING.value)
        # transId , c2bPaymentValidationId, serviceProvider
        payment_manager.MpesaPaymentManager.confirm_payment_request(vr.transId, vr.id, 'mpesa')
        vrAfterVerification = ph_model.mobile_payment_provider.C2BPaymentValidationRequest.objects.get(id=vr.id)
        self.assertEquals(vrAfterVerification.status, ph_model.mobile_payment_provider.C2BPaymentValidationRequestStatus.VERIFIED.value)

    def test_validate_payment_request_unknown__payer(self):
        unknownPayerPhoneNumber = '+254733555555'
        ph_model.event.Event.objects.all().delete()
        vr = self._send_payment_validation_request(unknownPayerPhoneNumber)
        self.assertIsNone(vr.account)
        self.assertEquals(vr.responseCode, ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.FAILURE_UNKNOWN_PAYEE.value)
        self.assertEquals(ph_model.event.Event.objects.count(), 1)
        self.assertEquals(ph_model.event.Event.objects.all()[0].eventType.eventNumber,
                          ph_model.event.EventTypeConst.NETWORK_SERVICE_UNKNOWN_CUSTOMER_PAYMENT.value)

    def test_verification_payment_request_unknown_payer(self):
        payerPhoneNumber = '+254733769588'
        vr = self._send_payment_validation_request(payerPhoneNumber)
        self.assertEquals(vr.status, ph_model.mobile_payment_provider.C2BPaymentValidationRequestStatus.DECLINED.value)
        payment_manager.MpesaPaymentManager.confirm_payment_request(vr.transId, vr.id, 'mpesa')
        vrAfterVerification = ph_model.mobile_payment_provider.C2BPaymentValidationRequest.objects.get(id=vr.id)
        self.assertEquals(vrAfterVerification.status, ph_model.mobile_payment_provider.C2BPaymentValidationRequestStatus.DECLINED.value)
