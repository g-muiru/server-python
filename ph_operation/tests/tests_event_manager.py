# coding=utf-8
"""Test cases for events"""
import datetime
from django.core.management import call_command
import ph_model.models as ph_model
from ph_operation.tests import base_test
from ph_operation import event_manager


class EventBaseTestCase(base_test.BaseTestOperation):
    # Event types defined in ph_model/test/fixtures
    # TODO this cropped up when switching to django-node test runner
    # running entire test suite "manage.py test" this data needs to be loaded,
    # running just this test    "manage.py test ph_operation.tests.tests_event_manager" the data already exists
    if ph_model.circuit.Circuit.objects.get_or_none(pk=401) is None:
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
    EVENT_TYPE_HARDWARE = {
        'GRID': ph_model.event.EventType.objects.get(
            eventNumber=ph_model.event.EventTypeConst.GRID_FAILURE.value),
        'QUEEN': ph_model.event.EventType.objects.get(
            eventNumber=ph_model.event.EventTypeConst.QUEEN_FAILURE.value),
        'PROBE': ph_model.event.EventType.objects.get(
            eventNumber=ph_model.event.EventTypeConst.PROBE_FAILURE.value),
        'CIRCUIT': ph_model.event.EventType.objects.get(
            eventNumber=ph_model.event.EventTypeConst.CIRCUIT_FAILURE.value),
        'POWERSTATION': ph_model.event.EventType.objects.get(
            eventNumber=ph_model.event.EventTypeConst.POWERSTATION_FAILURE.value),
        'INVERTER': ph_model.event.EventType.objects.get(
            eventNumber=ph_model.event.EventTypeConst.INVERTER_FAILURE.value),
        'BATTERYBANK': ph_model.event.EventType.objects.get(
            eventNumber=ph_model.event.EventTypeConst.BATTERYBANK_FAILURE.value),
    }

    def setUp(self):
        super(EventBaseTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/event_config.json', verbosity=0)
        self.circuit = ph_model.circuit.Circuit.objects.get(pk=401)
        self.probe = ph_model.probe.Probe.objects.get(pk=401)
        self.queen = ph_model.queen.Queen.objects.get(pk=401)
        self.grid = ph_model.grid.Grid.objects.get(pk=401)
        self.powerstation = ph_model.powerstation.PowerStation.objects.get(pk=401)
        self.inverter = ph_model.inverter.Inverter.objects.get(pk=401)
        self.batteryBank = ph_model.battery.BatteryBank.objects.get(pk=401)

    def generate_event(self, event_type, hardware):
        """Generates event for testing.

        Arguments:
           event_type: Event type from self.EVENT_TYPE_HARDWARE.
           hardware: on of circuit/probe/queen/grid.

        Returns:
               event
        """
        collect_time = datetime.datetime.utcnow()
        event = ph_model.event.Event(
            eventType=self.EVENT_TYPE_HARDWARE[event_type], itemId=hardware.id, collectTime=collect_time
        )
        event.save()
        return event

    def assert_event_null(self, hardwares):
        for hardware in hardwares:
            hardware = hardware.__class__.objects.get(pk=hardware.id)
            self.assertIsNone(hardware.highestUnclearedEvent)

    def assert_event_equals(self, hardwares, event):
        for hardware in hardwares:
            hardware = hardware.__class__.objects.get(pk=hardware.id)
            self.assertEquals(hardware.highestUnclearedEvent, event)


class CircuitEventTestCase(EventBaseTestCase):
    """Tests ph_operation.event_manager.CircuitEvent."""

    def test_get_hardwares(self):
        expected = [self.circuit, self.probe, self.queen, self.grid]
        event = self.generate_event('CIRCUIT', self.circuit)
        # noinspection PyCompatibility
        self.assertItemsEqual(event_manager.CircuitEvent(event).get_hardwares(), expected)

    def test_update_hardware(self):
        hardwares = [self.circuit, self.probe, self.queen, self.grid]
        self.assert_event_null(hardwares)
        event = self.generate_event('CIRCUIT', self.circuit)
        event_manager.CircuitEvent(event).update_hardware()
        self.assert_event_equals(hardwares, event)

    def test_update_hardware_with_less_severe_event(self):
        hardwares = [self.circuit, self.probe, self.queen, self.grid]
        self.assert_event_null(hardwares)
        event = self.generate_event('CIRCUIT', self.circuit)
        event_manager.CircuitEvent(event).update_hardware()
        self.assert_event_equals(hardwares, event)

        event_manager.CircuitEvent(event).update_hardware()
        # events on hardware not updated by newer event
        self.assert_event_equals(hardwares, event)


class QueenNotifyTestCase(EventBaseTestCase):
    """Tests ph_operation.event_manager.notify_event."""

    def test_notify_event(self):
        event = self.generate_event('PROBE', self.probe)
        # Make sure calling sending email doesn't fail.
        event_manager.notify_event(event)
