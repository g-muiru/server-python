import ph_model.models as ph_model
from ph_operation import  circuit_analyzer
from ph_operation.tests import base_test

ENABLED = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
DISABLED = ph_model.circuit.SwitchEnabledStatus.DISABLED.value
UNKNOWN = ph_model.circuit.SwitchEnabledStatus.UNKNOWN.value

class CircuitAnalyzerTestCaseDetectFault(base_test.BaseTestOperation):

    def test_fault_detection(self):
        circuit = ph_model.circuit.Circuit.objects.get(pk=1)
        self.assertIsNotNone(circuit)
        self.assertEqual(circuit.switchEnabled, UNKNOWN)
        circuit.switchEnabled = DISABLED
        self.assertEqual(circuit.switchEnabled, DISABLED)
        ca = circuit_analyzer.CircuitAnalyzer()
        self.assertEqual(ca.detect_fault(circuit, 4), circuit_analyzer.DetectFaultStatus.NORMAL.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.RAISED_EVENT.value)
        self.assertEqual(ca.detect_fault(circuit, 25), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_groundfault(circuit, 10, 10), circuit_analyzer.DetectFaultStatus.NORMAL.value)
        self.assertEqual(ca.detect_groundfault(circuit, 20, 20), circuit_analyzer.DetectFaultStatus.NORMAL.value)
        self.assertEqual(ca.detect_groundfault(circuit, 10, 45), circuit_analyzer.DetectFaultStatus.NORMAL.value)
        self.assertEqual(ca.detect_groundfault(circuit, 1000, 1035), circuit_analyzer.DetectFaultStatus.NORMAL.value)
        self.assertEqual(ca.detect_groundfault(circuit, 20, 100), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_groundfault(circuit, 20, 100), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_groundfault(circuit, 20, 100), circuit_analyzer.DetectFaultStatus.RAISED_EVENT.value)
        self.assertEqual(ca.detect_groundfault(circuit, 20, 100), circuit_analyzer.DetectFaultStatus.FAULT.value)

    def test_meter_issue_detection(self):
        circuit = ph_model.circuit.Circuit.objects.get(pk=1)
        self.assertIsNotNone(circuit)
        ca = circuit_analyzer.CircuitAnalyzer()
        self.assertEqual(ca.detect_meter_issue(circuit, 1, 0), circuit_analyzer.DetectFaultStatus.NORMAL.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.FAULT.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.RAISED_EVENT.value)
        self.assertEqual(ca.detect_meter_issue(circuit, 4, 0), circuit_analyzer.DetectFaultStatus.FAULT.value)

