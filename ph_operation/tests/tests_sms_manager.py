# coding=utf-8
"""Test sms functionality"""
from django.core.management import call_command
import mock
import random
import uuid
import ph_model.models as ph_model
from ph_operation.tests import base_test
from ph_operation import sms_manager
from django.conf import settings


def get_rand_string(digits=10):
    """Generates 10 digit random numbers as string. Uses double randomizing uuid 32 byte then randomize to 10 byte"""
    return "".join(random.sample(list(str(uuid.uuid4().int)), digits))


# noinspection PyMissingOrEmptyDocstring,PyMissingOrEmptyDocstring,PyMissingOrEmptyDocstring,PyPep8Naming,PyPep8Naming,PyPep8Naming
# noinspection PyUnusedLocal,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
class MockAfricasTalkingGateWayBalRequest(mock.Mock):
    called = False

    @staticmethod
    def sendMessage(to, message, from_=None):
        return [
            {"number": "+2544444444444",
             "cost": 1.5,
             "status": "Success",
             "messageId": get_rand_string()
             }
        ]

    def fetchMessages(self, lastInboundSMSId):
        if self.called:
            return []
        self.called = True
        return [{"from": "+2545555555", "linkId": "0", "to": "22870",
                 "text": "balance.4444444444", "date": "2013-11-07T09:25:27.000",
                 "id": 2127748}]


# noinspection PyMissingOrEmptyDocstring,PyMissingOrEmptyDocstring,PyMissingOrEmptyDocstring,PyPep8Naming,PyPep8Naming
# noinspection PyPep8Naming,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
class MockAfricasTalkingGateWayBalRequestWithUnknownCustomerNumberSMS(mock.Mock):
    called = False

    @staticmethod
    def sendMessage(to, message, from_=None):
        return [
            {"number": "+3232323232",
             "cost": 1.5,
             "status": "Success",
             "messageId": get_rand_string()
             }
        ]

    def fetchMessages(self, lastInboundSMSId):
        if self.called:
            return []
        self.called = True
        return [{"from": "+34343434343", "linkId": "0", "to": "22870",
                 "text": "balance.4342432443", "date": "2013-11-07T09:25:27.000",
                 "id": 2127748}]


# noinspection PyMissingOrEmptyDocstring,PyMissingOrEmptyDocstring,PyMissingOrEmptyDocstring,PyPep8Naming,PyPep8Naming
# noinspection PyPep8Naming,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
class MockAfricasTalkingGateWayScratchcard(mock.Mock):
    called = False

    @staticmethod
    def sendMessage(to, message, from_=None):
        return [
            {"number": "+254733769589",
             "cost": 1.5,
             "status": "Success",
             "messageId": get_rand_string()
             }
        ]

    def fetchMessages(self, lastInboundSMSId):
        if self.called:
            return []
        self.called = True
        return [{"from": "+254733769589", "linkId": "0", "to": "22870",
                 "text": "#spay#KEN123456789#", "date": "2013-11-07T09:25:27.000",
                 "id": 2127748}]


class BaseSMSManagerTestCase(base_test.BaseTestOperation):
    """Base test case for sms_manager module."""

    def setUp(self):
        # Load test data
        super(BaseSMSManagerTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/sms_config.json', verbosity=0)

        self.grid = ph_model.grid.Grid.objects.get(pk=500)
        self.queen = ph_model.queen.Queen.objects.get(pk=500)
        self.probe = ph_model.probe.Probe.objects.get(pk=500)
        self.circuit = ph_model.circuit.Circuit.objects.get(pk=500)
        self.customer = ph_model.customer.Customer.objects.get(pk=500)
        self.testMessage = 'Test message'
        self.dailySMSBalRequested = self.customer.dailySMSBalRequested


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class GridSMSManagerTestCase(BaseSMSManagerTestCase):
    """Tests sms_manager.GridSMSManager."""

    def test_get_recipients_phone(self):
        smsManager = sms_manager.GridSMSManager(500, self.testMessage, shortCode='0000')
        self.assertEquals(str(self.customer.phoneNumber), smsManager.to)

    @mock.patch("ph_ext_lib.AfricasTalkingGateway.AfricasTalkingGateway", MockAfricasTalkingGateWayBalRequest)
    def test_send_disable_sms(self):
        ph_model.sms.SMS.objects.all().delete()
        settings.DISABLE_SMS = True
        smsManager = sms_manager.GridSMSManager(500, self.testMessage, shortCode='0000')
        smsManager.send()
        settings.DISABLE_SMS = False
        res = ph_model.sms.SMS.objects.count()
        settings.DISABLE_SMS = True
        self.assertEquals(1, res)

    @mock.patch("ph_ext_lib.AfricasTalkingGateway.AfricasTalkingGateway", MockAfricasTalkingGateWayBalRequest)
    def test_send(self):
        settings.DISABLE_SMS = False
        ph_model.sms.SMS.objects.all().delete()
        smsManager = sms_manager.GridSMSManager(500, self.testMessage, shortCode='0000')
        smsManager.send()
        res = ph_model.sms.SMS.objects.count()
        settings.DISABLE_SMS = True
        self.assertEquals(1, res)

    @mock.patch("ph_ext_lib.AfricasTalkingGateway.AfricasTalkingGateway", MockAfricasTalkingGateWayBalRequest)
    def test_sync_incoming_sms_message(self):
        ph_model.sms.SMS.objects.all().delete()
        # TODO(estifanos): Change this to name with unique identifier e.g. PH-1
        phAccount = ph_model.sms.PowerhiveSMSAccount.objects.get(id=500)
        phAccountLastUpdated = phAccount.updated

        sms_manager.GridSMSManager.fetch_incoming_sms_message(shortCode='0000')

        # Since message type is account enquiry sms should have two counts one when fetching the other responding.
        self.assertEquals(2, ph_model.sms.SMS.objects.count())

        # sms.PowerhiveSMSAccount. should be updated with with new lastInboundSMSId after fetching incoming message.
        self.assertNotEquals(ph_model.sms.PowerhiveSMSAccount.objects.get(id=500).updated, phAccountLastUpdated)

    @mock.patch("ph_ext_lib.AfricasTalkingGateway.AfricasTalkingGateway", MockAfricasTalkingGateWayBalRequest)
    def test_sync_incoming_sms_message_new_customer(self):
        settings.DISABLE_SMS = False
        ph_model.sms.SMS.objects.all().delete()
        self.customer.status = "POTENTIAL"
        self.customer.circuit = None
        self.customer.save()
        # TODO(estifanos): Change this to name with unique identifier e.g. PH-1
        phAccount = ph_model.sms.PowerhiveSMSAccount.objects.get(id=500)
        phAccountLastUpdated = phAccount.updated

        sms_manager.CustomerSMSManager.fetch_incoming_sms_message(shortCode='0000')

        # Since message type is account enquiry sms should have two counts one when fetching the other responding.
        res = ph_model.sms.SMS.objects.count()
        settings.DISABLE_SMS = True
        self.assertEquals(1, res)

        # sms.PowerhiveSMSAccount. should be updated with with new lastInboundSMSId after fetching incoming message.
        self.assertNotEquals(ph_model.sms.PowerhiveSMSAccount.objects.get(id=500).updated, phAccountLastUpdated)

    @mock.patch("ph_ext_lib.AfricasTalkingGateway.AfricasTalkingGateway", MockAfricasTalkingGateWayBalRequestWithUnknownCustomerNumberSMS)
    def test_sync_incoming_sms_message(self):
        ph_model.sms.SMS.objects.all().delete()
        ph_model.event.Event.objects.all().delete()
        # TODO(estifanos): Change this to name with unique identifier e.g. PH-1
        phAccount = ph_model.sms.PowerhiveSMSAccount.objects.get(id=500)
        phAccountLastUpdated = phAccount.updated

        sms_manager.GridSMSManager.fetch_incoming_sms_message(shortCode='0000')
        # Since Unnkown number is responded there shouldn't be any sms persisted rather event raised
        self.assertEquals(0, ph_model.sms.SMS.objects.all().count())
        self.assertEqual(1, ph_model.event.Event.objects.all().count())
        # sms.PowerhiveSMSAccount. should be updated with with new lastInboundSMSId after fetching incoming message.
        self.assertNotEquals(ph_model.sms.PowerhiveSMSAccount.objects.get(id=500).updated, phAccountLastUpdated)

    @mock.patch("ph_ext_lib.AfricasTalkingGateway.AfricasTalkingGateway", MockAfricasTalkingGateWayBalRequest)
    def test_sync_incoming_sms_bal_request_daily_limit(self):
        self.assertEqual(ph_model.customer.Customer.objects.get(id=self.customer.id).dailySMSBalRequested, 0)
        sms_manager.GridSMSManager.fetch_incoming_sms_message(shortCode='0000')
        self.assertEqual(ph_model.customer.Customer.objects.get(id=self.customer.id).dailySMSBalRequested, 1)


# noinspection PyPep8Naming
class QueenSMSManagerTestCase(BaseSMSManagerTestCase):
    """Tests sms_manager.QueenSMSManager."""

    def test_get_recipients_phone(self):
        smsManager = sms_manager.QueenSMSManager(500, self.testMessage, shortCode='0000')
        self.assertEquals(str(self.customer.phoneNumber), smsManager.to)


# noinspection PyPep8Naming
class CircuitSMSManagerTestCase(BaseSMSManagerTestCase):
    """Tests sms_manager.CircuitSMSManager."""

    def test_get_recipients_phone(self):
        smsManager = sms_manager.CircuitSMSManager(500, self.testMessage, shortCode='0000')
        self.assertEquals(str(self.customer.phoneNumber), smsManager.to)


# noinspection PyPep8Naming
class CustomerSMSManagerTestCase(BaseSMSManagerTestCase):
    """Tests sms_manager.CustomerSMSManager."""

    def test_get_recipients_phone(self):
        smsManager = sms_manager.CustomerSMSManager(500, self.testMessage, shortCode='0000')
        self.assertEquals(str(self.customer.phoneNumber), smsManager.to)


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class IncomingSMSTestCase(BaseSMSManagerTestCase):
    def setUp(self):
        # Load test data
        super(BaseSMSManagerTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/sms_config.json', verbosity=0)
        self.customer = ph_model.customer.Customer.objects.get(pk=500)
        self.customerPhone = self.customer.phoneNumber
        self.nonCustomerPhone = "+2547000000000"
        self.balanceInquiryMessage = 'Balance.{}'.format("4444444444")

    def assertSMSFormat(self, incomingSMS, senderPhone, smsType, intendedCustomersPhone, customer):
        self.assertEquals(incomingSMS.senderPhone, senderPhone)
        self.assertEquals(incomingSMS.type, smsType)
        self.assertEqual(incomingSMS.intendedCustomerNumbers, intendedCustomersPhone)
        self.assertEqual(incomingSMS.customer, customer)

    def test_initialization(self):
        balanceReqMessages = ['Balance.4444444444', 'BALANCE.4444444444', 'BALANCE.04444444444', 'BALANCE 04444444444',
                              'balance %d' % self.customer.account.id]
        for messages in balanceReqMessages:
            incomingSMS = sms_manager.IncomingSMSFactory(messages, self.nonCustomerPhone).incomingSMS
            self.assertSMSFormat(incomingSMS, self.nonCustomerPhone, ph_model.sms.SMSType.BALANCE_INQUIRY.value,
                                 [str(self.customerPhone), str(self.nonCustomerPhone)], self.customer)


class ScratchcardPaymentIncomingSMSTestCase(BaseSMSManagerTestCase):
    def setUp(self):
        # Load test data
        super(ScratchcardPaymentIncomingSMSTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/scratchcard_config/scratchcard_config.json', verbosity=0)
        self.customer = ph_model.customer.Customer.objects.get(pk=101)
        self.customerPhone = self.customer.phoneNumber
        self.customerBalance = self.customer.account.accountBalance

    @mock.patch("ph_ext_lib.AfricasTalkingGateway.AfricasTalkingGateway", MockAfricasTalkingGateWayScratchcard)
    def test_sync_incoming_sms_message(self):
        #TODO: fix test   AssertionError: Decimal('0E-20') != Decimal('100.00000000000000000000')
        pass
        return
        sms_manager.GridSMSManager.fetch_incoming_sms_message(shortCode='0000')
        self.assertEquals(ph_model.customer.Customer.objects.get(pk=101).account.accountBalance, self.customerBalance + 100)
