# coding=utf-8
"""Tests for the loan manager"""
# TODO 2017-08-19 we do not charge the commercial fixed fee monthly anymore ---
#     once we are sure this will not come back, test_monthly_tariff_loan  can be removed
# TODO ad tests for loans with interest
from datetime import timedelta
from decimal import Decimal

from django.core.management import call_command
from django.db.models import Q
from django.db import connection
from ph_model.models import Account, fields
from ph_model.models import Loan
from ph_model.models import LoanPaymentHistory
from ph_model.models.customer import CustomerType, Customer
from ph_model.models.municipality import Municipality
from ph_model.models.region import Region
from ph_model.models.transaction import MasterTransactionHistory, update_mth_balances
from ph_operation import loan_manager, transaction_manager
from ph_operation.loan_manager import LoanManager
from ph_operation.tests import base_test
import mock

from ph_script import monthly_commercial_tariff_fixed_fee
from ph_util import date_util
from ph_util.accounting_util import AccountingEnums, calc_vat, add_vat, LoanType, RESIDENTIAL_CONN_LOAN_AMT, \
    COMMERCIAL_CONN_LOAN_AMT, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT, \
    KUKU_POA_SUBSCRIPTION_5_DAYS, LoanRepayment, PaymentEnums
from ph_util.date_util import str_to_dt, DELTA_365_DAYS, DELTA_30_DAYS
from ddt import ddt, unpack, idata
from ph_util.loan_utils import create_loan, create_payment_plan_no_vat
from ph_util.test_utils import assert_eq, quant


class PaymentsTestCase(base_test.BaseTestOperation):
    """
    Setup data
    """

    def setUp(self):
        """
        Setup data
        :return:
        """
        super(PaymentsTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/loan_config.json', verbosity=0)
        LoanPaymentHistory.objects.all().delete()
        Loan.objects.filter(~Q(pk=601)).delete()
        cust = Customer.objects.get(id=601)
        cust.firstName = 'Steve'
        cust.lastName = 'Testers'
        cust.save()


# noinspection PyUnusedLocal
@ddt
class LoanConnectionTestCase(PaymentsTestCase):
    """Tests various payments."""

    data = (
        (601, str_to_dt('2017-01-21'), 10000.0, 0, 0, 0, 10010.0, 0, 0, 0, 'Bad pt'),
        (601, str_to_dt('2017-01-21'), 10000.0, 0, 0, 0, 10010.0, 0, 0, 0, PaymentEnums.ACCOUNT.name),
        (601, str_to_dt('2017-01-21'), 10000.0, 0, 0, 0, 10010.0, 0, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (601, str_to_dt('2017-01-21'), 10000.0, 0, 0, 0, 10010.0, 0, 0, 0, PaymentEnums.CLEARING.name),
        (601, str_to_dt('2017-01-21'), 10000.0, 0, 0, 0, 10010.0, 0, 0, 0, PaymentEnums.RECONNECT.name),
        (601, str_to_dt('2017-01-21'), 100.0, -10, -20, -30, 110.0, -10, -20, -30, 'Bad pt'),
        (601, str_to_dt('2017-01-21'), 100.0, -10, -20, -30, 110.0, -10, -20, -30, PaymentEnums.ACCOUNT.name),
        (601, str_to_dt('2017-01-21'), 100.0, -10, -20, -30, 100.0, 0, -20, -30, PaymentEnums.UNCOLLECTED.name),
        (601, str_to_dt('2017-01-21'), 100.0, -10, -20, -30, 90.0, -10, 0, -30, PaymentEnums.CLEARING.name),
        (601, str_to_dt('2017-01-21'), 100.0, -10, -20, -30, 80.0, -10, -20, 0, PaymentEnums.RECONNECT.name),
        (601, str_to_dt('2017-01-21'), 100.0, -110, -120, -130, 110.0, -110, -120, -130, 'Bad pt'),
        (601, str_to_dt('2017-01-21'), 100.0, -110, -120, -130, 110.0, -110, -120, -130, PaymentEnums.ACCOUNT.name),
        (601, str_to_dt('2017-01-21'), 100.0, -110, -120, -130, 10.0, -10, -120, -130, PaymentEnums.UNCOLLECTED.name),
        (601, str_to_dt('2017-01-21'), 100.0, -110, -120, -130, 10.0, -110, -20, -130, PaymentEnums.CLEARING.name),
        (601, str_to_dt('2017-01-21'), 100.0, -110, -120, -130, 10.0, -110, -120, -30, PaymentEnums.RECONNECT.name),
        (601, str_to_dt('2017-01-21'), 1000.0, -110, -120, -130, 1010.0, -110, -120, -130, 'Bad pt'),
        (601, str_to_dt('2017-01-21'), 1000.0, -110, -120, -130, 1010.0, -110, -120, -130, PaymentEnums.ACCOUNT.name),
        (601, str_to_dt('2017-01-21'), 1000.0, -110, -120, -130, 900.0, 0, -120, -130, PaymentEnums.UNCOLLECTED.name),
        (601, str_to_dt('2017-01-21'), 1000.0, -110, -120, -130, 890.0, -110, 0, -130, PaymentEnums.CLEARING.name),
        (601, str_to_dt('2017-01-21'), 1000.0, -110, -120, -130, 880.0, -110, -120, 0, PaymentEnums.RECONNECT.name),
    )

    # noinspection PyUnreachableCode
    @idata(data)
    @unpack
    def test_repay_conn_fee_loan(self, acc_cust_id, now, amt, starting_uncol, starting_clearing, starting_recon,
                                 expected_bal, expected_uncol, expected_clearing, expected_recon, pt):
        """
        Make sure payments get credited to the right balance
        Starts customer with a balance of 10 Ksh

        :param acc_cust_id:
        :param now:
        :param amt:
        :param starting_uncol:
        :param starting_clearing:
        :param starting_recon:
        :param expected_bal:
        :param expected_uncol:
        :param expected_clearing:
        :param expected_recon:
        :param pt:
        :return:
        """

        MasterTransactionHistory.objects.all().delete()
        cust = Customer.objects.get(id=acc_cust_id)

        transaction_manager.TransactionManager. \
            process_mobile_payment(cust.account, ('trans-' + str(acc_cust_id)), 10.0, str_to_dt('2016-03-02T01:01:00Z'))
        res = transaction_manager.TransactionManager.send_payment_sms(update_mth_balances()[3])
        self.assertTrue('Balance: 10' in res[0][0].message)
        self.assertTrue('for 10' in res[0][0].message)
        assert_eq(1, MasterTransactionHistory.objects.count())

        print('')
        for m in MasterTransactionHistory.objects.all():
            print('mth', m.id, m.processed, m.accountBalance, m.uncollected_bal, m.clearing_bal, m.reconnect_bal)
            m.uncollected_bal = starting_uncol
            m.clearing_bal = starting_clearing
            m.reconnect_bal = starting_recon
            m.save()

        transaction_manager.TransactionManager. \
            process_mobile_payment(cust.account, ('trans1-' + str(acc_cust_id)), amt, str_to_dt('2016-03-02T02:01:00Z'), pt)
        res1 = transaction_manager.TransactionManager.send_payment_sms(update_mth_balances()[3])

        assert_eq(2, MasterTransactionHistory.objects.count())
        for m in MasterTransactionHistory.objects.all().order_by('-pk'):
            print('mth', m.id, m.processed, m.accountBalance, m.uncollected_bal, m.clearing_bal, m.reconnect_bal)
            assert_eq(expected_bal, m.accountBalance)
            assert_eq(expected_uncol, m.uncollected_bal)
            assert_eq(expected_clearing, m.clearing_bal)
            assert_eq(expected_recon, m.reconnect_bal)
            break

        a = Customer.objects.get(id=acc_cust_id).account
        self.assertTrue('Balance: ' + str(int(expected_bal)) in res1[0][0].message)
        self.assertTrue('for ' + str(int(amt)) in res1[0][0].message)
        assert_eq(expected_bal, a.accountBalance)
        assert_eq(expected_uncol, a.uncollected_bal)
        assert_eq(expected_clearing, a.clearing_bal)
        assert_eq(expected_recon, a.reconnect_bal)
