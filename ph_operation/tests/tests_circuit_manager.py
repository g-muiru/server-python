# coding=utf-8
""" test circuit manager functionality"""
from django.core.management import call_command
import ph_model.models as ph_model
from ph_operation import circuit_manager
from ph_operation.tests import base_test
from ddt import ddt, unpack, idata

ENABLED = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
DISABLED = ph_model.circuit.SwitchEnabledStatus.DISABLED.value


class BaseCircuitTestCase(base_test.BaseTestOperation):
    """Base test case form circuit_manager module."""

    def setUp(self):
        # Load test data circuit is defaulted to default value.
        super(BaseCircuitTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/circuit_config.json', verbosity=0)

        self.account = ph_model.account.Account.objects.get(pk=102)
        self.accountRule = ph_model.account.AccountRule.objects.get(pk=102)
        self.circuit = ph_model.circuit.Circuit.objects.get(pk=102)
        self.circuit100 = ph_model.circuit.Circuit.objects.get(pk=100)
        self.circuit101 = ph_model.circuit.Circuit.objects.get(pk=101)
        self.customer = ph_model.customer.Customer.objects.get(pk=102)
        self.usage = ph_model.usage.Usage.objects.get(pk=102)
        self.monitor = ph_model.monitor.CircuitMonitor.objects.get(pk=102)


class CircuitManagerTestCaseConfigureByBalance(BaseCircuitTestCase):
    """Tests circuit_manger.Circuit."""

    # noinspection PyPep8Naming
    def _validate(self, customer, circuit, account, accountRule):
        self.assertEquals(self.customer, customer)
        self.assertEquals(self.account, account)
        self.assertEquals(self.accountRule, accountRule)
        self.assertEquals(self.circuit, circuit)

    def test_init(self):
        circuit = circuit_manager.BalanceUpdateConfigCircuit(self.circuit)
        self._validate(circuit.customer, circuit.circuit, circuit.account, circuit.account_rule)

    def test_configure_circuit_after_account_change_with_insufficient_balance_with_prior_enabled_value(self):
        self.account.accountBalance = self.account.accountRule.minBalance - 1
        self.account.save()
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        circuit_manager.CircuitManager.configure_circuit_after_account_adjustment(self.account)
        self.assertFalse(ph_model.circuit.SwitchEnabledHistory.objects.get(circuit=self.circuit).enableBalanceServer)

    def test_configure_circuit_after_account_change_with_sufficient_balance_with_no_prior_disabled_request_and_none_value(self):
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        self.assertIsNone(self.circuit.server_request_enabled)
        self.account.accountBalance = self.account.accountRule.minBalance + 1
        self.account.save()
        circuit_manager.CircuitManager.configure_circuit_after_account_adjustment(self.account)
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.count(), 1)
        self.assertEquals(None, self.circuit.server_request_enabled)

    def test_configure_circuit_after_account_change_with_insufficient_balance_with_prior_enabled_value_negative(self):
        self.account.accountRule.minBalance = -100
        self.account.accountRule.save()
        self.account.accountBalance = self.account.accountRule.minBalance - 1
        self.account.save()
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        circuit_manager.CircuitManager.configure_circuit_after_account_adjustment(self.account)
        self.assertFalse(ph_model.circuit.SwitchEnabledHistory.objects.get(circuit=self.circuit).enableBalanceServer)

    def test_configure_circuit_after_account_change_with_sufficient_balance_with_no_prior_disabled_request_and_none_value_negative(self):
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        self.assertIsNone(self.circuit.server_request_enabled)
        self.account.accountRule.minBalance = -100
        self.account.accountRule.save()
        self.account.accountBalance = self.account.accountRule.minBalance + 1
        self.account.save()
        circuit_manager.CircuitManager.configure_circuit_after_account_adjustment(self.account)
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.count(), 1)
        self.assertEquals(None, self.circuit.server_request_enabled)

class CircuitManagerTestCaseConfigureByCircuit(BaseCircuitTestCase):
    """Tests circuit_manger.Circuit."""

    def test_init(self):
        circuit = circuit_manager.CircuitMonitorConfigCircuit(self.monitor)
        self.assertEquals(self.circuit.id, circuit.circuit.id)
        self.assertEquals(ph_model.circuit.Circuit.objects.get(id=self.circuit.id).switchEnabled,
                          self.monitor.switchEnabled)

    def test_configure_circuit_when_circuit_monitor_data_not_changed(self):
        self.monitor.switchEnabled = self.circuit.switchEnabled
        self.monitor.disableVaLimit = self.circuit.disableVaLimitQueen
        self.monitor.save()
        monitor = ph_model.monitor.CircuitMonitor.objects.get(id=self.monitor.id)
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        circuit_manager.CircuitManager.configure_circuits_by_circuit_monitor(monitor)
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.count(), 0)

    def test_configure_circuit_when_va_disable_limit_entry_changed(self):
        self.monitor.disableVaLimit = not self.circuit.disableVaLimitQueen
        self.monitor.save()
        monitor = ph_model.monitor.CircuitMonitor.objects.get(id=self.monitor.id)
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        circuit_manager.CircuitManager.configure_circuits_by_circuit_monitor(monitor)
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.count(), 1)

    def test_configure_circuit_when_switch_enabled_entry_changed(self):
        self.monitor.switchEnabled = not self.circuit.switchEnabled
        self.monitor.save()
        monitor = ph_model.monitor.CircuitMonitor.objects.get(id=self.monitor.id)
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        circuit_manager.CircuitManager.configure_circuits_by_circuit_monitor(monitor)
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.count(), 1)


class CircuitManagerTestCaseOverrideSwitchEnabledConfiguredCircuit(BaseCircuitTestCase):
    """Tests circuit_manger.Circuit.override_switch_enabled"""

    def test_configure_circuit_when_override_switch_enabled_not_changed(self):
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        circuit_manager.CircuitManager.override_switch_enabled(self.circuit, self.circuit.overrideSwitchEnabled)
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.count(), 0)

    def test_configure_circuit_when_override_switch_enabled_changed(self):
        ph_model.circuit.SwitchEnabledHistory.objects.all().delete()
        circuit_manager.CircuitManager.override_switch_enabled(self.circuit, not self.circuit.overrideSwitchEnabled)
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.count(), 1)


@ddt
class CircuitManagerTestCaseClearOverrides(BaseCircuitTestCase):
    data = (
        (True, True, True, False, False, False),
        (False, True, True, False, False, False),
        (True, False, True, False, False, False),
        (True, True, False, False, False, False),
        (False, False, True, False, False, False),
        (True, False, False, False, False, False),
        (False, False, False, False, False, False),

        (True, True, True, False, False, False, 'CIRCUIT_TESTER_QUEEN_100'),
        (False, True, True, False, False, False, 'CIRCUIT_TESTER_QUEEN_100'),
        (True, False, True, False, False, False, 'CIRCUIT_TESTER_QUEEN_100'),
        (True, True, False, False, False, False, 'CIRCUIT_TESTER_QUEEN_100'),
        (False, False, True, False, False, False, 'CIRCUIT_TESTER_QUEEN_100'),
        (True, False, False, False, False, False, 'CIRCUIT_TESTER_QUEEN_100'),
        (False, False, False, False, False, False, 'CIRCUIT_TESTER_QUEEN_100'),

        (True, True, True, False, False, False, ['QUEEN-DOES-NOT-EXIST']),
        (False, True, True, False, False, False, ['QUEEN-DOES-NOT-EXIST']),
        (True, False, True, False, False, False, ['QUEEN-DOES-NOT-EXIST']),
        (True, True, False, False, False, False, ['QUEEN-DOES-NOT-EXIST']),
        (False, False, True, False, False, False, ['QUEEN-DOES-NOT-EXIST']),
        (True, False, False, False, False, False, ['QUEEN-DOES-NOT-EXIST']),
        (False, False, False, False, False, False, ['QUEEN-DOES-NOT-EXIST']),

        (True, True, True, False, True, False, ['QUEEN-DOES-NOT-EXIST','CIRCUIT_TESTER_QUEEN_100',7]),
        (False, True, True, False, True, False, ['QUEEN-DOES-NOT-EXIST','CIRCUIT_TESTER_QUEEN_100',7]),
        (True, False, True, False, True, False, ['QUEEN-DOES-NOT-EXIST','CIRCUIT_TESTER_QUEEN_100',7]),
        (True, True, False, False, True, False, ['QUEEN-DOES-NOT-EXIST','CIRCUIT_TESTER_QUEEN_100',7]),
        (False, False, True, False, True, False, ['QUEEN-DOES-NOT-EXIST','CIRCUIT_TESTER_QUEEN_100',7]),
        (True, False, False, False, True, False, ['QUEEN-DOES-NOT-EXIST','CIRCUIT_TESTER_QUEEN_100',7]),
        (False, False, False, False, True, False, ['QUEEN-DOES-NOT-EXIST','CIRCUIT_TESTER_QUEEN_100',7]),

        (True, True, True, False, True, False, ['CIRCUIT_TESTER_QUEEN_100']),
        (False, True, True, False, True, False, ['CIRCUIT_TESTER_QUEEN_100']),
        (True, False, True, False, True, False, ['CIRCUIT_TESTER_QUEEN_100']),
        (True, True, False, False, True, False, ['CIRCUIT_TESTER_QUEEN_100']),
        (False, False, True, False, True, False, ['CIRCUIT_TESTER_QUEEN_100']),
        (True, False, False, False, True, False, ['CIRCUIT_TESTER_QUEEN_100']),
        (False, False, False, False, True, False, ['CIRCUIT_TESTER_QUEEN_100']),

        (True, True, True, False, False, True, ['CIRCUIT_TESTER_QUEEN_101']),
        (False, True, True, False, False, True, ['CIRCUIT_TESTER_QUEEN_101']),
        (True, False, True, False, False, True, ['CIRCUIT_TESTER_QUEEN_101']),
        (True, True, False, False, False, True, ['CIRCUIT_TESTER_QUEEN_101']),
        (False, False, True, False, False, True, ['CIRCUIT_TESTER_QUEEN_101']),
        (True, False, False, False, False, True, ['CIRCUIT_TESTER_QUEEN_101']),
        (False, False, False, False, False, True, ['CIRCUIT_TESTER_QUEEN_101']),

        (True, True, True, True, False, False, ['ESPECIALQUEEN']),
        (False, True, True, True, False, False, ['ESPECIALQUEEN']),
        (True, False, True, True, False, False, ['ESPECIALQUEEN']),
        (True, True, False, True, False, False, ['ESPECIALQUEEN']),
        (False, False, True, True, False, False, ['ESPECIALQUEEN']),
        (True, False, False, True, False, False, ['ESPECIALQUEEN']),
        (False, False, False, True, False, False, ['ESPECIALQUEEN']),

        (True, True, True, False, True, True, ['CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (False, True, True, False, True, True, ['CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (True, False, True, False, True, True, ['CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (True, True, False, False, True, True, ['CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (False, False, True, False, True, True, ['CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (True, False, False, False, True, True, ['CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (False, False, False, False, True, True, ['CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),

        (True, True, True, True, True, True, ['ESPECIALQUEEN', 'CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (False, True, True, True, True, True, ['ESPECIALQUEEN', 'CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (True, False, True, True, True, True, ['ESPECIALQUEEN', 'CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (True, True, False, True, True, True, ['ESPECIALQUEEN', 'CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (False, False, True, True, True, True, ['ESPECIALQUEEN', 'CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (True, False, False, True, True, True, ['ESPECIALQUEEN', 'CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
        (False, False, False, True, True, True, ['ESPECIALQUEEN', 'CIRCUIT_TESTER_QUEEN_100', 'CIRCUIT_TESTER_QUEEN_101']),
    )

    @idata(data)
    @unpack
    def test_override_clears(self, sw, sw100, sw101, expect, expect100, expect101, queen_list=None):
        self.circuit.overrideSwitchEnabled = sw
        self.circuit100.overrideSwitchEnabled = sw100
        self.circuit101.overrideSwitchEnabled = sw101
        self.circuit.save()
        self.circuit100.save()
        self.circuit101.save()
        circuit_manager.CircuitManager.set_clear_circuit_overrides(queen_list)
        self.circuit.refresh_from_db()
        self.circuit100.refresh_from_db()
        self.circuit101.refresh_from_db()
        self.assertEquals(self.circuit.overrideSwitchEnabled, expect)
        self.assertEquals(self.circuit100.overrideSwitchEnabled, expect100)
        self.assertEquals(self.circuit101.overrideSwitchEnabled, expect101)
