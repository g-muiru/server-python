# coding=utf-8
""" Test airtel """
import datetime
import decimal
import pytz
from django.core.management import call_command
import ph_model.models as ph_model
from ph_model.models.transaction import MasterTransactionHistory
from ph_operation.tests import base_test
from ph_operation import airtel_payment_manager
import mock

from ph_util.loan_utils import mth_mobile_balance

SERVICE_PROVIDER = airtel_payment_manager.AIRTEL
TEST_USER = 'AIRTEL_USER'
TEST_PASS = 'AIRTE_PASS'
FROM = 20140101010101
TO = 20140102010101
ACCOUNT = 101
PHONE_ACCOUNT = 101
PHONE_ACCOUNT_MOBILE_NUMBER = '254733769589'


class AirtelBaseTest(base_test.BaseTestOperation):
    def setUp(self):
        """Create especial customer and circuit or related usage objects for the dynamic usage test from file."""
        super(AirtelBaseTest, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/payment_config.json', verbosity=0)


# noinspection PyUnusedLocal
def airtel_success_response_account_number(cls):
    """

    :param cls:
    :return:
    """
    mockResponse = mock.Mock()
    mockResponse.Status = 0
    mockResponse.TotalTransactions = 2
    mockResponse.TotalAmount = 100.0
    # # is unicode character airtel uses to separate transactions
    mockResponse.Transactions = u'[2541000000000#1#50#4.50#101#2022-02-22 21:24:01.0#SOME#BABU],' \
                                u'[2541000000001#2#50#5.75#101#2022-02-22 22:17:02.0#SHEM#NYANDIEKA]'
    mockResponse.Message = "Successfully fetched 2 Transactions Valued at 100"
    return mockResponse


# noinspection PyUnusedLocal
def airtel_success_response_self_payment(cls):
    """

    :param cls:
    :return:
    """
    mockResponse = mock.Mock()
    mockResponse.Status = 0
    mockResponse.TotalTransactions = 2
    mockResponse.TotalAmount = 100.0
    # # is unicode character airtel uses to separate transactions
    mockResponse.Transactions = u'[2541000000000#254733769589#50#4.50#733769589#2022-02-22 21:24:01.0#SOME#BABU],' \
                                u'[2541000000001#254733769589#50#5.75#732851956#2022-02-22 22:17:02.0#SHEM#NYANDIEKA]'
    mockResponse.Message = "Successfully fetched 2 Transactions Valued at 100"
    return mockResponse


# noinspection PyUnusedLocal
def airtel_success_response_some_one_paying_to_customer(cls):
    """

    :param cls:
    :return:
    """
    mockResponse = mock.Mock()
    mockResponse.Status = 0
    mockResponse.TotalTransactions = 2
    mockResponse.TotalAmount = 100.0
    # # is unicode character airtel uses to separate transactions
    mockResponse.Transactions = u'[2541000000000#1#50#4.50#733769589#2022-02-22 21:24:01.0#SOME#BABU],' \
                                u'[2541000000001#2#50#5.75#733769589#2022-02-22 22:17:02.0#SHEM#NYANDIEKA]'
    mockResponse.Message = "Successfully fetched 2 Transactions Valued at 100"
    return mockResponse


# noinspection PyUnusedLocal
def airtel_success_with_duplicate_response(cls):
    """

    :param cls:
    :return:
    """
    mockResponse = mock.Mock()
    mockResponse.Status = 0
    mockResponse.TotalTransactions = 1  # They are just duplicates.
    mockResponse.TotalAmount = 50.0
    # # is unicode character airtel uses to separate transactions
    mockResponse.Transactions = u'[2541000000001#254733769589#50#4.50#732851956#2022-02-22 21:24:01.0#SOME#BABU],' \
                                u'[2541000000001#254733769589#50#4.50#732851956#2022-02-22 21:24:01.0#SOME#BABU]'
    mockResponse.Message = "Successfully fetched 1 Transaction Valued at 50"
    return mockResponse


# noinspection PyUnusedLocal
def airtel_success_response_unknown_customer(cls):
    """

    :param cls:
    :return:
    """
    mockResponse = mock.Mock()
    mockResponse.Status = 0
    mockResponse.TotalTransactions = 2
    mockResponse.TotalAmount = 100.0
    # # is unicode character airtel uses to separate transactions
    mockResponse.Transactions = u'[2541000000000#1#50#4.50#1234#2014-02-16 21:24:01.0#UNKNOWN_1#UNKNOWN],' \
                                u'[2541000000001#2#50#5.75#2345#2014-02-16 22:17:02.0#UNKNOWN_2#UNKNOWN]'
    mockResponse.Message = "Successfully fetched 2 Transactions Valued at 100"
    return mockResponse


# noinspection PyArgumentEqualDefault,PyArgumentEqualDefault,PyArgumentEqualDefault,PyArgumentEqualDefault
# noinspection PyArgumentEqualDefault,PyArgumentEqualDefault
class AirtelPaymentManagerTestCase(AirtelBaseTest):
    def test_airtel_webservice_transaction_response_format(self):
        transactionRawData = u'[2541000000004#254733769589#50#4.50#732851956#2014-02-16 21:24:01.0#SOME#BABU]'
        transactions = airtel_payment_manager.AirtelPaymentManager._get_transaction_detail_objects(transactionRawData)
        self.assertEquals(len(transactions), 1)
        transaction = transactions[0]
        self.assertEqual(transaction.payerPhoneNumber, '+254733769589')
        self.assertEqual(transaction.msisdn, '+254732851956')
        self.assertEqual(transaction.transactionId, '2541000000004')
        self.assertEqual(transaction.paidAmount, '50')
        collectTime = datetime.datetime(2014, 2, 16, 18, 24, 1, tzinfo=pytz.UTC)  # Airtel sends datetime in EAT (+03)
        self.assertEqual(transaction.collectTime, collectTime)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV2.fetch_payments', airtel_success_response_self_payment)
    def test_date_with_mock_v2(self):
        # noinspection PyArgumentEqualDefault
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V2)

        payment = ph_model.transaction.MobilePayment.objects.get(transactionId=u'2541000000001')
        self.assertEquals(decimal.Decimal(50.0), payment.amount)
        self.assertEquals(u'2541000000001', payment.transactionId)
        self.assertEquals(datetime.datetime(2022, 2, 22, 19, 17, 2, tzinfo=pytz.UTC), payment.processedTime)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV2.fetch_payments', airtel_success_response_self_payment)
    def test_process_new_payments_with_mock_v2(self):
        accountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        mth_mobile_balance(accountBalance, PHONE_ACCOUNT)
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 100
        # noinspection PyArgumentEqualDefault
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V2)
        newAccountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance

        self.assertEquals(accountBalance + decimal.Decimal(100.0), newAccountBalance)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV1.fetch_payments', airtel_success_response_self_payment)
    def test_process_new_payments_with_mock_v1(self):
        accountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        mth_mobile_balance(accountBalance, PHONE_ACCOUNT)
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 100
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V1)
        newAccountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        self.assertEquals(accountBalance + decimal.Decimal(100.0), newAccountBalance)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV2.fetch_payments', airtel_success_response_account_number)
    def test_process_pay_to_account_with_mock_v2(self):
        accountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        mth_mobile_balance(accountBalance, PHONE_ACCOUNT)
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 100
        # noinspection PyArgumentEqualDefault
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V2)
        newAccountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        self.assertEquals(accountBalance + decimal.Decimal(100.0), newAccountBalance)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV2.fetch_payments', airtel_success_response_some_one_paying_to_customer)
    def test_process_new_payments_paying_to_customer_with_mock_v2(self):
        accountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        mth_mobile_balance(accountBalance, PHONE_ACCOUNT)
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 100
        # noinspection PyArgumentEqualDefault
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V2)
        newAccountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        self.assertEquals(accountBalance + decimal.Decimal(100.0), newAccountBalance)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV1.fetch_payments', airtel_success_response_some_one_paying_to_customer)
    def test_process_new_payments_paying_to_customer_with_mock_v1(self):
        accountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        mth_mobile_balance(accountBalance, PHONE_ACCOUNT)
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 100
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V1)
        newAccountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        self.assertEquals(accountBalance + decimal.Decimal(100.0), newAccountBalance)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV1.fetch_payments', airtel_success_with_duplicate_response)
    def test_airtel_duplicate_with_mock_v1(self):
        MasterTransactionHistory.objects.all().delete()
        ph_model.transaction.MobilePayment.objects.all().delete()
        accountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        mth_mobile_balance(accountBalance, PHONE_ACCOUNT)
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 50
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V1)
        newAccountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        self.assertEquals(accountBalance + decimal.Decimal(50.0), newAccountBalance)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV2.fetch_payments', airtel_success_with_duplicate_response)
    def test_airtel_duplicate_with_mock_v2(self):
        accountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        mth_mobile_balance(accountBalance, PHONE_ACCOUNT)
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 50
        # noinspection PyArgumentEqualDefault
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V2)
        newAccountBalance = ph_model.account.Account.objects.get(pk=PHONE_ACCOUNT).accountBalance
        self.assertEquals(accountBalance + decimal.Decimal(50.0), newAccountBalance)

    @mock.patch('ph_operation.airtel_payment_manager.AirtelPaymentWebserviceV2.fetch_payments', airtel_success_response_unknown_customer)
    def test_process_new_payments_unknown_customer_with_mock_v2(self):
        # Expected to make two payments from phone number MOBILE_NUMBER, to account ACCOUNT additional amount of 100
        ph_model.event.Event.objects.all().delete()
        # noinspection PyArgumentEqualDefault
        airtel_payment_manager.AirtelPaymentManager.process_new_payments(
            SERVICE_PROVIDER, serviceVersion=airtel_payment_manager.PaymentServiceVersion.V2)
        self.assertEquals(ph_model.event.Event.objects.all().count(), 2)  # 2 unknown users
