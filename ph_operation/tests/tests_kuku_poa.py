# coding=utf-8
"""Tests for Kuku Poa"""

import copy
from ph_operation.kuku_poa import KukuUtils, KukuDistr, KukuSignUps
from ph_operation.tests import base_test
from ph_util.test_utils import assert_eq


class KukPoaTestCase(base_test.BaseTestOperation):
    """Tests Kuku Poa functions."""

    def test_remove_duplicates(self):
        acc_removed_status = [[1, 2, 3], [1, 2, 3], [1, 3, 2], [2, 3, 1], [3, 2, 1]]
        l1 = len(acc_removed_status)
        acc_removed_status = KukuUtils.remove_duplicates(acc_removed_status)
        self.assertEqual(1, l1 - len(acc_removed_status))

    def test_update_last_pickup_no_recs(self):
        """
        Test with no records
        :return:
        """
        res, updated = KukuDistr.update_last_pickup([])
        assert_eq([], res)
        assert_eq(0, len(res))
        assert_eq(0, updated)

    def test_update_last_pickup_one_rec(self):
        """
        Test 1 account with a single record
        :return:
        """
        res, updated = KukuDistr.update_last_pickup([KukuDistr.example_rec])
        assert_eq([(1, u'2018-01-18T20:07:34   Z', 4, u'2222-01-20')], res)
        assert_eq(1, len(res))
        assert_eq(1, updated)
        res, updated = KukuDistr.update_last_pickup([KukuDistr.example_rec])
        assert_eq([(1, u'2018-01-18T20:07:34   Z', 4, u'2222-01-20')], res)
        assert_eq(1, len(res))
        assert_eq(0, updated)

    def test_update_last_pickup_two_recs(self):
        """
        Test 2 accounts with a single record
        :return:
        """
        r = copy.deepcopy(KukuDistr.example_rec)
        r['form_values'][KukuDistr.account_id] = '2'
        r['version'] = 22

        res, updated = KukuDistr.update_last_pickup([KukuDistr.example_rec, r])
        res = sorted(res, key=lambda element: (element[0], element[2]))
        assert_eq((1, u'2018-01-18T20:07:34   Z', 4, u'2222-01-20'), res[0])
        assert_eq((2, u'2018-01-18T20:07:34   Z', 22, u'2222-01-20'), res[1])
        assert_eq(2, len(res))
        assert_eq(2, updated)
        res, updated = KukuDistr.update_last_pickup([KukuDistr.example_rec, r])
        res = sorted(res, key=lambda element: (element[0], element[2]))
        assert_eq((1, u'2018-01-18T20:07:34   Z', 4, u'2222-01-20'), res[0])
        assert_eq((2, u'2018-01-18T20:07:34   Z', 22, u'2222-01-20'), res[1])
        assert_eq(2, len(res))
        assert_eq(0, updated)

    def test_update_last_pickup_multiple_recs(self):
        """
        Test multiple accounts with multiple records
        :return:
        """
        a = copy.deepcopy(KukuDistr.example_rec)
        a['form_values'][KukuDistr.distribution_date] = '2222-02-22'
        a['version'] = 22

        b = copy.deepcopy(KukuDistr.example_rec)
        b['form_values'][KukuDistr.distribution_date] = '2222-03-31'
        b['version'] = 23

    def test_signup_update_all(self):
        """
        Add / Remove Kuku Poa customers based on fulcrum data
        :return:
        """

        r = copy.deepcopy(KukuSignUps.example_res)
        r['form_values'][KukuSignUps.system_removed] = 'yes'
        r[KukuSignUps.status] = '1'
        res, removed, added = KukuSignUps.update_all_kukupoa([KukuSignUps.example_res])
        assert_eq({1}, res)
        assert_eq(0, removed)
        assert_eq(1, added)

        res, removed, added = KukuSignUps.update_all_kukupoa([r])
        assert_eq({1}, res)
        assert_eq(1, removed)
        assert_eq(0, added)
