from django.core.management import call_command
import mock
import os
import ph_model.models as ph_model
from ph_operation.tests import base_test
from ph_operation import scratchcard_manager

MOCK_SCRATCHCARD_BASE_DIR = '{}/tests/fixtures/scratchcard_config'.format(os.path.dirname(os.path.dirname(__file__)))
TEST_PIN_DATA = 'scratchcard_pin_data.csv'


class BaseScratchcardManagerTestCase(base_test.BaseTestOperation):
    """Base test case for scratchcard module."""

    def setUp(self):
        # Load test data
        super(BaseScratchcardManagerTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/scratchcard_config/scratchcard_config.json', verbosity=0)


class LoadScratchcardPinDataTestCase(BaseScratchcardManagerTestCase):
   """Tests scratchcard.load_pin_data."""

   @mock.patch("ph_operation.scratchcard_manager.SCRATCHCARD_BASE_DIR", MOCK_SCRATCHCARD_BASE_DIR)
   def test_load_scratch_card_from_csv(self):
       ph_model.scratchcard.Scratchcard.objects.all().delete()
       scratchcard_manager.load_pin_data_to_db(TEST_PIN_DATA)
       self.assertEquals(ph_model.scratchcard.Scratchcard.objects.count(), 1)


class ScratchCardManagerTestCase(BaseScratchcardManagerTestCase):
   """Tests scratchcard.load_pin_data."""

   def test_init(self):
       validScratchCard = ph_model.scratchcard.Scratchcard.objects.get(id=101)
       customer = ph_model.customer.Customer.objects.get(id=101)
       scm = scratchcard_manager.ScratchcardManager(customer, validScratchCard.pin)
       self.assertEquals(scm.isPinValid, True)

   def test_process_scratchcard(self):
       ph_model.transaction.ScratchcardPaymentHistory.objects.all().delete()
       validScratchCard = ph_model.scratchcard.Scratchcard.objects.get(id=101)
       customer = ph_model.customer.Customer.objects.get(id=101)
       scm = scratchcard_manager.ScratchcardManager(customer, validScratchCard.pin)
       scm.process()
       self.assertEquals(ph_model.account.Account.objects.get(id=customer.account.id).accountBalance,
                         validScratchCard.amount)
       self.assertEquals(ph_model.scratchcard.Scratchcard.objects.get(id=101).status,
                         ph_model.scratchcard.ScratchcardStatus.USED.value)
       self.assertEquals( ph_model.transaction.ScratchcardPaymentHistory.objects.count(), 1)

   def test_generate_pin(self):
       ph_model.scratchcard.Scratchcard.objects.all().delete()
       scratchcard_manager.generate_pin_to_db(size=5, amount=[50, 100])
       self.assertEquals(ph_model.scratchcard.Scratchcard.objects.count(), 5)




