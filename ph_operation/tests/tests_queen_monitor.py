import ph_model.models as ph_model
from ph_operation import queen_monitor 
from ph_operation.tests import base_test
from ph_util import date_util
import logging

LOGGER = logging.getLogger(__name__)


class QueenMonitorTestCase(base_test.BaseTestOperation):

    def test_queen_connection(self):
        qm = queen_monitor.QueenConnectionMonitor()
        # inactive queen, no response expected
        queen = ph_model.queen.Queen.objects.get(pk=1)
        self.assertEqual(qm.check(), False)
        # active queen, not responding for more than two hours)
        queen.status = 1
        queen.save()
        self.assertEqual(qm.check(), False)
        # active queen, not responding for more than an hour, but less than two hours
        queen.lastContact = date_util.get_utc_now() - date_util.datetime.timedelta(minutes=60)
        queen.save()
        self.assertEqual(qm.check(), True)
        # active queen, responding as expected
        queen.lastContact = date_util.get_utc_now()
        queen.save()
        self.assertEqual(qm.check(), False)


