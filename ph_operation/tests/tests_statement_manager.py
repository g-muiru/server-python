# coding=utf-8
""" Statement manager tests """
from ph_model.models import Usage
from ph_operation.statement_manager import StatementManager
from ph_operation.tests import base_test
from ph_util.date_util import str_to_dt


class StatementBaseTest(base_test.BaseTestOperation):
    def setUp(self):
        """Create especial customer and circuit or related usage objects for the dynamic usage test from file."""
        super(StatementBaseTest, self).setUp()


class StatementManagerTestCase(StatementBaseTest):
    def test_monthly_statement_no_usage(self):
        res = StatementManager.monthly_statement(today=str_to_dt('2017-11-30'), test=True)
        res_str = 'Dear None, you used 0.0 Wh of stima in November at a cost of KSh 0. Your Current balance is KSh 100, Total STIMA PTS: 123.'

        self.assertEquals(len(res), 4)
        self.assertEquals(res[0][1], res_str)
        self.assertEquals(res[1][1], res_str)

    def test_monthly_statement_july(self):
        res = StatementManager.monthly_statement(today=str_to_dt('2014-07-31'), test=True)
        res_str = ('Dear None, you used 1234.0 Wh of stima in July at a cost of KSh 0. Your Current balance is KSh 100, Total STIMA PTS: 123.',
                   'Dear None, you used 0.0 Wh of stima in July at a cost of KSh 0. Your Current balance is KSh 100, Total STIMA PTS: 123.')
        self.assertEquals(len(res), 4)
        self.assertEquals(res[0][1], res_str[0])
        self.assertEquals(res[1][1], res_str[1])

    def test_monthly_statement_sept(self):
        res = StatementManager.monthly_statement(today=str_to_dt('2011-09-30'), test=True)
        res_str = ('Dear None, you used 0.0 Wh of stima in September at a cost of KSh 10. Your Current balance is KSh 100, Total STIMA PTS: 0.',
                   'Dear None, you used 1234.0 Wh of stima in September at a cost of KSh 0. Your Current balance is KSh 100, Total STIMA PTS: 123.')

        self.assertEquals(len(res), 4)
        self.assertEquals(res[0][1], res_str[0])
        self.assertEquals(res[1][1], res_str[1])

    def test_monthly_statement_jan(self):
        res = StatementManager.monthly_statement(today=str_to_dt('2000-01-31'), test=True)
        res_str = 'Dear None, you used 0.0 Wh of stima in January at a cost of KSh 0. Your Current balance is KSh 100, Total STIMA PTS: 0.'
        self.assertEquals(len(res), 4)
        self.assertEquals(res[0][1], res_str)
        self.assertEquals(res[1][1], res_str)

    def test_monthly_statement_eom_and_not_eom(self):
        res = StatementManager.monthly_statement(today=str_to_dt('2000-01-30'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-01-30'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-02-22'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-03-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-04-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-05-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-06-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-07-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-08-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-09-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-10-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-11-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-12-28'), test=True)
        self.assertEquals(len(res), 0)

        res = StatementManager.monthly_statement(today=str_to_dt('2000-01-31'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-01-31'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-02-28'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-03-31'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-04-30'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-05-31'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-06-30'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-07-31'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-08-31'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-09-30'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-10-31'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-11-30'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2017-12-31'), test=True)
        self.assertEquals(len(res), 4)

    def test_monthly_statement_leap(self):
        res = StatementManager.monthly_statement(today=str_to_dt('2016-02-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2016-02-29'), test=True)
        self.assertEquals(len(res), 4)
        res = StatementManager.monthly_statement(today=str_to_dt('2020-02-28'), test=True)
        self.assertEquals(len(res), 0)
        res = StatementManager.monthly_statement(today=str_to_dt('2020-02-29'), test=True)
        self.assertEquals(len(res), 4)
