# coding=utf-8
"""Tests for the loan manager"""
# TODO 2017-08-19 we do not charge the commercial fixed fee monthly anymore ---
#     once we are sure this will not come back, test_monthly_tariff_loan  can be removed
# TODO ad tests for loans with interest
from datetime import timedelta
from decimal import Decimal

from django.core.management import call_command
from django.db.models import Q
from django.db import connection
from ph_model.models import Account, fields
from ph_model.models import Loan
from ph_model.models import LoanPaymentHistory
from ph_model.models.customer import CustomerType, Customer
from ph_model.models.municipality import Municipality
from ph_model.models.region import Region
from ph_model.models.transaction import MasterTransactionHistory, update_mth_balances
from ph_operation import loan_manager, transaction_manager
from ph_operation.loan_manager import LoanManager
from ph_operation.tests import base_test
import mock

from ph_script import monthly_commercial_tariff_fixed_fee
from ph_util import date_util
from ph_util.accounting_util import AccountingEnums, calc_vat, add_vat, LoanType, RESIDENTIAL_CONN_LOAN_AMT, \
    COMMERCIAL_CONN_LOAN_AMT, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT, \
    KUKU_POA_SUBSCRIPTION_5_DAYS, LoanRepayment, PaymentEnums
from ph_util.date_util import str_to_dt, DELTA_365_DAYS, DELTA_30_DAYS
from ddt import ddt, unpack, idata
from ph_util.loan_utils import create_loan, create_payment_plan_no_vat
from ph_util.test_utils import assert_eq, quant


def mock_get_utc_now(utc_value):
    """Mock datetime"""
    return fields.DateTimeUTC().to_python(utc_value)


class LoanBaseTestCase(base_test.BaseTestOperation):
    """
    Setup data
    """

    def setUp(self):
        """
        Setup data
        :return:
        """
        super(LoanBaseTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/loan_config.json', verbosity=0)
        self.inSufficientBalanceAccount = Account.objects.get(id=602)
        self.inSufficientAccountLoan = Loan.objects.get(id=602)
        self.sufficientBalanceAccount = Account.objects.get(id=601)
        self.sufficientAccountLoan = Loan.objects.get(id=601)
        self.fixedScheduleBalanceAccount = Account.objects.get(id=603)
        self.fixedScheduleLoan = Loan.objects.get(id=603)
        LoanPaymentHistory.objects.all().delete()
        Loan.objects.filter(~Q(pk=601)).delete()
        cust = Customer.objects.get(id=601)
        cust.firstName = 'Steve'
        cust.lastName = 'Testers'
        cust.save()


class LoanTestCase(LoanBaseTestCase):
    """Tests loan_manager.Loan."""

    def test_repay_loan_with_insufficient_balance(self):
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        self.assertFalse(self.inSufficientAccountLoan.passedDue)
        loan = loan_manager.Loan(self.inSufficientAccountLoan)
        Loan.objects.all().delete()
        loan, acc_bal = loan.repay_loan(Customer.objects.get_or_none(account_id=loan.account.id))
        self.assertTrue(loan.passedDue)
        assert_eq(1, loan.lastPaymentNumber)
        assert_eq(4, LoanPaymentHistory.objects.count())

    # Passed due by 1 hour
    @mock.patch('ph_util.date_util.get_utc_now', mock_get_utc_now, '2015-01-02T1:1:00Z')
    def test_repay_other_loan_with_sufficient_balance(self):
        MasterTransactionHistory.objects.all().delete()
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        assert_eq(0, MasterTransactionHistory.objects.count())
        loan = loan_manager.Loan(self.sufficientAccountLoan)
        loan.repay_loan(Customer.objects.get_or_none(account_id=loan.account.id))
        self.assertEqual(add_vat(loan.payment), loan.principalPayment + loan.interestPayment)
        loan_db = Loan.objects.get(id=601)
        assert_eq(1, loan_db.lastPaymentNumber)
        loan_with_payments = Loan.objects.get(id=601)
        loan_with_payments.lastPaymentNumber = 6
        loan = loan_manager.Loan(loan_with_payments)
        loan.repay_loan(Customer.objects.get_or_none(account_id=loan.account.id))
        loan_db = Loan.objects.get(id=601)
        assert_eq(7, loan_db.lastPaymentNumber)

    def test_repay_fixed_schedule_LAST_OF_THE_MONTH(self):
        MasterTransactionHistory.objects.all().delete()
        Loan.objects.all().delete()
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        assert_eq(0, MasterTransactionHistory.objects.count())
        assert_eq(0, Loan.objects.count())
        loan = self.fixedScheduleLoan
        loan.repaymentSchedule = LoanRepayment.LAST_OF_THE_MONTH.name
        loan.save()
        for i in range(1, 31):
            dt = str_to_dt('2017-10-' + str(i))
            LoanManager.repay_due_loans(dt)
            loan = Loan.objects.get(id=603)
            assert_eq(0, loan.lastPaymentNumber)
        dt = str_to_dt('2017-09-30')
        LoanManager.repay_due_loans(dt)
        loan = Loan.objects.get(id=603)
        assert_eq(1, loan.lastPaymentNumber)
        dt = str_to_dt('2017-10-31')
        LoanManager.repay_due_loans(dt)
        loan = Loan.objects.get(id=603)
        assert_eq(2, loan.lastPaymentNumber)

    def test_repay_fixed_schedule_FIRST_OF_THE_MONTH(self):
        MasterTransactionHistory.objects.all().delete()
        Loan.objects.all().delete()
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        assert_eq(0, MasterTransactionHistory.objects.count())
        assert_eq(0, Loan.objects.count())
        loan = self.fixedScheduleLoan
        loan.repaymentSchedule = LoanRepayment.FIRST_OF_THE_MONTH.name
        loan.save()
        for i in range(2, 32):
            dt = str_to_dt('2017-10-' + str(i))
            LoanManager.repay_due_loans(dt)
            loan = Loan.objects.get(id=603)
            assert_eq(0, loan.lastPaymentNumber)
        dt = str_to_dt('2017-10-01')
        LoanManager.repay_due_loans(dt)
        loan = Loan.objects.get(id=603)
        assert_eq(1, loan.lastPaymentNumber)

    def test_repay_fixed_schedule_FIFTEENTH_AND_LAST(self):
        MasterTransactionHistory.objects.all().delete()
        Loan.objects.all().delete()
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        assert_eq(0, MasterTransactionHistory.objects.count())
        assert_eq(0, Loan.objects.count())
        loan = self.fixedScheduleLoan
        loan.repaymentSchedule = LoanRepayment.FIFTEENTH_AND_LAST.name
        loan.save()
        for i in range(1, 32):
            if i not in [31, 15]:
                LoanManager.repay_due_loans(str_to_dt('2017-10-' + str(i)))
                loan = Loan.objects.get(id=603)
                assert_eq(0, loan.lastPaymentNumber)
        for x in range(0, 3):
            LoanManager.repay_due_loans(str_to_dt('2017-10-15'))
            loan = Loan.objects.get(id=603)
            assert_eq(1, loan.lastPaymentNumber)
        for x in range(0, 3):
            LoanManager.repay_due_loans(str_to_dt('2017-10-31'))
            loan = Loan.objects.get(id=603)
            assert_eq(2, loan.lastPaymentNumber)

    def test_repay_fixed_schedule_WEEKLY(self):
        MasterTransactionHistory.objects.all().delete()
        Loan.objects.all().delete()
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        assert_eq(0, MasterTransactionHistory.objects.count())
        assert_eq(0, Loan.objects.count())
        loan = self.fixedScheduleLoan
        loan.totalNumberOfPayments = 100
        sdw = loan.startDate.weekday()
        loan.repaymentSchedule = LoanRepayment.WEEKLY.name
        loan.save()
        cnt = 0
        for i in range(1, 31):
            dt = str_to_dt('2017-10-' + str(i))
            wd = dt.weekday()
            if wd == sdw:
                cnt += 1
            LoanManager.repay_due_loans(dt)
            loan = Loan.objects.get(id=603)
            assert_eq(cnt, loan.lastPaymentNumber)
            LoanManager.repay_due_loans(dt)
            loan = Loan.objects.get(id=603)
            assert_eq(cnt, loan.lastPaymentNumber)

    def test_repay_fixed_schedule_DAILY(self):
        MasterTransactionHistory.objects.all().delete()
        Loan.objects.all().delete()
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        assert_eq(0, MasterTransactionHistory.objects.count())
        assert_eq(0, Loan.objects.count())
        loan = self.fixedScheduleLoan
        loan.totalNumberOfPayments = 100
        loan.repaymentSchedule = LoanRepayment.DAILY.name
        loan.save()
        for i in range(1, 32):
            LoanManager.repay_due_loans(str_to_dt('2017-10-' + str(i)))
            loan = Loan.objects.get(id=603)
            assert_eq(i, loan.lastPaymentNumber)
            LoanManager.repay_due_loans(str_to_dt('2017-10-' + str(i)))
            loan = Loan.objects.get(id=603)
            assert_eq(i, loan.lastPaymentNumber)

    def test_repay_fixed_schedule_FIRST_AND_FIFTEENTH(self):
        MasterTransactionHistory.objects.all().delete()
        Loan.objects.all().delete()
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        assert_eq(0, MasterTransactionHistory.objects.count())
        assert_eq(0, Loan.objects.count())
        loan = self.fixedScheduleLoan
        loan.repaymentSchedule = LoanRepayment.FIRST_AND_FIFTEENTH.name
        loan.save()
        for i in range(2, 32):
            if i not in [1, 15]:
                LoanManager.repay_due_loans(str_to_dt('2017-10-' + str(i)))
                loan = Loan.objects.get(id=603)
                assert_eq(0, loan.lastPaymentNumber)
        for x in range(0, 3):
            LoanManager.repay_due_loans(str_to_dt('2017-10-1'))
            loan = Loan.objects.get(id=603)
            assert_eq(1, loan.lastPaymentNumber)
        for x in range(0, 3):
            LoanManager.repay_due_loans(str_to_dt('2017-10-15'))
            loan = Loan.objects.get(id=603)
            assert_eq(2, loan.lastPaymentNumber)


# noinspection PyUnusedLocal
@ddt
class LoanConnectionTestCase(LoanBaseTestCase):
    """Tests Connection fee loans."""

    @staticmethod
    def find_loan_start_date(dt_str, loan_id=601):
        """
        Takes a date time and increments it by 1 day, until it hits the test loan start date
        This code will iterate 100 time before giving up.
        Also contains common code for the tests that use this

        :param dt_str: string representing a date time
        :param loan_id: loan id we will work on
        :return: the date of first payment, and the loan payment number
        """
        LoanPaymentHistory.objects.all().delete()
        assert_eq(0, LoanPaymentHistory.objects.count())
        max_loop = 400
        dt = str_to_dt(dt_str) - timedelta(days=1)
        # should not make a payment until the loan start date
        cnt = Loan.objects.get(id=loan_id).lastPaymentNumber
        while cnt == 0:
            dt += timedelta(days=1)
            loan_manager.LoanManager.repay_due_loans(dt)
            cnt = Loan.objects.get(id=loan_id).lastPaymentNumber
            max_loop -= 1
            # this should never happen
            if max_loop <= 0:
                exit(999)
        return dt, cnt

    # TODO for now there is no bonus power applied when you pay off a loan --- remove bonus_amt
    data = (
        (CustomerType.RESIDENTIAL.name, 601, -1.8, str_to_dt('2017-01-21'), 6516.41, 0.0, 10000.0, False, 'Bad stuff', -100),
        (CustomerType.COMMERCIAL.name, 601, -0.17, str_to_dt('2017-01-22'), 4200.47, 0.0, 10000.0, False, PaymentEnums.CLEARING.name, -100),
        (CustomerType.COMMERCIAL_II.name, 601, -1.8, str_to_dt('2017-01-23'), 6516.41, 0.0, 10000.0, False, PaymentEnums.UNCOLLECTED.name, -100),
        (CustomerType.RESIDENTIAL.name, 601, -1.8, str_to_dt('2017-01-21'), 0.01, -2483.60, 1000.0, True, PaymentEnums.RECONNECT.name, -0.35),
        (CustomerType.COMMERCIAL.name, 601, -0.17, str_to_dt('2017-01-22'), 1.21, -4800.74, 1000.0, True, PaymentEnums.CLEARING.name, -0.29),
        (CustomerType.COMMERCIAL_II.name, 601, -1.8, str_to_dt('2017-01-23'), 0.01, -2483.60, 1000.0, True, PaymentEnums.UNCOLLECTED.name, -0.35),
    )

    # noinspection PyUnreachableCode
    @idata(data)
    @unpack
    def test_repay_conn_fee_loan(self, cust_type, acc_cust_id, bonus_amt, now, expected_bal, expected_uncol, starting_bal, passed_due, pt, sqlite):
        """
        pay down connection fee loan over time, until loan is paid off
        bonus power should be added when loan is paid off, either through a credit adjustment
        NOTE: SQLITE does some strange rounding, seems update_mth_balances() does not process the 700+ records TODO look into this one day
        :param cust_type:
        :param acc_cust_id:
        :param bonus_amt:
        :param now:
        :param expected_bal:
        :param expected_uncol:
        :param starting_bal:
        :param passed_due:
        :param pt:
        :param sqlite
        :return:
        """

        Loan.objects.all().delete()
        LoanPaymentHistory.objects.all().delete()
        MasterTransactionHistory.objects.all().delete()

        municipality = Municipality.objects.get_or_create(
            timeZone=date_util.DEFAULT_TIME_ZONE,
            region=Region.objects.all()[0],
            name='GoofyVille',
            connectionFeeDueDate=now + DELTA_365_DAYS)[0]

        cust = Customer.objects.get(id=acc_cust_id)
        cust.customerType = cust_type
        cust.save()
        transaction_manager.TransactionManager. \
            process_mobile_payment(cust.account, ('xx' + str(cust.id)), starting_bal, str_to_dt('2016-03-02T01:01:00Z'), pt)

        transaction_manager.TransactionManager.send_payment_sms(update_mth_balances()[3])


        conn_loan = RESIDENTIAL_CONN_LOAN_AMT
        if cust.customerType == CustomerType.COMMERCIAL.name:
            conn_loan = COMMERCIAL_CONN_LOAN_AMT

        days = (municipality.connectionFeeDueDate.date() - now.date()).days
        if days < 1:
            days = 1
        daily_fee = add_vat(conn_loan) / days
        loan = create_loan(
            loan_cls=Loan,
            loan_type=LoanType.CONNECTION_FEE_LOAN.name,
            acc=cust.account,
            loan_amt=conn_loan,
            start_date=now,
            payment=daily_fee)

        dt, cnt = self.find_loan_start_date('2017-01-01', loan.id)
        loan = Loan.objects.get(id=loan.id)
        last_payment_cnt = 0
        payment_cnt = LoanPaymentHistory.objects.count()
        max_loop = 400
        cnt = Loan.objects.get(id=loan.id).lastPaymentNumber
        while payment_cnt != last_payment_cnt:
            last_payment_cnt = payment_cnt
            dt += timedelta(days=1)
            loan_manager.LoanManager.repay_due_loans(dt)
            cnt = Loan.objects.get(id=loan.id).lastPaymentNumber
            payment_cnt = LoanPaymentHistory.objects.count()
            max_loop -= 1
            # this should never happen
            if max_loop <= 0:
                exit(999)

        assert_eq(DELTA_365_DAYS.days, cnt)
        # bonus power payment
        assert_eq(DELTA_365_DAYS.days * 2 + 3, MasterTransactionHistory.objects.count())
        assert_eq(loan.startDate.date() + DELTA_365_DAYS + timedelta(days=1), dt.date())

        # verify loan does not deduct after being paid off
        dt += timedelta(days=1)
        loan_manager.LoanManager.repay_due_loans(dt)
        dt += timedelta(days=1)
        loan_manager.LoanManager.repay_due_loans(dt)

        transaction_manager.TransactionManager.send_payment_sms(update_mth_balances()[3])

        cnt = 0
        for mth in MasterTransactionHistory.objects.all().order_by('pk'):
            if mth.source == 'CreditAdjustmentHistory-BONUS_PWR':
                cnt += 1

        assert_eq(0, cnt)
        assert_eq(DELTA_365_DAYS.days * 2 + 3, MasterTransactionHistory.objects.count())

        loan = Loan.objects.get(id=loan.id)
        assert_eq(DELTA_365_DAYS.days, loan.lastPaymentNumber)
        assert_eq(DELTA_365_DAYS.days, loan.totalNumberOfPayments)
        assert_eq(passed_due, loan.passedDue)
        cust = Customer.objects.get(account_id=acc_cust_id)
        if connection.vendor == 'postgresql':
            sqlite = 0
        assert_eq(quant(Decimal(expected_bal - sqlite)), quant(Decimal(cust.account.accountBalance)))
        if connection.vendor != 'postgresql':
            if sqlite < -1:
                sqlite = 0
            else:
                sqlite += 100.01
        assert_eq(quant(Decimal(expected_uncol + sqlite)), quant(Decimal(cust.account.uncollected_bal)))


@ddt
class LoanOtherTestCase(LoanBaseTestCase):
    @staticmethod
    def find_loan_start_date(dt_str, loan_id=601):
        """
        Takes a date time and increments it by 1 day, until it hits the test loan start date
        This code will iterate 100 time before giving up.
        Also contains common code for the tests that use this

        :param dt_str: string representing a date time
        :param loan_id: loan id we will work on
        :return: the date of first payment, and the loan payment number
        """
        LoanPaymentHistory.objects.all().delete()
        assert_eq(0, LoanPaymentHistory.objects.count())
        MasterTransactionHistory.objects.all().delete()
        assert_eq(0, MasterTransactionHistory.objects.all().count())
        max_loop = 400
        dt = str_to_dt(dt_str) - timedelta(days=1)
        # should not make a payment until the loan start date
        cnt = Loan.objects.get(id=loan_id).lastPaymentNumber
        while cnt == 0:
            dt += timedelta(days=1)
            loan_manager.LoanManager.repay_due_loans(dt)
            cnt = Loan.objects.get(id=loan_id).lastPaymentNumber
            max_loop -= 1
            # this should never happen
            if max_loop <= 0:
                exit(999)
        return dt, cnt

    data = (
        (20000, CustomerType.COMMERCIAL_II.name, 601, 604, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT),
        (20000, CustomerType.COMMERCIAL.name, 601, 604, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT),
    )

    @idata(data)
    @unpack
    def test_monthly_tariff_loan(self, bal, cust_type, acc_cust_id, loan_id, loan_amt):
        """
        pay down a loan over time, until loan is paid off

        :param bal:
        :param cust_type:
        :param acc_cust_id:
        :param loan_id:
        :param loan_amt:
        :return:
        """

        # make sure we have plenty of money
        acc = Account.objects.get(id=acc_cust_id)
        acc.accountBalance = bal
        acc.save()
        starting_acc_balance = Decimal(acc.accountBalance)
        cust = Customer.objects.get(id=acc_cust_id)
        cust.customerType = cust_type
        cust.circuit.queen.grid.operational_date = str_to_dt('2001-02-01').date()
        cust.circuit.queen.grid.save()
        cust.save()
        Loan.objects.all().delete()
        assert_eq([], monthly_commercial_tariff_fixed_fee.MonthlyCommercialTariffFixedFee.monthly_check(str_to_dt('2016-01-04')))
        assert_eq([loan_id], monthly_commercial_tariff_fixed_fee.MonthlyCommercialTariffFixedFee.monthly_check(str_to_dt('2016-01-01')))
        assert_eq([], monthly_commercial_tariff_fixed_fee.MonthlyCommercialTariffFixedFee.monthly_check(str_to_dt('2016-01-02')))
        assert_eq([], monthly_commercial_tariff_fixed_fee.MonthlyCommercialTariffFixedFee.monthly_check(str_to_dt('2016-01-03')))
        assert_eq([], monthly_commercial_tariff_fixed_fee.MonthlyCommercialTariffFixedFee.monthly_check(str_to_dt('2016-01-04')))

        dt, cnt = self.find_loan_start_date('2016-01-01', loan_id)
        loan = Loan.objects.get(id=loan_id)

        assert_eq(1, cnt)
        assert_eq(dt.date(), (loan.startDate + timedelta(days=1)).date())
        idx = 10
        acc = Account.objects.get(id=acc_cust_id)
        vat = quant(calc_vat(loan_amt))
        for mth in MasterTransactionHistory.objects.all():
            self.assertTrue(mth.source in (AccountingEnums.get_keys()))
            self.assertTrue(idx in (10, 11))
            self.assertEqual(acc_cust_id, mth.account_id)
            self.assertEqual(mth.sourceProcessedTime, str_to_dt('2016-02-04 00:00:00+00:00'))
            if idx == 10:
                self.assertEqual(mth.source, AccountingEnums.LOAN_PAYMENTc600.name)
                self.assertEqual(quant(mth.amount), quant(Decimal(loan_amt)))
                self.assertEqual(quant(mth.accountBalance), quant(acc.accountBalance + vat))

                self.assertEqual(quant(Decimal(str(starting_acc_balance - loan_amt))),
                                 quant(mth.accountBalance))
            if idx == 11:
                self.assertEqual(mth.source, AccountingEnums.VAT_LOAN_PAYMENTc601.name)
                self.assertEqual(quant(mth.amount), vat)
                self.assertEqual(quant(acc.accountBalance),
                                 quant(mth.accountBalance))
            idx += 1

        # make loan for next month
        assert_eq([loan_id + 1], monthly_commercial_tariff_fixed_fee.MonthlyCommercialTariffFixedFee.monthly_check(str_to_dt('2017-02-02')))
        dt, cnt = self.find_loan_start_date('2017-01-01', loan_id + 1)
        loan = Loan.objects.get(id=loan_id + 1)
        assert_eq(1, cnt)
        assert_eq(dt.date(), (loan.startDate + timedelta(days=1)).date())

    data = (
        ("Muni time before midnight", '2015-01-03T23:02:00Z', 2),
        ("Muni time exactly midnight", '2015-01-04T00:00:00Z', 2),
        ("Muni time just past midnight", '2015-01-04T00:02:00Z', 2),
        ("Muni time more than 5 minutes past midnight", '2015-01-04T00:06:00Z', 2),
        ("Muni time more than 5 minutes past midnight", '2015-01-04T01:02:00Z', 2),
    )

    @idata(data)
    @unpack
    def test_repay_due_loans_before_midnight(self, msg, dt_str, cnt):
        """
        test_repay_due_loans_before_midnight by repaying loans
        we do not use withing 5 minutes of midnight as a check any more
        so make sure loan only gets paid once per day

        :param msg:
        :param dt_str:
        :param cnt:
        :return:
        """
        print(msg)
        self.assertEqual(0, LoanPaymentHistory.objects.count())
        dt = str_to_dt(dt_str)
        loan_manager.LoanManager.repay_due_loans(dt)
        assert_eq(cnt, LoanPaymentHistory.objects.count())
        dt = dt + timedelta(minutes=20)
        loan_manager.LoanManager.repay_due_loans(dt)
        assert_eq(cnt, LoanPaymentHistory.objects.count())

    data = (
        (CustomerType.RESIDENTIAL.name, 601, 100.00, str_to_dt('2017-01-21')),
        (CustomerType.COMMERCIAL.name, 601, 10.00, str_to_dt('2017-01-22')),
        (CustomerType.COMMERCIAL_II.name, 601, 55.00, str_to_dt('2017-01-23')),
        (CustomerType.RESIDENTIAL.name, 601, 1.25, str_to_dt('2017-01-21')),
        (CustomerType.COMMERCIAL.name, 601, -22.22, str_to_dt('2017-01-22')),
        (CustomerType.COMMERCIAL_II.name, 601, 0, str_to_dt('2017-01-23')),
        (CustomerType.RESIDENTIAL.name, 601, 1000.00, str_to_dt('2017-01-21')),
        (CustomerType.COMMERCIAL.name, 601, 2000.00, str_to_dt('2017-01-22')),
        (CustomerType.COMMERCIAL_II.name, 601, 5555.00, str_to_dt('2017-01-23')),
        (CustomerType.RESIDENTIAL.name, 601, 12500, str_to_dt('2017-01-21')),
        (CustomerType.COMMERCIAL.name, 601, 3666.66, str_to_dt('2017-01-22')),
        (CustomerType.COMMERCIAL_II.name, 601, 3333.33, str_to_dt('2017-01-23')),
    )

    @idata(data)
    @unpack
    def test_repay_kuku_poa_loan(
            self,
            cust_type,
            acc_cust_id,
            bal,
            now
    ):
        """
        pay down kuku poa loan over time, until loan is paid off
        :param bal:
        :param cust_type:
        :param acc_cust_id:
        :param bal:
        :param now:
        :return:
        """
        Loan.objects.all().delete()
        LoanPaymentHistory.objects.all().delete()
        MasterTransactionHistory.objects.all().delete()

        cust = Customer.objects.get(id=acc_cust_id)
        cust.customerType = cust_type
        cust.account.accountBalance = bal
        cust.account.save()
        cust.save()

        kuku_loan_amt = KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT

        # set the starting balance
        acc = Account.objects.get(pk=acc_cust_id)
        acc.accountBalance = bal
        acc.save()

        loan = create_payment_plan_no_vat(
            loan_cls=Loan,
            loan_type=LoanType.KUKU_POA_NO_VAT_LOAN.name,
            acc=cust.account,
            loan_amt=kuku_loan_amt,
            start_date=now,
            ttl_days=KUKU_POA_SUBSCRIPTION_5_DAYS)

        dt, cnt = self.find_loan_start_date('2017-01-01', loan.id)
        loan = Loan.objects.get(id=loan.id)

        last_payment_cnt = 0
        payment_cnt = LoanPaymentHistory.objects.count()
        max_loop = 400
        cnt = Loan.objects.get(id=loan.id).lastPaymentNumber
        while payment_cnt != last_payment_cnt:
            last_payment_cnt = payment_cnt
            dt += timedelta(days=1)
            loan_manager.LoanManager.repay_due_loans(dt)
            cnt = Loan.objects.get(id=loan.id).lastPaymentNumber
            payment_cnt = LoanPaymentHistory.objects.count()
            max_loop -= 1
            # this should never happen
            if max_loop <= 0:
                exit(999)

        assert_eq(DELTA_30_DAYS.days, cnt)
        # bonus power payment
        assert_eq(DELTA_30_DAYS.days, MasterTransactionHistory.objects.count())
        assert_eq(loan.startDate.date() + DELTA_30_DAYS + timedelta(days=1), dt.date())

        loan = Loan.objects.get(id=loan.id)
        assert_eq(DELTA_30_DAYS.days, loan.lastPaymentNumber)
        assert_eq(DELTA_30_DAYS.days, loan.totalNumberOfPayments)
        assert_eq(bal < KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT, loan.passedDue)

        cust = Customer.objects.get(id=acc_cust_id)
        uncollected = cust.account.uncollected_bal
        acc_bal = cust.account.accountBalance
        assert_eq(quant(Decimal(Decimal(bal) - Decimal(uncollected) - Decimal(KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT))), quant(Decimal(acc_bal)))

        # verify loan does not deduct after being paid off
        dt += timedelta(days=1)
        loan_manager.LoanManager.repay_due_loans(dt)
        dt += timedelta(days=1)
        loan_manager.LoanManager.repay_due_loans(dt)

        cnt = 0
        for mth in MasterTransactionHistory.objects.all():
            if mth.source == 'CreditAdjustmentHistory-BONUS_PWR':
                cnt += 1

        assert_eq(0, cnt)
        assert_eq(DELTA_30_DAYS.days, MasterTransactionHistory.objects.count())

        loan = Loan.objects.get(id=loan.id)
        assert_eq(DELTA_30_DAYS.days, loan.lastPaymentNumber)
        assert_eq(DELTA_30_DAYS.days, loan.totalNumberOfPayments)
        assert_eq(bal < KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT, loan.passedDue)
        cust = Customer.objects.get(id=acc_cust_id)
        uncollected = cust.account.uncollected_bal
        acc_bal = cust.account.accountBalance
        assert_eq(quant(Decimal(Decimal(bal) - Decimal(uncollected) - Decimal(KUKU_POA_SUBSCRIPTION_5_DISCOUNT_AMT))), quant(Decimal(acc_bal)))
