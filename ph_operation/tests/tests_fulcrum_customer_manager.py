# coding=utf-8
"""Test fulcrum customer manager"""
from datetime import datetime

from django.core.management import call_command

from ph_model.models import Grid
from ph_model.models.customer import CustomerType
from ph_model.models.municipality import Municipality
from ph_operation import fulcrum_customer_manager
from ph_operation.tests import base_test
from ph_util import phone_number_util
import mock
import ph_model.models as ph_model

# noinspection PyMissingOrEmptyDocstring,PyPep8Naming,PyPep8Naming,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
from ph_util.date_util import str_to_dt, DEFAULT_TIME_ZONE, DELTA_365_DAYS


# noinspection PyPep8Naming,PyPep8Naming,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
def mock_time_zone(latitude=None, longitude=None, dateTime=None, timeZoneService=None):
    """

    :param latitude:
    :param longitude:
    :param dateTime:
    :param timeZoneService:
    :return:
    """
    return DEFAULT_TIME_ZONE


class BaseCustomerImportTestCase(base_test.BaseTestOperation):
    """Base customer manger test case."""

    def setUp(self):
        """ Setup """
        super(BaseCustomerImportTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/fulcrum_service_contract_records.json', verbosity=0)
        ph_model.customer.PotentialCustomerImportHistory.objects.all().delete()
        ph_model.customer.Customer.objects.all().delete()


class UtilCustomerImportTestCase(BaseCustomerImportTestCase):
    def test_get_value(self):
        val = fulcrum_customer_manager.get_value({'a': 'a', 'b': 'b', 'aaa': 'aaa'}, 'aaa')
        self.assertEquals(val, 'aaa')

        val = fulcrum_customer_manager.get_value({'a': 'a', 'b': 'b', 'aa': {'aaa': 'aaa'}}, 'aaa')
        self.assertEquals(val, 'aaa')

        val = fulcrum_customer_manager.get_value({'a': 'a', 'b': 'b', 'aa': {'aaa': 'aaa'}}, 'bogus')
        self.assertIsNone(val)

        val = fulcrum_customer_manager.get_value({'a': 'a', 'b': 'b', 'aa': {'aaa': 'aaa'}}, None)
        self.assertIsNone(val)


class CloverfieldCustomerManagerTest(BaseCustomerImportTestCase):
    def setUp(self):
        """ Setup"""
        super(CloverfieldCustomerManagerTest, self).setUp()
        self.ccm = fulcrum_customer_manager.CloverfieldCustomerManager()

    def test_create_grid_if_necessary(self):
        """Creates a grid if it does not exit, otherwise returns the existing grid"""
        muni = Municipality.objects.all()[0]
        Grid.objects.all().delete()
        muni.name = 'YMCA'
        self.ccm.create_grid_if_necessary('village_people', muni)
        grid = Grid.objects.all()[0]
        self.assertEqual(grid.id, 2)
        self.assertEqual(grid.village_name, 'village_people')
        self.assertEqual(grid.name, 'YMCA')
        self.assertEqual(Grid.objects.count(), 1)

        self.ccm.create_grid_if_necessary('not_village_people', muni)
        grid = Grid.objects.all()[0]
        self.assertEqual(Grid.objects.count(), 1)
        self.assertEqual(grid.id, 2)
        self.assertEqual(grid.village_name, 'village_people')
        self.assertEqual(grid.name, 'YMCA')

        muni.name = 'Wakala'
        self.ccm.create_grid_if_necessary('waky_people', muni)
        grid = Grid.objects.get(id=3)
        self.assertEqual(Grid.objects.count(), 2)
        self.assertEqual(grid.village_name, 'waky_people')
        self.assertEqual(grid.name, 'Wakala')

        now = datetime.utcnow()
        location = 'Cloverfield'
        grid_name = 'Goofy Grid'
        ph_model.municipality.Municipality.objects.all().delete()
        ph_model.grid.Grid.objects.all().delete()
        self.assertEqual(0, ph_model.municipality.Municipality.objects.count())
        self.assertEqual(0, ph_model.grid.Grid.objects.count())
        muni = self.ccm.get_or_create_municipality(location, dueDt=now + DELTA_365_DAYS)
        self.ccm.create_grid_if_necessary(grid_name, muni)
        for g in ph_model.grid.Grid.objects.all():
            self.assertEqual(g.municipality_id, muni.id)
        self.assertEqual(1, ph_model.municipality.Municipality.objects.count())
        self.assertEqual(1, ph_model.grid.Grid.objects.count())

    @mock.patch("ph_operation.fulcrum_customer_manager.get_time_zone", mock_time_zone)
    def test_get_customer_model(self):
        record = ph_model.fulcrum.FulcrumRecord.objects.get(id="9b1fa10e-7d16-4c66-8a90-447b3686e8dd")
        ph_model.Loan.objects.all().delete()
        customer, status, loan = self.ccm.get_customer_model(record, str_to_dt('2016-03-01'))
        self.assertTrue(customer.customerType == CustomerType.RESIDENTIAL.value)
        self.assertTrue(isinstance(customer, ph_model.customer.Customer))

        record = ph_model.fulcrum.FulcrumRecord.objects.get(id="4931cf50-881a-4f69-a7a2-d636cb8e9702")
        customer, status, loan = self.ccm.get_customer_model(record, str_to_dt('2016-01-01'))
        self.assertTrue(customer.customerType == CustomerType.COMMERCIAL.value)
        self.assertTrue(isinstance(customer, ph_model.customer.Customer))
        self.assertEquals(status, 1)  # new customer
        self.assertEquals(customer.status, "POTENTIAL")
        self.assertEquals(customer.firstName, record.form_data['first_name'])
        self.assertEquals(customer.connectionFeeDue, customer.municipality.connectionFeeDueDate)
        ph_model.account.PhoneAccount.objects.get(
            mobileMoneyNumber=phone_number_util.get_e164_format(record.form_data['primary_mobile_phone_no'], self.ccm.DEFAULT_COUNTRY_CODE))
        customer, status, loan = self.ccm.get_customer_model(record)
        self.assertEquals(status, 0)
        fd = record.form_data
        fd['primary_mobile_phone_no'] = '2547000000'
        record.form_data = fd
        customer, status, loan = self.ccm.get_customer_model(record)
        ph_model.account.PhoneAccount.objects.get(
            mobileMoneyNumber=phone_number_util.get_e164_format(record.form_data['primary_mobile_phone_no'], self.ccm.DEFAULT_COUNTRY_CODE))
        self.assertEquals(status, 2)

    @mock.patch("ph_operation.fulcrum_customer_manager.get_time_zone", mock_time_zone)
    def test_save_customers_from_records(self):
        record = ph_model.fulcrum.FulcrumRecord.objects.get(id="4931cf50-881a-4f69-a7a2-d636cb8e9702")
        # noinspection PyUnresolvedReferences
        with mock.patch.object(self.ccm, 'send_creation_sms') as patch:
            self.ccm.save_customers_from_records([record])
            patch.assert_called_once()
        # noinspection PyUnresolvedReferences
        with mock.patch.object(self.ccm, 'send_creation_sms') as patch:
            self.ccm.save_customers_from_records([record])
            patch.assert_not_called()
