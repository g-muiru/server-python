# coding=utf-8
"""Test processing usage for various tariffs --- nodb"""
import decimal
import ph_model.models as ph_model
from ph_model.models.circuit import SwitchEnabledStatus
from ph_operation import tariff_manager
import datetime
from django.test import SimpleTestCase
from model_mommy import mommy
from model_mommy import recipe
from model_mommy.timezone import now
from ddt import ddt, unpack, idata

from ph_util.date_util import str_to_dt

DECI_PLACE = decimal.Decimal('.000000000001')


@ddt
class UsageTariffTest(SimpleTestCase):
    def calc_usage_charge(self, zero=False):
        """
        Mimic the actual calculation, or return 0.0
        
        :param zero: True return 0.0
        :return: 
        """
        if zero:
            return decimal.Decimal(0.0)

        total_charge = \
            self.ts.perSegmentCharge + self.ts.perWhCharge * self.usage.intervalWh + self.ts.perVACharge * self.usage.intervalVAmax
        return total_charge.quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN)

    def setUp(self):
        """ Set up all the tests """
        self.acc = mommy.prepare(ph_model.account.Account)
        self.acc.accountBalance = decimal.Decimal(100.0)
        self.grid = mommy.prepare(ph_model.grid.Grid)
        self.grid.operational_date = str_to_dt('2001-01-01').date()
        self.queen = mommy.prepare(ph_model.queen.Queen)
        self.queen.grid = self.grid
        self.circ = recipe.Recipe(
            ph_model.circuit.Circuit,
            overrideSwitchEnabled=False,
            switchEnabled=ph_model.circuit.SwitchEnabledStatus.ENABLED.value,
            queen=self.queen
        ).prepare()
        self.tc = mommy.prepare(ph_model.tariff.TariffCalendar)
        self.ts = mommy.prepare(ph_model.tariff.TariffSegment)
        self.ts.timeExpires = 32
        self.ts.perSegmentCharge = decimal.Decimal(0.0)
        self.ts.perWhCharge = decimal.Decimal(.12)
        self.ts.perVACharge = decimal.Decimal(.11)
        self.tc.dateExpires = (now().date() - datetime.timedelta(days=5))
        # self.usage = mommy.prepare(ph_model.usage.Usage)
        self.t = mommy.prepare(ph_model.tariff.Tariff)

        self.region = mommy.prepare(ph_model.region.Region)
        self.muni = recipe.Recipe(
            ph_model.municipality.Municipality,
            name='Nowhere',
            timeZone='Africa/Nairobi',
            region=self.region
        ).prepare()
        self.cust = recipe.Recipe(
            ph_model.customer.Customer,
            municipality=self.muni,
            phoneNumber='123-4567-1234',
            lastUsageProcessedTime=now(),
            circuit=self.circ
        ).prepare()
        self.usage = recipe.Recipe(
            ph_model.usage.Usage,
            collectTime=now(),
            intervalVAmax=decimal.Decimal(20),
            intervalWh=decimal.Decimal(10),
            customer=self.cust
        ).prepare()

    def test_load_data(self):
        customer = self.cust
        customer.circuit.overrideSwitchEnabled = False
        time_tariff_handler = tariff_manager.TimeTariff(
            customer, customer.account, customer.circuit, self.tc, self.ts, self.usage)
        charge, usage_charge, processed = time_tariff_handler.process_usage(True)
        self.assertEquals(decimal.Decimal(charge).quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN),
                          self.calc_usage_charge().quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN))
        self.assertEquals(True, processed)
        self.assertEquals(time_tariff_handler.tariffSegment.pk, int(self.ts.pk))

    def test_load_data_not_live(self):
        self.grid.operational_date = str_to_dt('2221-01-01').date()
        customer = self.cust
        customer.circuit.overrideSwitchEnabled = False
        time_tariff_handler = tariff_manager.TimeTariff(
            customer, customer.account, customer.circuit, self.tc, self.ts, self.usage)
        charge, usage_charge, processed = time_tariff_handler.process_usage(True)
        self.assertEquals(decimal.Decimal(charge).
                          quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN), decimal.Decimal(0.0).quantize(DECI_PLACE))
        self.assertEquals(True, processed)
        self.assertEquals(time_tariff_handler.tariffSegment.pk, int(self.ts.pk))

    def test_load_data_override(self):
        customer = self.cust
        customer.circuit.overrideSwitchEnabled = True
        time_tariff_handler = tariff_manager.TimeTariff(
            customer, customer.account, customer.circuit, self.tc, self.ts, self.usage)
        charge, usage_charge, processed = time_tariff_handler.process_usage(True)
        self.assertEquals(decimal.Decimal(charge).quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN),
                          0.0)
        self.assertEquals(True, processed)
        self.assertEquals(time_tariff_handler.tariffSegment.pk, int(self.ts.pk))

    data = (
        ('2016-01-01', SwitchEnabledStatus.ENABLED.value, False, False),
        ('2016-01-01', SwitchEnabledStatus.ENABLED.value, True, True),
        ('2017-01-01', SwitchEnabledStatus.ENABLED.value, False, False),
        ('2017-01-01', SwitchEnabledStatus.ENABLED.value, True, True),

        ('2116-01-01', SwitchEnabledStatus.DISABLED.value, False, True),
        ('2116-01-01', SwitchEnabledStatus.DISABLED.value, True, True),
        ('2116-01-01', SwitchEnabledStatus.DISABLED.value, False, True),
        ('2116-01-01', SwitchEnabledStatus.DISABLED.value, True, True),

        ('2222-01-01', SwitchEnabledStatus.ENABLED.value, False, True),
        ('2222-01-01', SwitchEnabledStatus.ENABLED.value, True, True),
        ('2222-01-01', SwitchEnabledStatus.ENABLED.value, False, True),
        ('2222-01-01', SwitchEnabledStatus.ENABLED.value, True, True),
        ('2222-01-01', SwitchEnabledStatus.DISABLED.value, False, True),
        ('2222-01-01', SwitchEnabledStatus.DISABLED.value, True, True),
        ('2222-01-01', SwitchEnabledStatus.DISABLED.value, False, True),
        ('2222-01-01', SwitchEnabledStatus.DISABLED.value, True, True),
    )

    @idata(data)
    @unpack
    def test_load_data(self, dt, enabled, override, zero):
        customer = self.cust
        self.grid.operational_date = str_to_dt(dt)
        customer.circuit.switchEnabled = enabled
        customer.circuit.overrideSwitchEnabled = override
        time_tariff_handler = tariff_manager.TimeTariff(
            customer, customer.account, customer.circuit, self.tc, self.ts, self.usage, self.grid.operational_date.date())
        charge, usage_charge, processed = time_tariff_handler.process_usage(True)
        self.assertEquals(decimal.Decimal(charge).quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN),
                          self.calc_usage_charge(zero).quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN))
        self.assertEquals(True, processed)
        self.assertEquals(time_tariff_handler.tariffSegment.pk, int(self.ts.pk))
