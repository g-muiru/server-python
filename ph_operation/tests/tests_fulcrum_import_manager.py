# coding=utf-8
""" Fulcrum import"""
from ph_operation import fulcrum_import_manager
from ph_operation.tests import base_test
import ph_model.models as ph_model
from datetime import datetime
import pytz


class BaseFulcrumImportTestCase(base_test.BaseTestOperation):
    def setUp(self):
        """Setup"""
        super(BaseFulcrumImportTestCase, self).setUp()
        self.formData = self.load_as_json_data('fulcrum_forms')
        self.recordData = self.load_as_json_data('fulcrum_records')


class FormImportTestCase(BaseFulcrumImportTestCase):
    def setUp(self):
        """Setup"""
        super(FormImportTestCase, self).setUp()
        self.question_names = set(map(lambda x: x['data_name'], fulcrum_import_manager.flatten_questions(self.formData[0]['elements'])))
        self.created = fulcrum_import_manager.to_tz(self.formData[0]['created_at'])
        self.updated = fulcrum_import_manager.to_tz(self.formData[0]['updated_at'])

    def test_form_import_first_time(self):
        fulcrum_import_manager.save_form_data(self.formData)
        form = ph_model.fulcrum.FulcrumForm.objects.get(id='047e2401-8221-4ef4-819f-902d332dbd08')
        questions = form.fulcrumquestion_set.all()
        question_names = set((q.data_name for q in questions))
        diff = question_names - self.question_names
        self.assertEquals(len(diff), 0)
        for q in questions:
            self.assertTrue(q.active)
            dt = (q.created - q.updated).total_seconds()
            self.assertEquals(int(dt), 0)
        self.assertEquals(form.created, self.created)
        self.assertEquals(form.updated, self.updated)
        pass

    def test_form_import_no_change(self):
        updated = fulcrum_import_manager.save_form_data(self.formData)
        self.assertEqual(len(updated), 2)
        updated = fulcrum_import_manager.save_form_data(self.formData)
        self.assertEqual(updated[0], ['no need to update form Internet survey'])

    def test_form_import_updated_question(self):
        fulcrum_import_manager.save_form_data(self.formData)
        form = ph_model.fulcrum.FulcrumForm.objects.get(id='047e2401-8221-4ef4-819f-902d332dbd08')
        question = ph_model.fulcrum.FulcrumQuestion.objects.get(key='c414', form=form)
        up0 = question.updated
        self.formData[0]['updated_at'] = datetime.now(pytz.utc).strftime(fulcrum_import_manager._FORMAT_STRING)
        self.formData[0]['elements'][0]['description'] = 'abc'
        fulcrum_import_manager.save_form_data(self.formData)
        form = ph_model.fulcrum.FulcrumForm.objects.get(id='047e2401-8221-4ef4-819f-902d332dbd08')
        question = ph_model.fulcrum.FulcrumQuestion.objects.get(key='c414', form=form)
        self.assertEquals(form.updated, fulcrum_import_manager.to_tz(self.formData[0]['updated_at']))
        self.assertEquals(question.description, 'abc')
        self.assertTrue((question.updated - up0).total_seconds() > 0)

    def test_form_import_new_question(self):
        fulcrum_import_manager.save_form_data(self.formData)
        self.formData[0]['elements'].append(
            {
                'data_name': 'test',
                'description': None,
                'label': 'test',
                'key': '1234'
            }
        )
        self.formData[0]['updated_at'] = datetime.now(pytz.utc).strftime(fulcrum_import_manager._FORMAT_STRING)
        fulcrum_import_manager.save_form_data(self.formData)
        form = ph_model.fulcrum.FulcrumForm.objects.get(id='047e2401-8221-4ef4-819f-902d332dbd08')
        question = ph_model.fulcrum.FulcrumQuestion.objects.get(key='1234', form=form)
        self.assertEquals(form.updated, fulcrum_import_manager.to_tz(self.formData[0]['updated_at']))
        self.assertTrue(question.active)
        self.assertEquals(question.data_name, 'test')

    def test_form_import_deleted_question(self):
        fulcrum_import_manager.save_form_data(self.formData)
        el = self.formData[0]['elements'][0]
        del self.formData[0]['elements'][0]
        self.formData[0]['updated_at'] = datetime.now(pytz.utc).strftime(fulcrum_import_manager._FORMAT_STRING)
        fulcrum_import_manager.save_form_data(self.formData)
        form = ph_model.fulcrum.FulcrumForm.objects.get(id='047e2401-8221-4ef4-819f-902d332dbd08')
        question = ph_model.fulcrum.FulcrumQuestion.objects.get(key=el['key'], form=form)
        self.assertEquals(form.updated, fulcrum_import_manager.to_tz(self.formData[0]['updated_at']))
        self.assertFalse(question.active)
        self.formData[0]['elements'] = [el] + self.formData[0]['elements']

    def test_form_import_changed_name(self):
        fulcrum_import_manager.save_form_data(self.formData)
        self.formData[0]['name'] = 'abc'
        self.formData[0]['updated_at'] = datetime.now(pytz.utc).strftime(fulcrum_import_manager._FORMAT_STRING)
        fulcrum_import_manager.save_form_data(self.formData)
        form = ph_model.fulcrum.FulcrumForm.objects.get(id='047e2401-8221-4ef4-819f-902d332dbd08')
        self.assertEquals(form.updated, fulcrum_import_manager.to_tz(self.formData[0]['updated_at']))
        self.assertEquals(form.name, 'abc')


class RecordImportTestCase(BaseFulcrumImportTestCase):
    def test_record_import_first_time(self):
        fulcrum_import_manager.save_form_data(self.formData)
        fulcrum_import_manager.save_record_data(self.recordData)
        self.assertEquals(ph_model.fulcrum.FulcrumRecord.objects.count(), 2)
        for record_json in self.recordData:
            record = ph_model.fulcrum.FulcrumRecord.objects.get(id=record_json['id'])
            self.assertEquals(record.updated, fulcrum_import_manager.to_tz(record_json['updated_at']))

    def test_record_import_no_change(self):
        fulcrum_import_manager.save_form_data(self.formData)
        updated, ids = fulcrum_import_manager.save_record_data(self.recordData)
        self.assertEqual(len(updated), 2)
        updated, ids = fulcrum_import_manager.save_record_data(self.recordData)
        self.assertEqual(len(updated), 2)
        res = (updated[0] == ['no change in record 1984b139-2dd3-451d-927e-f40b1954b3f7 on form Internet survey'] or
                             [updated[0] == 'no change in record d638c66f-8e98-4550-88da-18f8ab388b8f on form Internet survey'])
        self.assertTrue(res)
        res = (updated[1] == ['no change in record 1984b139-2dd3-451d-927e-f40b1954b3f7 on form Internet survey'] or
               [updated[1] == 'no change in record d638c66f-8e98-4550-88da-18f8ab388b8f on form Internet survey'])
        self.assertTrue(res)

    def test_record_import_with_changes(self):
        fulcrum_import_manager.save_form_data(self.formData)
        fulcrum_import_manager.save_record_data(self.recordData)
        self.recordData[1]['updated_at'] = datetime.now(pytz.utc).strftime(fulcrum_import_manager._FORMAT_STRING)
        updated, ids = fulcrum_import_manager.save_record_data(self.recordData)
        for record_json in self.recordData:
            record = ph_model.fulcrum.FulcrumRecord.objects.get(id=record_json['id'])
            self.assertEquals(record.updated, fulcrum_import_manager.to_tz(record_json['updated_at']))
        self.assertEqual(len(updated), 2)
        self.assertEqual(updated[0], 'no change in record d638c66f-8e98-4550-88da-18f8ab388b8f on form Internet survey')
        self.assertEqual(updated[1], 'updated record %s on form %s' % (self.recordData[1]['id'], self.formData[0]['name']))
