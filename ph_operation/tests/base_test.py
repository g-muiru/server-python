# coding=utf-8
""" Load test data """
from django import test
from django.core.management import call_command
import os
import json


class BaseTestOperation(test.TestCase):
    """Base model for view test classes"""

    def setUp(self):
        call_command('loaddata', 'ph_model/fixtures/common/json/initial_data.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/countries.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/sms_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_operation/tests/fixtures/grid_config.json', verbosity=0)
        call_command('loaddata', 'ph_operation/tests/fixtures/loan_config.json', verbosity=0)


    @classmethod
    def load_as_json_data(cls, schema_name):
        data_dir = os.path.dirname(os.path.realpath(__file__))
        path = "{}/fixtures/{}.json".format(data_dir, schema_name)
        with open(path) as data_file:
            json_data = json.load(data_file)
            return json_data
