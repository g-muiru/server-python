
import mock
import os

import ph_model.models as ph_model
from ph_operation import  queen_firmware_manager
from ph_operation.tests import base_test
from django.test.utils import override_settings

QUEEN_FIRMWARE_FILE_BASE_NAME = 'queen_firmware'
QUEEN_FIRMWARE_FILE_EXTENSION = '.bin'
QUEEN_FIRMWARE_FILE_NAME = "{}{}".format(QUEEN_FIRMWARE_FILE_BASE_NAME, QUEEN_FIRMWARE_FILE_EXTENSION)
QUEEN_FIRMWARE_MD5 = 'defbdffbfb54e357fe591013fee25ebb'
QUEEN_FIRMWARE_DIR_PATH = "{}/queen_firmware".format(os.path.abspath(os.path.dirname(__file__)))
QUEEN_FIRMWARE_FILE_PATH = "{}/{}".format(QUEEN_FIRMWARE_DIR_PATH, QUEEN_FIRMWARE_FILE_NAME)
QUEEN_FIRMWARE_VERSION = 'queen_firmware'


def bogusMockMd5Sum():
    return 'Bogus MD5 checksum'

class QueenFirmwareTests(base_test.BaseTestOperation):
    def setUp(self):
        super(QueenFirmwareTests, self).setUp()

    def test_get_files_from_directory(self):
        firmwareFiles = queen_firmware_manager.get_files_from_directory(QUEEN_FIRMWARE_DIR_PATH)
        self.assertEquals([QUEEN_FIRMWARE_FILE_NAME], firmwareFiles)

    def test_get_getMd5Sum(self):
        firmwareFileMd5 = queen_firmware_manager.getMd5Sum(QUEEN_FIRMWARE_FILE_PATH)
        self.assertEquals(QUEEN_FIRMWARE_MD5, firmwareFileMd5)

    @override_settings(FIRMWARE_PATH=QUEEN_FIRMWARE_DIR_PATH)
    def test_sync_firmwares_with_valid_checksum(self):
        ph_model.queen.QueenFirmware.objects.all().delete()
        queenFirmwareManager = queen_firmware_manager.QueenFirmwareManager()
        queenFirmwareManager.sync_firmwares()
        syncedFirmwares = ph_model.queen.QueenFirmware.objects.all()
        self.assertEquals(len(syncedFirmwares), 1)
        syncedFirmware = syncedFirmwares[0]
        self.assertEquals(syncedFirmware.md5Sum, QUEEN_FIRMWARE_MD5)
        self.assertEquals(syncedFirmware.fileBaseName, QUEEN_FIRMWARE_FILE_BASE_NAME)
        self.assertEquals(syncedFirmware.fileExtension, QUEEN_FIRMWARE_FILE_EXTENSION)
        self.assertEquals(syncedFirmware.path, QUEEN_FIRMWARE_DIR_PATH)
        self.assertEquals(syncedFirmware.version, QUEEN_FIRMWARE_VERSION)

    @override_settings(FIRMWARE_PATH=QUEEN_FIRMWARE_DIR_PATH)
    def test_sync_firmwares_with_in_valid_checksum(self):
        ph_model.queen.QueenFirmware.objects.all().delete()
        queen_firmware_manager.QueenFirmwareManager().sync_firmwares()
        syncedFirmwares = ph_model.queen.QueenFirmware.objects.all()
        self.assertEquals(len(syncedFirmwares), 1)
        syncedFirmware = syncedFirmwares[0]
        syncedFirmware.md5Sum = bogusMockMd5Sum()
        syncedFirmware.save()
        queen_firmware_manager.QueenFirmwareManager().sync_firmwares()
        self.assertEquals( ph_model.queen.QueenFirmware.objects.get(id=syncedFirmware.id).status,
                           ph_model.queen.FirmwareStatus.CHECKSUM_FAILURE.value)
