# coding=utf-8
"""Test the tariff manager"""
import csv
import datetime
from decimal import Decimal
import os
import pytz
from ddt import ddt, unpack, idata

from django.core.management import call_command
from django.utils import dateparse

import ph_model.models as ph_model
from ph_model.models import Customer, TariffSegment
from ph_model.models.transaction import save_usage_charge_to_master_transaction_accounting, update_mth_balances
from ph_operation import tariff_manager, transaction_manager
from ph_operation.tariff_manager import TariffMemoizer
from ph_operation.tests import base_test
from ph_operation.transaction_manager import TransactionManager
from ph_util.accounting_util import AccountingEnums
from ph_util.date_util import str_to_dt

TARIFF_INDEX = 0
TARIFF_CALENDAR_INDEX = 1
TARIFF_SEGMENT_INDEX = 2
CUSTOMER_INDEX = 3

DECI_PLACES = Decimal('0.000000')


class BaseTariffTestCase(base_test.BaseTestOperation):
    """
    Base class
    """

    def setUp(self):
        """
        Set vars for tests
        :return:
        """
        super(BaseTariffTestCase, self).setUp()
        self.usage = ph_model.usage.Usage.objects.get(pk=1)
        tariff_info = self.get_tariff_info(self.usage)
        self.customer = tariff_info[CUSTOMER_INDEX]
        self.tariffCalendar = tariff_info[TARIFF_CALENDAR_INDEX]
        self.tariffSegment = tariff_info[TARIFF_SEGMENT_INDEX]
        self.tariffSegment = ph_model.tariff.TariffSegment.objects.get(segmentId=self.customer.tariffSegment_id)

    @staticmethod
    def get_tariff_info(usage):
        """
        Helper to get tariff associated with usage.
        :param usage: usage object
        :return: tuple of (tariff, tariff_calendar, TariffSegment, customer
        """

        customer = ph_model.customer.Customer.objects.get(pk=usage.customer_id)
        tariff = ph_model.tariff.Tariff.objects.get(tariffId=customer.tariff_id)
        tariff_calendar = ph_model.tariff.TariffCalendar.objects.get(calendarId=customer.tariffCalendar_id)
        tariff_segment = ph_model.tariff.TariffSegment.objects.get(segmentId=customer.tariffSegment_id)
        return tariff, tariff_calendar, tariff_segment, customer


class TariffMidNightTestCase(BaseTariffTestCase):
    def test_mid_night_passed_first_time_usage_before_mid_night(self):
        # set before mid night
        self.customer.lastUsageProcessedTime = None
        self.usage.collectTime = datetime.datetime(2014, 1, 1, hour=23, tzinfo=pytz.utc)
        tariff = tariff_manager.TimeTariff(self.customer, self.customer.account, self.customer.circuit,
                                           self.tariffCalendar, self.tariffSegment, self.usage)
        self.assertFalse(tariff.mid_night_passed())

    def test_mid_night_passed_first_time_usage_at_mid_night(self):
        # set exactly mid night
        self.customer.lastUsageProcessedTime = None
        self.usage.collectTime = datetime.datetime(2014, 1, 1, tzinfo=pytz.utc)
        tariff = tariff_manager.TimeTariff(self.customer, self.customer.account, self.customer.circuit,
                                           self.tariffCalendar, self.tariffSegment, self.usage)
        self.assertTrue(tariff.mid_night_passed())

    def test_mid_night_passed_between_last_usage_and_collect_time(self):
        self.customer.lastUsageProcessedTime = datetime.datetime(2014, 1, 1, hour=23, minute=59, tzinfo=pytz.utc)
        self.usage.collectTime = self.customer.lastUsageProcessedTime + datetime.timedelta(minutes=2)
        tariff = tariff_manager.TimeTariff(self.customer, self.customer.account, self.customer.circuit,
                                           self.tariffCalendar, self.tariffSegment, self.usage)
        self.assertTrue(tariff.mid_night_passed())

    def test_mid_night_passed_exactly_at_mid_night(self):
        self.customer.lastUsageProcessedTime = datetime.datetime(2014, 1, 1, hour=23, minute=45, tzinfo=pytz.utc)
        self.usage.collectTime = self.customer.lastUsageProcessedTime + datetime.timedelta(minutes=15)
        tariff = tariff_manager.TimeTariff(self.customer, self.customer.account, self.customer.circuit,
                                           self.tariffCalendar, self.tariffSegment, self.usage)
        self.assertTrue(tariff.mid_night_passed())


@ddt
class DisabledCircuitTariffCalculationTest(base_test.BaseTestOperation):
    """
    Base class
    """

    def setUp(self):
        """
        Set vars for tests
        :return:
        """
        super(DisabledCircuitTariffCalculationTest, self).setUp()
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_operation/tests/fixtures/usage_config.json', verbosity=0)
        # default to operaional grid and queen
        circuit = ph_model.circuit.Circuit.objects.get(pk=100)
        circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
        circuit.save()
        # default to grid and queen operational
        circuit.queen.grid.operational_date = str_to_dt('2011-01-01').date()
        circuit.queen.grid.save()
        circuit.queen.operational_date = str_to_dt('2011-01-01').date()
        circuit.queen.save()

    def test_disabled_circuit_generates_no_charge(self):
        ph_model.usage.Usage.objects.all().delete()
        collect_time = datetime.datetime.utcnow()
        circuit = ph_model.circuit.Circuit.objects.get(pk=100)
        circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.DISABLED.value
        circuit.save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time, intervalWh=10).save()
        pu, uc, duc = tariff_manager.TariffManager.process_usages()
        self.assertTrue(len(pu) == 1)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 0)
        for usageCharge in ph_model.transaction.UsageCharge.objects.all():
            self.assertEqual(usageCharge.amount, 0)

    before_date_str = '2001-01-01'
    after_date_str = '2222-02-22'
    data = (
        (before_date_str, before_date_str, True),
        (before_date_str, after_date_str, True),
        (after_date_str, before_date_str, True),
        (after_date_str, after_date_str, False)
    )

    @idata(data)
    @unpack
    def test_enabled_circuit_generates_charge_operational_date(self, grid_dt, queen_dt, charge):
        ph_model.usage.Usage.objects.all().delete()
        collect_time = str_to_dt('2016-01-01T010:00')
        circuit = ph_model.circuit.Circuit.objects.get(pk=100)
        circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
        circuit.save()
        circuit.queen.grid.operational_date = str_to_dt(grid_dt).date()
        circuit.queen.grid.save()
        circuit.queen.operational_date = str_to_dt(queen_dt).date()
        circuit.queen.save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time, intervalWh=100).save()
        pu, uc, duc = tariff_manager.TariffManager.process_usages()
        self.assertTrue(len(pu) == 1)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 0)
        for usageCharge in ph_model.transaction.UsageCharge.objects.all():
            if charge:
                self.assertEqual(Decimal(5.0).quantize(Decimal('.1')), usageCharge.amount.quantize(Decimal('.1')))
            else:
                self.assertEqual(usageCharge.amount.quantize(Decimal('.1')), Decimal(0.0).quantize(Decimal('.1')))

    def test_override_circuit_generates_no_charge(self):
        ph_model.usage.Usage.objects.all().delete()
        collect_time = datetime.datetime.utcnow()
        circuit = ph_model.circuit.Circuit.objects.get(pk=100)
        circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
        circuit.overrideSwitchEnabled = True
        circuit.save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time, intervalWh=10).save()
        pu, uc, duc = tariff_manager.TariffManager.process_usages()
        self.assertTrue(len(pu) == 1)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 0)
        for usageCharge in ph_model.transaction.UsageCharge.objects.all():
            self.assertEqual(usageCharge.amount, 0)


@ddt
class DynamicUsageTariffTestFromCSV(base_test.BaseTestOperation):
    # TODO(estifanos): Check and add in the usage generation data.
    USAGE_FIELDS = ['circuit_id', 'customer_id', 'intervalWh', 'intervalVAmax']  # intervalVmin
    COLLECT_TIME = 'collectTime'
    USAGE_CHARGE = 'usageCharge'
    TARIFF_SEGMENT = 'tariffSegment'

    def setUp(self):
        """Create especial customer and circuit or related usage objects for the dynamic usage test from file."""
        super(DynamicUsageTariffTestFromCSV, self).setUp()
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_operation/tests/fixtures/usage_config.json', verbosity=0)
        circuit = ph_model.circuit.Circuit.objects.get(pk=100)
        circuit.queen.grid.operational_date = str_to_dt('2011-01-01').date()
        circuit.queen.grid.save()
        circuit.queen.operational_date = str_to_dt('2011-01-01').date()
        circuit.queen.save()

    def load_usage_csv_with_charge(self, csv_file_name='usages_fixed.csv'):
        """
        csv fields to dict, with model id as key and usage charge as value

        :param csv_file_name: name of the file
        :return: A dict of usage model id as key and charge price as value.
        """

        usage_info = {}
        current_path = os.path.dirname(os.path.realpath(__file__))
        with open("{}/fixtures/{}".format(current_path, csv_file_name)) as usageAsCsv:
            usage_data = csv.DictReader(usageAsCsv)
            for each in usage_data:
                usage = ph_model.usage.Usage()
                usage.collectTime = dateparse.parse_datetime(each[self.COLLECT_TIME]) or dateparse.parse_date(self.COLLECT_TIME)
                for usageField in self.USAGE_FIELDS:
                    field_value = each[usageField]
                    if not field_value or field_value == 'none' or field_value == 'null':
                        continue
                    setattr(usage, usageField, field_value)
                usage.save()
                usage_info.update({usage.pk: {self.USAGE_CHARGE: each[self.USAGE_CHARGE],
                                              self.TARIFF_SEGMENT: each[self.TARIFF_SEGMENT]}})
        return usage_info

    def test_max_usage_tariff(self):
        """Max usage tariff test"""
        # noinspection PyCompatibility
        TariffMemoizer()
        TariffMemoizer.flush_customer_data()
        # noinspection PyCompatibility
        for usageId, usageData in self.load_usage_csv_with_charge('max_usage.csv').iteritems():
            usage = ph_model.usage.Usage.objects.get(pk=usageId)
            customer, account, circuit, customer_tariff, tariff_calendar, tariff_segment, operational_date = \
                TariffMemoizer.get_customer_info(usage.customer_id)
            customer.circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
            customer.circuit.queen.grid.operational_date = str_to_dt('2001-01-01').date()
            time_tariff_handler = \
                tariff_manager.TimeTariff(customer, customer.account, customer.circuit, tariff_calendar, tariff_segment, usage,
                                          customer.circuit.queen.grid.operational_date)
            c, uc, processed = time_tariff_handler.process_usage()
            # daily fee is no longer charged on the tariff, but on a cron job
            if Decimal(usageData[self.USAGE_CHARGE]) > 16:
                self.assertTrue((c - Decimal(usageData[self.USAGE_CHARGE]) - Decimal(16.44)) < .01)
            else:
                self.assertEquals(c, Decimal(usageData[self.USAGE_CHARGE]))
            self.assertEquals(time_tariff_handler.tariffSegment.pk, int(usageData[self.TARIFF_SEGMENT]))

    def test_usage_process(self):
        # Clear all existing usages.
        ph_model.usage.Usage.objects.all().delete()
        collect_time = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
        usage = ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time)
        usage.save()
        self.assertFalse(usage.processed)

        tariff_manager.TariffManager.process_usage(usage)
        self.assertTrue(usage.processed)

    def test_process_usages_duplicates(self):
        # Clear all existing usages.
        ph_model.usage.Usage.objects.all().delete()
        collect_time = datetime.datetime.utcnow()
        ph_model.transaction.MasterTransactionHistory.objects.all().delete()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()
        self.assertNotEqual(0, ph_model.usage.Usage.objects.count())
        for usage in ph_model.usage.Usage.objects.all():
            self.assertFalse(usage.processed)
        pu, uc, duc = tariff_manager.TariffManager.process_usages()
        self.assertTrue(len(pu) == 2)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 1)
        for usage in ph_model.usage.Usage.objects.all():
            self.assertTrue(usage.processed)

    data = (
        ('2016-01-01 00:05:00', 1),
        ('2016-01-02 01:05:00', 1),
        ('2016-01-03 02:05:00', 1),
        ('2016-01-04 03:05:00', 1),
        ('2016-01-05 04:05:00', 2),
        ('2016-01-06 05:05:00', 2),
        ('2016-01-07 06:05:00', 2),
        ('2016-01-08 07:05:00', 2),
        ('2016-01-09 08:05:00', 2),
        ('2016-01-10 09:05:00', 2),
        ('2016-01-11 10:05:00', 2),
        ('2016-01-12 11:05:00', 2),
        ('2016-01-13 12:05:00', 2),
        ('2016-01-14 13:05:00', 2),
        ('2016-01-15 14:05:00', 2),
        ('2016-01-16 15:05:00', 2),
        ('2016-01-17 16:05:00', 2),
        ('2016-01-18 17:05:00', 2),
        ('2016-01-19 18:05:00', 2),
        ('2016-01-20 19:05:00', 2),
        ('2016-01-21 20:05:00', 2),
        ('2016-01-22 21:05:00', 1),
        ('2016-01-23 22:05:00', 1),
        ('2016-01-24 23:05:00', 1)
    )

    @idata(data)
    @unpack
    def test_process_usages_and_master_transaction_history_duplicate(self, dt_str, div):
        """There should be 4 invoices per usage if there is a fixed fee, otherwise 2"""
        # Clear all existing usages.
        ph_model.usage.Usage.objects.all().delete()
        collect_time = str_to_dt(dt_str)
        TariffMemoizer.flush_customer_data()
        ph_model.transaction.MasterTransactionHistory.objects.all().delete()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()

        self.assertEqual(4, ph_model.usage.Usage.objects.count())
        self.assertEqual(0, ph_model.transaction.MasterTransactionHistory.objects.count())
        for usage in ph_model.usage.Usage.objects.all():
            self.assertFalse(usage.processed)

        TransactionManager.load_usage_charges_to_master_transaction(page_size=1)
        self.assertEqual(0, ph_model.transaction.MasterTransactionHistory.objects.count())

        pu, uc, duc = tariff_manager.TariffManager.process_usages()
        self.assertTrue(len(pu) == 4)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 3)
        for usage in ph_model.usage.Usage.objects.all():
            self.assertTrue(usage.processed)

        # make sure there is actually some usage, use bulk update so syncToMasterTransaction does not get set True
        ph_model.transaction.UsageCharge.objects.all().update(amount=33.33)

        TransactionManager.load_usage_charges_to_master_transaction(page_size=1)
        # invoices for tariff variable, variable vat, fixed, and fixed vat
        self.assertEqual(ph_model.usage.Usage.objects.count() / div, ph_model.transaction.MasterTransactionHistory.objects.count())

    def test_init_tariff(self):
        # Customer from usage config.
        ph_model.event.Event.objects.all().delete()
        customer = ph_model.customer.Customer.objects.get(id=100)
        self.assertEqual(customer.tariff_id, 1)

        tariff_manager.TariffManager.init_tariff(customer, 2)
        new_customer_created_event_count = ph_model.event.Event.objects.filter(
            eventType__eventNumber=ph_model.event.EventTypeConst.CUSTOMER_TARIFF_CHANGE.value).count()
        self.assertEqual(new_customer_created_event_count, 1)

    def test_process_usages_by_customers(self):
        # Clear all existing usages and test usage processed by list of customer.
        ph_model.usage.Usage.objects.all().delete()
        collect_time = datetime.datetime.utcnow()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=collect_time).save()
        for usage in ph_model.usage.Usage.objects.all():
            self.assertFalse(usage.processed)

        pu, uc, duc = tariff_manager.TariffManager.process_usages_by_customers([100])
        self.assertTrue(len(pu) == 2)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 1)
        for usage in ph_model.usage.Usage.objects.all():
            self.assertTrue(usage.processed)

    def test_process_usages_by_circuits(self):
        """Clear all existing usages and test usage processed by list of circuits."""
        ph_model.usage.Usage.objects.all().delete()
        ph_model.transaction.UsageCharge.objects.all().delete()
        ph_model.transaction.MasterTransactionHistory.objects.all().delete()
        ph_model.usage.Usage.objects.all().delete()
        self.assertEqual(0, ph_model.transaction.UsageCharge.objects.count())
        circuit = ph_model.circuit.Circuit.objects.get(pk=100)
        circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
        circuit.save()
        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=str_to_dt('2016-01-02T01:01:00Z'), intervalWh=10).save()

        pu, uc, duc = tariff_manager.TariffManager.process_usages_by_circuits([100])
        self.assertTrue(len(pu) == 1)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 0)
        uc = ph_model.transaction.UsageCharge.objects.all()[0]
        self.assertTrue(ph_model.transaction.MasterTransactionHistory.objects.count() == 0)
        self.assertTrue(uc.perSegmentChargeComponent != 0)
        save_usage_charge_to_master_transaction_accounting(uc)

        idx = 4
        self.assertTrue(idx, ph_model.transaction.MasterTransactionHistory.objects.count())
        for mth in ph_model.transaction.MasterTransactionHistory.objects.order_by('pk'):
            self.assertTrue(mth.source in (AccountingEnums.get_keys()))
            self.assertTrue(idx in (4, 5, 6, 7))
            if idx == 4:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertEqual(mth.amount.quantize(DECI_PLACES), Decimal('4.300000'))
            if idx == 5:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertEqual(mth.amount.quantize(DECI_PLACES), Decimal('0.688000'))
            if idx == 6:
                self.assertEqual(mth.source, AccountingEnums.RES_FIXED_FEEc236.name)
                self.assertEqual(mth.amount.quantize(DECI_PLACES), Decimal('16.440000'))
            if idx == 7:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_FIXEDc346.name)
                self.assertEqual(mth.amount.quantize(DECI_PLACES), Decimal('2.630400'))
            idx += 1

        ph_model.usage.Usage(customer_id=100, circuit_id=100, collectTime=str_to_dt('2016-01-02T21:01:00Z'), intervalWh=10).save()
        ph_model.transaction.MasterTransactionHistory.objects.all().delete()
        pu, uc, duc = tariff_manager.TariffManager.process_usages_by_circuits([100])
        self.assertTrue(len(pu) == 1)
        self.assertTrue(len(uc) == 1)
        self.assertTrue(len(duc) == 0)
        uc = ph_model.transaction.UsageCharge.objects.all()[0]
        self.assertTrue(ph_model.transaction.MasterTransactionHistory.objects.count() == 0)
        self.assertTrue(uc.perSegmentChargeComponent != 0)
        save_usage_charge_to_master_transaction_accounting(uc)
        self.assertEqual(2, ph_model.transaction.MasterTransactionHistory.objects.count())

        idx = 6
        for mth in ph_model.transaction.MasterTransactionHistory.objects.order_by('pk'):
            self.assertTrue(mth.source in (AccountingEnums.get_keys()))
            self.assertTrue(idx in (6, 7))
            if idx == 6:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertEqual(mth.amount.quantize(DECI_PLACES), Decimal('4.300000'))
            if idx == 7:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertEqual(mth.amount.quantize(DECI_PLACES), Decimal('0.688000'))
            idx += 1

    hours = (
        '2018-05-12 00:05:00',
        '2018-05-12 01:05:00',
        '2018-05-12 02:05:00',
        '2018-05-12 03:05:00',
        '2018-05-12 04:05:00',
        '2018-05-12 05:05:00',
        '2018-05-12 06:05:00',
        '2018-05-12 07:05:00',
        '2018-05-12 08:05:00',
        '2018-05-12 09:05:00',
        '2018-05-12 10:05:00',
        '2018-05-12 11:05:00',
        '2018-05-12 12:05:00',
        '2018-05-12 13:05:00',
        '2018-05-12 14:05:00',
        '2018-05-12 15:05:00',
        '2018-05-12 16:05:00',
        '2018-05-12 17:05:00',
        '2018-05-12 18:05:00',
        '2018-05-12 19:05:00',
        '2018-05-12 20:05:00',
        '2018-05-12 21:05:00',
        '2018-05-12 22:05:00',
        '2018-05-12 23:05:00'
    )

    @idata(hours)
    @unpack
    def assess_daily_fee(self):
        # Clear all existing usages.
        ph_model.usage.Usage.objects.all().delete()



class Pu(base_test.BaseTestOperation):
    def test_pu(self):
        """Clear all existing usages and test usage processed by list of circuits.
        TODO: Results vary depending how test is run "python manage.py test ph_operation.tests.tests_tariff_manager:Pu" or "python manage.py test -p"
        Thus Need to understand why this happens
        """
        ph_model.usage.Usage.objects.all().delete()
        ph_model.transaction.UsageCharge.objects.all().delete()
        ph_model.transaction.MasterTransactionHistory.objects.all().delete()
        ph_model.usage.Usage.objects.all().delete()
        self.assertEqual(0, ph_model.transaction.UsageCharge.objects.count())

        tariff = Customer.objects.get(pk=1).tariff
        Customer.objects.get(pk=603).delete()
        for c in Customer.objects.all():
            c.circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
            c.circuit.save()
            c.circuit.queen.grid.operational_date = str_to_dt('2016-01-01')
            c.circuit.queen.grid.save()
            c.circuit.queen.grid.project.name = 'Cloverfield'
            c.circuit.queen.grid.project.save()
            c.account.accountBalance = 0
            c.account.uncollected_bal = 0
            c.account.save()
            c.municipality.name = 'Nyamondo'
            c.municipality.save()
            c.tariff = tariff
            c.save()
            if c.id == 1:
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'xx0', 30, str_to_dt('2016-03-02T01:01:00Z'))
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'xx1', 300, str_to_dt('2016-03-02T11:01:00Z'))
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2016-03-02T2:01:00Z'), intervalWh=10).save()
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2016-03-02T5:16:00Z'), intervalWh=20).save()
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'xx2', 100, str_to_dt('2016-03-02T18:02:00Z'))
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2016-03-02T10:31:00Z'), intervalWh=3).save()
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2016-03-02T20:46:00Z'), intervalWh=5).save()
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'xx3', 40, str_to_dt('2016-03-02T19:03:00Z'))

            if c.id == 601:
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'yy0', 11, str_to_dt('2018-03-02T01:01:00Z'))
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'yy1', 101, str_to_dt('2018-03-02T11:01:00Z'))
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2018-03-02T2:01:00Z'), intervalWh=5).save()
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2018-03-02T5:16:00Z'), intervalWh=10).save()
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'yy2', 101, str_to_dt('2018-03-02T18:02:00Z'))
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2018-03-02T10:31:00Z'), intervalWh=30).save()
                ph_model.usage.Usage(customer_id=c.id, circuit_id=c.circuit_id, collectTime=str_to_dt('2018-03-02T20:46:00Z'), intervalWh=10).save()
                transaction_manager.TransactionManager.process_mobile_payment(c.account, 'yy3', 51, str_to_dt('2018-03-02T19:03:00Z'))

            t = TariffSegment.objects.get(pk=2)
            t.perSegmentCharge = 0
            t.perVaCharge = 0
            # t.timeExpires = 360
            t.save()
            # print(str_obj(c.tariff))
            # print(str_obj(TariffCalendar.objects.get(pk=1)))
            # print(str_obj(TariffSegment.objects.get(pk=1)))
            # print(str_obj(TariffSegment.objects.get_or_none(pk=2)))

        pu, uc, duc = tariff_manager.TariffManager.process_usages()

        transaction_manager.TransactionManager.load_usage_charges_to_master_transaction()

        update_mth_balances()

        self.assertEqual(len(pu), 8)
        self.assertEqual(len(uc), 8)
        self.assertEqual(len(duc), 0)
        uc = ph_model.transaction.UsageCharge.objects.all()[0]
        self.assertEqual(ph_model.transaction.MasterTransactionHistory.objects.count(), 24)
        self.assertTrue(round(uc.perSegmentChargeComponent, 2) == 16.44 or round(uc.perSegmentChargeComponent, 2) == 0.00)
        save_usage_charge_to_master_transaction_accounting(uc)

        update_mth_balances()

        idx = 4
        self.assertTrue(idx, ph_model.transaction.MasterTransactionHistory.objects.count())
        for mth in ph_model.transaction.MasterTransactionHistory.objects.order_by('pk'):
            print(mth.account_id, mth.source,mth.amount)
        for mth in ph_model.transaction.MasterTransactionHistory.objects.order_by('pk'):
            if mth.source == 'MobilePayment':
                continue
            self.assertTrue(mth.source in (AccountingEnums.get_keys()))
            self.assertTrue(idx in (4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19))

            if idx == 4:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES) == Decimal('100.0') or mth.amount.quantize(DECI_PLACES) == Decimal('4.3'))
            if idx == 5:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('16.0') or mth.amount.quantize(DECI_PLACES) == Decimal('0.688'))
            if idx == 6:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('200.0') or mth.amount.quantize(DECI_PLACES) == Decimal('8.6'))
            if idx == 7:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('32.0') or mth.amount.quantize(DECI_PLACES) == Decimal('1.376'))
            if idx == 8:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('30.0'))
            if idx == 9:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('4.8'))
            if idx == 10:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('50.0') or mth.amount.quantize(DECI_PLACES) == Decimal('0.25'))
            if idx == 11:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('8.0') or mth.amount.quantize(DECI_PLACES) == Decimal('0.04'))
            if idx == 12:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('50.0') or mth.amount.quantize(DECI_PLACES) == Decimal('2.15'))
            if idx == 13:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('8.0') or mth.amount.quantize(DECI_PLACES) == Decimal('0.344'))
            if idx == 14:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('100.0') or mth.amount.quantize(DECI_PLACES) == Decimal('4.3'))
            if idx == 15:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('16.0') or mth.amount.quantize(DECI_PLACES) == Decimal('0.688'))
            if idx == 16:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('300.0'))
            if idx == 17:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('48.0'))
            if idx == 18:
                self.assertEqual(mth.source, AccountingEnums.RES_VAR_FEEc238.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('100.000000') or mth.amount.quantize(DECI_PLACES) == Decimal('0.5'))
            if idx == 19:
                self.assertEqual(mth.source, AccountingEnums.VAT_TARIFF_VARIABLEc345.name)
                self.assertTrue(mth.amount.quantize(DECI_PLACES), Decimal('16.000000') or mth.amount.quantize(DECI_PLACES) == Decimal('0.08'))
            idx += 1
