# coding=utf-8
"""Transaction manager tests"""
import datetime
import decimal

from ddt import ddt, idata, unpack
from django.db import connection
from ph_model.models import Account, Customer, PhoneAccount
from ph_model.models.transaction import MasterTransactionHistory, update_mth_balances, MobilePayment, UsageCharge
from ph_operation import transaction_manager
from ph_operation.tests import base_test
from django.core.management import call_command

from ph_util.accounting_util import str_obj, PaymentEnums
from ph_util.loan_utils import mth_mobile_balance
from ph_util.test_utils import assert_eq


class TransactionBaseTestCase(base_test.BaseTestOperation):
    AMOUNT_TO_DEBIT_OR_CREDIT = decimal.Decimal(100.00)

    def setUp(self):
        """
        Setup
        """
        super(TransactionBaseTestCase, self).setUp()
        self.customer = Customer.objects.get(pk=1)
        self.account = self.customer.account
        self.accountBalance = self.account.accountBalance
        self.debitTransaction = transaction_manager.Debit(self.account, self.AMOUNT_TO_DEBIT_OR_CREDIT)
        self.creditTransaction = transaction_manager.Credit(self.account, self.AMOUNT_TO_DEBIT_OR_CREDIT)

@ddt
class DebitTestCase(TransactionBaseTestCase):
    """Tests transaction_manager.Debit."""

    data = (
        (100, 100, 0, 0, 0, 0, 0, 0, 0, None),
        (100, 100, 0, 0, 0, 0, 0, 0, 0, PaymentEnums.ACCOUNT.name),
        (100, 100, 0, 0, 0, 100, 0, 0, -100, PaymentEnums.RECONNECT.name),
        (100, 100, 0, 0, 0, 100, -100, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (100, 100, 0, 0, 0, 100, 0, -100, 0, PaymentEnums.CLEARING.name),

        (88, 88, -50, 0, 0, 0, -50, 0, 0, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, 0, 0, 10, -50, 0, 0, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, 0, 0, 0, -100, 0, 0, PaymentEnums.ACCOUNT.name),
        (88, 88, -50, -10, -20, 0, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, -10, -20, 10, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, -10, -20, 0, -100, -10, -20, PaymentEnums.ACCOUNT.name),
        (88, 88, -50, -10, -20, 0, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, -10, -20, 10, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, -10, -20, 0, -100, -10, -20, PaymentEnums.ACCOUNT.name),

        (88, 88, -50, 0, 0, 88, -138, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (78, 88, -50, 0, 0, 88, -128, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (138, 88, -50, 0, 0, 88, -188, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (88, 88, -50, -10, -20, 88, -138, -10, -20, PaymentEnums.UNCOLLECTED.name),
        (78, 88, -50, -10, -20, 88, -128, -10, -20, PaymentEnums.UNCOLLECTED.name),
        (138, 88, -50, -10, -20, 88, -188, -10, -20, PaymentEnums.UNCOLLECTED.name),

        (88, 88, 0, 0, 0, 88, 0,-88,  0, PaymentEnums.CLEARING.name),
        (78, 88, 0, 0, 0, 88, 0,-78,  0, PaymentEnums.CLEARING.name),
        (138, 88, 0, 0, 0, 88, 0,-138,  0, PaymentEnums.CLEARING.name),
        (88, 88, -50, -10, -20, 88, -50, -98, -20, PaymentEnums.CLEARING.name),
        (78, 88, -50, -10, -20, 88, -50, -88, -20, PaymentEnums.CLEARING.name),
        (138, 88, -50, -10, -20, 88, -50, -148, -20, PaymentEnums.CLEARING.name),

        (88, 88, 0, 0, 0, 88, 0, 0, -88, PaymentEnums.RECONNECT.name),
        (78, 88, 0, 0, 0, 88, 0, 0, -78, PaymentEnums.RECONNECT.name),
        (138, 88, 0, 0, 0, 88, 0, 0, -138, PaymentEnums.RECONNECT.name),
        (88, 88, -50, -10, -20, 88, -50, -10, -108, PaymentEnums.RECONNECT.name),
        (78, 88, -50, -10, -20, 88, -50, -10, -98, PaymentEnums.RECONNECT.name),
        (138, 88, -50, -10, -20, 88, -50, -10, -158, PaymentEnums.RECONNECT.name),
    )

    @idata(data)
    @unpack
    def test_process(self, amt, bal, uncol, clearing, recon, expect_bal, expect_uncol, expect_clearing, expect_recon, adj_type):
        """
        Test the Debit process function

        :param amt:
        :param bal:
        :param uncol:
        :param clearing:
        :param recon:
        :param expect_bal:
        :param expect_uncol:
        :param expect_clearing:
        :param expect_recon:
        :param adj_type:
        :return:
        """

        a = self.account
        a.accountBalance = bal
        a.uncollected_bal = uncol
        a.clearing_bal = clearing
        a.reconnect_bal = recon
        a.save()

        debitTransaction = transaction_manager.Debit(a, amt)

        dt = debitTransaction.process(adj_type)
        a = Account.objects.get(id=a.id)
        assert_eq(expect_bal, a.accountBalance)
        assert_eq(expect_uncol, a.uncollected_bal)
        assert_eq(expect_clearing, a.clearing_bal)
        assert_eq(expect_recon, a.reconnect_bal)
        assert_eq(dt.accountBalance, a.accountBalance)
        assert_eq(dt.uncollected_bal, a.uncollected_bal)
        assert_eq(dt.clearing_bal, a.clearing_bal)
        assert_eq(dt.reconnect_bal, a.reconnect_bal)
@ddt
class CreditTestCase(TransactionBaseTestCase):
    """Tests transaction_manager.Credit."""

    data = (
        (100, 100, 0, 0, 0, 200, 0, 0, 0, None),
        (100, 100, 0, 0, 0, 200, 0, 0, 0, PaymentEnums.ACCOUNT.name),
        (100, 100, 0, 0, 0, 200, 0, 0, 0, PaymentEnums.RECONNECT.name),
        (100, 100, 0, 0, 0, 200, 0, 0, 0, PaymentEnums.CLEARING.name),
        (100, 100, 0, 0, 0, 200, 0, 0, 0, PaymentEnums.UNCOLLECTED.name),


        (88, 88, -50, 0, 0, 0, -50, 0, 0, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, 0, 0, 10, -50, 0, 0, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, 0, 0, 0, -100, 0, 0, PaymentEnums.ACCOUNT.name),
        (88, 88, -50, -10, -20, 0, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, -10, -20, 10, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, -10, -20, 0, -100, -10, -20, PaymentEnums.ACCOUNT.name),
        (88, 88, -50, -10, -20, 0, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, -10, -20, 10, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, -10, -20, 0, -100, -10, -20, PaymentEnums.ACCOUNT.name),

        (88, 88, -50, 0, 0, 88, -138, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (78, 88, -50, 0, 0, 88, -128, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (138, 88, -50, 0, 0, 88, -188, 0, 0, PaymentEnums.UNCOLLECTED.name),
        (88, 88, -50, -10, -20, 88, -138, -10, -20, PaymentEnums.UNCOLLECTED.name),
        (78, 88, -50, -10, -20, 88, -128, -10, -20, PaymentEnums.UNCOLLECTED.name),
        (138, 88, -50, -10, -20, 88, -188, -10, -20, PaymentEnums.UNCOLLECTED.name),

        (88, 88, 0, 0, 0, 88, 0,-88,  0, PaymentEnums.CLEARING.name),
        (78, 88, 0, 0, 0, 88, 0,-78,  0, PaymentEnums.CLEARING.name),
        (138, 88, 0, 0, 0, 88, 0,-138,  0, PaymentEnums.CLEARING.name),
        (88, 88, -50, -10, -20, 88, -50, -98, -20, PaymentEnums.CLEARING.name),
        (78, 88, -50, -10, -20, 88, -50, -88, -20, PaymentEnums.CLEARING.name),
        (138, 88, -50, -10, -20, 88, -50, -148, -20, PaymentEnums.CLEARING.name),

        (88, 88, 0, 0, 0, 88, 0, 0, -88, PaymentEnums.RECONNECT.name),
        (78, 88, 0, 0, 0, 88, 0, 0, -78, PaymentEnums.RECONNECT.name),
        (138, 88, 0, 0, 0, 88, 0, 0, -138, PaymentEnums.RECONNECT.name),
        (88, 88, -50, -10, -20, 88, -50, -10, -108, PaymentEnums.RECONNECT.name),
        (78, 88, -50, -10, -20, 88, -50, -10, -98, PaymentEnums.RECONNECT.name),
        (138, 88, -50, -10, -20, 88, -50, -10, -158, PaymentEnums.RECONNECT.name),
    )

    data = (
        (88, 88, -50, 0, 0, 176, -50, 0, 0, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, 0, 0, 166, -50, 0, 0, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, 0, 0, 226, -100, 0, 0, PaymentEnums.ACCOUNT.name),
        (88, 88, -50, -10, -20, 176, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, -10, -20, 166, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, -10, -20, 226, -100, -10, -20, PaymentEnums.ACCOUNT.name),
        (88, 88, -50, -10, -20, 0, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (78, 88, -50, -10, -20, 10, -50, -10, -20, PaymentEnums.ACCOUNT.name),
        (138, 88, -50, -10, -20, 0, -100, -10, -20, PaymentEnums.ACCOUNT.name),
    )

    @idata(data)
    @unpack
    def test_process(self, amt, bal, uncol, clearing, recon, expect_bal, expect_uncol, expect_clearing, expect_recon, adj_type):
        """
        Test the Debit process function

        :param amt:
        :param bal:
        :param uncol:
        :param clearing:
        :param recon:
        :param expect_bal:
        :param expect_uncol:
        :param expect_clearing:
        :param expect_recon:
        :param adj_type:
        :return:
        """
        a = self.account
        a.accountBalance = bal
        a.uncollected_bal = uncol
        a.clearing_bal = clearing
        a.reconnect_bal = recon
        a.save()

        creditTransaction = transaction_manager.Credit(a, amt)

        dt = creditTransaction.process(adj_type)
        a = Account.objects.get(id=a.id)
        assert_eq(expect_bal, a.accountBalance)
        assert_eq(expect_uncol, a.uncollected_bal)
        assert_eq(expect_clearing, a.clearing_bal)
        assert_eq(expect_recon, a.reconnect_bal)
        assert_eq(dt.accountBalance, a.accountBalance)
        assert_eq(dt.uncollected_bal, a.uncollected_bal)
        assert_eq(dt.clearing_bal, a.clearing_bal)
        assert_eq(dt.reconnect_bal, a.reconnect_bal)

class TransactionManagerTestCase(base_test.BaseTestOperation):
    def test_debit_transaction_type(self):
        customer = Customer.objects.get(pk=1)
        account = customer.account
        amount = decimal.Decimal(100.00)
        t_mgr = transaction_manager.TransactionManager(account, amount, transaction_manager.TRANSACTION_TYPE.DEBIT)
        self.assertIsInstance(t_mgr.transaction, transaction_manager.Debit)

    def test_credit_transaction_type(self):
        customer = Customer.objects.get(pk=1)
        account = customer.account
        amount = decimal.Decimal(100.00)
        t_mgr = transaction_manager.TransactionManager(account, amount, transaction_manager.TRANSACTION_TYPE.CREDIT)
        self.assertIsInstance(t_mgr.transaction, transaction_manager.Credit)

    def test_process_mobile_payment(self):
        paid_amt = decimal.Decimal(10)
        # Clearing all payment transaction data.
        MobilePayment.objects.all().delete()
        MasterTransactionHistory.objects.all().delete()
        phone_account = PhoneAccount.objects.get(pk=1)
        account = phone_account.account
        mth_mobile_balance(account.accountBalance, account.id)
        update_mth_balances([account.id])
        phone_account = PhoneAccount.objects.get(pk=1)
        account = phone_account.account
        processed_time = datetime.datetime.utcnow()
        account_balance_before_process = account.accountBalance
        transaction_id = 'some_id'
        transaction_manager.TransactionManager.process_mobile_payment(account, transaction_id, paid_amt, processed_time)
        account_balance_after_process = Account.objects.get(id=account.id).accountBalance
        self.assertEquals(account_balance_after_process, account_balance_before_process + paid_amt)
        self.assertEquals(len(UsageCharge.objects.all()), 1)
        mth = None
        # only 1 trans
        for m in MasterTransactionHistory.objects.all():
            mth = m.accountBalance
        self.assertEquals(account_balance_after_process, mth)
        MasterTransactionHistory.objects.all().delete()
        transaction_manager.TransactionManager.process_mobile_payment(account, transaction_id + '1', 300, datetime.datetime.utcnow())
        # only 1 trans
        for m in MasterTransactionHistory.objects.all():
            mth = m.accountBalance
        account_balance_after_process = Account.objects.get(id=account.id).accountBalance
        self.assertEquals(account_balance_after_process, account_balance_before_process + paid_amt + 300)
        self.assertEquals(account_balance_after_process, mth)


class MasterTransactionLoadTestCase(base_test.BaseTestOperation):
    def setUp(self):
        """Create especial customer and circuit or related usage objects for the dynamic usage test from file."""
        super(MasterTransactionLoadTestCase, self).setUp()

    def test_load_usage_charges_update_all(self):
        """ Assumes a single account update all"""

        UsageCharge.objects.all().delete()
        MasterTransactionHistory.objects.all().delete()
        call_command('loaddata', 'ph_operation/tests/fixtures/master_transaction_config.json', verbosity=0)
        MasterTransactionHistory.objects.all().delete()
        UsageCharge.objects.all().update(syncToMasterTransaction=False)
        self.assertEquals(MasterTransactionHistory.objects.count(), 0)
        transaction_manager.TransactionManager.load_usage_charges_to_master_transaction(page_size=1000)
        self.assertEquals(MasterTransactionHistory.objects.count(), 10)

        decimal.getcontext().rounding = decimal.ROUND_FLOOR
        for m in MasterTransactionHistory.objects.order_by('sourceProcessedTime', 'id').all().order_by('pk'):
            if connection.vendor == 'sqlite':
                self.assertEquals(round(m.accountBalance, 2), round(100.00, 2))
                self.assertEquals(round(m.uncollected_bal, 2), round(0.00, 2))
                self.assertEquals(round(m.clearing_bal, 2), round(0.00, 2))
                self.assertEquals(round(m.reconnect_bal, 2), round(0.00, 2))
                continue
            self.assertEquals(round(m.accountBalance, 2), round(100.0, 2))
            self.assertEquals(round(m.accountBalance, 2), round(100.00, 2))
            self.assertEquals(round(m.uncollected_bal, 2), round(0, 2))
            self.assertEquals(round(m.clearing_bal, 2), round(0.00, 2))
            self.assertEquals(round(m.reconnect_bal, 2), round(0.00, 2))
        self.assertEquals(UsageCharge.objects.filter(syncToMasterTransaction=False).count(), 0)

    def test_load_usage_charges_update_limited(self):
        """ Assumes a single account update limited"""

        UsageCharge.objects.all().delete()
        MasterTransactionHistory.objects.all().delete()
        call_command('loaddata', 'ph_operation/tests/fixtures/master_transaction_config.json', verbosity=0)
        MasterTransactionHistory.objects.all().delete()
        UsageCharge.objects.all().update(syncToMasterTransaction=False)

        self.assertEquals(MasterTransactionHistory.objects.count(), 0)
        transaction_manager.TransactionManager.load_usage_charges_to_master_transaction(page_size=1000)
        self.assertEquals(MasterTransactionHistory.objects.count(), 10)
        update_mth_balances([1])

        cnt = 0
        x = 0
        decimal.getcontext().rounding = decimal.ROUND_FLOOR
        uncol_array = [-56.4, -58.0, -44.8, -46.4, -33.2, -34.8, -21.6, -23.2, -10.0, -11.6]
        for m in MasterTransactionHistory.objects.order_by('sourceProcessedTime', 'id').all().order_by('pk'):
            if connection.vendor == 'sqlite':
                self.assertEquals(round(m.accountBalance, 2), round(100.00, 2))
                self.assertEquals(round(m.uncollected_bal, 2), round(0.00, 2))
                self.assertEquals(round(m.clearing_bal, 2), round(0.00, 2))
                self.assertEquals(round(m.reconnect_bal, 2), round(0.00, 2))
                continue
            self.assertEquals(round(m.accountBalance, 2), round(0.0, 2))
            if m.transactionType == 'Credit':
                x += m.amount

            self.assertEquals(round(m.accountBalance, 2), round(x, 2))
            self.assertEquals(round(m.uncollected_bal, 2), round(uncol_array[cnt], 2))
            self.assertEquals(round(m.clearing_bal, 2), round(0.00, 2))
            self.assertEquals(round(m.reconnect_bal, 2), round(0.00, 2))
            cnt += 1
        self.assertEquals(UsageCharge.objects.filter(syncToMasterTransaction=False).count(), 0)
