"""

@Deprecated Module to handle Queen firmware.

"""
import hashlib
import logging
import os
from django.conf import settings

from ph_operation import event_manager
import ph_model.models as ph_model

LOGGER = logging.getLogger(__name__)


def get_files_from_directory(path):
    files = []
    for name in os.listdir(path):
        filePath = os.path.join(path, name)
        if os.path.isfile(filePath):
            files.append(name)
    return files

def getMd5Sum(path, blockSsize=256*128, hr=True):
    """
    blockSize  4096 octets (Default NTFS)
    """
    md5 = hashlib.md5()
    with open(path,'rb') as f:
        for chunk in iter(lambda: f.read(blockSsize), b''):
             md5.update(chunk)
    if hr:
        return md5.hexdigest()
    return md5.digest()



def extract_firmware_version(firmwareName):
    """Extracts frimware version from firmware file name."""
    #TODO(estifanos): update when agreed on version format
    return firmwareName


class QueenFirmwareManager(object):
    """Class to manage circuit logic."""

    @classmethod
    def sync_firmwares(cls):
        """Sync firmwares from firmwares path"""
        firmwarePath = settings.FIRMWARE_PATH
        firmwareFiles = get_files_from_directory(firmwarePath)
        for firmwareFile in firmwareFiles:
            fullFilePath = os.path.join(firmwarePath, firmwareFile)
            md5Sum = getMd5Sum(fullFilePath)
            fileBaseName, fileExtension =  os.path.splitext(firmwareFile)
            version = extract_firmware_version(fileBaseName)
            existingFirmware = ph_model.queen.QueenFirmware.objects.get_or_none(version=version)
            if not existingFirmware:
                firmware =  ph_model.queen.QueenFirmware(version=version, md5Sum=md5Sum, fileBaseName=fileBaseName,
                    fileExtension=fileExtension, path=firmwarePath)
                firmware.save()
                continue
            if existingFirmware.md5Sum != md5Sum:
                existingFirmware.status = ph_model.queen.FirmwareStatus.CHECKSUM_FAILURE.value
                existingFirmware.save()
                checksumData = [{'existing checksum': existingFirmware.md5Sum,
                                 'calculated checksum': md5Sum}]
                event_manager.raise_event(existingFirmware.id,
                    ph_model.event.EventTypeConst.QUEEN_FIRMWARE_CHECKSUM_MISMATCH.value, checksumData)





