# coding=utf-8
"""

@Deprecated Module to handle Queen firmware.

"""
import logging

from ph_model.models import EventDetail
from ph_operation import event_manager
from ph_operation import sms_manager
import ph_model.models as ph_model
from ph_util import date_util

LOGGER = logging.getLogger(__name__)

# Daniel Boucher, Justin Nyantu, Michael Okoth
SUPPORT_SMS = '+254714009968,+254728407358,+254740044200'

CLOVERFIELD_STAFF_SMS = ['+254726020500',  # Julius Machira
                         '+254708086457',  # Mark Eddy
                         '+254720236107',  # James Torori (village agent)
                         '+254720737754',  # Mathew Kimolo
                         '+254725322436',  # Collins Agai
                         '+254700779131',  # Alex Mbolozi
                         '+254715556611',  # (Rik)
                         '+254740021720',  # (Chris)
                         ]


class QueenConnectionMonitor(object):
    """Queen down over an hour"""

    @classmethod
    def check(cls):
        """
        Check for queens not communicating and raise event
        :return: True if detected at least 1 queen
        """
        raisedEvent = False
        now = date_util.get_utc_now()
        active_queens = ph_model.queen.Queen.objects.filter(status=1)
        for queen in active_queens:
            # notify if it's been down for over an hour (may notify twice, depending on interval drift)
            if 5400 > (now - queen.lastContact).total_seconds() >= 3600:
                LOGGER.debug("haven't heard from " + queen.deviceId + " in " + str(now - queen.lastContact))
                message = "haven't heard from " + queen.deviceId + " in " + str(now - queen.lastContact)
                event_manager.raise_event(queen.id, 206, [{'message': message}])
                cls.notify_priority(queen.id)
                raisedEvent = True
        return raisedEvent

    @classmethod
    def notify_priority(cls, queen_id):
        """ special notification for high-priority circuits """
        circuits = ph_model.circuit.Circuit.objects.filter(queen=queen_id)
        priority_circuit_ids = ph_model.circuit.PriorityCircuit.objects.all().values_list('circuit', flat=True)
        for circuit in circuits:
            if circuit.id in priority_circuit_ids:
                priority_circuit = ph_model.circuit.PriorityCircuit.objects.filter(circuit__id=circuit.id)[0]
                LOGGER.debug("PRIORITY NOTIFIED: " + str(priority_circuit.notified))
                # only notify on initial outage - notified flag reset on queen firmware restart
                if priority_circuit.notified is False:
                    event_manager.raise_event(circuit.id, 426)
                    # send sms to customer support
                    phAccount, gateway, smsHost = sms_manager.get_sms_config(sms_manager.DEFAULT_SHORT_CODE_POWERHIVE)
                    message = "Priority Alert! Circuit: " + str(circuit.deviceId) + ", " + str(circuit.queen)
                    gateway.sendMessage(SUPPORT_SMS, message, from_=sms_manager.DEFAULT_SENDER)
                    priority_circuit.notified = True
                    priority_circuit.save()


class GridConnectionMonitor(object):
    """ same thing, but for whole-grid outages """

    @classmethod
    def check(cls):
        """

        Check for entire grids not communicating and raise event
        :return: True if detected at least 1 grid
        """
        raisedEvent = False
        now = date_util.get_utc_now()
        active_grids = ph_model.grid.Grid.objects.filter(status=1)
        lastContact = now
        for grid in active_grids:
            queens = ph_model.queen.Queen.objects.filter(grid=grid)
            notifyDown = True
            for queen in queens:
                # if any queen on this grid has successfully communicated in the last 30min, grid likely has power, so don't notify
                if (now - queen.lastContact).total_seconds() < 1800:
                    notifyDown = False

                if lastContact is now:
                    lastContact = queen.lastContact
                elif queen.lastContact > lastContact:
                    lastContact = queen.lastContact

            if notifyDown is True and (now - lastContact).total_seconds() < 3600:
                last_event = EventDetail.objects.all().filter(itemValue=grid.name + " appears to be down").order_by('id').last()
                if (now - last_event.event.collectTime).total_seconds() > 3 * 60 * 60:
                    LOGGER.debug(grid.name + " appears to be down")
                    message = grid.name + " appears to be down"
                    event_manager.raise_event(grid.id, 100, [{'message': message}])
                    # staff sms notifications for cloverfield only
                    if grid.project.name == "Cloverfield":
                        phAccount, gateway, smsHost = sms_manager.get_sms_config(sms_manager.DEFAULT_SHORT_CODE_POWERHIVE)
                        sms_to = ','.join(CLOVERFIELD_STAFF_SMS)
                        gateway.sendMessage(sms_to, message, from_=sms_manager.DEFAULT_SENDER)
                    raisedEvent = True

        return raisedEvent
