# coding=utf-8
"""
Module to handle tariff management.

Classes:
Tariff: Base class to handle Tariff.
EnergyTariff(Tariff): Energy base tariff
TimeTariff(Tariff): Time based tariff
TariffManager: Run process on usage
TariffManager.process_usages(),
TariffManager.init_tariff(customer, tariff) : Configure/Reconfigure customer with tariff data.
"""
import datetime
import decimal
import pytz

import ph_model.models as ph_model
import logging

import circuit_manager
import event_manager
from django.db import transaction

from ph_util.accounting_util import add_vat, FAR_FUTURE_DATE

LOGGER = logging.getLogger(__name__)


class UnknownTariffException(Exception):
    """Currently powerhive supports ENERGY and TIME tariffs"""
    pass


DECI_PLACE = decimal.Decimal('.0000000000000000001')
DEFAULT_TARIFF_ID = 1

TARIFF_TEMPLATE = 'TARIFF_{}'
TARIFF_CAL_TEMPLATE = 'TARIFF_CAL_{}'
TARIFF_SEGMENT_TEMPLATE = 'TARIFF_SEGMENT_{}'

UPDATE_OPERATIONAL_INTERVAL = 4

MAX_USAGE_CHARGE = 400


class TariffMemoizer(object):
    """Tariff manager class to drive tariff usage calculation."""
    TARIFF_CONFIG = {}
    CUSTOMERS = {}

    def __init__(self):
        self.TARIFF_CONFIG = self.load_tariff_to_memory()

    @classmethod
    def load_tariff_to_memory(cls):
        """Load tariff into memory"""
        if cls.TARIFF_CONFIG:
            return cls.TARIFF_CONFIG
        # TODO(estifanos): Memoize
        tariffs = ph_model.tariff.Tariff.objects.all()
        tariff_cals = ph_model.tariff.TariffCalendar.objects.all()
        tariff_segments = ph_model.tariff.TariffSegment.objects.all()

        tariff_config = {}
        for t in tariffs:
            tariff_config[TARIFF_TEMPLATE.format(t.tariffId)] = t
        for tc in tariff_cals:
            tariff_config[TARIFF_CAL_TEMPLATE.format(tc.calendarId)] = tc
        for ts in tariff_segments:
            tariff_config[TARIFF_SEGMENT_TEMPLATE.format(ts.segmentId)] = ts
        cls.TARIFF_CONFIG = tariff_config

    @classmethod
    def get_tariff_segment(cls, segment_id):
        """
        Fetch the class tariff segment

        :param segment_id: segment_id: ID of tariff segment to fetch
        :return: tariff segment
        """
        return cls.TARIFF_CONFIG.get(TARIFF_SEGMENT_TEMPLATE.format(segment_id))

    @classmethod
    def get_tariff_calendar(cls, calendar_id):
        """

        :param calendar_id: ID of calendar to fetch
        :return: calendar
        """
        return cls.TARIFF_CONFIG.get(TARIFF_CAL_TEMPLATE.format(calendar_id))

    @classmethod
    def get_tariff(cls, tariff_id):
        """

        :param tariff_id: tariff_id: ID of tariff to fetch
        :return: tariff
        """
        return cls.TARIFF_CONFIG.get(TARIFF_TEMPLATE.format(tariff_id))

    @classmethod
    def set_customer_info(cls, customer, account, circuit, operational_date):
        """
        Set customer data for customer.id

        :param operational_date: date grid went operational
        :param customer: customer to set
        :param account:  the related account
        :param circuit:  the related circuit
        :return:
        """

        cls.CUSTOMERS[customer.id] = (customer, account, circuit, operational_date)

    @classmethod
    def flush_customer_data(cls):
        """Delete all class customers
        """
        cls.CUSTOMERS = {}

    @classmethod
    def get_customer_info(cls, customer_id, now=datetime.datetime.utcnow()):
        """
        Gets customer tariff info from usage. Initially there may not be tariff calendar associated to the customer,
        in that case tariffCalendar defaults to customer tariff.entryTariffCalendar, and tariffSegment to
        tariffCalendar.entryTariffSegment.
        The operational_date returned is the less recent of the 2 operational_dates, grid and queen.

        :param now:         used for testing
        :param customer_id: Db id of customer
        :return: tuple of (customer, circuit, tariff, tariffCalendar, tariffSegment, operational_date)
        """
        customer, account, circuit, operational_date = cls.CUSTOMERS.get(customer_id) or (None, None, None, None)
        if not customer:
            customer = ph_model.customer.Customer.objects.get(id=customer_id)
            account = customer.account
            circuit = customer.circuit
            operational_date = get_operational_date(circuit)
            cls.CUSTOMERS[customer_id] = (customer, account, circuit, operational_date)
        # refresh the operational_date every UPDATE_OPERATIONAL_INTERVAL hours
        # (UPDATE_OPERATIONAL_INTERVAL minute update window may not update every pass)
        elif now.hour % UPDATE_OPERATIONAL_INTERVAL == 0 and now.minute < UPDATE_OPERATIONAL_INTERVAL:
            operational_date = get_operational_date(circuit)
            cls.CUSTOMERS[customer_id] = (customer, account, circuit, operational_date)
        customer_tariff = cls.get_tariff(customer.tariff_id)
        if not customer_tariff:
            # TODO(estifanos):raise event
            # LOGGER.error("Customer tariff not configured")
            return
        customer_tariff_cal = cls.get_tariff_calendar(
            customer.tariffCalendar_id or customer_tariff.entryTariffCalendar_id)

        customer_tariff_segment = cls.get_tariff_segment(
            # TODO entryTariffSegment_id is unresolved
            customer.tariffSegment_id or customer_tariff_cal.entryTariffSegment_id)
        return customer, account, circuit, customer_tariff, customer_tariff_cal, customer_tariff_segment, operational_date


class Tariff(object):
    """Base Tariff object, to identify tariff calendar and tariff segment for a given usage. This accepts default
    calendar and segment but validates to the right tariff calendar and segment."""

    def __init__(self, customer, account, circuit, tariff_calendar, tariff_segment, usage=None, operational_date=FAR_FUTURE_DATE):
        """

        :param operational_date:    if in the future usage will not be charged
        :param customer:            ph_model.Customer
        :param account:             customer account
        :param circuit:             customer circuit
        :param tariff_calendar:     ph_model.TariffCalendar calendar where customer is in.
        :param tariff_segment:      ph_model.TariffSegment segment where customer is in.
        :param usage:               ph_model.Usage usage collected for customer or none to configure customer for current time.

        """
        self.timeZone = customer.municipality.timeZone
        self.usageInterval = ph_model.queen.DEFAULT_USAGE_INTERVAL
        self.usage = usage
        self.processedUsageCharge = None
        self.customer = customer
        self.account = account
        self.circuit = circuit
        self.tariffSegment = tariff_segment
        self.tariffSegmentBeforeProcessed = tariff_segment
        self.tariffCalendar = tariff_calendar
        self.tariffCalendarBeforeProcessed = self.tariffCalendar
        self.customer.tariffCalendar = self.tariffCalendar
        self.customer.tariffSegment = self.tariffSegment
        self.collect_checked = False
        self.operational_date = operational_date
        if usage is not None:
            self.collectTime = datetime.datetime.astimezone(
                usage.collectTime, pytz.timezone(self.timeZone))
        else:
            now = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
            self.collectTime = datetime.datetime.astimezone(now, pytz.timezone(self.timeZone))
        self.lastCollectTime = None
        if customer.lastUsageProcessedTime:
            self.lastCollectTime = datetime.datetime.astimezone(
                customer.lastUsageProcessedTime, pytz.timezone(self.timeZone))

    def is_valid(self):
        """Check against null for all required values."""
        return self.customer and self.account and self.circuit and self.usage and self.processedUsageCharge

    def _update_customer_tariff_cal(self):
        """Helper to update tariff calendar and segment for a customer."""
        self.customer.tariffCalendar = self.tariffCalendar
        self.customer.tariffSegment = self.tariffCalendar.entryTariffSegment
        self.tariffSegment = self.tariffCalendar.entryTariffSegment

    def _update_customer_tariff_segment(self):
        """Helper to update tariff segment."""
        self.customer.tariffSegment = self.tariffSegment

    def set_tariff_calendar(self):
        """Calculates and update tariff calendar according to the tariff configuration and usage collect time."""
        collect_time = datetime.date(self.collectTime.year, self.collectTime.month, self.collectTime.day)
        # TODO unable to reference dateExpires
        cal_expires = self.tariffCalendar and self.tariffCalendar.dateExpires
        if not cal_expires or collect_time <= cal_expires:
            if self.tariffCalendar != self.tariffCalendarBeforeProcessed:
                self._update_customer_tariff_cal()
            return
        # TODO unable to reference nextTariffCal_id
        next_tariff_calendar = TariffMemoizer.get_tariff_calendar(self.tariffCalendar.nextTariffCal_id)
        if next_tariff_calendar:
            self.tariffCalendar = next_tariff_calendar
            self.set_tariff_calendar()

    #  meant to be overridden
    def set_tariff_segment(self):
        """Calculates and update tariff segment according to the tariff configuration and usage collect time."""
        LOGGER.warning('Tariff type not provided.')
        pass

    def calc_usage_charge(self, fixed_only, divisor):
        """Calculates usage charge after finding right calendar and segment.
        :param fixed_only: if false we do not add the perSegmentCharge
        :param divisor: charge partial daily fee
        """
        if fixed_only:
            per_segment_charge = self.tariffSegment.perSegmentCharge / divisor
            #LOGGER.info('assess_daily_fee calc perseg divisor: %s' % str(divisor) + '   ' + str(per_segment_charge))
        else:
            per_segment_charge = 0
        if self.tariffSegment == self.tariffSegmentBeforeProcessed:
            per_segment_charge = decimal.Decimal(0.0)
        # Ignore continuations
        total_charge = \
            per_segment_charge + \
            self.tariffSegment.perWhCharge * self.usage.intervalWh + \
            self.tariffSegment.perVACharge * self.usage.intervalVAmax
        if total_charge > MAX_USAGE_CHARGE:
            total_charge = decimal.Decimal(0)
        return total_charge.quantize(DECI_PLACE, rounding=decimal.ROUND_DOWN)

    def mid_night_passed(self):
        """See if last collection was not today
        2017-01-03 add pass midnight on year or month rollover
        
        :return: true if collect time is greater than last collect time
        """
        if not self.lastCollectTime:
            # First time usage.
            self.lastCollectTime = self.collectTime - datetime.timedelta(seconds=self.usageInterval)
        # convience vars
        ct = self.collectTime
        lct = self.lastCollectTime
        return ct.day > lct.day or ct.month > lct.month or ct.year > lct.year

    def reset_usage_if_midnight(self):
        """Reset usage if passed midnight
        
        :return: always False
        """
        # TODO add customerPaidDailyFee so we know if the customer didn't pay daily fee, we still need to charge it.
        if self.mid_night_passed():
            self.customer.cumulativeDailyUsageWh = 0.0
        # TODO what's the point ?    
        return False

    def process_usage(self, no_db=None, divisor=1, fixed_only=False):
        """Sets valid tariff calendar and segment, calculate charge and update customer's account with new charge.
        and with new va/wa limit from the current assigned tariff segment
        
        :param no_db: true means testing with no DB
        :param divisor: charge partial daily fee
        :param fixed_only: If True, only charge fixed portion of tariff

        :return: calculated charge for usage
        """
        self.set_tariff_calendar()
        # noinspection PyArgumentList
        self.set_tariff_segment(no_db)
        if self.circuit:
            if self.circuit.whLimit != self.tariffSegment.whLimit or self.circuit.vaLimit != self.tariffSegment.vaLimit:
                self.circuit.whLimit = self.tariffSegment.whLimit
                self.circuit.vaLimit = self.tariffSegment.vaLimit

        '''
        Because there might be grid faults that causes circuits to
        register usage even though the customer did not use any
        electricity, we need to make sure we do not charge customer
        for any Wh when the circuit is disabled or the override is enabled.
        '''

        # Ignore continuations
        if self.circuit.overrideSwitchEnabled or \
                self.circuit.switchEnabled != ph_model.circuit.SwitchEnabledStatus.ENABLED.value or \
                self.operational_date > datetime.datetime.utcnow().date():
            charge = 0
        else:
            charge = self.calc_usage_charge(fixed_only, divisor)
        c = charge
        if no_db:
            c = float(c)

        account_balance_before = self.account.accountBalance
        uncollected_before = self.account.uncollected_bal
        if fixed_only:
            per_segment_charge = self.customer.tariff.entryTariffCalendar.entryTariffSegment.perSegmentCharge / divisor
            charge = per_segment_charge
            c = per_segment_charge
        else:
            per_segment_charge = self.tariffSegment.perSegmentCharge / divisor

        #LOGGER.info('assess_daily_fee final perseg divisor: %s' % str(divisor) + '   ' + str(per_segment_charge) + '   ' + str(fixed_only))

        if self.account.accountBalance >= add_vat(c):
            self.account.accountBalance = decimal.Decimal(self.account.accountBalance) - add_vat(c)
        else:
            self.account.uncollected_bal = decimal.Decimal(self.account.uncollected_bal) - add_vat(c)

        self.processedUsageCharge = ph_model.transaction.UsageCharge(
            account=self.account,
            processedTime=self.usage.collectTime,
            usage=self.usage,
            amount=charge,
            accountBalanceBefore=account_balance_before,
            accountBalanceAfter=self.account.accountBalance,
            uncollected_before=uncollected_before,
            uncollected_after=self.account.uncollected_bal,
            perSegmentChargeComponent=per_segment_charge,
            notcollected=(self.account.accountBalance < add_vat(c)))

        self.usage.processed = True
        self.customer.lastUsageProcessedTime = self.collectTime
        return c, self.processedUsageCharge, self.usage.processed


class TimeTariff(Tariff):
    """Time based tariff"""

    @staticmethod
    def _replace_hour_minutes(to_copy, minutes_since_mid_night):
        """
        Helper to get copy of datetime and replace it's hour minutes to @param minutes_since_mid_night.

        :param to_copy: dateTime, to copy the year, month and day.
        :param minutes_since_mid_night: int minutes since mid night
        :return: copy of datetime.
        """
        new_date = to_copy
        new_date = new_date.replace(minute=0, hour=0, second=0, microsecond=0)
        new_date += datetime.timedelta(minutes=minutes_since_mid_night)
        return new_date

    # @Override
    def set_tariff_segment(self, no_db=None):
        """
        Find current tariff segment and set the customer tariff segment

        :param no_db: true is testing without a DB
        :return:
        """
        self.reset_usage_if_midnight()
        # Ignore continuations
        collect_check = \
            self.collectTime.hour * 60 + self.collectTime.minute < self.lastCollectTime.hour * 60 + self.lastCollectTime.minute
        if self.tariffSegment.timeExpires:
            tariff_segment_expires = self._replace_hour_minutes(self.collectTime, self.tariffSegment.timeExpires)
            if self.collectTime > tariff_segment_expires and self.collect_checked is False:
                if no_db is None:
                    self.tariffSegment = ph_model.tariff.TariffSegment.objects.get(
                        pk=self.tariffSegment.nextTariffSegment_id)
                    self.set_tariff_segment(no_db)
        elif collect_check:
            if no_db is None:
                self.tariffSegment = self.tariffCalendar.entryTariffSegment
                # prevent infinite recursion on collect_check reset to entryTariffSegment
                self.collect_checked = True
                self.set_tariff_segment(no_db)
        if self.tariffSegment != self.tariffSegmentBeforeProcessed:
            self._update_customer_tariff_segment()

    def __str__(self):
        return "From str method of TimeTariff: " \
               "\nacc:  %s " \
               "\ncirc: %s " \
               "\ncust: %s" \
               "\nct: %s" \
               "\nlct: %s\n" % (self.account, self.circuit, self.customer, self.collectTime, self.lastCollectTime)


class EnergyTariff(Tariff):
    """Energy usage based tariff"""

    # @Override
    def set_tariff_segment(self):
        """Find current tariff segment and set the customer tariff segment"""
        self.reset_usage_if_midnight()
        if self.customer.cumulativeDailyUsageWh < self.tariffSegment.whExpires:
            if self.tariffSegment != self.tariffSegmentBeforeProcessed:
                self._update_customer_tariff_segment()
            return
        self.tariffSegment = ph_model.tariff.TariffSegment.objects.get(pk=self.tariffSegment.nextTariffSegment)
        self.set_tariff_segment()


# TODO(estifanos): put singelton queue manager to handle process, or make a chrone based task
class TariffManager(object):
    """Tariff manager class to drive tariff usage calculation."""

    @classmethod
    @transaction.atomic
    def _bulk_process_usages(cls, usages, divisor=1, fixed_only=False):
        """
        Helper to process @param usages.

        :param divisor:     divide the fixed fee
        :param fixed_only: If True, only charge fixed portion of tariff
        :param usages:      We can hand in what we want to process
        :return:            list of usage IDs processed
        """

        TariffMemoizer.flush_customer_data()
        processed_usages = []
        duplicate_usages = []
        usage_charges = []
        circuits = {}
        customers = {}
        accounts = {}
        last_usage = None
        cnt = 0
        for usage in usages:
            try:
                # assumes usages is in customer_id, collectTime order
                # guards against meter sending duplicate data
                if last_usage is not None and last_usage.customer_id == usage.customer_id and last_usage.collectTime == usage.collectTime:
                    processed_usages.append(usage.id)
                    duplicate_usages.append(usage.id)
                else:
                    last_usage = usage
                    tariff_handler = cls.process_usage(usage, divisor, fixed_only)
                    if not tariff_handler.is_valid():
                        continue
                    processed_usages.append(tariff_handler.usage.id)
                    usage_charges.append(tariff_handler.processedUsageCharge)
                    TariffMemoizer.set_customer_info(
                        tariff_handler.customer, tariff_handler.account, tariff_handler.circuit, tariff_handler.operational_date)
                    customers[tariff_handler.customer.id] = tariff_handler.customer
                    accounts[tariff_handler.account.id] = tariff_handler.account
                    if tariff_handler.circuit:
                        circuits[tariff_handler.circuit.id] = tariff_handler.circuit
                    cnt += 1
            except Exception as e:
                print("_bulk_process_usages ERROR: ", e.message)
                LOGGER.warning(e)

        if processed_usages:
            ph_model.usage.Usage.objects.filter(id__in=processed_usages).update(processed=True)
        if usage_charges and fixed_only is False:
            ph_model.transaction.UsageCharge.objects.bulk_create(usage_charges)
        for c in customers.values():
            c.save()
        for a in accounts.values():
            a.save()
        for c in circuits.values():
            circuit_manager.CircuitManager.configure_circuit_after_processed_usage(c)
        if len(processed_usages) > 1:
            LOGGER.info('processed_usages {} usage_charges {} duplicate_usages {}'
                        .format(len(processed_usages), len(usage_charges), len(duplicate_usages)))
        return processed_usages, usage_charges, duplicate_usages

    @classmethod
    def process_usage(cls, usage, divisor=1, fixed_only=False):
        """Identifies which tariff type to use, instantiates the tariff and calculate usage.
        :param usage: usage obj
        :param divisor: charge partial daily fee
        :param fixed_only: If True, only charge fixed portion of tariff
        :raises: UnknownTariffException for unrecognized tariff types.
        """
        TariffMemoizer()
        customer, account, circuit, tariff, tariff_calendar, tariff_segment, operational_date = \
            TariffMemoizer.get_customer_info(usage.customer_id)
        tariff_type = tariff_segment.tariffType.upper()
        if tariff_type == 'TIME':
            tariff_handler = TimeTariff(customer, account, circuit, tariff_calendar, tariff_segment, usage, operational_date)
        elif tariff_type == 'ENERGY':
            tariff_handler = EnergyTariff(customer, account, circuit, tariff_calendar, tariff_segment, usage, operational_date)
        else:
            raise UnknownTariffException()

        # noinspection PyArgumentEqualDefault
        tariff_handler.process_usage(None, divisor, fixed_only)
        return tariff_handler

    @classmethod
    def process_usages(cls, usages=None, divisor=1):
        """Processed for all unprocessed usage in the database.
        :param usages:  QuerySet
        :param divisor: charge partial daily fee
        """
        try:
            if usages is not None:
                return cls._bulk_process_usages(usages, divisor, True)
            else:
                return cls._bulk_process_usages(ph_model.usage.Usage.unprocessed.all())
        except Exception as e:
            LOGGER.error(e)

    # TODO(estifanos): Remove after performance tune task
    @classmethod
    def _process_usages_with_profiling(cls):
        """Processed for all unprocessed usage in the database with performance profiling.
        This should be used only when testing performance."""
        # noinspection PyCompatibility
        import cProfile
        import pstats
        # noinspection PyCompatibility
        import StringIO
        pr = cProfile.Profile()
        pr.enable()
        cls.process_usages()
        pr.disable()
        s = StringIO.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())

    @classmethod
    def process_usages_by_circuits(cls, circuit_ids):
        """
        TODO: if this ever gets used we will have to apply the fixed charge logic
        Process usages by circuit ids.

        :param circuit_ids: circuit_ids: list of pk of circuits
        :return:
        """
        """
        Arguments:
            circuit_ids: list of pk of circuits
        """
        usages = ph_model.usage.Usage.unprocessed_no_limit.all().filter(
            circuit_id__in=circuit_ids, processed=False).order_by('collectTime')
        return cls._bulk_process_usages(usages)

    @classmethod
    def process_usages_by_customers(cls, customer_ids):
        """
        TODO: if this ever gets used we will have to apply the fixed charge logic
        Process usages by customer ids.

        :param customer_ids: customer_ids: list of pk of customers
        :return:
        """

        return cls._bulk_process_usages(ph_model.usage.Usage.unprocessed_no_limit.filter(
            customer_id__in=customer_ids, processed=False).order_by('collectTime'))

    @classmethod
    def init_tariff(cls, customer, tariff_id=None):
        """
        Configure/Reconfigure customer's tariff without usage.

        :param customer: customer object
        :param tariff_id: id of customer tariff
        :return: Updated customer ph_model.customer.Customer
        """

        tariff_id = tariff_id or DEFAULT_TARIFF_ID
        tariff = ph_model.tariff.Tariff.objects.get(tariffId=tariff_id)
        tariff_calendar = tariff.entryTariffCalendar
        tariff_segment = tariff_calendar.entryTariffSegment
        circuit = customer.circuit
        account = customer.account
        if customer.tariff != tariff:
            customer.tariff = tariff
            customer.tariffCalendar = tariff_calendar
            customer.tariffSegment = tariff_segment
            customer.save()
            tariff_history = ph_model.customer.CustomerTariffHistory(tariff=tariff, customer=customer)
            tariff_history.save()
            event_manager.raise_event(
                customer.id,
                ph_model.event.EventTypeConst.CUSTOMER_TARIFF_CHANGE.value,
                [{'Existing Tariff Id': customer.tariff},
                 {'New Tariff Id': tariff.tariffId}])
            LOGGER.info('Tariff Change for customer {}, from {} to {}'.format(
                customer.customerId, customer.tariff, tariff))
        # This code used to blowup if we pass in an unknown tariff type, so default to TIME tariff
        if tariff_segment.tariffType.upper() == 'ENERGY':
            tariff_handler = EnergyTariff(customer, account, circuit, tariff_calendar, tariff_segment)
        else:
            tariff_handler = TimeTariff(customer, account, circuit, tariff_calendar, tariff_segment)

        tariff_handler.set_tariff_calendar()
        tariff_handler.set_tariff_segment()
        return tariff_handler.customer


def get_operational_date(circuit):
    """
    Get operational date, which is the earlier of the grid or queen operational date

    :param circuit: Used to access grid and queen
    :return: The less recent operational_date
    """
    if circuit.queen.grid.operational_date < circuit.queen.operational_date:
        return circuit.queen.grid.operational_date
    else:
        return circuit.queen.operational_date
