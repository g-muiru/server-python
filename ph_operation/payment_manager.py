# coding=utf-8
"""Module to provide mobile money payment push service strategy."""

import datetime
import logging
import pytz

import ph_model.models as ph_model
from ph_operation import event_manager
import transaction_manager
import decimal
import phonenumbers
from django.db import IntegrityError

from ph_util.accounting_util import PaymentEnums

LOGGER = logging.getLogger(__name__)


# noinspection PyBroadException,PyPep8
def _format_phone_number(pn, country=None):
    try:
        pn = phonenumbers.parse(pn, country)
    except:
        return None
    if not (phonenumbers.is_valid_number(pn) and phonenumbers.is_possible_number(pn)):
        return None
    return phonenumbers.format_number(pn, phonenumbers.PhoneNumberFormat.E164)


# noinspection PyUnusedLocal,PyUnusedLocal
class MpesaPaymentManager(object):
    """Mpesa payment service manager."""

    # noinspection PyBroadException,PyUnusedLocal,PyPep8
    @classmethod
    def validate_payment_request(cls, transType, transId, transTime, transAmount, businessShortCode,
                                 msisdn, kycInfo, serviceProvider, billRefNumber=None, invoiceNumber=None):
        """
        Validate a MPESA transaction
        :param transType:
        :param transId:
        :param transTime:
        :param transAmount:
        :param businessShortCode:
        :param msisdn:
        :param kycInfo:
        :param serviceProvider:
        :param billRefNumber:
        :param invoiceNumber:
        :return:
        """
        payment_type = PaymentEnums.ACCOUNT.name
        mmProvider = ph_model.mobile_payment_provider.NetworkServiceProvider.objects.get(serviceProvider=serviceProvider)
        countryCode = mmProvider.countryCode
        payerPhoneNumber = _format_phone_number(msisdn, countryCode)
        payeeNumber = None
        useBillRef = False
        if billRefNumber:
            useBillRef = True
            if len(billRefNumber) > 8:
                payeeNumber = _format_phone_number(billRefNumber, countryCode)
            else:
                billRefNumber = billRefNumber.replace(" ", "")
                if '*' in billRefNumber:
                    if billRefNumber[0] == '*' and billRefNumber[-1] == '*':
                        payment_type = PaymentEnums.RECONNECT.name
                    elif billRefNumber[0] == '*':
                        payment_type = PaymentEnums.UNCOLLECTED.name
                    elif billRefNumber[-1] == '*':
                        payment_type = PaymentEnums.CLEARING.name
                    payeeNumber = billRefNumber.replace("*", "")
                else:
                    payeeNumber = billRefNumber
        if not useBillRef:
            payeeNumber = payerPhoneNumber
        intended_account_number = payeeNumber
        customerAccount = None
        if intended_account_number:
            if len(intended_account_number) == 13:
                phoneAccount = ph_model.account.PhoneAccount.objects.get_or_none(mobileMoneyNumber=intended_account_number)
                if phoneAccount:
                    customerAccount = phoneAccount.account
            else:
                try:
                    customerAccount = ph_model.account.Account.objects.get_or_none(id=intended_account_number)
                except:
                    customerAccount = None

        transTime = datetime.datetime.strptime(transTime, '%Y%m%d%H%M%S').replace(tzinfo=pytz.UTC)

        c2bpvr = ph_model.mobile_payment_provider.C2BPaymentValidationRequest(
            paymentServiceProvider=mmProvider,
            transId=transId,
            transTime=transTime, transAmount=decimal.Decimal(transAmount),
            billRefNumber=billRefNumber,
            invoiceNumber=invoiceNumber, payerPhoneNumber=payerPhoneNumber,
            firstName=kycInfo.get('firstName'),
            middleName=kycInfo.get('middleName'), lastName=kycInfo.get('lastName'),
            payment_type=payment_type)

        if transType:
            c2bpvr.transType = transType
        if customerAccount:
            c2bpvr.account = customerAccount
            c2bpvr.responseCode = ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.SUCCESS.value
            c2bpvr.responseDesc = ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusDesc.SUCCESS.value
            c2bpvr.status = ph_model.mobile_payment_provider.C2BPaymentValidationRequestStatus.PENDING.value
        else:
            c2bpvr.responseCode = ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.FAILURE_UNKNOWN_PAYEE.value
            c2bpvr.responseDesc = ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusDesc.FAILURE_UNKNOWN_PAYEE.value
            c2bpvr.status = ph_model.mobile_payment_provider.C2BPaymentValidationRequestStatus.DECLINED.value
        try:
            c2bpvr.save()
        except IntegrityError:
            event_manager.raise_event(
                mmProvider.id,
                ph_model.event.EventTypeConst.NETWORK_SERVICE_DUPLICATE_TRANSACTION.value,
                [{'detail': serviceProvider + ' Validation Request for Existing Transaction {}'.format(transId)}])

            return (ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.FAILURE_DUPLICATE_TRANSACTION.value,
                    'duplicate transaction in c2b validation %s' % transId)
        if c2bpvr.responseCode == ph_model.mobile_payment_provider.C2BPaymentValidationRequestResponseStatusCode.SUCCESS.value:
            return c2bpvr
        else:
            event_manager.raise_event(mmProvider.id,
                                      ph_model.event.EventTypeConst.NETWORK_SERVICE_UNKNOWN_CUSTOMER_PAYMENT.value,
                                      [{'detail': serviceProvider + ' Unknown customer payment validation request - payer: %s payee: %s' % tuple(
                                          map(str, (msisdn, billRefNumber)))}])
            return c2bpvr

    @classmethod
    def confirm_payment_request(cls, transId, c2bPaymentValidationId, serviceProvider):
        """
        Confirm a payment has completed
        :param transId:
        :param c2bPaymentValidationId:
        :param serviceProvider:
        :return:
        """
        response = ''
        c2bpvr = ph_model.mobile_payment_provider.C2BPaymentValidationRequest.objects.get_or_none(transId=transId)
        if not c2bpvr:
            mmProvider = ph_model.mobile_payment_provider.NetworkServiceProvider.objects.get(serviceProvider=serviceProvider)
            response = serviceProvider + ' Payment confirmation sent without prior payment validation, for transaction {}'.format(transId)
            event_manager.raise_event(mmProvider.id,
                                      ph_model.event.EventTypeConst.NETWORK_SERVICE_FAILURE.value,
                                      [{'detail': response}])
        elif not c2bpvr.account:
            response = serviceProvider + ' Unknown customer {} payment confirmation request'.format(c2bpvr.payerPhoneNumber)
            event_manager.raise_event(c2bpvr.paymentServiceProvider.id,
                                      ph_model.event.EventTypeConst.NETWORK_SERVICE_UNKNOWN_CUSTOMER_PAYMENT.value,
                                      [{'detail': response}])
        else:
            c2bpvr.status = ph_model.mobile_payment_provider.C2BPaymentValidationRequestStatus.VERIFIED.value
            c2bpvr.save()
            response = 'OK'
            try:
                mmProvider = ph_model.mobile_payment_provider.NetworkServiceProvider.objects.get(serviceProvider='MPESA')
                transaction_manager.TransactionManager.process_mobile_payment(
                    c2bpvr.account, c2bpvr.transId, c2bpvr.transAmount, c2bpvr.transTime, c2bpvr.payment_type, mmProvider)
            except transaction_manager.DuplicateTransactions:
                # TODO: may need to change this logic,PyUnboundLocalVariable
                # noinspection PyUnboundLocalVariable
                event_manager.raise_event(mmProvider.id,
                                          ph_model.event.EventTypeConst.NETWORK_SERVICE_DUPLICATE_TRANSACTION.value,
                                          [{'detail': serviceProvider + ' Validation Request for Existing Transaction {}'.format(transId)}])
        return response
