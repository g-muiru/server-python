# coding=utf-8
"""Import fulcrum customer data"""

import logging
import fulcrum

import ph_model.models as ph_model
from ph_util.date_util import str_to_dt
from powerhive.common_settings import FULCRUM_API_TOKEN
from datetime import datetime
import time
import pytz

LOGGER = logging.getLogger(__name__)
_QUESTION_KEYS = ('description', 'label', 'data_name', 'key')
_FORM_KEYS = ('elements', 'created_at', 'updated_at', 'name', 'description')
_RECORD_KEYS = ('latitude', 'longitude', 'form_id', 'form_values', 'id', 'created_at', 'updated_at')
_FORMAT_STRING = '%Y-%m-%dT%H:%M:%SZ'


def ordered(obj):
    """

    :param obj:
    :return:
    """
    if isinstance(obj, dict):
        return sorted((k, ordered(v)) for k, v in obj.items())
    if isinstance(obj, list):
        return sorted(ordered(x) for x in obj)
    else:
        return obj


def subtract_keys(data, subkeys):
    """

    :param data:
    :param subkeys:
    :return:
    """
    # noinspection PyCompatibility
    return {i: j for i, j in data.iteritems() if i not in set(subkeys)}


def to_tz(ts):
    """

    :param ts:
    :return:
    """
    return datetime.strptime(ts, _FORMAT_STRING).replace(tzinfo=pytz.UTC)


def save_question_data(qobj, q):
    """

    :param qobj:
    :param q:
    """
    qobj.description = q['description']
    qobj.label = q['label']
    qobj.data_name = q['data_name']
    qobj.active = True
    qobj.data = subtract_keys(q, _QUESTION_KEYS)
    qobj.save()


def save_questions_new(questions, form):
    """

    :param questions:
    :param form:
    """
    for q in questions:
        qobj = ph_model.fulcrum.FulcrumQuestion(key=q['key'], form=form)
        save_question_data(qobj, q)


def save_questions_existing(questions, form):
    """

    :param questions:
    :param form:
    """
    for q in questions:
        qobj = ph_model.fulcrum.FulcrumQuestion.objects.get(key=q['key'], form=form)
        same = True
        if q['description'] != qobj.description or q['label'] != qobj.label or qobj.data_name != q['data_name']:
            same = False
        if same and ordered(qobj.data) != subtract_keys(q, _QUESTION_KEYS):
            same = False
        if not same:
            qobj.updated = form.updated
            save_question_data(qobj, q)


def update_questions(questions, form):
    """

    :param questions:
    :param form:
    """
    form_questions = form.fulcrumquestion_set.all()
    existing_keys = set([q.key for q in form_questions])
    new_keys = set([q['key'] for q in questions])
    deactivated_questions = existing_keys - new_keys
    for key in deactivated_questions:
        qobj = ph_model.fulcrum.FulcrumQuestion.objects.get(key=key, form=form)
        qobj.active = False
        qobj.save()
    new_questions = [q for q in questions if q['key'] not in existing_keys]
    save_questions_new(new_questions, form)
    existing_questions = [q for q in questions if q['key'] in existing_keys]
    save_questions_existing(existing_questions, form)


def flatten_questions(questions):
    """

    :param questions:
    :return:
    """
    final_questions = []
    for q in questions:
        if 'elements' in q:
            final_questions += flatten_questions(q['elements'])
        else:
            final_questions.append(q)
    return final_questions


def save_form_data(form_data):
    """

    :param form_data:
    """
    updated = []
    ids = []
    for form_json in form_data:
        i = form_json['id']
        ids.append(i)
        form = ph_model.fulcrum.FulcrumForm.objects.get_or_none(id=i)
        if form is None:
            form_updated = False
            form = ph_model.fulcrum.FulcrumForm(id=i)
        elif form.updated != to_tz(form_json['updated_at']):
            form_updated = True
        else:
            updated.append('no need to update form %s' % form.name)
            # LOGGER.info('no need to update form %s' % form.name)
            continue
        form.created = form_json['created_at']
        form.updated = str_to_dt(form_json['updated_at'])
        form.name = form_json['name']
        form.description = form_json['description']
        questions = form_json['elements']
        questions = flatten_questions(questions)
        form.data = subtract_keys(form_json, _FORM_KEYS)
        form.save()
        if not form_updated:
            save_questions_new(questions, form)
        else:
            update_questions(questions, form)
        if not form_updated:
            LOGGER.info('saved new form %s' % form.name)
        else:
            LOGGER.info('updated form %s' % form.name)
    return updated, ids


def get_form_ids(form_data):
    """
    Get the id's of all fulcrum forms

    :param form_data:
    :return:
    """
    ids = []
    for form_json in form_data:
        i = form_json['id']
        ids.append(i)
    return None, ids

def get_records(record_data):
    """
    Get the id's of all fulcrum forms

    :param form_data:
    :return: Array of records
    """
    records = []
    for rec_json in record_data:
        records.append(rec_json)
    return records, None


def replace_question_keys_with_names(form_values, questions):
    """

    :param form_values:
    :param questions:
    :return:
    """
    ret = {}
    for i, j in form_values.items():
        if i in questions:
            ret[questions[i]] = j
        else:
            ret[i] = j
    return ret


# noinspection PyUnusedLocal
def save_record_data(record_data):
    """

    :param record_data:
    """
    updated = []
    ids = []
    forms = {}
    form_questions = {}
    for record_json in record_data:
        form_id = record_json['form_id']
        if form_id not in forms:
            form = ph_model.fulcrum.FulcrumForm.objects.get(id=form_id)
            forms[form_id] = form
            form_questions[form_id] = form.fulcrumquestion_set.all()
            form_questions[form_id] = {q.key: q.data_name for q in form_questions[form_id]}
        form = forms[form_id]
        questions = form_questions[form_id]
        i = record_json['id']
        ids.append(i)
        record = ph_model.fulcrum.FulcrumRecord.objects.get_or_none(id=i)
        if record is None:
            # noinspection PyUnusedLocal
            record_updated = False
            record = ph_model.fulcrum.FulcrumRecord(id=i, form=form)
        elif record.updated != to_tz(record_json['updated_at']):
            record_updated = True
        else:
            updated.append('no change in record %s on form %s' % (i, form.name))
            # LOGGER.debug('no change in record %s on form %s' % (i, form.name))
            continue
        record.created = record_json['created_at']
        record.updated = str_to_dt(record_json['updated_at'])
        record.form_data = replace_question_keys_with_names(record_json['form_values'], questions)
        if record_json['latitude']:
            record.latitude = float(record_json['latitude'])
            record.longitude = float(record_json['longitude'])
        record.metadata = subtract_keys(record_json, _RECORD_KEYS)
        record.save()
        if not record_updated:
            #    LOGGER.debug('saved new record %s on form %s' % (i, form.name))
            updated.append('saved new record %s on form %s' % (i, form.name))
        else:
            #    LOGGER.debug('updated record %s on form %s' % (i, form.name))
            updated.append('updated record %s on form %s' % (i, form.name))
    return updated, ids


def do_query(to_search, save_method, item_key, updated_since=None, form_id=None):
    """

    :param to_search:
    :param save_method:
    :param item_key:
    :param form_id:     If not None only get data for the specified form
    :param updated_since:
    """
    found_stuff = False
    current_page = 1
    updated = ''
    ids = []
    while not found_stuff:
        url_params = {'current_page': current_page}
        if updated_since is not None:
            url_params['updated_since'] = updated_since
        if form_id is not None:
            url_params['form_id'] = form_id
        stuff = to_search.search(url_params=url_params)
        total_pages = stuff['total_pages']
        if current_page == total_pages or not total_pages:
            found_stuff = True
        stuff = stuff[item_key]
        updated, ids = save_method(stuff)
        cnt = 0
        if ids is not None:
            cnt = str(len(ids))
        elif updated is not None:
            cnt = str(len(updated))
        LOGGER.info('saved %s   %s  from page %d, total_pages %d' % (cnt, item_key, current_page, total_pages))
        if current_page > 1000:
            LOGGER.error('fulcrum importer went out of control!!')
            break
        current_page += 1
    return updated, ids


def run_import():
    """
    Run the import of  and records
    """
    client = fulcrum.Fulcrum(key=FULCRUM_API_TOKEN)
    updated_since = int(time.time()) - 3600 * 24 * 3
    last_updated_record = ph_model.FulcrumRecord.objects.latest('updated')
    last_updated_time = int(time.mktime(last_updated_record.updated.timetuple()))
    if last_updated_time < updated_since:
        updated_since = last_updated_time
    do_query(client.forms, save_form_data, 'forms')
    updated, ids = do_query(client.forms, get_form_ids, 'forms')
    for i in ids:
        do_query(client.records, save_record_data, 'records', updated_since, i)
