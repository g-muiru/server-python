# coding=utf-8
""""
List of cron job for this applications.
Run by: python manage.py runcrons, Needs to be run by os cron runner.
E.g in linux
crontab -e
# Add line
1 2 3 4 5 /path/to/manage.py runcrons
"""

import datetime
import logging

import pytz
from django.db import models
import django_cron

import customer_manager
import fulcrum_customer_manager
import event_manager
import loan_manager
import sms_manager
import tariff_manager
import transaction_manager
import fulcrum_import_manager
import statement_manager
import circuit_manager
import queen_monitor
from ph_model.models import Queen
from ph_model.models.transaction import update_mth_balances
from ph_operation.kuku_poa import KukuSignUps, KukuDistr
from ph_util import email_util
# this lognoise is fixed in a later version of django_cron, suppressing the msg til we can upgrade
import warnings
from ph_util.db_utils import assess_daily_fee_grid
from ph_util.never_neg_tariff_utils import process_uncollected

LOGGER = logging.getLogger(__name__)

warnings.filterwarnings(
    u'ignore',
    message=u'DateTimeField CronJobLog.start_time received a naive datetime',
    category=RuntimeWarning,
)

DAILY_IN_MINUTES = 1440
RETRY_AFTER_FAILURE_MINS = 15


class ProcessUsageTariff(django_cron.CronJobBase):
    """Run tariff of unprocessed usages."""

    RUN_EVERY_MINS = 1
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-operation-process-usage'

    # noinspection PyMethodMayBeStatic,PyMissingOrEmptyDocstring
    def do(self):
        tariff_manager.TariffManager.process_usages()
        transaction_manager.TransactionManager.load_usage_charges_to_master_transaction()


class ProcessBalances(django_cron.CronJobBase):
    """Sync balances in mtd account and uc tables"""

    RUN_EVERY_MINS = 1
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-operation-sync-balances'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """Update balances"""
        # x = utcnow().datetime.minute
        LOGGER.info(transaction_manager.TransactionManager.send_payment_sms(update_mth_balances()[3]))

        # Airtel has been broken for a long time
        # if x % 31 == 0:
        # airtel_payment_manager.AirtelPaymentManager.process_new_payments(lookBackSeconds=3600 * 24)


class QueenConnectionMonitor(django_cron.CronJobBase):
    """raise event if active queen lastContact > 60 minute"""

    RUN_EVERY_MINS = 28
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-queen-connection-monitor'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """process queens"""
        queen_monitor.QueenConnectionMonitor.check()


class GridConnectionMonitor(django_cron.CronJobBase):
    """raise event if every queen on active grid has lastContact > 60 minute"""

    RUN_EVERY_MINS = 29
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-grid-connection-monitor'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """process grids"""
        queen_monitor.GridConnectionMonitor.check()


class SyncInboundSMS(django_cron.CronJobBase):
    """Sync inbound smss."""

    RUN_EVERY_MINS = 1
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-operation-sync-inboud-sms'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """Sync inbound sms."""
        sms_manager.CustomerSMSManager.fetch_incoming_sms_message()


class DeductLoan(django_cron.CronJobBase):
    """update loan payments.
    ./manage.py runcrons ph_operation.cron.DeductLoan --prod --force
    """

    RUN_EVERY_MINS = 240
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-operation-deduct-loan'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """deduct loan."""
        loan_manager.LoanManager.repay_due_loans()


class CustomerDailyUpdate(django_cron.CronJobBase):
    """Update miscellaneous customer data which needs to be updated daily of customer timezone."""

    RUN_EVERY_MINS = 5
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-daily-customer-data-update'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """Update customer data."""
        customer_manager.CustomerManager.update_customer_daily_config_data()


class CloverfieldCustomerImport(django_cron.CronJobBase):
    """Creates customer accounts from fulcrum service contract records"""

    RUN_EVERY_MINS = 15
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-cloverfield-customer-import'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """Update customer data."""
        fulcrum_customer_manager.CloverfieldCustomerManager().run()
        KukuSignUps.update_all_kukupoa()


class ProcessPaidDeposits(django_cron.CronJobBase):
    """
    moves paying customers from POTENTIAL to PAID_DEPOSIT
    updates balances based on pending transactions
    """

    RUN_EVERY_MINS = 25
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-process-paid-deposits'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """Update customer data."""
        customer_manager.PotentialCustomerManager.process_paid_connection_fees()
        # this does not need to run that often
        KukuDistr.update_last_pickup()


class ImportFromFulcrum(django_cron.CronJobBase):
    """ imports all of the powerhive data from fulcrum """
    RUN_EVERY_MINS = 15
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-fulcrum-import'

    # noinspection PyMethodMayBeStatic,PyMissingOrEmptyDocstring
    def do(self):
        fulcrum_import_manager.run_import()


class UpdateMasterTransaction(django_cron.CronJobBase):
    """ Assess the daily fees """

    code = 'ph-master-transaction-update'
    RUN_EVERY_MINS = 60
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    EMAIL_SUBJECT = 'Daily  powerhive cron job log summary!'

    def do(self, mins=RUN_EVERY_MINS):
        """
        Assess daily fee, multiple times in a day --- deduct some part of the daily fee
        :return:
        """

        assess_daily_fee_grid(1440 / mins)


class ClearCircuitOverrides(django_cron.CronJobBase):
    """
    Clears test mode from circuits at noon
    To run:  python manage.py runcrons "ph_operation.cron.ClearCircuitOverrides"
    """
    RUN_AT_TIMES = ['12:00']
    schedule = django_cron.Schedule(run_at_times=RUN_AT_TIMES)
    code = 'ph-clear-overrides'

    # noinspection PyMethodMayBeStatic,PyMissingOrEmptyDocstring
    def do(self):
        # the excluded queens are not turned off, they are actually turned on
        # Phils grid = 25 (Philippines)  Kirwa site C = 13
        #  exclude_queens = Queen.objects.filter(grid_id__in=[25, 13])
        # Currently only Phils grid
        exclude_queens = Queen.objects.filter(grid_id=25)

        if exclude_queens:
            exclude_queens = [q.deviceId for q in exclude_queens]
        circuit_manager.CircuitManager.set_clear_circuit_overrides(exclude_queens)


class SendReminderSMS(django_cron.CronJobBase):
    """reminds customers to pay their connection fee once per x days"""

    RUN_AT_TIMES = ['08:00']
    schedule = django_cron.Schedule(run_at_times=RUN_AT_TIMES)
    code = 'ph-send-reminder-sms'

    # noinspection PyMethodMayBeStatic,PyMissingOrEmptyDocstring
    def do(self):
        # noinspection PyPep8Naming
        SEND_EVERY_DAYS = 7
        customer_manager.PotentialCustomerManager.send_connection_fee_reminders(SEND_EVERY_DAYS)


class NotifyCronJobStatus(django_cron.CronJobBase):
    """Sends cron job status
       ./manage.py runcrons ph_operation.cron.NotifyCronJobStatus --prod --force
    """

    code = 'cron-jobs-email-notificatoin'
    RUN_EVERY_MINS = DAILY_IN_MINUTES
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    LOOK_BACK_DAYS = 1
    EMAIL_SUBJECT = 'Daily  powerhive cron job log summary!'

    def do(self):
        """
        email cron log once per day
        :return:
        """
        look_back_time = datetime.datetime.utcnow().replace(tzinfo=pytz.utc) - datetime.timedelta(days=self.LOOK_BACK_DAYS)
        cron_job_logs = django_cron.models.CronJobLog.objects.filter(start_time__gte=look_back_time).values(
            'code', 'is_success').annotate(run_since=models.Min('start_time'), counts=models.Count('is_success')
                                           ).order_by('is_success')

        data = {'ph_admins': 'Powerhive Admins', 'cron_job_logs': cron_job_logs}
        email_util.send_template_email(self.EMAIL_SUBJECT, 'cron_job_email_log_template.html', data)


class ProcessExpiredEvents(django_cron.CronJobBase):
    """Expire expired events, update associated hardware to next most severe non expired events."""

    RUN_EVERY_MINS = DAILY_IN_MINUTES
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-operation-process-expired-events'

    # noinspection PyMethodMayBeStatic
    def do(self):
        """Expire events."""
        event_manager.EventManager.process_expired_events()


class MonthlyStatement(django_cron.CronJobBase):
    """generate monthly account statement and send via SMS"""

    RUN_EVERY_MINS = DAILY_IN_MINUTES  # run daily, but only generate statements if last day of month
    schedule = django_cron.Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ph-operation-monthly-statement'

    # noinspection PyMethodMayBeStatic,PyMissingOrEmptyDocstring
    def do(self):
        process_uncollected()
        statement_manager.StatementManager.monthly_statement()
