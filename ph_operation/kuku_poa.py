# coding=utf-8
""" All things chicken"""

from fulcrum import Fulcrum

from ph_model.models import Customer, Account
from ph_operation.fulcrum_import_manager import do_query, get_records
from ph_util.date_util import str_to_dt
from ph_util.test_utils import test_mode
from powerhive.common_settings import FULCRUM_API_TOKEN
import logging

LOGGER = logging.getLogger(__name__)

SIGNUP_FORM_ID = 'c03a8c4e-49fa-476f-a2c7-52e2222fda41'
KP_DISTR_FORM_ID = '1535b67b-702c-4167-aa84-fc31e2c37518'


class KukuUtils:
    """
      Utility functions
    """

    def __init__(self):  # pragma: no cover
        pass

    @staticmethod
    def remove_duplicates(dup_list):
        """
        remove duplicate sub lists

        :param dup_list: List with possible duplicates
        :return:
        """
        b = []
        for sublist in dup_list:
            if sublist not in b:
                b.append(sublist)
        return b


# noinspection LongLine,PyArgumentEqualDefault
class KukuDistr:
    """
    Fulcrum Kuku Poa distribution form functions

    form question keys:

    7017	a051	false	total_number_of_small_chicks_in_compound
    7021	e993	false	is_there_any_additional_information_about_this_customer_you_would_like_to_provide
    7042	6485	false	if_yes_to_previous_question_in_about_how_many_weeks_do_you_expect_the_chickens_to_be_ready
    7565	f1bc	true	circuit
    7024	1f53	true	intermediary_name
    7034	4e75	true	what_is_unsatisfactory_about_the_chickens
    7560	b75f	true	customer_name
    7564	cbc4	true	queen
    7038	d635	true	customer_signature
    7022	705d	true	date_customers_account_was_credited
    7559	ef7d	true	account_id
    7010	30c4	true	number_of_chicks_distributed
    7041	c155	false	can_the_chickens_be_collected_at_a_later_date
    7019	5672	false	are_the_chickens_primarily_caged_or_free_range_
    7040	1e6b	false	add_a_way_to_say_collection_is_delayed_because_kukus_not_ready_but_they_wont_be_charged_calculate_non_delivery_amount_and_have_a_field_for_that_
    7003	b9a2	true	name_of_agent_distributing_chicks
    7036	e371	true	find_customer
    7563	9fe3	true	national_id
    7007	e7eb	true	how_many_weeks_old_are_the_chicks_being_distributed_today
    7009	469c	true	date_ready_for_market
    7014	8359	true	photo_of_customer_card
    7037	b8ff	true	tell_the_customer_the_followingby_signing_this_form_and_collecting_chicks_today_you_agree_to_raise_5_of_these_chicks_for_powerhive_and_deliver_them_to_powerhive_according_to_the_terms_of_the_kuku_poa_customer_contract_powerhive_will_credit_kes_700_to_your_account_within_48_hours
    7023	84c9	true	source_of_funds
    7039	6891	true	name_of_agent_collecting_chickens_for_market
    7028	1c24	true	actual_quantity_chickens_collected
    7029	ec4e	true	non_delivery_quantity
    7033	8deb	true	why_is_the_customer_refusing_to_deliver
    7043	46f8	true	non_delivery_fee_per_chicken
    7044	b4cb	true	total_non_delivery_charges
    7026	ed80	true	date_chickens_collected_from_customer
    7031	723f	true	how_heavy_are_the_chickens_currently_kgs
    7018	7598	false	how_clean_is_the_area_where_chicks_are_kept_including_brooder_cage_and_drinkers
    7004	1a41	false	site_id_old
    7032	1c80	true	why_are_chickens_not_available
    7016	f870	false	total_number_of_midsize_chicks_in_compound
    7561	79fc	true	site_id
    7006	c084	true	distribution_date
    7008	96b5	true	ready_for_market_age_weeks
    7711	7f10	true	the_customer_must_sign_to_confirm_they_understand_the_quantity_delivered_and_any_charges_that_may_be_incurred
    7027	9b7d	true	qty_chicks_to_collect
    7013	0922	true	photo_of_customer_holding_chicks
    7011	02cd	true	number_of_chickens_to_be_collected_per_contract
    7012	dbfa	true	total_amount_to_be_credited_to_the_customers_account
    7035	0cb9	true	date_non_delivery_fee_was_charged
    7020	4080	false	have_a_look_at_the_customers_electric_brooder_is_there_evidence_that_the_customer_uses_the_electric_brooder_regularly
    7005	4ab8	false	customer
    7015	9dbb	false	total_number_of_table_ready_chickens_in_compound
    7562	3da9	true	phone_number
    7025	8d67	true	are_you_collecting_chickens_from_the_seller_today
    7030	e982	true	reason_for_non_delivery
    """

    # meta data keys and example values:

    example_rec = {
        u'vertical_accuracy': None,
        u'updated_at': u'2018-01-21T10:25:33   Z',
        u'updated_duration': 3,
        u'course': None,
        u'assigned_to_id': None,
        u'updated_by_id': u'58cb4555-26d5-4f4d-ba06-96afc3846498',
        u'speed': None,
        u'id': u'7fa0bc3e-433d-42f8-b2a4-71a12f57fe72',
        u'form_id': u'1535b67b-702c-4167-aa84-fc31e2c37518',
        u'created_duration': 194,
        u'altitude': None,
        u'created_by': u'Kuku Poa Village Staff',
        u'form_values': {
            u'd635': {
                u'timestamp': u'2018-01-18T11:19:45         Z',
                u'signature_id': u'97f75074-ff82-4c17-8a9f-c8a8679a1497'
            },
            u'1f53': {
                u'choice_values': [
                    u'Powerhive'
                ],
                u'other_values': [

                ]
            },
            u'e371': [
                {
                    u'record_id': u'db0e17b5-a245-4d89-ba15-95c0b38c6930'
                }
            ],
            u'ef7d': u'1',
            u'f1bc': u'12',
            u'79fc': u'Nyamondo-CF',
            u'c084': u'2222-01-20',
            u'02cd': u'5',
            u'b75f': u'Joseph Ogora',
            u'9fe3': u'316969394',
            u'cbc4': u'18',
            u'dbfa': u'700',
            u'b9a2': {
                u'choice_values': [
                    u'Miram Nyambane'
                ],
                u'other_values': [

                ]
            },
            u'8d67': u'no',
            u'1a41': {
                u'choice_values': [
                    u'Nyamondo'
                ],
                u'other_values': [

                ]
            },
            u'84c9': {
                u'choice_values': [
                    u'HC ADJUST'
                ],
                u'other_values': [

                ]
            },
            u'e7eb': u'6',
            u'0922': [
                {
                    u'caption': None,
                    u'photo_id': u'3b6ebfb0-623c-4f00-892e-e3049e44bf2c'
                }
            ],
            u'96b5': u'24',
            u'469c': u'2018-05-25',
            u'30c4': u'8',
            u'705d': u'2018-01-19',
            u'3da9': u'254704456185'
        },
        u'client_created_at': u'2018-01-18T11:16:32   Z',
        u'version': 4,
        u'latitude': None,
        u'created_location': {
            u'latitude': -0.9564133,
            u'altitude': 1870.0,
            u'longitude': 34.6994567,
            u'horizontal_accuracy': 3.9
        },
        u'project_id': None,
        u'status': u'PEND BUYER PICKUP ',
        u'updated_by': u'PowerHive Admin',
        u'horizontal_accuracy': None,
        u'client_updated_at': u'2018-01-18T20:07:34   Z',
        u'edited_duration': 206,
        u'created_at': u'2018-01-18T20:13:58   Z',
        u'longitude': None,
        u'assigned_to': None,
        u'created_by_id': u'4a2f14f6-d90e-45ce-96de-36f7e5e41e95',
        u'updated_location': {
            u'latitude': -0.6762128,
            u'altitude': None,
            u'longitude': 34.7821921,
            u'horizontal_accuracy': 2599.999
        }
    }

    def __init__(self):  # pragma: no cover
        pass

    account_id = 'ef7d'
    distribution_date = 'c084'

    @staticmethod
    def get_distr_records():  # pragma: no cover
        """
        Return all signup records
        :return:
        """
        client = Fulcrum(key=FULCRUM_API_TOKEN)
        recs, ids = do_query(client.records, get_records, 'records', None, KP_DISTR_FORM_ID)
        return recs

    @staticmethod
    def update_last_pickup(recs=None):
        """
        Update the account last pickup date based on Fulcrum data
        
        :param recs: List with acc_num, updated date string, version number, distribution date
        :return:
        """
        acc_array = []
        if recs is None: # pragma: no cover
            recs = KukuDistr.get_distr_records()
        for r in recs:
            up = r['client_updated_at']
            dist = r['form_values'][KukuDistr.distribution_date]
            ver = r['version']
            acc_num = int(r['form_values'][KukuDistr.account_id])
            acc_array.append((acc_num, up, ver, dist))
        updated_cnt = 0
        accs = []
        if len(acc_array) != 0:
            acc_array = sorted(acc_array, key=lambda element: (element[0], element[2]))
            last_r = acc_array[0]
            last_acc = acc_array[0][0]
            ll = 0
            for r in acc_array:
                if last_acc != r[0]:
                    accs.append(last_r)
                ll += 1
                if ll == len(acc_array):
                    accs.append(r)
                last_acc = r[0]
                last_r = r
            for r in accs:
                # save last pickup
                acc = Account.objects.get_or_none(id=r[0])
                if acc is not None:
                    if acc.kuku_last_pickup != str_to_dt(r[3]).date():
                        updated_cnt += 1
                        acc.kuku_last_pickup = r[3]
                        acc.save()
        LOGGER.info(str(len(accs)) + '  Records, ' + str(updated_cnt) + '  Updated')
        return accs, updated_cnt


# noinspection LongLine,PyArgumentEqualDefault
class KukuSignUps:
    """
    Fulcrum Kuku Poa signup form functions

    form question keys:

    id      key     active  data_name
    7505	06b5	true	what_is_the_persons_mobile_number
    7495	07b7	true	would_you_be_interested_in_signing_up_for_kuku_poa_in_the_future
    7499	0bdb	true	do_you_understand_that_powerhive_will_pay_kes_700_to_your_powerhive_account_in_consideration_for_raising_5_chickens_for_the_company_and_that_after_delivering_5_chickens_from_each_flock_you_may_keep_any_remaining_chickens_for_yourself
    7510	1078	true	do_you_have_a_powerhive_customer_card
    7502	11c7	true	inform_the_customer_that_you_will_now_record_audio_after_you_start_recording_you_will_ask_do_you_understand_and_agree_to_all_of_the_terms_and_conditions_of_the_kuku_poa_agreementthe_customer_must_say_i_name_of_customer_understand_the_kuku_poa_terms_and_conditions_and_agree_to_themthen_stop_the_recording
    7508	1540	true	is_the_customer_a_single_mother_with_young_children
    7516	15f4	true	when_a_customer_must_be_removed_from_kuku_poa_fill_this_section_out_after_changing_the_status_field_to_removed
    7490	1775	true	circuit
    7511	181f	true	please_give_the_names_of_any_self_help_groups_to_which_you_belong
    7515	18b4	true	would_you_like_to_remove_the_customer_from_the_program
    7483	18e8	true	find_customer
    7498	193c	true	do_you_understand_that_failure_to_deliver_5_chickens_that_meet_the_minimum_quality_standard_describedin_the_kuku_poa_agreement_will_result_in_a_charge_of_kes450_for_each_undelivered_chicken
    7503	1a0c	true	begin_recording_audio_and_ask_the_customer_whether_he_or_she_understands_and_agrees_to_all_of_the_terms_and_conditions_in_the_kuku_poa_agreement_await_the_customers_response_then_stop_recording_
    7506	29b0	true	what_is_the_persons_gender
    7513	315e	true	take_a_clear_and_readable_photograph_of_the_signed_kuku_poa_customer_agreement
    7514	3bb3	true	by_signing_my_name_i_agree_to_the_terms_and_conditions_described_above_explain_this_to_the_customer
    7493	3c2f	true	after_reading_the_terms_and_conditions_to_the_customer_ask_him_or_her_the_following_questions
    7481	425f	true	kuku_poa_signup_form
    7517	4dc1	true	reason_for_removal
    7507	50a7	true	what_is_the_persons_highest_level_of_education
    7486	554a	true	site_id
    7492	7400	true	please_read_and_explain_the_kuku_poa_terms_and_conditions_to_the_customer_and_ensure_they_understand_all_terms_and_conditions
    7504	8677	true	who_in_the_household_will_attend_the_training_and_be_responsible_for_your_households_participation_in_the_kuku_poa_program
    7482	9f02	true	name_of_sign_up_agent
    7489	9f70	true	queen
    7519	acaf	true	remarks
    7494	b51f	true	do_you_wish_to_register_for_kuku_poa
    7488	b812	true	national_id
    7512	bee6	true	take_a_photograph_of_the_customers_face
    7484	c1b8	true	account_id
    7485	c8ce	true	customer_name
    7497	ca2a	true	do_you_understand_that_you_are_required_to_deliver_5_chicks_of_each_batch_of_chicks_you_collect_back_to_powerhive_after_4_months_and_that_you_will_receive_no_additional_compensation_upon_delivery
    7500	cb5c	true	do_you_understand_that_you_are_solely_responsible_for_ensuring_the_chickens_stay_alive_healthy_and_grow_to_meet_the_minimum_quality_standard
    7509	cb66	true	how_many_chickens_do_you_currently_own
    7689	d26f	true	removal_date
    7518	ddde	false	why_does_the_customer_wish_to_leave_the_program
    7487	e409	true	phone_number
    7491	e604	true	signup_date
    7690	e6bd	true	system_removed
    7501	f336	true	do_you_understand_that_you_are_required_to_own_a_movable_chick_cage_feeder_and_drinker_before_collecting_your_first_batch_of_chicks
    7496	fe5d	true	instructions_please_thank_the_customer_for_their_time_and_move_to_the_next_customer_submit_the_record_to_fulcrum

    """

    # meta data keys and example values:
    example_res = {u'vertical_accuracy': None, u'updated_at': u'2018-01-22T09:44:54Z', u'updated_duration': 769, u'course': None, u'assigned_to_id': None, u'updated_by_id': u'4a2f14f6-d90e-45ce-96de-36f7e5e41e95', u'speed': None, u'id': u'a36a53ff-f618-4e2b-908e-c05b348d4243', u'form_id': u'c03a8c4e-49fa-476f-a2c7-52e2222fda41', u'created_duration': None, u'altitude': None, u'created_by': u'PowerHive Admin', u'form_values': {u'193c': {u'choice_values': [u'Yes'], u'other_values': []}, u'e604': u'2018-01-19', u'c1b8': u'1', u'315e': [{u'caption': None, u'photo_id': u'66704a3a-fa24-440f-a553-155dea106ca2'}], u'06b5': u'720130950', u'18e8': [{u'record_id': u'47f38356-d08e-4b1e-aff5-44eb918eaa65'}], u'c8ce': u'James Otieno', u'1a0c': [{u'caption': None, u'audio_id': u'dd5e02b1-a06f-48de-b1a0-784c4a5db938'}], u'ca2a': {u'choice_values': [u'Yes'], u'other_values': []}, u'1775': u'16', u'cb5c': {u'choice_values': [u'Yes'], u'other_values': []}, u'1540': u'no', u'8677': {u'choice_values': [u'My spouse'], u'other_values': []}, u'b812': u'7309954', u'b51f': u'yes', u'9f70': u'1', u'cb66': u'40', u'3bb3': {u'timestamp': u'2018-01-22T08:20:40Z', u'signature_id': u'dd68ed3e-6434-4a3e-a618-e32d9576a1ae'}, u'18b4': u'no', u'e409': u'254720130950', u'f336': {u'choice_values': [u'Yes'], u'other_values': []}, u'1078': u'yes', u'0bdb': {u'choice_values': [u'Yes'], u'other_values': []}, u'181f': u'One acre fund', u'9f02': {u'choice_values': [u'Winnie Hyvonne'], u'other_values': []}, u'bee6': [{u'caption': None, u'photo_id': u'14eaf7a9-3941-4511-a04d-29ca9c2418b6'}], u'50a7': {u'choice_values': [u'High school'], u'other_values': []}, u'29b0': {u'choice_values': [u'Female'], u'other_values': []}, u'554a': u'Kirwa site C'}, u'client_created_at': u'2018-01-19T04:50:09Z', u'version': 4, u'latitude': None, u'created_location': None, u'project_id': None, u'status': u'1', u'updated_by': u'Kuku Poa Village Staff', u'horizontal_accuracy': None, u'client_updated_at': u'2018-01-22T08:26:19Z', u'edited_duration': 795, u'created_at': u'2018-01-19T04:50:09Z', u'longitude': None, u'assigned_to': None, u'created_by_id': u'58cb4555-26d5-4f4d-ba06-96afc3846498', u'updated_location': {u'latitude': -0.6751943, u'altitude': None, u'longitude': 34.780046, u'horizontal_accuracy': 19.221}}

    def __init__(self):  # pragma: no cover
        pass

    system_removed = '18b4'
    status = 'status'
    account_id = 'c1b8'

    @staticmethod
    def get_signup_records():  # pragma: no cover
        """
        Return all signup records
        :return:
        """
        client = Fulcrum(key=FULCRUM_API_TOKEN)
        recs, ids = do_query(client.records, get_records, 'records', None, SIGNUP_FORM_ID)
        return recs

    @staticmethod
    def update_record(client, rid, rec): # pragma: no cover
        """
        Update a signup form record

        :param client:  fulcrum client
        :param rid:      of record
        :param rec:     the updated data record
        :return:        True upon success
        """

        if test_mode():
            return True
        try:
            record = client.records.update(rid, rec)
        except Exception as e:
            LOGGER.error('Error Updating record' + e.message)
            LOGGER.error(rid, rec)
            return None
        return record

    @staticmethod
    def update_all_kukupoa(res=None):
        """
        Update all customers based on the signup form records
        :return:
        """

        if res is None: # pragma: no cover
            res = KukuSignUps.get_signup_records()
        acc_removed_status = []
        acc_array = []
        for r in res:
            rem = False
            s = False
            up = r['client_updated_at']
            acc = int(r['form_values'][KukuSignUps.account_id])
            acc_array.append(acc)
            if KukuSignUps.system_removed in r['form_values']:
                if r['form_values'][KukuSignUps.system_removed] != 'no':
                    rem = True
            if r[KukuSignUps.status] == '1':
                s = True
            acc_removed_status.append([acc, rem, s, up, r])
        # remove duplicate accounts
        acc_removed_status = KukuUtils.remove_duplicates(acc_removed_status)
        acc_array = set(acc_array)
        added = 0
        removed = 0
        client = Fulcrum(key=FULCRUM_API_TOKEN)
        for c in Customer.objects.all().filter(account_id__in=acc_array):
            for r in acc_removed_status:
                if r[0] == c.account_id:
                    if r[1] is False and r[2] is True:
                        # update kuku poa id if it does not match the record
                        if c.kuku_poa_id != c.account_id:
                            c.kuku_poa_id = c.account_id
                            c.save()
                            added += 1
                    else:
                        if c.kuku_poa_id is not None:
                            c.kuku_poa_id = None
                            c.save()
                        if r[2] is True:
                            r[4][KukuSignUps.status] = '0'
                            rid = r[4]['id']
                            record = KukuSignUps.update_record(client, rid, r[4])
                            if record is not None:
                                removed += 1
        LOGGER.info(str(len(acc_array)) + '  Records, ' + str(removed) + '  Removed,  ' + str(added) + '  Added')
        return acc_array, removed, added
