# coding=utf-8
"""
Circuit issue detection
"""
import pytz
import enum
from datetime import datetime, timedelta
from ph_operation import event_manager
import ph_model.models as ph_model
from ph_util import mixins


class DetectFaultStatus(mixins.PairEnumMixin, enum.Enum):
    """circuit switch enabled status."""
    NORMAL = 'NORMAL'
    FAULT = 'FAULT'
    RAISED_EVENT = 'RAISED EVENT'


class CircuitAnalyzer(object):
    """
    Various circuit issue detection
    """
    @classmethod
    def __is_event_current(cls, circuit, event_type):
        current_events = ph_model.event.Event.objects.filter(
            itemId=circuit.id,
            eventType=event_type,
            collectTime__gt=datetime.utcnow().replace(tzinfo=pytz.utc) - timedelta(days=1)).count()
        return current_events

    @classmethod
    def __evaluate_to_create_fault_event(cls, circuit, current_events, event_number, interval_threshold):
        if not current_events:
            if circuit.faultCount > interval_threshold:
                event_manager.raise_event(circuit.id, event_number)
                circuit.faultCount = 0
                detect_fault_status = DetectFaultStatus.RAISED_EVENT.value
            else:
                circuit.faultCount += 1
                detect_fault_status = DetectFaultStatus.FAULT.value
            circuit.save()
        else:
            detect_fault_status = DetectFaultStatus.FAULT.value
        return detect_fault_status

    @classmethod
    def detect_fault(cls, circuit, power):
        """

        :param circuit:
        :param power:
        :return:
        """
        event_number = 420
        detection_limit = 20
        interval_threshold = 5

        if circuit.switchEnabled == 'DISABLED' and power > detection_limit:
            event_type = ph_model.event.EventType.objects.get_or_none(eventNumber=event_number)
            current_events = cls.__is_event_current(circuit, event_type)
            detect_fault_status = cls.__evaluate_to_create_fault_event(circuit, current_events, event_number, interval_threshold)
        else:
            detect_fault_status = DetectFaultStatus.NORMAL.value

        return detect_fault_status

    """
    Ignore cases where both the neutral sensor (usage message JSON tag: intervalVAmax) and the hot sensor read below 40 VA (usage message
    JSON tag: intervalVAmaxH).
    If one or both sensors read above 40 VA, generate the event if the absolute value of the difference between the hot and
    neutral sensors is greater than 40 VA and greater than 4% of the larger reading of the two sensors.
    """

    @classmethod
    def detect_groundfault(cls, circuit, neutral, hot):
        """

        :param circuit:
        :param neutral:
        :param hot:
        :return:
        """
        event_number = 427
        interval_threshold = 1
        detection_limit = 40

        if neutral > detection_limit or hot > detection_limit:
            difference = hot - neutral
            va_max = max(hot, neutral)
            if abs(difference) > detection_limit and abs(difference) > (va_max * .04):
                event_type = ph_model.event.EventType.objects.get_or_none(eventNumber=event_number)
                current_events = cls.__is_event_current(circuit, event_type)
                detect_fault_status = cls.__evaluate_to_create_fault_event(circuit, current_events, event_number, interval_threshold)
            else:
                detect_fault_status = DetectFaultStatus.NORMAL.value
        else:
            detect_fault_status = DetectFaultStatus.NORMAL.value

        return detect_fault_status

    @classmethod
    def __evaluate_to_create_meter_issue_event(cls, circuit, current_events, event_number, interval_threshold):
        if not current_events:
            if circuit.meterIssueCount > interval_threshold:
                event_manager.raise_event(circuit.id, event_number)
                circuit.meterIssueCount = 0
                detect_fault_status = DetectFaultStatus.RAISED_EVENT.value
            else:
                circuit.meterIssueCount += 1
                detect_fault_status = DetectFaultStatus.FAULT.value
            circuit.save()
        else:
            detect_fault_status = DetectFaultStatus.FAULT.value
        return detect_fault_status

    @classmethod
    def detect_meter_issue(cls, circuit, power, energy):
        """

        :param circuit:
        :param power:
        :param energy:
        :return:
        """
        event_number = 421
        power_detection_limit = 3
        interval_threshold = 5

        if power > power_detection_limit and energy == 0:
            event_type = ph_model.event.EventType.objects.get_or_none(eventNumber=event_number)
            current_events = cls.__is_event_current(circuit, event_type)
            detect_fault_status = cls.__evaluate_to_create_meter_issue_event(circuit, current_events, event_number, interval_threshold)
        else:
            detect_fault_status = DetectFaultStatus.NORMAL.value

        return detect_fault_status
