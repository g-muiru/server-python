"""
Module to handle Scratch card logic.
"""

import logging
import ph_model.models as ph_model
import re
import transaction_manager
import random
import csv
import datetime
from ph_util import email_util
from django.conf import settings
import decimal


LOGGER = logging.getLogger(__name__)

class ScratchCardManagerException(Exception):
    pass

SCRATCHCARD_BASE_DIR = '/powerhive/scratchcard'
DEFAULT_MANUFACTURER_COMPANY_NAME = 'scratchoff.com'
DEFAULT_PIN_GENERATED_BY = DEFAULT_MANUFACTURER_COMPANY_NAME


def send_pin_as_attached_email(pin_file, metaData, emails=None):
    receivers = emails or settings.POWERHIVE_DEV_TEAM
    email_util.send_template_email('Scratchcard pins generated!', 'scratchcard_pin_list_template.html', metaData,
        receivers=receivers, attachments=[pin_file])


def export_pin(query=None, emails=None):
    """Exports generated scratchcard pins. if @param emails send email as attachment.

    Arguments:
        query: list of dict query data.
        emails: list email to get notification with pin attachments.
    """
    keys = ('controlNumber', 'pin', 'status', 'amount', "country__name")
    if not query:
        query = ph_model.scratchcard.Scratchcard.objects.values(*keys).filter(status=ph_model.scratchcard.ScratchcardStatus.VALID.value).all()
    file_name = '{}/{}_exported_pin.csv'.format(SCRATCHCARD_BASE_DIR, datetime.datetime.now().strftime("%Y_%m_%d_%H_%m_%S"))
    with open(file_name, 'wb') as f:
        w = csv.DictWriter(f, keys)
        w.writeheader()
        for datum in query:
            datum['pin'] = str(datum['pin'])
            datum['amount'] = datum['amount'].quantize(decimal.Decimal('.01'))
            w.writerow(datum)
    if emails:
       send_pin_as_attached_email(file_name, metaData={'size': len(query)}, emails=emails)


def generate_pin_to_db(size=2000, length=12, countryIsoCode='KEN', pinGeneratedBy='powerhive',
                   manufacturer='scratchoff', status=ph_model.scratchcard.ScratchcardStatus.VALID.value,
                   controlNumber=None, amount = None):
    """Returns @param size of rand num."""
    scratches = []
    country = ph_model.country.Country.objects.get(countryIsoCode=countryIsoCode)
    pinGeneratedBy = ph_model.company.Company.objects.get(name=pinGeneratedBy)
    manufacturer = ph_model.company.Company.objects.get(name=manufacturer)
    status = ph_model.scratchcard.ScratchcardStatus.value_of_case_insensitive(status)
    if not controlNumber:
        try:
           latestControlNumber = ph_model.scratchcard.Scratchcard.objects.latest('controlNumber').controlNumber
        except:
           latestControlNumber = None
        if latestControlNumber:
           controlNumber = int(latestControlNumber) +1
        else:
            controlNumber = 1
    amountChoice = amount or [50, 100]
    for s in xrange(size):
        while True:
          pin = str(int(random.uniform(0, 1) * pow(10, length)))
          existingScratch = ph_model.scratchcard.Scratchcard.objects.get_or_none(pin=pin)
          if existingScratch or (len(pin) != length):
              continue
          amount =  random.choice(amountChoice)
          scratches.append( ph_model.scratchcard.Scratchcard(pin=pin, controlNumber=controlNumber,
                       manufacturer=manufacturer, pinGeneratedBy=pinGeneratedBy, country=country, status=status,
                       amount=amount))
          break
    if scratches:
        ph_model.scratchcard.Scratchcard.objects.bulk_create(scratches)
    return controlNumber



def load_pin_data_to_db(file_name):
    """Pin data should be available in specific server location for security purpose, this module will import that to db."""

    with open('{}/{}'.format(SCRATCHCARD_BASE_DIR, file_name)) as csvFile:
        csvReader = csv.DictReader(csvFile)
        for line in csvReader:
            try:
                pin = line['pin']
                amount = line['amount']
                controlNumber = line['control_number']
                countryIsoCode = re.match('^\D*', pin).group()
                country = ph_model.country.Country.objects.get(countryIsoCode=countryIsoCode)
                manufacturer = ph_model.company.Company.objects.get(name=line['manufacturer'])
                pinGeneratedBy = ph_model.company.Company.objects.get(name=line['pin_generated_by'])
                scratchCard = ph_model.scratchcard.Scratchcard(pin=pin, controlNumber=controlNumber,
                   manufacturer=manufacturer, pinGeneratedBy=pinGeneratedBy, amount=amount, country=country)
                scratchCard.save()
            except Exception as e:
                pass
            # TODO(estifanos) Send event with immediate notification of pin generation.


class ScratchcardManager(object):
    """Scratchcard Manager."""

    def __init__(self, customer, pin):
        self.customer = customer
        self.pin = pin
        self.isPinValid = False
        self.scratchcard = ph_model.scratchcard.Scratchcard.objects.get_or_none(pin=self.pin)
        if self.scratchcard and self.scratchcard.status == ph_model.scratchcard.ScratchcardStatus.VALID.value:
            self.isPinValid = True


    def process(self):
        if self.isPinValid:
            pt = transaction_manager.TransactionManager.process_scratch_card_payment(self.customer, self.scratchcard)
            if pt:
                self.scratchcard.status = ph_model.scratchcard.ScratchcardStatus.USED.value
                self.scratchcard.save()
        else:
            #TODO(estifanos) raise event trying to process invalid scratch card
            pass
