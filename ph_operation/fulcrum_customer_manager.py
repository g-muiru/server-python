# coding=utf-8
"""
Module to handle customer logic.

Classes:
.. code-block:: python

    Customer: Wrapper on top of ph_model.customer.Customer..
    CustomerManager: Manager class to run logic on customer.
    CustomerManager.initialize_customer: Initialize new customer, associating ,
"""

import logging
import requests
import json

from django.utils.timezone import make_aware, is_naive

import ph_model.models as ph_model
from ph_model.models.customer import CustomerType
from ph_operation import sms_manager
from ph_util.accounting_util import RESIDENTIAL_CONN_LOAN_AMT, COMMERCIAL_CONN_LOAN_AMT, add_vat, CONN_LOAN_DEPOSIT_AMT
from ph_util import mixins
import enum
from ph_util import phone_number_util
from ph_util import date_util
import datetime
import event_manager
from ph_util.date_util import DELTA_365_DAYS

LOGGER = logging.getLogger(__name__)

# TODO(estifanos): Replace this with powerhive admin google api account, currently using e.gebrehiwot@powerhive's account
GOOGLE_TIME_ZONE_API_URL = \
    "https://maps.googleapis.com/maps/api/timezone/json?location={},{}&timestamp={}&sensor=false&key=AIzaSyAEp0wPpKkMdoGtrJ6bt0XKWv2h43Te2LA"


# noinspection PyPep8Naming
class TIME_ZONE_SERVICE(mixins.PairEnumMixin, enum.Enum):
    """Fulcrum Customer Record Field."""
    GOOGLE = GOOGLE_TIME_ZONE_API_URL


# noinspection PyPep8Naming,PyPep8Naming
def get_time_zone(latitude, longitude, dateTimeUTC, timeZoneService=TIME_ZONE_SERVICE.GOOGLE):
    """
    Get timezone for a particular latitude & longitude
    :param latitude:
    :param longitude:
    :param dateTimeUTC:
    :param timeZoneService:
    :return:
    """
    # noinspection PyBroadException
    try:
        # noinspection PyPep8Naming
        timeStamp = date_util.to_epoch(ph_model.fields.DateTimeUTC().to_python(dateTimeUTC))

        if timeZoneService == TIME_ZONE_SERVICE.GOOGLE:
            url = timeZoneService.value.format(latitude, longitude, timeStamp)
            key = 'timeZoneId'
        # noinspection PyUnboundLocalVariable
        data = json.loads(requests.get(url).content)
        # noinspection PyPep8Naming,PyUnboundLocalVariable
        timeZone = get_value(data, key)
    except:
        return None
    return timeZone


def get_value(d, key, l=0):  # TODO this doesn't belong here
    """
    Used by get_time_zone function

    :param d:
    :param key:
    :param l:
    :return:
    """
    if key in d:
        if l == 0:
            return d[key]
        return d[key], True
    # noinspection PyCompatibility
    for k, v in d.iteritems():
        if isinstance(v, dict):
            v, found = get_value(v, key, l + 1)
            if found:
                if l == 0:
                    return v
                return v, True
    if l == 0:
        return None
    return None, False


class CloverfieldCustomerManager(object):
    """
    Cloverfield specific customer manager
    """
    DEFAULT_COUNTRY = 'Kenya'
    DEFAULT_COUNTRY_CODE = '+254'
    DEFAULT_REGION = 'Cloverfield'
    DEFAULT_TIME_ZONE = date_util.DEFAULT_TIME_ZONE
    DEFAULT_PROJECT = None
    ACCOUNT_TYPE_MAP = {
        'Household': CustomerType.RESIDENTIAL.name,
        'Small Business': CustomerType.COMMERCIAL.name
    }
    # Stima Service Contract Form
    FORM_ID = '8121273d-ab67-4111-acb6-69605da0ceda'
    # New Cloverfield survey questions
    SURVEY_FORM_ID = 'ffc513c1-8b74-487e-ab71-fe1824236b6a'

    CUSTOMER_SMS_MESSAGE = \
        'Thanks for signing up for Powerhive! Pay KShs %s via mPesa (829250) or Airtel Money (22870) to get stima. Your account number is %s'
    KISWAHILI_SMS_MESSAGE = \
        'Lipa shilingi %s kupitia MPesa paybill 829250 au Airtel Money 22870 ili upate stima! Akaunti yako ni %s'

    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
    def __init__(self, defaultTimeZone=None, timeZoneService='GOOGLE', countryCode=None, country=None, region=None, updatedSince=None, project=None):

        if defaultTimeZone:
            self.DEFAULT_TIME_ZONE = defaultTimeZone
        self.timeZoneService = TIME_ZONE_SERVICE.value_of_case_insensitive(timeZoneService)
        if countryCode:
            self.DEFAULT_COUNTRY_CODE = countryCode
        if country:
            self.DEFAULT_COUNTRY = country
        if region:
            self.DEFAULT_REGION = region
        if project:
            self.DEFAULT_PROJECT = project
        if self.DEFAULT_PROJECT is None:
            self.DEFAULT_PROJECT = ph_model.project.Project.objects.get_or_none(name=self.DEFAULT_REGION)
        self.updatedSince = updatedSince or self.get_last_time_updated()

    def get_last_time_updated(self):
        """

        :return:
        """
        # noinspection PyBroadException
        try:
            return ph_model.customer.PotentialCustomerImportHistory.objects.filter(dataSource=self.FORM_ID).latest(
                'lastSyncedSourceUpdatedAtSinceEpoch').lastSyncedSourceUpdatedAtSinceEpoch
        except:
            LOGGER.warning('no import history, returning 0')
            return 0

    def create_grid_if_necessary(self, grid_name, muni):
        """
        If grid does not exist, it is created
        :return: Grid if exists or is created, otherwise None if grid_name is not specified
        """
        if grid_name is None:
            return None
        grid = ph_model.grid.Grid.objects.get_or_none(name=muni.name)
        if grid is None:
            grid = ph_model.grid.Grid.objects.get_or_create(
                name=muni.name,
                village_name=grid_name,
                project=self.DEFAULT_PROJECT,
                municipality=muni)
        return grid

    # noinspection PyPep8Naming
    def get_or_create_municipality(self, municipalityName, region=None, country=None, latlng=None, dueDt=datetime.datetime.utcnow()):
        """
        Will create a municipality if necessary
        :param municipalityName:    Name of muni
        :param region:              At the moment it is always 'Cloverfield'
        :param country:             Country code At the moment it is always Africa
        :param latlng:              Location array
        :param dueDt:               connection fee due date time, used by connection loans
        :return:                    Municipality model
        """
        if is_naive(dueDt):
            dueDt = make_aware(dueDt)
        if not municipalityName:
            return
        municipality = ph_model.municipality.Municipality.objects.get_or_none(name=municipalityName)
        if not municipality:
            if latlng:
                # noinspection PyPep8Naming
                timeZone = self._get_time_zone(latlng[0], latlng[1])
            else:
                # noinspection PyArgumentList,PyArgumentList,PyPep8Naming
                timeZone = self._get_time_zone()
            # noinspection PyPep8Naming
            countryName = country or self.DEFAULT_COUNTRY
            # noinspection PyPep8Naming
            regionName = region or self.DEFAULT_REGION
            country = ph_model.country.Country.objects.get_or_create(name=countryName)[0]  # get_or_create return tuple
            region = ph_model.region.Region.objects.get_or_create(country=country, name=regionName)[0]
            municipality = ph_model.municipality.Municipality.objects.get_or_create(
                region=region,
                timeZone=timeZone,
                name=municipalityName,
                connectionFeeDueDate=dueDt)[0]
        else:
            if municipality.connectionFeeDueDate is None:
                municipality.connectionFeeDueDate = dueDt
                municipality.save()
        return municipality

    def _get_time_zone(self, latitude=None, longitude=None):
        return self.timeZoneService and get_time_zone(latitude, longitude, datetime.datetime.utcnow()) or self.DEFAULT_TIME_ZONE

    # noinspection PyMethodMayBeStatic
    def get_or_create_phone_account(self, phone_number, account, first, last):
        """

        :param phone_number:
        :param account:
        :param first:
        :param last:
        :return:
        """
        # noinspection PyBroadException
        try:
            phone_account = ph_model.account.PhoneAccount.objects.get_or_none(mobileMoneyNumber=phone_number)
            if not phone_account:
                phone_account, created = ph_model.account.PhoneAccount.objects.get_or_create(mobileMoneyNumber=phone_number, account=account,
                                                                                             firstName=first, lastName=last)
            else:
                phone_account.account = account
                phone_account.firstName = first
                phone_account.lastName = last
            return phone_account
        except Exception, e:
            return None

    # noinspection PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
    def get_matching_survey(self, customer, first, last, national_id, phone):
        """
        :param customer:
        :param first:
        :param last:
        :param national_id:
        :param phone:
        :return:
        """
        try:
            survey = ph_model.fulcrum.FulcrumRecord.objects.all().filter(
                form__id=self.SURVEY_FORM_ID, form_data__national_id_number=national_id).order_by('-updated')[:1].get()
        except:
            return None

        if survey is None:
            return survey

        res = ph_model.customer.Customer.objects.get_or_none(customerSurvey=survey)
        if res == customer:
            return survey

        return None

    def get_customer_model(self, record, now=datetime.datetime.utcnow()):
        """

        :param record:      STIMA contract form
        :param now          Current date or test date
        :return:
        """
        customer = ph_model.customer.Customer.objects.get_or_none(serviceContract=record)
        data = record.form_data
        status = -1
        national_id = data['national_idpassport_no']
        phone_number = phone_number_util.get_e164_format(data['primary_mobile_phone_no'], self.DEFAULT_COUNTRY_CODE)
        first = data['first_name']
        last = data['last_name']
        if customer and customer.phoneNumber and customer.phoneNumber.as_e164 == phone_number:
            status = 0
        # noinspection PyBroadException
        try:
            account_type = self.ACCOUNT_TYPE_MAP[data['account_type']['choice_values'][0]]
        except Exception,e:
            account_type = self.ACCOUNT_TYPE_MAP[data['account_type_pick_one']['choice_values'][0]]

        location = data['powerhive_site_id']['choice_values'][0]
        if 'village_name' in data:
            grid_name = data['village_name']['choice_values'][0]
        else:
            grid_name = None

        latitude = record.latitude
        if not latitude:
            raise Exception('ERROR - record does not have GPS coordinates - name: %s' % (first + ' ' + last))
        longitude = record.longitude
        municipality = self.get_or_create_municipality(location, latlng=(latitude, longitude), dueDt=now + DELTA_365_DAYS)
        self.create_grid_if_necessary(grid_name, municipality)

        # default to no connection fee loan
        loan = False

        language = data['preferred_language_for_sms_messages']['choice_values'][0]
        if 'will_you_pay_the_full_deposit_upfront_or_a_partial_deposit_with_a_loan' in data:
            fee_type = data['will_you_pay_the_full_deposit_upfront_or_a_partial_deposit_with_a_loan']['choice_values'][0]
            if fee_type.find('1,000') > -1:
                loan = True
        airtel_account = None
        mpesa_account = None
        if 'airtel_mobile_money_account_number' in data:
            airtel_account = phone_number_util.get_e164_format(data['airtel_mobile_money_account_number'], self.DEFAULT_COUNTRY_CODE)
        if 'm_pesa_mobile_money_account_number' in data:
            mpesa_account = phone_number_util.get_e164_format(data['m_pesa_mobile_money_account_number'], self.DEFAULT_COUNTRY_CODE)
        if customer:
            account = customer.account
        else:
            account = ph_model.account.Account()
            account.save()
        phone_account = self.get_or_create_phone_account(phone_number, account, first, last)
        if not phone_account:
            if not customer:
                account.delete()
            raise Exception('invalid mobile money number: %s' % phone_number)
        phone_account2 = None
        phone_account3 = None
        if airtel_account and airtel_account != phone_number:
            phone_account2 = self.get_or_create_phone_account(airtel_account, account, first, last)
        if mpesa_account and mpesa_account != phone_number and mpesa_account != airtel_account:
            phone_account3 = self.get_or_create_phone_account(mpesa_account, account, first, last)

        survey = self.get_matching_survey(customer, first, last, national_id, phone_number)
        msg = ''
        if not customer:
            status = 1
            customer = ph_model.customer.Customer(
                nationalIdNumber=national_id,
                firstName=first,
                lastName=last,
                phoneNumber=phone_number,
                account=account,
                latitude=latitude,
                longitude=longitude,
                municipality=municipality,
                customerType=account_type,
                connectionFeeLoan=loan,
                connectionFeeDue=municipality.connectionFeeDueDate,
                languagePreference=language,
                serviceContract=record,
                customerSurvey=survey)
        else:
            ct = ' changed to: '
            pn = phone_number
            if status == -1:
                status = 2
                old_phone_account = ph_model.account.PhoneAccount.objects.get(mobileMoneyNumber=customer.phoneNumber.as_e164)
                old_phone_account.delete()
                customer.phoneNumber = phone_number

            if customer.phoneNumber != pn:
                msg += 'phoneNumber' + ct + str(phone_number) + '\n'
                customer.phoneNumber = str(phone_number)
            if customer.nationalIdNumber != national_id:
                msg += 'nat_id' + ct + str(national_id) + '\n'
                customer.nationalIdNumber = national_id
            if customer.latitude != latitude:
                msg += 'latitude' + ct + str(latitude) + '\n'
                customer.latitude = latitude
            if customer.longitude != longitude:
                msg += 'longitude' + ct + str(longitude) + '\n'
                customer.longitude = longitude
            if customer.customerType != account_type:
                msg += 'acc_type' + ct + str(account_type) + '\n'
                customer.customerType = account_type
            if customer.connectionFeeLoan != loan:
                msg += 'loan' + ct + str(loan) + '\n'
                customer.connectionFeeLoan = loan
            if customer.firstName != first:
                msg += 'firstName' + ct + str(first) + '\n'
                customer.firstName = first
            if customer.lastName != last:
                msg += 'lastName' + ct + str(last) + '\n'
                customer.lastName = last
            if customer.municipality != municipality:
                msg += 'municipality' + ct + str(municipality) + '\n'
                municipality = municipality
            if customer.connectionFeeDue != municipality.connectionFeeDueDate:
                msg += 'connectionFeeDue' + ct + str(municipality.connectionFeeDueDate) + '\n'
                customer.connectionFeeDue = municipality.connectionFeeDueDate
            if customer.languagePreference != language:
                msg += 'languagePreference' + ct + str(language) + '\n'
                customer.languagePreference = language
            if customer.serviceContract != record:
                msg += 'serviceContract' + ct + str(record) + '\n'
                customer.serviceContract = record
            if customer.customerSurvey != survey:
                msg += 'customerSurvey' + ct + str(survey) + '\n'
        # noinspection PyCompatibility
        try:
            customer.save()
            if msg != '':
                print("\n ACC: %s Fulcrum data changed %s" % (str(customer.account_id), msg))
                LOGGER.info("\n ACC: %s Fulcrum data changed %s" % (str(customer.account_id), msg))
        except Exception, e:
            LOGGER.error('ERROR saving customer account, rolling back')
            if status == 1:
                phone_account.delete()
                if phone_account2:
                    phone_account2.delete()
                if phone_account3:
                    phone_account3.delete()
                account.delete()
            raise e
        return customer, status, loan

    def send_creation_sms(self, customer):
        """

        :param customer:
        """
        connection_fee = add_vat(CONN_LOAN_DEPOSIT_AMT)
        if not customer.connectionFeeLoan:
            if customer.customerType == 'RESIDENTIAL':
                connection_fee = str(add_vat(RESIDENTIAL_CONN_LOAN_AMT))
            else:
                connection_fee = str(add_vat(COMMERCIAL_CONN_LOAN_AMT))
        # noinspection PyUnusedLocal
        date_owed = customer.connectionFeeDue.strftime('%d/%m/%Y')
        account_number = customer.account.id
        if customer.languagePreference == 'Kiswahili':
            message = self.KISWAHILI_SMS_MESSAGE % (connection_fee, account_number)
        else:
            message = self.CUSTOMER_SMS_MESSAGE % (connection_fee, account_number)
        LOGGER.debug('sending message "%s" to %s' % (message, customer.getName()))
        sms_manager.CustomerSMSManager(customer.id, message).send()

    def save_customers_from_records(self, records):
        """

        :param records:
        :return:
        """
        new_customers = 0
        updated_customers = 0
        # noinspection PyPep8Naming
        updatedSince = self.updatedSince
        for r in records:
            # noinspection PyCompatibility,PyBroadException,PyUnusedLocal
            try:
                customer, status, loan = self.get_customer_model(r)
            except Exception, e:
                import traceback
                LOGGER.error('error importing record :\n %s\n\nException:\n%s\n' % (str(r.form_data), traceback.format_exc()))
                continue
            if status:
                if status == 1:
                    LOGGER.info('created customer %s' % customer.getName())
                    new_customers += 1
                elif status == 2:
                    LOGGER.info('updated customer %s' % customer.getName())
                    updated_customers += 1
                self.send_creation_sms(customer)
            else:
                LOGGER.info('already have customer %s' % customer.getName())
            c_updated = date_util.to_epoch(customer.updated) + 1
            if c_updated > updatedSince:
                # noinspection PyPep8Naming
                updatedSince = c_updated
        LOGGER.info(
            'done saving customers from %d records - %d new customers, %d updated customers' % (len(records), new_customers, updated_customers))
        # noinspection PyArgumentEqualDefault
        self._updatedCustomerImportHistory('OK', new_customers, updatedSince, None)
        return new_customers

    def run(self):
        """
        Get customers from fulcrum
        """
        form = ph_model.fulcrum.FulcrumForm.objects.get(id=self.FORM_ID)
        records = ph_model.fulcrum.FulcrumRecord.objects.filter(updated__gte=datetime.datetime.fromtimestamp(self.updatedSince), form=form)
        self.save_customers_from_records(records)

    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
    def _updatedCustomerImportHistory(self, status, totalCustomerSynced, lastSyncedSourceUpdatedAtSinceEpoch, eventLog=None):
        """
        Attributes:
           status: import status ph_model.customer.CustomerImportStatus
           totalCustomerSynced: Total number of customer synced.
           eventLog = {eventNumber: EVENT_TYPE_NUMBEr, items: [{key: value}]} Event to be persisted in case of failure.
        """
        # noinspection PyPep8Naming
        importHistory = ph_model.customer.PotentialCustomerImportHistory(
            dataSource=self.FORM_ID,
            totalCustomerDataSynced=totalCustomerSynced,
            lastSyncedSourceUpdatedAtSinceEpoch=lastSyncedSourceUpdatedAtSinceEpoch,
            status=status)
        importHistory.save()
        if eventLog:
            event_manager.raise_event(importHistory.id, eventLog['eventNumber'], eventLog['details'])
