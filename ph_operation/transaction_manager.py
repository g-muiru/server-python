# coding=utf-8
"""
Module to handle transaction (payment/charge).

Classes:
  Transaction: Base class to handle Debit and Credit.
  Debit: Add money.
  Credit: Deduct money.
"""
import abc
import collections
import decimal
import logging
from django.db import transaction
from ph_model.models.transaction import save_usage_charge_to_master_transaction_accounting, MobilePayment, ScratchcardPaymentHistory, UsageCharge
from ph_operation import sms_manager
from ph_util import date_util
from ph_util.accounting_util import PaymentEnums

logger = logging.getLogger(__name__)


class UnknownTransactionException(Exception):
    """Currently transaction type."""
    pass


class DuplicateTransactions(Exception):
    """Duplicate transaction occurred, we don't want to charge you just because!."""
    pass


# TODO(estifanos): Use constant CREDIT, DEBIT from ph_model.transaction.Type
_transactionType = ('USAGE', 'PAYMENT')
TRANSACTION_TYPE = collections.namedtuple('TTYPE', 'DEBIT CREDIT')._make(_transactionType)


class Transaction(object):
    """Base transaction object to update account balance."""

    def __init__(self, account, amount):
        """
        Arguments:
            account: ph_model.Account
            amount: decimal.Decimal
        """
        self.account = account
        self.amount = decimal.Decimal(amount)
        self.accountBalance = decimal.Decimal(self.account.accountBalance)
        self.accountBalanceBefore = decimal.Decimal(self.accountBalance)
        self.clearing_bal = decimal.Decimal(self.account.clearing_bal)
        self.clearing_bal_before = decimal.Decimal(self.clearing_bal)
        self.uncollected_bal = decimal.Decimal(self.account.uncollected_bal)
        self.uncollected_bal_before = decimal.Decimal(self.uncollected_bal)
        self.reconnect_bal = decimal.Decimal(self.account.reconnect_bal)
        self.reconnect_bal_before = decimal.Decimal(self.reconnect_bal)

    @abc.abstractmethod
    def update_amount(self, adj_type=None):
        """Increase or decrease amount and save it on the fly."""
        logger.warning('Needs to be overridden.')
        pass

    def process(self, adj_type=PaymentEnums.ACCOUNT.name):
        """Increase or decrease amount and save it on the fly."""
        print('IIIIIIIIIIIIIIIIIIIIIIIIIII9999999999999999999', self.accountBalance, self.uncollected_bal, self.clearing_bal, self.reconnect_bal)
        self.update_amount(adj_type)
        print('IIIIIIIIIIIIIIIIIIIIIIIIIII9999999999999999999', self.accountBalance, self.uncollected_bal, self.clearing_bal, self.reconnect_bal)
        if  self.accountBalanceBefore != self.accountBalance or self.uncollected_bal_before != self.uncollected_bal or \
                self.clearing_bal_before != self.clearing_bal or self.reconnect_bal_before != self.reconnect_bal:
            print('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP', self.accountBalance,self.uncollected_bal,self.clearing_bal,self.reconnect_bal)
            self.account.accountBalance = self.accountBalance
            self.account.uncollected_bal = self.uncollected_bal
            self.account.clearing_bal = self.clearing_bal
            self.account.reconnect_bal = self.reconnect_bal
            self.account.save()
        return self


# noinspection PyCompatibility
class Debit(Transaction):
    """Takes money from @param customer an amount of @param amount."""

    def __init__(self, account, amount):
        super(Debit, self).__init__(account, amount)

    def update_amount(self, adj_type=None):
        print('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
        """Deduct money to customer account."""
        if isinstance(adj_type, basestring):
            adj_type = adj_type.upper()
            if adj_type == PaymentEnums.CLEARING.name:
                self.clearing_bal -= self.amount
                print('CCCCCCCCCCCCCCCCCCCCCCCCCCCC', self.clearing_bal)
                return
            if adj_type == PaymentEnums.UNCOLLECTED.name:
                self.uncollected_bal -= self.amount
                print('UUUUUUUUUUUUUUUUUUUUUUUUUUUU', self.uncollected_bal)
                return
            if adj_type == PaymentEnums.RECONNECT.name:
                self.reconnect_bal -= self.amount
                print('RRRRRRRRRRRRRRRRRRRRRRRRRRR', self.reconnect_bal)
                return
        if self.accountBalance < self.amount:
            self.accountBalance = 0
            self.uncollected_bal -= self.amount - self.accountBalanceBefore
            print('ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ111111111111111111', self.accountBalance, self.uncollected_bal)
            return
        self.accountBalance -= self.amount
        print('XXXXXXXXXYYYYYYYYYYZZZZZZZZZZZZ', self.accountBalance, self.uncollected_bal, self.clearing_bal, self.reconnect_bal)


# noinspection PyCompatibility
class Credit(Transaction):
    """Adds money to @param customer an amount of @param amount."""

    def __init__(self, account, amount):
        super(Credit, self).__init__(account, amount)

    def update_amount(self, adj_type=None):
        """Add money to customer account."""
        if isinstance(adj_type, basestring):
            adj_type = adj_type.upper()

            if adj_type == PaymentEnums.ACCOUNT.name:
                self.accountBalance += self.amount
                return
            if adj_type == PaymentEnums.UNCOLLECTED.name:
                if self.uncollected_bal_before < 0:
                    if abs(self.uncollected_bal_before) < self.amount:
                        self.uncollected_bal = 0
                        self.accountBalance += self.uncollected_bal_before + self.amount
                    else:
                        self.uncollected_bal = self.uncollected_bal_before + self.amount
                elif self.clearing_bal_before < 0:
                    if abs(self.clearing_bal_before) < self.amount:
                        self.clearing_bal = 0
                        self.accountBalance +=  self.amount + self.clearing_bal_before
                    else:
                        self.clearing_bal = self.clearing_bal_before + self.amount
                else:
                    self.accountBalance += self.amount
                return
            if adj_type == PaymentEnums.CLEARING.name:
                if self.clearing_bal_before < 0:
                    if abs(self.clearing_bal_before) < self.amount:
                        self.clearing_bal = 0
                        self.accountBalance += self.amount + self.clearing_bal_before
                    else:
                        self.clearing_bal = self.clearing_bal_before + self.amount
                elif self.uncollected_bal_before < 0:
                    if abs(self.uncollected_bal_before) < self.amount:
                        self.uncollected_bal = 0
                        self.accountBalance += self.amount + self.uncollected_bal_before
                    else:
                        self.uncollected_bal = self.uncollected_bal_before + self.amount
                else:
                    self.accountBalance += self.amount
                return
            if adj_type == PaymentEnums.RECONNECT.name:
                if self.reconnect_bal_before < 0:
                    if abs(self.reconnect_bal_before) < self.amount:
                        self.recon_bal = 0
                        self.accountBalance += self.amount + self.reconnect_bal_before
                    else:
                        self.recon_bal = self.reconnect_bal_before + self.amount
                elif self.uncollected_bal_before < 0:
                    if abs(self.uncollected_bal_before) < self.amount:
                        self.uncollected_bal = 0
                        self.accountBalance += self.amount + self.uncollected_bal_before
                    else:
                        self.uncollected_bal = self.uncollected_bal_before + self.amount
                elif self.clearing_bal_before < 0:
                    if abs(self.clearing_bal_before) < self.amount:
                        self.clearing_bal = 0
                        self.accountBalance += self.amount + self.clearing_bal_before
                    else:
                        self.clearing_bal = self.clearing_bal_before + self.amount
                else:
                    self.accountBalance += self.amount

        else:
            self.accountBalance += self.amount


class TransactionManager(object):
    """Transaction manager to drive debit/credit logic."""

    def __init__(self, account, amount, tran_type=TRANSACTION_TYPE.DEBIT):
        """Intializes transaction type and process with the @param amount

        Attribute:
            account: ph_model.account.Account
            amount: decimal.Decimal.
            type: TRANSACTION_TYPE, Debit or Credit.
        """

        if tran_type == TRANSACTION_TYPE.DEBIT:
            self.transaction = Debit(account, amount)
        elif tran_type == TRANSACTION_TYPE.CREDIT:
            self.transaction = Credit(account, amount)
        else:
            raise UnknownTransactionException
        self.transaction.process()

    @classmethod
    def send_payment_sms(cls, smss):
        """
        Send payment sms's
        :param smss: array of tuples with customer ID
        :return:
        """
        res = []
        if smss is not None:
            for s in smss:
                res.append(sms_manager.CustomerSMSManager(s[0], s[1]).send())
        return res

    @classmethod
    def process_mobile_payment(cls, account, transaction_id, paid_amount, collect_time,
                               payment_type=PaymentEnums.ACCOUNT.name, network_service_provider=None):
        """
        Process customer mobile payment. Add to customer account.

        :param account:
        :param transaction_id:
        :param paid_amount:
        :param collect_time:
        :param payment_type:
        :param network_service_provider:
        :return:
        """

        try:
            payment_exist = MobilePayment.objects.get(transactionId=transaction_id)
            if payment_exist:
                raise DuplicateTransactions
        except MobilePayment.DoesNotExist:
            pass
        credit = Credit(account, paid_amount)
        credit.process()
        processed_transaction = MobilePayment(
            account=account,
            amount=paid_amount,
            transactionId=transaction_id,
            processedTime=collect_time,
            payment_type=payment_type,
            networkServiceProvider=network_service_provider)
        processed_transaction.save()
        return processed_transaction

    @classmethod
    def process_scratch_card_payment(cls, customer, scratchcard):
        """Process scratch card payment. save history."""
        account = customer.account
        credit = Credit(account, scratchcard.amount)
        credit.process()
        processed_time = date_util.get_utc_now()
        processed_transaction = ScratchcardPaymentHistory(
            account=account, amount=scratchcard.amount,
            scratchcard=scratchcard, processedTime=processed_time)
        processed_transaction.save()
        return processed_transaction

    @classmethod
    @transaction.atomic
    def load_usage_charges_to_master_transaction(cls, page_size=250):
        """Since usage charges are persisted as bulk save trigger won't fire signal to update master transaction.
         For performance reason a separate cron is run to update master transaction for the usage charges already
         persisted."""
        usage_charges = UsageCharge.objects.filter(syncToMasterTransaction=False)[0: page_size]
        if not usage_charges:
            return
        usage_charges_ids = []
        cnt = 0
        for usageCharge in usage_charges:
            save_usage_charge_to_master_transaction_accounting(usageCharge)
            usage_charges_ids.append(usageCharge.id)
            cnt += 1
        UsageCharge.objects.filter(id__in=usage_charges_ids).update(syncToMasterTransaction=True)
        logger.info("Updated usage charges: %s" % str(cnt))
        return cnt
