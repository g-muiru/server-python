# coding=utf-8
"""Monthly Account Statement Manager"""
import logging

from datetime import datetime
from dateutil.relativedelta import relativedelta
import ph_model.models as ph_model
from ph_util import date_util
from ph_operation import sms_manager

from ph_util.db_utils import calc_monthly_usage_stima_points

LOGGER = logging.getLogger(__name__)


class StatementManager(object):
    """
    The ask is:
    100 Stima Points for every kWh consumed.
    Monthly SMS showing customers how many points they earned during the previous month,
    and how many points they have accumulated so far (the points balance)
    Delivered:
    We only show the total accummulated points for the customer, 100 Stima Points for every kWh consumed.
    """

    # noinspection PyPep8Naming
    @classmethod
    def monthly_statement(cls, today=date_util.get_utc_now(), test=False):
        """
            If the last day of the month send monthly statement via SMS
            Traps bad dates
        :param today:   Date to consider
        :param test:    If true do not send SMS
        :return:
        """
        test_data = []
        try:
            """ generate monthly statement and send via SMS """
            first_of_next_month = datetime(today.year, today.month, 1) + relativedelta(months=1)
            # noinspection PyCallingNonCallable
            last_date_of_month = datetime(today.year, today.month, 1) + relativedelta(months=1, days=-1)
            # noinspection PyCallingNonCallable
            first_of_this_month = datetime(today.year, today.month, 1)
            # this is when we turned on the first Cloverfield site
            """ if it's not the last day of the month, bail out """
            if today.day == last_date_of_month.day:
                LOGGER.debug("last day of the month, generating account statements")
                # only process customers with circuits assigned to them
                customers = ph_model.customer.Customer.objects.exclude(circuit_id__isnull=True).all().order_by('account_id')
                if customers:
                    for customer in customers:
                        """
                        Exclude some projects TODO Banton might eventually be included
                        ID  Project
                        4   Banton Pilot
                        3   Cloverfield
                        2   Engineering
                        1   Powerhive - Pilots
                        """
                        if customer.circuit.queen.grid.project_id != 2 and customer.circuit.queen.grid.project_id != 4:
                            usageRollup, stima_points, kwh = calc_monthly_usage_stima_points(customer,today)
                            totalCharge = 0
                            usageCharges = ph_model.transaction.UsageCharge.objects.filter(
                                account=customer.account,
                                processedTime__gte=first_of_this_month,
                                processedTime__lt=first_of_next_month)
                            if usageCharges:
                                for usageCharge in usageCharges:
                                    totalCharge += usageCharge.amount
                            message = 'Dear %s, you used %s Wh of stima in %s at a cost of KSh %s. ' \
                                      'Your Current balance is KSh %s, Total STIMA PTS: %s.' \
                                      % (customer.firstName, str(round(usageRollup, 3)), today.strftime("%B"), str(int(round(totalCharge))),
                                         str(int(round(customer.account.accountBalance))), str(int(stima_points)))
                            if test:
                                test_data.append((customer.id, message))
                                # print(customer.account_id, message)
                            else:
                                sms_manager.CustomerSMSManager(customer.id, message).send()
            else:
                LOGGER.debug("it's not the last day of the month yet")
        except Exception as e:
            LOGGER.error("ERROR: %s " % str(e))
        return test_data
