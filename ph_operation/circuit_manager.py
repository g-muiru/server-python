# coding=utf-8
"""
Module to handle circuit logic.

Classes:
Circuit: Wrapper on top of ph_model.circuit.Circuit containing other information like account ...
CircuitManager: Manager class to run logic on circuit.
CircuitManager.configure_circuit_after_processed_usage: Enable/Disable circuit by consumption,
"""
import abc
import logging

import ph_model
from ph_model.models import SwitchEnabledHistory, Queen
from ph_model.models.account import DEFAULT_MIN_ALLOWED_BALANCE
from ph_model.models.circuit import SwitchEnabledChangeReason

LOGGER = logging.getLogger(__name__)


class Circuit(object):
    """Main Circuit class"""

    def __init__(self, circuit):
        self.circuit = circuit

    @abc.abstractmethod
    def _update_switch_enable_history(self, reason):
        """Should be called only with Customer or NonCustomer circuit handler if there are any changes."""
        pass

    @abc.abstractmethod
    def configure_circuit(self):
        """Configures circuit according to sever side requirements or update as it for queen notificaiton."""
        pass


class BalanceUpdateConfigCircuit(Circuit):
    """Queen initialized circuit, this happens when Server side initialize circuit to  configure circuit. e.g
    after tariff processing, or payment."""

    def __init__(self, circuit):
        super(BalanceUpdateConfigCircuit, self).__init__(circuit)
        self.customer = circuit.circuitOwner
        self.account = self.customer.account
        self.account_rule = self.account.accountRule
        self.minAllowedBalance = DEFAULT_MIN_ALLOWED_BALANCE

        if self.account_rule:
            self.minAllowedBalance = self.account_rule.minBalance

    def _update_switch_enable_history(self, reason):
        SwitchEnabledHistory(
            circuit=self.circuit,
            switchEnabled=self.circuit.switchEnabled,
            enableBalanceServer=self.circuit.enableBalanceServer,
            reason=reason,
            accountBalance=self.account.accountBalance,
            serverRequestEnabled=self.circuit.server_request_enabled).save()

    def configure_circuit(self):
        """switch enable circuit by balance, and persist into SwitchEnabled model if there are any changes."""
        if self.account.accountBalance > self.minAllowedBalance:
            if not self.circuit.enableBalanceServer:
                # Circuit was requested earlier to be disabled or by min acc balance or is first time setup. Now customer is cool!
                self.circuit.enableBalanceServer = True
                self.circuit.save()
                self._update_switch_enable_history(SwitchEnabledChangeReason.SUFFICIENT_BALANCE.value)

        else:
            if self.circuit.enableBalanceServer or self.circuit.enableBalanceServer is None:
                self.circuit.enableBalanceServer = False
                self.circuit.save()
                self._update_switch_enable_history(SwitchEnabledChangeReason.INSUFFICIENT_BALANCE.value)


class CircuitMonitorConfigCircuit(Circuit):
    """Queen initialized circuit, this happens when queen sent out circuit monitor to configure circuit."""

    # noinspection PyPep8Naming
    def __init__(self, circuitMonitor):
        super(CircuitMonitorConfigCircuit, self).__init__(circuitMonitor.circuit)
        self.monitor = circuitMonitor
        self.circuit.switchEnabled = self.monitor.switchEnabled
        self.circuit.save()

    def _update_switch_enable_history(self, reason):
        SwitchEnabledHistory(
            circuit=self.circuit,
            switchEnabled=self.circuit.switchEnabled,
            disableVaLimitQueen=self.monitor.disableVaLimit,
            reason=reason).save()

    # noinspection PyPep8Naming
    def configure_circuit(self):
        """Any changes reported by circuit monitor, persist in circuit.SwithcEnabledHistory."""
        # noinspection PyPep8Naming
        switchReason = SwitchEnabledChangeReason.value_of_case_insensitive(self.monitor.switchReason)
        if self.circuit.disableVaLimitQueen != \
                self.monitor.disableVaLimit or self.circuit.switchEnabled != self.monitor.switchEnabled:
            self._update_switch_enable_history(switchReason)  # TODO if we updated the switch enable in __init__, how will this ever run?


class OverrideSwitchEnabledConfiguredCircuit(Circuit):
    """Configure switchEnabled by global override."""

    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
    def __init__(self, circuit, overrideSwitchEnabled, whLimit=None, vaLimit=None):
        super(OverrideSwitchEnabledConfiguredCircuit, self).__init__(circuit)
        self.overrideSwitchEnabled = overrideSwitchEnabled
        self.whLimit = whLimit
        self.vaLimit = vaLimit

    def _update_switch_enable_history(self, reason):
        SwitchEnabledHistory(
            circuit=self.circuit,
            switchEnabled=self.circuit.switchEnabled,
            overrideSwitchEnabled=self.circuit.overrideSwitchEnabled,
            reason=reason).save()

    def configure_circuit(self):
        """switch enable circuit by Override switch enabled."""
        # TODO(estifanos): Look into it further there could be more logic involving whLimt and vaLimit
        if self.circuit.overrideSwitchEnabled == self.overrideSwitchEnabled:
            return
        self.circuit.overrideSwitchEnabled = self.overrideSwitchEnabled
        self.circuit.save()
        self._update_switch_enable_history(SwitchEnabledChangeReason.OVERRIDE_SWITCH_ENABLED.value)


class CircuitManager(object):
    """Class to manage circuit logic."""

    @classmethod
    def configure_circuit_after_processed_usage(cls, circuit):
        """Configures circuit after processing usage."""
        BalanceUpdateConfigCircuit(circuit).configure_circuit()

    @classmethod
    def configure_circuit_after_payment(cls, cust):
        """Configures circuit after payment have done to an account."""
        if cust.circuit_id is not None:
            BalanceUpdateConfigCircuit(cust.circuit).configure_circuit()

    @classmethod
    def configure_circuit_after_account_adjustment(cls, account):
        """Configures circuit after account adjustment is done."""
        if account.accountOwner.circuit:
            BalanceUpdateConfigCircuit(account.accountOwner.circuit).configure_circuit()

    # noinspection PyPep8Naming
    @classmethod
    def configure_circuits_by_circuit_monitor(cls, listOfCircuitMonitor):
        """Configures circuit by queen's notification via circuit monitor.."""
        if not isinstance(listOfCircuitMonitor, list):
            listOfCircuitMonitor = [listOfCircuitMonitor]
        for circuitMonitor in listOfCircuitMonitor:
            CircuitMonitorConfigCircuit(circuitMonitor).configure_circuit()

    # noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
    @classmethod
    def override_switch_enabled(cls, circuit, overrideSwitchEnabled, whLimit=None, vaLimit=None):
        """Override switch enabled."""
        OverrideSwitchEnabledConfiguredCircuit(circuit, overrideSwitchEnabled, whLimit=whLimit, vaLimit=vaLimit).configure_circuit()

    @classmethod
    def set_clear_circuit_overrides(cls, exclude_queen_device_id=None):
        """ Reset circuit overrides
        
        :param exclude_queen_device_id:    list of queens to exclude
        :return: 
        """
        queens = None
        if exclude_queen_device_id:
            queens = Queen.objects.filter(deviceId__in=exclude_queen_device_id)
            if queens:
                queens = [q.id for q in queens]
                overrides = ph_model.models.circuit.Circuit.objects.exclude(overrideSwitchEnabled=True).filter(queen_id__in=queens)
                for c in overrides:
                    c.overrideSwitchEnabled = True
                    c.save()

        overrides = ph_model.models.circuit.Circuit.objects.filter(overrideSwitchEnabled=True)
        if queens:
            overrides = overrides.exclude(queen_id__in=queens)
        for c in overrides:
            c.overrideSwitchEnabled = False
            c.save()
