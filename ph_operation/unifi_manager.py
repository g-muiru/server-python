# coding=utf-8
"""
Module to manage UniFi network users, sessions, and payments

"""

import abc
import logging
import json
import csv
from ph_ext_lib import unifi 
import ph_model.models as ph_model
from ph_util import phone_number_util
from django.conf import settings

LOGGER = logging.getLogger(__name__)



class UnifiManagerException(Exception):
    """TODO: log exception"""
    LOGGER.warning(Exception)
    pass


class UnifiManager(object):

    @classmethod
    def get_status(cls):
        """
        :return:
        """
        # noinspection PyBroadException,PyUnusedLocal
        try:
            api = unifi.Unifi()
            api.login()
            response = api.get_status()
            LOGGER.debug(response)
            return response

        except Exception as e:
            LOGGER.warning(e)

        return None


    @classmethod
    def stat_vouchers(cls, create_time = 0):
        """
        :return:
        """
        #create_time = 1501091315
        # noinspection PyBroadException,PyUnusedLocal
        try:
            api = unifi.Unifi()
            api.login()
            response = api.stat_vouchers(create_time)
            LOGGER.debug(response)
            return response

        except Exception as e:
            LOGGER.warning(e)

        return None

    @classmethod
    def create_voucher(cls):
        """
        :return:
        """
        # noinspection PyBroadException,PyUnusedLocal
        try:
            api = unifi.Unifi()
            api.login()
            response = api.create_voucher()
            LOGGER.debug(response)
            return response

        except Exception as e:
            LOGGER.warning(e)

        return None

    @classmethod
    def issue_voucher(cls):
        """
        :return:
        """
        # noinspection PyBroadException,PyUnusedLocal
        try:
            api = unifi.Unifi()
            api.login()
            create_data = json.loads(api.create_voucher())
            LOGGER.debug(create_data['data'][0]['create_time'])
            create_time = create_data['data'][0]['create_time']
            voucher_data = json.loads(api.stat_vouchers(create_time))
            LOGGER.debug(voucher_data)

            return voucher_data

        except Exception as e:
            LOGGER.warning(e)

        return None
