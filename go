#!/bin/bash
if [ $1 = 'startprod' ]
	then
		uwsgi --ini uwsgi-prod.ini --env DJANGO_SETTINGS_MODULE=powerhive.prod_settings --uid root --gid www-data --stats /tmp/statsock
fi

if [ $1 = 'stopprod' ]
	then
		cat ../data.powerhive.com.pid | xargs kill -9
fi

if [ $1 = 'startstg' ]
	then
		uwsgi --ini uwsgi-stg.ini --env DJANGO_SETTINGS_MODULE=powerhive.stg_settings --uid root --gid www-data
fi

if [ $1 = 'stopstg' ]
	then
		cat ../data.powerhive.com.pid | xargs kill -9
fi

if [ $1 = 'startdev' ]
	then
		uwsgi --ini uwsgi-local.ini --env DJANGO_SETTINGS_MODULE=powerhive.local_settings
fi

if [ $1 = 'stopdev' ]
	then
		rm /tmp/data.powerhive.com.sock
fi

if [ $1 = 'startfprod' ]
	then
		./manage.py runfcgi pidfile=/var/www/fapi.powerhive.com.pid host=127.0.0.1 port=8081 --prod
fi

if [ $1 = 'stopfprod' ]
	then
		cat ../fapi.powerhive.com.pid | xargs kill -9
fi

if [ $1 = 'startfstg' ]
	then
		./manage.py runfcgi pidfile=/var/www/fapidev.powerhive.com.pid host=127.0.0.1 port=8889 --stg
fi

if [ $1 = 'stopfstg' ]
	then
		cat ../fapidev.powerhive.com.pid | xargs kill -9
fi

