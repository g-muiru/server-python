# coding=utf-8
"""
Test views
"""
from django.core.urlresolvers import reverse
from rest_framework import status
import base_test
import ph_model.models as ph_model
import os
from decimal import *


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming,PyPep8Naming
class UsageTestCase(base_test.BaseTestView):
    """Tests ph_grid_api.views.UsageList."""

    def test_post_usage_with_customer_end_point(self):
        """Tests post usage data with customer."""
        data = self.load_as_json_data('usage')
        usageCountBeforePost = ph_model.usage.Usage.customer_usages.count()
        url = "/api/circuits/usage/"
        response = self.client.post(url, data)
        usageCountAfterPost = ph_model.usage.Usage.customer_usages.count()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Test data expected to have two more usage on data/usage.json
        self.assertEqual(usageCountAfterPost, usageCountBeforePost + 2)

    def test_post_usage_without_customer_end_point(self):
        """Tests post usage data with out customer."""
        data = self.load_as_json_data('usage')
        usageCountBeforePost = ph_model.usage.Usage.non_customer_usages.count()
        url = "/api/circuits/usage/"
        response = self.client.post(url, data)
        usageCountAfterPost = ph_model.usage.Usage.non_customer_usages.count()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(ph_model.usage.Usage.non_customer_usages.all()[0].customer)
        self.assertTrue(ph_model.usage.Usage.non_customer_usages.all()[0].processed)

        # Test data expected to have two more usage on data/usage.json
        self.assertEqual(usageCountAfterPost, usageCountBeforePost + 1)

    def test_post_usage_with_status(self):
        """
        Create usage
        :return:
        """
        usageData = {
            "header": {
                "collectTime": "2014-06-09T23:14:00Z"
            },
            "data": {
                "usages": [
                    {
                        "deviceId": "NO_CUSTOMER_CIRCUIT",
                        "intervalWh": 0,
                        "intervalVAmax": 1,
                        "intervalVAmaxH": 1,
                        "intervalVmin": 2
                    }
                ]
            }
        }
        ph_model.usage.Usage.objects.all().delete()
        url = "/api/circuits/usage/"
        response = self.client.post(url, usageData)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ph_model.usage.Usage.objects.count(), 1)
        circuit = ph_model.circuit.Circuit.objects.get(deviceId='NO_CUSTOMER_CIRCUIT')
        self.assertIsNone(ph_model.customer.Customer.objects.get_or_none(circuit=circuit.id))

    def test_post_usage_without_intervalVAmaxH(self):
        """

        :return:
        """
        usageData = {
            "header": {
                "collectTime": "2014-06-09T23:14:00Z"
            },
            "data": {
                "usages": [
                    {
                        "deviceId": "NO_CUSTOMER_CIRCUIT",
                        "intervalWh": 0,
                        "intervalVAmax": 1,
                        "intervalVmin": 2
                    }
                ]
            }
        }
        ph_model.usage.Usage.objects.all().delete()
        url = "/api/circuits/usage/"
        response = self.client.post(url, usageData)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.assertEqual(ph_model.usage.Usage.objects.count(), 0)
        circuit = ph_model.circuit.Circuit.objects.get(deviceId='NO_CUSTOMER_CIRCUIT')
        self.assertIsNone(ph_model.customer.Customer.objects.get_or_none(circuit=circuit.id))

    # TODO do we want to create usage when there is a circuit fault ?????
    def test_post_usage_with_ground_fault(self):
        """
        create usage even with ground fault
        :return:
        """
        usageData = {
            "header": {
                "collectTime": "2014-06-09T23:14:00Z"
            },
            "data": {
                "usages": [
                    {
                        "deviceId": "NO_CUSTOMER_CIRCUIT",
                        "intervalWh": 0,
                        "intervalVAmax": 20,
                        "intervalVAmaxH": 1000,
                        "intervalVmin": 2
                    }
                ]
            }
        }
        ph_model.usage.Usage.objects.all().delete()
        url = "/api/circuits/usage/"
        response = self.client.post(url, usageData)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ph_model.usage.Usage.objects.count(), 1)
        circuit = ph_model.circuit.Circuit.objects.get(deviceId='NO_CUSTOMER_CIRCUIT')
        self.assertIsNone(ph_model.customer.Customer.objects.get_or_none(circuit=circuit.id))

    def test_get_usage_customer(self):
        """

        :return:
        """
        url = "{}?type={}".format(reverse('usage'), "CUSTOMER")
        response = self.client.get(url)
        self.assertEquals(len(response.data), 2)

    def test_get_usage_circuit(self):
        """

        :return:
        """
        url = "{}?type={}".format(reverse('usage'), "NON_CUSTOMER")
        response = self.client.get(url)
        self.assertEquals(len(response.data), 1)

    def test_get_usage_with_and_without_customers(self):
        """

        :return:
        """
        url = "{}?type={}".format(reverse('usage'), 'ALL')
        response = self.client.get(url)
        self.assertEquals(len(response.data), 3)


class EventTestCase(base_test.BaseTestView):
    """Tests ph_grid_api.views.EventList."""

    # Should match event/event_detail data in test fixture.
    GET_DATA = [
        {
            "id": 1,
            "eventType": 100,
            "collectTime": "2011-09-01T10:20:30Z",
            'itemId': 1,
            "details": [
                {
                    "id": 1,
                    "event": 1,
                    "itemName": "Name 1",
                    "itemValue": "Value 1"
                }
            ]
        },
        {
            "id": 2,
            'itemId': 1,
            "eventType": 200,
            "collectTime": "2011-09-01T10:20:30Z",
            "details": []
        }
    ]

    def test_post_event_item_id_set_to_device_primary_key(self):
        """

        :return:
        """

        # Constructed from data/event_by_device_id.json\
        ph_model.event.Event.objects.all().delete()
        data = self.load_as_json_data('event_by_pk')
        self.client.post(reverse('event'), data)
        self.assertEqual(1, ph_model.event.Event.objects.count())
        self.assertEqual(2, ph_model.event.EventDetail.objects.count())

    def test_post_event_item_id_set_to_devce_id(self):
        """

        :return:
        """
        # Constructed from data/event_by_device_id.json

        ph_model.event.Event.objects.all().delete()
        data = self.load_as_json_data('event_by_pk')
        self.client.post(reverse('event'), data)
        self.assertEqual(1, ph_model.event.Event.objects.count())
        self.assertEqual(2, ph_model.event.EventDetail.objects.count())

    def test_get_event(self):
        """

        :return:
        """
        response = self.client.get(reverse('event'))
        self.assertItemsEqual(response.data, self.GET_DATA)


class QueenTestCase(base_test.BaseTestView):
    """Tests ph_grid_api.views.QueenList."""

    # Should match queen data in test fixture.
    GET_DATA = {'expectedFirmware': {'version': 'MAJOR_MINOR_BETA', 'md5Sum': None,
                                     'url': 'http://0.0.0.0:8000/api/queenfirmwares/MAJOR_MINOR_BETA'},
                'currentFirmware': {'version': 'MAJOR_MINOR_BETA', 'md5Sum': None,
                                    'url': 'http://0.0.0.0:8000/api/queenfirmwares/MAJOR_MINOR_BETA'},
                'verbosity': 0, 'usageInterval': 10, 'generationInterval': 900,
                'monitorInterval': 120, 'isPs': 0}

    def test_get_queen(self):
        """

        :return:
        """
        url = "/api/{}/{}/".format('queens', 'ESPECIALQUEEN')
        response = self.client.get(url)
        self.assertEquals(response.data, self.GET_DATA)


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
class QueenFirmwareTestCase(base_test.BaseTestView):
    """Tests ph_grid_firmware_api.views.QueenFirmwareViewSet."""

    @staticmethod
    def _set_firmware_path_to_test_directory(queenFirmwareVersion):
        queenFirmware = ph_model.queen.QueenFirmware.objects.get(version=queenFirmwareVersion)
        testPath = os.path.abspath(os.path.dirname(__file__))

        queenFirmware.path = '{}/queen_firmware'.format(testPath)
        queenFirmware.fileBaseName = 'queen_firmware'
        queenFirmware.fileExtension = '.bin'
        queenFirmware.save()

    def test_get_queen_firmware(self):
        """

        :return:
        """
        url = "/api/{}/{}/".format('queenfirmwares', 'MAJOR_MINOR_BETA')
        self._set_firmware_path_to_test_directory('MAJOR_MINOR_BETA')
        response = self.client.get(url)
        self.assertIsNotNone(response)


class QueenPutConfigTestCase(base_test.BaseTestView):
    """Test queen config"""
    def hit_queen_put_config_endpoint(self):
        """

        :return:
        """
        data = self.load_as_json_data('existing_queen_config')
        url = "/api/{}/{}/config/".format("queens", "ESPECIALQUEEN")
        self.client.post(url, data)

    def test_non_existing_queen_config(self):
        """

        :return:
        """
        data = self.load_as_json_data('non_existing_queen_config')
        url = "/api/{}/{}/config/".format("queens", "RANDOM_QUEEN")
        self.client.post(url, data)
        queen = ph_model.queen.Queen.objects.get(deviceId='NONEXISTING')
        grid = ph_model.grid.Grid.objects.get(name=ph_model.grid.DEFAULT_GRID_NAME)
        self.assertIsNotNone(queen)
        self.assertEqual(queen.grid, grid)

    def test_queen_before_put(self):
        """

        :return:
        """
        queen = ph_model.queen.Queen.objects.get(deviceId='ESPECIALQUEEN')
        self.assertEquals(-0.571371, queen.latitude)

    def test_queen_after_put(self):
        """

        :return:
        """
        self.hit_queen_put_config_endpoint()
        queen = ph_model.queen.Queen.objects.get(deviceId='ESPECIALQUEEN')
        self.assertEquals(-0.571371, queen.latitude)

    def test_disconnect_circuit_from_probe(self):
        """

        :return:
        """
        self.hit_queen_put_config_endpoint()
        circuit = ph_model.circuit.Circuit.objects.get(deviceId='PROBESERIAL2:2')
        self.assertEquals(None, circuit.probe)

    def test_probe_set_in_circuit(self):
        """

        :return:
        """
        self.hit_queen_put_config_endpoint()
        circuit = ph_model.circuit.Circuit.objects.get(deviceId='PROBESERIAL2:1')
        self.assertEquals("PROBESERIAL2", circuit.probe.deviceId)

    @staticmethod
    def test_probe_is_deleted_if_missing_from_config():
        """

        :return:
        """
        # TODO: Implement test
        return


class CircuitTestCase(base_test.BaseTestView):
    """Tests ph_grid_api.views.CircuitList"""

    # Should match circuits data in test fixture.
    GET_DATA = [{'enabledRequest': False, 'vaLimit': 0.0, 'number': 1},  # Switch is enabled but not connected to customer
                {'enabledRequest': False, 'vaLimit': 0.0, 'number': 2},  # Switch is disabled but not connected to customer
                {'enabledRequest': None, 'vaLimit': 0.0, 'number': 3},  # all default values
                {'enabledRequest': True, 'vaLimit': 0.0, 'number': 4},  # Switch is disabled, balance server enable to true with circuit connected
                {'enabledRequest': False, 'vaLimit': 0.0, 'number': 5},  # Default non customer circuit.
                {'enabledRequest': True, 'vaLimit': 0.0, 'number': 6}  # Overridden non customer circuit
                ]

    def test_get_circuit(self):
        """

        :return:
        """
        url = "/api/queens/{}/circuit/".format('ESPECIALQUEEN')
        response = self.client.get(url)
        self.assertItemsEqual(response.data, self.GET_DATA)


# noinspection PyPep8Naming
class GenerationTestCase(base_test.BaseTestView):
    """Test generation post"""
    def test_post_generation(self):
        """

        Tests generation data posting
        """
        data = self.load_as_json_data('generation')
        ph_model.generation.Generation.objects.all().delete()
        url = "/api/queens/ESPECIALQUEEN/generation/"
        self.client.post(url, data)
        generationCountAfterPost = ph_model.generation.Generation.objects.count()
        self.assertEqual(generationCountAfterPost, 1)


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
class QueenMonitorTestCase(base_test.BaseTestView):
    """Test queen monitors"""
    def test_post_monitor(self):
        """

        :return:
        """
        data = self.load_as_json_data('queen_monitor')
        ph_model.monitor.QueenMonitor.objects.all().delete()
        url = "/api/queens/{}/monitor/".format("ESPECIALQUEEN")
        self.client.post(url, data)
        queenMonitorCountAfterPost = ph_model.monitor.QueenMonitor.objects.count()
        self.assertEqual(queenMonitorCountAfterPost, 1)

    def test_post_monitor_update_queen_started_datetime(self):
        """

        :return:
        """
        data = self.load_as_json_data('queen_monitor')
        ph_model.monitor.QueenMonitor.objects.all().delete()
        url = "/api/queens/{}/monitor/".format("ESPECIALQUEEN")
        self.client.post(url, data)
        queen = ph_model.queen.Queen.objects.get(deviceId='ESPECIALQUEEN')
        from dateutil import parser
        self.assertEqual(queen.osStarted, parser.parse('2014-09-26T23:03:03Z'))
        self.assertEqual(queen.fwStarted, parser.parse('2014-09-26T23:02:53Z'))

    def test_post_monitor_with_fs_fields(self):
        """

        :return:
        """
        data = self.load_as_json_data('queen_monitor_with_fs_fields')
        ph_model.monitor.QueenMonitor.objects.all().delete()
        url = "/api/queens/{}/monitor/".format("ESPECIALQUEEN")
        self.client.post(url, data)
        queenMonitorCountAfterPost = ph_model.monitor.QueenMonitor.objects.count()
        self.assertEqual(queenMonitorCountAfterPost, 1)
        queenMonitor = ph_model.monitor.QueenMonitor.objects.all()
        self.assertEqual(queenMonitor[0].fsFreeStatus, 1)
        self.assertEqual(queenMonitor[0].fsFreePct, Decimal('35.13958200000000000000'))


# noinspection PyPep8Naming,PyPep8Naming
class PowerStationConfigurationTestCase(base_test.BaseTestView):
    """Test power station config"""
    def test_post_ps_config(self):
        """

        :return:
        """
        data = self.load_as_json_data('ps_config')
        ph_model.inverter.Inverter.objects.all().delete()
        ph_model.battery.BatteryBank.objects.all().delete()
        url = "/api/queens/{}/psconfig/".format("ESPECIALQUEEN")
        self.client.post(url, data)
        inverterCountAftrePost = ph_model.inverter.Inverter.objects.count()
        batteryBankCountAftrePost = ph_model.battery.BatteryBank.objects.count()
        self.assertEqual(inverterCountAftrePost, 2)
        self.assertEqual(batteryBankCountAftrePost, 1)


# noinspection PyPep8Naming
class InverterMonitorTestCase(base_test.BaseTestView):
    """Test inverter monitors"""
    def test_post_ps_monitor(self):
        """

        Tests inverter monitor post
        """
        data = self.load_as_json_data('inverter_monitor')
        ph_model.monitor.InverterMonitor.objects.all().delete()
        url = "/api/queens/{}/invertermonitor/".format("ESPECIALQUEEN")
        self.client.post(url, data)
        inverterMonitorCountAftrePost = ph_model.monitor.InverterMonitor.objects.count()
        self.assertEqual(inverterMonitorCountAftrePost, 2)


# noinspection PyPep8Naming
class BatteryBankMonitorTestCase(base_test.BaseTestView):
    """Test battery monitors"""
    def test_post_ps_monitor(self):
        """

        :return:
        """
        data = self.load_as_json_data('batterybank_monitor')
        ph_model.monitor.BatteryBankMonitor.objects.all().delete()
        url = "/api/queens/{}/batterybankmonitor/".format("ESPECIALQUEEN")
        self.client.post(url, data)
        batteryBankMonitorCountAftrePost = ph_model.monitor.BatteryBankMonitor.objects.count()
        self.assertEqual(batteryBankMonitorCountAftrePost, 1)


# noinspection PyPep8Naming
class ProbeMonitorTestCase(base_test.BaseTestView):
    """ Test probe monitors"""
    def test_post_probe_monitor(self):
        """

        :return:
        """
        data = self.load_as_json_data('probe_monitor')
        ph_model.monitor.ProbeMonitor.objects.all().delete()
        url = '/api/probes/monitor/'
        self.client.post(url, data)
        monitorCount = ph_model.monitor.ProbeMonitor.objects.count()
        self.assertEqual(monitorCount, 2)


# noinspection PyPep8Naming
class CircuitMonitorTestCase(base_test.BaseTestView):
    """ Test circuit monitors"""

    def test_post_circuit_monitor(self):
        """

        :return:
        """
        data = self.load_as_json_data('circuit_monitor')
        ph_model.monitor.CircuitMonitor.objects.all().delete()
        url = '/api/circuits/monitor/'
        self.client.post(url, data)
        monitorCount = ph_model.monitor.CircuitMonitor.objects.count()
        self.assertEqual(monitorCount, 2)
        monitor1 = ph_model.monitor.CircuitMonitor.objects.get(circuit__deviceId='PROBESERIAL1:1')
        self.assertEquals(ph_model.circuit.SwitchEnabledStatus.DISABLED.value, monitor1.switchEnabled)
        self.assertTrue(monitor1.disableVaLimit)


# noinspection PyPep8Naming,PyPep8Naming,PyPep8Naming
class LastTimeQueenContact(base_test.BaseTestView):
    """Tests middleware class on CUD operation from grid firmware, check if last contact is updated on the queen."""

    def _test_queen_last_update(self, queenDeviceId, methodType, url, postDataPath=None):
        """

        :param queenDeviceId:
        :param methodType:
        :param url:
        :param postDataPath:
        :return:
        """
        queen = ph_model.queen.Queen.objects.get(deviceId=queenDeviceId)
        queen.lastContact = None
        queen.save()
        self.assertIsNone(queen.lastContact)
        if methodType.lower() == 'get':
            self.client.get(url)
        elif methodType.lower() == 'post':
            data = self.load_as_json_data(postDataPath)
            self.client.post(url, data)
        self.assertIsNotNone(ph_model.queen.Queen.objects.get(deviceId=queenDeviceId).lastContact)

    def test_queen_last_updated_on_get_ps_config(self):
        """

        :return:
        """
        url = "/api/queens/{}/psconfig/".format('ESPECIALQUEEN')
        self._test_queen_last_update('ESPECIALQUEEN', 'post', url, postDataPath='ps_config')

    def test_queen_last_updated_on_get_circuit(self):
        """

        :return:
        """
        url = "/api/queens/{}/circuit/".format('ESPECIALQUEEN')
        self._test_queen_last_update('ESPECIALQUEEN', 'get', url)

    def test_queen_last_updated_on_get_queen_config(self):
        """

        :return:
        """
        url = "/api/{}/{}/".format('queens', 'ESPECIALQUEEN')
        self._test_queen_last_update('ESPECIALQUEEN', 'get', url)

    def test_queen_last_update_on_post_usage(self):
        """

        :return:
        """
        url = "/api/circuits/usage/"
        self._test_queen_last_update('ESPECIALQUEEN', 'post', url, postDataPath='usage')


class CustomerProbeTestCase(base_test.BaseTestView):
    """
    Tests the effect of customer when a probe is removed
    """

    @staticmethod
    def test_customer_circuit_removed():
        """
        Assert that circuit id and circuit deviceid are cleared
        if a circuit is removed.
        """
        # TODO: Implement test
        pass
