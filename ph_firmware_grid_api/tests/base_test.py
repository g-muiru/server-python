import os
import json

from django.core.management import call_command
from rest_framework import test as rest_framework_test


class BaseTestView(rest_framework_test.APITestCase):
    """Base model for view test classes"""

    def setUp(self):
        """Initialize common tests utilities."""
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_firmware_grid_api/tests/fixtures/grid_config.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/initial_data.json', verbosity=0)
        self.client.login(username='superuser', password='superuser')

    def load_as_json_data(self, schema_name):
        dataDir= os.path.dirname(os.path.realpath(__file__))
        path = "{}/fixtures/{}.json".format(dataDir, schema_name)
        with open(path) as data_file:
          jsonData = json.load(data_file)
          return jsonData