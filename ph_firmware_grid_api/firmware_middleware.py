# coding=utf-8
"""Middleware for ph_firmware_grid_api."""

import datetime
import logging
import re
import json
import ph_model.models as ph_model

LOGGER = logging.getLogger(__name__)


def _get_queen_device_id_from_post_request(request):
    # noinspection PyBroadException
    try:
        return json.loads(request.body).get('header', {}).get('queenDeviceId')
    except Exception:
        return None


def _get_queen_device_id_from_get_request(request):
    # noinspection PyBroadException
    try:
        path = request.path + '/'
        queenId = re.search(r'(.*?)(queens/)(.*?)([/|])', path).groups()[2]
        return queenId
    except Exception:
        return None


class QueenContactServerMiddleware(object):
    """Wrapper for Queen connect firmware middleware."""

    # noinspection PyMethodMayBeStatic
    def process_request(self, request):
        """

        :param request:
        """
        try:
            deviceId = None
            if request.method == 'POST':
                deviceId = _get_queen_device_id_from_post_request(request)
            if request.method == 'GET':
                deviceId = _get_queen_device_id_from_get_request(request)
            if deviceId:
                queen = ph_model.queen.Queen.objects.get_or_none(deviceId=deviceId)
                """ #743: auto-provision new queen on API miss """
                if not queen:
                    queen = ph_model.queen.Queen.objects.create(deviceId=deviceId, grid_id=ph_model.queen.DEFAULT_GRID)
                    queen.firstContactDate = datetime.datetime.utcnow()
                    queen.number = str(deviceId[0:4]) + str(deviceId[8:])
                if queen:
                    queen.lastContact = datetime.datetime.utcnow()
                    queen.save()
        except Exception as e:
            LOGGER.error(e)
