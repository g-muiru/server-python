from django.conf.urls import  include, url
from rest_framework import routers
import views

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'countries', views.CountryViewSet,'Countries')
router.register(r'grids', views.GridViewSet,'Grids')
router.register(r'projects', views.ProjectViewSet,'Projects')
router.register(r'municipalities', views.MunicipalityViewSet,'Municipalities')
router.register(r'powerstations', views.PowerStationViewSet,'Powerstations')
router.register(r'queens', views.QueenViewSet,'Queens')
router.register(r'circuits', views.CircuitViewSet,'Circuits')
router.register(r'customers', views.CustomerViewSet,'Customers')
router.register(r'payments', views.PaymentViewSet,'Payments')
router.register(r'tariffs', views.TariffViewSet,'Tariffs')
router.register(r'probes', views.ProbeViewSet,'Probes')
router.register(r'inverters', views.InverterViewSet,'Inverters')

urlpatterns = [
	url(r'^', include(router.urls)),
	url(r'^usages/$', views.UsageList.as_view(), name='usage'),
	url(r'^events/$', views.EventList.as_view(), name='event'),
    url(r'queenfirmwares/(?P<version>[\w-]+)', views.QueenFirmwareView.as_view(), name='queen_firmware'),
    url(r'time/', views.getTime),
]
