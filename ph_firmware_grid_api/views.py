# coding=utf-8
import enum
import logging
import django.http as django_http
import calendar
import time
import json
import datetime
import tinys3
from dateutil import parser
from wsgiref.util import FileWrapper
from rest_framework.decorators import detail_route, list_route
from django.core import exceptions as django_exception
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
import serializers

import ph_model.models as ph_model
from ph_operation import event_manager
from ph_operation import circuit_manager
from ph_operation import circuit_analyzer
from ph_common_api import json_response
from ph_util import mixins

LOGGER = logging.getLogger(__name__)


class EventRegistredDeviceModels(mixins.PairEnumMixin, enum.Enum):
    """Helper model constants."""
    GRID = ph_model.grid.Grid
    QUEEN = ph_model.queen.Queen
    PROBE = ph_model.probe.Probe
    CIRCUIT = ph_model.circuit.Circuit
    POWERSTATION = ph_model.powerstation.PowerStation
    INVERTER = ph_model.inverter.Inverter
    BATTERYBANK = ph_model.battery.BatteryBank


class ApiException(Exception):
    """Grid api exception, could happen for different reasons, maily serialization issue."""
    pass


class FirmwareNotFoundException(Exception):
    """Firmware not found exception."""
    pass


class CountryViewSet(viewsets.ModelViewSet):
    model = ph_model.country.Country


class GridViewSet(viewsets.ModelViewSet):
    model = ph_model.grid.Grid
    queryset = ph_model.grid.Grid.objects.all()


class ProjectViewSet(viewsets.ModelViewSet):
    model = ph_model.project.Project


class MunicipalityViewSet(viewsets.ModelViewSet):
    model = ph_model.municipality.Municipality


class PowerStationViewSet(viewsets.ModelViewSet):
    model = ph_model.powerstation.PowerStation


class CustomerViewSet(viewsets.ModelViewSet):
    model = ph_model.customer.Customer


class PaymentViewSet(viewsets.ModelViewSet):
    model = ph_model.transaction.MobilePayment


class TariffViewSet(viewsets.ModelViewSet):
    model = ph_model.tariff.TariffSegment


class UsageViewSet(viewsets.ModelViewSet):
    model = ph_model.usage.Usage


class UsageList(APIView):
    """Usage list combination of usage by customer, or un-configured circuit."""
    model = ph_model.usage.Usage
    queryset = ph_model.usage.Usage.objects.all()

    def get(self, request, format=None):
        type = self.request.query_params.get('type') or 'ALL'
        type = type.upper()
        if type == 'CUSTOMER':
            usages = serializers.UsageSerializer(ph_model.usage.Usage.customer_usages.all(), many=True).data
        elif type == 'NON_CUSTOMER':
            usages = serializers.UsageSerializer(ph_model.usage.Usage.non_customer_usages.all(), many=True).data
        else:
            usages = serializers.UsageSerializer(ph_model.usage.Usage.objects.all(), many=True).data
        return Response(usages)


# noinspection PyBroadException
class EventList(generics.GenericAPIView):
    model = ph_model.event.Event
    queryset = ph_model.event.Event.objects.all()

    @staticmethod
    def _get_hardware(eventNumber, itemId):
        try:
            eventType = ph_model.event.EventType.objects.get(eventNumber=eventNumber)
            modelClass = EventRegistredDeviceModels.value_of(eventType.itemType)
            obj = modelClass.objects.get(deviceId=itemId)
        except django_exception.ObjectDoesNotExist:
            try:
                obj = modelClass.objects.get(id=itemId)
            except:
                obj = None
        if not obj:
            raise ApiException("Unknown hardware {}: item deviceId/item pk {}".format(modelClass, itemId))
        return obj

    @staticmethod
    def get(request, format=None):
        events = ph_model.event.Event.objects.all()
        serializer = serializers.EventSerializer(events, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        # Process incoming json
        header = request.data['header']
        event = request.data['data']['event']
        eventDetails = event['details']
        event['collectTime'] = header['collectTime']
        event['itemId'] = self._get_hardware(event['eventType'], event['itemId']).id
        event.pop('details')
        # Save Event
        serializer = serializers.EventSerializer(data=event)
        if serializer.is_valid():
            serializer.object = serializer.save()
            for eventDetail in eventDetails:
                eventDetail['event'] = serializer.data['id']
            # Save EventDetails
            serializerEventDetail = serializers.EventDetailSerializer(data=eventDetails, many=True)
            if serializerEventDetail.is_valid():
                serializerEventDetail.save()
                event_manager.EventManager.update_hardware_if_most_severe(serializer.object)
            else:
                raise ApiException(serializerEventDetail.errors)
        else:
            raise ApiException(serializer.errors)
        event_manager.notify_event(serializer.object)
        return json_response.JsonSuccessPostResponse.get_response(serializer.data)


# noinspection PyBroadException
class QueenViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.QueenSerializer
    model = ph_model.queen.Queen
    queryset = ph_model.queen.Queen.objects.all()
    lookup_field = 'deviceId'

    @staticmethod
    def get_or_none_by_device_id(modelClass, deviceId):
        try:
            obj = modelClass.objects.get(deviceId=deviceId)
        except django_exception.ObjectDoesNotExist:
            obj = None
        return obj

    @staticmethod
    def get_int_or_none(value):
        try:
            return int(value)
        except Exception:
            return None

    @staticmethod
    def _get_or_create_powerstation(reportingQueen):
        """Gets or create powerstation during ps configuration."""
        grid = reportingQueen.grid
        try:
            powerstation = grid.powerstation
        except django_exception.ObjectDoesNotExist:
            powerstation = None
        if not powerstation:
            # TODO(estifanos): Raise event new powerstation is being created
            powerstation = ph_model.powerstation.PowerStation(name="P:{}".format(grid.name), grid=grid)
        if powerstation.queen != reportingQueen:
            # TODO(estifanos) raise event powerstation changed queen
            pass
        powerstation.queen = reportingQueen
        powerstation.save()
        return powerstation

    @detail_route(methods=['POST'])
    def psconfig(self, request, deviceId):
        responseMessages = []
        queen = ph_model.queen.Queen.objects.get(deviceId=deviceId)
        powerstation = self._get_or_create_powerstation(queen)
        inverters = request.data['data'].get('inverters')
        for inverter in inverters:
            inverterObj = self.get_or_none_by_device_id(ph_model.inverter.Inverter, inverter['deviceId'])
            inverter['powerstation'] = powerstation.id
            inverter['type'] = ph_model.inverter.Type.value_of(inverter['type'].upper())
            if inverterObj:
                inverterSerializer = serializers.InverterSerializer(inverterObj, data=inverter)
            else:
                inverterSerializer = serializers.InverterSerializer(data=inverter)
            if inverterSerializer.is_valid():
                inverterSerializer.save()
                responseMessages.append(json_response.JsonSuccessPostResponse.get_response(inverterSerializer.data))
            else:
                responseMessages.append(json_response.JsonErrorResponse.get_response(request, Exception(inverterSerializer.errors)))

        bateryBanks = request.data['data'].get('batteryBanks')
        for batteryBank in bateryBanks:
            batteryBankObj = self.get_or_none_by_device_id(ph_model.battery.BatteryBank, batteryBank['deviceId'])
            batteryBank['powerstation'] = powerstation.id
            if batteryBankObj:
                batteryBankSerializer = serializers.BatteryBankSerializer(batteryBankObj, data=batteryBank)
            else:
                batteryBankSerializer = serializers.BatteryBankSerializer(data=batteryBank)
            if batteryBankSerializer.is_valid():
                batteryBankSerializer.save()
                responseMessages.append(json_response.JsonSuccessPostResponse.get_response(batteryBankSerializer.data))
            else:
                responseMessages.append(json_response.JsonErrorResponse.get_response(request, Exception(batteryBankSerializer.errors)))

        return json_response.JsonQueuedResponse.get_response(responseMessages)

    @detail_route(methods=['POST'])
    def invertermonitor(self, request, deviceId):
        inverters = request.data['data']['inverters']
        for inverter in inverters:
            inverter['collectTime'] = request.data['header']['collectTime']
            inverter['inverter'] = ph_model.inverter.Inverter.objects.get(deviceId=inverter['deviceId']).id
        inverterMonitorSerializer = serializers.InverterMonitorSerializer(data=inverters, many=True)
        if inverterMonitorSerializer.is_valid():
            inverterMonitorSerializer.save()
            return json_response.JsonSuccessPostResponse.get_response(inverterMonitorSerializer.data)
        raise ApiException(inverterMonitorSerializer.errors)

    @detail_route(methods=['POST'])
    def batterybankmonitor(self, request, deviceId):
        batteryBanks = request.data['data']['batteryBanks']
        for batterybank in batteryBanks:
            batterybank['collectTime'] = request.data['header']['collectTime']
            batterybank['batteryBank'] = ph_model.battery.BatteryBank.objects.get(deviceId=batterybank['deviceId']).id
        batteryBankMonitorSerializer = serializers.BatteryBankMonitorSerializer(data=batteryBanks, many=True)
        if batteryBankMonitorSerializer.is_valid():
            batteryBankMonitorSerializer.save()
            return json_response.JsonSuccessPostResponse.get_response(batteryBankMonitorSerializer.data)
        raise ApiException(batteryBankMonitorSerializer.errors)

    @detail_route(methods=['POST'])
    def generation(self, request, deviceId):
        header = request.data['header']
        generation = request.data['data']['generation']
        generation['grid'] = ph_model.queen.Queen.objects.get(deviceId=deviceId).grid.id
        generation['collectTime'] = header['collectTime']
        # !!!TODO: fix this in the firmware, queen shouldnt be sending nulls
        LOGGER.debug(generation['vacAvg'])
        if generation.get('vacAvg', 0) is None:
            generation['vacAvg'] = 0
        if generation.get('socAvg', 0) is None:
            generation['socAvg'] = 0
        if generation.get('freqAvg', 0) is None:
            generation['freqAvg'] = 0
        serializer = serializers.GenerationSerializer(data=generation)
        if serializer.is_valid():
            serializer.save()
            return json_response.JsonSuccessPostResponse.get_response(serializer.data)
        raise ApiException(serializer.errors)

    @detail_route(methods=['POST'])
    def monitor(self, request, deviceId):
        queenMonitor = request.data['data']['queen']
        header = request.data['header']
        queen = ph_model.queen.Queen.objects.get(deviceId=deviceId)
        queenMonitor['queen'] = queen.id
        queenMonitor['collectTime'] = header['collectTime']
        serializer = serializers.QueenMonitorSerializer(data=queenMonitor)
        if serializer.is_valid():
            serializer.save()
            collectTime = parser.parse(header.get('collectTime'))
            osUptime = self.get_int_or_none(queenMonitor.get('osUptime'))
            fwUptime = self.get_int_or_none(queenMonitor.get('fwUptime'))
            lanAddress = queenMonitor.get('lanAddress')
            wanAddress = queenMonitor.get('wanAddress')
            queen.fsFreeStatus = self.get_int_or_none(queenMonitor.get('fsFreeStatus'))
            queen.fsFreePct = self.get_int_or_none(queenMonitor.get('fsFreePct'))
            if lanAddress:
                queen.lanAddress = lanAddress
            if wanAddress:
                queen.wanAddress = wanAddress
            if osUptime:
                queen.osStarted = (collectTime - datetime.timedelta(seconds=osUptime)).strftime('%Y-%m-%dT%H:%M:%SZ')
            if fwUptime:
                queen.fwStarted = (collectTime - datetime.timedelta(seconds=fwUptime)).strftime('%Y-%m-%dT%H:%M:%SZ')
            queen.save()
            return json_response.JsonSuccessPostResponse.get_response(serializer.data)
        raise ApiException(serializer.errors)

    @detail_route(methods=['GET'])
    def circuit(self, request, deviceId):
        """
        Retrieve the circuits for the queen
        """
        queen = ph_model.queen.Queen.objects.get(deviceId=deviceId)
        queryset = ph_model.circuit.Circuit.objects.filter(queen=queen)
        circuitSerializer = serializers.CircuitSerializer(queryset, many=True)
        data = circuitSerializer.data
        return Response(data)

    @detail_route(methods=['GET'])
    def command(self, request, deviceId):
        queen = ph_model.queen.Queen.objects.get(deviceId=deviceId)
        commandSerializer = serializers.QueenCommandSerializer(queen)
        data = commandSerializer.data

        # Reset reboot to false if true
        if queen.reboot:
            queen.reboot = False

        # Reset tunnel to 0 if not 0
        if queen.tunnel:
            queen.tunnel = 0

        # Reset ethConnect to empty if not empty
        if queen.ethConnect:
            queen.ethConnect = ""

        # Reset generateKeyPair to false if true
        if queen.generateKeyPair:
            queen.generateKeyPair = False

        # Reset sendPublicKey to empty if not empty
        if queen.sendPublicKey:
            queen.sendPublicKey = ""

        # Reset sendPublicKeyPassword to empty if not empty
        if queen.sendPublicKeyPassword:
            queen.sendPublicKeyPassword = ""

        queen.save()

        return Response(data)

    @detail_route(methods=['POST'])
    def config(self, request, deviceId):
        """
        Queen sends configuration for itself and its probes/circuits.
        This PUT endpoint will handle adding records if existing
        resources are not availble for the deviceId.
        """
        queenJson = request.data['data']['queen']

        try:
            existingQueen = ph_model.queen.Queen.objects.get(deviceId=queenJson['deviceId'])
            queenJson['grid'] = existingQueen.grid.id
        except ph_model.queen.Queen.DoesNotExist:
            existingQueen = None
            queenJson['grid'] = ph_model.grid.Grid.objects.get(name=ph_model.grid.DEFAULT_GRID_NAME).id
        currentFirmware = ph_model.queen.QueenFirmware.objects.get_or_none(version=queenJson['currentFirmwareVersion'])
        if currentFirmware:
            currentFirmware = currentFirmware.id
        queenJson['currentFirmware'] = currentFirmware
        if existingQueen is None:
            serializerQueen = serializers.QueenUpdateSerializer(data=queenJson)
        else:
            serializerQueen = serializers.QueenUpdateSerializer(existingQueen, data=queenJson)

        if serializerQueen.is_valid():
            serializerQueen.save()
        else:
            raise ApiException(serializerQueen.errors)

        probesJson = request.data["data"]["probes"]

        # Disconnect probe from queen that are not included in the queen's config
        currentProbes = ph_model.probe.Probe.objects.filter(queen=serializerQueen.data['id'])
        for currentProbe in currentProbes:
            disconnectProbe = True
            for probeJson in probesJson:
                if probeJson.get("deviceId") == currentProbe.deviceId:
                    disconnectProbe = False
                    break

            if disconnectProbe:
                currentProbe.queen = None
                currentProbe.number = None
                currentProbe.save()

        for probeJson in probesJson:
            # Attach queen to data
            probeJson['queen'] = serializerQueen.data['deviceId']

            try:
                existingProbe = ph_model.probe.Probe.objects.get(deviceId=probeJson['deviceId'])
            except ph_model.probe.Probe.DoesNotExist:
                existingProbe = None

            if existingProbe is None:
                serializerProbe = serializers.ProbeUpdateSerializer(data=probeJson)
            else:
                serializerProbe = serializers.ProbeUpdateSerializer(existingProbe, data=probeJson)

            if serializerProbe.is_valid():
                serializerProbe.save()
            else:
                raise ApiException(serializerProbe.errors)

        priority_circuit_ids = ph_model.circuit.PriorityCircuit.objects.all().values_list('circuit', flat=True)
        circuitsJson = request.data["data"]["circuits"]
        for circuitJson in circuitsJson:
            # Attach queen to data
            circuitJson["queen"] = serializerQueen.data["deviceId"]

            # Handle no reference to probe
            if not circuitJson.get("probe"):
                # return Response("no probe")
                circuitJson["probe"] = ""
                circuitJson["switchEnabled"] = "DISCONNECTED"

            try:
                existingCircuit = ph_model.circuit.Circuit.objects.get(deviceId=circuitJson['deviceId'])
            except ph_model.circuit.Circuit.DoesNotExist:
                existingCircuit = None

            if existingCircuit is None:
                serializerCircuit = serializers.CircuitUpdateSerializer(data=circuitJson)
            else:
                serializerCircuit = serializers.CircuitUpdateSerializer(existingCircuit, data=circuitJson)
                # clear priority notification flags, if needed
                if existingCircuit.id in priority_circuit_ids:
                    priority_circuit = ph_model.circuit.PriorityCircuit.objects.filter(circuit__id=existingCircuit.id)[0]
                    priority_circuit.notified = False
                    priority_circuit.save()

            if serializerCircuit.is_valid():
                serializerCircuit.save()
            else:
                raise ApiException(serializerCircuit.errors)

        # # TODO: Remove probe association from circuits if
        # # probe deviceId is not in the circuit json
        # probes = ph_model.probe.Probe.objects.filter(queen__deviceId=serializerQueen.data["deviceId"])
        #
        # for probe in probes:
        #     matched = False
        #
        #     for probeJson in probesJson:
        #         if probeJson.get("deviceId") == probe.deviceId:
        #             matched = True
        #             break
        #     if not matched:
        #         # Delete probe will cascade delete onto circuits as defined in the DB schema
        #         probe.delete()

        return json_response.JsonSuccessPostResponse.get_response(serializerQueen.data)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['POST'])
    # save functional test data  to S3 until we figure out what else to do with it
    def functionaltest(self, request):
        my_endpoint = "s3-us-west-2.amazonaws.com"
        conn = tinys3.Connection("AKIAJK63HWFOFZHKTIXA", 'u/E3niuOQ+CEXmntS7LTtZcsCG+sDzFDkMaMHZOR', tls=True, endpoint=my_endpoint)
        responseMessages = []
        if request.method == 'POST':
            conn.upload(request.FILES['file'].name, request.FILES['file'], 'asali-functional-test')
        return json_response.JsonQueuedResponse.get_response(responseMessages)


class ProbeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProbeSerializer
    model = ph_model.probe.Probe
    queryset = ph_model.probe.Probe.objects.all()
    lookup_field = 'deviceId'

    @list_route(methods=['POST'])
    def monitor(self, request):
        probeMonitors = request.data['data']['probes']
        collectTime = request.data['header']['collectTime']
        for probeMonitor in probeMonitors:
            try:
                probeMonitor['probe'] = ph_model.probe.Probe.objects.get(deviceId=probeMonitor['deviceId']).id
                probeMonitor['collectTime'] = collectTime
            except ph_model.probe.Probe.DoesNotExist:
                LOGGER.warning("Probe does not exist")
        serializer = serializers.ProbeMonitorSerializer(data=probeMonitors, many=True)
        if serializer.is_valid():
            serializer.save()
            return json_response.JsonSuccessPostResponse.get_response(serializer.data)
        return json_response.JsonErrorResponse.get_response(request, serializer.errors)


class InverterViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.InverterSerializer
    model = ph_model.inverter.Inverter
    queryset = ph_model.inverter.Inverter.objects.all()
    lookup_field = 'deviceId'

    @list_route(methods=['POST'])
    # save inverter data dump csv to S3 until we figure out what else to do with it
    # !!! not totally sure this is the best place to do this
    def monitor(self, request):
        my_endpoint = "s3-us-west-2.amazonaws.com"
        conn = tinys3.Connection("AKIAJK63HWFOFZHKTIXA", 'u/E3niuOQ+CEXmntS7LTtZcsCG+sDzFDkMaMHZOR', tls=True, endpoint=my_endpoint)
        responseMessages = []
        if request.method == 'POST':
            conn.upload(request.FILES['file'].name, request.FILES['file'], 'inverters')
        return json_response.JsonQueuedResponse.get_response(responseMessages)


class CircuitViewSet(viewsets.ModelViewSet):
    """
    Process usages from the meter
    """
    serializer_class = serializers.CircuitSerializer
    model = ph_model.circuit.Circuit
    queryset = ph_model.circuit.Circuit.objects.all()
    lookup_field = 'deviceId'

    @list_route(methods=['POST'])
    def usage(self, request):
        usages = request.data['data']['usages']
        header = request.data['header']
        if not usages:
            if request.data['header']['queenDeviceId']:
                message = "Possible metering chip fail: no data from circuit monitor"
                queenDeviceId = request.data['header']['queenDeviceId']
                queen = ph_model.queen.Queen.objects.get(deviceId=queenDeviceId)
                event_manager.raise_event(queen.id, 205, [{'message': message}])
        customerUsages = []
        for usage in usages:
            usage['collectTime'] = header['collectTime']
            deviceId = usage.pop('deviceId')
            circuit = ph_model.circuit.Circuit.objects.get(deviceId=deviceId)
            usage['circuit'] = circuit.id
            # Do fault detection
            ca = circuit_analyzer.CircuitAnalyzer()
            ca.detect_fault(circuit, usage['intervalVAmax'])
            # detect groundfaults
            ca.detect_groundfault(circuit, usage['intervalVAmax'], usage['intervalVAmaxH'])
            # Do meter detection
            ca.detect_meter_issue(circuit, usage['intervalVAmax'], usage['intervalWh'])

            try:
                usage['customer'] = ph_model.customer.Customer.objects.get(circuit__deviceId=deviceId).id
            except ph_model.customer.Customer.DoesNotExist:
                usage['processed'] = True
            customerUsages.append(usage)
        serializer = serializers.UsageSerializer(data=customerUsages, many=True)
        responseData = []
        if serializer.is_valid():
            serializer.save()
            responseData.append(serializer.data)
        if serializer.is_valid():
            return json_response.JsonSuccessPostResponse.get_response(responseData)
        raise ApiException(serializer.errors)

    @list_route(methods=['POST'])
    def monitor(self, request):
        circuitMonitors = request.data['data']['circuits']
        collectTime = request.data['header']['collectTime']
        if not circuitMonitors:
            if request.data['header']['queenDeviceId']:
                message = "Possible metering chip fail: no data from circuit monitor"
                queenDeviceId = request.data['header']['queenDeviceId']
                queen = ph_model.queen.Queen.objects.get(deviceId=queenDeviceId)
                event_manager.raise_event(queen.id, 205, [{'message': message}])
        for circuitMonitor in circuitMonitors:
            circuitMonitor['circuit'] = ph_model.circuit.Circuit.objects.get(deviceId=circuitMonitor['deviceId']).id
            circuitMonitor['collectTime'] = collectTime
        serializer = serializers.CircuitMonitorSerializer(data=circuitMonitors, many=True)
        if serializer.is_valid():
            # !!! TODO: probably do this in the serializers
            serializer.object = serializer.save()
            circuit_manager.CircuitManager.configure_circuits_by_circuit_monitor(serializer.object)
            return json_response.JsonSuccessPostResponse.get_response(serializer.data)
        raise ApiException(serializer.errors)


class QueenFirmwareView(APIView):
    """Usage list combination of usage by customer, or un-configured circuit."""
    # application type application/octet-stream according RFC2046 or better yet use zip file.
    model = ph_model.queen.QueenFirmware
    queryset = ph_model.queen.QueenFirmware.objects.all()

    @staticmethod
    def get(request, version):
        """
        Retrieve queen-firmware as downloadable file.
        """
        queenFirmware = ph_model.queen.QueenFirmware.objects.get_or_none(version=version)
        if queenFirmware:
            # Wrapper will close the file.
            zip_file = open(queenFirmware.filePath, 'rb')
            response = django_http.HttpResponse(FileWrapper(zip_file), content_type='application/octet-stream')
            response['Content-Disposition'] = 'attachment; filename={}'.format(queenFirmware.fileName)
            return response
        raise FirmwareNotFoundException('Firmware version-{} not found'.format(version))


def getTime(request):
    output = {'epochTime': calendar.timegm(time.gmtime())}

    return django_http.HttpResponse(json.dumps(output), content_type="application/json")
