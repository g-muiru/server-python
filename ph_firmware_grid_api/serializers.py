from rest_framework import serializers
import ph_model.models as ph_model
import logging
import calendar
import time

LOGGER = logging.getLogger(__name__)



class QueenFirmwareSerializer(serializers.ModelSerializer):
     url = serializers.CharField()

     class Meta:
        fields = ('version', 'md5Sum', 'url')
        model = ph_model.queen.QueenFirmware

class QueenCommandSerializer(serializers.ModelSerializer):
    class Meta:
        model = ph_model.queen.Queen
        fields = ('reboot', 'tunnel', 'ethConnect', 'generateKeyPair', 'sendPublicKey', 'sendPublicKeyPassword')

class QueenSerializer(serializers.ModelSerializer):
    isPs = serializers.SerializerMethodField('get_is_ps')

    expectedFirmware = QueenFirmwareSerializer()
    currentFirmware = QueenFirmwareSerializer()

    def get_is_ps(self, data):
        if (hasattr(data.grid, 'powerstation') and data.grid.powerstation.queen == data):
            isPs = 1
        else:
            isPs = 0
        return isPs

    class Meta:
        model = ph_model.queen.Queen
        expectedFirmware = QueenFirmwareSerializer()
        fields = ('expectedFirmware', 'currentFirmware', 'verbosity', 'usageInterval',
                  'generationInterval', 'monitorInterval', 'isPs')

class QueenUpdateSerializer(serializers.ModelSerializer):
    #currentFirmware = serializers.SlugRelatedField(slug_field='version')

    class Meta:
        model = ph_model.queen.Queen
        fields = ('id', 'deviceId', 'grid', 'currentFirmware', 'lanAddress', 'wanAddress')

class CircuitSerializer(serializers.ModelSerializer):
    enabledRequest = serializers.BooleanField(source='server_request_enabled')
    vaLimit = serializers.FloatField()

    def get_identity(self, data):
        return data.get('deviceId', None)

    class Meta:
        model = ph_model.circuit.Circuit
        fields = ('enabledRequest', 'vaLimit', 'number')

class CircuitUpdateSerializer(serializers.ModelSerializer):
    queen = serializers.SlugRelatedField(slug_field='deviceId',queryset=ph_model.queen.Queen.objects.all())
    probe = serializers.SlugRelatedField(slug_field='deviceId',queryset=ph_model.probe.Probe.objects.all(), required=False, allow_null = True)

    def get_identity(self, data):
        return data.get('deviceId', None)

    class Meta:
        model = ph_model.circuit.Circuit
        fields = ('deviceId', 'number', 'queen', 'probe', 'switchEnabled')

class CircuitUpdateNoProbeSerializer(serializers.ModelSerializer):
    queen = serializers.SlugRelatedField(slug_field='deviceId',queryset=ph_model.queen.Queen.objects.all())

    def get_identity(self, data):
        return data.get('deviceId', None)

    class Meta:
        model = ph_model.circuit.Circuit
        fields = ('deviceId', 'number', 'queen')

class ProbeSerializer(serializers.ModelSerializer):
    circuits = CircuitSerializer(required=False, many=True)
    queen = serializers.SlugRelatedField(slug_field='deviceId',queryset=ph_model.queen.Queen.objects.all())

    def get_identity(self, data):
        try:
            return data.get('deviceId', None)
        except AttributeError:
            return None

    class Meta:
        fields = ('number', 'queen', 'deviceId', 'id')
        model = ph_model.probe.Probe

class ProbeUpdateSerializer(serializers.ModelSerializer):
    queen = serializers.SlugRelatedField(slug_field='deviceId',queryset=ph_model.queen.Queen.objects.all())

    class Meta:
        model = ph_model.probe.Probe
        fields = ('queen', 'deviceId', 'number')

class InverterSerializer(serializers.ModelSerializer):
    serialNumber = serializers.CharField(required=False)

    class Meta:
        model = ph_model.inverter.Inverter
        fields = ('deviceId', 'serialNumber', 'type', 'powerstation')


class BatteryBankSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('deviceId', 'powerstation', 'capacity', 'stateOfCharge')
        model = ph_model.battery.BatteryBank


class QueenMonitorSerializer(serializers.ModelSerializer):
    fsFreeStatus = serializers.IntegerField(min_value=0, required=False)
    fsFreePct = serializers.DecimalField(max_value=100, min_value=0, decimal_places=10, max_digits = 999, required=False)

    class Meta:
        model = ph_model.monitor.QueenMonitor
        fields = ('queen', 'collectTime', 'rssiAvg', 'rssiMin', 'rssiMax', 'tempAvg', 'tempMin', 'tempMax',
                  'photoAvg', 'photoMin', 'photoMax', 'commAttempts', 'commErrors', 'osUptime', 'fwUptime', 'fwVersion',
                  'fsFreeStatus', 'fsFreePct', 'hardwareReinits')

    def get_validation_exclusions(self, instance=None):
        exclusions = super(QueenMonitorSerializer, self).get_validation_exclusions(instance)
        return exclusions + ['fsFreeStatus', 'fsFreePct']


class ProbeMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ph_model.monitor.ProbeMonitor

class CircuitMonitorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ph_model.monitor.CircuitMonitor
        fields = ('circuit', 'collectTime', 'vacAvg', 'vacMin', 'vacMax', 'iacMinN', 'iacMaxN', 'iacAvgN', 'iacMinH',
                    'iacMaxH', 'iacAvgH','commAttempts', 'commErrors', 'switchEnabled', 'switchReason', 'disableVaLimit')


class InverterMonitorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ph_model.monitor.InverterMonitor


class BatteryBankMonitorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ph_model.monitor.BatteryBankMonitor

class UsageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ph_model.usage.Usage
        fields = ('customer', 'circuit', 'intervalWh', 'intervalVAmax', 'intervalWhH', 'intervalVAmaxH', 'intervalVmin', 'collectTime','processed')

class GenerationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ph_model.generation.Generation

class EventDetailSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'event', 'itemName', 'itemValue')
        model = ph_model.event.EventDetail


class EventSerializer(serializers.ModelSerializer):
    details = EventDetailSerializer(many=True, required=False)
    eventType = serializers.SlugRelatedField(slug_field='eventNumber',queryset=ph_model.event.EventType.objects.all())


    class Meta:
        fields = ('id', 'eventType', 'itemId', 'collectTime', 'details')
        model = ph_model.event.Event

