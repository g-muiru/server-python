"""Fixture to load Legacy db."""
import csv
import json
import logging
import os
import ph_model
from ph_model.fixtures.common import loader as common_loader

LOGGER = logging.getLogger(__name__)

CSV_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/csv/{}'
USAGE_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/csv/usage/{}'
JSON_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/json/{}'


LOCATION_DATA = [(CSV_DIR_TEMP.format('country.csv'), 'ph_model.Country'),
                 (CSV_DIR_TEMP.format('region.csv'), 'ph_model.Region'),
                 (CSV_DIR_TEMP.format('municipality.csv'), 'ph_model.Municipality'),
                 (CSV_DIR_TEMP.format('project.csv'), 'ph_model.Project'),
                 (CSV_DIR_TEMP.format('permission.csv'), 'ph_model.Permission')]

DEVICE_DATA = [(CSV_DIR_TEMP.format('grid.csv'), 'ph_model.Grid'),
               (CSV_DIR_TEMP.format('queen.csv'), 'ph_model.Queen'),
               (CSV_DIR_TEMP.format('probe.csv'), 'ph_model.Probe'),
               (CSV_DIR_TEMP.format('circuit.csv'), 'ph_model.Circuit')]


def load_location_data():
    common_loader.load_one_to_one_mapping_fixture(LOCATION_DATA,
                                                  jsonDestinationPath=JSON_DIR_TEMP.format('location.json'))
    print 'Populate location data...................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/stg_db/json/location.json')


def load_device_data():
    common_loader.load_one_to_one_mapping_fixture(DEVICE_DATA,
                                                  jsonDestinationPath=JSON_DIR_TEMP.format('device.json'))
    print 'Populate device data......................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/stg_db/json/device.json')


def load_customer_data():
    """Loads customer csv data to Customer, Account and PhoneAccount fixtures to fixture and db.."""
    csvFilePath = CSV_DIR_TEMP.format('customer.csv')
    output = []
    #customerId,firstName,lastName,email,mobileMoneyNumber,deviceId
    customerFields = ['pk', 'customerId', 'firstName', 'lastName', 'email', 'phoneNumber',
                      'account', 'tariff']
    accountField = ['pk']
    phoneAccountFields = ['pk', 'account', 'firstName', 'lastName', 'mobileMoneyNumber', 'networkProvider']
    with open(csvFilePath) as csvfile:
            csvReader = csv.DictReader(csvfile)
            for line in csvReader:
                customerRow = common_loader.construct_fixture_object('ph_model.customer', 'pk', customerFields, line, mapping={'account': 'pk'})
                output.append(customerRow)
                accountRow = common_loader.construct_fixture_object('ph_model.Account', 'pk', accountField, line)
                output.append(accountRow)
                phoneAccount = common_loader.construct_fixture_object('ph_model.PhoneAccount', 'pk', phoneAccountFields,
                    line, mapping={'account': 'pk', 'mobileMoneyNumber': 'phoneNumber'})
                output.append(phoneAccount)
    with open(JSON_DIR_TEMP.format('customer.json'), 'w') as jsonFile:
        json.dump(output, jsonFile, indent=2)
    print 'Populate device data......................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/stg_db/json/customer.json')

def associate_customer_to_circuit():
    """Associate customer to circuit from csv file."""
    csvFilePath = CSV_DIR_TEMP.format('customer_to_circuit.csv')
    with open(csvFilePath) as csvfile:
        csvReader = csv.DictReader(csvfile)
        for line in csvReader:
            try:
              circuit = ph_model.circuit.Circuit.objects.get(deviceId=line['deviceId'])
              customer = ph_model.customer.Customer.objects.get(customerId=line['customerId'])
              customer.circuit = circuit
              customer.circuitDeviceId = circuit.deviceId
              customer.save()
            except Exception:
                LOGGER.error('Circuit: {} or circuit {} not found'.format(line['deviceId'], line['customerId']))

def load():
    """Load stg data."""
    common_loader.load()
    load_location_data()
    load_device_data()
    load_customer_data()
    associate_customer_to_circuit()



