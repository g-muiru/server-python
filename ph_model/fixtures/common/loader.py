"""Fixture to load powerhive prod data.
Usage:
   ph_model.fixtures.prod_data_loader.init():Constructs and loads prod data fixtures..
"""

import csv
import datetime
import decimal
import json
import logging
import random
import os
import uuid

from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist

import ph_model.models as ph_model

LOGGER = logging.getLogger(__name__)

CSV_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/csv/{}'
JSON_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/json/{}'

CSV_FILE_PATH, MODEL_NAME, PK_IDENTIFER = xrange(3)


def get_rand_string(digits=10):
   """Generates 10 digit random numbers as string. Uses double randomizing uuid 32 byte then randomize to 10 byte"""
   return "".join(random.sample(list(str(uuid.uuid4().int)), digits))


def get_subset_of_dict(bigDict, keys):
    """Extract subset of a dictionary."""
    return dict([(k, bigDict[k]) for k in keys if k in bigDict])

def to_deci(val):
    """Convert to decimal."""
    return decimal.Decimal(val)

def to_utc(val):
    """Convert to utc datetime."""
    return  ph_model.fields.DateTimeUTC().to_python(val)


def check_exception(func):
   """Wrapps exception."""
   def silenceit(*args, **kwargs):
      try:
         return func(*args, **kwargs)
      except IntegrityError, e:
         print '---------------------------------------------------------------------------------'
         print 'There is data Integrity issue: {}'.format(str(e))
         print 'Clean up database by running ph_model.fixtures.common.cleanup.cleanup()'
         print '----------------------------------------------------------------------------------'
      except Exception as e:
         print 'There is an error: {}'.format(str(e))
   return silenceit

def get_index(listData, index, default=None):
    """Returns index of a list, if error occured returns @param default value."""
    try:
        value = listData[index]
    except IndexError:
        value = default
    return value

def print_time():
    print '---------------------------------------------------------------------------------'
    print str(datetime.datetime.now())
    print '---------------------------------------------------------------------------------'

def load_one_to_one_mapping_fixture(csvConfigs, jsonDestinationPath, pkIndexProvided=True):
    """csv fiels to django json format fixture converter. It works fine if csv and model have one to one mapping

    Arguments:
      csvConfig: Tuple of (csv file name, associated model, primary key identifer field)
      jsonDestinationPath: output json file name to write into.

    Returns:
       List of fixture or write to destination file if file provided.
    """

    output = []
    for csvConfig in csvConfigs:
        csvfilePath = get_index(csvConfig, CSV_FILE_PATH)
        modelName = get_index(csvConfig, MODEL_NAME)
        pk = 1
        if pkIndexProvided:
            pkIdentifier = get_index(csvConfig, PK_IDENTIFER, 'pk')
        else:
            pkIdentifier = get_index(csvConfig, PK_IDENTIFER, None)
        with open(csvfilePath) as csvfile:
            reader = csv.DictReader(csvfile)
            for each in reader:
              row = {
                  'model':modelName,
                  'fields': {},
                  'pk': pk
              }
              pk=pk+1
              for field in reader.fieldnames:

                if field == pkIdentifier:
                    row['pk'] = each[field]
                else:
                    fieldValue = each[field].lower()
                    if not fieldValue or fieldValue == 'none' or fieldValue == 'null':
                      continue
                    row['fields'][field] = each[field]
              output.append(row)
    if not jsonDestinationPath:
        return output
    if output:
        with open(jsonDestinationPath, 'w') as jsonFile:
            json.dump(output, jsonFile, indent=2)



def construct_fixture_object(modelName, pkIdentifier, fields, line, mapping=None):
    """Helper to construct django fixture json format.

    Attributes:
        modelName: Full qualified django model name.
        pkIdentifier: Primary key identifier of the model.
        fields: Fields to add to the model.
        line: Csv line reader
        mapping: mapping of any fields to use value for the fields.
    """
    row = {
                  'model':modelName,
                  'pk': line[pkIdentifier],
                  'fields': {}
    }
    for field in fields:
        if field == pkIdentifier:
            continue
        if mapping and field in mapping.keys():
            value = line[mapping[field]] or None
            row['fields'][field] = value
            continue
        value = line[field] or None
        row['fields'][field] = value
    return row


def load_pseudo_powerstation():
    """Loads psudo  Batterybank, inverter and powerstation. to simulate new db schema."""
    grids = ph_model.grid.Grid.objects.all()
    for grid in grids:
        try:
            if grid.powerstation:
                pass
        except ObjectDoesNotExist:
            powerstation= ph_model.powerstation.PowerStation(grid=grid, name='P:{}'.format(grid.name))
            powerstation.save()
            batteryBank= ph_model.battery.BatteryBank(deviceId="BB:{}".format(grid.name), powerstation=powerstation)
            batteryBank.save()
            inverterSerialNumber = get_rand_string()
            inverter = ph_model.inverter.Inverter(powerstation=powerstation, deviceId=inverterSerialNumber,
                 serialNumber=inverterSerialNumber, type='PV')
            inverter.save()


def _load_tariff():
    """Load tariff."""
    load_one_to_one_mapping_fixture([
                          (CSV_DIR_TEMP.format('tariff_segment.csv'), 'ph_model.TariffSegment', 'segmentId'),
                          (CSV_DIR_TEMP.format('tariff_calendar.csv'), 'ph_model.TariffCalendar', 'calendarId'),
                          (CSV_DIR_TEMP.format('tariff.csv'), 'ph_model.Tariff', 'tariffId')
                         ], jsonDestinationPath=JSON_DIR_TEMP.format('tariff.json'))



def _load_event_types():
    """Load tariff."""
    load_one_to_one_mapping_fixture([
                          (CSV_DIR_TEMP.format('event_type.csv'), 'ph_model.EventType', 'segmentId')
                         ], jsonDestinationPath=JSON_DIR_TEMP.format('event_types.json'), pkIndexProvided=False)


def load_mobile_payment_to_master_transaction_model():
    for o in ph_model.transaction.MobilePayment.objects.all():
       try:
         ph_model.transaction.save_to_master_transaction(o, o.__class__.__name__)
       except:
         pass


def load_scratch_card_payment_to_master_transaction_model():
    for o in ph_model.transaction.ScratchcardPaymentHistory.objects.all():
       try:
         ph_model.transaction.save_to_master_transaction(o, o.__class__.__name__)
       except:
         pass

def load_credit_adjustment_to_master_transaction_model():
    for o in ph_model.transaction.CreditAdjustmentHistory.objects.all():
       try:
         ph_model.transaction.save_to_master_transaction(o, o.__class__.__name__)
       except:
         pass

def load_usage_charge_to_master_transaction_model():
    for o in ph_model.transaction.UsageCharge.objects.all():
       try:
         ph_model.transaction.save_to_master_transaction(o, o.__class__.__name__)
       except:
         pass


def load_loan_history_to_master_transaction_model():
    for o in ph_model.loan.LoanPaymentHistory.objects.all():
       try:
         ph_model.transaction.save_to_master_transaction(o, o.__class__.__name__)
       except:
         pass


def load():
    """Load common data."""
    _load_tariff()


    print 'Populate Country data ........................................................'
    os.system('./manage.py loaddata ph_model/fixtures/common/json/countries.json')
    
    print 'Populate Mobile Network service provider data ........................................................'
    os.system('./manage.py loaddata ph_model/fixtures/common/json/net_work_service_provider.json')
    
    #print 'Populate initial grid data...................................................................'
    #os.system('./manage.py loaddata ph_model/fixtures/common/json/initial_data.json')

    print 'Populate network service data...................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/common/json/sms_service_provider.json')

    print 'Populate tariff data ...................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/common/json/tariff.json')

    _load_event_types()
    print 'Populate event types data ..............................................................'
    os.system('./manage.py loaddata ph_model/fixtures/common/json/event_types.json')
