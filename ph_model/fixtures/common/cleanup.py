"""Fixture to cleanup database, this will recreate wholse schema."""
import shutil
import os

from django.db import connection


def cleanup():
    """Cleanup existing ph_models."""
    apps = ['ph_model', 'webui']
    tables = ['account', 'accountrule', 'alarm', 'battery', 'batterybank', 'country', 'circuit', 'circuitMonitor', 'customer', 'PotentialCustomer',
              'PotentialCustomerImportHistory', 'customertariffhistory',
              'event', 'eventdetail', 'eventtype', 'generation', 'grid', 'hardwarestatus', 'inverter', 'invertor', 'monitor',
              'probemonitor', 'invertermonitor', 'batterybankmonitor', 'municipality',
              'networkprovider', 'networkserviceprovider', 'payment', 'PaymentSyncHistory', 'permission', 'phoneaccount', 'powerstation',
              'probe', 'project', 'PowerhiveAccount', 'PowerhiveSMSAccount', 'queen', 'queenfirmware', 'queenmonitor', 'region',
              'sms', 'SMSMessage', 'SMSServiceProvider', 'SMSSyncHistory', 'SwitchEnabledHistory',
              'tariff', 'tariffCalendar', 'tariffbracket','tariffsegment', 'transaction', 'mobilepayment',
              'usagecharge', 'usage', 'NonCustomerUsage', 'user', 'loanpaymenthistory', 'loan',
              'creditadjustmenthistory', 'aggregategrid', 'aggregategridtracking', 'aggregatetracking']

    dropTableTemplate = 'DROP TABLE if EXISTS {}_{} cascade;'

    cleanUpQuery =''
    for app in apps:
        for table in tables:
            cleanUpQuery += dropTableTemplate.format(app, table)
    cursor = connection.cursor()
    
    print 'Removing Previous migration artifacts and running initial south migration........'
    shutil.rmtree('ph_model/migrations', ignore_errors=True)
    os.system('./manage.py schemamigration ph_model --initial; ./manage.py migrate ph_model')
