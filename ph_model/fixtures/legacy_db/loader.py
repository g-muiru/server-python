"""Fixture to load Legacy db."""
import decimal
import csv
import json
import logging
import os
import ph_model
from ph_model.fixtures.common import loader as common_loader

LOGGER = logging.getLogger(__name__)

CSV_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/csv/{}'
USAGE_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/csv/usage/{}'
JSON_DIR_TEMP = os.path.dirname(os.path.realpath(__file__)) + '/json/{}'


LOCATION_DATA = [(CSV_DIR_TEMP.format('country.csv'), 'ph_model.Country'),
                 (CSV_DIR_TEMP.format('region.csv'), 'ph_model.Region'),
                 (CSV_DIR_TEMP.format('municipality.csv'), 'ph_model.Municipality'),
                 (CSV_DIR_TEMP.format('project.csv'), 'ph_model.Project'),
                 (CSV_DIR_TEMP.format('permission.csv'), 'ph_model.Permission')]

DEVICE_DATA = [(CSV_DIR_TEMP.format('grid.csv'), 'ph_model.Grid'),
               (CSV_DIR_TEMP.format('queen.csv'), 'ph_model.Queen'),
               (CSV_DIR_TEMP.format('probe.csv'), 'ph_model.Probe'),
               (CSV_DIR_TEMP.format('circuit.csv'), 'ph_model.Circuit')]


def load_location_data():
    common_loader.load_one_to_one_mapping_fixture(LOCATION_DATA,
                                                  jsonDestinationPath=JSON_DIR_TEMP.format('location.json'))
    print 'Populate location data...................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/legacy_db/json/location.json')


def load_device_data():
    common_loader.load_one_to_one_mapping_fixture(DEVICE_DATA,
                                                  jsonDestinationPath=JSON_DIR_TEMP.format('device.json'))
    print 'Populate device data......................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/legacy_db/json/device.json')


def load_customer_data():
    """Loads customer csv data to Customer, Account and PhoneAccount fixtures to fixture and db.."""
    csvFilePath = CSV_DIR_TEMP.format('customer.csv')
    output = []
    #customerId,firstName,lastName,email,mobileMoneyNumber,deviceId
    customerFields = ['pk', 'customerId', 'firstName', 'lastName', 'email', 'phoneNumber', 'circuit', 'circuitDeviceId',
                      'account', 'tariff']
    accountField = ['pk', 'accountBalance']
    phoneAccountFields = ['pk', 'account', 'firstName', 'lastName', 'mobileMoneyNumber', 'networkProvider']
    with open(csvFilePath) as csvfile:
            csvReader = csv.DictReader(csvfile)
            for line in csvReader:
                customerRow = common_loader.construct_fixture_object('ph_model.customer', 'pk', customerFields, line, mapping={'account': 'pk'})
                output.append(customerRow)
                accountRow = common_loader.construct_fixture_object('ph_model.Account', 'pk', accountField, line)
                output.append(accountRow)
                phoneAccount = common_loader.construct_fixture_object('ph_model.PhoneAccount', 'pk', phoneAccountFields, line, mapping={'account': 'pk'})
                output.append(phoneAccount)
    with open(JSON_DIR_TEMP.format('customer.json'), 'w') as jsonFile:
        json.dump(output, jsonFile, indent=2)
    print 'Populate device data......................................................................'
    os.system('./manage.py loaddata ph_model/fixtures/legacy_db/json/customer.json')


def load_usage(fileName):
    """Loads customer or non customer circuit usage directly to db."""
    common_loader.print_time()
    csvFilePath = USAGE_DIR_TEMP.format(fileName)
    issueCount = 0
    total = 0

    with open(csvFilePath) as csvfile:
        csvReader = csv.DictReader(csvfile)
        for line in csvReader:
            total+= 1
            try:
                usageData = {'circuit_id': int(line['circuit']),'intervalWh': common_loader.to_deci(line['intervalWh']),
                 'processed': True, 'intervalVAmax': common_loader.to_deci(line['intervalVAmax']),
                 'collectTime': common_loader.to_utc(line['collectTime'])}
                if line['customer']:
                    usage = ph_model.usage.Usage(customer_id=int(line['customer']), **usageData)
                    usage.save()
                    usageChargeData = {'usage_id':usage.id, 'account_id': int(line['customer']),
                        'amount': common_loader.to_deci(line['usageCharge']),
                        'processedTime': common_loader.to_utc(line['collectTime']),
                        'accountBalanceBefore': common_loader.to_deci(line['accountBalanceBefore']),
                        'accountBalanceAfter': common_loader.to_deci(line['accountBalanceAfter'])}
                    usageCharge = ph_model.transaction.UsageCharge(**usageChargeData)
                    usageCharge.save()
                else:
                    nonCustomerUsage = ph_model.usage.Usage(**usageData)
                    nonCustomerUsage.processed=True
                    nonCustomerUsage.customer=None
                    nonCustomerUsage.save()
            except Exception:
                issueCount += 1


    LOGGER.warning("Issues found {} loading total {}".format(issueCount, total))
    common_loader.print_time()

def load_active_customers(fileName):
    csvFilePath = CSV_DIR_TEMP.format(fileName)
    with open(csvFilePath) as csvfile:
        csvReader = csv.DictReader(csvfile)
        for line in csvReader:
            customer = ph_model.customer.Customer.objects.get(customerId=line['customerId'])
            circuit = ph_model.circuit.Circuit.objects.get(deviceId=line['circuitDeviceId'])
            tariff = ph_model.tariff.Tariff.objects.get(tariffId=line['newTariffId'])
            customer.status = ph_model.customer.CustomerStatus.ACTIVE.value
            customer.circuit = circuit
            customer.tariff = tariff
            customer.save()


def copy_non_customer_usage_to_usage():
    nonCustomerUsages = ph_model.usage.NonCustomerUsage.objects.all()
    for ncu in nonCustomerUsages:
        try:
            usage = ph_model.usage.Usage(intervalWh=ncu.intervalWh, intervalVAmax=ncu.intervalVAmax,
              intervalVmin=ncu.intervalVmin, collectTime=ncu.collectTime, processed=True, circuit=ncu.circuit)
            usage.save()
            ncu.delete()
        except:
            pass

def load():
    """Load legacy db data."""
    common_loader.load()
    load_location_data()
    load_device_data()
    load_customer_data()
    common_loader.load_pseudo_powerstation()



