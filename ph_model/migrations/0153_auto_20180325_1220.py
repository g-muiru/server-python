# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-03-25 18:20
from __future__ import unicode_literals

from django.db import migrations, models

class Migration(migrations.Migration):

    dependencies = [
        ('ph_model', '0152_create_materialized_views'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='invertermonitor',
            name='commAttempts',
        ),
        migrations.RemoveField(
            model_name='invertermonitor',
            name='commErrors',
        ),
        migrations.AddField(
            model_name='invertermonitor',
            name='commAttempts',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='invertermonitor',
            name='commErrors',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
