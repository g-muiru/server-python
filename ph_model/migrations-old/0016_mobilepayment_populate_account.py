# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-12-07 22:03
from __future__ import unicode_literals

from django.db import migrations, models, transaction
import django.db.models.deletion

@transaction.atomic
def set_account(MobilePayment):
    for mp in MobilePayment.objects.all():
        mp.account = mp.phoneAccount.account
        mp.save()

@transaction.atomic
def set_phoneAccount(MobilePayment):
    for mp in MobilePayment.objects.all():
        mp.phoneAccount = mp.account.phoneaccount_set.first()
        mp.save()

def migrate_mobilepayment_phoneaccount_to_account_forward(apps,schema_editor):
    MobilePayment = apps.get_model('ph_model','MobilePayment')
    db_alias = schema_editor.connection.alias
    set_account(MobilePayment)

def migrate_mobilepayment_phoneaccount_to_account_reverse(apps,schema_editor):
    MobilePayment = apps.get_model('ph_model','MobilePayment')
    db_alias = schema_editor.connection.alias
    set_phoneAccount(MobilePayment)


class Migration(migrations.Migration):

    dependencies = [
        ('ph_model', '0015_mobilepayment_account'),
    ]

    operations = [
        migrations.RunPython(migrate_mobilepayment_phoneaccount_to_account_forward,migrate_mobilepayment_phoneaccount_to_account_reverse),
    ]
