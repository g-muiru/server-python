# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-09-12 23:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ph_model', '0004_auto_20160831_1616'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fulcrumform',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='fulcrumquestion',
            name='data_name',
            field=models.CharField(max_length=300),
        ),
        migrations.AlterField(
            model_name='fulcrumquestion',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='fulcrumquestion',
            name='label',
            field=models.CharField(max_length=300),
        ),
    ]
