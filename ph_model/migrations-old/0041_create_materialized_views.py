# -*- coding: utf-8 -*-
"""
Create the materialized views in the DB
"""
from __future__ import unicode_literals
from django.db import migrations
from django.db import connection
from ph_util.test_utils import get_sql_from_files


class Migration(migrations.Migration):
    """
    Executes all the sql in all the files in PATH.
    These are Postgres material view definitions
    """

    dependencies = [
        ('ph_model', '0040_grid_operational_date'),
    ]
    if connection.vendor == 'postgresql':
        sql = get_sql_from_files("/ph_script/sql_material_views")
        operations = [
           migrations.RunSQL(sql),
        ]
    else:
        operations = [
        ]
