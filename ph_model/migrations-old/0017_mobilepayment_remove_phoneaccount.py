# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-12-07 22:03
from __future__ import unicode_literals

from django.db import migrations, models, transaction
import django.db.models.deletion

class Migration(migrations.Migration):

    dependencies = [
        ('ph_model', '0016_mobilepayment_populate_account'),
    ]

    operations = [
        migrations.RemoveField(model_name='mobilepayment',name='phoneAccount')
    ]
