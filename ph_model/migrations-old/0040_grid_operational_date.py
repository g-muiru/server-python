# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-13 20:59
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ph_model', '0039_customer_days_no_balance'),
    ]

    operations = [
        migrations.AddField(
            model_name='grid',
            name='operational_date',
            field=models.DateField(default=datetime.date(2222, 2, 2)),
        ),
    ]
