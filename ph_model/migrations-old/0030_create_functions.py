# -*- coding: utf-8 -*-
"""
Create the functions in the DB
"""
from __future__ import unicode_literals
from django.db import migrations
from django.db import connection
from ph_util.test_utils import get_sql_from_files


class Migration(migrations.Migration):
    """
    Executes all the sql in all the files in PATH.
    These are Postgres function definitions
    """

    dependencies = [
        ('ph_model', '0029_auto_20170115_1523'),
    ]
    if connection.vendor == 'postgresql':
        sql = get_sql_from_files("/ph_script/sql")

        operations = [
            migrations.RunSQL(sql),
        ]
    else:
        operations = [
        ]
