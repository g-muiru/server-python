# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-23 20:55
from __future__ import unicode_literals

from django.db import migrations, models, connection
import django.db.models.deletion


class Migration(migrations.Migration):
    # add the 426 event type if it does not exist
    if connection.vendor == 'postgresql':
        sql = """INSERT INTO ph_model_eventtype (created, updated, "eventNumber", "itemType", severity, "notifyImmediate", "eventDescription")
                SELECT '2017-01-01','2017-01-01',426, 'CIRCUIT', 2, True, 'Priority Circuit Failure: Possible Power Loss To Critical Circuit'
                WHERE 426 NOT IN (SELECT "eventNumber" FROM ph_model_eventtype where "eventNumber" = 426);"""
    else:
        sql = """INSERT INTO ph_model_eventtype (created, updated, "eventNumber", "itemType", severity, "notifyImmediate", "eventDescription")
                        SELECT '2017-01-01','2017-01-01',426, 'CIRCUIT', 2, 0, 'Priority Circuit Failure: Possible Power Loss To Critical Circuit'
                        WHERE 426 NOT IN (SELECT "eventNumber" FROM ph_model_eventtype where "eventNumber" = 426);"""


    dependencies = [
        ('ph_model', '0044_auto_20170517_1019'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriorityCircuit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('circuitType', models.CharField(choices=[(b'Chicken Brooder', b'BROODER'), (b'Chicken Hatchery', b'HATCHERY'), (b'Medical', b'MEDICAL'), (b'Unknown', b'UNKNOWN')], default=b'Unknown', max_length=50)),
                ('notified', models.NullBooleanField(default=False)),
                ('circuit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ph_model.Circuit')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
