# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-01-30 06:03
"""Change Kuku Poa Id """
from __future__ import unicode_literals

from django.db import migrations, connection


class Migration(migrations.Migration):
    if connection.vendor == 'postgresql':
        sql = """ALTER TABLE ph_model_customer ALTER COLUMN kuku_poa_id TYPE integer USING (case when length(trim(kuku_poa_id)) = 0 
                 then null else kuku_poa_id end ::integer);"""
    else:
        sql = ";"
    dependencies = [
        ('ph_model', '0049_auto_20171023_1302'),
    ]

    operations = [
        migrations.RunSQL(sql),
    ]
