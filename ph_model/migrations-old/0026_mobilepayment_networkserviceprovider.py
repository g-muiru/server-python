# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-12-23 00:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ph_model', '0025_auto_20161222_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='mobilepayment',
            name='networkServiceProvider',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ph_model.NetworkServiceProvider'),
        ),
    ]
