# coding=utf-8
"""Hardware events reported by queen.

Models:
    EventType: Pre-defined event types.
    Event: Actual event reported.
    EventDetail: Event's detail/s
"""

import enum
from django.db import models

import base_model
import fields
from ph_util import mixins

# TODO(estifanos): Revistit if we need to implement if events define their expiration date.
DEFAULT_EVENT_EXPIRATION_HOUR = 48


class Severity(mixins.PairEnumMixin, enum.Enum):
    """Severity constants."""
    FINE = 0
    UNKNOWN = 1
    WARNING = 2
    CRITICAL = 3


class EventType(base_model.BaseModel):
    """
    Attributes:
        eventNumber: Event protocol.
        itemType: type events e.g for Grid event, Queen event..
        eventDescription: Description of an event.
        severity: Severity level.
        notifyImmediate: boolean to enable immediate notification.
    """
    ITEM_TYPE_CHOICES = (
        ('GRID', 'Grid'),
        ('QUEEN', 'Queen'),
        ('PROBE', 'Probe'),
        ('CIRCUIT', 'Circuit'),
        ('POWERSTATION', 'Powerstation'),
        ('INVERTER', 'Inverter'),
        ('BATTERYBANK', 'Battery bank'),
        ('NETWORK_SERVICE_PROVIDER', 'Network Service Provider'),
        ('SMS_SERVICE_PROVIDER', 'SMS Service Provider'),
        ('CUSTOMER', 'Customer')
    )
    eventNumber = models.IntegerField(unique=True)
    itemType = models.CharField(max_length=200, choices=ITEM_TYPE_CHOICES)
    eventDescription = models.TextField(null=True, blank=True)
    severity = models.IntegerField(choices=Severity.get_values_map())
    notifyImmediate = models.BooleanField(default=False)

    def __str__(self):
        return "{} - Event Number: {}".format(self.itemType, self.eventNumber)


class Event(base_model.BaseModel):
    """
    Attributes:
        eventType: event type.
        itemId: Hardware identifier, e.g for event type 'Grid' will be Grid's primary key.
        collectTime: time when event collected.
        expiredAfter: Expiration time in hours after created.
        isExpired: whether event is expired or not.
    """
    eventType = models.ForeignKey(EventType, related_name='events')
    itemId = models.IntegerField()
    collectTime = fields.DateTimeUTC()
    expiredAfter = models.IntegerField(default=DEFAULT_EVENT_EXPIRATION_HOUR)
    isExpired = models.BooleanField(default=False)

    def __str__(self):
        return "Id: {}".format(self.itemId)


class EventDetail(base_model.BaseModel):
    """
    Attributes:
         event: Event
         itemName: Event name, e.g. temperature.
         itemValue: Event value, e.g 100.
    """
    event = models.ForeignKey(Event, related_name='details')
    itemName = models.CharField(max_length=250)
    itemValue = models.TextField()

    def __str__(self):
        return "{} - {}".format(self.itemName, self.itemValue)


class EventTypeConst(mixins.PairEnumMixin, enum.Enum):
    """Event constants in the form of EVENT_MODEL_DESCRIPTION, Lower number indicates highest severity."""
    GRID_FAILURE = 100

    QUEEN_FAILURE = 200
    QUEEN_METERING_FAILURE = 205
    QUEEN_REBOOTED = 210
    QUEEN_FIRMWARE_CHECKSUM_MISMATCH = 211
    QUEEN_CIRCUIT_TEST_ENABLED = 220
    QUEEN_CIRCUIT_TEST_DISABLED = 221
    QUEEN_GRID_CHANGE = 222

    PROBE_FAILURE = 300

    CIRCUIT_FAILURE = 400
    CIRCUIT_TURNED_OFF = 410
    CIRCUIT_FAULT_DETECTED = 420
    CIRCUIT_METER_ISSUE_DETECTED = 421
    CIRCUIT_TEST_ENABLED = 422
    CIRCUIT_TEST_DISABLED = 423
    CUSTOMER_CONNECTED_TO_CIRCUIT = 424
    CUSTOMER_DISCONNECTED_FROM_CIRCUIT = 425
    PRIORITY_CIRCUIT_FAILURE = 426

    POWERSTATION_FAILURE = 500

    INVERTER_FAILURE = 600

    BATTERYBANK_FAILURE = 700

    NETWORK_SERVICE_FAILURE = 800  # Failure during payment sync process
    NETWORK_SERVICE_UNKNOWN_CUSTOMER_PAYMENT = 850  # Payment Service provider sync unknown customer
    NETWORK_SERVICE_DUPLICATE_TRANSACTION = 851  # service provider attempt to validate transaction twice

    SMS_SERVICE_FAILURE = 900  # Failure during sms service.
    SMS_FROM_UNREGISTERED_USER = 950
    SMS_BROADCAST_TOOL_SUCCESS = 970
    SMS_BROADCAST_TOOL_ERROR = 971

    CUSTOMER_OUT_OF_BALANCE = 1000
    CUSTOMER_ACCOUNT_LOAN_PAYMENT_INSUFFICIENT_BALANCE = 1001
    CUSTOMER_PAID_DEPOSIT = 1002
    NEW_CUSTOMER_CREATED = 1050
    CUSTOMER_TARIFF_CHANGE = 1060
    BONUS_POWER_CREDIT_FAILED = 1070

    FULCRUM_API_ACCESS_FAILURE = 1100
    FULCRUM_CORRUPTED_DATA = 1101
