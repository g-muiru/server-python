"""Scratch card system."""
from django.db import models
import enum
from ph_util import mixins
import base_model
import fields
import country
import company


class ScratchcardStatus(mixins.PairEnumMixin, enum.Enum):
        """Scratch card status"""
        VALID = 'VALID'
        USED = 'USED'
        IN_PROCESS= 'IN_PROCESS'


#TODO(estifanos) handle dateActivated
class Scratchcard(base_model.BaseModel):
    """
    Attributes:
        pin: Scratch card hidden pin number.
        controlNumber: Serial number to look up, may be visible.
        status: Scratch card status.
        manufacturer: Company creating the card.
        pinGeneratedBy: Company generating the pins.
        dateGenerated: date pin generated.
        batch: Batch id or group id to identify scratch card collectively.
        country: Country card is applicable to.
    """
    pin = models.CharField(max_length=200, unique=True)
    controlNumber = models.IntegerField()
    amount = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.00)
    status = models.CharField(max_length=50, default=ScratchcardStatus.IN_PROCESS.value,
                              choices=ScratchcardStatus.get_values_map())
    manufacturer = models.ForeignKey(company.Company, related_name='cards')
    pinGeneratedBy = models.ForeignKey(company.Company,  related_name='pins')
    country = models.ForeignKey(country.Country)


    def __str__(self):
        return "control number: {}".format(self.controlNumber)

