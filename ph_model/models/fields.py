import calendar
import datetime
import time
from django.utils import dateparse
from dateutil import tz
from django.db import models
from django.utils import timezone
from django.conf import settings


class DateTimeUTC(models.DateTimeField):
    """Creates a DB timestamp field that is TZ naive."""

    description = "Date (with time and no time zone)"

    def from_db_value(self, value, expression, connection, context):
        value = self.to_python(value)
        return value
    
    def to_python(self, value):

        if value is None:
            return value

        if isinstance(value, unicode) or isinstance(value, str):
            value = dateparse.parse_datetime(value) or dateparse.parse_date(value)
        if isinstance(value, datetime.datetime):
            if settings.USE_TZ and timezone.is_naive(value):
                return value.replace(tzinfo=tz.gettz('UTC'))
            return value
        return super(DateTimeUTC, self).to_python(value)

    def get_prep_value(self, value):
        if isinstance(value, datetime.datetime):
            if timezone.is_naive(value):
                return value.replace(tzinfo = tz.gettz('UTC'))
            return value.astimezone(tz.gettz('UTC'))
        return value

