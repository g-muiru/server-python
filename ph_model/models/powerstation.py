"""Powerstation model."""

from django.db import models

import base_device_model
import grid
import queen

# TODO(estifanos) is powerstation name unique?, can we use identifier on this? something like deviceId
# TODO(estifanos) remove name.
class PowerStation(base_device_model.DeviceModel):
    """
    Attributes:
        name: name.
        latitude: physical location of powerstation, latitude
        longitude: physical location of powerstation, longitude
        grid: Grid powerstation belongs to.
        queen: Especial queen attached to the powerstation.
        battery: Powerstation battery.
    """
    name = models.CharField(max_length=200)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    grid = models.OneToOneField(grid.Grid, related_name='powerstation')
    queen = models.OneToOneField(queen.Queen, null=True, blank=True)

    @property
    def get_name(self):
        return str('P{}:{}'.format(self.name, self.grid.name))

    def __str__(self):
        return str('P{}:{}'.format(self.name, self.grid.name))

