"""Country model."""
from django.db import models
import base_model


class Country(base_model.BaseModel):
    """
    Attribute:
        name: Country name.
        countryCode: CountryCode
    """
    name = models.CharField(max_length=200, unique=True)
    countryCode = models.CharField(max_length=10)
    countryIsoCode = models.CharField(max_length=5, null=True, blank=True)
    countryITUCode = models.CharField(max_length=5, null=True, blank=True)

    def __str__(self):
        return self.name

