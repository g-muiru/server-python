"""Project model."""
from django.db import models

import base_model


class Project(base_model.BaseModel):
    """
    Attribute:
        name: Project's name
    """
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

