"""Company model."""
from django.db import models
import base_model


class Company(base_model.BaseModel):
    """
    Attributes:
        name: Registered name of the company.
        poc: Point of contact.
        website: website of the company.
    """
    name = models.CharField(max_length=200)

    def __str__(self):
        return "Name: {}".format(self.name)
