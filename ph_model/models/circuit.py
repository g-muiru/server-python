# coding=utf-8
"""Circuit associated with the customer."""
import decimal
from django.db import models
import enum
import base_model
import base_device_model
from ph_util import mixins
import probe as probe_model
import queen as queen_model


class SwitchEnabledStatus(mixins.PairEnumMixin, enum.Enum):
    """circuit switch enabled status."""
    UNKNOWN = 'UNKNOWN'
    ENABLED = 'ENABLED'
    DISABLED = 'DISABLED'
    DISCONNECTED = 'DISCONNECTED'


class SwitchEnabledChangeReason(mixins.PairEnumMixin, enum.Enum):
    """circuit switch enabled status."""
    OVERRIDE_SWITCH_ENABLED = 'OVERRIDE_SWITCH_ENABLED'
    INSUFFICIENT_BALANCE = 'INSUFFICIENT_BALANCE'
    SUFFICIENT_BALANCE = 'SUFFICIENT_BALANCE'
    EXCEEDED_VA_LIMIT = 'EXCEEDED_VA_LIMIT'
    TIME_OUT_RESET = 'TIME_OUT_RESET'
    REQUESTED_BY_SERVER = 'REQUESTED_BY_SERVER'
    NO_CHANGE = 'NO_CHANGE'
    UNKNOWN = 'UNKNOWN'

class PriorityCircuitType(mixins.PairEnumMixin, enum.Enum):
    """circuit switch enabled status."""
    UNKNOWN = 'Unknown'
    HATCHERY = 'Chicken Hatchery'
    BROODER = 'Chicken Brooder'
    MEDICAL = 'Medical'


class Circuit(base_device_model.DeviceModel):
    """
    Attributes:
    number: circuit positon number in the whole queen.
    deviceId: Circuit identifier in the form of queen_number
    probe: Probe holding the circuit.
    queen: queen holding the probe.
    whLimit: max energy circuit can support derived from tariff.
    vwMax: max power circuit can support derived from tariff.
    switchEnabled: actual circuit status set by the queen.
    disableBalanceServer: Disable request set by the server.
    disableVaLimitQueen: queen's reason for disabling circuit, due to va limit.
    overrideSwitchEnabled: Override switch enabled e.g when performing maintenance in queen.
    """
    number = models.IntegerField(default=0)
    probe = models.ForeignKey(probe_model.Probe, related_name="circuits", null=True, blank=True)
    queen = models.ForeignKey(queen_model.Queen, related_name='queen_circuits')
    deviceId = models.CharField(max_length=50, unique=True)
    whLimit = models.DecimalField(max_digits=base_device_model.MAX_DIGITS, decimal_places=base_device_model.DECI_PLACES,
                                  default=decimal.Decimal('0.00'))
    vaLimit = models.DecimalField(max_digits=base_device_model.MAX_DIGITS, decimal_places=base_device_model.DECI_PLACES,
                                  default=decimal.Decimal('0.00'))
    switchEnabled = models.CharField(max_length=50, default=SwitchEnabledStatus.UNKNOWN.value, choices=SwitchEnabledStatus.get_values_map())

    # TODO(estifanos): Change to enableServerBalance
    disableVaLimitQueen = models.NullBooleanField(null=True, blank=True)
    enableBalanceServer = models.NullBooleanField(null=True, blank=True)
    overrideSwitchEnabled = models.NullBooleanField(null=True, blank=True)

    faultCount = models.IntegerField(default=0)
    meterIssueCount = models.IntegerField(default=0)

    def __str__(self):
        return 'deviceId: {}'.format(self.deviceId)

    def get_customer_or_none(self):
        """Wrap in exception handler"""
        # noinspection PyBroadException
        try:
            return self.circuitOwner
        except Exception:
            return None

    @property
    def get_name(self):
        """number to string"""
        return str(self.number)

    @property
    def server_request_enabled(self):
        """Switch enabled  request of server at any point."""
        if self.overrideSwitchEnabled:
            return True
        customer = self.get_customer_or_none()
        if not customer:
            return False
        return self.enableBalanceServer


class SwitchEnabledHistory(base_model.BaseModel):
    """
    Switch enabled change or change request history.

    Attributes:
    circuit: Relevant circuit.
    switchEnabled: actual circuit status set by the queen.
    enableBalanceServer: Disable request set by the server.
    disableVaLimitQueen: queen's reason for disabling circuit, due to va limit.
    disableVaLimitQueen: queen's reason for disabling circuit, due to va limit.
    reason: Reason of changes in switch.
    accountBalance: Optional account balance at the time of snapshot for circuit with non customer it will be null
    serverRequestEnabled: Server request enable value at any point if queen asked for configuration request.
    """
    circuit = models.ForeignKey(Circuit)
    switchEnabled = models.CharField(max_length=50, default=SwitchEnabledStatus.UNKNOWN.value, choices=SwitchEnabledStatus.get_values_map())
    enableBalanceServer = models.NullBooleanField(null=True, blank=True)
    disableVaLimitQueen = models.NullBooleanField(null=True, blank=True)
    overrideSwitchEnabled = models.NullBooleanField(null=True, blank=True)
    reason = models.CharField(max_length=50, choices=SwitchEnabledChangeReason.get_values_map())
    accountBalance = models.DecimalField(max_digits=50, decimal_places=3, null=True, blank=True)
    serverRequestEnabled = models.NullBooleanField(null=True, blank=True)

class PriorityCircuit(base_model.BaseModel):
    """
    high-priority customer circuits that may need more urgent monitoring and notification, etc

    Attributes:
    circuit: Relevant circuit.
    circuitType: priority circuit type
    notified: has customer already been notified about the current outage?
    """
    circuit = models.ForeignKey(Circuit)
    circuitType = models.CharField(max_length=50, default=PriorityCircuitType.UNKNOWN.value, choices=PriorityCircuitType.get_values_map())
    notified = models.NullBooleanField(default=False)

