# coding=utf-8
"""Transaction model."""
import decimal
import logging
import traceback
import enum
from delorean import utcnow
from django.db import models, connection, DatabaseError
import account
import customer
import base_model
import fields
import usage
import scratchcard
# noinspection PyCompatibility
import user
import mobile_payment_provider
from ph_operation import circuit_manager
from ph_util import mixins
from django.dispatch import receiver
from django.db.models import signals

from ph_util.accounting_util import AccountingEnums, calc_vat, PaymentEnums, deci_or_float

logger = logging.getLogger(__name__)


class TransactionType(mixins.PairEnumMixin, enum.Enum):
    """Transaction types."""
    CREDIT = 'Credit'
    DEBIT = 'Debit'


class TransactionSource(mixins.PairEnumMixin, enum.Enum):
    """Transaction source."""
    MOBILE_PAYMENT = 'MobilePayment'
    SCRATCHCARD_PAYMENT = 'ScratchcardPaymentHistory'
    CREDIT_ADJUSTMENT = 'CreditAdjustmentHistory'
    USAGE_CHARGE = 'UsageCharge'
    LOAN_PAYMENT = 'LoanPaymentHistory'


class _BaseTransactionHistory(base_model.BaseModel):
    """Base for money transactions processed, supported transactions are = (
    ('Mobile payment, 'Customer added money/ Credit'),
    ('usage charges', 'Customer got charged/ Debit')
    )

    Attributes:
    amount: transaction amount.
    processedTime: Transaction processed time.
    accountBalanceBefore: Account balance before transaction happened.
    accountBalanceAfter: Account balance after transaction happened.
    """
    amount = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.00)
    processedTime = fields.DateTimeUTC()
    accountBalanceBefore = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                               default=decimal.Decimal('0.00'))
    accountBalanceAfter = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                              default=decimal.Decimal('0.00'))

    def get_account(self):
        """

        :return:
        """
        return self.account

    def get_transaction_type(self):
        """

        :return:
        """
        pass

    # noinspection PyMissingOrEmptyDocstring
    class Meta:
        abstract = True


class MobilePayment(_BaseTransactionHistory):
    """ Mobile payment money transaction.

    Attributes:
    transactionId: Transaction id.
    phoneAccount: Phone account crediting to account.
    """
    transactionId = models.CharField(max_length=200)
    account = models.ForeignKey(account.Account)
    payment_type = models.CharField(max_length=20, choices=PaymentEnums.get_values_map(), default=PaymentEnums.ACCOUNT.name)
    networkServiceProvider = models.ForeignKey(mobile_payment_provider.NetworkServiceProvider, null=True, blank=True,
                                               on_delete=models.SET_NULL)

    def get_account(self):
        """

        :return:
        """
        return self.account

    def get_transaction_type(self):
        """

        :return:
        """
        return TransactionType.CREDIT.value

    # noinspection PyMissingOrEmptyDocstring
    class Meta:
        unique_together = (("transactionId", "account"),)

    def __str__(self):
        return "Account {}, amount paid {} ".format(self.account, self.amount)


class ScratchcardPaymentHistory(_BaseTransactionHistory):
    """ Scratch card payment history.

    Attributes:
    scratchcard: scratch card used to make payment.
    phoneAccount: Phone account crediting to account.
    account: Account being processed
    """
    # smh Got tired of seeing 'OneToOneField' warning on startup
    # scratchcard = models.ForeignKey(scratchcard.Scratchcard, unique=True)
    scratchcard = models.OneToOneField(scratchcard.Scratchcard, related_name='scratchcard_id')
    account = models.ForeignKey(account.Account)

    def get_transaction_type(self):
        """

        :return:
        """
        return TransactionType.CREDIT.value

    def __str__(self):
        return "Phone account {}, scratchcard {} amount paid {} ".format(self.account, self.scratchcard, self.amount)


class UsageCharge(_BaseTransactionHistory):
    """customer usage money transaction.

    Attributes:
    syncToMasterTransaction: Boolean to indicate if transaction history is synced to master transaction.
    When doing bulk_create it needs to
    account: Account to deduct usage from.
    usage: Usage.
    perSegmentChargeComponent, tariffSegment charge component.
    notcollected: True means the amount billed was not collected (insufficient funds)
    """
    account = models.ForeignKey(account.Account)
    perSegmentChargeComponent = models.DecimalField(
        max_digits=base_model.MAX_DIGITS,
        decimal_places=base_model.DECI_PLACES, default=0.00)
    usage = models.OneToOneField(usage.Usage, related_name='charge')
    syncToMasterTransaction = models.BooleanField(default=False)
    uncollected_before = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    uncollected_after = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    notcollected = models.BooleanField(default=False)

    def get_transaction_type(self):
        """

        :return:
        """
        return TransactionType.DEBIT.value

    def __str__(self):
        return "Usage {} ".format(self.usage)


class CreditAdjustmentHistory(_BaseTransactionHistory):
    """customer credit adjustment.

    Attributes:
    account: Account to deduct usage from.
    user: ph_model.user making the adjustment.
    reason: Reason for adjustment.
    transactionType: Adjustment type when adjusting credit.
    adj_type: NetSuite accounting adjustment type
    """
    account = models.ForeignKey(account.Account)
    user = models.ForeignKey(user.User)
    reason = models.TextField(null=True, blank=True)
    transactionType = models.CharField(max_length=100, choices=TransactionType.get_values_map())
    adj_type = models.TextField(null=True, blank=True)

    def get_transaction_type(self):
        """

        :return:
        """
        return self.transactionType

    def __str__(self):
        return "User {} ".format(self.user)


def save_to_master_transaction(transaction_obj, source):
    """

    :param transaction_obj:     the transaction object
    :param source:              source of transaction
    """

    account_local = transaction_obj.get_account()
    # set mobile payment process time to now
    if source == TransactionSource.MOBILE_PAYMENT.value:
        processed_time = transaction_obj.created
    else:
        processed_time = transaction_obj.processedTime
    amount = transaction_obj.amount
    transaction_type = transaction_obj.get_transaction_type()
    source = source
    source_id = transaction_obj.id
    account_balance = account_local.accountBalance

    mth = MasterTransactionHistory(
        amount=abs(decimal.Decimal(amount)), account=account_local, transactionType=transaction_type, source=source,
        sourceId=source_id, accountBalance=account_balance, sourceProcessedTime=processed_time)
    mth.save()


# noinspection PyUnusedLocal,PyUnusedLocal
def update_mth_balances(id_array=None, trans_diff=0.001, project='Cloverfield', row_limit=1000, bal_diff=0.001):
    """
    Adjusts balances based on transactions just processed, or an array of id's

    :param id_array:    Array of account IDs
    :param trans_diff:  precision to filter trans_diff query to
    :param project:     the project name
    :param row_limit:   row limit for trans_diff query
    :param bal_diff:    precision to filter account balance diff to
    :return:
    """
    st = utcnow().epoch
    sql_ids = """
    select distinct mth.account_id
       from ph_model_mastertransactionhistory mth
       , ph_model_customer c
       , ph_model_municipality m
      where  c.account_id = mth.account_id and m.id = c.municipality_id and mth.processed = false
    """

    sql_select = """
          select  mth.id,mth.account_id,start_bal,start_uncol,
            case when "source" like '%%INIT%%' then 0 else
              case when "transactionType" = 'Debit'  then -mth.amount else mth.amount end
            end amt,
            case when mth."source" = 'MobilePayment' then mp.payment_type end pt,
            start_clearing,start_reconnect, 
            case when mth."source" ='MobilePayment' then (select "transactionId" from ph_model_mobilepayment where id = mth."sourceId") end trans_id, 
            mth.processed, proc
          from ph_model_mastertransactionhistory mth
          left join (select account_id acc_id, "accountBalance" start_bal, uncollected_bal start_uncol, 
              clearing_bal start_clearing, reconnect_bal start_reconnect, processed as proc from ph_model_mastertransactionhistory
              where account_id = %s and processed = true order by "sourceProcessedTime" desc, id desc limit 1) as st
              on st.acc_id = mth.account_id
          left join ph_model_mobilepayment as mp on mp.id = mth."sourceId"
          where  mth.processed = false and mth.account_id = %s
          order by mth.account_id, mth."sourceProcessedTime", mth.id
    """

    sql_mth = """
        update ph_model_mastertransactionhistory  mth
                set "accountBalance" = data.bal, uncollected_bal = data.uncol, clearing_bal = data.clearing, 
                reconnect_bal = data.recon, processed = true
        FROM (
          VALUES
            %s
        ) AS data(id, bal, uncol, clearing, recon)
        WHERE mth.id = data.id;
    """

    # TODO we may need to do this for mobile payments, credit adjustments, and loan payments --- I will look at the data
    # for now not used
    sql_uc = """
        update ph_model_usagecharge  uc
                set "accountBalanceBefore" = data.bal_before, "accountBalanceAfter" = data.bal_after, 
                uncollected_before = data.uncol_before, uncollected_after = data.uncol_after
        FROM (
          VALUES
            %s
        ) AS data(id, bal_before, uncol_before, bal_after, uncol_after)
        WHERE uc.id = data.id;
    """

    err = cnt = msg = sms = None
    try:
        if id_array is None:
            err, cnt, msg, sms = run_db_update(sql_ids, sql_select, sql_mth, pre_msg='update mth\n')
        else:
            err, cnt, msg, sms = run_db_update(id_array, sql_select, sql_mth, pre_msg='update mth\n')
        if err is not None:
            logger.error('update_mth_balances update mth ERROR: %s \nelapsed: %s seconds' % (msg, str(utcnow().epoch - st)))
        if cnt < 1:
            logger.info('Nothing updated, skipping further updates \nelapsed: %s seconds' % str(utcnow().epoch - st))
        logger.info('update_mth_balances update: Updated %s records in %s seconds' % (str(cnt), str(utcnow().epoch - st)))
    except Exception as e:
        print(e)
        traceback.print_exc()
        logger.error('update_mth_balances ERROR: %s \n elapsed: %s seconds' % (e.message, str(utcnow().epoch - st)))
    return err, cnt, msg, sms


def cust_type_to_accounting_code(cust, variable=False):
    """
    Set the accounting source for the mth record

    :param cust:        customer object
    :param variable:    true is variable charge, not fixed
    :return:
    """

    if cust is None:
        return TransactionSource.USAGE_CHARGE.name
    if cust.customerType == customer.CustomerType.RESIDENTIAL.name:
        if variable:
            return AccountingEnums.RES_VAR_FEEc238.name
        else:
            return AccountingEnums.RES_FIXED_FEEc236.name
    if cust.customerType == customer.CustomerType.COMMERCIAL.name:
        if variable:
            return AccountingEnums.COM1_VAR_FEEc233.name
        else:
            return AccountingEnums.COM1_FIXED_FEEc231.name
    if cust.customerType == customer.CustomerType.COMMERCIAL_II.name:
        if variable:
            return AccountingEnums.COM2_VAR_FEEc234.name
        else:
            return AccountingEnums.COM2_FIXED_FEEc232.name

    return TransactionSource.USAGE_CHARGE.name


def mth_save(mth, test=False):
    """
    Ok to fail save as we may be processing old data
    :param mth: MasterTransactionHistory object
    :param test:    True for debug
    :return:
    """
    # noinspection PyBroadException
    try:
        if MasterTransactionHistory.objects.get_or_none(source=mth.source, sourceId=mth.sourceId) is None:
            mth.save()
    except Exception as e:
        if test:
            print("mth_save: ", e.message)


def save_usage_charge_to_master_transaction_accounting(uc_obj, test=False):
    """
    Usage charges need to add VAT
    :param uc_obj:  The usage charge
    :param test:    True for debug
    """
    account_local = uc_obj.get_account()
    cust = customer.Customer.objects.filter(account_id=uc_obj.account_id)[0]
    processed_time = uc_obj.processedTime
    amount = decimal.Decimal(uc_obj.amount)
    transaction_type = uc_obj.get_transaction_type()
    pcc = decimal.Decimal(uc_obj.perSegmentChargeComponent)
    variable_charge = amount
    if pcc != 0.0 and amount >= pcc:
        variable_charge = amount - pcc
    source = cust_type_to_accounting_code(cust, variable=variable_charge != 0.0)
    source_id = uc_obj.id
    account_balance = account_local.accountBalance

    if test:
        print(account_local.id, account_local.accountBalance)
    if variable_charge != 0.0:
        mth = MasterTransactionHistory(
            amount=abs(variable_charge), account=account_local, transactionType=transaction_type, source=source,
            sourceId=source_id, accountBalance=account_balance, sourceProcessedTime=processed_time)
        mth_save(mth, test)

        source = AccountingEnums.VAT_TARIFF_VARIABLEc345.name
        vat_variable = calc_vat(variable_charge)
        mth = MasterTransactionHistory(
            amount=abs(vat_variable), account=account_local, transactionType=transaction_type, source=source,
            sourceId=source_id, accountBalance=account_balance, sourceProcessedTime=processed_time)
        mth_save(mth, test)

    if 0 < pcc <= amount:
        source = cust_type_to_accounting_code(cust)
        mth = MasterTransactionHistory(
            amount=abs(pcc), account=account_local, transactionType=transaction_type, source=source,
            sourceId=source_id, accountBalance=account_balance, sourceProcessedTime=processed_time)
        mth_save(mth, test)
        source = AccountingEnums.VAT_TARIFF_FIXEDc346.name
        vat_pcc = calc_vat(pcc)
        mth = MasterTransactionHistory(
            amount=abs(vat_pcc), account=account_local, transactionType=transaction_type, source=source,
            sourceId=source_id, accountBalance=account_balance, sourceProcessedTime=processed_time)
        mth_save(mth, test)


@receiver(signals.post_save, sender=MobilePayment)
@receiver(signals.post_save, sender=ScratchcardPaymentHistory)
def money_transactions_post_save(sender, **kwargs):
    """

    :param sender:
    :param kwargs:
    :return:
    """
    if not kwargs['created']:
        return
    save_to_master_transaction(kwargs['instance'], sender.__name__)


@receiver(signals.post_save, sender=CreditAdjustmentHistory)
def adjustment_transactions_post_save(sender, **kwargs):
    """

    :param sender:
    :param kwargs:
    :return:
    """
    if not kwargs['created']:
        return
    obj = kwargs['instance']
    source = sender.__name__
    if obj.adj_type:
        source += '-' + obj.adj_type
    save_to_master_transaction(kwargs['instance'], source)


# noinspection PyUnusedLocal
@receiver(signals.post_save, sender=UsageCharge)
def money_usage_transactions_post_save(sender, **kwargs):
    """
    Special handling for usage charges, need to add a VAT usage charge
    :param sender:
    :param kwargs:
    :return:
    """
    if not kwargs['created']:
        return
    save_usage_charge_to_master_transaction_accounting(kwargs['instance'])


# noinspection PyUnusedLocal
@receiver(signals.pre_save, sender=MobilePayment)
@receiver(signals.pre_save, sender=ScratchcardPaymentHistory)
@receiver(signals.pre_save, sender=CreditAdjustmentHistory)
@receiver(signals.pre_save, sender=UsageCharge)
def money_transactions_pre_save(sender, **kwargs):
    """

    :param sender:
    :param kwargs:
    """
    obj = kwargs['instance']
    obj.syncToMasterTransaction = True


class MasterTransactionHistory(base_model.BaseModel):
    """Master transaction history model

    Attributes:
    amount: transaction amount.
    sourceProcessedTime: Transaction processed time.
    account: Account to deduct usage from.
    transactionType: enum to indicate credit vs debit.
    source: Model source where the transaction is initiated.
    sourceId: pk/id of the @param source.
    accountBalance: current balance
    uncollected_bal: uncollected amounts
    """
    amount = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.00)
    account = models.ForeignKey(account.Account)
    transactionType = models.CharField(max_length=100, choices=TransactionType.get_values_map())
    source = models.CharField(max_length=100, choices=TransactionSource.get_values_map())
    sourceId = models.BigIntegerField()
    sourceProcessedTime = fields.DateTimeUTC()
    accountBalance = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    uncollected_bal = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    clearing_bal = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    reconnect_bal = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    processed = models.NullBooleanField(null=True, blank=True, default=None)

    # noinspection PyMissingOrEmptyDocstring
    class Meta:
        unique_together = (("source", "sourceId"),)

    def __str__(self):
        return "Account {}: {} - {} ( {} ) ".format(
            self.account, self.source, self.transactionType, self.accountBalance)


# noinspection PyUnusedLocal
@receiver(signals.pre_save, sender=MasterTransactionHistory)
def money_transactions_pre_save(sender, **kwargs):
    """

    :param sender:
    :param kwargs:
    """
    obj = kwargs['instance']
    if obj.processed is None:
        obj.processed = False


# noinspection LongLine,PyUnusedLocal
def run_db_update(sql_or_id_array, sql_select, sql_mth, conn=connection, pre_msg='', log=logger, debug=False):
    """
    Executes a DB update SQL statement
    :param sql_or_id_array: SQL to execute --- not validated, or array of account id's
    :param sql_select:      SQL to execute --- not validated
    :param sql_mth:         SQL to execute --- not validated
    :param conn:            Connection to use
    :param pre_msg:         string to prepend to messages
    :param log:             logger to use
    :param debug:           if true print to console
    :return:                msg
    """
    sms = []
    err = None
    st = utcnow().epoch
    mth_str = ''
    msg = pre_msg + 'Start time UTC: %s\n' % utcnow().format_datetime()
    ids = []
    ttl_cnt = 0
    with conn.cursor() as cursor:
        # noinspection PyPep8,PyCompatibility
        if isinstance(sql_or_id_array, basestring):
            try:
                if debug:
                    msg += ('SQL: "%s"' % sql_or_id_array).replace('\n', ' ')
                    print(msg)
                cursor.execute(sql_or_id_array)
                s = utcnow().epoch - st
            except DatabaseError as e:
                s = utcnow().epoch - st
                err = e
                msg += '\nDatabaseError %s \nafter %s seconds' % (str(e), s)
                log.error(msg, exc_info=1)
            if err is None:
                r = cursor.fetchone()
                while r is not None:
                    ids.append(r[0])
                    r = cursor.fetchone()
                ttl_cnt += len(ids)
                msg += "\nFound  %s  rows in %s seconds\n" % (cursor.rowcount, str(s))
        else:
            ids = sql_or_id_array
            s = utcnow().epoch - st
            msg += "\nFound  %s  rows in %s seconds\n" % (cursor.rowcount, str(s))
        if err is None:
            for acc in ids:
                keys = []
                mth_str = ''
                try:
                    cursor.execute(sql_select % (acc, acc))
                    s = utcnow().epoch - st
                except DatabaseError as e:
                    s = utcnow().epoch - st
                    err = e
                    msg += '\nDatabaseError %s \nafter %s seconds' % (str(e), s)
                    log.error(msg, exc_info=1)
                if err is None:
                    ttl_cnt += cursor.rowcount
                    msg += "\nUpdated  %s  rows in %s seconds\n" % (cursor.rowcount, str(s))

                    if debug:
                        log.info(msg)
                    r = cursor.fetchone()
                    idx = 0
                    last_uncol = 0
                    last_bal = 0
                    last_clearing = 0
                    last_recon = 0

                    # noinspection LongLine
                    while r is not None:
                        """
                                 Acc      bal     uncoll    amt      pt       clearing  reconnect  trans_id processed    source        s_id
                        (id,   41707,  -242.068,  -10.0   2474.07, 'ACCOUNT',  -20.0      -30.0    '123xyz'   True    'MobilePayment'  39392
                          0       1        2        3        4        5          6          7         8         9          10            11
                        """
                        if debug:
                            print('rrrrrrrr', r)
                        if idx == 0:
                            if r[3] is not None:
                                last_uncol = r[3]
                            if r[2] is not None:
                                last_bal = r[2]
                            if r[6] is not None:
                                last_clearing = r[6]
                            if r[7] is not None:
                                last_recon = r[7]
                            acc_id = r[1]
                        idx += 1

                        if idx % 250 == 0:
                            print('run_db_update ', idx)
                        accountBalance = last_bal
                        uncollected_bal = last_uncol
                        clearing_bal = last_clearing
                        recon_bal = last_recon
                        pt = r[5]
                        if r[4] > 0:
                            if debug:
                                print('R[4]', idx, 'credit', last_uncol, r[4], r[4] > abs(last_uncol), last_bal, last_uncol,
                                      last_clearing, last_recon, r[5])
                            if pt == PaymentEnums.ACCOUNT.name:
                                accountBalance = last_bal + r[4]
                            elif pt == PaymentEnums.UNCOLLECTED.name:
                                if last_uncol < 0:
                                    if abs(last_uncol) < r[4]:
                                        uncollected_bal = 0
                                        accountBalance = last_bal + r[4] + last_uncol
                                    else:
                                        uncollected_bal = last_uncol + r[4]
                                elif last_clearing < 0:
                                    if abs(last_clearing) < r[4]:
                                        clearing_bal = 0
                                        accountBalance = last_bal + r[4] + last_clearing
                                    else:
                                        clearing_bal = last_clearing + r[4]
                                else:
                                    accountBalance = last_bal + r[4]
                            elif pt == PaymentEnums.CLEARING.name:
                                if last_clearing < 0:
                                    if abs(last_clearing) < r[4]:
                                        clearing_bal = 0
                                        accountBalance = last_bal + r[4] + last_clearing
                                    else:
                                        clearing_bal = last_clearing + r[4]
                                elif last_uncol < 0:
                                    if abs(last_uncol) < r[4]:
                                        uncollected_bal = 0
                                        accountBalance = last_bal + r[4] + last_uncol
                                    else:
                                        uncollected_bal = last_uncol + r[4]
                                else:
                                    accountBalance = last_bal + r[4]
                            elif pt == PaymentEnums.RECONNECT.name:
                                if last_recon < 0:
                                    if abs(last_recon) < r[4]:
                                        recon_bal = 0
                                        accountBalance = last_bal + r[4] + last_recon
                                    else:
                                        recon_bal = last_recon + r[4]
                                elif last_uncol < 0:
                                    if abs(last_uncol) < r[4]:
                                        uncollected_bal = 0
                                        accountBalance = last_bal + r[4] + last_uncol
                                    else:
                                        uncollected_bal = last_uncol + r[4]
                                elif last_clearing < 0:
                                    if abs(last_clearing) < r[4]:
                                        clearing_bal = 0
                                        accountBalance = last_bal + r[4] + last_clearing
                                    else:
                                        clearing_bal = last_clearing + r[4]
                                else:
                                    accountBalance = last_bal + r[4]
                            else:  # this should not happen
                                pt = PaymentEnums.ACCOUNT.name
                                accountBalance = last_bal + r[4]
                        else:
                            if debug:
                                print(idx, 'debit', last_bal, r[4], last_bal < abs(r[4]), last_uncol)
                            if last_bal < abs(r[4]):
                                uncollected_bal = last_uncol + r[4]
                            else:
                                accountBalance = last_bal + r[4]

                        # noinspection PyUnboundLocalVariable
                        last_bal = accountBalance
                        last_uncol = uncollected_bal
                        last_clearing = clearing_bal
                        last_recon = recon_bal
                        mth_str += '(' + str(r[0]) + ',' + str(last_bal) + ',' + str(last_uncol) + \
                                     ',' + str(last_clearing) + ',' + str(last_recon) + '),'
                        keys.append(r[0])
                        if r[9] is False and pt is not None:
                            # we have accounts with no customer TODO: fix the DB
                            cust = customer.Customer.objects.get_or_none(id=acc)
                            if cust is not None:
                                if cust.circuit_id is not None:
                                    circuit_manager.CircuitManager.configure_circuit_after_payment(cust)

                                msg = '%s Acc Balance: %s Transaction %s for %s has been credited' % \
                                      (cust.firstName,
                                       str(int(round(deci_or_float(last_bal)))),
                                       r[8], str(int(round(deci_or_float(r[4])))))
                                sms.append((cust.id, msg))

                        if debug:
                            print(idx, last_bal, last_uncol)

                        r = cursor.fetchone()

                    if len(keys) > 0:
                        try:
                            if mth_str != '':
                                mth_str = mth_str[:-1]
                                cursor.execute(sql_mth % mth_str)
                            s = utcnow().epoch - st
                            myacc = account.Account.objects.get_or_none(id=acc)
                            if myacc is not None:
                                myacc.accountBalance = last_bal
                                myacc.uncollected_bal = last_uncol
                                myacc.clearing_bal = last_clearing
                                myacc.reconnect_bal = last_recon
                                myacc.save()
                        except DatabaseError as e:
                            print('error', e)
                            s = utcnow().epoch - st
                            err = e
                            msg += '\nDatabaseError %s \nafter %s seconds' % (str(e), s)
                            log.error(msg, exc_info=1)
        return err, ttl_cnt, msg, sms
