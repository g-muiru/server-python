"""Municipality model."""
from django.db import models

import base_model
import region


class Municipality(base_model.BaseModel):
    """
    Attributes:
        name: Name.
        region: Municipality region.

    """
    name = models.CharField(max_length=200)
    region = models.ForeignKey(region.Region)
    timeZone = models.CharField(max_length=200)
    connectionFeeDueDate = models.DateTimeField(null=True,blank=True)

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        ordering = ['name',]

