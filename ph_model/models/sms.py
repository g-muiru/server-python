"""SMS messaging model.

Models:
SMS: SMS message between powerhive account and customers.
SMSServiceProvider: sms service providers. e.g Africa's talking.
SMSSyncHistory: sms sync history, used to persist cron based sync history from service providers.
"""
import enum
from django.db import models
from django.utils import  timezone

from ph_util import mixins
import base_model
import fields
from phonenumber_field import modelfields as phone_model_field
import customer


class ServiceProviders(mixins.PairEnumMixin, enum.Enum):
        """sms service provider type"""
        AFRICAS_TAKING = 'AFRICAS_TAKING'



class SMSType(mixins.PairEnumMixin, enum.Enum):
    """SMS message type."""
    BALANCE_INQUIRY = "Balance Inquiry"
    INFORMATION = "Information"
    SCRATCH_CARD_PAYMENT = 'Scratchcard Payment'


class SMSFlow(mixins.PairEnumMixin, enum.Enum):
    """SMS flow type, some how hybrid structure with sql & no-sql design.."""
    OUTBOUND = "POWERHIVE_TO_CUSTOMER"
    INBOUND = "CUSTOMER_TO_POWERHIVE"


class SMSStatus(mixins.PairEnumMixin, enum.Enum):
    """
    SMS message send status, inbound message will have success by default, sending might failure for number of reasons.
    E.g bad phone number
    """
    FAILURE = 'Failure'
    SUCCESS = "Success"


class SMSAPISyncStatus(mixins.PairEnumMixin, enum.Enum):
    """
    SMS API sync status, when fetching sms from sms providers. e.g AfricanTalk.
    Failure could happen for a number of reasons. E.g unable to access their server.
    """
    FAILURE = 'Failure'
    SUCCESS = "Success"



class SMSServiceProvider(base_model.BaseModel):
    """
    Attributes:
    name: Service provider name E.g Africa's talk.
    userName: user name to access network service. E.g via webservice
    password: Password associated to the account.
    shortCode: code provided by service provider to reference non default associated phone numbers.
    apiKey: api key if exist to access api.
    lastAccessed: Service last accessed.
    status: last access status.
    """
    name = models.CharField(max_length=200, choices=ServiceProviders.get_values_map())
    userName = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    apiKey = models.CharField(max_length=200, null=True, blank=True)
    lastAccessed = fields.DateTimeUTC(null=True, blank=True)

    def __str__(self):
        return "Service Provider: {}".format(self.name)


class PowerhiveSMSAccount(base_model.BaseModel):
    """
    Attribute:
    shortCode: Short code, code provided by service providers.
    smsHost: sms service host used to access sms service.
    latestInBoundSMSId: latest inbound sms id fetched,
    """
    shortCode = models.CharField(max_length=200, null=True, blank=True)
    smsHost = models.ForeignKey(SMSServiceProvider)
    lastInBoundSMSId = models.IntegerField(default=0)

    def __str__(self):
        return "SMS Account: {}".format(self.smsHost)

#TODO(estifanos): Re-introduce sms message as separate model if message gets bigger.
class SMS(base_model.BaseModel):
    """
    Attributes:
    collectTime: When the message was collected or sent.
    message: message content
    status: sms status, inbound message by default are success, as we will be pulling available message from message api.
    inBoundSMSId: sms in-bound id.
    outBoundSMSId: sms out-bound id,
    phAccount: Powerhive Account.
    customer: customer.
    smsFlow: SMS flow type.
    senderPhone: There may be situation sms send out from different phone for customer.
    cost: SMS cost.
    """
    collectTime = fields.DateTimeUTC(default=timezone.now)
    message = models.TextField()
    status  = models.CharField(max_length=100, default=SMSStatus.SUCCESS.value, choices=SMSStatus.get_values_map())
    type  = models.CharField(max_length=100, default=SMSType.INFORMATION.value, choices=SMSType.get_values_map())
    inBoundSMSId = models.IntegerField(null=True, blank=True)
    outBoundSMSId = models.CharField(null=True, blank=True, max_length=100)
    phAccount = models.ForeignKey(PowerhiveSMSAccount)
    customer = models.ForeignKey(customer.Customer)
    smsFlow  = models.CharField(max_length=100, choices=SMSFlow.get_values_map())
    senderPhone = phone_model_field.PhoneNumberField(null=True, blank=True)
    cost = models.CharField(max_length=100, default="0.00")

    def __str__(self):
        return "{} message-{}".format(self.smsFlow, self.message)
