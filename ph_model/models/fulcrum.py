from django.contrib.postgres.fields import JSONField
from django.db import models
from ph_model.models import base_model

class FulcrumForm(base_model.BaseModel):
    data = JSONField()
    description = models.TextField(null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    id = models.CharField(max_length=36,primary_key=True)

class FulcrumQuestion(base_model.BaseModel):
    data = JSONField()
    description = models.TextField(null=True,blank=True)
    label = models.TextField(null=False,blank=False)
    key = models.CharField(max_length=10,null=False,blank=False, db_index=True)
    data_name = models.TextField(null=False,blank=False)
    active = models.BooleanField(default=True)
    form = models.ForeignKey(FulcrumForm)

class FulcrumRecord(base_model.BaseModel):
    metadata = JSONField()
    form = models.ForeignKey(FulcrumForm)
    id = models.CharField(max_length=36, primary_key = True)
    latitude = models.FloatField(null=True,blank=True)
    longitude = models.FloatField(null=True,blank=True)
    form_data = JSONField()


FulcrumForm._meta.get_field('created').auto_now_add = False
FulcrumForm._meta.get_field('updated').auto_now = False
FulcrumRecord._meta.get_field('created').auto_now_add = False
FulcrumRecord._meta.get_field('updated').auto_now = False
