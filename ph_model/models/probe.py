"""Probe model."""
import enum
from django.db import models

import base_device_model
import queen as queen_model
from ph_util import mixins


# TODO(estifanos) Add device ID for probe, Does probe have serial number?
class Probe(base_device_model.DeviceModel):
    """
    Attribute:
        number: Probe's number.
        queen: queen holding the probe.
        deviceId: Probe identifier, queen:slot
        serialNumber: Probe serial number.
    """
    number = models.IntegerField(default=None, null=True, blank=True)
    queen = models.ForeignKey(queen_model.Queen, related_name='probes', null=True, blank=True, default=None)
    deviceId = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return "Probe: {}".format(self.deviceId)

    @property
    def get_name(self):
        return str(self.deviceId)


