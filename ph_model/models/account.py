# coding=utf-8
"""Customer Account info.

Models:
AccountRule: Dynamic account rule to be applied per an Account, helpful to apply circuit on/off logic.
Account: Account for customer.
PhoneAccount: Phone account associated to an account, source of mobile payment.
"""
import decimal

from django.db import models
from phonenumber_field import modelfields as phone_model_field

import base_model
from ph_util.accounting_util import DEFAULT_SMS_LOW_BALANCE_DATE_STR
from ph_util.date_util import str_to_dt

DEFAULT_MIN_ALLOWED_BALANCE = decimal.Decimal('1.0')
DEFAULT_NUM_OF_ALLOWED_BAL_REQ = 3


# TODO(Estifanos): Discuss and add more account rules.
# number of balance query you can make
# Over limit reset timeout when queen decided to turn off circuit.
class AccountRule(base_model.BaseModel):
    """
    Attributes:
    minBalance: Minimum balance an account can hold to stay connected to power.
    dailyAllowedSMSBalReq: Number of allowed sms balance request
    """
    name = models.CharField(max_length=200, null=False, default='Unnamed')
    minBalance = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=DEFAULT_MIN_ALLOWED_BALANCE)
    dailyAllowedSMSBalReq = models.SmallIntegerField(default=DEFAULT_NUM_OF_ALLOWED_BAL_REQ)

    def __str__(self):
        return "{} - Min Bal:{} SMS Limit:{}".format(self.name, self.minBalance, self.dailyAllowedSMSBalReq)


class Account(base_model.BaseModel):
    """
    Attributes:
    accountBalance:     Current account balance.
    accountRule:        AccountRule for an account.
    clearing_bal:       When we set an account to zero the amount goes here
    uncollected_bal:    Amount this account is overdue
    reconnect_bal:      if this is not zero power is disconnected
    stima:              stima points for this account
    """
    accountBalance = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.00)
    accountRule = models.ForeignKey(AccountRule, null=True, blank=True, related_name='accounts')
    sms_low_bal_sent = models.DateField(default=str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date())
    sms_neg_bal_sent = models.DateField(default=str_to_dt(DEFAULT_SMS_LOW_BALANCE_DATE_STR).date())
    kuku_last_pickup = models.DateField(blank=True, null=True)
    clearing_bal = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                       default=decimal.Decimal('0.00'))
    uncollected_bal = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                          default=decimal.Decimal('0.00'))
    reconnect_bal = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                          default=decimal.Decimal('0.00'))
    stima = models.BigIntegerField(default=0)

    def __str__(self):
        return "{}".format(self.id)


class PhoneAccount(base_model.BaseModel):
    """
    Attribute:
    mobileMoneyNumber: Mobile number to credit an account.
    misisd: Phone msisdn
    account: Account phone number associated with.
    firstName: First name of phone holder.
    lastName: Last name of phone holder.
    """

    mobileMoneyNumber = phone_model_field.PhoneNumberField(unique=True)
    msisdn = models.CharField(max_length=200, null=True, blank=True)
    account = models.ForeignKey(Account)
    firstName = models.CharField(max_length=200, null=True, blank=True)
    lastName = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return "Account: {}".format(self.fullName())

    # noinspection PyPep8Naming
    def fullName(self):
        """Concat first and last name"""
        return self.firstName + ' ' + self.lastName
