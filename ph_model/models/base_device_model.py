# coding=utf-8
"""Device related models"""
import enum
from django.db import models
from ph_util import mixins

import base_model
import event

# Default max digits used to decimal fields.
MAX_DIGITS = base_model.MAX_DIGITS
# Default decimal places used to decimal fields.
DECI_PLACES = base_model.DECI_PLACES


# TODO(estifanos) change constants to unknown, ok, fault , notfound.
class DeviceStatus(mixins.PairEnumMixin, enum.Enum):
    """Severity constants."""
    UNKNOWN = "UNKNOWN"
    OK = "OK"
    FAULT = "FAULT"
    NOT_FOUND = "NOT_FOUND"


class DeviceModel(base_model.BaseModel):
    """Not instantiable, base model for devices or hardwares.

    Attributes:
    deviceStatus: Hardware device status.
    highestUnclearedEvent: Highest severity reported in the device or it's descendant, e.g if queen,
    then it will be highest severe event on the queen, or it's probes or circuits.
    """
    highestUnclearedEvent = models.ForeignKey(event.Event, on_delete=models.SET_NULL, null=True, blank=True)
    deviceStatus = models.CharField(max_length=100, default=DeviceStatus.UNKNOWN.value, null=True, blank=True)

    # noinspection PyMissingOrEmptyDocstring
    class Meta:
        abstract = True
