from django.db import models
import base_model
import decimal
import fields
import grid

class AggregateGridTracking(base_model.BaseModel):
    grid = models.ForeignKey(grid.Grid)
    lastRun = fields.DateTimeUTC()

class AggregateGrid(base_model.BaseModel):
    aggregateDate = models.DateField(auto_now=False, auto_now_add=False)
    grid = models.ForeignKey(grid.Grid)
    customersUsing = models.IntegerField(default=0)
    energyUsed = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    debits = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    averageTariff = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    deposits = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    adjustments = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    whPerUser = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    arpu = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    totalBalance = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
