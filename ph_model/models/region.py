# coding=utf-8
"""Region model."""
from django.db import models

import base_model
import country


class Region(base_model.BaseModel):
    """
    Attributes:
        name: Region name.
        country: Region's country.
    """
    name = models.CharField(max_length=200)
    country = models.ForeignKey(country.Country)

    def __str__(self):
        return "{}-{}".format(self.country, self.name)

