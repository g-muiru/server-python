# coding=utf-8
"""Tariffs defition models

   Models:
      Tariff: Tariff customer assigned to.
      TariffCalendar: current tariff calendar.
      Tariff Segment: available tariff slots.
"""
import decimal
from django.db import models

import base_model


# noinspection PyUnresolvedReferences
class TariffSegment(base_model.BaseModel):
    """
    Attributes:
        segmentId: Tariff segment's id.
        nextTariffSegment: Next tariff segment, linked via logic according time or usage.
        tariffType: Tariff types, listed as  TARIFF_TYPE_CHOICES.
        timeExpires: amount where time expires for time based tariff.
        whExpires, amount where Energy expires.
        whLimit: Maximum energy can be consumed under this tariff segment, otherwise should link to other segment.
        vaLimit: Maximum power can be consumed under this tariff segment.
        perSegmentCharge: Initial, one time charge when enterying tariff segment.
        perWhCharge: Amount charged per Watt-Hour on this segment(energy).
        perVACharge: Amount charged per Watt on this segment(power).
    """
    TARIFF_TYPE_CHOICES = (
        ('TIME', 'Time based tariff.'),
        ('ENERGY', 'Energy based tariff'),
    )
    segmentId = models.IntegerField(primary_key=True)
    nextTariffSegment = models.ForeignKey('self', null=True)
    tariffType = models.CharField(choices=TARIFF_TYPE_CHOICES, default='TIME', max_length=20)
    timeExpires = models.IntegerField(null=True)
    whExpires = models.DecimalField(
        max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
        default=decimal.Decimal('0.00'))
    whLimit = models.DecimalField(
        max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
        default=decimal.Decimal('0.00'))
    vaLimit = models.DecimalField(
        max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
        default=decimal.Decimal('0.00'))
    perSegmentCharge = models.DecimalField(
        max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
        default=decimal.Decimal('0.00'))
    perWhCharge = models.DecimalField(
        max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
        default=decimal.Decimal('0.00'))
    perVACharge = models.DecimalField(
        max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
        default=decimal.Decimal('0.00'))

    def __str__(self):
        return "Id: {}, type: {} timeExpires: {} perSegmentCharge: {}". \
            format(self.segmentId, self.tariffType, self.timeExpires, self.perSegmentCharge)


# noinspection PyUnresolvedReferences
class TariffCalendar(base_model.BaseModel):
    """
    Attributes:
         calendarId: Calendar Id.
         nextTariffCal: Next tariff calendar
         entryTariffSegment:entry tariff segment for tariff calendar
         dateExpires: Date Calendar expires.
    """
    calendarId = models.IntegerField(primary_key=True)
    nextTariffCal = models.ForeignKey('self', null=True, blank=True)
    entryTariffSegment = models.ForeignKey(TariffSegment, null=True, blank=True)
    dateExpires = models.DateField(null=True, blank=True)

    def __str__(self):
        return "{} - Expires-{}".format(self.calendarId, self.dateExpires)


# noinspection PyUnresolvedReferences
class Tariff(base_model.BaseModel):
    """
    Attributes:
        tariffId: Tariff id associated with the customer.
        entryTariffCalendar: Entry of tariff calendar per tariff.
    """
    tariffId = models.IntegerField(primary_key=True)
    entryTariffCalendar = models.OneToOneField(TariffCalendar)
    name = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.tariffId, self.name)
