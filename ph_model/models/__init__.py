# coding=utf-8
"""Powerhive model schema, ER diagram https://github.com/Powerhive1/documentation/blob/master/powerhive_db_schema/index.html.
Related models are grouped in same module.

App includes, sub-packages:
  admin: Django model admins to do CRUDs, host://admin.
  migrations: South migrations.
  Fixture: Initial data.
  tests: Model tests.
  fields: custom django model fields.
  Models: Ph models.
"""

from grid import Grid
from probe import Probe
from queen import Queen
from queen import QueenFirmware
from inverter import Inverter
from powerstation import PowerStation
from generation import Generation
from account import Account
from account import AccountRule
from account import PhoneAccount
from aggregate import AggregateGrid
from aggregate import AggregateGridTracking
from battery import BatteryBank
from circuit import Circuit
from circuit import SwitchEnabledHistory
from company import Company
from customer import Customer
from customer import PotentialCustomerImportHistory
from customer import CustomerTariffHistory
from event import Event
from event import EventDetail
from event import EventType
from loan import Loan
from loan import LoanPaymentHistory
from monitor import QueenMonitor
from monitor import ProbeMonitor
from monitor import InverterMonitor
from monitor import BatteryBankMonitor
from permission import Role
from permission import Permission
from tariff import Tariff
from tariff import TariffCalendar
from tariff import TariffSegment
from mobile_payment_provider import NetworkServiceProvider
from mobile_payment_provider import PaymentSyncHistory
from mobile_payment_provider import C2BPaymentValidationRequest
from scratchcard import Scratchcard
from sms import PowerhiveSMSAccount
from sms import SMS
from sms import SMSServiceProvider
from transaction import CreditAdjustmentHistory
from transaction import MobilePayment
from transaction import UsageCharge
from usage import Usage
from usage import NonCustomerUsage
from fulcrum import FulcrumForm, FulcrumQuestion, FulcrumRecord

default_app_config = 'ph_model.apps.PhModelConfig'

__all__ = ['account', 'Account', 'AccountRule', 'AggregateGrid', 'AggregateGridTracking', 'battery', 'BatteryBank', 'C2BPaymentValidationRequest',
           'circuit', 'company', 'Company', 'country', 'CreditAdjustmentHistory', 'customer',
           'Customer', 'PotentialCustomerImportHistory', 'CustomerTariffHistory', 'event', 'Event', 'EventDetail', 'EventType', 'generation', 'grid',
           'inverter', 'loan', 'Loan', 'LoanPaymentHistory', 'monitor', 'QueenMonitor', 'QueenFirmware', 'Queen', 'PowerhiveSMSAccount',
           'ProbeMonitor', 'InverterMonitor', 'BatteryBankMonitor', 'municipality',
           'mobile_payment_provider', 'NetworkServiceProvider', 'permission', 'PaymentSyncHistory', 'PhoneAccount', 'powerstation', 'scratchcard',
           'Scratchcard',
           'sms', 'SMS', 'SMSServiceProvider', 'SwitchEnabledHistory', 'tariff', 'TariffCalendar', 'TariffSegment', 'transaction', 'MobilePayment',
           'UsageCharge', 'usage',
           'Usage', 'NonCustomerUsage', 'user', 'fields', 'fulcrum', 'FulcrumForm', 'FulcrumQuestion', 'FulcrumRecord']
