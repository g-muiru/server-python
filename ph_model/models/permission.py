"""User Permissions."""
import enum

from django.db import models

import base_model
import user
from ph_util import mixins


# TODO(estifanos): figure out all the roles involved. use camel notation.
class Role(mixins.PairEnumMixin, enum.Enum):
    """Permission role constants."""
    VIEW = 1
    WRITE = 2

class Permission(base_model.BaseModel):
    """
    Attributes:
        user: power-hive user.
        itemId: Item id where the permission is applied.
        role: Permission role.
        itemType: Item type, e.g Grid.
    """
    ITEM_TYPE_CHOICES = (
        ('GRID', 'Grid'), ('PROJECT', 'Project'),
    )
    user = models.ForeignKey(user.User)
    itemId = models.IntegerField()
    role = models.IntegerField(choices=Role.get_values_map(), default=Role.WRITE.value)
    itemType = models.CharField(max_length=200, choices=ITEM_TYPE_CHOICES, default='GRID')

    class Meta:
        unique_together = (("user", "itemId", "itemType"),)

    def __str__(self):
        return 'User: {}, Item type: {}, Item id: {}, role: {}'.format(
            self.user, self.itemType, self.itemId, self.role)

