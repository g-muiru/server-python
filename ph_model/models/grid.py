# coding=utf-8
"""Grid model."""
from datetime import date
import enum

from django.db import models

import base_device_model
import municipality
import project
from ph_util import mixins

# Initial grid configured in ph_model/fixtures/json/initial_data.json
DEFAULT_GRID_NAME = 'unallocated'

class GridStatus(mixins.PairEnumMixin, enum.Enum):
    """Queen  status."""
    DISABLED = 0
    ENABLED = 1
    UNKNOWN = 2



class Grid(base_device_model.DeviceModel):
    """
        Attributes:
            name: Grid name.
            project: Project
            municipality: Physical location, region municipality.
            village_name: village grid is located in
            operational_date: date grid goes live and customers get charged
        """
    name = models.CharField(max_length=200, unique=True)
    project = models.ForeignKey(project.Project)
    municipality = models.ForeignKey(municipality.Municipality)
    village_name = models.CharField(max_length=200)
    status = models.IntegerField(default=GridStatus.DISABLED.value, choices=GridStatus.get_values_map())
    # can not import FAR_FUTURE_DATE --- messes up models
    operational_date = models.DateField(default=date(2222, 2, 2))

    def __str__(self):
        return "{}".format(self.name)

    @property
    def get_name(self):
        """return the name"""
        return str(self.name)
