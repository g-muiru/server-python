# coding=utf-8
"""Customer loan info.

Models:
Loan: Loan given a customer.
LoanPaymentHistory: Loan payment history.
"""

import decimal
from django.db import models
from django.utils import timezone
import base_model
import account
import fields
from django.dispatch import receiver
from django.db.models import signals
import transaction
from ph_util.date_util import get_utc_now
from ph_util.email_util import send_loan_init_email
from ph_util.accounting_util import add_vat, str_obj, init_loan_type_to_accounting_code, LoanType, LoanRepayment, AccountingEnums, DEAD_LOAN_ACCOUNT

DEFAULT_PAYMENT_FREQUENCY_IN_DAYS = 1


class Loan(base_model.BaseModel):
    """
    Attributes:
    account:                    the account this loan is against
    passedDue:                  Boolean to indicate if loan is past due.
    fixedRepayment:             on fixed repayment schedule (last day of month, etc)
    repaymentSchedule:          repayment schedule, if fixedRepayment
    outStandingPrinciple:       unpaid loan amount.
    loanAmount:                 initial loan amount.
    startDate:                  Start date of the loan.
    paymentFrequency:           Payment frequency of in days.
    lastPaymentReceivedDate:    last payment received date.
    lastPaymentNumber:          last payment number out of total number of payments.
    totalNumberOfPayments:      total number of payments expected per loan duration.
    annualInterestRate:         annual interest rate.
    loanType:                   The type of loan from the LoanType enum
    notes:                      loan info
    """
    account = models.ForeignKey(account.Account)
    passedDue = models.BooleanField(default=False)
    fixedRepayment = models.BooleanField(default=False)
    repaymentSchedule = models.CharField(max_length=200, null=True, blank=True, choices=LoanRepayment.get_values_map())
    outStandingPrinciple = models.DecimalField(max_digits=50, decimal_places=2, default=decimal.Decimal('0.00'))
    loanAmount = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    startDate = fields.DateTimeUTC(default=timezone.now)
    paymentFrequency = models.IntegerField(default=DEFAULT_PAYMENT_FREQUENCY_IN_DAYS)
    lastPaymentReceivedDate = fields.DateTimeUTC(null=True, blank=True)
    lastPaymentNumber = models.IntegerField(null=True, blank=True, default=0)
    totalNumberOfPayments = models.IntegerField()
    annualInterestRate = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                             default=decimal.Decimal('0.00'))
    loanType = models.CharField(max_length=200, null=True, blank=True, choices=LoanType.get_values_map())
    notes = models.TextField(null=True, blank=True)

    def create_payment_history_loan_init(self):
        """
        When a loan is created this function is called
        Note that creating loans does not change the customer balance
        :return:
        """

        if self.account.id == DEAD_LOAN_ACCOUNT:
            return
        loan_amt = decimal.Decimal(self.loanAmount)
        self.outStandingPrinciple = loan_amt
        if self.loanType is None:
            self.loanType = LoanType.OTHER_LOAN.name
        if self.loanType != LoanType.KUKU_POA_NO_VAT_LOAN.name:
            self.outStandingPrinciple = add_vat(loan_amt)
        self.save()
        msg = str_obj(self) + '\n\n'

        # add loan debit invoice
        payment_history = LoanPaymentHistory(
            loan=self,
            principlePayment=0.0,
            interestPayment=0.0,
            balanceAfterPayment=self.account.accountBalance,
            processedTime=get_utc_now(),
            amount=loan_amt * -1)

        payment_history.save_accounting(init_loan_type_to_accounting_code(self.loanType))
        msg += str_obj(payment_history) + '\n\n'

        if self.loanType != LoanType.KUKU_POA_NO_VAT_LOAN.name:
            # Add a VAT debit invoice
            # noinspection PyTypeChecker
            payment_history = LoanPaymentHistory(
                loan=self,
                principlePayment=0.0,
                interestPayment=0.0,
                balanceAfterPayment=self.account.accountBalance,
                processedTime=get_utc_now(),
                amount=((self.outStandingPrinciple - self.loanAmount) * -1))

            # TODO not doing this strange accounting
            # decrease the account balance by the loan VAT amount
            # self.account.accountBalance = decimal.Decimal(self.account.accountBalance) + decimal.Decimal(payment_history.amount)
            # self.account.save()

            payment_history.balanceAfterPayment = self.account.accountBalance
            # there is only 1 VAT code for loans -- brody wanted it that way
            payment_history.save_accounting(init_loan_type_to_accounting_code(self.loanType, vat=self.loanType != LoanType.KUKU_POA_NO_VAT_LOAN.name))
            msg += str_obj(payment_history) + '\n\n'

        send_loan_init_email(msg)

    def __str__(self):
        return str_obj(self)

    def __repr__(self):
        return self.__str__()


class LoanPaymentHistory(base_model.BaseModel):
    """
    Attribute:
    loan: link to type of loan.
    principlePayment: A payment toward the amount of principal owed
    interestPayment: interest payed
    balanceAfterPayment: account balance after payment.
    balanceBeforePayment: account balance before payment.
    balanceAfterPayment: account balance after payment.
    lastName: Last name of phone holder.
    amount: principalPayment + interestPayment
    notcollected: True means the amount billed was not collected (insufficient funds)
    """
    syncToMasterTransaction = models.BooleanField()
    loan = models.ForeignKey(Loan)
    principlePayment = models.DecimalField(max_digits=50, decimal_places=3,
                                           default=decimal.Decimal('0.00'))
    interestPayment = models.DecimalField(max_digits=50, decimal_places=3,
                                          default=decimal.Decimal('0.00'))
    balanceAfterPayment = models.DecimalField(max_digits=50, decimal_places=3,
                                              default=decimal.Decimal('0.00'))
    processedTime = fields.DateTimeUTC(default=timezone.now)
    amount = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                 default=decimal.Decimal('0.00'))
    notcollected = models.BooleanField(default=False)

    # noinspection PyMethodMayBeStatic
    def get_transaction_type(self):
        """

        :return:
        """
        return transaction.TransactionType.DEBIT.value

    def get_account(self):
        """

        :return:
        """
        return self.loan.account

    def __str__(self):
        return "Loan ID: {}".format(self.loan)

    def save_accounting(self, acc_code_enum=AccountingEnums.LOAN_OTHER_INITc280):
        """
        Called when loans are created to apply the proper source identifier
        :param acc_code_enum:
        :return:
        """
        super(LoanPaymentHistory, self).save()
        transaction.save_to_master_transaction(self, acc_code_enum.name)


# noinspection PyUnusedLocal
@receiver(signals.pre_save, sender=LoanPaymentHistory)
def money_transactions_pre_save(sender, **kwargs):
    """

    :param sender:
    :param kwargs:
    """
    obj = kwargs['instance']
    obj.syncToMasterTransaction = True


# noinspection PyUnusedLocal
@receiver(signals.post_save, sender=Loan)
def receiver_function(sender, **kwargs):
    """
    When a loan is created the account balance is not changed
    Transactions are created in both the ph_model_loanpaymenthistory and ph_model_mastertransactionhistory tables
    The loan outStandingPrinciple is set to the loan amount plus VAT

    :param sender:  Always a Loan object
    :param kwargs:  We only runt the code when 'created' is true
    :return:
    """
    # TODO this should be in a DB transaction
    if kwargs['created']:
        """ Create initial invoices for the loan"""
        loan = kwargs['instance']
        loan.create_payment_history_loan_init()
