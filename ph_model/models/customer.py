# coding=utf-8
"""Customer model."""
import decimal
import random
import uuid
from django.db import models
from phonenumber_field import modelfields as phone_model_field
import account
import base_model
import circuit
import fields
import tariff as customer_tariff
import enum
from ph_util import mixins
import municipality
import fulcrum


class CustomerStatus(mixins.PairEnumMixin, enum.Enum):
    """customer status constants."""
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"
    POTENTIAL = "POTENTIAL"
    PAID_DEPOSIT = "PAID_DEPOSIT"
    CONSTRUCTION_PENDING = "CONSTRUCTION_PENDING"
    DISCONNECTED = "DISCONNECTED"
    ABANDONED = "ABANDONED"

    ACTIVE_STATUSES = (ACTIVE, POTENTIAL, PAID_DEPOSIT, CONSTRUCTION_PENDING)


class CustomerType(mixins.PairEnumMixin, enum.Enum):
    """customer types"""
    RESIDENTIAL = "RESIDENTIAL"
    COMMERCIAL = "COMMERCIAL"
    COMMERCIAL_II = "COMMERCIAL_II"  # eg, posho mill, larger lathe / bandsaw
    ANCHOR = "ANCHOR"


def get_rand_string(digits=10):
    """Generates 10 digit random numbers as string. Uses double randomizing uuid 32 byte then randomize to 10 byte"""
    return "".join(random.sample(list(str(uuid.uuid4().int)), digits))


# noinspection PyPep8Naming,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences
# PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences
class Customer(base_model.BaseModel):
    """
    Attributes:
    customerId: Auto generated 10 digites Customer ID.
    firstName: First name
    lastName: Last name.
    email: Email,
    phoneNumber: Phone number.
    nationalIdNumber: id card number
    languagePreference: prefered language
    account: the account associated with this phone
    circuit: circuit customer assigned to.
    tariff: customer's tariff.
    tariffSegment: current tariff segment/slot from the tariff definition.
    tariffCalendar: current tariff calendar.
    latitude: customer/circuit physical location, latitude.
    longitude: customer/circuit physical location, longitude.
    municipality: where this account resides
    serviceContract: from fulcrum
    customerSurvey: from fulcrum
    lastUsageProcessedTime: last usage processed time for customer.
    cumulativeDailyUsageWh: cumulative daily usage to be reset every 24 hours
    status: customer status
    customerType: customer type
    connectionFeeLoan: true if there is a connection fee loan
    connectionFeeDue: date loan is due
    dailySMSBalRequested: Daily SMS balance requested.
    days_no_balance: Number of continous days the customer's account balance has been zero or less
    kuku_poa_id: if this equals the account_id for this customer then this is a active Kuku Poa customer
    cumulativeKwh:  Total Kwh used to date
    """
    customerId = models.CharField(max_length=10, unique=True, default=get_rand_string)
    firstName = models.CharField(max_length=200, null=True, blank=True)
    lastName = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=200, null=True, blank=True)
    phoneNumber = phone_model_field.PhoneNumberField()
    nationalIdNumber = models.CharField(max_length=20, null=True, blank=True)
    languagePreference = models.CharField(max_length=30, null=True, blank=True)
    account = models.OneToOneField(account.Account, related_name='accountOwner')
    circuit = models.OneToOneField(circuit.Circuit, on_delete=models.DO_NOTHING, null=True, blank=True, related_name='circuitOwner')
    tariff = models.ForeignKey(customer_tariff.Tariff, null=True, blank=True, on_delete=models.SET_NULL)
    tariffSegment = models.ForeignKey(customer_tariff.TariffSegment, null=True, blank=True, on_delete=models.SET_NULL)
    tariffCalendar = models.ForeignKey(customer_tariff.TariffCalendar, null=True, blank=True, on_delete=models.SET_NULL)
    latitude = models.FloatField(default=37.863606)
    longitude = models.FloatField(default=-122.29665)
    municipality = models.ForeignKey(municipality.Municipality)
    serviceContract = models.OneToOneField(fulcrum.FulcrumRecord, null=True, blank=True, on_delete=models.SET_NULL, related_name='contract_customer')
    customerSurvey = models.OneToOneField(fulcrum.FulcrumRecord, null=True, blank=True, on_delete=models.SET_NULL, related_name='surveyed_customer')
    lastUsageProcessedTime = fields.DateTimeUTC(null=True, blank=True)
    cumulativeDailyUsageWh = models.DecimalField(max_digits=base_model.MAX_DIGITS,
                                                 decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    status = models.CharField(max_length=200, default=CustomerStatus.POTENTIAL.value, choices=CustomerStatus.get_values_map())
    customerType = models.CharField(max_length=200, default=CustomerType.RESIDENTIAL.value, choices=CustomerType.get_values_map())
    connectionFeeLoan = models.BooleanField(default=False)
    connectionFeeDue = fields.DateTimeUTC(null=True, blank=True)
    dailySMSBalRequested = models.SmallIntegerField(default=0)
    days_no_balance = models.IntegerField(default=-1)
    kuku_poa_id = models.IntegerField(null=True, default=None, blank=True)
    cumulativeKwh = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=decimal.Decimal('0.00'))

    def getName(self):
        """

        :return:
        """
        return "{} {}".format(self.firstName, self.lastName)

    def add_service_contract_by_id(self, fid):
        """

        :param fid:
        :return:
        """
        f = fulcrum.FulcrumRecord.objects.get(id=fid)
        self.serviceContract = f
        self.save()

    def __str__(self):
        return "{}: {} {} {} {}".format(self.customerId, self.firstName, self.lastName, self.id, self.circuit_id)


class CustomerTariffHistory(base_model.BaseModel):
    """
    Attributes:
    customer: customer.
    tariff: customer's tariff.
    """
    customer = models.ForeignKey(Customer)
    tariff = models.ForeignKey(customer_tariff.Tariff)


class CustomerImportStatus(mixins.PairEnumMixin, enum.Enum):
    """Customer import status."""
    OK = 'OK'
    FAILURE = 'FAILURE'


class PotentialCustomerImportHistory(base_model.BaseModel):
    """
    Attributes:
    dataSource: source where the information is collected.
    totalCustomerDataSynced: number of new/updated customer data synced form data source.
    lastSyncedSourceUpdatedAtSinceEpoch: last updated time.
    status: collect status.
    """
    dataSource = models.CharField(max_length=200)
    totalCustomerDataSynced = models.IntegerField(default=0)
    lastSyncedSourceUpdatedAtSinceEpoch = models.IntegerField(null=True, blank=True)
    status = models.CharField(max_length=200, choices=CustomerImportStatus.get_values_map(),
                              default=CustomerImportStatus.OK.value)

    # noinspection PyMissingOrEmptyDocstring
    class Meta:
        get_latest_by = "lastSyncedSourceUpdatedAtSinceEpoch"

    def __str__(self):
        return "{} lastSourceUpdatedAtEpoch: {}".format(self.dataSource, self.lastSyncedSourceUpdatedAtSinceEpoch)
