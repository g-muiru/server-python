from django.db import models
from django.utils import  timezone

# Default max digits used to decimal fields.
MAX_DIGITS = 40
# Default decimal places used to decimal fields.
DECI_PLACES = 20

class GetOrNoneManager(models.Manager):
    """Adds get_or_none method to objects """
    def get_or_none(self, **kwargs):
        """Gets or returns None."""
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None

    def get_or_raise_exception(self, exception, **kwargs):
        """Get or raise exception @parar exception."""
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist as e:
            raise exception(e)

class BaseModel(models.Model):
    """Non instantiable,  base model for all models."""
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    objects = GetOrNoneManager() # The default manager.


    class Meta:
        abstract = True
        app_label = 'ph_model'
