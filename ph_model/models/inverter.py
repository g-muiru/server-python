"""Inverter model."""
import enum
from django.db import models

import base_device_model
import powerstation
from ph_util import mixins


class Type(mixins.PairEnumMixin, enum.Enum):
    """Inverter types."""
    PV = 'PV'
    BATTERY = 'BATTERY'


# TODO(estifanos): Add volatage related fields. Check invertor types, verify has serial  number.
class Inverter(base_device_model.DeviceModel):
    """
    Attributes:
        deviceId: inverter identifier.
        type; Inverter type.
        powerstation: powerstation attached to the inverters.
    """
    deviceId = models.CharField(max_length=50, unique=True, null=True, blank=True)
    serialNumber = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(max_length=30, choices=Type.get_keys_map())
    powerstation = models.ForeignKey(powerstation.PowerStation, related_name='inverters')

    @property
    def get_name(self):
        return str('I{}:{}'.format(self.type, self.powerstation))

    def __str__(self):
        return '{}-{}'.format(self.type, self.deviceId)


