"""Model to monitor grid."""
from django.db import models

import base_model
import inverter as inverter_model
import battery as battery_model
import base_device_model as device_model
import fields
import circuit as circuit_model
import probe as probe_model
import queen as queen_model


class _BaseDeviceMonitor(base_model.BaseModel):
    """
    Attributes:
         collectTime: Time collected.
         commAttempts: Number of communication attempts.
         commErrors: Number of communication errors.
    """
    collectTime = fields.DateTimeUTC()
    commAttempts = models.PositiveIntegerField(default=0)
    commErrors = models.PositiveIntegerField(default=0)
    deviceStatus = models.CharField(max_length=100, default=device_model.DeviceStatus.UNKNOWN.value)

    class Meta:
        abstract = True


class QueenMonitor(_BaseDeviceMonitor):
    """
    Attributes:
         queen: Queen monitor data belonging to.,
         rssiAvg: Average RSSI(Received signal strength indication).
         rssiMin: Minimum RSSI.
         tempAvg: Average temperature.
         tempMin: Minimum temperature.
         tempMax: Max temperature.
         osUptime: OS up time
         fwUptime: Firmware up time
    """
    queen = models.ForeignKey(queen_model.Queen)
    rssiAvg = models.FloatField(default=0.0)
    rssiMin = models.FloatField(default=0.0)
    rssiMax = models.FloatField(default=0.0)
    tempAvg = models.FloatField(default=0.0)
    tempMin = models.FloatField(default=0.0)
    tempMax = models.FloatField(default=0.0)
    photoAvg = models.FloatField(default=0.0)
    photoMin = models.FloatField(default=0.0)
    photoMax = models.FloatField(default=0.0)
    osUptime = models.IntegerField(default=0)
    fwUptime = models.IntegerField(default=0)
    fwVersion = models.IntegerField(default=0)
    fsFreeStatus = models.IntegerField(null=True)
    fsFreePct = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                    null=True)
    hardwareReinits = models.IntegerField(null=True)

    def __str__(self):
        return '{}'.format(self.queen)


class ProbeMonitor(_BaseDeviceMonitor):
    """
    Attributes:
         probe: probe monitor data belonging to.
    """
    probe = models.ForeignKey(probe_model.Probe)

    def __str__(self):
        return '{}'.format(self.probe)


class CircuitMonitor(_BaseDeviceMonitor):
    """
    Attributes:
         circuit: circuit monitor data belonging to.
    """
    circuit = models.ForeignKey(circuit_model.Circuit)
    vacAvg = models.FloatField(default=0.0)
    vacMin = models.FloatField(default=0.0)
    vacMax = models.FloatField(default=0.0)
    iacMinN = models.FloatField(default=0.0)
    iacMaxN = models.FloatField(default=0.0)
    iacAvgN = models.FloatField(default=0.0)
    iacMinH = models.FloatField(default=0.0)
    iacMaxH = models.FloatField(default=0.0)
    iacAvgH = models.FloatField(default=0.0)
    switchEnabled = models.CharField(max_length=50, choices=circuit_model.SwitchEnabledStatus.get_values_map())
    disableVaLimit = models.NullBooleanField(null=True, blank=True)
    switchReason = models.CharField(max_length=50, default=circuit_model.SwitchEnabledChangeReason.UNKNOWN.value,
                                    choices=circuit_model.SwitchEnabledChangeReason.get_values_map())


    def __str__(self):
        return '{}'.format(self.circuit)


class InverterMonitor(_BaseDeviceMonitor):
    """
    Attributes:
         inverterStatus: inverter monitor data belonging to.
         va: power
         vac: power alternate current.
         vdc: power direct current.
    """
    inverter = models.ForeignKey(inverter_model.Inverter)
    whFromInverter = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                         default=0.0)

    vaFromInverterMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                            default=0.0)
    vaFromInverterAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                            default=0.0)
    vaFromInverterMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                            default=0.0)

    vacMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    vacAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    vacMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)

    vdcMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    vdcAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    vdcMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)

    idcMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    idcAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    idcMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)

    # Sepecific to the battery inverters
    socMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0,
                                 null=True)
    socAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0,
                                 null=True)
    socMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0,
                                 null=True)

    # updated to account for the fact that some inverters are Pv inverters, some are battery inverters, and some are
    #   both at the same time!
    inverterStatus = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    condition = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    operatingStatus = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)

    whFromPvInverter = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    vdcPvMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    vdcPvAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    vdcPvMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    idcPvMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    idcPvAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    idcPvMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    pdcPvMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    pdcPvAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    pdcPvMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    
    whFromBatteryInverter = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    vdcBatteryMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    vdcBatteryAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    vdcBatteryMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    idcBatteryMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    idcBatteryAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    idcBatteryMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    tempBatteryMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    tempBatteryAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)
    tempBatteryMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0, null=True)


    def __str__(self):
        return '{}'.format(self.inverter)


class BatteryBankMonitor(_BaseDeviceMonitor):
    """
    Attributes:
         batteryBank: BatteryBank monitor data belonging to,
         soc: state of charge.
         i: current ampere.
    """
    batteryBank = models.ForeignKey(battery_model.BatteryBank)

    vdcMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    vdcAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    vdcMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)

    idcMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    idcAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    idcMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)

    socMin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    socAvg = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)
    socMax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES, default=0.0)


    def __str__(self):
        return '{}'.format(self.batteryBank)

