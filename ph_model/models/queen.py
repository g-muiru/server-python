# coding=utf-8
"""Queen Model."""
from datetime import date

import enum

from django.db import models
from django.conf import settings
import base_device_model

import base_model
import fields
import grid
from ph_util import mixins

# TODO(estifanos): Change default intervals.
DEFAULT_USAGE_INTERVAL = 900  # In seconds
DEFAULT_MONITOR_INTERVAL = 120  # In seconds
DEFAULT_GENERATION_INTERVAL = 900  # In seconds
DEFAULT_GRID = 7  # "Unallocated"
DEFAULT_FIRMWARE = 32  # init default, increment as needed  !!! must exist in ph_model_queenfirmware !!!


class FirmwareStatus(mixins.PairEnumMixin, enum.Enum):
    """Queen firmware status."""
    CHECKSUM_FAILURE = 0
    OK = 1


class QueenStatus(mixins.PairEnumMixin, enum.Enum):
    """Queen  status."""
    DISABLED = 0
    ENABLED = 1


class FsFreeStatus(mixins.PairEnumMixin, enum.Enum):
    """Queen fsFreeStatus"""
    OK = 0
    COULD_NOT_OPEN_MOUNT_FILE = 1
    COULD_NOT_GET_ROOTFS_STATUS = 2
    COULD_NOT_FIND_ROOTFS_IN_THE_MOUNT = 3


# TODO(estifanos)Remove unnecessary fields.
class QueenFirmware(base_model.BaseModel):
    """
    Attributes:
        version: Firmware version part of firmware location directory
        fileName: firmware file name.
        fileExtension: File type
        baseURL: Base url where the firmware resides.
        relativeUrl: Relative url where the firmware resides.
        md5Sum: md5 hash algorithm.
        path: used to get firmware file
    """
    version = models.CharField(max_length=100, unique=True)
    md5Sum = models.CharField(max_length=200, null=True, blank=True)
    status = models.IntegerField(default=FirmwareStatus.OK.value, choices=FirmwareStatus.get_values_map())
    fileBaseName = models.CharField(max_length=400, default=settings.DEFAULT_FIRMWARE_FILE_NAME)
    fileExtension = models.CharField(max_length=100, default=settings.FIRMWARE_FILE_EXTENSION)
    baseUrl = models.CharField(max_length=200, default=settings.FIRMWARE_BASE_URL)
    relativeUrl = models.CharField(max_length=200, default=settings.FIRMWARE_RELATIVE_URL)
    path = models.CharField(max_length=400, default=settings.FIRMWARE_PATH)

    @property
    def url(self):
        """

        :return:
        """
        return str('{}/{}/{}'.format(self.baseUrl, self.relativeUrl, self.version))

    # noinspection PyPep8Naming
    @property
    def filePath(self):
        """

        :return:
        """
        return str('{}/{}{}'.format(self.path, self.fileBaseName, self.fileExtension))

    # noinspection PyPep8Naming
    @property
    def fileName(self):
        """

        :return:
        """
        return str('{}.{}'.format(self.fileBaseName, self.fileExtension))

    def __str__(self):
        return self.version


class Queen(base_device_model.DeviceModel):
    """
    Attributes:
        number: Queen number on a grid.
        Grid: Grid queen belongs to.By default newly discovered queen will be under grid 1(pseudo grid)
        latitude: Physical queen location, latitude.
        longitude: Physical queen location, longitude.
        deviceId: Queen device id.
        status: Queen status.
        lastContact: Queen last contacted date.
        firstContactDate: Queens first contacted date.
        currentFirmware: Current embedded system software version
        expectedFirmware: Expected embedded system software version
        lanAddress: Lan Address
        wanAddress: Wan Address
        verbosity: verbosity
        usageInterval: Interval queen send out usage data.
        generationInterval: Interval queen send out generation data.
        monitorInterval: Interval queen send out monitor data.
        osStarted: TS for Linux OS start
        fwStarted: TS for queen software start
        reboot: True means the firmware will restart
        tunnel: Port number for remote access
        ethConnect: TODO -
        generateKeyPair: For remote tunnel access
        sendPublicKey: For remote tunnel access
        sendPublicKeyPassword: For remote tunnel access
        fsFreeStatus: Disk free space
        fsFreePct:
        notes:
        operational_date: must be in the past for the queen to bill power usage
    """
    number = models.CharField(max_length=8, null=True, blank=True)
    grid = models.ForeignKey(grid.Grid, related_name='queens')
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    deviceId = models.CharField(max_length=50, unique=True)
    status = models.IntegerField(default=QueenStatus.DISABLED.value, choices=QueenStatus.get_values_map())
    lastContact = fields.DateTimeUTC(null=True, blank=True)
    firstContactDate = fields.DateTimeUTC(null=True, blank=True)
    currentFirmware = models.ForeignKey(QueenFirmware, null=True, blank=True, on_delete=models.SET_NULL, related_name='current_queens')
    expectedFirmware = models.ForeignKey(QueenFirmware, null=True, blank=True, on_delete=models.SET_NULL, related_name='expected_queens')
    lanAddress = models.CharField(max_length=100, null=True, blank=True)
    wanAddress = models.CharField(max_length=100, null=True, blank=True)
    verbosity = models.IntegerField(default=0)
    usageInterval = models.IntegerField(default=DEFAULT_USAGE_INTERVAL)
    generationInterval = models.IntegerField(default=DEFAULT_GENERATION_INTERVAL)
    monitorInterval = models.IntegerField(default=DEFAULT_MONITOR_INTERVAL)
    osStarted = fields.DateTimeUTC(null=True, blank=True)
    fwStarted = fields.DateTimeUTC(null=True, blank=True)
    reboot = models.BooleanField(default=False)
    tunnel = models.IntegerField(default=0)
    ethConnect = models.CharField(max_length=100, default="", blank=True)
    generateKeyPair = models.BooleanField(default=False)
    sendPublicKey = models.CharField(max_length=200, default="", blank=True)
    sendPublicKeyPassword = models.CharField(max_length=200, default="", blank=True)
    fsFreeStatus = models.IntegerField(null=True, choices=FsFreeStatus.get_values_map(), blank=True)
    fsFreePct = models.IntegerField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    operational_date = models.DateField(default=date(2222, 2, 2))

    @property
    def get_name(self):
        """

        :return:
        """
        return str('Q{}:{}'.format(self.number, self.grid.name))

    def __str__(self):
        return "Queen: {}, Grid: {}".format(self.number, self.grid)
