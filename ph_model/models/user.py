# coding=utf-8
"""User Model"""
from django.db import models
from django.contrib.auth import models as django_user
from phonenumber_field import modelfields as phone_model_field

import base_model


class User(base_model.BaseModel):
    """
    Attributes:

    username: Username
    userInfo: A one to one Django user associated with this user, so that we can use all the django auth module
    type: User type one of USER_TYPE_CHOICES.
    phoneNumber: Users phone number.
    """
    USER_TYPE_CHOICES = (
        ('TECHNICIAN', 'O&M Technician'),
        ('SUPERVISOR', 'O&M Supervisor'),
        ('CORPORATE', 'O&M Corporate'),
        ('ADMIN', 'Admin')
    )
    userInfo = models.OneToOneField(django_user.User)
    phoneNumber = phone_model_field.PhoneNumberField()
    type = models.CharField(max_length=20, choices=USER_TYPE_CHOICES, default='TECHNICIAN')

    def __str__(self):
        return "{} - {} - {}".format(self.id, self.userInfo.username, self.type)
