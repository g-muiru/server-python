import decimal
import enum
from django.db import models
import powerstation

import base_device_model
from ph_util import mixins


class Status(mixins.PairEnumMixin, enum.Enum):
    """BatteryBank actual status."""
    ERROR = -1
    OK = 1


class BatteryBank(base_device_model.DeviceModel):
    """
    Attributes:
        deviceId: Batterybank identifier.
        capacity: Battery capacity in kwh.
        stateOfCharge: State of the battery in percentile.
        numBatteries: Number of batteries in the battery bank.
        powerstaiton: Powerstation associated to the battery.
    """
    deviceId = models.CharField(max_length=50, unique=True)
    capacity = models.DecimalField(max_digits=base_device_model.MAX_DIGITS,
                                   decimal_places=base_device_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    stateOfCharge = models.DecimalField(max_digits=base_device_model.MAX_DIGITS,
                                        decimal_places=base_device_model.DECI_PLACES, default=decimal.Decimal('0.00'))
    numBatteries = models.IntegerField(default=24, null=True, blank=True)
    powerstation = models.ForeignKey(powerstation.PowerStation, related_name='battery_banks')


    @property
    def get_name(self):
        return str('B{}:'.format(self.stateOfCharge))

    def __str__(self):
        return "Capacity: {}-kwh, State of Charge:{}%".format(self.capacity, self.stateOfCharge)

