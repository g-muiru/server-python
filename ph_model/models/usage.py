"""Usage model.

   Classes:
       Usage: usage sent from circuit configured with customer.
       NonCustomerUsage: Usage not configured with customer.
       """
import decimal
import enum
from django.db import models
import base_model
import circuit
import customer
import fields
from ph_util import mixins
from django.db.models import Q

from ph_util.test_utils import test_mode


class USAGE_TYPE(mixins.PairEnumMixin, enum.Enum):
        """Usage type"""
        CUSTOMER = 'Circuit usages for a customer'
        NO_CUSTOMER = 'Circuit usages with out a customer'
        ALL = 'Circuit usage with or with out customer'


# TODO(estifanos): See if we can attach multiple filtering on same model manager
# limit rows to not run over the cron time limit
# sorts to guard against duplicate meter usages
class UnProcessdUsageManager(models.Manager):
    """Unprocessed usage manager."""
    def get_queryset(self):
        return super(UnProcessdUsageManager, self).get_queryset().filter(
            customer__isnull=False, processed=False, intervalWh__gt=0.0).order_by('customer_id','collectTime')[:250]

# sorts to guard against duplicate meter usages
class UnProcessdUsageManager_no_limit(models.Manager):
    """Unprocessed usage manager."""
    def get_queryset(self):
        return super(UnProcessdUsageManager_no_limit, self).get_queryset().filter(
            customer__isnull=False, processed=False).order_by('customer_id','collectTime')

# does not guard against duplicate meter usages
class CustomerUsageManager(models.Manager):
    """Customer usage manager."""
    def get_queryset(self):
        return super(CustomerUsageManager, self).get_queryset().filter(customer__isnull=False)

# does not guard against duplicate meter usages
class NonCustomerUsageManager(models.Manager):
    """Non customer  usage manager."""
    def get_queryset(self):
        return super(NonCustomerUsageManager, self).get_queryset().filter(customer__isnull=True)


class UsageBase(base_model.BaseModel):
    """Abstract usage base class.

    Attributes:
        customer: Customer associated with the usage.
        intervalWh: energy consumed over the last measurement interval defaults to 15 minutes.
        intervalVAmax: peak power during the last measurement interval.
        intervalVmin: minimum  voltage during the last measurement interval.
        collectTime: Usage collect time, in UTC.
        circuitStatus: Circuit status associated to the usage.
        processed: Processed query manager.
        circuit: Usage associated to circuit.
        unprocessed: Unprocessed query manager.
    """
    intervalWh = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                     default=decimal.Decimal('0.00'))
    intervalVAmax = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                        default=decimal.Decimal('0.00'))
    intervalWhH = models.DecimalField(max_digits = base_model.MAX_DIGITS, decimal_places = base_model.DECI_PLACES, default = decimal.Decimal('0.00'))
    intervalVAmaxH = models.DecimalField(max_digits = base_model.MAX_DIGITS, decimal_places = base_model.DECI_PLACES, default = decimal.Decimal('0.00'))
    intervalVmin = models.DecimalField(max_digits=base_model.MAX_DIGITS, decimal_places=base_model.DECI_PLACES,
                                       default=decimal.Decimal('0.00'))
    
    collectTime = fields.DateTimeUTC()
    processed= models.BooleanField(default=False, db_index=True)
    circuit = models.ForeignKey(circuit.Circuit)


    class Meta:
        abstract = True


class Usage(UsageBase):
    """
    Attributes:
        customer: Customer associated with the usage.
    """
    customer = models.ForeignKey(customer.Customer, null=True, blank=True)
    customer_usages = CustomerUsageManager()
    non_customer_usages = NonCustomerUsageManager()
    if test_mode():  # pragma: no cover
        unprocessed = UnProcessdUsageManager_no_limit()
    else:
        unprocessed = UnProcessdUsageManager()
    unprocessed_no_limit = UnProcessdUsageManager_no_limit()


    class Meta:
        index_together = [
            ["customer", "processed"],
        ]
    def __str__(self):
        return "circuit: {}, Collect time: {}".format(self.circuit, self.collectTime)


#@Deprecated(in favor or Usage)
class NonCustomerUsage(UsageBase):
    """
    circuit usage not associated with a customer.

    Attributes:
        circuit: Circuit associated with the customer.
    """
    def __str__(self):
        return "Circuit: {}, Collect time: {}".format(self.circuit, self.collectTime)
