import logging
from django.core.management import call_command
from django import test
from django.db import connection
from django.db import  DatabaseError
#from django.db.models.loading import load_app
from django.apps import apps
from django.core.management import sql as django_sql
from django.core.management.color import no_style

LOGGER = logging.getLogger(__name__)

class TestCase(test.TestCase):
    initiated = False

    @classmethod
    def setUpClass(cls, *args, **kwargs):
        #if not TestCase.initiated:
        #    TestCase.create_models_from_app('ph_model')
        #    TestCase.initiated = True

        super(TestCase, cls).setUpClass(*args, **kwargs)

    @classmethod
    def create_models_from_app(cls, app_name):
        """
        Manually create Models (used only for testing) from the specified string app name.
        Models are loaded from the module "<app_name>.models"
        """
        app = apps.get_app_config(app_name).models_module
        sql = django_sql.sql_create(app, no_style(), connection)  ## WTF this is obviously deprecated
        cursor = connection.cursor()
        for statement in sql:
            try:
                cursor.execute(statement)
            except DatabaseError, ex:
                LOGGER.debug(ex.message)


class BaseTestModel(TestCase):
    """Base model for model test classes"""
    pass
