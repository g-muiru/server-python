# coding=utf-8
"""Test powerhive models"""
import csv

from django.core.management import call_command
import decimal
import ph_model.models as ph_model
import base_test as base_test
import os
from ph_model.models import Customer
from ph_operation.tariff_manager import TimeTariff
from ph_util.accounting_util import AccountingEnums
from ph_util.date_util import str_to_dt
from ph_util.loan_utils import mth_mobile_balance


class BaseModelTestCase(base_test.BaseTestModel):
    """Tests ph_model.base_model.BaseModel"""

    @classmethod
    def setUp(cls):
        """Class setup"""
        call_command('loaddata', 'ph_model/fixtures/common/json/net_work_service_provider.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/tests/fixtures/initial_data.json', verbosity=0)
        mth_mobile_balance(decimal.Decimal(12345678901.1234), 1)

    def test_initialization(self):
        """Base Model should not be instantiable."""
        # noinspection PyProtectedMember
        self.assertTrue(ph_model.base_model.BaseModel._meta.abstract)

    def test_load_topology_fixtures(self):
        """Base Model, extending from base_test.BaseTestModel should load topology test data."""
        self.assertEquals(ph_model.country.Country.objects.all().count(), 1)
        self.assertEquals(ph_model.region.Region.objects.all().count(), 1)
        self.assertEquals(ph_model.project.Project.objects.all().count(), 1)

        self.assertEquals(ph_model.municipality.Municipality.objects.all().count(), 1)

    def test_load_hardware_fixtures(self):
        """Base Model, extending from base_test.BaseTestModel should load hardware test data."""
        self.assertEquals(ph_model.powerstation.PowerStation.objects.all().count(), 1)
        self.assertEquals(ph_model.inverter.Inverter.objects.all().count(), 2)
        self.assertEquals(ph_model.generation.Generation.objects.all().count(), 1)
        self.assertEquals(ph_model.grid.Grid.objects.all().count(), 1)
        self.assertEquals(ph_model.queen.Queen.objects.all().count(), 2)
        self.assertEquals(ph_model.probe.Probe.objects.all().count(), 2)
        self.assertEquals(ph_model.circuit.Circuit.objects.all().count(), 5)
        self.assertEquals(ph_model.event.Event.objects.all().count(), 2)
        self.assertEquals(ph_model.event.EventDetail.objects.all().count(), 1)
        self.assertEquals(ph_model.battery.BatteryBank.objects.all().count(), 1)

    def test_events_load_fixture(self):
        """Test combination of updated event types,
        helps to capture inconsistency on introducing registering new eventtype."""

        event_type_counts_in_db = ph_model.event.EventType.objects.all().count()
        project_path = os.path.abspath(os.path.dirname(__name__))
        event_type_csv_path = "{}/ph_model/fixtures/common/csv/event_type.csv".format(project_path)
        with open(event_type_csv_path) as csvfile:
            reader = csv.DictReader(csvfile)
            rows = list(reader)
            total_rows = len(rows)

        event_type_constants = len(ph_model.event.EventTypeConst.get_keys())
        self.assertEqual(event_type_counts_in_db, event_type_constants)
        self.assertEqual(event_type_counts_in_db, total_rows)

    def test_load_hardware_monitor_fixture(self):
        """Base Model, extending from base_test.BaseTestModel should load hardware monitoring history."""
        self.assertEquals(ph_model.circuit.SwitchEnabledHistory.objects.all().count(), 1)

    def test_load_configuration_fixtures(self):
        """Base Model, extending from base_test.BaseTestModel should load configuration test data."""
        self.assertEquals(ph_model.queen.QueenFirmware.objects.all().count(), 1)

    def test_load_om_user_fixtures(self):
        """Base Model, extending from base_test.BaseTestModel should load O&M user test data."""
        self.assertEquals(ph_model.permission.Permission.objects.all().count(), 2)
        self.assertEquals(ph_model.user.User.objects.all().count(), 2)

    def test_account_load_with_decimal_precision(self):
        """Check account balance"""
        self.assertEquals(ph_model.account.Account.objects.all().count(), 2)
        account_balance = ph_model.account.Account.objects.get(id=1).accountBalance
        self.assertEqual(decimal.Decimal('12345678901.12340000000000000000'), account_balance)

    def test_load_customer_usage_fixtures(self):
        """Base Model, extending from base_test.BaseTestModel should load customer and usage test data."""
        self.assertEquals(ph_model.account.PhoneAccount.objects.all().count(), 2)
        self.assertEquals(ph_model.account.AccountRule.objects.all().count(), 1)
        self.assertEquals(ph_model.loan.Loan.objects.all().count(), 1)
        from django.db import connection
        if connection.vendor == 'postgresql':
            self.assertEquals(ph_model.loan.LoanPaymentHistory.objects.all().count(), 3)
        else:
            self.assertEquals(ph_model.loan.LoanPaymentHistory.objects.all().count(), 2)
        self.assertEquals(ph_model.customer.Customer.objects.all().count(), 2)
        self.assertEquals(ph_model.tariff.Tariff.objects.all().count(), 1)
        self.assertEquals(ph_model.tariff.TariffCalendar.objects.all().count(), 3)
        self.assertEquals(ph_model.tariff.TariffSegment.objects.all().count(), 4)
        self.assertEquals(ph_model.transaction.MobilePayment.objects.all().count(), 2)
        self.assertEquals(ph_model.transaction.UsageCharge.objects.all().count(), 1)
        self.assertEquals(ph_model.transaction.CreditAdjustmentHistory.objects.all().count(), 1)
        self.assertEquals(ph_model.usage.Usage.customer_usages.all().count(), 2)
        self.assertEquals(ph_model.usage.Usage.non_customer_usages.all().count(), 1)

    def test_sms(self):
        """Base Model, extending from base_test.BaseTestModel sms test data."""
        self.assertEquals(ph_model.sms.SMS.objects.all().count(), 1)
        self.assertEquals(ph_model.sms.SMSServiceProvider.objects.all().count(), 1)
        self.assertEquals(ph_model.sms.PowerhiveSMSAccount.objects.all().count(), 1)

    def test_load_monitors(self):
        """Base Model, extending from base_test.BaseTestModel should load monitor test data."""
        self.assertEquals(ph_model.monitor.BatteryBankMonitor.objects.all().count(), 1)


# TODO why is this commented out ?
'''
class DateTimeUTCTestCase(base_test.BaseTestModel):
    """Test custom  UTC DB Field."""
    def test_nullified_field(self):
       m = models.DateTimeTestModel()
       try:
          m.save()
          self.fail("Required Field should fail")
       except db.IntegrityError:
          pass

    def test_naive_date_time(self):
        unaware = datetime.datetime(2011, 8, 15, 8, 15, 12, 0)
        m= models.DateTimeTestModel(requiredDateTime=unaware)
        m.save()
        self.assertTrue(timezone.is_naive(unaware))
        self.assertTrue(timezone.is_aware(m.requiredDateTime))

        aware = pytz.utc.localize(unaware)
        self.assertEquals(m.requiredDateTime, aware)

    def test_aware_date_time(self):
        aware = datetime.datetime(2011, 8, 15, 8, 15, 12, 0, pytz.UTC)
        m= models.DateTimeTestModel(requiredDateTime=aware)
        m.save()
        self.assertEquals(m.requiredDateTime, aware)

    def test_auto_add_now_support(self):
        aware = datetime.datetime(2011, 8, 15, 8, 15, 12, 0, pytz.UTC)
        m= models.DateTimeTestModel(requiredDateTime=aware)
        utcNow = datetime.datetime.utcnow().replace(tzinfo = pytz.utc)
        m.save()
        savedAutoAddNow = m.autoAddNow
        savedAutoNow = m.autoNow

        m.requiredDateTime = utcNow
        m.save()
        self.assertEqual(m.autoAddNow, savedAutoAddNow)
        self.assertNotEqual(m.autoNow, savedAutoNow)

    def test_to_python(self):
        # YYYY-MM-DDThh:mm:ss.sTZD complete datetime format
        d = ph_model.fields.DateTimeUTC()
        awareDateTime = d.to_python( "2014-10-9T8:7:6.0Z")
        self.assertIs(type(awareDateTime), datetime.datetime)
        self.assertTrue(timezone.is_aware(awareDateTime))
        self.assertEqual(awareDateTime, datetime.datetime(2014, 10, 9, 8, 7, 6, 0, pytz.UTC))

'''


class MasterTransactionHookTestCase(BaseModelTestCase):
    """Test custom  UTC DB Field."""

    @classmethod
    def setUp(cls):
        """Class setup"""
        super(MasterTransactionHookTestCase, cls).setUp()
        ph_model.transaction.MasterTransactionHistory.objects.all().delete()
        cls.account = ph_model.account.Account.objects.get(id=1)
        cls.phoneAccount = cls.account.phoneaccount_set.all()[0]
        cls.accountBalance = cls.account.accountBalance
        cls.processedTime = ph_model.fields.DateTimeUTC().to_python("2014-10-9T8:7:6.0Z")
        cls.scratchcard = ph_model.scratchcard.Scratchcard.objects.get(id=1)
        cls.user = ph_model.user.User.objects.get(id=1)
        cls.usage = ph_model.usage.Usage.objects.get(id=1)
        cls.loan = ph_model.loan.Loan.objects.get(id=1)

    def test_master_transaction_history_mobile_payment(self):
        """Add a mobile payment transaction and validate"""
        ph_model.transaction.MobilePayment.objects.all().delete()
        ph_model.transaction.MobilePayment(
            account=self.account, transactionId="MP12345",
            accountBalanceBefore=self.account.accountBalance,
            amount=50, accountBalanceAfter=50 + self.accountBalance, processedTime=self.processedTime).save()
        self.assertEqual(1, ph_model.transaction.MasterTransactionHistory.objects.count())
        mp_master_record = ph_model.transaction.MasterTransactionHistory.objects.all()[0]
        self.assertEquals(mp_master_record.transactionType, ph_model.transaction.TransactionType.CREDIT.value)
        self.assertEquals(mp_master_record.source, ph_model.transaction.TransactionSource.MOBILE_PAYMENT.value)
        self.assertEquals(mp_master_record.sourceId, ph_model.transaction.MobilePayment.objects.all()[0].id)
        self.assertEquals(mp_master_record.account, self.account)

    def test_master_transaction_history_scratchcard_payment_history(self):
        """Add a scratch card payment and validate"""
        ph_model.transaction.ScratchcardPaymentHistory.objects.all().delete()
        ph_model.transaction.ScratchcardPaymentHistory(account=self.account, scratchcard=self.scratchcard, processedTime=self.processedTime).save()
        self.assertEqual(1, ph_model.transaction.MasterTransactionHistory.objects.count())
        mp_master_record = ph_model.transaction.MasterTransactionHistory.objects.all()[0]
        self.assertEquals(mp_master_record.transactionType, ph_model.transaction.TransactionType.CREDIT.value)
        self.assertEquals(mp_master_record.source, ph_model.transaction.TransactionSource.SCRATCHCARD_PAYMENT.value)
        self.assertEquals(mp_master_record.sourceId, ph_model.transaction.ScratchcardPaymentHistory.objects.all()[0].id)
        self.assertEquals(mp_master_record.account, self.account)

    def test_master_transaction_history_credit_adjustment_credit(self):
        """Add a credit adjustment credit and validate"""
        ph_model.transaction.CreditAdjustmentHistory.objects.all().delete()
        ph_model.transaction.CreditAdjustmentHistory(
            account=self.account, user=self.user,
            processedTime=self.processedTime,
            transactionType=ph_model.transaction.TransactionType.CREDIT.value).save()
        self.assertEqual(1, ph_model.transaction.MasterTransactionHistory.objects.count())
        mp_master_record = ph_model.transaction.MasterTransactionHistory.objects.all()[0]
        self.assertEquals(mp_master_record.transactionType, ph_model.transaction.TransactionType.CREDIT.value)
        self.assertEquals(mp_master_record.source, ph_model.transaction.TransactionSource.CREDIT_ADJUSTMENT.value)
        self.assertEquals(mp_master_record.sourceId, ph_model.transaction.CreditAdjustmentHistory.objects.all()[0].id)
        self.assertEquals(mp_master_record.account, self.account)

    def test_master_transaction_history_credit_adjustment_debit(self):
        """Add a credit adjustment debit and validate"""
        ph_model.transaction.CreditAdjustmentHistory.objects.all().delete()
        ph_model.transaction.CreditAdjustmentHistory(
            account=self.account, user=self.user,
            processedTime=self.processedTime,
            transactionType=ph_model.transaction.TransactionType.DEBIT.value).save()
        self.assertEqual(1, ph_model.transaction.MasterTransactionHistory.objects.count())
        mp_master_record = ph_model.transaction.MasterTransactionHistory.objects.all()[0]
        self.assertEquals(mp_master_record.transactionType, ph_model.transaction.TransactionType.DEBIT.value)
        self.assertEquals(mp_master_record.source, ph_model.transaction.TransactionSource.CREDIT_ADJUSTMENT.value)
        self.assertEquals(mp_master_record.sourceId, ph_model.transaction.CreditAdjustmentHistory.objects.all()[0].id)
        self.assertEquals(mp_master_record.account, self.account)

    def test_master_transaction_history_usage_charge_not_operational(self):
        """Add usage charge and validate"""
        ph_model.transaction.UsageCharge.objects.all().delete()

        cust = Customer.objects.all()[0]
        tariff_calendar = ph_model.tariff.TariffCalendar.objects.get(calendarId=cust.tariffCalendar_id)
        tariff_segment = ph_model.tariff.TariffSegment.objects.get(segmentId=cust.tariffSegment_id)
        t = TimeTariff(cust, cust.account, cust.circuit, tariff_calendar, tariff_segment, self.usage, str_to_dt('2222-06-01').date())
        self.usage.collectTime = str_to_dt('2222-01-01T00:00:00Z')
        cust.circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
        c, uc, processed = t.process_usage()
        uc.save()
        self.assertEqual(0, ph_model.transaction.MasterTransactionHistory.objects.count())

    def test_master_transaction_history_max_usage_charge_past(self):
        """Add usage charge and validate"""
        ph_model.transaction.UsageCharge.objects.all().delete()

        cust = Customer.objects.all()[0]
        tariff_calendar = ph_model.tariff.TariffCalendar.objects.get(calendarId=cust.tariffCalendar_id)
        tariff_segment = ph_model.tariff.TariffSegment.objects.get(segmentId=cust.tariffSegment_id)
        t = TimeTariff(cust, cust.account, cust.circuit, tariff_calendar, tariff_segment, self.usage, str_to_dt('2014-01-01').date())
        self.usage.collectTime = str_to_dt('2015-01-01T00:00:00Z')
        cust.circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
        c, uc, processed = t.process_usage()
        uc.save()
        self.assertEqual(0, ph_model.transaction.MasterTransactionHistory.objects.count())
        mth = ph_model.transaction.MasterTransactionHistory.objects.all()
        self.assertEquals(len(mth), 0)

    def test_master_transaction_history_usage_charge_past(self):
        """Add usage charge and validate"""
        ph_model.transaction.UsageCharge.objects.all().delete()

        cust = Customer.objects.all()[0]
        tariff_calendar = ph_model.tariff.TariffCalendar.objects.get(calendarId=cust.tariffCalendar_id)
        tariff_segment = ph_model.tariff.TariffSegment.objects.get(segmentId=cust.tariffSegment_id)
        self.usage.intervalWh = 10
        t = TimeTariff(cust, cust.account, cust.circuit, tariff_calendar, tariff_segment, self.usage, str_to_dt('2014-01-01').date())
        self.usage.collectTime = str_to_dt('2015-01-01T00:00:00Z')
        cust.circuit.switchEnabled = ph_model.circuit.SwitchEnabledStatus.ENABLED.value
        c, uc, processed = t.process_usage()
        uc.save()
        self.assertEqual(4, ph_model.transaction.MasterTransactionHistory.objects.count())
        uc_id = ph_model.transaction.UsageCharge.objects.all()[0].id
        mth = ph_model.transaction.MasterTransactionHistory.objects.all()
        self.assertEquals(mth[0].transactionType, ph_model.transaction.TransactionType.DEBIT.value)
        self.assertEquals(mth[0].source, AccountingEnums.RES_VAR_FEEc238.name)
        self.assertEquals(mth[0].sourceId, uc_id)
        self.assertEquals(mth[0].account, cust.account)
        self.assertEquals(mth[2].transactionType, ph_model.transaction.TransactionType.DEBIT.value)
        self.assertEquals(mth[2].source, AccountingEnums.RES_FIXED_FEEc236.name)
        self.assertEquals(mth[2].sourceId, uc_id)
        self.assertEquals(mth[2].account, cust.account)

    def test_master_loan_payment_history_loan_charge(self):
        """Add a loan payment and validate"""
        ph_model.loan.LoanPaymentHistory.objects.all().delete()
        ph_model.loan.LoanPaymentHistory(loan=self.loan).save()
        self.assertEqual(0, ph_model.transaction.MasterTransactionHistory.objects.count())
        ph_model.loan.LoanPaymentHistory.objects.all().delete()
        ph_model.loan.LoanPaymentHistory(loan=self.loan).save_accounting(AccountingEnums.VAT_TARIFF_FIXEDc346)
        self.assertEqual(1, ph_model.transaction.MasterTransactionHistory.objects.count())

        mp_master_record = ph_model.transaction.MasterTransactionHistory.objects.all()[0]
        self.assertEquals(mp_master_record.transactionType, ph_model.transaction.TransactionType.DEBIT.value)
        self.assertEquals(mp_master_record.source, AccountingEnums.VAT_TARIFF_FIXEDc346.name)
        self.assertEquals(mp_master_record.sourceId, ph_model.loan.LoanPaymentHistory.objects.all()[0].id)
        self.assertEquals(mp_master_record.account, self.account)
