# coding=utf-8
""" Testing tariffs  --- nodb """

import datetime
import pytz
from django.test import SimpleTestCase
from ddt import ddt, unpack, idata
from model_mommy import mommy
from model_mommy import recipe
from model_mommy.timezone import now
import ph_model.models as ph_model
from ph_operation.tariff_manager import TimeTariff


@ddt
class TariffModelTestCase(SimpleTestCase):
    """ Class to test the Tariff models """

    # data to run tests multiple times, self.cust.lastUsageProcessedTime & self.cust.municipality.timeZone
    last_dt_tz = (
        # no month or year rollover
        (datetime.datetime(2014, 1, 1, hour=23, minute=59, second=59, tzinfo=pytz.utc), 'Etc/GMT'),
        (datetime.datetime(2016, 12, 1, hour=23, minute=59, second=59, tzinfo=pytz.utc), 'Etc/GMT'),
        # month rollover
        (datetime.datetime(2014, 1, 31, hour=23, minute=59, second=59, tzinfo=pytz.utc), 'Etc/GMT'),
        (datetime.datetime(2016, 3, 31, hour=23, minute=59, second=59, tzinfo=pytz.utc), 'Etc/GMT'),
        # month and year rollover
        (datetime.datetime(2014, 12, 31, hour=23, minute=59, second=59, tzinfo=pytz.utc), 'Etc/GMT'),
        (datetime.datetime(2016, 12, 31, hour=23, minute=59, second=59, tzinfo=pytz.utc), 'Etc/GMT'),
        # same as above but with London timezone
        (datetime.datetime(2014, 1, 1, hour=23, minute=59, second=59, tzinfo=pytz.timezone('America/Los_Angeles')),
         'America/Los_Angeles'),
        (datetime.datetime(2016, 12, 1, hour=23, minute=59, second=59, tzinfo=pytz.timezone('America/Los_Angeles')),
         'America/Los_Angeles'),
        (datetime.datetime(2014, 1, 1, hour=23, minute=59, second=59, tzinfo=pytz.timezone('America/Los_Angeles')),
         'America/Los_Angeles'),
        (datetime.datetime(2016, 3, 31, hour=23, minute=59, second=59, tzinfo=pytz.timezone('America/Los_Angeles')),
         'America/Los_Angeles'),
        (datetime.datetime(2014, 12, 31, hour=23, minute=59, second=59, tzinfo=pytz.timezone('America/Los_Angeles')),
         'America/Los_Angeles'),
        (datetime.datetime(2016, 12, 31, hour=23, minute=59, second=59, tzinfo=pytz.timezone('America/Los_Angeles')),
         'America/Los_Angeles'),
        # same as above but with Nairobi timezone
        (datetime.datetime(2014, 1, 1, hour=23, minute=59, second=59, tzinfo=pytz.timezone('Africa/Nairobi')),
         'Africa/Nairobi'),
        (datetime.datetime(2016, 12, 1, hour=23, minute=59, second=59, tzinfo=pytz.timezone('Africa/Nairobi')),
         'Africa/Nairobi'),
        (datetime.datetime(2014, 1, 1, hour=23, minute=59, second=59, tzinfo=pytz.timezone('Africa/Nairobi')),
         'Africa/Nairobi'),
        (datetime.datetime(2016, 3, 31, hour=23, minute=59, second=59, tzinfo=pytz.timezone('Africa/Nairobi')),
         'Africa/Nairobi'),
        (datetime.datetime(2014, 12, 31, hour=23, minute=59, second=59, tzinfo=pytz.timezone('Africa/Nairobi')),
         'Africa/Nairobi'),
        (datetime.datetime(2016, 12, 31, hour=23, minute=59, second=59, tzinfo=pytz.timezone('Africa/Nairobi')),
         'Africa/Nairobi'))

    ts_expires = 64
    ts_id = 76543
    tc_id = 54321
    t_id = 123
    t_name = 'Tariff Name'

    def setUp(self):
        """ Set up all the tests objects"""
        self.acc = mommy.prepare(ph_model.account.Account)
        self.circ = mommy.prepare(ph_model.circuit.Circuit)

        self.ts = mommy.prepare(ph_model.tariff.TariffSegment)
        self.ts.segmentId = self.ts_id
        self.ts.timeExpires = self.ts_expires

        self.tc = mommy.prepare(ph_model.tariff.TariffCalendar)
        self.tc.calendarId = self.tc_id

        self.t = mommy.prepare(ph_model.tariff.Tariff)
        self.t.tariffId = self.t_id
        self.t.name = self.t_name
        self.t.entryTariffCalendar = self.tc

        self.region = mommy.prepare(ph_model.region.Region)

        self.muni = recipe.Recipe(
            ph_model.municipality.Municipality,
            name='Nowhere',
            timeZone='Africa/Nairobi',
            region=self.region
        ).prepare()

        self.cust = recipe.Recipe(
            ph_model.customer.Customer,
            municipality=self.muni,
            # TODO define custom fields for mommy
            phoneNumber='123-4567-1234',
            lastUsageProcessedTime=now()
        ).prepare()

        self.usage = recipe.Recipe(
            ph_model.usage.Usage,
            collectTime=now(),
            customer=self.cust
        ).prepare()

        str_t = str(self.t)
        self.assertIn('123 - Tariff Name', str_t)
        str_t = str(self.ts)
        self.assertIn('Id: 76543, type: TIME timeExpires: 64', str_t)
        str_t = str(self.tc)
        self.assertIn('54321 - Expires-None', str_t)

    @idata(last_dt_tz)
    @unpack
    def test_mid_night_passed_between_last_usage_and_collect_time(self, dt, tz):
        """
        Set collect time after midnight, last collect before midnight

        :param dt:      date time to check
        :param tz:      timezone for date time
        :return:        true if mid_night_passed is true
        """
        self.cust.lastUsageProcessedTime = dt
        self.cust.municipality.timeZone = tz
        self.usage.collectTime = self.cust.lastUsageProcessedTime + datetime.timedelta(seconds=2)
        self.tt = TimeTariff(self.cust, self.acc, self.circ, self.tc, self.ts, self.usage)
        self.assertTrue(self.tt.mid_night_passed())

    @idata(last_dt_tz)
    @unpack
    def test_mid_night_passed_first_time_usage_before_mid_night(self, dt, tz):
        """
        Set collect time before midnight, last collect None

        :param dt:      date time to check
        :param tz:      timezone for date time
        :return:        true if mid_night_passed is false
        """
        self.cust.municipality.timeZone = tz
        self.cust.lastUsageProcessedTime = None
        self.usage.collectTime = dt
        self.tt = TimeTariff(self.cust, self.acc, self.circ, self.tc, self.ts, self.usage)
        self.assertFalse(self.tt.mid_night_passed())

    @idata(last_dt_tz)
    @unpack
    def test_mid_night_passed_first_time_usage_at_mid_night(self, dt, tz):
        """
        Set collect time exctly midnight, last collect None

        :param dt:      date time to check
        :param tz:      timezone for date time
        :return:        true if mid_night_passed is false
        """
        self.cust.municipality.timeZone = tz
        self.cust.lastUsageProcessedTime = None
        self.usage.collectTime = dt + datetime.timedelta(seconds=1)
        self.tt = TimeTariff(self.cust, self.acc, self.circ, self.tc, self.ts, self.usage)
        self.assertTrue(self.tt.mid_night_passed())

    @idata(last_dt_tz)
    @unpack
    def test_mid_night_passed_exactly_at_mid_night(self, dt, tz):
        """
        Set collect time exactly midnight, last collect before midnight

        :param dt:      date time to check
        :param tz:      timezone for date time
        :return:        true if mid_night_passed is true
        """
        self.cust.lastUsageProcessedTime = dt
        self.cust.municipality.timeZone = tz
        self.usage.collectTime = self.cust.lastUsageProcessedTime + datetime.timedelta(seconds=1)
        self.tt = TimeTariff(self.cust, self.acc, self.circ, self.tc, self.ts, self.usage)
        self.assertTrue(self.tt.mid_night_passed())
