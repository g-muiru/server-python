# coding=utf-8
"""Test for the loan model and manager """
# TODO 2017-08-19 we do not charge the commercial fixed fee monthly anymore ---
#      once we are sure this will not come back, test_monthly_fixed_fee_loan can be removed
# TODO ad tests for loans with interest
from django.core.management import call_command
from django.db.models import Q
from ph_model.models import Account
from ph_model.models import Loan
from ph_model.models import LoanPaymentHistory
from ph_model.models.customer import CustomerType, Customer
from ph_model.models.transaction import MasterTransactionHistory
from ph_operation import loan_manager
from ph_operation.tests import base_test
from ph_script.monthly_commercial_tariff_fixed_fee import MonthlyCommercialTariffFixedFee
from ph_util.accounting_util import AccountingEnums, calc_vat, add_vat, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT
from ph_util.date_util import str_to_dt
from ddt import ddt, unpack, idata
from ph_util.test_utils import assert_eq


class LoanBaseTestCase(base_test.BaseTestOperation):
    def setUp(self):
        """
        Setup the test data
        """
        super(LoanBaseTestCase, self).setUp()
        call_command('loaddata', 'ph_operation/tests/fixtures/loan_config.json', verbosity=0)
        self.test_account = Account.objects.get(id=603)
        LoanPaymentHistory.objects.all().delete()
        MasterTransactionHistory.objects.all().delete()
        Loan.objects.filter(~Q(pk=601)).delete()


@ddt
class LoanInitTestCase(LoanBaseTestCase):
    """Tests model.Loan."""
    op_date_str = '2002-01-01'
    before_date_str = '2001-01-01'
    after_date_str = '2222-02-22'
    data = (
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), op_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), op_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), op_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), op_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), op_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), op_date_str, op_date_str),

        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), op_date_str, after_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), op_date_str, after_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), op_date_str, after_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), op_date_str, after_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), op_date_str, after_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), op_date_str, after_date_str),

        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), op_date_str, before_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), op_date_str, before_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), op_date_str, before_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), op_date_str, before_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), op_date_str, before_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), op_date_str, before_date_str),

        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), after_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), after_date_str, op_date_str),

        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), before_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), before_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), after_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), after_date_str, op_date_str),

        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), before_date_str, op_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), before_date_str, op_date_str),

        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), after_date_str, after_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), after_date_str, after_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), after_date_str, after_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), after_date_str, after_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), after_date_str, after_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), after_date_str, after_date_str),

        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), before_date_str, before_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), before_date_str, before_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), before_date_str, before_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), before_date_str, before_date_str),
        (100, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), before_date_str, before_date_str),
        (100, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), before_date_str, before_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), op_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), op_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), op_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), op_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), op_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), op_date_str, op_date_str),

        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), op_date_str, after_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), op_date_str, after_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), op_date_str, after_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), op_date_str, after_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), op_date_str, after_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), op_date_str, after_date_str),

        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), op_date_str, before_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), op_date_str, before_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), op_date_str, before_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), op_date_str, before_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), op_date_str, before_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), op_date_str, before_date_str),

        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), after_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), after_date_str, op_date_str),

        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), before_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), before_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), after_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), after_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), after_date_str, op_date_str),

        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), before_date_str, op_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), before_date_str, op_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), before_date_str, op_date_str),

        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), after_date_str, after_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), after_date_str, after_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), after_date_str, after_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), after_date_str, after_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), after_date_str, after_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), after_date_str, after_date_str),

        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2017-01-01'), before_date_str, before_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2017-01-01'), before_date_str, before_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt('2001-01-01'), before_date_str, before_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt('2001-01-01'), before_date_str, before_date_str),
        (5000, COMMERCIAL_II_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL_II.name,
         AccountingEnums.COM2_FIXED_FEEc232, str_to_dt(op_date_str), before_date_str, before_date_str),
        (5000, COMMERCIAL_I_FIXED_TARIFF_LOAN_AMT, CustomerType.COMMERCIAL.name,
         AccountingEnums.COM1_FIXED_FEEc231, str_to_dt(op_date_str), before_date_str, before_date_str),
    )

    @idata(data)
    @unpack
    def test_monthly_fixed_fee_loan(self, bal, amt, cust_type, loan_type, dt, op_date_str, q_op_date_str):
        """
        Validate the loan is created and paid correctly

        :param bal:         the account starting balance
        :param amt:         the fixed fee without VAT
        :param loan_type:   the type of commercial loan
        :param dt:          date of creation Note: this code only works if the day of the month < 4
        :return:
        """
        cnt = 1
        ctype = cust_type
        lt = loan_type
        fixed_src = loan_type.name

        c = Customer.objects.get(id=601)
        c.customerType = ctype
        c.circuit.queen.grid.operational_date = str_to_dt(op_date_str).date()
        c.circuit.queen.operational_date = str_to_dt(q_op_date_str).date()
        c.circuit.queen.grid.save()
        c.circuit.queen.save()
        c.account.accountBalance = bal
        c.account.save()
        c.save()

        start_balance = bal
        start_uncollect = c.account.uncollected_bal

        if dt.date() < c.circuit.queen.grid.operational_date and dt.date() < c.circuit.queen.operational_date:
            assert_eq([], MonthlyCommercialTariffFixedFee.monthly_check(dt))
        else:
            l_pk = MonthlyCommercialTariffFixedFee.monthly_check(dt)[0]
            l1 = loan_manager.Loan(Loan.objects.get(id=l_pk))
            loan, acc_bal = l1.repay_loan(c)

            assert_eq(str(lt), loan.loanType)
            assert_eq(amt, int(loan.loanAmount))

            c = Customer.objects.get(id=601)
            if start_balance < add_vat(amt):
                assert_eq(round(c.account.accountBalance, 2), round(start_balance, 2))
                assert_eq(round(c.account.uncollected_bal, 2), round((-add_vat(amt)), 2))
            else:
                assert_eq(round(c.account.accountBalance, 2), round((start_balance - add_vat(amt)), 2))
                assert_eq(round(c.account.uncollected_bal, 2), round(start_uncollect, 2))

            for mth in MasterTransactionHistory.objects.all().order_by('-id'):
                if cnt == 1:
                    assert_eq(AccountingEnums.VAT_LOAN_PAYMENTc601.name, mth.source)
                    assert_eq(round(calc_vat(amt), 2), round(mth.amount, 2))
                if cnt == 2:
                    assert_eq(AccountingEnums.LOAN_PAYMENTc600.name, mth.source)
                    assert_eq(round(amt, 2), round(mth.amount, 2))
                if cnt == 3:
                    assert_eq(AccountingEnums.VAT_TARIFF_FIXEDc346.name, mth.source)
                    assert_eq(round(calc_vat(amt), 2), round(mth.amount, 2))
                if cnt == 4:
                    assert_eq(fixed_src, mth.source)
                    assert_eq(round(amt, 2), round(mth.amount, 2))
                cnt += 1

        c.circuit_id = None
        c.save()
        assert_eq([], MonthlyCommercialTariffFixedFee.monthly_check(dt))
        assert_eq([], MonthlyCommercialTariffFixedFee.monthly_check(str_to_dt(self.after_date_str)))
