# coding=utf-8
"""Admin functionality"""
from django.core import urlresolvers
from django.contrib import admin
from django.contrib.auth import models as django_user
from django_cron.models import CronJobLog

import ph_model.models as ph_model
from functools import wraps
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

# noinspection PyPackageRequirements
from pygments import highlight
# noinspection PyUnresolvedReferences,PyPackageRequirements
from pygments.lexers import JsonLexer
# noinspection PyUnresolvedReferences,PyPackageRequirements
from pygments.formatters import HtmlFormatter

from django.utils.safestring import mark_safe
from import_export import resources
from import_export.admin import ExportMixin
from import_export import fields
import json
import logging

LOGGER = logging.getLogger(__name__)

LINK_TEMPLATE = '<a href="{}">{}</a>'


def get_referred_model_url(model_name, referred_as=None, display=None, short_description=None, admin_url=None):
    """
    Decorator to format foreign key links.

    E.g Grid to Queen has one to many relation ship, on the queen admin display we want to
    link to grid with formatted url. The link display will be one of the properties in the referred model(grid)
    if not provide we will use __str__ notation.

    :param model_name: String referred model name.
    :param referred_as: String property name in the referee.
    :param display: String one of the property to use as a link name.
    :param short_description: Column label on model admin table.
    :param admin_url: Url string, this will be used instead of the default admin edit url.
    :rtype: object
    Returns: Formatted url http://hostname/adminph_model/grid/1/.
    """

    def func_wrapper(func):
        """

        :param func:
        :return:
        """
        func.allow_tags = True
        func.short_description = short_description or model_name

        # noinspection PyUnusedLocal,PyUnusedLocal,PyPep8Naming
        @wraps(func)
        def _wrapper(modelAdmin, obj, **kwargs):
            """

            :param modelAdmin:
            :param obj:
            :param kwargs:
            :return:
            """
            admin_change_url = admin_url or 'admin:ph_model_{}_change'.format(model_name)
            model = getattr(obj, referred_as or model_name)
            pk = getattr(model, 'pk', None)
            if not pk:
                return " "
            if not display:
                url_display = str(model)
            else:
                url_display = getattr(model, display)
            url = urlresolvers.reverse(admin_change_url, args=(pk,))
            return LINK_TEMPLATE.format(url, url_display)

        return _wrapper

    return func_wrapper


class BaseAdmin(admin.ModelAdmin):
    """Base admin class"""
    pass


class JSONMixin(object):
    """Json Support"""

    @staticmethod
    def prettify(response):
        """Function to display pretty version of our data

        :param response:
        :return:
        """

        # Truncate the data. Alter as needed
        response = response[:5000]

        # Get the Pygments formatter
        formatter = HtmlFormatter(style='colorful')

        # Highlight the data
        response = highlight(response, JsonLexer(), formatter)

        # Get the stylesheet
        style = "<style>" + formatter.get_style_defs() + "</style><br>"

        # Safe the output
        return mark_safe(style + response)

    def data_prettified(self, instance):
        """ Convert the data to sorted, indented JSON

        :param instance:
        :return:
        """
        response = json.dumps(instance.data, sort_keys=True, indent=2)
        return self.prettify(response)

    def form_data_prettified(self, instance):
        """

        :param instance:
        :return:
        """
        response = json.dumps(instance.form_data, sort_keys=True, indent=2)
        return self.prettify(response)

    def metadata_prettified(self, instance):
        """

        :param instance:
        :return:
        """
        response = json.dumps(instance.metadata, sort_keys=True, indent=2)
        return self.prettify(response)

    form_data_prettified.short_description = 'form data'
    data_prettified.short_description = 'data'
    metadata_prettified.short_description = 'metadata'


class ReadOnlyAdmin(BaseAdmin):
    """Disallow editing"""
    actions = None

    enable_change_view = False

    def get_list_display_links(self, request, list_display):
        """
        Return a sequence containing the fields to be displayed as links
        on the changelist. The list_display parameter is the list of fields
        returned by get_list_display().

        We override Django's default implementation to specify no links unless
        these are explicitly set.

        :param request:
        :param list_display:
        :return:
        """
        if self.list_display_links or not list_display:
            return self.list_display_links
        else:
            return None

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """
        The 'change' admin view for this model.

        We override this to redirect back to the changelist unless the view is
        specifically enabled by the "enable_change_view" property.

        :param request:
        :param object_id:
        :param form_url:
        :param extra_context:
        :return:
        """
        if self.enable_change_view:
            return super(ReadOnlyAdmin, self).change_view(
                request,
                object_id,
                form_url,
                extra_context
            )
        else:
            # noinspection PyProtectedMember
            opts = self.model._meta
            url = reverse('admin:{app}_{model}_changelist'.format(
                app=opts.app_label,
                model=opts.model_name,
            ))
            return HttpResponseRedirect(url)

    def has_add_permission(self, request):
        """

        :param request:
        :return:
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """

        :param request:
        :param obj:
        :return:
        """
        return False


class EditableAdmin(BaseAdmin):
    """Allow editing"""
    pass


class CountryAdmin(EditableAdmin):
    """Location/Project related database admins."""
    pass


class RegionAdmin(EditableAdmin):
    """Edit region"""
    list_display = ('name', 'country_link')

    @get_referred_model_url('country', referred_as='country', display='name')
    def country_link(self, obj):
        """

        :param obj:
        """
        pass


class MunicipalityAdmin(EditableAdmin):
    """Edit municipality"""
    list_display = ('name', 'region_link', 'timeZone')

    @get_referred_model_url('region', referred_as='region', display='name')
    def region_link(self, obj):
        """

        :param obj:
        """
        pass


class ProjectAdmin(EditableAdmin):
    """edit project"""
    list_display = ('id', 'name',)

    pass


class UserProfileAdmin(admin.StackedInline):
    """Powerhive users related database admins"""
    model = ph_model.user.User


class DjangoUserAdmin(EditableAdmin):
    """edit users"""
    fields = ('username', 'password', 'first_name', 'last_name', 'email')
    list_display = ('username', 'user')
    readonly_fields = ('password',)
    inlines = (UserProfileAdmin,)


class PermissionAdmin(EditableAdmin):
    """edit permissions"""
    list_display = ('user', 'role', 'itemId', 'itemName', 'itemType')
    list_filter = ('user',)

    # noinspection PyPep8Naming
    def itemName(self, obj):
        """

        :param obj:
        :return:
        """
        if obj.itemType == 'GRID':
            grid = ph_model.grid.Grid.objects.get(pk=obj.itemId)
            # noinspection PyCompatibility
            return grid
        elif obj.itemType == 'PROJECT':
            project = ph_model.project.Project.objects.get(pk=obj.itemId)
            # noinspection PyCompatibility
            return project

        else:
            return None

    itemName.short_description = 'item name'


class _HardwareAdmin(EditableAdmin):
    """Hardware related database admins."""
    readonly_fields = ('highestUnclearedEvent',)


class GridAdmin(_HardwareAdmin):
    """edit grid"""
    list_display = ('status', 'id', 'highestUnclearedEvent', 'name', 'project_link', 'municipality_link')

    @get_referred_model_url('project', display='name', referred_as='project')
    def project_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('municipality', display='name', referred_as='municipality')
    def municipality_link(self, obj):
        """

        :param obj:
        """
        pass


class PowerStationAdmin(_HardwareAdmin):
    """edit power station"""
    list_display = ('name', 'latitude', 'longitude', 'grid_link', 'queen_link', 'deviceStatus')
    list_filter = ('grid', 'queen')

    @get_referred_model_url('grid')
    def grid_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('queen')
    def queen_link(self, obj):
        """

        :param obj:
        """
        pass


class InverterAdmin(_HardwareAdmin):
    """edit inverters"""
    list_display = ('deviceId', 'type', 'powerstation_link', 'deviceStatus')
    search_fields = ('deviceId',)

    @get_referred_model_url('powerstation')
    def powerstation_link(self, obj):
        """

        :param obj:
        """
        pass


class BatteryBankAdmin(_HardwareAdmin):
    """edit batteries"""
    list_display = ('id', 'deviceId', 'capacity', 'stateOfCharge', 'numBatteries', 'powerstation_link', 'deviceStatus')
    list_filter = ('powerstation',)
    search_fields = ('deviceId',)

    @get_referred_model_url('powerstation')
    def powerstation_link(self, obj):
        """

        :param obj:
        """
        pass


class QueenFirmwareAdmin(ReadOnlyAdmin):
    """firmware is read only"""
    pass


class QueenAdmin(_HardwareAdmin):
    """edit queen"""
    list_display = ('deviceId', 'number', 'grid_link', 'status', 'lastContact', 'firstContactDate',
                    'current_firmware_link', 'expected_firmware_link', 'lanAddress', 'wanAddress', 'verbosity',
                    'usageInterval',
                    'generationInterval', 'monitorInterval', 'deviceStatus', 'reboot', 'osStarted', 'fwStarted',
                    'fsFreeStatus', 'fsFreePct')
    list_filter = ('grid',)
    search_fields = ('deviceId',)

    @get_referred_model_url('grid', display='name')
    def grid_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('queenfirmware', display='version', referred_as='currentFirmware')
    def current_firmware_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('queenfirmware', display='version', referred_as='expectedFirmware')
    def expected_firmware_link(self, obj):
        """

        :param obj:
        """
        pass


class ProbeAdmin(_HardwareAdmin):
    """edit probe"""
    list_display = ('deviceId', 'queen_link', 'deviceStatus')
    list_filter = ('queen',)

    @get_referred_model_url('queen', display='deviceId')
    def queen_link(self, obj):
        """

        :param obj:
        """
        pass


class CircuitAdmin(_HardwareAdmin):
    """edit circuit"""
    list_display = ('deviceId', 'number', 'queen_link', 'probe_link', 'switchEnabled', 'overrideSwitchEnabled',
                    'enableBalanceServer', 'whLimit', 'vaLimit', 'deviceStatus')
    list_filter = ('queen',)
    fieldsets = (
        (None, {'fields': ('deviceId', 'number', 'queen', 'probe', 'whLimit', 'vaLimit', 'deviceStatus', 'faultCount',
                           'meterIssueCount', 'highestUnclearedEvent'),
                }),
        ("Circuit Toggle", {'fields': ('overrideSwitchEnabled', 'enableBalanceServer', 'switchEnabled',),
                            'description': "To enable circuit: enableBalanceServer == Yes OR overrideSwitchEnabled == Yes <br/>\
                                To disable circuit: enableBalanceServer == No AND overrideSwitchEnabled == No <br/>\
                                switchEnabled is firmware state reported from queen"
                            }),
    )
    # switchEnabled is set by the queen firmware and reflects queen state, and should never be changed manually
    readonly_fields = ['switchEnabled', 'faultCount', 'meterIssueCount', 'highestUnclearedEvent']
    search_fields = ('deviceId',)

    @get_referred_model_url('queen', display='deviceId')
    def queen_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('probe', display='number')
    def probe_link(self, obj):
        """

        :param obj:
        """
        pass


class SwitchEnabledHistoryAdmin(EditableAdmin):
    """edit switch"""
    list_display = ('circuit_link', 'switchEnabled', 'enableBalanceServer', 'disableVaLimitQueen',
                    'accountBalance', 'serverRequestEnabled', 'overrideSwitchEnabled', 'reason', 'created', 'updated')
    search_fields = ('circuit__deviceId',)

    list_filter = ('circuit__queen',)

    @get_referred_model_url('circuit', display='deviceId')
    def circuit_link(self, obj):
        """

        :param obj:
        """
        pass


class PriorityCircuitAdmin(EditableAdmin):
    """edit switch"""
    raw_id_fields = ('circuit',)
    list_display = ('id', 'circuit_link', 'circuitType', 'notified')
    list_filter = ('circuit__queen',)

    @get_referred_model_url('circuit', display='deviceId')
    def circuit_link(self, obj):
        """

        :param obj:
        """
        pass


class GenerationAdmin(ReadOnlyAdmin):
    """read only generation"""
    list_display = ('grid_link', 'whFromPV', 'whToGrid', 'vaFromPVMin', 'vaFromPVAvg', 'vaFromPVMax',
                    'vaToGridMin', 'vaToGridAvg', 'vaToGridMax', 'vacMin', 'vacAvg',
                    'vacMax', 'socMin', 'socAvg', 'socMax', 'collectTime', 'generationInterval', 'processed')
    list_filter = ('grid', 'processed')

    @get_referred_model_url('grid')
    def grid_link(self, obj):
        """

        :param obj:
        """
        pass


class EventDetailInlineAdmin(admin.TabularInline):
    """event admin"""
    model = ph_model.EventDetail
    readonly_fields = ['id', 'itemName', 'itemValue']


class EventTypeAdmin(EditableAdmin):
    """event type admin"""
    list_display = ('eventNumber', 'itemType', 'notifyImmediate', 'eventDescription', 'severity')


def dehydrate_detail(obj):
    """

    :param obj:
    :return:
    """
    eventdetails = ph_model.EventDetail.objects.filter(event=obj)
    if eventdetails.count() == 0:
        return '(None)'
    # noinspection PyCompatibility
    output = ', '.join([unicode(eventdetail.itemValue) for eventdetail in eventdetails])
    return output


class EventResource(resources.ModelResource):
    """ resource definitions for exporter """
    detail = fields.Field()

    class Meta:
        """event resource meta"""
        model = ph_model.Event
        fields = ('id', 'eventType', 'collectTime', 'itemId', 'detail')
        export_order = ('id', 'eventType', 'collectTime', 'itemId', 'detail')


class EventAdmin(ExportMixin, EditableAdmin):
    """edit events"""
    list_display = ('id', 'event_type_link', 'event_detail', 'itemId', 'collectTime')
    readonly_fields = ['id', 'eventType', 'collectTime', 'itemId', 'expiredAfter', 'isExpired']
    list_filter = ('collectTime', 'eventType')
    search_fields = ('itemId',)
    inlines = [
        EventDetailInlineAdmin,
    ]
    resource_class = EventResource

    @get_referred_model_url('eventtype', referred_as='eventType', display='eventDescription')
    def event_type_link(self, obj):
        """

        :param obj:
        """
        pass

    def event_detail(self, obj):
        """

        :param obj:
        :return:
        """
        eventdetails = ph_model.EventDetail.objects.filter(event=obj)
        if eventdetails.count() == 0:
            return '(None)'
        # noinspection PyCompatibility
        output = ', '.join([unicode(eventdetail.itemValue) for eventdetail in eventdetails])
        return output

    event_detail.short_description = 'details'


class EventDetailAdmin(ReadOnlyAdmin):
    """event details read only"""
    list_display = ('id', 'event_link', 'itemName', 'itemValue')
    list_filter = ('event',)

    @get_referred_model_url('event', referred_as='event')
    def event_link(self, obj):
        """

        :param obj:
        """
        pass


# noinspection PyBroadException
class CustomerResource(resources.ModelResource):
    """ resource definitions for exporter """
    balance = fields.Field()
    municipality = fields.Field()
    account = fields.Field()
    circuit = fields.Field()
    queen = fields.Field()

    class Meta:
        """customer resource meta"""
        model = ph_model.Customer
        fields = ('customerId', 'firstName', 'lastName', 'status', 'phoneNumber', 'email', 'connectionFeeLoan',
                  'connectionFeeDue', 'latitude', 'longitude', 'lastUsageProcessedTime',
                  'cumulativeDailyUsageWh', 'cumulativeKwh', 'dailySMSBalRequested')
        export_order = (
            'customerId', 'account', 'circuit', 'queen', 'firstName', 'lastName', 'status', 'balance', 'phoneNumber',
            'municipality',
            'connectionFeeLoan', 'connectionFeeDue', 'latitude', 'longitude', 'email', 'lastUsageProcessedTime',
            'cumulativeDailyUsageWh', 'cumulativeKwh', 'dailySMSBalRequested')

    @staticmethod
    def dehydrate_balance(obj):
        """

        :param obj:
        :return:
        """
        return round(obj.account.accountBalance, 2)

    @staticmethod
    def dehydrate_municipality(obj):
        """

        :param obj:
        :return:
        """
        return obj.municipality.name

    @staticmethod
    def dehydrate_queen(obj):
        """

        :param obj:
        :return:
        """
        # noinspection PyPep8
        try:
            return obj.circuit.queen
        except:
            return None

    @staticmethod
    def dehydrate_account(obj):
        """

        :param obj:
        :return:
        """
        return obj.account.id

    @staticmethod
    def dehydrate_circuit(obj):
        """

        :param obj:
        :return:
        """
        if obj.circuit:
            return obj.circuit.deviceId
        else:
            pass


# Customer related admins
class CustomerAdmin(ExportMixin, EditableAdmin):
    """customer edit and export"""
    list_display = (
        'customerId', 'account_link', 'queen', 'firstName', 'lastName', 'email', 'phone_account', 'phoneNumber', 'municipality', 'contract_link',
        'circuit_link', 'status', 'balance', 'connectionFeeLoan', 'connectionFeeDue', 'tariff_link',
        'tariff_calendar_link', 'tariff_segment_link', 'latitude', 'longitude', 'lastUsageProcessedTime',
        'cumulativeDailyUsageWh', 'cumulativeKwh', 'dailySMSBalRequested')
    readonly_fields = ('customerId', 'contract_link', 'customerSurvey')
    exclude = ('serviceContract',)
    list_filter = ('status', 'connectionFeeDue', 'municipality')
    search_fields = ('customerId', '=account__id', 'phoneNumber', 'circuit__deviceId', 'firstName', 'lastName')
    resource_class = CustomerResource

    @get_referred_model_url('fulcrumrecord', referred_as='serviceContract', display='id',
                            short_description='Service Contract')
    def contract_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('customerSurvey')
    def survey_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('account')
    def account_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('circuit', display='deviceId')
    def circuit_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('tariff', display='name')
    def tariff_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('tariffcalendar', referred_as='tariffCalendar')
    def tariff_calendar_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('tariffsegment', referred_as='tariffSegment')
    def tariff_segment_link(self, obj):
        """

        :param obj:
        """
        pass

    def phone_account(self, obj):
        """
        Set PhoneAccount url

        :param obj: PhoneAccount
        :return:    htm template or None
        """
        phone_accounts = ph_model.PhoneAccount.objects.filter(account=obj.account.id)
        if phone_accounts:
            urls = []
            for phone_account in phone_accounts:
                admin_change_url = 'admin:ph_model_{}_change'.format('phoneaccount')
                url = urlresolvers.reverse(admin_change_url, args=(phone_account.id,))
                urls.append(LINK_TEMPLATE.format(url, phone_account.id))
            return urls
        else:
            return None

    phone_account.allow_tags = True

    def queen(self, obj):
        """
        Queen link
        :param obj:
        :return:
        """
        # noinspection PyPep8,PyBroadException
        try:
            if obj.circuit.queen:
                admin_change_url = 'admin:ph_model_{}_change'.format('queen')
                url = urlresolvers.reverse(admin_change_url, args=(obj.circuit.queen.id,))
                url = LINK_TEMPLATE.format(url, obj.circuit.queen)
                return url
            else:
                return None
        except:
            return None

    queen.allow_tags = True

    @staticmethod
    def balance(obj):
        """

        :param obj:
        :return:
        """
        return round(obj.account.accountBalance, 2)


class CustomerTariffHistoryAdmin(ReadOnlyAdmin):
    """tariff history read only"""
    list_display = ('id', 'tariff_link', 'customer_link')
    list_filter = ('tariff', 'customer')

    @get_referred_model_url('tariff')
    def tariff_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('customer')
    def customer_link(self, obj):
        """

        :param obj:
        """
        pass


class AccountAdmin(EditableAdmin):
    """edit account"""
    list_display = ('created', 'id', 'customer_link', 'accountBalance', 'account_rule_link', 'clearing_bal','uncollected_bal','reconnect_bal')
    list_filter = ('accountRule',)
    search_fields = ('id', '=accountOwner__firstName', '=accountOwner__lastName')

    @get_referred_model_url('accountrule', referred_as='accountRule')
    def account_rule_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('customer', referred_as='accountOwner', display='customerId')
    def customer_link(self, obj):
        """

        :param obj:
        """
        pass


class AccountRuleAdmin(EditableAdmin):
    """edit account rules"""
    list_display = ('id', 'name', 'minBalance', 'dailyAllowedSMSBalReq')


class LoanAdmin(EditableAdmin):
    """edit loans"""
    list_display = (
        'id', 'created', 'loanType', 'account_link', 'loanAmount', 'outStandingPrinciple', 'startDate', 'fixedRepayment', 'repaymentSchedule',
        'paymentFrequency',
        'totalNumberOfPayments', 'remaining_payments', 'passedDue', 'lastPaymentNumber', 'lastPaymentReceivedDate', 'notes')
    list_filter = ('loanType', 'passedDue')

    @get_referred_model_url('account', referred_as='account', display='accountOwner')
    def account_link(self, obj):
        """

        :param obj:
        """
        pass

    @staticmethod
    def customer_name(obj):
        """

        :param obj:
        :return:
        """
        return str(obj.account.accountOwner)

    @staticmethod
    def remaining_payments(obj):
        """

        :param obj:
        :return:
        """
        return obj.totalNumberOfPayments - int(obj.lastPaymentNumber or 0)


class LoanPaymentHistoryAdmin(EditableAdmin):
    """edit loan payments"""
    pass


class PowerhiveSMSAccountAdmin(EditableAdmin):
    """sms account edit"""
    pass


class SMSAdmin(EditableAdmin):
    """sms edit"""
    list_display = ('collectTime', 'type', 'powerhive_sms_account_link', 'customer_link', 'smsFlow', 'message',
                    'inBoundSMSId', 'outBoundSMSId', 'cost')
    list_filter = ('type',)

    @get_referred_model_url('powerhivesmsaccount', referred_as='phAccount')
    def powerhive_sms_account_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('customer', referred_as='customer')
    def customer_link(self, obj):
        """

        :param obj:
        """
        pass


class SMSServiceProviderAdmin(EditableAdmin):
    """sms provider edit"""
    pass


class PhoneAccountAdmin(EditableAdmin):
    """phone edit"""
    list_display = ('mobileMoneyNumber', 'msisdn', 'account_link', 'firstName', 'lastName')
    raw_id_fields = ('account',)
    search_fields = ('mobileMoneyNumber', 'firstName', 'lastName')

    @get_referred_model_url('account', referred_as='account')
    def account_link(self, obj):
        """

        :param obj:
        """
        pass


class NetworkServiceProviderAdmin(EditableAdmin):
    """network edit"""
    pass


class MobilePaymentAdmin(EditableAdmin):
    """mobile payment edit"""
    list_display = (
        'transactionId', 'account_link', 'processedTime', 'amount', 'accountBalanceBefore', 'accountBalanceAfter', 'payment_type')
    date_hierarchy = 'processedTime'
    search_fields = ('=transactionId', 'account__accountOwner__phoneNumber', '=account__acountOwner__firstName',
                     '=account__accountOwner__lastName')

    @get_referred_model_url('account')
    def account_link(self, obj):
        """

        :param obj:
        """
        pass


class PaymentSyncHistoryAdmin(EditableAdmin):
    """payment history edit"""
    list_display = ('mobile_payment_provider_link', 'collectTimeFrom', 'collectTimeTo', 'status', 'totalPayments',
                    'totalPayedAmount')

    @get_referred_model_url('networkserviceprovider', referred_as='paymentServiceProvider')
    def mobile_payment_provider_link(self, obj):
        """

        :param obj:
        """
        pass


class UsageChargeAdmin(EditableAdmin):
    """usage charge admin"""
    list_display = ('account_link', 'usage_link', 'processedTime', 'amount', 'accountBalanceBefore',
                    'accountBalanceAfter', 'perSegmentChargeComponent')
    list_filter = ('account', 'processedTime')

    @get_referred_model_url('account')
    def account_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('usage')
    def usage_link(self, obj):
        """

        :param obj:
        """
        pass


class CreditAdjustmentHistoryAdmin(EditableAdmin):
    """credit adjust edit"""
    pass


class MasterTransactionHistoryAdmin(ReadOnlyAdmin):
    """master transaction read only"""
    list_display = ('id', 'amount', 'account', 'transactionType', 'source', 'sourceId', 'accountBalance', 'sourceProcessedTime', 'processed')
    list_filter = ('source', 'transactionType', 'processed')
    search_fields = ('=account__id',)


class QueenMonitorAdmin(ReadOnlyAdmin):
    """queen monitor read only"""
    list_display = ('queen_link', 'collectTime', 'rssiAvg', 'rssiMin', 'rssiMax', 'tempAvg', 'tempMin', 'tempMax',
                    'hardwareReinits', 'commAttempts', 'commErrors', 'osUptime', 'fwUptime', 'fwVersion')
    list_filter = ('queen',)

    @get_referred_model_url('queen')
    def queen_link(self, obj):
        """

        :param obj:
        """
        pass


class CircuitMonitorAdmin(ReadOnlyAdmin):
    """circuit monitor read only"""
    list_display = ('circuit_link', 'vacAvg', 'vacMin', 'vacMax', 'iacMinN', 'iacMaxN', 'iacAvgN', 'iacMinH', 'iacMaxH',
                    'iacAvgH', 'collectTime', 'commAttempts', 'commErrors', 'switchEnabled', 'disableVaLimit', 'switchReason')
    search_fields = ('=circuit__queen__deviceId', '=circuit__deviceId')
    list_filter = ('switchReason', 'circuit__queen__grid')

    @get_referred_model_url('circuit')
    def circuit_link(self, obj):
        """

        :param obj:
        """
        pass


class ProbeMonitorAdmin(ReadOnlyAdmin):
    """probe monitor read only"""
    list_display = ('probe_link', 'collectTime', 'commAttempts', 'commErrors')
    list_filter = ('probe', 'probe__queen')

    @get_referred_model_url('probe')
    def probe_link(self, obj):
        """

        :param obj:
        """
        pass


class InverterMonitorAdmin(ReadOnlyAdmin):
    """inverter monitor read only"""
    list_display = ('inverter_link', 'collectTime', 'commAttempts', 'commErrors',
                    'whFromInverter', 'vaFromInverterMin', 'vaFromInverterAvg', 'vaFromInverterMax',
                    'vacMin', 'vacAvg', 'vacMax', 'vdcMin', 'vdcAvg', 'vdcMax', 'idcMin', 'idcAvg', 'idcMax', 'socMin',
                    'socAvg', 'socMax')
    list_filter = ('inverter', 'inverter__powerstation', 'inverter__powerstation__queen')

    @get_referred_model_url('inverter')
    def inverter_link(self, obj):
        """

        :param obj:
        """
        pass


class BatteryBankMonitorAdmin(ReadOnlyAdmin):
    """battery monitor read only"""
    list_display = ('battery_bank_link', 'collectTime', 'commAttempts', 'commErrors', 'vdcMin',
                    'vdcAvg', 'vdcMax', 'socMin', 'socAvg', 'socMax', 'idcMin', 'idcAvg', 'idcMax')
    list_filter = ('batteryBank', 'batteryBank__powerstation', 'batteryBank__powerstation__queen')

    @get_referred_model_url('batterybank', referred_as='batteryBank')
    def battery_bank_link(self, obj):
        """

        :param obj:
        """
        pass


class TariffAdmin(ReadOnlyAdmin):
    """tariff read only"""
    list_display = ('tariffId', 'name', 'entry_tariff_calendar_link')

    @get_referred_model_url('tariffcalendar', short_description='Entry Calendar', referred_as='entryTariffCalendar')
    def entry_tariff_calendar_link(self, obj):
        """

        :param obj:
        """
        pass


class TariffCalendar(ReadOnlyAdmin):
    """tariff calendar read only"""
    list_display = ('calendarId', 'next_tariff_cal_link', 'entry_tariff_segment_link', 'dateExpires')

    @get_referred_model_url('tariffcalendar', referred_as='nextTariffCal', short_description='Next Tariff Cal')
    def next_tariff_cal_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('tariffsegment', referred_as='entryTariffSegment', short_description='Entry Tariff Segment')
    def entry_tariff_segment_link(self, obj):
        """

        :param obj:
        """
        pass


class TariffSegmentAdmin(ReadOnlyAdmin):
    """tariff segment read only"""
    list_display = ('segmentId', 'next_tariff_segment_link', 'tariffType', 'timeExpires', 'vaLimit', 'perSegmentCharge',
                    'perWhCharge', 'perVACharge')

    @get_referred_model_url('tariffsegment', referred_as='nextTariffSegment', short_description='Next Tariff Segment')
    def next_tariff_segment_link(self, obj):
        """

        :param obj:
        """
        pass


class UsageAdmin(ReadOnlyAdmin):
    """usage read only"""
    list_display = ('customer_link', 'circuit_link', 'format_collect_time', 'processed',
                    'intervalWh', 'intervalWhH', 'intervalVAmax', 'intervalVAmaxH', 'intervalVmin')
    list_filter = ('processed', 'circuit__queen__grid',)
    search_fields = ('=customer__customerId', '=circuit__queen__deviceId', '=circuit__deviceId')

    @get_referred_model_url('customer', display='customerId', referred_as='customer')
    def customer_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('circuit', display='deviceId', referred_as='circuit')
    def circuit_link(self, obj):
        """

        :param obj:
        """
        pass

    def format_collect_time(self, obj):
        """Format collectTime to display detail time stamp.
        :param obj:
        :return:
        """
        return obj.collectTime.strftime("%d %b %Y %H:%M:%S")

    format_collect_time.short_description = 'Collect Time'


class NonCustomerUsageAdmin(ReadOnlyAdmin):
    """ non customer usage read only"""
    list_display = ('circuit_link', 'collectTime', 'processed', 'intervalWh', 'intervalVAmax', 'intervalVmin')

    @get_referred_model_url('circuit', display='deviceId', referred_as='circuit')
    def circuit_link(self, obj):
        """

        :param obj:
        """
        pass


class PotentialCustomerImportHistoryAdmin(EditableAdmin):
    """potential customer edit"""
    list_display = ('id', 'dataSource', 'totalCustomerDataSynced', 'lastSyncedSourceUpdatedAtSinceEpoch', 'status')


class CronJobLogAdmin(ReadOnlyAdmin):
    """cron read only"""
    list_display = ('code', 'is_success', 'start_time', 'message')
    list_filter = ('is_success', 'code')


class CompanyAdmin(EditableAdmin):
    """company edit"""
    pass


class C2BPaymentValidationRequestAdmin(EditableAdmin):
    """c2b payment edit"""
    list_display = (
        'transTime', 'transAmount', 'status', 'responseDesc', 'billRefNumber', 'payerPhoneNumber', 'firstName',
        'lastName', 'payment_type')
    search_fields = ('billRefNumber', 'payerPhoneNumber', 'firstName', 'lastName')
    list_filter = ('status',)


class ScratchcardAdmin(EditableAdmin):
    """scratch card edit"""
    list_display = ('id', 'pin', 'status', 'controlNumber',
                    'manufacturer_link', 'pin_generated_by_link', 'amount',
                    'country_link')

    @get_referred_model_url('company', display='name', referred_as='manufacturer')
    def manufacturer_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('company', display='name', referred_as='pinGeneratedBy')
    def pin_generated_by_link(self, obj):
        """

        :param obj:
        """
        pass

    @get_referred_model_url('country', display='name', referred_as='country')
    def country_link(self, obj):
        """

        :param obj:
        """
        pass


class ScratchcardPaymentHistoryAdmin(EditableAdmin):
    """scratch card payment edit"""
    pass


class FulcrumFormAdmin(ReadOnlyAdmin, JSONMixin):
    """fulcrum read only"""
    list_display = ('id', 'name', 'description', 'data_prettified')
    list_display_links = None


class FulcrumQuestionAdmin(ReadOnlyAdmin, JSONMixin):
    """fulcrum questions read only"""
    list_display = ('data_name_styled', 'form_link', 'label', 'description', 'active', 'key', 'data_prettified')
    list_filter = ('form__name', 'active')

    def data_name_styled(self, obj):
        """

        :param obj:
        :return:
        """
        data_name = obj.data_name
        data_name = data_name.replace('_', ' ')
        return data_name

    @get_referred_model_url('fulcrumform', display='name', referred_as='form')
    def form_link(self, obj):
        """

        :param obj:
        """
        pass

    data_name_styled.short_description = 'name'


class FulcrumRecordAdmin(EditableAdmin, JSONMixin):
    """fulcrum record edit"""
    list_display = ('id', 'form_link', 'latitude', 'longitude', 'form_data_prettified', 'metadata_prettified')
    readonly_fields = ('id', 'form_link', 'latitude', 'longitude', 'form_data_prettified', 'metadata_prettified')
    search_fields = ('id', 'form__id', 'form_data')
    exclude = ('form_data', 'metadata')
    list_filter = ('form__name',)

    @get_referred_model_url('fulcrumform', display='name', referred_as='form')
    def form_link(self, obj):
        """

        :param obj:
        """
        pass


# Location/Project related database Register.
admin.site.register(ph_model.country.Country, CountryAdmin)
admin.site.register(ph_model.region.Region, RegionAdmin)
admin.site.register(ph_model.municipality.Municipality, MunicipalityAdmin)
admin.site.register(ph_model.project.Project, ProjectAdmin)

# Powerhive users related Registers.
# noinspection PyProtectedMember
if django_user.User in admin.site._registry:
    admin.site.unregister(django_user.User)
admin.site.register(django_user.User, DjangoUserAdmin)

admin.site.register(ph_model.permission.Permission, PermissionAdmin)

# Hardware related Registers.
admin.site.register(ph_model.grid.Grid, GridAdmin)
admin.site.register(ph_model.powerstation.PowerStation, PowerStationAdmin)
admin.site.register(ph_model.inverter.Inverter, InverterAdmin)
admin.site.register(ph_model.battery.BatteryBank, BatteryBankAdmin)
admin.site.register(ph_model.queen.Queen, QueenAdmin)
admin.site.register(ph_model.queen.QueenFirmware, QueenFirmwareAdmin)
admin.site.register(ph_model.probe.Probe, ProbeAdmin)
admin.site.register(ph_model.circuit.Circuit, CircuitAdmin)
admin.site.register(ph_model.circuit.SwitchEnabledHistory, SwitchEnabledHistoryAdmin)
admin.site.register(ph_model.circuit.PriorityCircuit, PriorityCircuitAdmin)
admin.site.register(ph_model.generation.Generation, GenerationAdmin)
admin.site.register(ph_model.event.EventType, EventTypeAdmin)
admin.site.register(ph_model.event.Event, EventAdmin)
admin.site.register(ph_model.event.EventDetail, EventDetailAdmin)
admin.site.register(ph_model.monitor.QueenMonitor, QueenMonitorAdmin)
admin.site.register(ph_model.monitor.ProbeMonitor, ProbeMonitorAdmin)
admin.site.register(ph_model.monitor.CircuitMonitor, CircuitMonitorAdmin)
admin.site.register(ph_model.monitor.InverterMonitor, InverterMonitorAdmin)
admin.site.register(ph_model.monitor.BatteryBankMonitor, BatteryBankMonitorAdmin)

# Customer related Registers.
admin.site.register(ph_model.customer.Customer, CustomerAdmin)
admin.site.register(ph_model.customer.CustomerTariffHistory, CustomerTariffHistoryAdmin)
admin.site.register(ph_model.account.PhoneAccount, PhoneAccountAdmin)
admin.site.register(ph_model.account.Account, AccountAdmin)
admin.site.register(ph_model.account.AccountRule, AccountRuleAdmin)

admin.site.register(ph_model.loan.Loan, LoanAdmin)
admin.site.register(ph_model.loan.LoanPaymentHistory, LoanPaymentHistoryAdmin)

admin.site.register(ph_model.mobile_payment_provider.NetworkServiceProvider, NetworkServiceProviderAdmin)
admin.site.register(ph_model.transaction.MobilePayment, MobilePaymentAdmin)

admin.site.register(ph_model.transaction.CreditAdjustmentHistory, CreditAdjustmentHistoryAdmin)
admin.site.register(ph_model.mobile_payment_provider.PaymentSyncHistory, PaymentSyncHistoryAdmin)
admin.site.register(ph_model.mobile_payment_provider.C2BPaymentValidationRequest, C2BPaymentValidationRequestAdmin)

admin.site.register(ph_model.transaction.MasterTransactionHistory, MasterTransactionHistoryAdmin)

admin.site.register(ph_model.sms.SMS, SMSAdmin)
admin.site.register(ph_model.sms.SMSServiceProvider, SMSServiceProviderAdmin)
admin.site.register(ph_model.sms.PowerhiveSMSAccount, PowerhiveSMSAccountAdmin)

admin.site.register(ph_model.transaction.UsageCharge, UsageChargeAdmin)
admin.site.register(ph_model.tariff.Tariff, TariffAdmin)
admin.site.register(ph_model.tariff.TariffCalendar, TariffCalendar)
admin.site.register(ph_model.tariff.TariffSegment, TariffSegmentAdmin)
admin.site.register(ph_model.usage.Usage, UsageAdmin)
admin.site.register(ph_model.usage.NonCustomerUsage, NonCustomerUsageAdmin)

# Potential customer related admin
admin.site.register(ph_model.customer.PotentialCustomerImportHistory, PotentialCustomerImportHistoryAdmin)

# Cron Job Log
# noinspection PyProtectedMember
if CronJobLog in admin.site._registry:
    admin.site.unregister(CronJobLog)
admin.site.register(CronJobLog, CronJobLogAdmin)

# Scratch card company
admin.site.register(ph_model.company.Company, CompanyAdmin)
admin.site.register(ph_model.scratchcard.Scratchcard, ScratchcardAdmin)
admin.site.register(ph_model.transaction.ScratchcardPaymentHistory, ScratchcardPaymentHistoryAdmin)

# fulcrum
admin.site.register(ph_model.fulcrum.FulcrumForm, FulcrumFormAdmin)
admin.site.register(ph_model.fulcrum.FulcrumQuestion, FulcrumQuestionAdmin)
admin.site.register(ph_model.fulcrum.FulcrumRecord, FulcrumRecordAdmin)
