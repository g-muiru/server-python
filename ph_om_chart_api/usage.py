# coding=utf-8
"""Chart views.."""

from ph_om_chart_api import util as chart_util
import ph_model.models as ph_model
from ph_om_chart_api import base_chart
import itertools
import logging
import abc
from ph_util.test_utils import test_mode

DB = 'replica'

if test_mode():
    DB = 'default'

LOGGER = logging.getLogger(__name__)

USAGE_CHART_DATA_LOOK_BACK_DAYS = 3
USAGE_CHART_VIEW_INTERVAL_MINUTES = 15


class _BaseUsageChart(base_chart.BaseChart):
    """ABC Base chart module need to override cls.model, self._get_customer_data_usage, self._get_non_customer_data_usage."""
    # Need to be override.
    model = None
    CUSTOMER_USAGE_FIELDS = (
        'circuit_id', 'circuit__queen_id', 'circuit__queen__grid_id', 'customer_id', 'intervalWh', 'intervalWhH', 'intervalVAmax',
        'intervalVAmaxH', 'collectTime', 'charge__amount', 'charge__accountBalanceAfter')
    NON_CUSTOMER_USAGE_FIELDS = (
        'circuit_id', 'circuit__queen_id', 'circuit__queen__grid_id', 'intervalWh', 'intervalWhH', 'intervalVAmax', 'intervalVAmaxH', 'collectTime')

    def __init__(self, user, device_id, query_config, usage_type=ph_model.usage.USAGE_TYPE.ALL):
        """

        :param user:            django user, o&m user.
        :param device_id:       hardware device id.
        :param query_config:    query info
        :param usage_type:       data model
        """
        self.usage_type = usage_type
        base_chart.BaseChart.__init__(self, user, device_id, query_config)

    @abc.abstractmethod
    def _get_non_customer_usage_data(self):
        """Returns circuit usage queryset not associated to a customer."""
        pass

    @abc.abstractmethod
    def _get_customer_usage_data(self):
        """Returns circuit usage queryset associated to a customer."""
        pass

    # noinspection PyDictCreation
    def _get_raw_data(self):

        usage_data = []
        if self.usage_type == ph_model.usage.USAGE_TYPE.CUSTOMER:
            usage_iter = itertools.chain(self._get_customer_usage_data())
        elif self.usage_type == ph_model.usage.USAGE_TYPE.NO_CUSTOMER:
            usage_iter = itertools.chain(self._get_non_customer_usage_data())
        else:
            usage_iter = itertools.chain(self._get_customer_usage_data(), self._get_non_customer_usage_data())

        last_bal = 0.0
        for usageChartDatum in usage_iter:
            data = {}
            data['circuit_id'] = usageChartDatum.get('circuit_id')
            data['customer_id'] = usageChartDatum.get('customer_id')
            data['circuit__queen_id'] = usageChartDatum.get('circuit__queen_id')
            data['circuit__queen__grid_id'] = usageChartDatum.get('circuit__queen__grid_id')
            data["collectTime"] = usageChartDatum.get('collectTime')
            data["intervalWh"] = chart_util.get_float(usageChartDatum.get('intervalWh'), 0.0)
            data["intervalWhH"] = chart_util.get_float(usageChartDatum.get('intervalWhH'), 0.0)
            data["charge__amount"] = chart_util.get_float(usageChartDatum.get('charge__amount'), 0.0)
            data["charge__accountBalanceAfter"] = chart_util.get_float(usageChartDatum.get('charge__accountBalanceAfter'), 0.0)
            data["intervalVAmax"] = chart_util.get_float(usageChartDatum.get('intervalVAmax'), 0.0)
            data["intervalVAmaxH"] = chart_util.get_float(usageChartDatum.get('intervalVAmaxH'), 0.0)

            if data["charge__accountBalanceAfter"] != 0.0:
                last_bal = data["charge__accountBalanceAfter"]
            else:
                data["charge__accountBalanceAfter"] = last_bal
            usage_data.append(data)
        return usage_data


class CircuitUsageChart(_BaseUsageChart):
    """
    Usage utils
    """
    model = ph_model.circuit.Circuit

    def _get_non_customer_usage_data(self):
        return ph_model.usage.Usage.non_customer_usages.using(DB).all().values(*self.NON_CUSTOMER_USAGE_FIELDS).all().filter(
            circuit=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)

    def _get_customer_usage_data(self):
        """Returns circuit usage queryset associated to a customer."""
        return ph_model.usage.Usage.customer_usages.using(DB).all().values(*self.CUSTOMER_USAGE_FIELDS).filter(
            circuit=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)

    def get_municipality(self):
        """
        Muni request
        :return:
        """
        return ph_model.municipality.Municipality.objects.using(DB).get(grid__queens__queen_circuits=self.device)


class QueenUsageChart(_BaseUsageChart):
    """
    Queen Usage utils
    """
    model = ph_model.queen.Queen

    def _get_non_customer_usage_data(self):
        return ph_model.usage.Usage.non_customer_usages.using(DB).all().values(*self.NON_CUSTOMER_USAGE_FIELDS).all().filter(
            circuit__queen=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)

    def _get_customer_usage_data(self):
        """Returns circuit usage queryset associated to a customer."""
        return ph_model.usage.Usage.customer_usages.using(DB).all().values(*self.CUSTOMER_USAGE_FIELDS).filter(
            circuit__queen=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)

    def get_municipality(self):
        """
        Muni request
        :return:
        """
        return ph_model.municipality.Municipality.objects.using(DB).get(grid__queens=self.device)


class GridUsageChart(_BaseUsageChart):
    """
    Grid usage utils
    """
    model = ph_model.grid.Grid

    def _get_non_customer_usage_data(self):
        return ph_model.usage.Usage.non_customer_usages.using(DB).all().values(*self.NON_CUSTOMER_USAGE_FIELDS).all().filter(
            circuit__queen__grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)

    def _get_customer_usage_data(self):
        """Returns circuit usage queryset associated to a customer."""
        return ph_model.usage.Usage.customer_usages.using(DB).all().values(*self.CUSTOMER_USAGE_FIELDS).filter(
            circuit__queen__grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)

    def get_municipality(self):
        """
        Muni utils
        :return:
        """
        return ph_model.municipality.Municipality.objects.using(DB).get(grid=self.device)
