"""Chart views.."""

import logging

import enum
from ph_om_chart_api import base_chart
from ph_om_chart_api import generation as  generation_chart
from ph_om_chart_api import aggregate_grid as aggregate_grid_chart
from ph_om_chart_api import monitor as  monitor_chart
from ph_om_chart_api import usage as  usage_chart
import ph_model.models as ph_model
from ph_common_api import acl
from ph_common_api import json_response
from ph_util import mixins


LOGGER = logging.getLogger(__name__)

class UnknownURL(Exception):
    """Unknown url exception."""
    pass


MINIMUM_CHART_DATA_VIEW_ROLE =  ph_model.permission.Role.VIEW.value
LOOK_BACK_DAYS, FROM_DATE, INTERVAL, FIELDS = xrange(4)

class UsageChartHandler(mixins.PairEnumMixin, enum.Enum):
        """Usage type"""
        GRID = usage_chart.GridUsageChart
        QUEEN = usage_chart.QueenUsageChart
        CIRCUIT = usage_chart.CircuitUsageChart

class MonitorChartHandler(mixins.PairEnumMixin, enum.Enum):
        """Monitor type"""
        QUEEN = monitor_chart.QueenMonitorChart
        PROBE = monitor_chart.ProbeMonitorChart
        CIRCUIT = monitor_chart.CircuitMonitorChart
        INVERTER = monitor_chart.InverterMonitorChart
        BATTERYBANK = monitor_chart.BatteryBankMonitorChart


class GenerationChartHandler(mixins.PairEnumMixin, enum.Enum):
        """Generation type"""
        GRID = generation_chart.GridGenerationChart

class AggregateGridHandlerHandler(mixins.PairEnumMixin, enum.Enum):
        """Aggregate grid type"""
        GRID = aggregate_grid_chart.AggregateGridChart

class DeviceModel(mixins.PairEnumMixin, enum.Enum):
    """util model mapping."""
    GRID = ph_model.grid.Grid
    QUEEN = ph_model.queen.Queen
    PROBE = ph_model.probe.Probe
    CIRCUIT = ph_model.circuit.Circuit
    POWERSTATION = ph_model.powerstation.PowerStation
    INVERTER = ph_model.inverter.Inverter
    BATTERYBANK = ph_model.battery.BatteryBank


def usage_view(request, model, deviceId):
    model= model.upper()
    if not UsageChartHandler.is_valid_key(model):
         raise UnknownURL("Unknown usage url {}".format(model))
    acl.check_permission(request.user, model, deviceId, MINIMUM_CHART_DATA_VIEW_ROLE)
    query = request.GET.get('queryConfig')
    queryConfig = base_chart.ChartQueryConfig(query)
    deviceChart = UsageChartHandler.value_of(model)(request.user, deviceId, queryConfig)
    return json_response.PHJsonHTTPResponse(deviceChart.get_data())


def monitor_view(request, deviceModel, deviceId):
     """Returns device monitor data."""
     deviceModel = deviceModel.upper()
     if not MonitorChartHandler.is_valid_key(deviceModel):
         raise UnknownURL("Unknown Monitor model url {}".format(deviceModel))
     acl.check_permission(request.user, deviceModel, deviceId, MINIMUM_CHART_DATA_VIEW_ROLE)
     query = request.GET.get('queryConfig')
     queryConfig = base_chart.ChartQueryConfig(query)
     monitorChartHandler = MonitorChartHandler.value_of(deviceModel)(request.user, deviceId, queryConfig, acl.AclModels.value_of(deviceModel))
     return json_response.PHJsonHTTPResponse(monitorChartHandler.get_data())


def agg_monitor_view(request, parentModel, parentDeviceId, deviceModel):
     """Returns Aggregate monitor data."""
     deviceModel = deviceModel.upper()
     parentModel = parentModel.upper()
     if not MonitorChartHandler.is_valid_key(deviceModel):
         raise UnknownURL("Unknown Monitor model url {}".format(deviceModel))
     acl.check_permission(request.user, parentModel, parentDeviceId, MINIMUM_CHART_DATA_VIEW_ROLE)
     query = request.GET.get('queryConfig')
     queryConfig = base_chart.ChartQueryConfig(query)
     monitorChartHandler = MonitorChartHandler.value_of(deviceModel)(request.user, parentDeviceId, queryConfig, acl.AclModels.value_of(parentModel))
     return json_response.PHJsonHTTPResponse(monitorChartHandler.get_data())


def generation_view(request, deviceModel, deviceId):
     """Returns device generation data."""
     deviceModel = deviceModel.upper()
     if not GenerationChartHandler.is_valid_key(deviceModel):
         raise UnknownURL("Unknown Generation model url {}".format(deviceModel))
     acl.check_permission(request.user, deviceModel, deviceId, MINIMUM_CHART_DATA_VIEW_ROLE)
     query = request.GET.get('queryConfig')
     queryConfig = base_chart.ChartQueryConfig(query)
     generationChartData = GenerationChartHandler.value_of(deviceModel)(request.user, deviceId, queryConfig, acl.AclModels.value_of(deviceModel))
     return json_response.PHJsonHTTPResponse(generationChartData.get_data())



def aggregate_grid_view(request, deviceModel, deviceId):
     """Returns grid daily aggregated data."""
     deviceModel = deviceModel.upper()
     if not AggregateGridHandlerHandler.is_valid_key(deviceModel):
         raise UnknownURL("Unknown Aggregate  model url {}".format(deviceModel))
     acl.check_permission(request.user, deviceModel, deviceId, MINIMUM_CHART_DATA_VIEW_ROLE)
     query = request.GET.get('queryConfig')
     queryConfig = base_chart.ChartQueryConfig(query)
     aggregateGridChartData = AggregateGridHandlerHandler.value_of(deviceModel)(request.user, deviceId, queryConfig, acl.AclModels.value_of(deviceModel))
     return json_response.PHJsonHTTPResponse(aggregateGridChartData.get_data())
