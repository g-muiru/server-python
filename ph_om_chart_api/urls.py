from django.conf import urls
from ph_om_chart_api import views as chart_views
from ph_common_api import auth as om_auth

urlpatterns = [
    urls.url(r'^login/$', om_auth.login, name='om-chart-login'),

    urls.url(r'^usage/([\w-]+)/([\w-]+)/$', chart_views.usage_view,
             name='om-chart-usage'),

    #e.g monitor/queen/1 all queen monitor data
    urls.url(r'^monitor/(?P<deviceModel>[\w-]+)/(?P<deviceId>[\w-]+)/$',
             chart_views.monitor_view, name='om-chart-device-monitor'),

    #e.g monitor/queen/1/agg/probe/ all probe monitor data aggregated by  queen id.
    urls.url(r'^monitor/(?P<parentModel>[\w-]+)/(?P<parentDeviceId>[\w-]+)/agg/(?P<deviceModel>[\w-]+)/$',
             chart_views.agg_monitor_view, name='om-chart-agg-monitor'),

        #e.g generation/grid/1 all grid generation data
    urls.url(r'^generation/(?P<deviceModel>[\w-]+)/(?P<deviceId>[\w-]+)/$',
             chart_views.generation_view, name='om-chart-device-generation'),

    urls.url(r'^aggregategrid/(?P<deviceModel>[\w-]+)/(?P<deviceId>[\w-]+)/$',
             chart_views.aggregate_grid_view, name='om-chart-aggregate-grid-daily-report'),
]
