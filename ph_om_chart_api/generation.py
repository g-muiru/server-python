# coding=utf-8
"""Generation Chart views.."""

import ph_model.models as ph_model
from ph_om_chart_api import base_chart
import logging
from ph_util.test_utils import test_mode

DB = 'replica'

if test_mode():
    DB = 'default'

LOGGER = logging.getLogger(__name__)

MINIMUM_CHART_VIEW_ROLE = ph_model.permission.Role.VIEW.value
USAGE_CHART_DATA_LOOK_BACK_DAYS = 3
USAGE_CHART_VIEW_INTERVAL_MINUTES = 15


class UnSupportedChartModel(Exception):
    """Unknow model for chart."""
    pass


class GridGenerationChart(base_chart.BaseChart):
    """
    Grid generation request
    """
    FIELDS = ('collectTime', 'whFromPV', 'whToGrid', 'vaFromPVMin', 'vaFromPVAvg', 'vaFromPVMax',
              'vaToGridMin', 'vaToGridAvg', 'vaToGridMax', 'vacMin', 'vacAvg', 'vacMax', 'socMin', 'socAvg', 'socMax')

    def __init__(self, user, device_id, query_config, model):
        """

        :param user:            django user, o&m user.
        :param device_id:       hardware device id.
        :param query_config:    query info
        :param model:           data model
        """
        base_chart.BaseChart.__init__(self, user, device_id, query_config, model=model)

    def _get_generation_data(self):
        """Returns queen monitor raw data aggregated by @param model form @param modelId."""
        if self.model == ph_model.grid.Grid:
            generation_data = ph_model.generation.Generation.objects.using(DB).all().values(*self.FIELDS).filter(
                grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        else:
            raise UnSupportedChartModel('Chart for model {} not supported'.format(str(self.model)))

        return generation_data

    def _get_raw_data(self):
        generation_data = self._get_generation_data()
        generations = []
        for generation in generation_data:
            data = {}
            for field in self.FIELDS:
                data[field] = generation[field]
            generations.append(data)
        return generations

    def get_municipality(self):
        """
        Municipality request
        :return:
        """
        return ph_model.municipality.Municipality.objects.using(DB).get(grid=self.device)
