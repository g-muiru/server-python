"""Tests ph_mo_api.test_manager."""

import datetime
from django import test
from dateutil import tz
from ph_om_chart_api import util as  chart_util
import ph_model.models as ph_model


def get_utc_datetime(utcString):
    return ph_model.fields.DateTimeUTC().to_python(utcString)


def get_utc_date(utcString):
    return ph_model.fields.DateTimeUTC().to_python(utcString).date()


class ChartUtilTest(test.TestCase):
    def setUp(self):
        self.input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 15.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-2T1:01:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0}
        ]

    def test_get_query_fetch_interval(self):
        expected = (datetime.datetime(2014, 1, 1, 0, 0, tzinfo=tz.gettz('UTC')),
                    datetime.datetime(2014, 1, 5, 0, 0, tzinfo=tz.gettz('UTC')))
        d = chart_util.get_query_fetch_interval(3, '20140104')
        self.assertItemsEqual(d, expected)


class DataAggregatorTest(test.TestCase):
    def setUp(self):

        self.input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 15.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]

    def test_group_by_minute_interval_15_minutes_merge(self):
        intervals = [15 * 60]
        grouping = ['merge']
        dateIdentifier = 'datetime'
        fieldsConfig = {'intervalVAmax': (['sum'],),
                       'datetime': (['min'],)}
        input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 15.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]
        agg = chart_util.DataAgg(input, dateIdentifier, intervals, grouping, fieldsConfig)
        EXPECTED_15_MINUTES = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), 'intervalVAmax': 45.0},

            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), 'intervalVAmax': 25.0},

            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), 'intervalVAmax': 10.0},
        ]
        self.assertItemsEqual(EXPECTED_15_MINUTES, agg.data)

    def test_group_by_minute_interval_15_minutes_merge_more_test_case(self):
        intervals = [15 * 60]
        grouping = ['merge']
        dateIdentifier = 'collectTime'
        fieldsConfig = {'intervalVAmax': (['max'],),
                       'collectTime': (['round'],)}
        input = [{'collectTime': get_utc_datetime('2014-12-22T20:41:52Z'), 'intervalVAmax': 60.4389534},
                 {'collectTime': get_utc_datetime('2014-12-22T20:39:53Z'), 'intervalVAmax': 24.932340622},
                 {'collectTime': get_utc_datetime('2014-12-22T20:37:53Z'), 'intervalVAmax': 73.753471375},
                 {'collectTime': get_utc_datetime('2014-12-22T20:35:52Z'), 'intervalVAmax': 74.661727905},
                 {'collectTime': get_utc_datetime('2014-12-22T20:33:52Z'), 'intervalVAmax': 0.074363269},
                 {'collectTime': get_utc_datetime('2014-12-22T20:31:52Z'), 'intervalVAmax': 21.321756363},

                 {'collectTime': get_utc_datetime('2014-12-22T20:29:53Z'), 'intervalVAmax': 20.326221466},
                 {'collectTime': get_utc_datetime('2014-12-22T20:27:53Z'), 'intervalVAmax': 34.005531311},
                 {'collectTime': get_utc_datetime('2014-12-22T20:25:52Z'), 'intervalVAmax': 9.827649117},
                 {'collectTime': get_utc_datetime('2014-12-22T20:23:52Z'), 'intervalVAmax': 0.445337951}]
        agg = chart_util.DataAgg(input, dateIdentifier, intervals, grouping, fieldsConfig)

        EXPECTED_15_MINUTES = [
           {'collectTime': get_utc_datetime('2014-12-22T20:15:00Z'), 'intervalVAmax': 34.005531311},
           {'collectTime': get_utc_datetime('2014-12-22T20:30:00Z'), 'intervalVAmax': 74.661727905}
        ]
        self.assertItemsEqual(EXPECTED_15_MINUTES, agg.data)

    def test_group_by_minute_interval_15_minutes_merge_datetime_rounding(self):
        intervals = [15 * 60]
        grouping = ['merge']
        dateIdentifier = 'datetime'
        fieldsConfig = {'intervalVAmax': (['sum'],),
                       'datetime': (['ROUND'],)}
        input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:01:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},

            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},

            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 30.0},

            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 15.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]
        agg = chart_util.DataAgg(input, dateIdentifier, intervals, grouping, fieldsConfig)
        EXPECTED_15_MINUTES = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-1T1:30:00Z"), 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:45:00Z"), 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:00Z"), 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-8T0:00:00Z"), 'intervalVAmax': 10.0},
        ]
        self.assertItemsEqual(EXPECTED_15_MINUTES, agg.data)


    def test_group_by_minute_interval_30_minutes_merge(self):
        input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 15.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]
        EXPECTED_30_MINUTES = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), 'intervalVAmax': 55.0},
            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), 'intervalVAmax': 55.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), 'intervalVAmax': 10.0},
        ]
        dateIdentifier = 'datetime'
        intervals = [30 * 60]
        grouping = ['merge']
        fieldsConfig = {'intervalVAmax': (['sum'],),
                        'datetime': (['min'],)}
        agg = chart_util.DataAgg(input, dateIdentifier, intervals, grouping, fieldsConfig)
        self.assertItemsEqual(EXPECTED_30_MINUTES, agg.data)

    def test_merge_30_minute_then_daily_merge(self):
        input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 15.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]
        daily = 24 * 60 * 60
        thirtyMinutesInterval = 30 * 60
        intervals = [thirtyMinutesInterval, daily ]
        grouping = ['merge', 'merge']
        dateIdentifier = 'datetime'
        fieldsConfig = {'intervalVAmax': (['sum', 'sum'],),
                        'datetime': (['min', 'min'],)}
        agg = chart_util.DataAgg(input, dateIdentifier, intervals, grouping, fieldsConfig)
        EXPECTED_DAILY_DATA = [
                {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), 'intervalVAmax': 110.0},
                {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), 'intervalVAmax': 10.0},
                {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), 'intervalVAmax': 10.0},
        ]
        self.assertItemsEqual(EXPECTED_DAILY_DATA, agg.data)

    def test_merge_30_minute_then_daily_then_weekly_merge(self):
        input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},

            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 30.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 15.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0}
        ]
        weekly = 24 * 60 * 60*7
        daily = 24 * 60 * 60
        thirtyMinutesInterval = 30 * 60
        intervals = [thirtyMinutesInterval, daily, weekly]
        grouping = ['merge', 'merge', 'merge']
        dateIdentifier = 'datetime'
        fieldsConfig = {'intervalVAmax': (['sum', 'max', 'max'],),
                        'datetime': (['max', 'max', 'max'],)}
        agg = chart_util.DataAgg(input, dateIdentifier, intervals, grouping, fieldsConfig)
        EXPECTED_DAILY_DATA = [
                {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), 'intervalVAmax': 55.0},
                {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), 'intervalVAmax': 10.0},
        ]
        self.assertItemsEqual(EXPECTED_DAILY_DATA, agg.data)



    def test_merge_15_minute_then_daily_merge_accumulate(self):
        input = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 10.0, "credit": 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:59:00Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0},
        ]
        merged = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 20.0, "credit": 20.0},
            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 10.0, "credit": 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 20.0, "credit": 20.0},

            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0},
        ]

        EXPECTED_DAILY_DATA = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 30.0, "credit": 20.0},

            {"datetime": get_utc_datetime("2014-01-1T1:35:00Z"), "usage": 40.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-1T1:46:00Z"), "usage": 60.0, "credit": 20.0},

            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0},

            {"datetime": get_utc_datetime("2014-01-8T0:00:1Z"), "usage": 10.0, "credit": 10.0},
        ]

        daily = 24 * 60 * 60
        thirtyMinutesInterval = 15 * 60
        intervals = [thirtyMinutesInterval, daily ]
        grouping = ['merge', 'acc']
        dateIdentifier = 'datetime'
        fieldsConfig = {'usage': (['sum', 'sum'],),
                        'credit': (['sum', 'last'],),
                        'datetime': (['min', 'last'],)}
        agg = chart_util.DataAgg(input, dateIdentifier, intervals, grouping, fieldsConfig)

        self.assertItemsEqual(EXPECTED_DAILY_DATA, agg.data)

class DataAggregatorRollUpTest(test.TestCase):

    def test_merge_15_minute_then_daily_merge_accumulate_rollup(self):
        daily = 24 * 60 * 60
        fifteenMinutesInterval = 15 * 60
        rollUpConfig = ('circuit_id', 'queen_id', 'merge')
        grouping = ['merge', 'acc', 'rollup']
        dateIdentifier = 'datetime'
        fieldsConfig = {'usage': (['sum', 'sum', 'sum'],),
                        'credit': (['sum', 'sum', 'sum'],),
                        'datetime': (['min', 'last', 'max'],),
                        'intervalVAmax': (['max', 'max', 'max'],)}

        input = [
            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

        ]

        merged_1 = [
            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 20.0, "credit": 20.0, 'intervalVAmax': 25.0},


            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]

        acc_daily = [
            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 30.0, "credit": 30.0, 'intervalVAmax': 25.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]

        to_merge_15_rollup = [
            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 30.0, "credit": 30.0, 'intervalVAmax': 25.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
        ]

        EXPECTED_15_MINUTES_DATA = [
            {"datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 20.0, "credit": 20.0, 'intervalVAmax': 10.0},
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 30.0, "credit": 30.0, 'intervalVAmax': 25.0},

            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 20.0, "credit": 20.0, 'intervalVAmax': 10.0},
        ]

        EXPECTED_30_MINUTES_DATA = [
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 50.0, "credit": 50.0, 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 20.0, "credit": 20.0, 'intervalVAmax': 10.0},
        ]

        intervals15 = (fifteenMinutesInterval, daily, 15*60)
        agg = chart_util.DataAgg(input, dateIdentifier, intervals15, grouping, fieldsConfig, rollUpConfig)
        self.assertItemsEqual(EXPECTED_15_MINUTES_DATA, agg.data)


    def test_merge_30_minute_then_daily_merge_accumulate_rollup(self):
        daily = 24 * 60 * 60
        fifteenMinutesInterval = 15 * 60
        rollUpConfig = ('circuit_id', 'queen_id', 'merge')
        grouping = ['merge', 'acc', 'rollup']
        dateIdentifier = 'datetime'
        fieldsConfig = {'usage': (['sum', 'sum', 'sum'],),
                        'credit': (['sum', 'sum', 'sum'],),
                        'datetime': (['min', 'last', 'max'],),
                        'intervalVAmax': (['max', 'max', 'max'],)}

        input = [
            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 20.0},
            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:16:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 25.0},

            {'circuit_id': 1, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-1T1:00:00Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},
            {'circuit_id': 2, 'queen_id': 1, "datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 10.0, "credit": 10.0, 'intervalVAmax': 10.0},

        ]

        EXPECTED_30_MINUTES_DATA = [
            {"datetime": get_utc_datetime("2014-01-1T1:15:00Z"), "usage": 50.0, "credit": 50.0, 'intervalVAmax': 25.0},
            {"datetime": get_utc_datetime("2014-01-2T0:00:1Z"), "usage": 20.0, "credit": 20.0, 'intervalVAmax': 10.0},
        ]

        intervals30 = (fifteenMinutesInterval, daily, 30*60)
        agg = chart_util.DataAgg(input, dateIdentifier, intervals30, grouping, fieldsConfig, rollUpConfig)
        self.assertItemsEqual(EXPECTED_30_MINUTES_DATA, agg.data)
