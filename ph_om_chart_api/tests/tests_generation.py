"""Tests ph_om_chart_api_generation."""

from django import test
from django.core.management import call_command
from ph_om_chart_api import base_chart
from ph_om_chart_api import generation as  generation_chart
import ph_model.models as ph_model


def get_utc_datetime(utcString):
    return ph_model.fields.DateTimeUTC().to_python(utcString)


class BaseGenerationChartTest(test.TestCase):
    """Tests chart_manager.GridGenerationChart."""
    # Test configuration from ph_om_chart_api/tests/data/grid_generation_data.json
    CONFIGURED_GENERATION_MONITOR_DATA = {
            'grid': 301
    }

    def setUp(self):
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/generation_data.json', verbosity=0)

        self.user = ph_model.user.User.objects.get(pk=301)
        self.grid = ph_model.grid.Grid.objects.get(pk=301)

class GridGenerationChartTest(BaseGenerationChartTest):
    """Tests chart_manager.GridGenerationChart."""


    def tests_get_generation_data_15_minutes_interval(self):
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [15 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'whFromPV': (['SUM'],),
                            'whToGrid': (['SUM'],),
                            'vaFromPVMin': (['MIN'],),
                            'vaFromPVAvg': (['AVG'],),
                            'vaFromPVMax': (['MAX'],),
                            'vaToGridMin': (['MIN'],),
                            'vaToGridAvg': (['AVG'],),
                            'vaToGridMax': (['MAX'],),
                            'vacMin': (['MIN'],),
                            'vacAvg': (['AVG'],),
                            'vacMax': (['MAX'],),

                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],)
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)

        EXPECTED_GRID_MONITOR_15_MINUTES_DATA = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                "whFromPV": 0.0,  "whToGrid": 0.0, "vaFromPVMin": 0.0, "vaFromPVAvg": 0.0, "vaFromPVMax": 0.0, "vaToGridMin": 0.0,
                "vaToGridAvg": 0.0, "vaToGridMax": 0.0, "vacMin": 0.0,"vacAvg": 0.0, "vacMax": 0.0, "socMin": 0.0, "socAvg": 0.0, "socMax": 0.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"),
                "whFromPV":15.0,  "whToGrid":15.0, "vaFromPVMin":15.0, "vaFromPVAvg":15.0, "vaFromPVMax":15.0, "vaToGridMin":15.0,
                "vaToGridAvg": 15.0, "vaToGridMax": 15.0, "vacMin": 15.0,"vacAvg": 15.0, "vacMax": 15.0, "socMin": 15.0, "socAvg": 15.0, "socMax": 15.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                "whFromPV":30.0,  "whToGrid":30.0, "vaFromPVMin":30.0, "vaFromPVAvg":30.0, "vaFromPVMax":30.0, "vaToGridMin":30.0,
                "vaToGridAvg": 30.0, "vaToGridMax": 30.0, "vacMin": 30.0,"vacAvg": 30.0, "vacMax": 30.0, "socMin": 30.0, "socAvg": 30.0, "socMax": 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"),
                "whFromPV":45.0,  "whToGrid":45.0, "vaFromPVMin":45.0, "vaFromPVAvg":45.0, "vaFromPVMax":45.0, "vaToGridMin":45.0,
                "vaToGridAvg": 45.0, "vaToGridMax": 45.0, "vacMin": 45.0,"vacAvg": 45.0, "vacMax": 45.0, "socMin": 45.0, "socAvg": 45.0, "socMax": 45.0},
        ]
        gridGenerationChart = generation_chart.GridGenerationChart(self.user, self.grid.id, queryConfig, model=ph_model.grid.Grid)
        data = gridGenerationChart.get_data()
        self.assertItemsEqual(data, EXPECTED_GRID_MONITOR_15_MINUTES_DATA)


    def tests_get_generation_data_30_minutes_interval(self):
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'whFromPV': (['SUM'],),
                            'whToGrid': (['SUM'],),
                            'vaFromPVMin': (['MIN'],),
                            'vaFromPVAvg': (['AVG'],),
                            'vaFromPVMax': (['MAX'],),
                            'vaToGridMin': (['MIN'],),
                            'vaToGridAvg': (['AVG'],),
                            'vaToGridMax': (['MAX'],),
                            'vacMin': (['MIN'],),
                            'vacAvg': (['AVG'],),
                            'vacMax': (['MAX'],),

                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],)
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        EXPECTED_GRID_MONITOR_30_MINUTES_DATA = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                "whFromPV": 15.0,  "whToGrid": 15.0, "vaFromPVMin": 0.0, "vaFromPVAvg": 7.5, "vaFromPVMax": 15.0, "vaToGridMin": 0.0,
                "vaToGridAvg": 7.5, "vaToGridMax": 15.0, "vacMin": 0.0,"vacAvg": 7.5, "vacMax": 15.0, "socMin": 0.0, "socAvg": 7.5, "socMax": 15.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                "whFromPV": 75.0,  "whToGrid": 75.0, "vaFromPVMin": 30.0, "vaFromPVAvg": 37.5, "vaFromPVMax": 45.0, "vaToGridMin": 30.0,
                "vaToGridAvg": 37.5, "vaToGridMax": 45.0, "vacMin": 30.0,"vacAvg": 37.5, "vacMax": 45.0, "socMin": 30.0, "socAvg": 37.5, "socMax": 45.0},
        ]

        gridGenerationChart = generation_chart.GridGenerationChart(self.user, self.grid.id, queryConfig, model=ph_model.grid.Grid)
        data = gridGenerationChart.get_data()
        self.assertItemsEqual(data, EXPECTED_GRID_MONITOR_30_MINUTES_DATA)
