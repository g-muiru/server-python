from django.core.urlresolvers import reverse
from django.core.management import call_command
import json
from ph_common_api import acl
from rest_framework import test as rest_framework_test
from rest_framework import status
from ph_om_chart_api import base_chart


class BaseTestView(rest_framework_test.APITestCase):
    """Base model for view test classes"""

    URL = '{}?queryConfig={}'

    def _get_collect_time_query_config(self):
         queryConfig = {
                     base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                     base_chart.ChartQueryConfig.FROM_DATE : '20140102',
                     base_chart.ChartQueryConfig.INTERVALS: [15*30],
                     base_chart.ChartQueryConfig.GROUPING : ['merge'],
                     base_chart.ChartQueryConfig.FIELDS_CONFIG: {'collectTime': (['MIN'],)},
                     base_chart.ChartQueryConfig.ROLL_UP_CONFIG: None
                      }
         queryAsString = json.dumps(queryConfig)
         return queryAsString


class APIUsageChartEndPointTest(BaseTestView):
     """Tests usage chart api endpoint urls."""

     def setUp(self):
        """Initialize common tests utilities."""
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/circuit_usage_data.json', verbosity=0)


      # permission based devices loaded from fixtures/circuit_usage_data.json
     DEVICE_USAGE_CHART_END_POINTS = [
         {'urlId': 'om-chart-usage', 'args': ('grid', 101,)},
         {'urlId': 'om-chart-usage', 'args': ('queen', 101,)},
         {'urlId': 'om-chart-usage', 'args': ('circuit', 101,)},
     ]

     def test_usage_chart_end_point_get_success(self):
         self.client.login(username='user101', password='user101')
         for endPoint in self.DEVICE_USAGE_CHART_END_POINTS:
            url = self.URL.format(reverse(endPoint['urlId'], args=endPoint.get('args')), self._get_collect_time_query_config())
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)


     def atest_usage_chart_end_point_get_access_denied(self):
         self.client.login(username='user103', password='user103')
         for endPoint in self.DEVICE_USAGE_CHART_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            #self.assertRaises(acl.AccessDenied, self.client.get, url)
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, url)


class APIMonitorChartEndPointTest(BaseTestView):
     """Tests monitor chart api endpoint urls."""

     def setUp(self):
        """Initialize common tests utilities."""
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/monitor_data.json', verbosity=0)


     # permission based  monitor devices data loaded from fixtures/monitor_data.json
     DEVICE_MONITOR_CHART_END_POINTS = [
         {'urlId': 'om-chart-device-monitor', 'args': ('queen', 201,)},
         {'urlId': 'om-chart-device-monitor', 'args': ('inverter', 201,)},
         {'urlId': 'om-chart-device-monitor', 'args': ('probe', 201,)},
         {'urlId': 'om-chart-device-monitor', 'args': ('circuit', 201,)},
         {'urlId': 'om-chart-device-monitor', 'args': ('batterybank', 201,)},


         {'urlId': 'om-chart-agg-monitor', 'args': ('grid', 201, 'queen')},
         {'urlId': 'om-chart-agg-monitor', 'args': ('grid', 201, 'probe')},
         {'urlId': 'om-chart-agg-monitor', 'args': ('grid', 201, 'circuit')},
         {'urlId': 'om-chart-agg-monitor', 'args': ('grid', 201, 'inverter')},
         {'urlId': 'om-chart-agg-monitor', 'args': ('grid', 201, 'batterybank')},


         {'urlId': 'om-chart-agg-monitor', 'args': ('queen', 201, 'probe')},
         {'urlId': 'om-chart-agg-monitor', 'args': ('queen', 201, 'circuit')},

         {'urlId': 'om-chart-agg-monitor', 'args': ('probe', 201, 'circuit')},

         {'urlId': 'om-chart-agg-monitor', 'args': ('powerstation', 201, 'inverter')},
         {'urlId': 'om-chart-agg-monitor', 'args': ('powerstation', 201, 'batterybank')},

     ]

     def test_monitor_chart_end_point_get_success(self):
         self.client.login(username='user103', password='user103')
         for endPoint in self.DEVICE_MONITOR_CHART_END_POINTS:
            url = self.URL.format(reverse(endPoint['urlId'], args=endPoint.get('args')),
                                        self._get_collect_time_query_config())
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)


     def test_monitor_chart_end_point_get_access_denied(self):
         self.client.login(username='user202', password='user202')
         for endPoint in self.DEVICE_MONITOR_CHART_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            #self.assertRaises(acl.AccessDenied, self.client.get, url)
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, url)



class APIGenerationChartEndPointTest(BaseTestView):
     """Tests generation chart api endpoint urls."""

     def setUp(self):
        """Initialize common tests utilities."""
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/generation_data.json', verbosity=0)


     # permission based  monitor devices data loaded from fixtures/generation_data.json
     DEVICE_GENERATION_CHART_END_POINTS = [
         {'urlId': 'om-chart-device-generation', 'args': ('grid', 301,)}
     ]

     def test_monitor_chart_end_point_get_success(self):
         self.client.login(username='user301', password='user301')
         for endPoint in self.DEVICE_GENERATION_CHART_END_POINTS:
            url  = self.URL.format(reverse(endPoint['urlId'], args=endPoint.get('args')),
                                   self._get_collect_time_query_config())
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)


     def test_monitor_chart_end_point_get_access_denied(self):
         self.client.login(username='user302', password='user202')
         for endPoint in self.DEVICE_GENERATION_CHART_END_POINTS:
            url = reverse(endPoint['urlId'], args=endPoint.get('args'))
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, url)


class APIAggregateGridChartEndPointTest(BaseTestView):
     """Tests aggregate chart api endpoint urls."""

     def setUp(self):
        """Initialize common tests utilities."""
        call_command('loaddata', 'ph_model/fixtures/common/json/event_types.json', verbosity=0)
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/aggregate_grid_config.json', verbosity=0)

     def _get_collect_time_query_config(self):
         queryConfig = {
                       base_chart.ChartQueryConfig.DATE_IDENTIFIER: "aggregateDate",
                       base_chart.ChartQueryConfig.DATE_IDENTIFIER_TYPE: "date",
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140102',
                       base_chart.ChartQueryConfig.INTERVALS:  [24 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'aggregateDate': (['MIN'],),
                            'energyUsed': (['SUM'],)
                       }
         }
         queryAsString = json.dumps(queryConfig)
         return queryAsString

     # permission based  monitor devices data loaded from fixtures/aggregate_grid_config.json
     DEVICE_GRID_AGGREGATE_CHART_END_POINTS = [
         {'urlId': 'om-chart-aggregate-grid-daily-report', 'args': ('grid', 1000,)}
     ]

     def test_monitor_chart_end_point_get_success(self):
         self.client.login(username='user1000', password='user1000')
         for endPoint in self.DEVICE_GRID_AGGREGATE_CHART_END_POINTS:
            url  = self.URL.format(reverse(endPoint['urlId'], args=endPoint.get('args')),
                                   self._get_collect_time_query_config())
            response = self.client.get(url)
            self.assertEquals(response.status_code, status.HTTP_200_OK, url)

