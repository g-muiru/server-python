# coding=utf-8
"""Tests ph_mo_api.test_manager."""

from django import test
import pytz
from django.core.management import call_command
from ph_om_chart_api import base_chart
from ph_om_chart_api import usage as usage_chart
import ph_model.models as ph_model


def get_utc_datetime(utc_string):
    """

    :param utc_string:
    :return:
    """
    return ph_model.fields.DateTimeUTC().to_python(utc_string)


def get_utc_date(utc_string):
    """

    :param utc_string:
    :return:
    """
    return ph_model.fields.DateTimeUTC().to_python(utc_string).date()


class BaseTestChart(test.TestCase):
    """Base model for view test classes"""

    def setUp(self):
        """

        :return:
        """
        call_command('loaddata', 'ph_model/fixtures/common/json/tariff.json', verbosity=0)
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/circuit_usage_data.json', verbosity=0)

        self.user = ph_model.user.User.objects.get(pk=101)
        self.customer = ph_model.customer.Customer.objects.get(pk=101)
        self.grid = ph_model.queen.Queen.objects.get(pk=101)
        self.queen = ph_model.queen.Queen.objects.get(pk=101)
        self.circuitWithCustomer = ph_model.circuit.Circuit.objects.get(pk=101)
        self.circuitWithOutCustomer = ph_model.circuit.Circuit.objects.get(pk=102)

    @staticmethod
    def _get_query_config(from_date, intervals, grouping, fields_config, rollup_config=None, look_back_days=3):
        """

        :param from_date:
        :param intervals:
        :param grouping:
        :param fields_config:
        :param rollup_config:
        :param look_back_days:
        :return:
        """
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: look_back_days,
            base_chart.ChartQueryConfig.FROM_DATE: from_date,
            base_chart.ChartQueryConfig.INTERVALS: intervals,
            base_chart.ChartQueryConfig.GROUPING: grouping,
            base_chart.ChartQueryConfig.FIELDS_CONFIG: fields_config,
            base_chart.ChartQueryConfig.ROLL_UP_CONFIG: rollup_config
        }
        query_config = base_chart.ChartQueryConfig(query_config)
        return query_config


# noinspection PyPep8Naming
class CircuitUsageChartTest(BaseTestChart):
    """Tests chart_manager.CircuitUsageChart."""

    CONFIGURED_USAGE_DATA_FROM_FIXTURE_WITH_CUSTOMERS = [
        {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 20.0, 'intervalVAmax': 20.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 30.0, 'intervalVAmax': 30.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 40.0, 'intervalVAmax': 40.0},
    ]
    CONFIGURED_USAGE_DATA_FROM_FIXTURE_WITH_OUT_CUSTOMERS = [
        {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 0.0, 'intervalVAmax': 20.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 0.0, 'intervalVAmax': 30.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 0.0, 'intervalVAmax': 40.0}
    ]

    def test_get_circuit_usage_data_with_customer(self):
        """

        :return:
        """

        fields_config = {'intervalVAmax': (['MAX'],),
                         "intervalWh": (['SUM'],),
                         'charge__amount': (['SUM'],),
                         'collectTime': (['MIN'],)}

        query_config = self._get_query_config('20140101', [15 * 60], ['merge'], fields_config)
        EXPECTED_USAGE_DATA = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 20.0, 'intervalVAmax': 20.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 30.0, 'intervalVAmax': 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 40.0, 'intervalVAmax': 40.0}
        ]
        circuit_chart_manager = usage_chart.CircuitUsageChart(self.user, self.circuitWithCustomer.id, query_config,
                                                              usage_type=ph_model.usage.USAGE_TYPE.CUSTOMER)
        data = circuit_chart_manager.get_data()
        self.assertEquals(data, EXPECTED_USAGE_DATA)

    # noinspection PyPep8Naming
    def test_get_circuit_usage_data_with_out_customer(self):
        """

        :return:
        """
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
            base_chart.ChartQueryConfig.FROM_DATE: '20140101',
            base_chart.ChartQueryConfig.INTERVALS: [15 * 60],
            base_chart.ChartQueryConfig.GROUPING: ['merge'],
            base_chart.ChartQueryConfig.FIELDS_CONFIG: {'intervalVAmax': (['MAX'],),
                                                        "intervalWh": (['SUM'],),
                                                        'charge__amount': (['SUM'],),
                                                        'collectTime': (['MIN'],)}
        }
        query_config = base_chart.ChartQueryConfig(query_config)
        EXPECTED_USAGE_DATA = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 0.0, 'intervalVAmax': 20.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 0.0, 'intervalVAmax': 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 0.0, 'intervalVAmax': 40.0}
        ]
        circuit_chart_manager = usage_chart.CircuitUsageChart(self.user, self.circuitWithOutCustomer.id, query_config,
                                                              usage_type=ph_model.usage.USAGE_TYPE.NO_CUSTOMER)
        data = circuit_chart_manager.get_data()
        self.assertEquals(data, EXPECTED_USAGE_DATA)

    def test_get_cumulative_daily_usage_data_with_customer(self):
        """

        :return:
        """
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
            base_chart.ChartQueryConfig.FROM_DATE: '20140102',
            base_chart.ChartQueryConfig.INTERVALS: [15 * 60, 60 * 60 * 24],
            base_chart.ChartQueryConfig.GROUPING: ['merge', 'acc'],
            base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                "intervalWh": (['SUM', 'SUM'],),
                'charge__accountBalanceAfter': (['LAST', 'LAST'],),
                'collectTime': (['MIN', 'LAST'],)}
        }
        query_config = base_chart.ChartQueryConfig(query_config)
        ACCOUNT_ONE_AVAILABLE_CREDIT_BEFORE_USAGE = 100.0

        EXPECTED_CIRCUIT_CUM_DATA_WITH_CUSTOMER = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z").astimezone(pytz.timezone('America/Los_Angeles')), "intervalWh": 10.0,
             "charge__accountBalanceAfter": ACCOUNT_ONE_AVAILABLE_CREDIT_BEFORE_USAGE - 10.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z").astimezone(pytz.timezone('America/Los_Angeles')), "intervalWh": 30.0,
             "charge__accountBalanceAfter": ACCOUNT_ONE_AVAILABLE_CREDIT_BEFORE_USAGE - 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z").astimezone(pytz.timezone('America/Los_Angeles')), "intervalWh": 60.0,
             "charge__accountBalanceAfter": ACCOUNT_ONE_AVAILABLE_CREDIT_BEFORE_USAGE - 60.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z").astimezone(pytz.timezone('America/Los_Angeles')), "intervalWh": 100.0,
             "charge__accountBalanceAfter": ACCOUNT_ONE_AVAILABLE_CREDIT_BEFORE_USAGE - 60.0},
            {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z").astimezone(pytz.timezone('America/Los_Angeles')), "intervalWh": 10.0,
             "charge__accountBalanceAfter": ACCOUNT_ONE_AVAILABLE_CREDIT_BEFORE_USAGE - 100.0 - 10.0}
        ]

        circuit_chart_manager = usage_chart.CircuitUsageChart(self.user, self.circuitWithCustomer.id, query_config,
                                                              usage_type=ph_model.usage.USAGE_TYPE.CUSTOMER)
        data = circuit_chart_manager.get_data()
        self.assertEquals(data, EXPECTED_CIRCUIT_CUM_DATA_WITH_CUSTOMER)

    def test_get_cumulative_daily_usage_data_with_out_customer(self):
        """

        :return:
        """
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
            base_chart.ChartQueryConfig.FROM_DATE: '20140102',
            base_chart.ChartQueryConfig.INTERVALS: [15 * 60, 60 * 60 * 24],
            base_chart.ChartQueryConfig.GROUPING: ['merge', 'acc'],
            base_chart.ChartQueryConfig.FIELDS_CONFIG: {"intervalWh": (['SUM', 'SUM'],),
                                                        'intervalVAmax': (['max', 'max'],),
                                                        'charge__amount': (['sum', 'sum'],),
                                                        'collectTime': (['MIN', 'LAST'],)}
        }
        query_config = base_chart.ChartQueryConfig(query_config)

        EXPECTED_CIRCUIT_CUM_DATA_WITH_OUT_CUSTOMER = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 30.0, "charge__amount": 0.0, 'intervalVAmax': 20},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 60.0, "charge__amount": 0.0, 'intervalVAmax': 30},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 100.0, "charge__amount": 0.0, 'intervalVAmax': 40},
            {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10}
        ]

        circuit_chart_manager = usage_chart.CircuitUsageChart(self.user, self.circuitWithOutCustomer.id, query_config,
                                                              usage_type=ph_model.usage.USAGE_TYPE.NO_CUSTOMER)
        data = circuit_chart_manager.get_data()
        self.assertEquals(data, EXPECTED_CIRCUIT_CUM_DATA_WITH_OUT_CUSTOMER)


class QueenUsageChartTest(BaseTestChart):
    """Tests chart_manager.QueenUsageChart."""
    QUEEN_ID = 101
    QUEEN_CIRCUITS = [101, 102]
    CONFIGURED_USAGE_DATA_FROM_FIXTURE_WITH_CUSTOMERS_CIRCUIT_101 = [
        {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 20.0, 'intervalVAmax': 20.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 30.0, 'intervalVAmax': 30.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 40.0, 'intervalVAmax': 40.0},
        {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
    ]
    CONFIGURED_USAGE_DATA_FROM_FIXTURE_WITH_NON_CUSTOMERS_CIRCUIT_102 = [
        {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 0.0, 'intervalVAmax': 20.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 0.0, 'intervalVAmax': 30.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 0.0, 'intervalVAmax': 40.0},
        {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10.0},
    ]

    def test_get_circuit_usage_data_with_customer_and_non_customer(self):
        """
        
        :return:
        """
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
            base_chart.ChartQueryConfig.FROM_DATE: '20140101',
            base_chart.ChartQueryConfig.INTERVALS: [15 * 60],
            base_chart.ChartQueryConfig.GROUPING: ['merge'],
            base_chart.ChartQueryConfig.FIELDS_CONFIG: {'intervalWh': (['SUM'],),
                                                        'intervalVAmax': (['MAX'],),
                                                        'charge__amount': (['SUM'],),
                                                        'collectTime': (['MIN'],)}
        }
        query_config = base_chart.ChartQueryConfig(query_config)
        EXPECTED_USAGE_DATA = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 20.0, "charge__amount": 10.0, "intervalVAmax": 10.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 40.0, "charge__amount": 20.0, "intervalVAmax": 20.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 60.0, "charge__amount": 30.0, "intervalVAmax": 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 80.0, "charge__amount": 40.0, "intervalVAmax": 40.0},
        ]

        queenChartManager = usage_chart.QueenUsageChart(self.user, self.QUEEN_ID, query_config)
        data = queenChartManager.get_data()
        self.assertEquals(data, EXPECTED_USAGE_DATA)

    def test_get_cumulative_daily_rolled_up_usage_customer_data(self):
        fields_config = {'intervalWh': (['sum', 'sum', 'sum'],),
                         'charge__amount': (['sum', 'sum', 'sum'],),
                         'collectTime': (['min', 'last', 'max'],),
                         'intervalVAmax': (['max', 'max', 'max'],)}
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
            base_chart.ChartQueryConfig.FROM_DATE: '20140102',
            base_chart.ChartQueryConfig.ROLL_UP_CONFIG: ('circuit_id', 'circuit__queen_id', 'merge'),
            base_chart.ChartQueryConfig.INTERVALS: [15 * 60, 60 * 60 * 24, 15 * 60],
            base_chart.ChartQueryConfig.GROUPING: ['merge', 'acc', 'rollup'],
            base_chart.ChartQueryConfig.FIELDS_CONFIG: fields_config
        }
        query_config = base_chart.ChartQueryConfig(query_config)
        EXPECTED_CIRCUIT_CUM_DATA = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 30.0, "charge__amount": 30.0, 'intervalVAmax': 20.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 60.0, "charge__amount": 60.0, 'intervalVAmax': 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 100.0, "charge__amount": 100.0, 'intervalVAmax': 40.0},
            {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
        ]

        queenChartManager = usage_chart.QueenUsageChart(self.user, self.queen.id, query_config,
                                                        usage_type=ph_model.usage.USAGE_TYPE.CUSTOMER)
        data = queenChartManager.get_data()
        self.assertEquals(data, EXPECTED_CIRCUIT_CUM_DATA)

    def test_get_cumulative_daily_rolled_up_usage_customer_and_non_customer_data(self):
        fields_config = {'intervalWh': (['sum', 'sum', 'sum'],),
                         'charge__amount': (['sum', 'sum', 'sum'],),
                         'collectTime': (['min', 'last', 'max'],),
                         'intervalVAmax': (['max', 'max', 'max'],)}
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
            base_chart.ChartQueryConfig.FROM_DATE: '20140102',
            base_chart.ChartQueryConfig.ROLL_UP_CONFIG: ('circuit_id', 'circuit__queen_id', 'merge'),
            base_chart.ChartQueryConfig.INTERVALS: [15 * 60, 60 * 60 * 24, 15 * 60],
            base_chart.ChartQueryConfig.GROUPING: ['merge', 'acc', 'rollup'],
            base_chart.ChartQueryConfig.FIELDS_CONFIG: fields_config
        }
        query_config = base_chart.ChartQueryConfig(query_config)

        EXPECTED_CIRCUIT_CUM_DATA = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 20.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 60.0, "charge__amount": 30.0, 'intervalVAmax': 20.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 120.0, "charge__amount": 60.0, 'intervalVAmax': 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 200.0, "charge__amount": 100.0, 'intervalVAmax': 40.0},
            {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 20.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
        ]

        queenChartManager = usage_chart.QueenUsageChart(self.user, self.QUEEN_ID, query_config)
        data = queenChartManager.get_data()
        self.assertEquals(data, EXPECTED_CIRCUIT_CUM_DATA)


# TODO(estifanos) test grid rollup
class GridUsageChartTest(BaseTestChart):
    """Tests chart_manager.QueenUsageChart."""
    GRID_ID = 101
    GRID_QUEENS = [101, 102]

    QUEEN_101_CIRCUITS = [101, 102]
    QUEEN_102_CIRCUITS = [103]

    CONFIGURED_USAGE_DATA_FROM_FIXTURE_WITH_CUSTOMERS_CIRCUIT_101 = [
        {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 20.0, 'intervalVAmax': 20.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 30.0, 'intervalVAmax': 30.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 40.0, 'intervalVAmax': 40.0},
        {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 10.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},
    ]
    CONFIGURED_USAGE_DATA_FROM_FIXTURE_WITH_NON_CUSTOMERS_CIRCUIT_102 = [
        {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 20.0, "charge__amount": 0.0, 'intervalVAmax': 20.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 30.0, "charge__amount": 0.0, 'intervalVAmax': 30.0},
        {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 40.0, "charge__amount": 0.0, 'intervalVAmax': 40.0},
        {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 10.0, "charge__amount": 0.0, 'intervalVAmax': 10.0},
    ]
    CONFIGURED_USAGE_DATA_FROM_FIXTURE_WITH_CUSTOMERS_CIRCUIT_103 = [
        {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 100.0, "charge__amount": 100.0, 'intervalVAmax': 100.0}
    ]

    def test_get_circuit_usage_data_with_customer_and_without_customer(self):
        query_config = {
            base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
            base_chart.ChartQueryConfig.FROM_DATE: '20140102',
            base_chart.ChartQueryConfig.INTERVALS: [15 * 60],
            base_chart.ChartQueryConfig.GROUPING: ['merge'],
            base_chart.ChartQueryConfig.FIELDS_CONFIG: {'intervalWh': (['SUM'],),
                                                        'intervalVAmax': (['MAX'],),
                                                        'charge__amount': (['SUM'],),
                                                        'collectTime': (['MIN'],)}
        }
        query_config = base_chart.ChartQueryConfig(query_config)
        EXPECTED_USAGE_DATA = [
            {"collectTime": get_utc_datetime("2014-01-1T1:00:00Z"), "intervalWh": 20.0, "charge__amount": 10.0, "intervalVAmax": 10.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "intervalWh": 40.0, "charge__amount": 20.0, "intervalVAmax": 20.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "intervalWh": 60.0, "charge__amount": 30.0, "intervalVAmax": 30.0},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "intervalWh": 180.0, "charge__amount": 140.0, "intervalVAmax": 100.0},
            {"collectTime": get_utc_datetime("2014-01-2T0:0:00Z"), "intervalWh": 20.0, "charge__amount": 10.0, 'intervalVAmax': 10.0},

        ]

        queenChartManager = usage_chart.GridUsageChart(self.user, self.GRID_ID, query_config)
        data = queenChartManager.get_data()
        self.assertEquals(data, EXPECTED_USAGE_DATA)
