"""Tests ph_om_chart_api_aggregate_grid."""

from django import test
from django.core.management import call_command
from ph_om_chart_api import base_chart
from ph_om_chart_api import aggregate_grid as  aggregate_grid_chart
import ph_model.models as ph_model
import datetime
import decimal


def get_utc_datetime(utcString):
    return ph_model.fields.DateTimeUTC().to_python(utcString)


class BaseAggregateGridChartTest(test.TestCase):
    """Tests chart_manager.GridAggregateGridChart."""

    def setUp(self):
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/aggregate_grid_config.json', verbosity=0)

        self.user = ph_model.user.User.objects.get(pk=1000)
        self.grid = ph_model.grid.Grid.objects.get(pk=1000)

class AggregateGridChartTest(BaseAggregateGridChartTest):
    """Tests chart_manager.GridAggregateGridChart."""


    def tests_grid_aggregate_data_daily_interval(self):
        queryConfig = {
                       base_chart.ChartQueryConfig.DATE_IDENTIFIER: "aggregateDate",
                       base_chart.ChartQueryConfig.DATE_IDENTIFIER_TYPE: "date",
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140102',
                       base_chart.ChartQueryConfig.INTERVALS:  [24 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'aggregateDate': (['MIN'],),
                            'energyUsed': (['SUM'],)
                       }
        }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        EXPECTED_GRID_AGGREGATE__DATA = [{'aggregateDate': datetime.date(2014, 1, 2),
                                         'energyUsed': decimal.Decimal('50.0')}]
        gridAggregateGridChart = aggregate_grid_chart.AggregateGridChart(self.user, self.grid.id, queryConfig, model=ph_model.grid.Grid)
        data = gridAggregateGridChart.get_data()
        self.assertItemsEqual(data, EXPECTED_GRID_AGGREGATE__DATA)
