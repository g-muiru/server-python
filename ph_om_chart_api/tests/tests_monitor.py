"""Tests ph_mo_api.test_manager."""

from django import test
from django.core.management import call_command
from ph_om_chart_api import monitor as  monitor_chart
from ph_om_chart_api import base_chart
import ph_model.models as ph_model


def get_utc_datetime(utcString):
    return ph_model.fields.DateTimeUTC().to_python(utcString)


class BaseMonitorChartTest(test.TestCase):
    """Tests chart_manager.QueenMonitorChart."""
    # Test configuration from ph_om_chart_api/tests/data/queen_monitor_data.json
    CONFIGURED_QUEEN_MONITOR_DATA = {
            'powerstation': {'inverter': [201, 202]},
            'queen201': {'user': 201, 'monitor': [101, 102, 103, 104], 'grid': 201, 'probes': [201, 202]},
            'queen202': {'user': 201, 'monitor': [105, 106, 107, 108], 'grid': 201, 'probes': []},
            'probe201': {'probeMonitor': [101, 102]},
            'probe202': {'probeMonitor': [103, 104]},
    }

    def setUp(self):
        #call_command('loaddata', 'ph_model/fixtures/common/json/initial_data.json', verbosity=0)
        call_command('loaddata', 'ph_om_chart_api/tests/fixtures/monitor_data.json', verbosity=0)

        self.user = ph_model.user.User.objects.get(pk=103)
        self.grid = ph_model.grid.Grid.objects.get(pk=201)  ###TODO WTF this is a Queen instance
        self.queen = ph_model.queen.Queen.objects.get(pk=201)
        self.probe = ph_model.probe.Probe.objects.get(pk=201)
        self.circuit = ph_model.circuit.Circuit.objects.get(pk=201)
        self.powerStation = ph_model.powerstation.PowerStation.objects.get(pk=201)
        #TODO(estifanos): Currently properties specific to either inverters are handled symmetrically check if is an issue.
        self.solarInverter = ph_model.inverter.Inverter.objects.get(pk=201)
        self.batteryInverter = ph_model.inverter.Inverter.objects.get(pk=202)
        self.batteryBank = ph_model.battery.BatteryBank.objects.get(pk=201)


class QueenMonitorChartTest(BaseMonitorChartTest):
    """Tests chart_manager.QueenMonitorChart."""


    def tests_get_monitor_data_15_minutes_interval(self):
        EXPECTED_QUEEN_MONITOR_15_MINUTES_DATA = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "rssiMin": 0.0, "tempAvg": 0.0, "commErrors": 0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "rssiMin": 15.0, "tempAvg": 15.0, "commErrors": 7,},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "rssiMin": 30.0, "tempAvg": 30.0, "commErrors": 15, },
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "rssiMin": 45.0, "tempAvg": 45.0, "commErrors": 22,},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [15 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'rssiMin': (['min'],),
                            'tempAvg': (['AVG'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        queenMonitorChart = monitor_chart.QueenMonitorChart(self.user, self.queen.id, queryConfig, model= ph_model.queen.Queen)
        data = queenMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_QUEEN_MONITOR_15_MINUTES_DATA)


    def tests_get_monitor_data_30_minutes_aggregated_by_grid(self):
        EXPECTED_QUEEN_MONITOR_30_MINUTES_DATA_AGG_BY_GRID = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "rssiMin": 0.0, "tempAvg": 7.5, "commErrors": 14},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "rssiMin": 30.0, "tempAvg": 37.5,"commErrors": 74,},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'rssiMin': (['min'],),
                            'tempAvg': (['AVG'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        queenMonitorChart = monitor_chart.QueenMonitorChart(self.user, self.grid.id, queryConfig, model= ph_model.grid.Grid)
        data = queenMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_QUEEN_MONITOR_30_MINUTES_DATA_AGG_BY_GRID)

class ProbeMonitorChartTest(BaseMonitorChartTest):
    """Tests chart_manager.ProbeMonitorChart."""

    def tests_get_monitor_data_15_minutes_interval(self):
        EXPECTED_PROBE_MONITOR_15_MINUTES_DATA = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 0, "commErrors": 0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "commAttempts": 15, "commErrors": 7},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 30, "commErrors": 15},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "commAttempts": 45, "commErrors": 22},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [15 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        probeMonitorChart = monitor_chart.ProbeMonitorChart(self.user,self.probe.id, queryConfig, model= ph_model.probe.Probe)
        data = probeMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_PROBE_MONITOR_15_MINUTES_DATA)


    def tests_get_monitor_data_30_minutes_interval(self):
        EXPECTED_PROBE_MONITOR_30_MINUTES_DATA = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 15, "commErrors": 7},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 75, "commErrors": 37},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        probeMonitorChart = monitor_chart.ProbeMonitorChart(self.user,self.probe.id, queryConfig, model= ph_model.probe.Probe)
        data = probeMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_PROBE_MONITOR_30_MINUTES_DATA)

    def tests_get_monitor_data_30_minutes_interval_aggregated_by_queen(self):
        EXPECTED_PROBE_MONITOR_30_MINUTES_DATA_AGG_BY_QUEEN = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 30, "commErrors": 14},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 150, "commErrors": 74},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        probeMonitorChart = monitor_chart.ProbeMonitorChart(self.user,self.queen.id, queryConfig, model= ph_model.queen.Queen)
        data = probeMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_PROBE_MONITOR_30_MINUTES_DATA_AGG_BY_QUEEN)

    def tests_get_monitor_data_30_minutes_interval_aggregated_by_grid(self):
        EXPECTED_PROBE_MONITOR_30_MINUTES_DATA_AGG_BY_GRID = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 30, "commErrors": 14},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 150, "commErrors": 74},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        probeMonitorChart = monitor_chart.ProbeMonitorChart(self.user, self.grid.id, queryConfig, model= ph_model.grid.Grid)
        data = probeMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_PROBE_MONITOR_30_MINUTES_DATA_AGG_BY_GRID)


class CircuitMonitorChartTest(BaseMonitorChartTest):
    """Tests chart_manager.CircuitMonitorChart."""

    def tests_get_monitor_data_15_minutes_interval(self):
        EXPECTED_CIRCUIT_MONITOR_15_MINUTES_DATA = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 0, "commErrors": 0},
            {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"), "commAttempts": 15, "commErrors": 7},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 30, "commErrors": 15},
            {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"), "commAttempts": 45, "commErrors": 22},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [15 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        circuitMonitorChart = monitor_chart.CircuitMonitorChart(self.user, self.circuit.id, queryConfig, ph_model.circuit.Circuit)
        data = circuitMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_CIRCUIT_MONITOR_15_MINUTES_DATA)


    def tests_get_monitor_data_30_minutes_interval(self):
        EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 15, "commErrors": 7},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 75, "commErrors": 37},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        circuitMonitorChart = monitor_chart.CircuitMonitorChart(self.user, self.circuit.id, queryConfig, ph_model.circuit.Circuit)
        data = circuitMonitorChart.get_data()
        self.assertEquals(data, EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA)

    def tests_get_monitor_data_30_minutes_interval_aggregated_by_probe(self):
        EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA_AGG_BY_PROBE= [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 30, "commErrors": 14},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 150, "commErrors": 74},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        circuitMonitorChart = monitor_chart.CircuitMonitorChart(self.user, self.probe.id, queryConfig, ph_model.probe.Probe)
        data = circuitMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA_AGG_BY_PROBE)

    def tests_get_monitor_data_30_minutes_interval_aggregated_by_queen(self):
        EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA_AGG_BY_QUEEN = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 30, "commErrors": 14},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 150, "commErrors": 74},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        circuitMonitorChart = monitor_chart.CircuitMonitorChart(self.user, self.queen.id, queryConfig, ph_model.queen.Queen)
        data = circuitMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA_AGG_BY_QUEEN)

    def tests_get_monitor_data_30_minutes_interval_aggregated_by_grid(self):
        EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA_AGG_BY_GRID = [
            {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"), "commAttempts": 30, "commErrors": 14},
            {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"), "commAttempts": 150, "commErrors": 74},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        circuitMonitorChart = monitor_chart.CircuitMonitorChart(self.user, self.grid.id, queryConfig, ph_model.grid.Grid)
        data = circuitMonitorChart.get_data()
        self.assertItemsEqual(data, EXPECTED_CIRCUIT_MONITOR_30_MINUTES_DATA_AGG_BY_GRID)



class InverterMonitorChartTest(BaseMonitorChartTest):
    """Tests chart_manager.InverterChart."""

    def tests_get_monitor_data_15_minutes_interval(self):
        EXPECTED_INVERTER_15_MINUTES_MONITOR_DATA = [
                {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                    "commAttempts": 0, "commErrors": 0, "whFromInverter": 0,
                    "vaFromInverterMin": 0.0, "vaFromInverterAvg": 0.0, "vaFromInverterMax": 0.0,
                    "vacMin": 0.0, "vacAvg": 0.0, "vacMax": 0.0, "vdcMin": 0.0, "vdcAvg": 0.0, "vdcMax": 0.0,
                    "idcMin": 0.0, "idcAvg": 0.0, "idcMax": 0.0, "socAvg": 0.0, "socMin": 0.0, "socMax": 0.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"),
                    "commAttempts": 15, "commErrors": 7, "whFromInverter": 15.0,
                    "vaFromInverterMin": 15.0, "vaFromInverterAvg": 15.0, "vaFromInverterMax": 15.0,
                    "vacMin": 15.0, "vacAvg": 15.0, "vacMax": 15.0, "vdcMin": 15.0, "vdcAvg": 15.0, "vdcMax": 15.0,
                    "idcMin": 15.0, "idcAvg": 15.0, "idcMax": 15.0, "socAvg": 15.0, "socMin": 15.0, "socMax": 15.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 30, "commErrors": 15, "whFromInverter": 30.0,
                    "vaFromInverterMin": 30.0, "vaFromInverterAvg": 30.0, "vaFromInverterMax": 30.0,
                    "vacMin": 30.0, "vacAvg": 30.0, "vacMax": 30.0, "vdcMin": 30.0, "vdcAvg": 30.0, "vdcMax": 30.0,
                    "idcMin": 30.0, "idcAvg": 30.0, "idcMax": 30.0, "socAvg": 30.0, "socMin": 30.0, "socMax": 30.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"),
                    "commAttempts": 45, "commErrors": 22, "whFromInverter": 45.0,
                    "vaFromInverterMin": 45.0, "vaFromInverterAvg": 45.0, "vaFromInverterMax": 45.0,
                    "vacMin": 45.0, "vacAvg": 45.0, "vacMax": 45.0, "vdcMin": 45.0, "vdcAvg": 45.0, "vdcMax": 45.0,
                    "idcMin": 45.0, "idcAvg": 45.0, "idcMax": 45.0, "socAvg": 45.0, "socMin": 45.0, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [15 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'whFromInverter': (['SUM'],),
                            'vaFromInverterMin': (['MIN'],),
                            'vaFromInverterAvg': (['AVG'],),
                            'vaFromInverterMax': (['MAX'],),
                            'vacMin': (['MIN'],),
                            'vacAvg': (['AVG'],),
                            'vacMax': (['MAX'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)

        inverterChart = monitor_chart.InverterMonitorChart(self.user,self.batteryInverter.id, queryConfig, ph_model.inverter.Inverter)
        data = inverterChart.get_data()
        self.assertItemsEqual(data, EXPECTED_INVERTER_15_MINUTES_MONITOR_DATA)

    def tests_get_monitor_data_30_minutes_interval(self):
        EXPECTED_BATTERYINVERTER_30_MINUTES_MONITOR_DATA = [
                {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                     "commAttempts": 15, "commErrors": 7,  "whFromInverter": 15,
                     "vaFromInverterMin": 0.0, "vaFromInverterAvg": 7.5, "vaFromInverterMax": 15.0,  "vacMin": 0.0,
                     "vacAvg": 7.5, "vacMax": 15.0, "vdcMin": 0.0, "vdcAvg": 7.5, "vdcMax": 15.0,
                     "idcMin": 0.0, "idcAvg": 7.5, "idcMax": 15.0, "socMin": 0.0, "socAvg": 7.5, "socMax": 15.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 75, "commErrors": 37, "whFromInverter": 75.0,
                    "vaFromInverterMin": 30.0, "vaFromInverterAvg": 37.5, "vaFromInverterMax": 45.0,
                    "vacMin": 30.0, "vacAvg": 37.5, "vacMax": 45.0, "vdcMin": 30.0, "vdcAvg": 37.5, "vdcMax": 45.0,
                    "idcMin": 30.0, "idcAvg": 37.5, "idcMax": 45.0, "socMin": 30.0, "socAvg": 37.5, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'whFromInverter': (['SUM'],),
                            'vaFromInverterMin': (['MIN'],),
                            'vaFromInverterAvg': (['AVG'],),
                            'vaFromInverterMax': (['MAX'],),
                            'vacMin': (['MIN'],),
                            'vacAvg': (['AVG'],),
                            'vacMax': (['MAX'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        inverterChart = monitor_chart.InverterMonitorChart(self.user,self.batteryInverter.id, queryConfig, ph_model.inverter.Inverter)
        data = inverterChart.get_data()
        self.assertItemsEqual(data, EXPECTED_BATTERYINVERTER_30_MINUTES_MONITOR_DATA)

    def tests_get_monitor_data_30_minutes_interval_agg_by_powerstation(self):
        EXPECTED_INVERTER_30_MINUTES_MONITOR_DATA_AGG_BY_POWERSTATION = [
                {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                     "commAttempts": 30, "commErrors": 14, "whFromInverter": 30.0,
                     "vaFromInverterMin": 0.0, "vaFromInverterAvg": 7.5, "vaFromInverterMax": 15.0,
                     "vacMin": 0.0, "vacAvg": 7.5, "vacMax": 15.0, "vdcMin": 0.0, "vdcAvg": 7.5, "vdcMax": 15.0,
                     "idcMin": 0.0, "idcAvg": 7.5, "idcMax": 15.0, "socMin": 0.0, "socAvg": 3.75, "socMax": 15.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 150, "commErrors": 74, "whFromInverter": 150.0,
                    "vaFromInverterMin": 30.0, "vaFromInverterAvg": 37.5, "vaFromInverterMax": 45.0,
                    "vacMin": 30.0, "vacAvg": 37.5, "vacMax": 45.0, "vdcMin": 30.0, "vdcAvg": 37.5, "vdcMax": 45.0,
                    "idcMin": 30.0, "idcAvg": 37.5, "idcMax": 45.0, "socMin": 0.0, "socAvg": 18.75, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'whFromInverter': (['SUM'],),
                            'vaFromInverterMin': (['MIN'],),
                            'vaFromInverterAvg': (['AVG'],),
                            'vaFromInverterMax': (['MAX'],),
                            'vacMin': (['MIN'],),
                            'vacAvg': (['AVG'],),
                            'vacMax': (['MAX'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
                      }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        inverterChart = monitor_chart.InverterMonitorChart(self.user, self.powerStation.id, queryConfig, ph_model.powerstation.PowerStation)
        data = inverterChart.get_data()
        self.assertItemsEqual(data, EXPECTED_INVERTER_30_MINUTES_MONITOR_DATA_AGG_BY_POWERSTATION)

    def tests_get_monitor_data_30_minutes_interval_agg_by_grid(self):
        EXPECTED_INVERTER_30_MINUTES_MONITOR_DATA_AGG_BY_GRID = [
                {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                     "commAttempts": 30, "commErrors": 14, "whFromInverter": 30.0,
                     "vaFromInverterMin": 0.0, "vaFromInverterAvg": 7.5, "vaFromInverterMax": 15.0,
                     "vacMin": 0.0, "vacAvg": 7.5, "vacMax": 15.0, "vdcMin": 0.0, "vdcAvg": 7.5, "vdcMax": 15.0,
                     "idcMin": 0.0, "idcAvg": 7.5, "idcMax": 15.0, "socMin": 0.0, "socAvg": 3.75, "socMax": 15.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 150, "commErrors": 74, "whFromInverter": 150.0,
                    "vaFromInverterMin": 30.0, "vaFromInverterAvg": 37.5, "vaFromInverterMax": 45.0,
                    "vacMin": 30.0, "vacAvg": 37.5, "vacMax": 45.0, "vdcMin": 30.0, "vdcAvg": 37.5, "vdcMax": 45.0,
                    "idcMin": 30.0, "idcAvg": 37.5, "idcMax": 45.0, "socMin": 0.0, "socAvg": 18.75, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'whFromInverter': (['SUM'],),
                            'vaFromInverterMin': (['MIN'],),
                            'vaFromInverterAvg': (['AVG'],),
                            'vaFromInverterMax': (['MAX'],),
                            'vacMin': (['MIN'],),
                            'vacAvg': (['AVG'],),
                            'vacMax': (['MAX'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
        }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        inverterChart = monitor_chart.InverterMonitorChart(self.user, self.grid.id, queryConfig, ph_model.grid.Grid)
        data = inverterChart.get_data()
        self.assertItemsEqual(data, EXPECTED_INVERTER_30_MINUTES_MONITOR_DATA_AGG_BY_GRID)



class BatteryMonitorChartTest(BaseMonitorChartTest):
    """Tests chart_manager.InverterChart."""

    def tests_get_monitor_data_15_minutes_interval(self):
        EXPECTED_BATTERYBANK_15_MINUTES_MONITOR_DATA = [
                {   "collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                    "commAttempts": 0, "commErrors": 0,
                    "vdcMin": 0.0, "vdcAvg": 0.0, "vdcMax": 0.0,
                    "idcMin": 0.0, "idcAvg": 0.0, "idcMax": 0.0,
                    "socAvg": 0.0, "socMin": 0.0, "socMax": 0.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:15:00Z"),
                    "commAttempts": 15, "commErrors": 7,
                     "vdcMin": 15.0, "vdcAvg": 15.0, "vdcMax": 15.0,
                    "idcMin": 15.0, "idcAvg": 15.0, "idcMax": 15.0,
                    "socAvg": 15.0, "socMin": 15.0, "socMax": 15.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 30, "commErrors": 15,
                    "vdcMin": 30.0, "vdcAvg": 30.0, "vdcMax": 30.0,
                    "idcMin": 30.0, "idcAvg": 30.0, "idcMax": 30.0,
                    "socAvg": 30.0, "socMin": 30.0, "socMax": 30.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:45:00Z"),
                    "commAttempts": 45, "commErrors": 22,
                    "vdcMin": 45.0, "vdcAvg": 45.0, "vdcMax": 45.0,
                    "idcMin": 45.0, "idcAvg": 45.0, "idcMax": 45.0,
                    "socAvg": 45.0, "socMin": 45.0, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [15 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
        }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        batteryBankChart = monitor_chart.BatteryBankMonitorChart(self.user, self.batteryBank.id, queryConfig,  ph_model.battery.BatteryBank)
        data = batteryBankChart.get_data()
        self.assertItemsEqual(data, EXPECTED_BATTERYBANK_15_MINUTES_MONITOR_DATA)

    def tests_get_monitor_data_30_minutes_interval(self):
        EXPECTED_BATTERYBANK_30_MINUTES_MONITOR_DATA = [
                {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                     "commAttempts": 15, "commErrors": 7, "vdcMin": 0.0, "vdcAvg": 7.5, "vdcMax": 15.0,
                     "idcMin": 0.0, "idcAvg": 7.5, "idcMax": 15.0, "socMin": 0.0, "socAvg": 7.5, "socMax": 15.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 75, "commErrors": 37, "vdcMin": 30.0, "vdcAvg": 37.5, "vdcMax": 45.0,
                    "idcMin": 30.0, "idcAvg": 37.5, "idcMax": 45.0, "socMin": 30.0, "socAvg": 37.5, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
        }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        batteryBankChart = monitor_chart.BatteryBankMonitorChart(self.user, self.batteryBank.id, queryConfig,  ph_model.battery.BatteryBank)
        data = batteryBankChart.get_data()
        self.assertItemsEqual(data, EXPECTED_BATTERYBANK_30_MINUTES_MONITOR_DATA)

    def tests_get_monitor_data_30_minutes_interval_agg_by_powerstation(self):
        EXPECTED_BATTERYBANK_30_MINUTES_MONITOR_DATA_AGG_BY_POWERSTATION = [
                 {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                     "commAttempts": 15, "commErrors": 7, "vdcMin": 0.0, "vdcAvg": 7.5, "vdcMax": 15.0,
                     "idcMin": 0.0, "idcAvg": 7.5, "idcMax": 15.0, "socMin": 0.0, "socAvg": 7.5, "socMax": 15.0},
                 {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 75, "commErrors": 37, "vdcMin": 30.0, "vdcAvg": 37.5, "vdcMax": 45.0,
                    "idcMin": 30.0, "idcAvg": 37.5, "idcMax": 45.0, "socMin": 30.0, "socAvg": 37.5, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
        }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        batteryBankChart = monitor_chart.BatteryBankMonitorChart(self.user, self.powerStation.id, queryConfig,  ph_model.powerstation.PowerStation) ### TODO something wrong with the aggregate stuff
        data = batteryBankChart.get_data()
        self.assertItemsEqual(data, EXPECTED_BATTERYBANK_30_MINUTES_MONITOR_DATA_AGG_BY_POWERSTATION)

    def tests_get_monitor_data_30_minutes_interval_agg_by_grid(self):
        EXPECTED_BATTERYBANK_30_MINUTES_MONITOR_DATA_AGG_BY_GRID = [
                {"collectTime": get_utc_datetime( "2014-01-1T1:00:00Z"),
                     "commAttempts": 15, "commErrors": 7, "vdcMin": 0.0, "vdcAvg": 7.5, "vdcMax": 15.0,
                     "idcMin": 0.0, "idcAvg": 7.5, "idcMax": 15.0, "socMin": 0.0, "socAvg": 7.5, "socMax": 15.0},
                {"collectTime": get_utc_datetime("2014-01-1T1:30:00Z"),
                    "commAttempts": 75, "commErrors": 37, "vdcMin": 30.0, "vdcAvg": 37.5, "vdcMax": 45.0,
                    "idcMin": 30.0, "idcAvg": 37.5, "idcMax": 45.0, "socMin": 30.0, "socAvg": 37.5, "socMax": 45.0},
        ]
        queryConfig = {
                       base_chart.ChartQueryConfig.LOOK_BACK_DAYS: 3,
                       base_chart.ChartQueryConfig.FROM_DATE : '20140101',
                       base_chart.ChartQueryConfig.INTERVALS:  [30 * 60],
                       base_chart.ChartQueryConfig.GROUPING : ['merge'],
                       base_chart.ChartQueryConfig.FIELDS_CONFIG: {
                            'collectTime': (['MIN'],),
                            'commAttempts': (['SUM'],),
                            'commErrors': (['SUM'],),
                            'vdcMin': (['MIN'],),
                            'vdcAvg': (['AVG'],),
                            'vdcMax': (['MAX'],),
                            'idcMin': (['MIN'],),
                            'idcAvg': (['AVG'],),
                            'idcMax': (['MAX'],),
                            'socMin': (['MIN'],),
                            'socAvg': (['AVG'],),
                            'socMax': (['MAX'],),

                       }
        }
        queryConfig = base_chart.ChartQueryConfig(queryConfig)
        batteryBankChart = monitor_chart.BatteryBankMonitorChart(self.user, self.grid.id, queryConfig,  ph_model.grid.Grid)  
        data = batteryBankChart.get_data()
        self.assertItemsEqual(data, EXPECTED_BATTERYBANK_30_MINUTES_MONITOR_DATA_AGG_BY_GRID)
