# coding=utf-8
"""Generation Chart views.."""

import ph_model.models as ph_model
from ph_om_chart_api import base_chart
import logging
from ph_util.test_utils import test_mode

DB = 'replica'

if test_mode():
    DB = 'default'

LOGGER = logging.getLogger(__name__)


class UnSupportedChartModel(Exception):
    """Unknow model for chart."""
    pass


class AggregateGridChart(base_chart.BaseChart):
    """
    Aggregrate request
    """
    FIELDS = ('aggregateDate', 'grid', 'customersUsing', 'energyUsed', 'debits', 'averageTariff',
              'deposits', 'adjustments', 'whPerUser', 'arpu', 'totalBalance')

    def __init__(self, user, device_id, query_config, model):
        """

        :param user:            django user, o&m user.
        :param device_id:       hardware device id.
        :param query_config:    query info
        :param model:           data model
        """
        base_chart.BaseChart.__init__(self, user, device_id, query_config, model=model)

    def _get_aggregate_grid_data(self):
        """Returns grid aggregate @param modelId."""
        if self.model == ph_model.grid.Grid:
            aggregate_grid_data = ph_model.aggregate.AggregateGrid.objects.using(DB).all().values(*self.FIELDS).filter(
                grid=self.device, aggregateDate__gte=self.lookBackDaysUTCFrom, aggregateDate__lte=self.lookBackDaysUTCTo)
        else:
            raise UnSupportedChartModel('Chart for model {} not supported'.format(str(self.model)))

        return aggregate_grid_data

    def _get_raw_data(self):
        aggregate_data = self._get_aggregate_grid_data()
        gridAggregates = []
        for gridAggregate in aggregate_data:
            data = {}
            for field in self.FIELDS:
                data[field] = gridAggregate[field]
            gridAggregates.append(data)
        return gridAggregates

    def get_municipality(self):
        """
        Muni request
        :return:
        """
        return ph_model.municipality.Municipality.objects.using(DB).get(grid=self.device)
