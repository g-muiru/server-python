"""Chart views.."""


import json
import logging

import abc
import delorean

LOGGER = logging.getLogger(__name__)

from ph_om_chart_api import util as chart_util


USAGE_CHART_DATA_LOOK_BACK_DAYS = 3
USAGE_CHART_VIEW_INTERVAL_MINUTES = 15

class ChartQueryConfig(object):
    DATE_IDENTIFIER = 'dateIdentifier'
    DATE_IDENTIFIER_TYPE = 'dateIdentifierType'
    INTERVALS = 'intervals'
    GROUPING = 'grouping'
    FIELDS_CONFIG= 'fieldConfig'
    ROLL_UP_CONFIG = 'rollUpConfig'
    FROM_DATE = 'fromDate'
    LOOK_BACK_DAYS = 'lookBackDays'
    ROLL_UP_ID = 'rollupId'

    def __init__(self, config):
        if isinstance(config, unicode):
            config = json.loads(config)
        self.dateIdentifier = config.get(self.DATE_IDENTIFIER, 'collectTime')
        self.intervals = config[self.INTERVALS]
        self.grouping = config[self.GROUPING]
        self.fieldConfig = config[self.FIELDS_CONFIG]
        self.rollUpConfig = config.get(self.ROLL_UP_CONFIG, None)
        self.fromDate = config.get(self.FROM_DATE, None)
        self.lookBackDays = config.get(self.LOOK_BACK_DAYS, USAGE_CHART_DATA_LOOK_BACK_DAYS)
        self.dateIdentifierType = config.get(self.DATE_IDENTIFIER_TYPE, chart_util.DateType.UTC.value)


class BaseChart(object):
    """ABS Base chart module need to override cls.model, self._get_customer_data_usage, self._get_non_customer_data_usage."""
    # Need to be override.
    model = None

    def __init__(self, user, deviceId, queryConfig, model=None):
         """
         Arguments:
              user: django user, o&m user.
              deviceId: deviceId to fetch the data from.
              queryConfig: ChartQueryConfig
         """
         self.user = user
         if model:
             self.model= model
         self.deviceId = deviceId
         self.device = self.model.objects.get(pk=deviceId)
         fetchDateInterval = chart_util.get_query_fetch_interval(queryConfig.lookBackDays, queryConfig.fromDate,
                                                                 dateType=queryConfig.dateIdentifierType)
         #TODO(estifanos)Rename to lookBackDaysFrom since we supporting both utc and date
         self.lookBackDaysUTCFrom = fetchDateInterval[0]
         self.lookBackDaysUTCTo = fetchDateInterval[1]
         data = self._get_raw_data()
         municipality = self.get_municipality()
         timeZone = municipality and municipality.timeZone or 'UTC'
         for d in data:
             try:
               d[queryConfig.dateIdentifier] = delorean.Delorean(d[queryConfig.dateIdentifier]).shift(timeZone).datetime
             except :
               #If dateidentifier is not aware datetime or is just a date .. pass
               pass

         self.dataAgg = chart_util.DataAgg(data, queryConfig.dateIdentifier, queryConfig.intervals,
                                           queryConfig.grouping, queryConfig.fieldConfig, queryConfig.rollUpConfig)


    @abc.abstractmethod
    def _get_raw_data(self):
        """Returns circuit usage queryset not associated to a customer."""
        pass

    @abc.abstractmethod
    def get_municipality(self):
        pass

    def get_data(self):
        if (hasattr(self.dataAgg, 'data')):
            return self.dataAgg.data

        return {}