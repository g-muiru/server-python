# coding=utf-8
"""Chart views.."""
import abc
import base_chart
import logging
import ph_model.models as ph_model
from ph_util.test_utils import test_mode

DB = 'replica'

if test_mode():
    DB = 'default'

LOGGER = logging.getLogger(__name__)

MINIMUM_CHART_VIEW_ROLE = ph_model.permission.Role.VIEW.value
USAGE_CHART_DATA_LOOK_BACK_DAYS = 3
USAGE_CHART_VIEW_INTERVAL_MINUTES = 15


class UnSupportedChartModel(Exception):
    """Unknown model for chart."""
    pass


class _BaseMonitorChart(base_chart.BaseChart):
    """ABC Base monitor chart class."""

    def __init__(self, user, device_id, query_config, model):
        """

        :param user:            django user, o&m user.
        :param device_id:       hardware device id.
        :param query_config:    query info
        :param model:           data model
        """
        base_chart.BaseChart.__init__(self, user, device_id, query_config, model)

    FIELDS = []

    @abc.abstractmethod
    def _get_monitor_data(self):
        pass

    def _get_raw_data(self):
        monitor_data = self._get_monitor_data()
        monitors = []
        # noinspection PyTypeChecker
        for monitor in monitor_data:
            data = {}
            for field in self.FIELDS:
                data[field] = monitor[field]
            monitors.append(data)
        return monitor_data

    def get_municipality(self):
        """

        :return:
        """
        if self.model == ph_model.probe.Probe:
            return ph_model.municipality.Municipality.objects.using(DB).get(grid__queens__probes=self.device)
        elif self.model == ph_model.circuit.Circuit:
            return ph_model.municipality.Municipality.objects.using(DB).get(grid__queens__queen_circuits=self.device)
        elif self.model == ph_model.queen.Queen:
            return ph_model.municipality.Municipality.objects.using(DB).get(grid__queens=self.device)
        elif self.model == ph_model.grid.Grid:
            return ph_model.municipality.Municipality.objects.using(DB).get(grid=self.device)
        elif self.model == ph_model.inverter.Inverter:
            return ph_model.municipality.Municipality.objects.using(DB).get(grid__powerstation__inverters=self.device)
        elif self.model == ph_model.battery.BatteryBank:
            return ph_model.municipality.Municipality.objects.using(DB).get(grid__powerstation__battery_banks=self.device)
        elif self.model == ph_model.powerstation.PowerStation:
            return ph_model.municipality.Municipality.objects.using(DB).get(grid__powerstation=self.device)
        else:
            raise Exception('invalid model for municipality aggregation')


class QueenMonitorChart(_BaseMonitorChart):
    """
    Queen monitor request
    """
    FIELDS = ('collectTime', 'commAttempts', 'commErrors', 'rssiMin', 'rssiAvg', 'rssiMax',
              'tempMin', 'tempAvg', 'tempMax', 'photoMin', 'photoAvg', 'photoMax', 'queen_id', 'queen__grid_id',
              'fsFreePct')

    def _get_monitor_data(self):
        """Returns queen monitor raw data aggregated by @param model form @param modelId."""

        if self.model == ph_model.queen.Queen:
            monitor_data = ph_model.monitor.QueenMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                queen=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.grid.Grid:
            monitor_data = ph_model.monitor.QueenMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                queen__grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        else:
            raise UnSupportedChartModel('Chart for model {} not supported'.format(str(self.device)))

        return monitor_data


class ProbeMonitorChart(_BaseMonitorChart):
    """
    Probe monitor request
    """
    FIELDS = ['collectTime', 'commAttempts', 'commErrors', 'probe_id', 'probe__queen_id', 'probe__queen__grid_id']

    def _get_monitor_data(self):
        """Returns probe monitor raw data aggregated by @param model form @param modelId."""
        if self.model == ph_model.probe.Probe:
            monitor_data = ph_model.monitor.ProbeMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                probe=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.queen.Queen:
            monitor_data = ph_model.monitor.ProbeMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                probe__queen=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.grid.Grid:
            monitor_data = ph_model.monitor.ProbeMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                probe__queen__grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        else:
            raise UnSupportedChartModel('Chart for model {} not supported'.format(str(self.model)))

        return monitor_data


class CircuitMonitorChart(_BaseMonitorChart):
    """
    Circuit monitor request
    """
    FIELDS = ['collectTime', 'vacAvg', 'vacMin', 'vacMax', 'iacMinN', 'iacMaxN', 'iacAvgN', 'iacMinH', 'iacMaxH', 'iacAvgH',
              'commAttempts', 'commErrors', 'circuit_id', 'circuit__probe_id', 'circuit__queen_id', 'circuit__queen__grid_id']

    def _get_monitor_data(self):
        """Returns probe monitor raw data aggregated by @param model form @param modelId."""

        if self.model == ph_model.circuit.Circuit:
            monitor_data = ph_model.monitor.CircuitMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                circuit=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.probe.Probe:
            monitor_data = ph_model.monitor.CircuitMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                circuit__probe=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.queen.Queen:
            monitor_data = ph_model.monitor.CircuitMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                circuit__queen=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.grid.Grid:
            monitor_data = ph_model.monitor.CircuitMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                circuit__queen__grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        else:
            raise UnSupportedChartModel('Chart for model {} not supported'.format(str(self.model)))

        return monitor_data


class InverterMonitorChart(_BaseMonitorChart):
    """
    Inverter monitor request
    """
    FIELDS = ['collectTime', 'commAttempts', 'commErrors', 'whFromInverter', 'vaFromInverterMin', 'vaFromInverterAvg',
              'vaFromInverterMax', 'vacMin', 'vacAvg', 'vacMax', 'vdcMin', 'vdcAvg', 'vdcMax', 'idcMin', 'idcAvg', 'idcMax',
              'socMin', 'socAvg', 'socMax', 'inverter_id', 'inverter__powerstation_id', 'inverter__powerstation__grid_id']

    def _get_monitor_data(self):
        """Returns inverter monitor raw data aggregated by @param model form @param modelId."""
        if self.model == ph_model.inverter.Inverter:
            monitor_data = ph_model.monitor.InverterMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                inverter=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.powerstation.PowerStation:
            monitor_data = ph_model.monitor.InverterMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                inverter__powerstation=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.grid.Grid:
            monitor_data = ph_model.monitor.InverterMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                inverter__powerstation__grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        else:
            raise UnSupportedChartModel('Chart for model {} not supported'.format(str(self.model)))

        return monitor_data


class BatteryBankMonitorChart(_BaseMonitorChart):
    """
    Battery monitor request
    """
    FIELDS = ['collectTime', 'commAttempts', 'commErrors', 'vdcMin', 'vdcAvg', 'vdcMax',
              'idcMin', 'idcAvg', 'idcMax', 'socMin', 'socAvg', 'socMax', 'batteryBank_id',
              'batteryBank__powerstation_id', 'batteryBank__powerstation__grid_id']

    def _get_monitor_data(self):
        """Returns batteryBank monitor raw data aggregated by @param model form @param modelId."""
        if self.model == ph_model.battery.BatteryBank:
            monitor_data = ph_model.monitor.BatteryBankMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                batteryBank=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.powerstation.PowerStation:
            monitor_data = ph_model.monitor.BatteryBankMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                batteryBank__powerstation=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        elif self.model == ph_model.grid.Grid:
            monitor_data = ph_model.monitor.BatteryBankMonitor.objects.using(DB).all().values(*self.FIELDS).filter(
                batteryBank__powerstation__grid=self.device, collectTime__gte=self.lookBackDaysUTCFrom, collectTime__lt=self.lookBackDaysUTCTo)
        else:
            raise UnSupportedChartModel('Chart for model {} not supported'.format(str(self.model)))

        return monitor_data
