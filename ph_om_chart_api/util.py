"""Chart views.."""

import calendar
import datetime
import enum
import logging
from dateutil import tz
from ph_util import mixins
from ph_model.models import fields
import delorean

LOGGER = logging.getLogger(__name__)

MINIMUM_CHART_VIEW_ROLE = 2  # ph_model.permission.Role.VIEW.value
USAGE_CHART_DATA_LOOK_BACK_DAYS = 3
USAGE_CHART_VIEW_INTERVAL_MINUTES = 15


class Avg(object):
    @classmethod
    def agg(cls, listData, dataAgg=None):
        if listData:
            return sum(listData) / len(listData)


class Last(object):
    @classmethod
    def agg(cls, sequence, dataAgg=None):
        if sequence:
            return sequence[len(sequence) - 1]


class First(object):
    @classmethod
    def agg(cls, sequence, dataAgg=None):
        if sequence:
            return sequence[0]


class Sum(object):
    @classmethod
    def agg(cls, sequence, dataAgg=None):
        return sum(sequence)


class Max(object):
    @classmethod
    def agg(cls, sequence, dataAgg=None):
        return max(sequence)


class Min(object):
    @classmethod
    def agg(cls, sequence, dataAgg=None):
        return min(sequence)


class DateTimeIntervalRound(object):
    @classmethod
    def agg(cls, sequence, dataAgg):
        if not sequence:
            return
        sequence.sort()
        minDateTime = to_epoch(sequence[0])
        baseMidNightDateTime = to_epoch(get_midnight(sequence[0], dataAgg.timeZone))
        interval = dataAgg.intervals[dataAgg.currentIndex]
        roundedEpoch = interval * ((minDateTime - baseMidNightDateTime) / interval)
        return delorean.epoch(roundedEpoch + baseMidNightDateTime).shift(dataAgg.timeZone).datetime


class AggFunction(mixins.PairEnumMixin, enum.Enum):
    """Aggregate function type"""
    SUM = Sum
    MAX = Max
    MIN = Min
    AVG = Avg
    LAST = Last
    FIRST = First
    ROUND = DateTimeIntervalRound


def get_midnight(dateTime, timeZone='UTC'):
    if isinstance(dateTime, datetime.datetime):
        return dateTime.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=tz.gettz(timeZone))
    return dateTime


def get_int(value, default):
    """Helper to parse @param to int, for any exception return default value"""
    try:
        return int(value)
    except Exception:
        return default


def get_float(value, default):
    """Helper to parse @param to int, for any exception return default value"""
    try:
        return float(value)
    except Exception:
        return default


def get_list_value(values, index, defaultValue):
    """Helper to parse @param to int, for any exception return default value"""
    try:
        return values(index)
    except Exception:
        return defaultValue


def to_epoch(actualDateTime):
    return calendar.timegm(actualDateTime.timetuple())


class DateType(mixins.PairEnumMixin, enum.Enum):
    """Aggregate function type"""
    UTC = 'UTC'
    DATE = 'DATE'


def _get_query_fetch_interval_utc(lookBackNumDays, lookUpEndDate=None):
    lookUpEndDate = lookUpEndDate or datetime.datetime.utcnow()
    lookBackDaysUTCTo = lookUpEndDate.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=tz.gettz('UTC'))
    lookBackDaysUTCFrom = lookBackDaysUTCTo - datetime.timedelta(days=lookBackNumDays)
    lookBackDaysUTCTo = lookBackDaysUTCTo + datetime.timedelta(days=1)
    return (lookBackDaysUTCFrom, lookBackDaysUTCTo)


def _get_query_fetch_interval_date(lookBackNumDays, lookUpEndDate=None):
    lookUpEndDate = lookUpEndDate or datetime.date.today()
    lookBackDaysFrom = lookUpEndDate - datetime.timedelta(days=lookBackNumDays)
    lookBackDaysTo = lookUpEndDate
    return (lookBackDaysFrom, lookBackDaysTo)


def get_query_fetch_interval(lookBackNumDays, fromDateString=None, dateType=DateType.UTC.value, dateFormat="%Y%m%d"):
    """
    Returns look back days interval utc from is inclusive while to is not

    :param lookBackNumDays:
    :param fromDateString:
    :param dateType:
    :param dateFormat:
    :return: tuple of date interval.
    """

    numLookBackDays = get_int(lookBackNumDays, USAGE_CHART_DATA_LOOK_BACK_DAYS)
    try:
        lookUpEndDate = datetime.datetime.strptime(fromDateString, dateFormat)
    except Exception:
        lookUpEndDate = None

    if DateType.value_of_case_insensitive(dateType) == DateType.UTC.value:
        return _get_query_fetch_interval_utc(numLookBackDays, lookUpEndDate)
    elif DateType.value_of_case_insensitive(dateType) == DateType.DATE.value:
        return _get_query_fetch_interval_date(numLookBackDays, lookUpEndDate)
    raise Exception('unsupported date type {}'.format(dateType))


class DictGrouper(dict):
    """Python dict grouper."""

    def __init__(self, seq, key=lambda x: x):
        for value in seq:
            k = key(value)
            self.setdefault(k, []).append(value)

    __iter__ = dict.iteritems


class InvalidGroupingDataConfig(Exception):
    """Chart data config is not valid."""
    pass


class DataAgg(object):
    """Appends grouping info in raw data, as well groups by minute or day interval."""

    AGG_FUNCTION_INDEX = 0
    ID_GROUPER = 'GROUP'
    ID_GROUPER_VALUE_TEMPLATE = 'G-{}-{}'
    ROLL_UP_GROUPER_VALUE_TEMPLATE = 'RG-{}-{}'
    ROLL_UP_GROUPER = 'ROLL_UP_GROUP'

    def __init__(self, data, dateIdentifier, intervals, grouping, fieldConfig, rollUpConfig=None, timeZone='UTC'):
        """
        data: list of dict data
        dateIdentifier: Date identifier used in the data series.
        intervals=[b2, b1] grouping intervals, list of seconds e.g. group data every minutes will be 60. If we are
        doing multiple grouping, bottom up data will be used. e.g first agg thirty minutes data then aggregate daily
        then agg weekly will be intervals=[30*60, 24*60*60, 24*60*60*7]
        grouping: Grouping mechanism either merging or cumulative
        fieldsConfig= Fields configuration , key is field name, values tuple of configuration. First configuration is
        aggregate function for each interval aggregating intervals, second configuration is specify grouping
        mechanism, e.g merging will merge all the data according to the aggregate function, accumulate will not
        merge the data but only accumulate on specified fields.
        {f1:  ([b2, b1],[merge, acc]), f2:  ([b2, b1], [merge, acc])}
        """
        if not data:
            return
        self.timeZone = timeZone
        self.currentIndex = -1
        self.dateIdentifier = dateIdentifier
        self.data = sorted(data, key=self.getDateSortKey)
        self.intervals = intervals
        self.rollUpInterval = intervals[0]
        self.rollUpInterval = intervals[len(intervals) - 1]
        self.rollUpConfig = rollUpConfig
        self.dataId = self.get_or_default(rollUpConfig, 0, '')
        self.rollUpId = self.get_or_default(rollUpConfig, 1, self.dataId)
        self.grouping = grouping
        self.fieldConfig = fieldConfig
        self.intervalLength = len(self.intervals)
        self.baseDateEpoch = []
        for i in xrange(self.intervalLength):
            self.baseDateEpoch.append(to_epoch(get_midnight(self.data[0][dateIdentifier], self.timeZone)))
        self._agg()
        self.clean_up_group_info(self.data)
        if self.data:
            self.data = sorted(self.data, key=self.getDateSortKey)

    def getSortKey(self, item):
        """Sort key."""
        return (item[self.ID_GROUPER], item[self.dateIdentifier], item[self.ROLL_UP_GROUPER])

    def getDateSortKey(self, item):
        return (item[self.dateIdentifier])

    def get_or_default(self, sequence, index, default=None):
        try:
            return sequence[index]
        except Exception:
            return default

    def clean_up_group_info(self, data):

        reqKeys = self.fieldConfig.keys()
        for datum in data:
            for key in datum.keys():
                if key not in reqKeys:
                    datum.pop(key)

    def _agg(self):
        for i in xrange(self.intervalLength):
            for datum in self.data:
                toEpoch = to_epoch(datum[self.dateIdentifier])
                secondDiff = toEpoch - self.baseDateEpoch[i]
                interval = self.intervals[i]
                if interval == 0:
                    datum[self.ID_GROUPER] = self.ID_GROUPER_VALUE_TEMPLATE.format(self.dataId, 0)
                    datum[self.ROLL_UP_GROUPER] = self.ROLL_UP_GROUPER_VALUE_TEMPLATE.format(self.rollUpId, 0)
                    continue
                datum[self.ID_GROUPER] = self.ID_GROUPER_VALUE_TEMPLATE.format(datum.get(self.dataId, ''), secondDiff / interval)
                datum[self.ROLL_UP_GROUPER] = self.ROLL_UP_GROUPER_VALUE_TEMPLATE.format(datum.get(self.rollUpId, ''),
                                                                                         secondDiff / self.rollUpInterval)

            if self.grouping[i] == 'merge':
                groupedData = [g for k, g in DictGrouper(self.data, key=lambda x: x['GROUP'])]
                self.data = self._merge_data(groupedData, i)
            elif self.grouping[i] == 'acc':
                groupedData = [g for k, g in DictGrouper(self.data, key=lambda x: x['GROUP'])]
                self.data = self._accumulate_data(groupedData, i)
            elif self.grouping[i] == 'rollup':
                groupedData = [g for k, g in DictGrouper(self.data, key=lambda x: x['ROLL_UP_GROUP'])]
                if self.rollUpConfig[2] == 'acc':
                    self.data = self._accumulate_data(groupedData, i)
                elif self.rollUpConfig[2] == 'merge':
                    self.data = self._merge_data(groupedData, i)

    def _merge_data(self, groupedData, index):
        self.currentIndex = index
        mergedData = []
        for g in groupedData:
            data = g[0]
            for field, conf in self.fieldConfig.iteritems():
                aggFunctionValue = conf[self.AGG_FUNCTION_INDEX][index]
                if type(aggFunctionValue) == AggFunction:
                    aggFunction = aggFunctionValue.value
                else:
                    aggFunction = AggFunction.value_of_case_insensitive(aggFunctionValue)
                data[field] = aggFunction.agg([d[field] for d in g], self)

            mergedData.append(data)
        return sorted(mergedData, key=self.getSortKey)

    def _accumulate_data(self, groupedData, index):
        self.currentIndex = index
        accumlatedData = []
        groupedData.sort()
        for sameIntervalData in groupedData:
            fieldValue = {}
            for data in sameIntervalData:
                resultData = data
                for field, conf in self.fieldConfig.iteritems():
                    aggFunctionValue = conf[self.AGG_FUNCTION_INDEX][index]
                    if type(aggFunctionValue) == AggFunction:
                        aggFunction = aggFunctionValue.value
                    else:
                        aggFunction = AggFunction.value_of_case_insensitive(aggFunctionValue)
                    savedFieldValue = fieldValue.get(field)
                    if savedFieldValue:
                        savedFieldValue = aggFunction.agg([savedFieldValue, data[field]], self)
                    else:
                        savedFieldValue = data[field]
                    fieldValue[field] = savedFieldValue
                    resultData[field] = savedFieldValue
                accumlatedData.append(resultData)
        return accumlatedData
