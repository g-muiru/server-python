from locust import HttpLocust, TaskSet
import json
import datetime


"""
basic loadtesting of queen interactions via GET and POST endpoints
to run: 
    with ServerPython running on port 8001 (ex: python manage.py runserver 8001), launch locust server with
        locust --host=http://127.0.0.1:8001
    and then open http://127.0.0.1:8089/ in your browser for the locust interface
"""

def login(l):
    payload = {'some': 'data'}
    probes = {"header":{"collectTime":"2016-06-07T20:18:50Z","queenDeviceId":"1002BBBK2006"},"data":{"probes":[{"deviceId":"2915PROB0036","commAttempts":0,"commErrors":0},{"deviceId":"2915PROB0018","commAttempts":0,"commErrors":0},{"deviceId":"2915PROB0007","commAttempts":20,"commErrors":0},{"deviceId":"2915PROB0003","commAttempts":4,"commErrors":0},{"deviceId":"2915PROB0022","commAttempts":0,"commErrors":0},{"deviceId":"2915PROB0016","commAttempts":0,"commErrors":0},{"deviceId":"2915PROB0033","commAttempts":0,"commErrors":0},{"deviceId":"2915PROB0021","commAttempts":0,"commErrors":0},{"deviceId":"2915PROB0030","commAttempts":0,"commErrors":0},{"deviceId":"2915PROB0029","commAttempts":0,"commErrors":0}]}}
    headers = {'content-type': 'application/json'}

    l.client.post("/api/probes/monitor/", data=json.dumps(probes), headers=headers, auth=('superuser', 'superuser'))

def queenMonitor(l):
    timeString = "2016-06-20T22:32:13Z"
    collectTime = str(datetime.datetime.utcnow())
    monitor = {"header":{"collectTime":collectTime,"queenDeviceId":"1002BBBK2006"},"data":{"queen":{"osUptime":775533,"fwUptime":356151,"commAttempts":5,"commErrors":0,"rssiAvg":0,"rssiMin":0,"rssiMax":0,"tempAvg":27.984375000,"tempMin":27.937500000,"tempMax":28,"photoAvg":1282.083333333,"photoMin":1281,"photoMax":1286,"fsFreeStatus":0,"fsFreePct":28.133253098}}}
    headers = {'content-type': 'application/json'}

    l.client.post("/api/queens/1002BBBK2006/monitor/", data=json.dumps(monitor), headers=headers, auth=('superuser', 'superuser'))

def firmware(l):
    l.client.get("/api/queens/1002BBBK2006/")

def circuit(l):
    l.client.get("/api/queens/1002BBBK2006/circuit/")

def command(l):
    l.client.get("/api/queens/1002BBBK2006/command/")

class UserBehavior(TaskSet):
    tasks = {queenMonitor:2, command:2, firmware:1}

    def on_start(self):
        queenMonitor(self)

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait=5000
    max_wait=9000
